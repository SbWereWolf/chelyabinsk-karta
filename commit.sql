use scheme;

create table raw_permit
(
    id          int unsigned auto_increment
        primary key,
    permit      varchar(20)  null,
    issuing_at  varchar(20)  null,
    start       varchar(20)  null,
    finish      varchar(20)  null,
    distributor varchar(200) null,
    type        varchar(100) null,
    remark      varchar(200) null
);

create table address_response
(
    id            int unsigned auto_increment
        primary key,
    raw_permit_id int unsigned null,
    response      longtext     null,
    error         longtext     null,
    constraint address_gps_raw_permit_id_fk
        foreign key (raw_permit_id) references raw_permit (id)
);

create index address_gps_raw_permit_id_id_index
    on address_response (raw_permit_id, id);

create table point_suggestion
(
    id            int unsigned auto_increment
        primary key,
    raw_permit_id int unsigned null,
    longitude     double       null,
    latitude      double       null,
    formatted     varchar(200) null,
    request       varchar(200) null,
    re_request    varchar(200) null,
    error         varchar(200) null,
    constraint point_suggestion_raw_permit_id_fk
        foreign key (raw_permit_id) references raw_permit (id)
);

create index point_suggestion_raw_permit_id_id_index
    on point_suggestion (raw_permit_id, id);

create table with_city_response
(
    id            int unsigned auto_increment
        primary key,
    raw_permit_id int unsigned null,
    response      longtext     null,
    error         longtext     null,
    constraint with_city_response_raw_permit_id_fk
        foreign key (raw_permit_id) references raw_permit (id)
);

create index with_city_response_raw_permit_id_id_index
    on with_city_response (raw_permit_id, id);

create table tx_scheme_construction
(
    uid      int auto_increment
        primary key,
    x        decimal(11, 8) default 0.00000000 not null,
    y        decimal(11, 8) default 0.00000000 not null,
    angle    decimal(14, 4) default 0.0000     not null,
    sector   int            default 0          not null,
    typename varchar(40)    default ''         not null,
    type     int            default 0          not null,
    svgdata  longblob                          null
);

create index tx_scheme_construction_type_x_y_index
    on tx_scheme_construction (type, x, y);

create table tx_type
(
    id    int unsigned not null
        primary key,
    title varchar(100) null
);

create table tx_permit
(
    id         int unsigned auto_increment
        primary key,
    permit     int unsigned null,
    issuing_at int unsigned null,
    start      int unsigned null,
    finish     int unsigned null,
    tx_type_id int unsigned null,
    address    varchar(200) null,
    longitude  double       null,
    latitude   double       null,
    constraint tx_permit_tx_type_id_fk
        foreign key (tx_type_id) references tx_type (id)
);

create index tx_permit_tx_type_id_index
    on tx_permit (tx_type_id);

create table tx_permit_scheme_construction
(
    id                         int auto_increment
        primary key,
    tx_permit_id               int unsigned   null,
    tx_scheme_construction_uid int            null,
    allowance                  decimal(11, 8) null,
    distance                   decimal(11, 8) null,
    constraint tx_permit_scheme_construction_tx_permit_id_fk
        foreign key (tx_permit_id) references tx_permit (id),
    constraint tx_permit_scheme_construction_tx_scheme_construction_uid_fk
        foreign key (tx_scheme_construction_uid)
            references tx_scheme_construction (uid)
);

INSERT INTO `tx_type` (`id`, `title`)
VALUES (0, '0');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (1, 'white-long-rectangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (2, 'six-rectangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (3, 'skipped');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (4, 'twice-rectangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (5, 'white-rectangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (6, 'black-rectangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (7, 'crocodile');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (8, 'white-cube');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (9, 'black-cube');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (10, 'white-circle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (11, 'black-circle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (12, 'white-circle-with-dot');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (13, 'white-triangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (14, 'black-triangle');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (15, 'flag');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (16, 'star');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (17, 'cross');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (18, 'V');
INSERT INTO `tx_type` (`id`, `title`)
VALUES (19, 'arrow');
