INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1, '414', '7/28/2014', '5/1/2014', '4/30/2019', 'ИП Еремина И.А.', 'стела', 'ул.Труда,153');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2, '415', '7/28/2014', '8/1/2014', '7/31/2019', 'ЗАО"Железнодорожное рекламное агентство ЛАЙСА"',
        'щитовая установка', 'ул.Новомеханическая,напротив дома по адресу:ул.Кожзаводская,108А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (3, '416', '7/28/2014', '8/1/2014', '7/31/2019', 'ЗАО"Железнодорожное рекламное агентство ЛАЙСА"',
        'щитовая установка', 'ул.Новомеханическая,напротив дома по адресу:ул.Российская,13Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (4, '417', '7/28/2014', '8/1/2014', '7/31/2019', 'ЗАО"Железнодорожное рекламное агентство ЛАЙСА"',
        'щитовая установка', 'автодорога Меридиан,напротив здания по адресу:ул.Артиллерийская,136');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (5, '418', '7/28/2014', '8/1/2014', '7/31/2019', 'ЗАО"Железнодорожное рекламное агентство ЛАЙСА"',
        'щитовая установка', 'автодорога Меридиан,напротив здания по адресу:ул.Артиллерийский пер.,д.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (6, '419', '7/28/2014', '8/1/2014', '7/31/2019', 'ЗАО"Железнодорожное рекламное агентство ЛАЙСА"',
        'щитовая установка', 'автодорога Меридиан,напротив здания по адресу:ул.Луценко,2/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (7, '1272', '8/6/2014', '8/1/2014', '7/31/2024', 'ООО РА "Гранат"', 'ЩУ арочного типа',
        'ул.Университетская Набережная,94');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (8, '1273', '8/7/2014', '8/1/2014', '7/31/2024', 'ИП Трифонов Э.С.', 'ЩУ арочного типа',
        'автодорога Меридиан,ост. "Сады"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (9, '1284', '8/8/2014', '8/1/2014', '7/31/2019', 'ИП Суходоев Д.А.', 'стела', 'ул.Героев Танкограда,21/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (10, '1285', '8/11/2014', '8/1/2014', '7/31/2019', 'ИП Никитин А.Н.', 'сити-формат', 'ул.Энтузиастов,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (11, '1286', '8/13/2014', '8/1/2014', '7/31/2019', 'ООО"Челябинск-Хино"', 'Флаги', 'ул.Дарвина,2В');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (12, '1287', '8/13/2014', '8/1/2014', '7/31/2019', 'ООО"Челябинск-Хино"', 'стела', 'ул.Дарвина,2В');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (13, '1288', '8/13/2014', '8/1/2014', '7/31/2019', 'ООО ПК "Гарантия"', 'щитовая установка',
        'ул.Героев Танкограда,67П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (14, '1292', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Очки и мода"', 'стела',
        'ул.Первой Пятилетки, на уровне дома № 15 по ул.40 лет Октября');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (15, '1293', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Очки и мода"', 'стела',
        'ул.Героев Танкограда, на уровне дома № 15 по ул. 40 лет Октября');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (16, '1294', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'панель-кронштейн',
        'ул.Блюхера,44,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (17, '1295', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'панель-кронштейн',
        'ул.Блюхера,44,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (18, '1296', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'панель-кронштейн',
        'ул.Блюхера,44,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (19, '1297', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'панель-кронштейн',
        'ул.Дарвина,18,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (20, '1298', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'панель-кронштейн',
        'ул.Дарвина,18,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (21, '1299', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'панель-кронштейн',
        'ул.Дарвина,18,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (22, '1300', '8/19/2014', '8/1/2014', '7/31/2019', 'ООО"Уральский технический центр"', 'стела', 'ул.Блюхера,67');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (23, '1320', '9/3/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.', 'ул.Комарова,34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (24, '1321', '9/3/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.', 'ул.Комарова,34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (25, '1322', '9/3/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.', 'ул.Комарова,34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (26, '1323', '9/3/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.', 'ул.Комарова,34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (27, '1324', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка', 'пр.Комарова, 60');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (28, '1325', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Блюхера, 97, 200 м до ул.Кузнецова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (29, '1326', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Калинина и ул.Тагильская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (30, '1328', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка', 'пр.Победы, 289');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (31, '1329', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.50 лет ВЛКСМ,напротив д.19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (32, '1330', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение автодороги Меридиан (в центр) и пер. 6-й Целинный');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (33, '1331', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Карус"', 'щитовая установка',
        'Троицкий тр.(в город), 750 м до виадука на п. Новосинеглазово');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (34, '1332', '9/9/2014', '8/1/2014', '7/31/2019', 'ООО"Национальная водная компания "Ниагара"', 'стела',
        'пересеч.ул.Новороссийская и пер.Бугурусланский');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (35, '1333', '9/17/2014', '8/1/2014', '7/31/2019', 'ЗАО Торговый дом "БОВИД"', 'стела', 'пр.Ленина,28д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (36, '1334', '9/17/2014', '8/1/2014', '7/31/2019', 'ЗАО Торговый дом "БОВИД"', 'Флаги', 'пр.Ленина,28д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (37, '1335', '9/18/2014', '8/1/2014', '7/31/2019', 'ООО"Лукойл-Уралнефтепродукт"', 'Флаги', 'ул.Труда,185/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (38, '1355', '10/20/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.',
        'ул.Пионерская,4,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (39, '1356', '10/20/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.',
        'Свердловский пр.,40а,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (40, '1357', '10/20/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.',
        'Свердловский пр.,40а,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (41, '1358', '10/20/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.',
        'Свердловский тр.,12б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (42, '1360', '10/20/2014', '6/1/2014', '5/31/2019', 'ООО"Шининвест"', 'нестанд.рекл.констр.',
        'Копейское шоссе,35д,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (43, '1370', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"Знак"', 'стела', 'ул.Энтузиастов,18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (44, '1373', '11/11/2014', '10/24/2014', '10/23/2019', 'Нотариус Афанасьева Т.И.', 'панель-кронштейн',
        'ул.Блюхера,10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (45, '1374', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"Уралавтохаус"', 'панель-кронштейн',
        'Копейское шоссе,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (46, '1375', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"Уралавтохаус"', 'панель-кронштейн',
        'Копейское шоссе,9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (47, '1376', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО УРК "ПЕРЕКРЕСТОК"', 'стела', 'Свердловский пр.,32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (48, '1377', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"Аргумент"', 'панель-кронштейн', 'ул.Чайковсокго,60');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (49, '1378', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"Аргумент"', 'панель-кронштейн',
        'Свердловский пр., на уровне дома № 21 по ул. Воровского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (50, '1379', '11/11/2014', '10/24/2014', '10/23/2019', 'ИП Евдокимов А.А.', 'щитовая установка',
        'Троицкий тр.,21 к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (51, '1380', '11/11/2014', '10/24/2014', '10/23/2019', 'ИП Евдокимов А.А.', 'щитовая установка',
        'Свердловский тр., 1а/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (52, '1381', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.250 летия Челябинска,17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (53, '1382', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Академика Королева,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (54, '1384', '11/11/2014', '10/24/2014', '10/23/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Машиностроителей,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (55, '1385', '11/13/2014', '10/24/2014', '10/23/2019', 'ОАО"Челябинский завод "Теплоприбор"',
        'транспарант-перетяжка', 'ул.2-я Павелецкая,36');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (56, '1387', '11/18/2014', '10/24/2014', '10/23/2019', 'ЗАО"ЧСДМ"', 'стела', 'ул.С.Разина,1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (57, '1388', '11/18/2014', '10/24/2014', '10/23/2019', 'ООО ЧОП "Варяг-2"', 'панель-кронштейн',
        'ул.Степана Разина,9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (58, '1389', '11/18/2014', '10/24/2014', '10/23/2019', 'ООО"Росарматура"', 'стела', 'ул.Блюхера,61');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (59, '1390', '11/18/2014', '10/24/2014', '10/23/2019', 'ИП Грачева О.Г.', 'транспарант-перетяжка',
        'Комсомольский пр.,69');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (60, '1393', '11/18/2014', '10/24/2014', '10/23/2019', 'ИП Мишкин С.Е.', 'стела', 'ул.Бр.Кашириных,79');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (61, '1397', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'стела',
        'ул. Братьев Кашириных,126,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (62, '1398', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'стела',
        'ул. Братьев Кашириных,126,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (63, '1399', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'Флаги',
        'ул. Братьев Кашириных,126,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (64, '1400', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'Флаги',
        'ул. Братьев Кашириных,126,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (65, '1401', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'Флаги',
        'ул. Братьев Кашириных,126,поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (66, '1402', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'Флаги',
        'ул. Братьев Кашириных,126,поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (67, '1403', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Автоцентр "Гольфстрим"', 'Флаги',
        'ул. Братьев Кашириных,126,поз. 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (68, '1404', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'стела',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (69, '1405', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'стела',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (70, '1406', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'Флаги',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (71, '1407', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'Флаги',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (72, '1408', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'Флаги',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (73, '1409', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'Флаги',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (74, '1410', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО Автоцентр"Чешский дом"', 'Флаги',
        'ул. Чичерина/ул. Братьев Кашириных,126,поз. 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (75, '1411', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Планета Авто"', 'стела', 'Копейское шоссе,82');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (76, '1412', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Планета Авто"', 'стела',
        'ул.Бр.Кашириных,137,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (77, '1413', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Планета Авто"', 'стела',
        'ул.Бр.Кашириных,137,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (78, '1414', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Планета Авто"', 'стела',
        'ул.Бр.Кашириных,137,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (79, '1415', '11/28/2014', '10/24/2014', '10/23/2019', 'ООО"Планета Авто"', 'стела',
        'ул.Бр.Кашириных,137,поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (80, '1416', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 83, автобус. ост. «ЮУрГУ» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (81, '1417', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 71а, троллейбус. ост. «Агроинженерная академия» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (82, '1418', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Энгельса, 63, троллейбус. ост. «Агроинженерная академия» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (83, '1419', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Худякова, 6, автобус. ост. «ул. Худякова» (из центра),');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (84, '1420', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Худякова, 11, автобус. ост. «ул. Худякова»  (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (85, '1421', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 65, троллейбус. ост. «Алое Поле» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (86, '1422', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., напротив д. 64, трамвайн. ост. «Алое Поле» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (87, '1423', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., напротив д.64, автобус. ост. «Алое Поле» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (88, '1424', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 64д, автобусн. ост. «Алое Поле» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (89, '1425', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 53, трамвайн. ост. «площадь Революции» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (90, '1426', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'площадь Революции, 1, трамвайн. ост. «площадь Революции» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (91, '1427', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 51, автобусн. ост. «площадь Революции» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (92, '1428', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул.Цвиллинга/ пр. Ленина, 52, автобусн. ост. «площадь Революции» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (93, '1429', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Свердловский, 84, троллейбусн. ост. «ул. Южная» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (94, '1430', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, 30, троллейбусн. ост. «ул. Курчатова» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (95, '1431', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, 21, троллейбусн. ост. «Горбольница» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (96, '1432', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, 16к7, троллейбусн. ост. «ул. Южная» ( в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (97, '1433', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, 23, автобусн. ост. «гостиница Центральная» (в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (98, '1434', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, 40, троллейбусн. ост. «ул. Доватора» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (99, '1435', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Тарасова, 56, троллейбусн. ост. «Мединститут» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (100, '1436', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Междугородная, напротив д. 21, трамвайн. ост. «Медгородок» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (101, '1437', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 45, ост. «Областная больница» (автобус рейсовый, в сторону Уфимского тракта)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (102, '1438', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 67, троллейбусн. ост. «Мебельная фабрика» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (103, '1439', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 85а, троллейбусн. ост. «поселок Мебельный» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (104, '1440', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Ярославская, 1к2, троллейбусн. ост. «АМЗ», (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (105, '1441', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Ярославская, напротив д.1к2 , троллейбусн. ост. «АМЗ» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (106, '1442', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Степана Разина, напротив д.4, трамвайн. ост. «ж/д вокзал» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (107, '1443', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Привокзальная площадь, 1, троллейбусн.ост. «Привокзальная площадь» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (108, '1444', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 173, троллейбусн. ост. «ж/д институт» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (109, '1445', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 46, троллейбусн. ост. «Детский мир» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (110, '1446', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 139, троллейбусн. ост. «Детский мир» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (111, '1447', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 68, троллейбусн.ост. «Детский мир» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (112, '1448', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 18, трамвайн. ост. «Комсомольская площадь» (в сторону северо-запада)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (113, '1449', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Российская, 43, автобусн. ост. «ул. Российская» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (114, '1450', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, напротив д. 2а, трамвайн. ост. «Теплотехнический институт» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (115, '1451', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 168, автобусн. ост. «Теплотехнический институт» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (116, '1452', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 1б, автобусн. ост. «Теплотехнический институт» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (117, '1453', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 2, автобусн. ост. «Цирк» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (118, '1454', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, напротив д. 5, автобусн. ост. «Оперный театр» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (119, '1455', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, 7, трамвайн. ост. «Оперный театр» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (120, '1456', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Калинина, 34, троллейбусн. ост. «ул. Калинина» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (121, '1457', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 35а, троллейбусн. ост. «ул. Калинина» (в  центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (122, '1458', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 16, троллейбусн. ост. «ул. Островского» (в  центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (123, '1459', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комсомольский, 10/1, троллейбусн. ост. «ул. Краснознаменная» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (124, '1460', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Краснознаменная, 1а/1, троллейбусн. ост. «ул. Краснознаменная» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (125, '1461', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комсомольский, 14, троллейбусн. ост. «ул. Косарева» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (126, '1462', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комсомольский, 23д, троллейбусн. ост. «ул. Косарева» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (127, '1463', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комсомольский, 61, троллейбусн. ост. «ул. Ворошилова» ( в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (128, '1464', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 85, троллейбусн. ост. «8-й микрорайон» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (129, '1465', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 103, автобусн. ост. «11-й микрорайон» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (130, '1466', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул.Чичерина/ пр. Победы, 392, автобусн. ост. «ул. Чичерина» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (131, '1467', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 327, автобусн.  ост. «ул. Чичерина» ( в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (132, '1468', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул.Чичерина/ пр. Победы, напротив д. 325, автобусн. ост. «ул. Чичерина» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (133, '1469', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 95б, троллейбусн. ост. «ул. Северо-Крымская» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (134, '1470', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных/ ул. Партизанская, 62, троллейбусн.  ост. «ул. Северо-Крымская» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (135, '1471', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных/ ул. Молодогвардейцев, 70а, автобусн. ост. «ул. Молодогвардейцев» ( из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (136, '1472', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных/ ул. Ворошилова, 57, автобусн. ост. «ул. Ворошилова» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (137, '1473', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 102, автобусн. ост. «ул. Солнечная» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (138, '1474', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 129а, автобусн. ост. «Челябинский государственный университет» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (139, '1475', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 108/1, автобусн. ост. «24-й микрорайон» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (140, '1476', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 40-летия Победы, 36, автобусн. ост. «ул. 40-летия Победы» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (141, '1477', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Чичерина, 29, автобусн.ост. «ул. 250 лет Челябинску» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (142, '1478', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 6, автобусн. ост. «Политехнический техникум» (в сторону ул. Новороссийская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (143, '1479', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 32, троллейбусн. ост. «кинотеатр «Аврора» (в сторону ул. Новороссийская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (144, '1480', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        ' ул. Гагарина, 35а, троллейбусн. ост. «Управление соцзащиты населения» (в сторону КБС)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (145, '1481', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дзержинского, 130, автобусн. ост. «ул. Барбюса» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (146, '1482', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дзержинского, 109, автобусн. ост. «ул. Барбюса» (из центра),');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (147, '1483', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 39, троллейбусн. ост. «библиотека им. Мамина-Сибиряка» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (148, '1484', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 55а, троллейбусн. ост. «магазин Спорттовары» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (149, '1485', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 40-летия Победы/ пр. Комсомольский, 105, автобусн. ост. «11-й микрорайон» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (150, '1486', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 100, автобусн. ост. «11-й микрорайон» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (151, '1487', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 54, троллейбусн. ост. «пр. Победы» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (152, '1488', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных/ ул. Парковая, 5, автобусн. ост. «ул. Партизанская» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (153, '1489', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 89, автобусн. ост. «ул. Партизанская» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (154, '1490', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бр. Кашириных, 73, автобусн. ост. «ул. Полковая» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (155, '1491', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 40-летия Победы, 35, автобусн. ост. «ул. 40-летия Победы» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (156, '1492', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул.Героев танкограда/ пр. Ленина, 8а, троллейбусн. ост. «Театр ЧТЗ» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (157, '1493', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Привокзальная площадь, 1, ост. «ж/д вокзал» (автобус №18, в сторону центра),');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (158, '1494', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'Привокзальная площадь, 1, ост. «ж/д вокзал» (автобус № 64, в сторону центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (159, '1495', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 7, автобусн. ост. «ул. Калинина» (в сторону Теплотехнического института)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (160, '1496', '11/28/2014', '10/24/2014', '10/23/2019', 'ИП Сидоров А.Н.', 'Стенд',
        'ул.Энгельса/ пр. Ленина, 73, автобусн. ост. «Агроинженерная академия» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (161, '1521', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Красная,42-ул.К.Либкнехта, до перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (162, '1522', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Свободы,44 - ул.Коммуны, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (163, '1523', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Коммуны, 87 - ул.Елькина, до перекр.в центр, у рест. "Бад Гаштейн"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (164, '1524', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Цвиллинга,10- ул.К.Маркса, за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (165, '1525', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'пр.Ленина,76, из центра, у ЮУРГУ, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (166, '1526', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'пр.Ленина,76, из центра, у ЮУРГУ, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (167, '1527', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'пр.Ленина,76, из центра, у ЮУРГУ, поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (168, '1528', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Бр.Кашириных, в центр, до ост. "Солнечная"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (169, '1529', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Дзержинского,104 - ул.Гагарина, 100м за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (170, '1530', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.К.Маркса,52 - ул.Пушкина, за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (171, '1531', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Красная-ул.К.Маркса, до перекр.в центр, у сквера');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (172, '1532', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'пр Ленина,50-ул.Советская, 100м до перекр.в центр, у маг. "Ювелирный зал"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (173, '1533', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'пр.Ленина,50 - ул. Советская, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (174, '1534', '12/10/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"',
        'панель-кронштейн на собст.опоре', 'ул.Свободы,60 - ул.Коммуны, за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (175, '1536', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Копейское шоссе, 50 м до поворота на Мясокомбинат');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (176, '1537', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Ленина,72 и ул.Энтузиастов');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (177, '1538', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Ленина,66а и ул.Энгельса');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (178, '1539', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Ленина,62 и ул.Васенко,96');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (179, '1540', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Ленина,45 и ул.Свободы,70');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (180, '1541', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пр.Ленина,28 (Агентство аэрофлота)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (181, '1542', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пр.Ленина,54 (середина дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (182, '1543', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Ленина,54 и ул.Цвиллинга');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (183, '1544', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Ленина и ул.Кирова,177');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (184, '1545', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение ул.Воровского (в центр) и ул.Тимирязева, за перекрестком');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (185, '1546', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр. Ленина,43 и ул. Свободы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (186, '1547', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр. Ленина,18  и ул.Горького ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (187, '1548', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр. Ленина,29 и ул. 3 Интернационала');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (188, '1549', '12/11/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'Тумба',
        'пересечение пр.Победы,168 и ул.Кирова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (189, '1563', '12/12/2014', '11/29/2014', '11/28/2019', 'ООО «РозаМира.ком»', 'сити-формат',
        'ул.Пушкина/ул.Тимирязева,27');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (190, '1564', '12/12/2014', '10/24/2014', '10/23/2024', 'ООО"Олми-Инвест"', 'стела', 'Свердловский тр.,8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (191, '1565', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Героев Танкограда, 75, ост. «ул. Первой Пятилетки» (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (192, '1566', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Героев Танкограда, 75, ост. «ул. Первой Пятилетки» (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (193, '1567', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Воровского, 64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (194, '1568', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Кирова, 7, ост. «ул. Калинина»');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (195, '1569', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Кирова, 44, ост. «ул. Калинина»');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (196, '1570', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Братьев Кашириных, 102');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (197, '1571', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Братьев Кашириных, 114');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (198, '1572', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Братьев Кашириных, 83');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (199, '1573', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье', 'пр. Ленина, 87');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (200, '1574', '12/12/2014', '11/29/2014', '11/28/2019', 'ИП Сарсенов К.Н.', 'Рекл. на скамье',
        'ул. Братьев Кашириных, 68а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (201, '1575', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского пр. и ул.Труда,на кольце в центр, напротив д.161');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (202, '1576', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Цвиллинга,58');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (203, '1577', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Бр.Кашириных и ул.1-го Мая, 100 м за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (204, '1578', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Труда и Свердловского пр., на кольце (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (205, '1579', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Черкасская,4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (206, '1580', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'Троицкий тр., 60/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (207, '1581', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Румянцева и ул.Богдана Хмельницкого');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (208, '1582', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Красная и ул. Труда, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (209, '1583', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Бр.Кашириных и ул.Тагильская, за перекрестком в центр,у ТЦ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (210, '1584', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Комсомольского пр., 14 и ул. Косарева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (211, '1585', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского пр. и ул.Труда,напротив ДС Юность, на кольце из центра,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (212, '1586', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Воровского и ул. Блюхера, со стороны Областной больницы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (213, '1587', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Энгельса,23 и ул.Труда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (214, '1588', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Бр.Кашириных и Свердловского пр., за перекр. в центр, у авт.ост."Свердловский пр."');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (215, '1589', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Гагарина, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (216, '1590', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Черкасская, напротив д. 6 и ул. 50 лет ВЛКСМ, на кольце');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (217, '1591', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Труда и ул.Красная,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (218, '1592', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы и ул. Косарева, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (219, '1593', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Дзержинского, 93');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (220, '1594', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Труда и ул.Красная,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (221, '1597', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы, 149 и ул. Болейко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (222, '1598', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского пр. и ул.Труда,напротив ДС Юность, на кольце из центра, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (223, '1599', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'Троицкийтр., 48Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (224, '1600', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Кирова, 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (225, '1601', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 33Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (226, '1602', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Блюхера и ул. Крупской, 34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (227, '1603', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Воровского, 43А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (228, '1604', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул.Бр. Кашириных и ул. Краснознаменная, 41/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (229, '1605', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Калинина, 17 и ул. Кыштымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (230, '1607', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., 10/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (231, '1608', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Елькина, 63 и ул. Карла Либкнехта');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (232, '1609', '12/18/2014', '11/29/2014', '11/28/2019', 'ООО "РФ"Армада Аутдор"', 'щитовая установка',
        'дорога в Аэропорт, на кольце дорожной развязки в Металлургический район');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (233, '1610', '12/18/2014', '11/29/2014', '11/28/2019', ' ООО"Стрит-Медиа"', 'щитовая установка',
        'ул. Новороссийская, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (234, '1611', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина - Энтузиастов, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (235, '1612', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина -ул.Свободы,до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (236, '1613', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат', 'пр.Ленина,30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (237, '1614', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,43-ул.Свободы, за перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (238, '1615', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,45-ул.Пушкина, за перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (239, '1616', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,48-ул.Пушкина, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (240, '1617', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,50-ул.Советская, до перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (241, '1618', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,51-ул.Советская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (242, '1619', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,52-ул.Цвиллинга, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (243, '1620', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,54-ул.Кирова, до перекр.из центра, у пиццерии"Помидор"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (244, '1621', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,55-ул.Воровского, до перекр.в центр, у Арбитражного суда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (245, '1622', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,61-ул.Васенко, до перекр.в центр, у "Сбербанк"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (246, '1623', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,63-ул.Красная, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (247, '1624', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,64-ул.Энгельса, до перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (248, '1625', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,64б-ул.Володарского, 32, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (249, '1626', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,73-ул.Энгельса, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (250, '1627', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина-ул.Красная, за перекр.из центра, у памятника "Орленок"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (251, '1628', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина,67-Свердловский пр., за перекр. в центр, авт.ост. "Алое поле"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (252, '1629', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Ленина-ул.Горького, у Комсомольской площади, из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (253, '1630', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'пр.Победы,160-А - ул.Болейко, за перекр. в центр, у маг. "Гавань"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (254, '1631', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.,27-пр.Победы, до перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (255, '1632', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.-пр.Победы, 192А, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (256, '1633', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.,65 - ул. С. Кривой, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (257, '1634', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр., 26-пр.Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (258, '1635', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.-ул.Калинина, у КПП Автомоб. инст., до перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (259, '1636', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.82-ул.К.Либкнехта, до перекр. из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (260, '1637', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.,71-ул.К.Либкнехта, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (261, '1638', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'Свердловский пр.,76-ул.С.Кривой,41 за перекр. из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (262, '1639', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Васенко,96-пр.Ленина, до перекр.в центр, у Дом быта');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (263, '1640', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Воровского,2-пр.Ленина, за перекр.в центр, у Арбитражного суда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (264, '1641', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Воровского,1-пр.Ленина,150м до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (265, '1642', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Воровского,9-ул.Красная, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (266, '1643', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Воровского,7-ул.Красная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (267, '1644', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Воровского,11-ул.Красная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (268, '1645', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Воровского-ул.С.Кривой, за перекр.из центра, до ост."Киномакс Урал" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (269, '1646', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул. Свободы,70 - пр.Ленина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (270, '1647', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Елькина -ул. Тимирязева, до перекрестка из центра ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (271, '1648', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Елькина-ул.Тимирязева, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (272, '1649', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,2-пр.Победы, за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (273, '1650', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,9а - ул.Калинина, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (274, '1652', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Коммуны, 70-ул.Красная, до перекр.из центра, у ФСБ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (275, '1653', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Красная,40-ул.С.Кривой, за перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (276, '1654', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Красная - ул.К.Либкнехта, за перекр. из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (277, '1655', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Красная,71-ул.С.Кривой,за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (278, '1656', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Красная, 11-ул.К.Маркса');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (279, '1657', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Пушкина,56-пр.Ленина, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (280, '1658', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Пушкина,59 - пр. Ленина, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (281, '1659', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Пушкина,65-ул.Тимирязева, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (282, '1660', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Пушкина,-пр.Ленина, 47 за перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (283, '1661', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат', 'ул.Пушкина,60');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (284, '1662', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.С.Кривой, 69А-ул.Энтузиастов, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (285, '1663', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Свободы 149-ул. Плеханова, за пер. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (286, '1664', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Свободы,151-ул.Плеханова, до перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (287, '1665', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Свободы,159-ул.Евтеева, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (288, '1666', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Свободы,78-ул.Тимирязева, за перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (289, '1667', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Свободы, 82-ул.Плеханова, до перекр. из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (290, '1668', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Советская,38-пр.Ленина, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (291, '1669', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Тимирязева,27-ул.Пушкина, за перекр. из центра ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (292, '1670', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Тимирязева,29-ул.Пушкина, до перекр.из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (293, '1671', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Тимирязева,31-ул.Цвиллинга, за перекр. из центра ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (294, '1672', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Тимирязева-ул. Елькина, 59, до перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (295, '1673', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Цвиллинга, 28 - ул. Коммуны, за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (296, '1674', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Цвиллинга-ул.Комунны, за перекр. из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (297, '1675', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Энгельса - пр.Ленина,71-А, до перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (298, '1676', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Энтузиастов,7-ул.С.Кривой, за перекр.в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (299, '1677', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Энтузиастов 12 - ул. С. Кривой, за перекр. из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (300, '1678', '12/18/2014', '10/24/2014', '10/23/2019', 'ООО"РФ"Армада Аутдор"', 'сити-формат',
        'ул.Цвиллинга 33 - пр. Ленина, до перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (301, '1680', '12/19/2014', '12/3/2014', '12/2/2019', 'Арбузин А.В.', 'щитовая установка',
        'Свердловский тр., 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (302, '1681', '12/19/2014', '11/29/2014', '11/28/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'пр. Победы/ул. Чичерина, 30к1, у поворота на трамвайное кольцо');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (303, '1682', '12/19/2014', '11/29/2014', '11/28/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'пересечение ул. Городская и ул. Олонецкая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (304, '1683', '12/19/2014', '11/29/2014', '11/28/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'Комарова пр., 54');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (305, '1684', '12/19/2014', '11/29/2014', '11/28/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'Троицкий тр., 500 м до виадука к посёлку Н-Синеглазово');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (306, '1685', '12/19/2014', '11/29/2014', '11/28/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'дорога в Аэропорт, у поста ГИБДД, у виадука, слева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (307, '1688', '12/19/2014', '12/3/2014', '12/2/2019', 'ООО"Карус"', 'сити-борд',
        'ул.Воровского, 21, 30 м после ост. "Горбольница" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (308, '1689', '12/19/2014', '11/29/2014', '11/28/2019', '  ООО «Челябинский масложировой комбинат»',
        'щитовая установка', 'Троицкий тр., напротив д. 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (309, '1690', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба', 'ул.Цвиллинга,44');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (310, '1691', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба', 'пр.Ленина,83');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (311, '1692', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба',
        'пересеч.пр.Ленина,71 и ул.Володарского,52а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (312, '1693', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба',
        'пересеч.пр.Ленина,30 и ул.3-го Интернационала');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (313, '1694', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба', 'пр.Победы,170');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (314, '1695', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба',
        'пересеч.ул.Воровского (из центра) и Свердловского пр. (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (315, '1696', '12/19/2014', '10/24/2014', '10/23/2019', 'ООО"Гэллэри Сервис"', 'Тумба',
        'пересеч.ул.Свободы (в центр) и ул.Тимирязева (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (316, '1700', '12/22/2014', '11/29/2014', '11/28/2019', 'ИП Грачева О.Г.', 'щитовая установка',
        'Троицкий тр.,46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (317, '1701', '12/22/2014', '11/29/2014', '11/28/2019', 'ИП Грачева О.Г.', 'щитовая установка',
        'пересечение ул.С.Юлаева и пр.Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (318, '1702', '12/22/2014', '11/29/2014', '11/28/2019', 'ИП Грачева О.Г.', 'щитовая установка',
        'Свердловский пр.,напротив д.62');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (319, '1703', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Ун. Набережная, напротив д. 185 по ул.Чайковского,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (320, '1704', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Новороссийская, напротив д. 25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (321, '1706', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.С. Юлаева, напротив д.29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (322, '1707', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Ун. набережная, напротив д. 185 по ул.Чайковского, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (323, '1708', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'пересечение ул.С.Юлаева и пр.Победы (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (324, '1709', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'Свердловский пр., напротив д.48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (325, '1710', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Курчатова,7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (326, '1711', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Ун. набережная, напротив д. 185 по ул.Чайковского, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (327, '1712', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Бр.Кашириных,89');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (328, '1713', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Молодогвардейцев,19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (329, '1714', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.С.Юлаева, напротив д. 34, на пересечении с ул.250 лет Челябинска ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (330, '1715', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'ул.Бр.Кашириных,напротив д.101');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (331, '1716', '12/24/2014', '12/16/2014', '12/15/2019', 'ООО"АТЛ КОМПАНИЯ"', 'щитовая установка',
        'пересечение ул. Университеская Набережная и ул. Чайковского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (332, '1717', '12/25/2014', '11/29/2014', '11/28/2019', ' ООО "Люкс Вода Инвест"', 'щитовая установка',
        'пересечение пр. Победы, 315 и ул. 40 лет Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (333, '1719', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'Флаги', 'ул.Черкасская,19,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (334, '1720', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'Флаги', 'ул.Черкасская,19,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (335, '1721', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'Флаги', 'ул.Черкасская,19,поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (336, '1722', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'Флаги', 'ул.Черкасская,19,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (337, '1723', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'Флаги', 'ул.Черкасская,19,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (338, '1724', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'Флаги', 'ул.Черкасская,19,поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (339, '1725', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'стела', 'ул.Черкасская,19,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (340, '1726', '12/26/2014', '9/1/2014', '8/31/2019', 'ООО"АМКапитал"', 'стела', 'ул.Черкасская,19,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (341, '1734', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"ЭЛИС ЛМ"', 'щитовая установка',
        'ул.Карла Либкнехта, 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (342, '1735', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"ЭЛИС ЛМ"', 'щитовая установка',
        'ул.Российская, 262');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (343, '1736', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"ЭЛИС ЛМ"', 'щитовая установка',
        'ул.Калинина, 17(середина дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (344, '1737', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'Западное шоссе (ул. Центральная,3в)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (345, '1738', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул.Доватора и ул. Федорова, возле АЗС Лукойл');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (346, '1739', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'ул.Блюхера, напротив д. 69');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (347, '1740', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'Гидрострой, 13а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (348, '1741', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение пр.Победы,398/1и и ул.Чичерина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (349, '1742', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул. Труда и ул.Свободы, напротив д. 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (350, '1743', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'ул.Центральная, напротив д. 3в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (351, '1744', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул. Кожзаводской и ул.Каслинской, напротив д. 5а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (352, '1745', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение Свердловского пр. и Комсомольского пр.,2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (353, '1746', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение пр. Победы,115 и ул. Горького,68/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (354, '1747', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Гагарина,23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (355, '1748', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Свободы,145');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (356, '1749', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул.Гостевой и ул. Центральная,1а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (357, '1750', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Худякова,13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (358, '1751', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'ул.Степана Разина,9/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (359, '1752', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Энгельса,32/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (360, '1753', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул. Чичерина и пр.Победы,325');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (361, '1754', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Горького,22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (362, '1755', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Труда,166');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (363, '1756', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Энтузиастов,32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (364, '1757', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'Комсомольский пр.,64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (365, '1759', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул.Энгельса,напротив д.58 и ул. Худякова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (366, '1760', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул. Артиллерийской,136 и ул.Ловина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (367, '1761', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Российская,159');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (368, '1762', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул.Коммуны,100 и ул. Энтузиастов');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (369, '1763', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул. Труда,153');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (370, '1764', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Энгельса,51');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (371, '1765', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул.Худякова,12а и ул.Верхнеуральской');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (372, '1766', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Блюхера,3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (373, '1767', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка', 'ул.Энгельса,69');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (374, '1768', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение ул.Краснознаменная,41/1 и ул.Братьев Кашириных');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (375, '1769', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение Комсомольского пр.,65 и ул.Солнечная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (376, '1770', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'пересечение пр.Победы,159а и ул. Кирова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (377, '1771', '12/26/2014', '11/29/2014', '11/28/2019', 'ЗАО"Элис"', 'щитовая установка',
        'Комсомольский пр., 55');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (378, '1783', '12/26/2014', '10/1/2014', '9/30/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Каслинская,64, Торговый центр, со стороны ул. Бр.Кашириных');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (379, '1784', '12/26/2014', '10/1/2014', '9/30/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Каслинская,64, Торговый центр, парковка магазина "Каслинский"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (380, '1786', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы/ Свердловский пр., 28А, трамв. ост. "пр. Победы", поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (381, '1787', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова, 10, трамв. ост. "ул. Калинина", поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (382, '1789', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова, 25А, трамв. ост. "Цирк", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (383, '1790', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова, 25А, трамв. ост. "Цирк", поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (384, '1791', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы, 168, трамв. ост. "Теплотехничесий институт", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (385, '1793', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова, 60А, трамв. ост. "Цирк", поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (386, '1794', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова, 60А, трамв. ост. "Цирк", поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (387, '1795', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Российская, 151, трамв. ост. "площадь Павших революционеров');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (388, '1796', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова,1, трамв.ост. "Теплотех", поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (389, '1797', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова,1, трамв.ост. "Теплотех", поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (390, '1798', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы, 188,трамв. ост. "пр. Победы", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (391, '1800', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы, 161,трамв. ост. "Теплотехнический институт", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (392, '1801', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы, 161,трамв. ост. "Теплотехнический институт", поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (393, '1802', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы, 289,трамв. ост. "ул. Молодогвардейцев"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (394, '1803', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы, 324,трамв. ост. "ул. Молодогвардейцев", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (395, '1805', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Кирова, 9,трамв. ост. "ул. Калинина", поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (396, '1807', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'пр. Победы/ ул. Чичерина, 30, трамв. ост. "Конечная", поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (397, '1809', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Горького,1Б, трамв. ост. "Комсомольская площадь"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (398, '1810', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'ул. Каслинская, 19, трамв. ост."ул. Островского", поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (399, '1814', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'Свердловский пр./ ул. Курчатова, 22, трамв. ост. "ул. Курчатова", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (400, '1816', '12/26/2014', '10/24/2014', '10/23/2019', 'ООО "Элит СТ 74"', 'Рекл. на ост.навесе',
        'Свердловский пр./ ул. Курчатова, 11А, трамв. ост. "ул. Курчатова", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (401, '1818', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка', 'ул. Комарова,78');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (402, '1819', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'автодорога Меридиан (ул. Приозерная, 4), поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (403, '1820', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Энтузиастов, 15Б и ул. Витебская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (404, '1821', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Калинина, 17 и ул. Каслинская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (405, '1822', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Цвиллинга, 66 и ул. Лазерная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (406, '1823', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Воровского и ул.Варненская, 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (407, '1824', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'Троицкий тр., 50, у Металлобазы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (408, '1825', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Сверловского пр. и ул. Коммуны, со стороны Алого поля');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (409, '1826', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Кирова, напротив д. 11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (410, '1827', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Воровского, 49');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (411, '1828', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Труда, 50 м до ул. Красная, палисадник музея');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (412, '1829', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Коммуны и ул. Володарского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (413, '1830', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Свердловского пр. (из центра) и ул. Труда (из центра), в палисаднике ДС "Юность"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (414, '1831', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Цвиллинга, 58 и ул. Евтеева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (415, '1832', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Блюхера, 65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (416, '1833', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Воровского, 64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (417, '1834', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'Троицкий тр.,21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (418, '1836', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'Уфимский тр., 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (419, '1837', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Новороссийская (в центр) и ул. Ереванская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (420, '1838', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'Троицкий тр., 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (421, '1839', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Свердловского пр.(в центр) и ул. Труда, с моста в центр первая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (422, '1840', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Труда (из центра), 135 м до ул. Северокрымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (423, '1841', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Новороссийская, 70 м от ул. Л.Чайкиной, в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (424, '1842', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Воровского, 57 и  ул. Доватора');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (425, '1843', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Блюхера и  ул. Дарвина, развязка у Мебельной фабрики');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (426, '1844', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Северо-Крымская, 150 м до моста через реку Миасс из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (427, '1845', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Воровского, 38');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (428, '1847', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Свердловского пр. (из центра) и ул. Труда, напротив АЗС "ЛУКОЙЛ"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (429, '1848', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка', 'Троицкий тр., 52В');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (430, '1849', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Блюхера, 95, 100 м до ул. Кузнецова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (431, '1850', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Свердловского пр., 16 и ул. Островского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (432, '1851', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Цвиллинга,62 и ул. Монакова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (433, '1852', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Блюхера (в центр), после троллейбусного кольца');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (434, '1853', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'Троицкий тр., напротив д. 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (435, '1854', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Цвиллинга,85, развязка у Привокзальной площади');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (436, '1855', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Блюхера, напротив Областной больницы,перед остановкой в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (437, '1858', '12/26/2014', '12/3/2014', '12/2/2019', 'ЗАО ТД "Бовид"', 'стела', 'пр.Ленина,28д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (438, '1859', '12/26/2014', '12/9/2014', '12/8/2024', 'ЗАО"ТД"БОВИД"', 'ЩУ арочного типа',
        'пр. Ленина (в центр), 26д, перед ж/д мостом');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (439, '1860', '12/26/2014', '12/9/2014', '12/8/2024', 'ЗАО"ТД"БОВИД"', 'ЩУ арочного типа',
        'пр.Ленина (в центр), 26/1, перед автомобильным мостом');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (440, '1861', '12/26/2014', '12/9/2014', '12/8/2024', 'ЗАО"ТД"БОВИД"', 'ЩУ арочного типа',
        'пр.Ленина (из центра), 28д, перед ж/д мостом');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (441, '1862', '12/26/2014', '12/9/2014', '12/8/2024', 'ЗАО"ТД"БОВИД"', 'ЩУ арочного типа',
        'пр.Ленина (из центра), 26/1, перед автомобильным мостом');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (442, '1863', '12/26/2014', '12/16/2014', '12/15/2019', 'ЗАО ТД "БОВИД"', 'щитовая установка',
        'а/д Меридиан, поворот на  реабилитационный центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (443, '1864', '12/26/2014', '12/16/2014', '12/15/2019', 'ЗАО ТД "БОВИД"', 'щитовая установка',
        'Троицкий тр., 1,3 км от переулка Озерный(в сторону п.Новосинеглазово)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (444, '1865', '12/26/2014', '12/16/2014', '12/15/2019', 'ЗАО ТД "БОВИД"', 'щитовая установка',
        'ул.Черкасская,поворот на ЧМЗ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (445, '1866', '12/26/2014', '12/16/2014', '12/15/2019', 'ЗАО ТД "БОВИД"', 'щитовая установка',
        'ул.Г.Танкограда,пересечение с Копейским переулком');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (446, '1867', '12/26/2014', '12/16/2014', '12/15/2019', 'ЗАО ТД "БОВИД"', 'щитовая установка',
        'пр.Ленина, 3,напротив проходной ЧТЗ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (447, '1868', '12/26/2014', '11/29/2014', '11/28/2019', ' ООО"Карус"', 'сити-борд', 'ул.Свободы,44');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (448, '1869', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'сити-борд',
        'пересечение ул. Воровского,21 и ул.Блюхера');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (449, '1870', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Карус"', 'сити-борд', 'пр. Ленина, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (450, '1871', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'ул. Краснознаменная, напротив д. 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (451, '1872', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'ул. Героев Танкограда, напротив д. 92');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (452, '1873', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'Копейское шоссе, 200 м от виадука, из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (453, '1874', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Блюхера (в центр) и ул. Корабельная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (454, '1875', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'пересечение пр.Победы,133 и ул.Кудрявцева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (455, '1876', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'пересечение автодороги Меридиан и Вагонного пер., 56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (456, '1877', '12/26/2014', '11/29/2014', '11/28/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'ул. Плеханова, 28 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (457, '1914', '12/26/2014', '12/3/2014', '12/2/2019', 'ИП Грачева О.Г.', 'транспарант-перетяжка',
        'пр. Победы, 166а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (458, '1915', '12/26/2014', '12/3/2014', '12/2/2019', 'ООО "СВ-ТУР"', 'щитовая установка', 'пр. Комарова, 42');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (459, '1921', '12/26/2014', '12/3/2014', '12/2/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'ул. Островского, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (460, '1922', '12/26/2014', '12/3/2014', '12/2/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'дорога из Аэропорта (в город), до поста ГИБДД, у виадука, справа');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (461, '1923', '12/26/2014', '12/3/2014', '12/2/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'пересечение ул. Рылеева, 16 и ул. Клиническая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (462, '1925', '12/26/2014', '12/3/2014', '12/2/2019', 'ООО"Карус"', 'сити-борд', 'пр.Ленина,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (463, '1926', '12/26/2014', '12/3/2014', '12/2/2019', 'ООО"Карус"', 'сити-борд', 'ул.Воровского,4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (464, '1927', '12/26/2014', '12/3/2014', '12/2/2024', 'ООО"РА"Гранат"', '96 кв., суперсайт', ' ул.Кирова, 25А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (465, '1928', '12/26/2014', '12/3/2014', '12/2/2019', 'ИП Смирнова Т.С.', 'щитовая установка',
        'ул. Хлебозаводская, напротив д. 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (466, '1929', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО "ДС Авто"', 'стела', 'ул.Бр. Кашириных, 135,к.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (467, '1930', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО "СЕЙХО Моторс СПОРТ"', 'Флаги',
        'ул.Бр. Кашириных, 135,к.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (468, '1931', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО "СЕЙХО Моторс СПОРТ"', 'стела',
        'ул.Бр. Кашириных, 135,к.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (469, '1932', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО "СЕЙХО Моторс СПОРТ"', 'стела',
        'ул.Бр. Кашириных, 135,к.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (470, '1933', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е стр.1, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (471, '1934', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е стр.1, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (472, '1935', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е стр.1, поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (473, '1936', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е стр.1, поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (474, '1937', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е стр.1, поз.5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (475, '1938', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (476, '1939', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (477, '1940', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (478, '1941', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (479, '1942', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (480, '1943', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (481, '1944', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр.2, поз.7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (482, '1971', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Родонит Альп"', 'стела',
        'ул.Энтузиастов,напротив д.40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (483, '1972', '12/26/2014', '12/9/2014', '12/8/2019', 'ОАО"ЧЗПСН-Профнастил"', 'щитовая установка',
        'пересечение ул. Героев Танкограда (из центра) и ул. Валдайская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (484, '1996', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Азбука"', 'щитовая установка',
        'пересечение ул.Бр. Кашириных (из центра) и ул. Салавата Юлаева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (485, '2000', '12/26/2014', '12/16/2014', '12/15/2019', 'ООО "Экстрим-клуб Челябинск"', 'стела',
        'Свердловский тр.,1ж');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (486, '2001', '12/26/2014', '12/16/2014', '12/15/2019', 'ООО "Экстрим-клуб Челябинск"', 'стела',
        'Свердловский тр.,1жк2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (487, '2003', '12/26/2014', '12/16/2014', '12/15/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр. 2, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (488, '2004', '12/26/2014', '12/16/2014', '12/15/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела',
        'пр. Ленина, 3е, стр. 2, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (489, '2005', '12/26/2014', '12/16/2014', '12/15/2019', 'ООО"СЕЙХО-МОТОРС"', 'стела', 'пр.Ленина,д.3е, стр.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (490, '2006', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'пр. Победы, 382б,130 м  до  ул. 40 лет Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (491, '2007', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'Комарова пр., 87');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (492, '2008', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'Калинина, ул, 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (493, '2009', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус Сервис"', 'щитовая установка',
        'Троицкий тр.(в город), 1200 м до виадука к пос. Н-Синеглазово');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (494, '2010', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Коммуны, 80');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (495, '2011', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Героев Танкограда, 65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (496, '2012', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Бр.Кашириных,12 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (497, '2013', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение пр. Победы (в центр) и ул. Российская, 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (498, '2014', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка', 'Свердловский пр., 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (499, '2015', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 52, начало дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (500, '2016', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пр. Победы, 163, у магазина "Перекресток"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (501, '2017', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Худякова, 1 950 м до ул. Лесопарковая, в центр ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (502, '2018', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. ГероеТанкограда, 65 - ул.Крылова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (503, '2019', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Свердловскго тр. и ул. Радонежская, у АЗС, напротив трамв. депо');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (504, '2020', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение автодороги Меридиан и ул.Артиллерийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (505, '2021', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 29, у завода "Прибор"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (506, '2022', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка', 'пр. Победы, 160-А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (507, '2023', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Худякова, 1 610 м до ул.Лесопарковая, в центр справа');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (508, '2024', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных  и ул. Чайковского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (509, '2025', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Гер.Танкограда, 220 м от ул. Ловина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (510, '2026', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 61, середина дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (511, '2027', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Свердловского тр. и  ул. Радонежская, со стороны Авторынка');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (512, '2028', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение пр. Победы и  ул. Ворошилова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (513, '2029', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Худякова, 1 060 м до ул. Лесопарковая, в центр справа');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (514, '2030', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Бр.Кашириных,,12, 120 м до ул.Кирова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (515, '2031', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'автодорога Меридиан (в центр), 300 м до пересечения с ул. Новороссийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (516, '2032', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Героев Танкограда и ул. 1 - й Пятилетки, у Сада Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (517, '2033', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 52, магазин "Олимп"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (518, '2034', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Худякова, 780 м до ул. Лесопарковая, в центр справа ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (519, '2035', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных (в центр) и   ул. 40 лет Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (520, '2036', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'Свердловский тр.(в центр), ост. "Ветлечебница", справа в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (521, '2037', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'автодорога Меридиан,80 м от ул. Новороссийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (522, '2038', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение пр. Победы пр.(в центр) и  ул. Горького');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (523, '2039', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 24, напротив завода "Прибор"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (524, '2040', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'автодорога Меридиан, 75 м до ул. Новороссийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (525, '2041', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Худякова,16А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (526, '2042', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Героев Танкограда (из центра) и ул. Механическая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (527, '2043', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Бр. Кашириных, 116');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (528, '2044', '12/26/2014', '12/9/2014', '12/8/2019', 'ООО"Карус"', 'щитовая установка',
        'выезд на автодорогу Меридиан с пр. Ленина (под мостом из центра слева)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (529, '2046', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 83,  конец дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (530, '2047', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Бр.Кашириных,130 м до ул.Северо-Крымская, из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (531, '2048', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Дзержинского и ул.Коммунаров,62');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (532, '2049', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пр. Победы, 170, у магазина "Ровесник"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (533, '2050', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Бр. Кашириных, 118');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (534, '2051', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка', 'пр. Победы, 158');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (535, '2052', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Свердловский тр., развязка в районе Ветлечебницы, 320 м от ул.Радонежской, справа в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (536, '2053', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Бр. Кашириных, 120 м до ул. Чичерина, из центра справа');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (537, '2054', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Свободыи  ул. Коммуны');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (538, '2055', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'выезд на автодорогу Меридиан с пр.Ленина под мостом, справа');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (539, '2056', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Плеханова, 29 и ул. Свободы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (540, '2057', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 50, напротив гост. "Виктория"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (541, '2058', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Бр. Кашириных, 106');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (542, '2059', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Победы пр., 170, у Теплотехнического института');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (543, '2060', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Свердловский пр., 41 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (544, '2061', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Комсомольский пр., 83, начало дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (545, '2062', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.1 - й Пятилетки и ул. Артиллерийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (546, '2063', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка', 'ул.Свободы , 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (547, '2064', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка', 'пр.Победы, 194');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (548, '2065', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Труда и Свердловского пр.,на кольце напротив Дворца спорта "Юность"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (549, '2066', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка', 'ул.Доватора,33');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (550, '2067', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение пр.Победы,202 и ул.Краснознаменной');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (551, '2068', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Дзержинского, 126А и  ул. Барбюса');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (552, '2069', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул. Ст.Разина,6, у гипермаркета "М-Видео"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (553, '2070', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Артиллерийская ул., 70м до пр.Ленина от ул.Ловина, слева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (554, '2071', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение пр.Победы и ул. Косарева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (555, '2072', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка', 'ул. Свободы, 82');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (556, '2074', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'Свердловский тр.,14б, 65 м от поворота на рынок "Северный двор"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (557, '2075', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Бр. Кашириных, 114');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (558, '2076', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'ул.Свободы, из центра, 100 м до ул. Труда, со стороны автостоянки');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (559, '2077', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Артиллерийская и ул.Ловина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (560, '2078', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение Комсомольского пр., 2 и Свердловского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (561, '2079', '12/29/2014', '12/16/2014', '12/15/2019', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул.Доватора,4 и ул.Степана Разина, у виадука в Ленинский район,напротив завода им.Колющенко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (562, '2080', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Победы, 293, начало дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (563, '2081', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского тр. (в центр) и ул. Черкасская, выезд с ЧМЗ, слева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (564, '2082', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Комарова, 110 и ул. Шуменская, за ост. "ул. Кулибина"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (565, '2083', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных, 129 и ул. Солнечная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (566, '2084', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Комарова, 110 (середина дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (567, '2085', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Победы, 293 (середина дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (568, '2086', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Кирова, 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (569, '2087', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Свободы, 90');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (570, '2088', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Черкасская/ шоссе Металлургов, 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (571, '2089', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Комарова (в центр), 200 м от пересечения с ул. Бажова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (572, '2090', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Свободы, 161');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (573, '2091', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Победы, 154');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (574, '2092', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Гагарина, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (575, '2093', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Елькина, 82');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (576, '2094', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Героев Танкограда (в центр) и ул. Ловина, у театра ЧТЗ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (577, '2095', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Северо-Крымская и ул. Труда (из центра), за ост. "Родничок"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (578, '2096', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Героев Танкограда (в центр) и ул. Механическая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (579, '2097', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (580, '2098', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Горького, 30 и ул. Котина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (581, '2099', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Героев Танкограда, 104');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (582, '2100', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Уфимский тр. (в город), 100 м до ул. Новоэлеваторная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (583, '2101', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Победы, 159');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (584, '2102', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Елькина, 63б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (585, '2103', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Краснознаменная, 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (586, '2104', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Комсомольский пр., 10/6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (587, '2105', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Чичерина,32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (588, '2106', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Блюхера (в центр) и ул. Нефтебазовая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (589, '2107', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пл. Мопра, 9 и ул. Российская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (590, '2108', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Бродокалмакский тр. (из центра), ост. "Сад Дружба"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (591, '2109', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Энгельса, 81');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (592, '2110', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'дорога в аэропорт, Бродокалмакский тр. (из центра) , за поворотом на ТЭЦ-3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (593, '2111', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        ' пр. Победы, 141');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (594, '2114', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Победы, 289, 150 м до ул. Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (595, '2115', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Кирова, 2 (конец дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (596, '2116', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Российская/ ул.Свободы, 169');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (597, '2118', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО "РФ Армада Аутдор"', 'сити-борд',
        'пересечение ул.Красная, 65 и пр.Ленина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (598, '2119', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО "РФ Армада Аутдор"', 'сити-борд',
        'пересечение пр.Ленина,26 и ул.Артиллерийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (599, '2120', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО "РФ Армада Аутдор"', 'сити-борд',
        'пересечение пр. Ленина (из центра) и автодороги Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (600, '2121', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО "РФ Армада Аутдор"', 'сити-борд', 'пр. Ленина,19А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (601, '2122', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО "РФ Армада Аутдор"', 'сити-борд',
        'пересечение пр. Ленина,7 и ул. Героев Танкограда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (602, '2123', '12/29/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'сити-борд',
        'пересечение пр.Ленина и ул.Советская,38');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (603, '2124', '12/29/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'сити-борд',
        'пересечение пр. Ленина и ул. Российская, 200');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (604, '2125', '12/29/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'сити-борд',
        'ул. Воровского, 53');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (605, '2126', '12/29/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'сити-борд',
        'пересечение пр. Ленина, 35 и ул. Российская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (606, '2129', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., 12Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (607, '2130', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Комарова и ул. Салютная, за остановкой в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (608, '2131', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы, 146 и ул. Кудрявцева, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (609, '2132', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Воровского и ул. Тимирязева, за перекрестком в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (610, '2133', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Комарова, 131 и ул. Шуменская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (611, '2134', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 62');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (612, '2135', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского пр. и ул. Воровского, со стороны Горбольницы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (613, '2136', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Гагарина, 31 и ул. Дзержинского, 96');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (614, '2137', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы,384А и ул. 40 лет Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (615, '2138', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Энгельса и ул. Труда, 177/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (616, '2139', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Цвиллинга, 77');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (617, '2140', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Лесопарковая, 9А и ул. Витебская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (618, '2141', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Курчатова, напротив д. 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (619, '2142', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский пр., напротив д. 54');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (620, '2143', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечениеи пр. Победы, 142 и ул. Артиллерийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (621, '2144', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Черкасская, 2г');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (622, '2145', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., 16Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (623, '2146', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 38А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (624, '2147', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Худякова, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (625, '2148', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского пр. и ул. Бр. Кашириных, за мостом из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (626, '2149', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Воровского, 73 и ул. Тарасова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (627, '2150', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Троицкий тр., 72Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (628, '2151', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы и Свердловского пр., 25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (629, '2152', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Свободы, 161');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (630, '2153', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Дзержинского, 91 и ул. Гагарина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (631, '2154', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Блюхера, 44 и ул. Рылеева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (632, '2155', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Героев Танкограда и ул. Первой Пятилетки');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (633, '2156', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Троицкий тр., напротив д. 42');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (634, '2157', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского тр. и ул. Автодорожная (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (635, '2158', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Новороссийская и автодороги Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (636, '2159', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Горького, 12 и ул. Савина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (637, '2160', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Молодогвардейцев, 57/к1 и ул. Бр. Кашириных');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (638, '2161', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Комсомольского пр., 10/1 и ул. Краснознаменная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (639, '2162', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., автомобильный рынок');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (640, '2163', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Троицкий тр., 52Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (641, '2164', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО "РФ "Армада Аутдор"', 'сити-борд', 'пр.Ленина, 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (642, '2165', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО "РФ "Армада Аутдор"', 'сити-борд',
        'пересечение ул.Кирова (в центр) и ул.Труда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (643, '2166', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО "РФ "Армада Аутдор"', 'сити-борд',
        'пересечение ул. Рождественского и пр. Ленина, 14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (644, '2167', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО "РФ "Армада Аутдор"', 'сити-борд',
        'ул.Свободы,139 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (645, '2168', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО "РФ "Армада Аутдор"', 'сити-борд',
        'ул.Российская, 249');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (646, '2169', '12/29/2014', '11/29/2014', '11/28/2019', 'ООО "РФ "Армада Аутдор"', 'сити-борд',
        'ул.Воровского, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (647, '2170', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Блюхера, 100 м до остановки "АМЗ" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (648, '2171', '12/29/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы, 151 и ул. Болейко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (649, '2172', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 123/2 (1868 км + 130 м)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (650, '2173', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 72а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (651, '2174', '12/30/2014', '12/20/2014', '12/19/2019', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Чичерина, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (652, '2176', '12/30/2014', '12/20/2014', '12/19/2019', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Бейвеля/ ул. Скульптора Головницкого, 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (653, '2177', '12/30/2014', '12/20/2014', '12/19/2019', 'ИП Трифонов Э.С.', 'щитовая установка',
        ' ул. Университетская Набережная / ул. Бр. Кашириных, 85Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (654, '2178', '12/30/2014', '12/20/2014', '12/19/2019', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Татищева, напротив д.262');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (655, '2179', '12/30/2014', '12/20/2014', '12/19/2019', 'ИП Трифонов Э.С.', 'щитовая установка',
        'пересечение ул. Бейвеля  (в центр)и ул. Хариса Юсупова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (656, '2181', '12/30/2014', '12/9/2014', '12/8/2019', 'ООО"РФ"Армада Аутдор"', 'сити-борд', 'пр.Ленина,60');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (657, '2182', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Центральная, 1550 м до поста ГИБДД, в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (658, '2183', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Артиллерийская, 107');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (659, '2184', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 20/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (660, '2185', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Артиллерийская, 136, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (661, '2186', '12/30/2014', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 23б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (662, '2187', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Плеханова, 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (663, '2188', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Российская и ул. Нагорная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (664, '2189', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Степана Разина, 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (665, '2190', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Калинина и Свердловского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (666, '2191', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Молодогвардейцев, 66');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (667, '2192', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский тр. (в центр), 400 м от ул. Радонежской');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (668, '2193', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. 250 лет Челябинску, 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (669, '2194', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'пр. Победы, 290');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (670, '2195', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение автодороги Меридиан (в центр) и ул. Артема, 151');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (671, '2196', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Новороссийская, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (672, '2197', '12/30/2014', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. 250 лет Челябинску, 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (673, '2198', '12/31/2014', '12/9/2014', '12/8/2024', 'ООО"Карус"', '120, суперсайт',
        'пересечение ул.Героев Танкограда и пр. Победы, со стороны парка');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (674, '2200', '12/31/2014', '12/9/2014', '12/8/2024', 'ООО"Карус"', '75, суперсайт',
        'пересечение пр. Победы (из центра) и ул. Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (675, '2201', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Героев Танкограда, 23, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (676, '2202', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Бажова (из центра) и ул. Комарова, до пересечения');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (677, '2203', '12/31/2014', '12/9/2014', '12/8/2019', 'ООО"РФ"Армада Аутдор"', 'сити-борд',
        'пересечение пр.Ленина,50 и ул.Пушкина,56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (678, '2204', '12/31/2014', '12/9/2014', '12/8/2019', 'ООО"РФ"Армада Аутдор"', 'сити-борд',
        'пересечение пр.Ленина,29 и ул.3-го Интернационала');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (679, '2205', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Бр.Кашириных и ул. Ворошилова,57А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (680, '2206', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Салавата Юлаева, 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (681, '2207', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Комсомольский пр., 66/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (682, '2208', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Курчатова, напротив д. 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (683, '2209', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Комарова, 135');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (684, '2210', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'шоссе Металлургов, 3г');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (685, '2211', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Троицкого тр. и ул. Рылеева, за перекрестком в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (686, '2212', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Дзержинского, 110');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (687, '2213', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Героев Танкограда и ул. Потемкина, у Никольской рощи');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (688, '2214', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Салавата Юлаева, 17в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (689, '2215', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Комарова и ул. Завалишина, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (690, '2216', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. 1-я Окружная и ул. Доватора, до перекрестка из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (691, '2217', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Шоссе Металлургов, 9 и ул. Дегтярева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (692, '2218', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Курчатова/ ул. Воровского, 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (693, '2219', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 60в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (694, '2220', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Героев Танкограда, 55 и ул. Котина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (695, '2221', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Комарова, 131(середина дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (696, '2222', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Блюхера, за остановкой "АМЗ" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (697, '2223', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул.Братьев Кашириных, 102 (начало дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (698, '2224', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечние ул. Свободы, 88 и ул. Орджоникидзе');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (699, '2225', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение пр. Победы и ул. Героев Танкограда, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (700, '2226', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (701, '2227', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Российская, 251 и ул. Тимирязева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (702, '2228', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Блюхера, 61');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (703, '2229', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Доватора, 17 и ул. Шаумяна');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (704, '2230', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Российская и пл. Мопра,9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (705, '2231', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Кирова, 17А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (706, '2232', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пр. Победы, 200/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (707, '2233', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Свободы, 102');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (708, '2234', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Энгельса, 32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (709, '2235', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Харлова,9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (710, '2236', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., 300 м до ост. "Ветлечебница" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (711, '2237', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Комарова, 112 (начало дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (712, '2238', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных, 107 и ул. Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (713, '2239', '12/31/2014', '12/3/2014', '12/2/2019', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Блюхера/ ул. Курчатова, 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (714, '2240', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского тр. и Северного луча, у моста');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (715, '2241', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Воровского, 71');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (716, '2242', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр. (в центр),150 м за пересечением ул. Индивидуальная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (717, '2243', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Троицкого тр., 38 и ул. Шарова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (718, '2245', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Российская, 40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (719, '2246', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Блюхера, 13 и ул. Тарасова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (720, '2247', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского тр. и ул. Автодорожная, за ост. "Цинковый завод"(в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (721, '2248', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Российская, 277');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (722, '2250', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Троицкий тр., 11а/4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (723, '2251', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., 350 м от поста ГИБДД, в центр ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (724, '2252', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Героев Танкограда, 40 и пр. Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (725, '2254', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Бр. Кашириных, 89 (конец дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (726, '2256', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Елькина, 81А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (727, '2257', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., 400 м до поста ГИБДД, из центра ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (728, '2258', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Горького, 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (729, '2260', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'ул. Молодогвардейцев, 47а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (730, '2261', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение Свердловского тр.(из центра) и ул. Индивидуальная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (731, '2262', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Машиностроителей, 34 и ул. Масленникова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (732, '2263', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'Свердловский тр. (из центра), 100 м до пересечения с ул. Рабочекрестьянская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (733, '2264', '12/31/2014', '12/3/2014', '12/2/2024', 'ООО"РФ"Армада Аутдор"', 'щитовая установка',
        'пересечение ул. Машиностроителей, 36 и ул. Масленникова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (734, '2266', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул.Российская, 159 и ул. Труда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (735, '2267', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул.Воровского, со стороны главного корпуса Мед. Академии, ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (736, '2268', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'ул. Чичерина, 35');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (737, '2269', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'Троицкий тр., 72б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (738, '2270', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. 50 лет ВЛКСМ и шоссе Металлургов, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (739, '2271', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Бр. Кашириных, 103');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (740, '2272', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Российская, 226');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (741, '2273', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Блюхера,конечн. ост." АМЗ" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (742, '2274', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'ул. Горького, 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (743, '2275', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'Свердловский тр., 240 м от поворота на ЧВВАКУШ, справа в город');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (744, '2276', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'ул. Энгельса, 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (745, '2277', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Молодогвардейцев, 48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (746, '2278', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пр. Ленина, 2, у главной проходной ЧТЗ , поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (747, '2279', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Бр. Кашириных (в центр), 300 м до ул.Молодогвардейцев, ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (748, '2280', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'шоссе Металлургов (из центра), 50 м до пересечения с ул. Сталеваров');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (749, '2281', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Труда (из центра), 200 м до ул. Северо-Крымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (750, '2282', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'ул. Салютная, 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (751, '2283', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Молодогвардейцев, 53');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (752, '2284', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Евтеева и ул. Цвиллинга, 59а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (753, '2285', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Молодогвардейцев, 68');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (754, '2286', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пр. Ленина, 2, у главной проходной ЧТЗ , поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (755, '2287', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'Троицкий тр., 23,   у "Сельхозтехники",');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (756, '2288', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Бр. Кашириных, 99А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (757, '2289', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Энтузиастов, 1 и пр. Ленина, 68');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (758, '2290', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'ул. Черкасская,2б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (759, '2291', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Механической и ул. Героев Танкограда ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (760, '2292', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'развязка Свердловского пр. и ул. Труда, трамв. ост. "ДС Юность"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (761, '2293', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'ул. Бр. Кашириных, напротив д. 91А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (762, '2294', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Молодогвардейцев (из центра) и Комсомольского пр., до перекрестка');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (763, '2295', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'пр. Комарова, 125');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (764, '2296', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'ул. Черкасская, 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (765, '2297', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка', 'Троицкий тр., 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (766, '2298', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Блюхера (из центра) и ул. Новоэлеваторная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (767, '2299', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'Свердловский тр.,  250 м от поворота на ЧВВАКУШ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (768, '2300', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО "Карус"', 'щитовая установка',
        'пересечение шоссе Металлургов и ул. Строительная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (769, '2301', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр.,23 Д и ул.Косарева, за перекр. в центр, авт.ост. "ул.Косарева"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (770, '2302', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение пр.Ленина,74 и ул.Энтузиастов, за перекр. из центра, тролл.ост. "Инст. Гражданпроект"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (771, '2305', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр.и ул.Ворошилова, за перекр.в центр, авт.ост."ул.Ворошилова"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (772, '2306', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение пр.Ленина,19 и ул.Артиллерийская, за перекр. из центра, авт.ост."к-т Спартак" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (773, '2307', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Энгельса и ул.Труда, до перекр.из центра, тролл. ост."ул.Труда", ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (774, '2308', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Г.Танкограда и пр.Ленина, за перекр.из центра, авт.ост. "Театр ЧТЗ", поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (775, '2309', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр.,22 и ул.Чайковского, за перекр.из центра, авт.ост."ул.Чайковского"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (776, '2310', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)', 'ул.Свободы,173, тролл.ост."Ж.Д. Институт"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (777, '2311', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Труда и Свердловского пр.,  в центр, трам.ост."ДС Юность"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (778, '2312', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Цвиллинга и ул.Труда, за перекр.в центр, ост.трам "Оперный театр"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (779, '2313', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)', 'Комсомольский пр., 69, в центр, ост. "Солнечная"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (780, '2314', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)', 'Комсомольский пр.,41, в центр, авт.ост. "Поликлиника"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (781, '2316', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пр.Победы,168, авт.ост. "Теплотехнический институт", в сторону Свердловского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (782, '2317', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Худякова,6 и ул.Энгельса, за перекр.из центра, авт. ост. "ул.Худякова"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (783, '2318', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение пр.Ленина и Свердловского пр., за перекр.из центра, ост."Алое поле"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (784, '2319', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр. и ул.Чайковского, до перекр.в центр, авт.ост."ул.Чайковского", поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (785, '2320', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Г.Танкограда и пр.Ленина, за перекр.из центра, авт.ост. "Театр ЧТЗ", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (786, '2321', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение пр.Ленина,76 и ул.Лесопарковая, 200 м до перекр. из центра, авт. ост. "ЮрГУ", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (787, '2322', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)', 'ул.Свободы,108');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (788, '2323', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Свердловского пр.,16 и ул.Островского, за перекр.в центр, ост.автобуса "ул.Островского", в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (789, '2324', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр.,14 и ул.Косарева, за перекр. из центра, авт.ост. "ул.Косарева"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (790, '2325', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Цвиллинга,11 и ул.Труда, до перекр.из центра, трам. ост. "Оперный театр"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (791, '2326', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Блюхера,44 и ул.Рылеева, за перекр.из центра, тролл.ост. "ул.Рылеева"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (792, '2327', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение пр.Ленина,8а и ул.Г.Танкограда, до перекр. в центр, трам.ост."Театр ЧТЗ"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (793, '2328', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр. и ул.Ворошилова, за перекр.из центра, авт.ост."ул.Ворошилова"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (794, '2329', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Кирова и ул.Калинина, за перекр.в центр, авт.ост "ул. Калинина" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (795, '2330', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Тимирязева,64 и ул.Пушкина, за перекр.в центр, тролл.ост. "Кинотеатр Пушкина"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (796, '2331', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение пр.Ленина,66А и ул.Энгельса, за перекр.из центра, авт.ост. "ул. Энгельса" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (797, '2332', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр.,35 и ул.Пионерская, за перекр.в центр, авт.ост. "ул.Пионерская"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (798, '2333', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр. и ул.Чайковского, до перекр.в центр, авт.ост."ул.Чайковского", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (799, '2334', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Свердловского пр. и ул.Калинина,34, за перекр.из центра, авт.ост "Автомобильное Училище" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (800, '2335', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'ул.Г.Танкограда-пр.Ленина, за перекр.из центра, тролл.ост. "Театр ЧТЗ", поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (801, '2336', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение Комсомольского пр.,65 - ул.Солнечная, ост."ул.Солнечная", за перекр. в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (802, '2337', '12/31/2014', '12/16/2014', '12/15/2019', 'ООО"РФ"Армада Аутдор"',
        'сити-формат (в составе ост.комп.)',
        'пересечение ул.Цвиллинга и ул.Плеханова, до перекр.из центра, трам.ост."Городской парк" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (803, '1', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Труда, 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (804, '2', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пр. Победы, напротив д. 305б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (805, '3', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 32а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (806, '4', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Черкасская, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (807, '5', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Комсомольский пр., 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (808, '6', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (809, '7', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, напротив д. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (810, '8', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Свободы, 92');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (811, '9', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (812, '10', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Энгельса, 103');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (813, '11', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (814, '12', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение пр. Победы, 167 и ул. Каслинская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (815, '13', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 103');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (816, '14', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Богдана Хмельницкого, 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (817, '15', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Хлебозаводская, 250 м от автодороги Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (818, '16', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Дарвина, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (819, '17', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Российская, 61а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (820, '18', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение пр. Победы и ул. Бажова, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (821, '19', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Гражданская, 14а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (822, '20', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр., напротив д. 86');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (823, '22', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (824, '23', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Российская, между д. 196 и д. 198');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (825, '24', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Молодогвардейцев, 48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (826, '25', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., напротив д. 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (827, '26', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Профинтерна/ ул. Салтыкова, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (828, '27', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Краснознаменная, 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (829, '28', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Артиллерийская, 117/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (830, '29', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Ловина/ пр. Ленина, 26а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (831, '30', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Механическая и ул. Артиллерийская, 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (832, '31', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 83П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (833, '32', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Копейское шоссе, 1п/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (834, '33', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 84');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (835, '34', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Чичерина/ ул. Бр. Кашириных, 133');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (836, '35', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Елькина, 85, автостоянка');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (837, '39', '1/30/2015', '12/9/2014', '12/8/2019', 'ООО "Стрит-Медиа"', 'щитовая установка',
        'пересечение автодороги Меридиан и 7-го Целинного переулка');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (838, '40', '1/30/2015', '8/1/2014', '7/31/2024', 'ООО"ИнвестПроект"', 'ЩУ арочного типа', 'Троицкий тр.,11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (839, '41', '1/30/2015', '12/3/2014', '12/2/2019', 'ОАО"Фортум"', 'стела', 'Бродокалмакский тр., 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (840, '42', '2/3/2015', '12/9/2014', '12/8/2019', 'ИП Петровский Н.А.', 'щитовая установка',
        'пересечение ул. Новороссийская и ул. Дербенская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (841, '48', '2/17/2015', '6/1/2014', '5/31/2019', 'ООО"ШИНИНВЕСТ"', 'стела', 'Свердловский тр., 12б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (842, '49', '2/17/2015', '12/3/2014', '12/2/2019', 'ООО"Элис ЛМ"', 'щитовая установка', 'ул. Кирова, 2а/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (843, '50', '2/17/2015', '12/3/2014', '12/2/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'ул. Героев Танкограда, 23, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (844, '51', '2/17/2015', '12/3/2014', '12/2/2019', 'ООО"Элис ЛМ"', 'щитовая установка', 'пр. Победы, 155А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (845, '52', '2/17/2015', '12/3/2014', '12/2/2019', 'ООО"Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Чайковского и пр. Победы, за перекрестком в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (846, '53', '2/17/2015', '12/3/2014', '12/2/2019', 'ООО"Элис ЛМ"', 'щитовая установка', 'ул. Энтузиастов,17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (847, '54', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский пр. (в центр), 40 м перед мостом через реку Миасс ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (848, '55', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение Троицкого тр., 9/2и ул. Дарвина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (849, '56', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Черкасская, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (850, '57', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Линейная, 51а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (851, '58', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'развязка Троицкого тр. и автодороги Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (852, '59', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Уфимский тр./ ул. Новоэлеваторная, 51/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (853, '60', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'Троицкий тр., 11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (854, '61', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр., 74');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (855, '62', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Героев Танкограда/ул. 40 лет Октября, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (856, '63', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Российская, 179');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (857, '64', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Комарова, 68');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (858, '65', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Героев Танкограда, 42');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (859, '66', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Танкистов, напротив д. 144');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (860, '67', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Горького, 17 и ул. Первой Пятилетки');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (861, '68', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Краснознаменная, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (862, '69', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр., 33, 170 м до ул. Красного урала');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (863, '70', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'пр. Победы, 323');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (864, '71', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский тр., напротив д.7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (865, '72', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Первой Пятилетки, напротив д. 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (866, '73', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Бр. Кашириных/ Северокрымский 1-й пер., 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (867, '74', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр.,10/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (868, '75', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский пр., 22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (869, '76', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'автодорога Меридиан/ ул. Ш. Руставелли, напротив д. 32/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (870, '77', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Бр. Кашириных, 134б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (871, '79', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение Копейского шоссе и ул. Гагарина, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (872, '78', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Доватора, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (873, '80', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Уфимский тр./ ул. Блюхера, напротив д. 111');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (874, '81', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Бродокалмакский тр., 3/1, выезд');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (875, '82', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пр. Победы/ ул. Чайковского, напротив д. 89');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (876, '83', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Хлебозаводская, 7/2 и ул. Автоматики');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (877, '84', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Салавата Юлаева и ул. 250 лет Челябинску, 25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (878, '85', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Троицкий тр., 52, поворот на Металлобазу');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (879, '86', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение пр. Победы, 305е и ул. Молдавская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (880, '87', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Энгельса, 49');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (881, '88', '2/17/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский пр., напротив д. 51, развязка у ДС "Юность"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (882, '89', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., ост. "Исаково"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (883, '90', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (884, '91', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан, 1/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (885, '93', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Механическая, 40а и ул. Артиллерийская (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (886, '94', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Краснознаменная, напротив д. 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (887, '95', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение автодороги Меридиан и ул. Ш. Руставелли, 32/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (888, '96', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Северный луч/ ул. Героев Танкограда, 42П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (889, '97', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан, 100 м до пересечения с ул. Хлебозаводская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (890, '99', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Хлебозаводская, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (891, '100', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Бродокалмакский тр., 1/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (892, '101', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Механическая/ул. Героев Танкограда, 80П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (893, '102', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Танкистов/ул. Кулибина,52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (894, '103', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Профинтерна/ ул. Табачная, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (895, '104', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', 'щитовая установка',
        'Шершневское водохранилище, пост ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (896, '105', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', '75, суперсайт',
        'пересечение ул. Бр. Кашириных,напротив д. 95А и ул. Северокрымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (897, '106', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Худякова, напротив д. 22к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (898, '107', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', 'щитовая установка',
        'Копейское шоссе/ ул. Харлова, 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (899, '108', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', '105 суперсайт',
        'пересечение ул. Труда, 67 и  ул. Красноармейская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (900, '109', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр./ ул. Индивидуальная,1 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (901, '110', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', '75, суперсайт',
        'пересечение ул. Бр. Кашириных (из центра) и ул. Северокрымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (902, '111', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Чичерина, 22/5 и Комсомольского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (903, '112', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', '72, суперсайт',
        'Бродокалмакский тр., 50 м от поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (904, '113', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', '75, суперсайт',
        'ул. Героев Танкоград,28П, поворот на Северный луч');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (905, '114', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', '75 супер',
        'Копейское шоссе/ ул. Харлова, 1/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (906, '115', '2/17/2015', '12/16/2014', '12/15/2024', 'ЗАО "Элис"', 'щитовая установка', 'Свердовский тр., 7/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (907, '116', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        ' пересечение ул. Бр. Кашириных , 97 и ул. Чайковского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (908, '117', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Салтыкова,49а и ул. Профинтерна');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (909, '118', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр./ ул. Новоэлеваторная, 51/1, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (910, '119', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Молодогвардейцев, 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (911, '120', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. 40 лет Победы/ пр. Победы, напротив д. 315');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (912, '121', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 33/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (913, '122', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр. ,37');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (914, '123', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр. (в центр)/ ул. Индивидуальная, 1а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (915, '124', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (916, '125', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 51П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (917, '126', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 15, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (918, '127', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Героев Танкограда и ул. Механическая, 65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (919, '128', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр./ ул. Черкасская, 15/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (920, '129', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Первой Пятилетки, 9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (921, '130', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Комсомолький пр., 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (922, '132', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., напротив д. 7/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (923, '133', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы,  329');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (924, '134', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Молодогвардейцев, 68');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (925, '135', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 110');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (926, '136', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (927, '137', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 198');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (928, '138', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение пр. Победы и ул. Российская, 32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (929, '139', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'шоссе Металлургов, 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (930, '140', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Дзержинского, 95');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (931, '141', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Курганский тр., 700 м до поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (932, '142', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Хлебозаводская, 7в/1 и ул. Автоматики');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (933, '143', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        ' пересечение ул. Северо-Крымской (из центра) и ул. Бр. Кашириных, 95');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (934, '144', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. 40 лет Победы, 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (935, '145', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Ельина, 90');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (936, '146', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Красная/ул.Коммуны, 70');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (937, '147', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гостевая/ ул. Заводская, напротив д. 14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (938, '148', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Артиллерийская, 124б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (939, '149', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 107');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (940, '150', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 66 (100 м от поста ГИБДД)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (941, '151', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Энгельса и ул. Труда (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (942, '152', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 106');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (943, '153', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр. (в центр)/ ул. 8 Марта, 108');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (944, '154', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Энгельса, 95');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (945, '155', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Ловина, 200 м до ул. Артиллерийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (946, '156', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Первой Пятилетки/ ул. Марченко, 25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (947, '157', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение Копейского шоссе и ул. Машиностроителей');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (948, '158', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Дарвина, напротив д. 4а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (949, '159', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Салютная, напротив д. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (950, '160', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Ворошилова  и пр. Победы,346');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (951, '161', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 67П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (952, '162', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Кирова, 7 и ул. Калинина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (953, '163', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Елькина,85, регистрационная палата');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (954, '164', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Дарвина, 18к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (955, '165', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Труда, 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (956, '166', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Плеханова, 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (957, '167', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 11а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (958, '168', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Артиллерийская, 136, по. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (959, '169', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пл. Павших Революционеров/ ул. Российская, 124/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (960, '170', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Труда, 183');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (961, '171', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Воровского и ул. Доватора, напротив д. 55');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (962, '172', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Российская, 202');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (963, '173', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Энгельса, 97б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (964, '175', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Западное шоссе (в центр), 1400 м до поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (965, '176', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Свободы, 185, ТК "Экспресс"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (966, '177', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Труда и ул. 3-го Интернационала, 105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (967, '178', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр./ ул. Морозова, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (968, '179', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Кирова, 19а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (969, '180', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Свердловский тр., 1д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (970, '181', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр.,поворот на Новосинеглазово, выезд');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (971, '182', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 51п/1, справа');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (972, '183', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Артема, 187/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (973, '184', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных (в центр)/ ул. Ворошилова, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (974, '185', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр., 40а/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (975, '186', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Бродокалмакский тр.,  напротив д. 3/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (976, '187', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Героев Танкограда, 80П и ул. Механическая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (977, '188', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных (центр), 400 м до ул. Краснознаменная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (978, '189', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Труда, 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (979, '190', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр./ ул. Новоэлеваторная, 49/7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (980, '191', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'площадь Аэропорта, с левой стороны по направлению к площади Аэропорта');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (981, '192', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр. (из центра), 150 м до поворота на Лакокрасочный завод');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (982, '193', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 90');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (983, '194', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., напротив д. 4б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (984, '195', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'дорога из Аэропорта, 140 м от площади аэропорта (в город)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (985, '196', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр., напротив д.86');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (986, '197', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан (из центра)/ ул. Артиллерийская, 136, поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (987, '198', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 12/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (988, '199', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Свободы, напротив д. 66');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (989, '200', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гагарина, напротив д. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (990, '201', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 118');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (991, '202', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Северный луч, до поворота на ул. Хлебозаводская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (992, '203', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Плеханова, 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (993, '204', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гостевая/ ул. Парковая, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (994, '205', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Дарвина, 10/1 и ул. Новосельская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (995, '206', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Молдавская, 21а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (996, '207', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., 11, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (997, '208', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Гагарина, 22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (998, '209', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Харлова, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (999, '210', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение автодороги Меридиан и ул. Гончаренко, напротив д. 99');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1000, '211', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 102в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1001, '212', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 110');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1002, '213', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр. (в центр), 150 м после пересечения с ул. Радонежская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1003, '214', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 52б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1004, '215', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомолький пр., 111/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1005, '216', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Бажова и ул. Комарова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1006, '218', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1007, '219', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Харлова, 4/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1008, '220', '1/26/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 79П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1009, '221', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1010, '222', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Центральная (п. Западный2, стр. 1)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1011, '223', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Гагарина, 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1012, '224', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'Копейское шоссе, 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1013, '225', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 154');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1014, '226', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 6/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1015, '227', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Центральная, 1700 м до поста ГИБДД, в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1016, '228', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        ' ул. Российская, 222');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1017, '229', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение пр. Победы (из центра) и ул. Чайковского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1018, '230', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 305б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1019, '231', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1020, '232', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Молдавская и пр,Победы, 374');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1021, '233', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Черкасская, 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1022, '234', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 116');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1023, '235', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Доватора, 1 и ул. Цехова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1024, '236', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 113');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1025, '237', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 33, 100 м до ул. Красного Урала');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1026, '238', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Воровского, 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1027, '239', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр./ ул. Новоэлеваторная, 49/7, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1028, '240', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Бродокалмакский тр.,  напротив д. 3/1, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1029, '241', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'дорога в Аэропорт, 150 м до поворота на таможню');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1030, '242', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 65П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1031, '243', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Артиллерийская, 111');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1032, '244', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., напротив д. 8а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1033, '245', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Елькина, напротив д. 59');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1034, '246', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Молдавская, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1035, '247', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гостевая/ ул. Центральная, 3бк1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1036, '248', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Сортировочная, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1037, '249', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 70, выезд');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1038, '250', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр (в центр)., 200 м до поворота на ул. Радонежская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1039, '251', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Лермонтовский пер. и ул. Котина, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1040, '252', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 192');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1041, '253', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан, напротив автодрома Обл. ГИБДД, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1042, '254', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1043, '255', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Копейское шоссе/ ул. Гагарина, 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1044, '256', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Механическая,40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1045, '257', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., въезд в город, 700 м после  поворота на Исаково');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1046, '258', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. 250 лет Челябинску,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1047, '259', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Новороссийская, 126');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1048, '260', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Бродокалмакский тр.,въезд в город с СНТ "Учитель"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1049, '261', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Салавата Юлаева, 29, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1050, '262', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Танкистов, 138');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1051, '263', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Копейское шоссе, 600 м от виадука, из города');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1052, '264', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. 250 лет Челябинску, 32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1053, '265', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Северный луч, 18/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1054, '266', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Танкистов и ул. Кулибина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1055, '267', '2/17/2015', '12/16/2014', '12/15/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул.Хлебозаводская, 7/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1056, '268', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 51-п/1, АЗС');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1057, '269', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', ' ул. Комарова, 133');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1058, '270', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 10к/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1059, '271', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 73');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1060, '272', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Гражданская, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1061, '273', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение шоссе Металлургов, 70 и ул. Румянцева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1062, '274', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Западное шоссе, 1-й км');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1063, '275', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 97');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1064, '276', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Артема, 105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1065, '277', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр./ ул. Трактовая, напротив д. 9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1066, '278', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Харлова/ ул. Барбюса, 2/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1067, '279', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гостевая/  ул. Степная, 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1068, '280', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 167');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1069, '281', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Гагарина, 33а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1070, '282', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Дарвина, 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1071, '283', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение Свердловского тр. (в центр) и ул. Радонежская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1072, '284', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Шота Руставели, 32/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1073, '285', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 21а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1074, '286', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Блюхера, напротив д. 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1075, '287', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гостевая/  ул. Садовая, 1а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1076, '288', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 65/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1077, '289', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Чайковского, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1078, '290', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Российская, 275');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1079, '291', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 9к2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1080, '292', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Труда/ ул. Свободы, 65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1081, '293', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Профинтерна/ ул. Рылеева, 20/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1082, '294', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Блюхера,101');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1083, '295', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1084, '296', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1085, '297', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ Троицкий тр., 1/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1086, '298', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 75-П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1087, '299', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Дарвина, 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1088, '300', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Бродокалмакский тр.(в центр), 550 м от поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1089, '301', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Мамина, 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1090, '302', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1868 км+30 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1091, '303', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1092, '304', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Копейское шоссе, напротив д. 1/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1093, '305', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Игуменка, 9а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1094, '306', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Хлебозаводская и ул. Винницкая, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1095, '307', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Танкистов, 175');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1096, '308', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Тухачевского, 18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1097, '309', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Краснознаменная, 32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1098, '310', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Хлебозаводская, напротив д. 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1099, '311', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Танкистов, напротив д. 148');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1100, '312', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Гражданская, напротив д.25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1101, '313', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пер. Лермонтова/ ул. Комарова, д. 110 к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1102, '314', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр. (в центр), 200 м после ост. "Подстанция"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1103, '315', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1104, '316', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 59П к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1105, '317', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр./ ул. Морозова,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1106, '318', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'пересечение пр. Ленина, 18 и ул. Горького');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1107, '319', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1869 км + 950 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1108, '320', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 61П');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1109, '321', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Бродокалмакский тр., 3/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1110, '322', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1111, '323', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., (в город), 400 м после поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1112, '324', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Хлебозаводская и ул. Пекинская, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1113, '325', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Доватора, 1м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1114, '327', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных, 95');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1115, '328', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Труда, 100');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1116, '329', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'шоссе Металлургов, 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1117, '330', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Линейная, 51');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1118, '331', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., после поста ГИБДД, въезд в город');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1119, '332', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бр. Кашириных/ Северо-Крымский 2-й пер., 10/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1120, '333', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Хлебозаводская, напротив д. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1121, '334', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1122, '335', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Салютная, 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1123, '336', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., 100 м за пос. Исаково');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1124, '337', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1125, '338', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр./ ул. Новгородская, 1г');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1126, '339', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Доватора, 31');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1127, '340', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пр. Победы, напротив д. 382а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1128, '341', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр, 54б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1129, '342', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., (в город), 50 м после поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1130, '343', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., поворот на Новосинеглазово, въезд');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1131, '344', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Молодогвардейцев и ул. Куйбышева, 75');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1132, '345', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение автодороги Меридиан и ул. Новороссийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1133, '346', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Красноармейская/ ул. Труда, 56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1134, '347', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан,170 м до виадука/ ул. Дзержинского, 125');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1135, '348', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Западное шоссе, поворот на п. Западный 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1136, '349', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных, 133/2 и ул. Чичерина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1137, '350', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Курчатова, 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1138, '351', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., напротив д. 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1139, '352', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Свободы, 179');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1140, '353', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1141, '354', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'пересечение пр. Ленина, 10 и ул. Героев Танкограда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1142, '355', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'ул. Артиллерийская, 119');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1143, '356', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 76, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1144, '357', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1145, '358', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'пр. Ленина/ ул. 40 лет Октября, 26');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1146, '359', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 24');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1147, '360', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', ' пр. Ленина, 21в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1148, '361', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр. (из центра), 250 м до поворота на ЧВВАКУШ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1149, '362', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Танкистов, 144');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1150, '363', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Худякова,12к1 и ул. Татьяничевой');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1151, '364', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д.35к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1152, '365', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Хлебозаводская, напротив д. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1153, '366', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'развяка на пос. Новосинеглазово, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1154, '367', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение автодороги Меридиан и ул. Новороссийская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1155, '368', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., п. Исаково/ ул. Трактовая, напротив д. 41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1156, '369', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 137');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1157, '370', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Дзержинского, 102');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1158, '371', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Блюхера/ ул. Мебельная, 88');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1159, '372', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Коммуны/ ул. Энтузиастов, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1160, '373', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Бажова, 35/ Лермонтовский пер., 4а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1161, '374', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр./ ул. Цинковая, 2а/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1162, '375', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Первой Пятилетки, напротив д.57');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1163, '376', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Комарова, 129');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1164, '377', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. 50 лет ВЛКСМ, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1165, '378', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 71П ст10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1166, '379', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 1б/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1167, '380', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Дзержинского, 104');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1168, '381', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Профинтерна/ ул. Марата, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1169, '382', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Чичерина/ Комсомольский пр., напротив д. 112/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1170, '383', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Приозерная, 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1171, '384', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., 800 м до поворота на п. Исаково');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1172, '385', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 85');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1173, '386', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Артиллерийская, 136');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1174, '387', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., въезд, 900 м до поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1175, '388', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1176, '389', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., выезд из города, после поворота на Новосинеглазово');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1177, '390', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Дарвина, напротив д. 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1178, '391', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Копейское шоссе (из города), 300 м от виадука');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1179, '392', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Механическая и ул.Артиллерийская (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1180, '393', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1181, '394', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр., 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1182, '395', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Артиллерийская, 117');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1183, '396', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 52П/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1184, '398', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Копейскре шоссе, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1185, '399', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., / ул. Крнтейнерная, напротив д.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1186, '400', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда/ ул. Валдайская, напротив д. 1а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1187, '401', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Краснознаменная, 36');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1188, '402', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Чайковского, напротив д. 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1189, '403', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 11а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1190, '404', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Победы, 140');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1191, '405', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан/ ул. Игуменка, 155');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1192, '406', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр.,/ ул. Трактовая, напротив д. 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1193, '407', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'пр. Ленина, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1194, '408', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Кулибина/ ул. Мамина, 27а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1195, '410', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан, 1/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1196, '411', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Уфимский тр., 1/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1197, '412', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 3а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1198, '413', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 38, завод транспортных трансмиссий');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1199, '414', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение пр. Победы, 293а и ул. Ворошилова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1200, '416', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Танкистов, 177');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1201, '417', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение Комсомольского пр. и ул. Косарева, у автобазы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1202, '418', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Первой Пятилетки, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1203, '419', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 5к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1204, '420', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Копейское шоссе, 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1205, '421', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 57');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1206, '422', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Котина, напротив д. 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1207, '423', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1208, '424', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных и ул. Ворошилова, 57а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1209, '425', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Российская/ пр. Ленина, 38');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1210, '426', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Чичерина, 15/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1211, '427', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1868 км + 510 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1212, '428', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Болейко, 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1213, '429', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив поста ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1214, '430', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'шоссе Металлургов, 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1215, '431', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр., 1ак1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1216, '432', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Богдана Хмельницкого, 21/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1217, '433', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Сталеваров, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1218, '434', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Горького, напротив д. 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1219, '435', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр./ ул. Блюхера, 101');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1220, '436', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'шоссе Металлургов, 25б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1221, '437', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., напротив д. 3а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1222, '438', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Красная, 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1223, '439', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр., 53а/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1224, '440', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Карла Маркса, 81');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1225, '441', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'уд. Доватора/ ул.Воровского, 40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1226, '442', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Российская/ ул. Труда, 61');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1227, '443', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр.,напротив д. 56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1228, '444', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Энгельса, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1229, '445', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Энтузиастов/ пр. Ленина, 74');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1230, '446', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский пр., 40а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1231, '447', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гостевая/ ул. Заводская, 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1232, '448', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1233, '449', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1234, '450', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 26/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1235, '451', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1236, '452', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1237, '453', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'ул. Артиллерийская, 121');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1238, '454', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'ул. Кирова, 78/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1239, '455', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 76, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1240, '456', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'пр. Ленина, 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1241, '457', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'ул. Рождественского, напротив д. 5, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1242, '458', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'ул. Воровского, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1243, '459', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'ул. Энгельса/ пр. Ленина, 71А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1244, '460', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'ул. Кирова (в центр), 150 м  до моста');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1245, '461', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'ул. Энгельса, 26');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1246, '462', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'ул. Рождественского, напротив д. 5, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1247, '463', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд', 'ул. Российская, 202');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1248, '464', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'сити-борд',
        'пересечение ул. Цвиллинга, 25к2 и ул. Коммуны');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1249, '465', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1250, '466', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр./ ул. Чайковского, 20/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1251, '467', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Свободы/ ул. К. Маркса, 38');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1252, '468', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'Троицкий тр.,11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1253, '469', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гагарина/ ул. Гончаренко, 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1254, '470', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловкий тр., цинковый завод');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1255, '471', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Сталеваров, 5к3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1256, '472', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул.Свободы/ Привокзальная площадь, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1257, '473', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Горького, 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1258, '474', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомолький пр., 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1259, '475', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Северный луч, напротив д. 47');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1260, '476', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пр. Победы/ ул. Чайковского, 83');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1261, '477', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Северный луч, 47 и ул. Хлебозаводская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1262, '478', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., 3/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1263, '479', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Героев Танкограда, 61п, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1264, '480', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., напротив д. 14б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1265, '481', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Красная/ ул. Карла Маркса,133');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1266, '482', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение Свердловского пр.  и ул. Воровского, 26');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1267, '483', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Молодогвардейцев, 56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1268, '484', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение Свердловского пр. и пр. Победы, 192а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1269, '485', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка', 'ул. Черкасская,15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1270, '486', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1868 км + 830 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1271, '488', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1871 км + 120 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1272, '489', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Комсомольский пр., 84');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1273, '490', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Богдана Хмельницкого, 35 и ул. Румянцева');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1274, '491', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Дзержинского, 119');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1275, '492', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловский тр., напротив д. 12в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1276, '493', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1869 км + 850 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1277, '494', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Красная (из центра) и ул. Труда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1278, '495', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Красная (из центра) и ул. Коммуны');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1279, '497', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Салтыкова/ ул. Междугородная, 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1280, '498', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Уфимский тр., 1/1,1868 км + 550 м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1281, '499', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'автодорога Меридиан / ул. Артиллерийкая, 136, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1282, '500', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Свердловкий тр., 1а/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1283, '502', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Краснознаменная/ ул. Островского, 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1284, '503', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Молодогвардейцев, 66');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1285, '504', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. 50 лет ВЛКСМ, 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1286, '505', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Хлебозаводская и ул. Морская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1287, '506', '2/17/2015', '12/20/2014', '12/19/2019', 'ЗАО "Элис"', 'щитовая установка',
        'Троицкий тр., 100 м за после ост. "Подстанция" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1288, '509', '2/17/2015', '12/20/2014', '12/19/2024', 'ЗАО "Элис"', 'ЩУ арочного типа',
        'автодорога Меридиан/ ул. Либединского, 39');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1289, '510', '2/17/2015', '12/20/2014', '12/19/2024', 'ООО"Амиго-Медиа"', 'Экран',
        'пересечение ул. Бр. Кашириных (из центра) и ул. Северо-Крымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1290, '513', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'сити-борд',
        'пр. Ленина, 17, за остановкой из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1291, '514', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Черкасская,7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1292, '515', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка', 'пр.Победы,317');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1293, '516', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Гагарина, на газоне между д.32 и д.36');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1294, '517', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка', 'ул.Свободы,88');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1295, '518', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка', 'пр.Ленина,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1296, '519', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Молодогвардейцев,напротив д.48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1297, '520', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'Свердловский пр., ост. "Торговый центр" (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1298, '521', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул. Курчатова, 19А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1299, '522', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Энгельса,99');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1300, '523', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Горького,22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1301, '524', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        ' пр.Победы,238');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1302, '525', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пересечение ул. Гагарина и ул. Южный Бульвар, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1303, '526', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пересеч.Комсомольского пр.(из центра) и ул.Ворошилова,у АЗС');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1304, '527', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'Свердловский пр,78,начало дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1305, '528', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Курчатова,23,за маг."Декорум"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1306, '529', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Кирова,23-а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1307, '530', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Энгельса,46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1308, '531', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Комарова,110,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1309, '532', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пр.Победы,287, больница Скорой помощи (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1310, '533', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Бр.Кашириных, 87а (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1311, '534', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'автодорога Меридиан, со стороны дома 2/2 по ул.Луценко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1312, '535', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка', 'пр.Победы,308');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1313, '536', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'Свердловский тр.,(из центра), перед трамвайным депо № 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1314, '538', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Курчатова,24');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1315, '539', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пересеч.пр.Победы (из центра) и ул.Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1316, '540', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка', 'ул.Кирова, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1317, '541', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Труда,ост. "ТРК "Родник"(из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1318, '542', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'Свердловский пр.(из центр),набережная р.Миасс,у Торгового центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1319, '543', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Бр.Кашириных,129 (ЧелГУ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1320, '544', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Гагарина,13 (газон перед Стоматологией)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1321, '545', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Энгельса,43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1322, '546', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пр.Победы(из центра), перед Ленинградским мостом');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1323, '547', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул. Степана Разина, 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1324, '548', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Комарова,110,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1325, '549', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пересечение Сверловского пр.,58 и ул. Коммуны');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1326, '550', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'пересечение  ул.Бр.Кашириных (из центра) и ул.Актюбинская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1327, '551', '3/4/2015', '12/9/2014', '12/8/2019', 'ООО"Гэллэри Сервис"', 'щитовая установка',
        'ул.Курчатова,27/ ул. Энгельса, 95');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1328, '552', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Черкасская,3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1329, '553', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский тр. (в центр), авторынок');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1330, '554', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Индивидуальная, 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1331, '555', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр., 78');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1332, '556', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Копейское шоссе, 100 м от виадука, из города');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1333, '557', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Хлебозаводская,5а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1334, '558', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Профинтерна/ ул. Московская, 26');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1335, '559', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'Троицкий тр., 7/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1336, '560', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Копейское шоссе/ ул. Харлова, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1337, '561', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Бажова, 91');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1338, '562', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул.Горького, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1339, '563', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр., 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1340, '564', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'пр. Победы,111');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1341, '565', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Артиллерийская, 105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1342, '566', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр., 29, у завода "Прибор"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1343, '567', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Троицкий тр., 600 м после ост. "Подстанция"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1344, '568', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Модавская, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1345, '569', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Чичерина, 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1346, '570', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'автодорога Меридиан/ул. Механическая, 40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1347, '571', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Копейское шоссе, 49');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1348, '572', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Черкасская/ ул. 50-летия ВЛКСМ, 39');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1349, '573', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Троицкий тр./ ул. Грязева, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1350, '574', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Героев Танкограда, 71П ст10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1351, '575', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Жукова,12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1352, '576', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Хлебозаводская, напротив д. 42');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1353, '577', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр. / ул. Молдавская, 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1354, '578', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Труда, 24');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1355, '579', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. Черкасская, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1356, '580', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский тр., 1а/3, поворот на Лакокрасочный завод');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1357, '581', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'ул. Каслинская, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1358, '582', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'Троицкий тр., 11а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1359, '583', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'пересечение ул. Молодогвардейцев и Комсомольского пр., 48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1360, '584', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Свердловский пр./  Комсомольский пр., 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1361, '585', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка', 'ул. С. Кривой, 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1362, '586', '3/4/2015', '12/16/2014', '12/15/2019', 'ООО "Элис ЛМ"', 'щитовая установка',
        'Комсомольский пр., 103');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1363, '626', '3/31/2015', '5/1/2014', '4/30/2019', 'ЗАО "Элис"', 'щитовая установка',
        'ул. Гагарина,7, Политех');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1364, '627', '3/31/2015', '5/1/2014', '4/30/2019', 'ЗАО "Элис"', 'щитовая установка',
        'пересечение ул. Гагарина,7 и ул. Тухачевского ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1365, '641', '4/30/2015', '11/1/2014', '10/31/2019', 'ЗАО "Райффазенбанк"', 'панель-кронштейн на собст.опоре',
        'ул. Молодогвардейцев,53');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1366, '646', '5/5/2015', '3/19/2015', '11/14/2019', 'ООО КБ "ИНТЕРКОММЕРЦ"', 'сити-формат',
        'ул.Карла Маркса,38');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1367, '654', '5/20/2015', '3/31/2015', '3/30/2020', 'ОАО"Челябинский электромеханический завод"',
        'щитовая установка', 'п.Шершни, ул.Центральная,3б (ул.Гостевая со стороны стадиона)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1368, '658', '5/20/2015', '1/15/2015', '1/14/2020', 'ООО"Алвик"', 'нестанд.рекл.констр.',
        'Комсомольский пр.,85');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1369, '664', '5/28/2015', '5/20/2015', '5/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'ул.Энтузиастов, 15д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1370, '717', '8/19/2015', '7/16/2015', '7/15/2020', 'ООО Компания "АВС"', 'стела',
        'Копейское шоссе, 52, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1371, '718', '8/19/2015', '7/16/2015', '7/15/2020', 'ООО Компания "АВС"', 'стела',
        'Копейское шоссе, 52, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1372, '719', '8/19/2015', '7/16/2015', '7/15/2020', 'ООО Компания "АВС"', 'стела',
        'Копейское шоссе, 52, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1373, '727', '8/31/2015', '9/1/2015', '8/31/2020', 'Клушин И.Ф.', 'щитовая установка', 'Копейское шоссе, 58');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1374, '767', '10/23/2015', '10/20/2015', '10/19/2020', 'ИП Леонгардт Д.И.', 'стела',
        'ул. Шарова, напротив д. 59');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1375, '768', '10/23/2015', '10/20/2015', '10/19/2020', 'ООО"КЕММА"', 'Указатель (с/о)',
        'Курганский тр. (перед развязкой ЧМЗ-Аэропорт-Курганский тр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1376, '769', '10/23/2015', '10/20/2015', '10/19/2020', 'ООО"КЕММА"', 'Указатель (с/о)',
        'Курганский тр. (после развязки ЧМЗ-Аэропорт-Курганский тр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1377, '770', '10/23/2015', '10/20/2015', '10/19/2020', 'ООО"КЕММА"', 'Указатель (с/о)',
        'Автодорога ЧМЗ (перед развязкой ЧМЗ-Аэропорт-Курганский тр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1378, '781', '10/26/2015', '10/20/2015', '10/19/2020', 'ООО "Омега"', 'стела', 'автодорога Меридиан (7-ой км)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1379, '782', '10/26/2015', '10/20/2015', '10/19/2020', 'ООО "Компания "Пегас-тур"', 'стела', 'ул.Коммуны,125');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1380, '786', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'ул. Труда, 197');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1381, '787', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'ул. Бр. Кашириных,147');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1382, '788', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'Свердловский пр., 86');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1383, '789', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'ул. Курчатова, 8б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1384, '790', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'Свердловский пр., напротив д. 64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1385, '791', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'ул. Курчатова, между домами № 16 и № 14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1386, '792', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'Комсомольский пр., 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1387, '793', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО РА "Гранат"', 'щитовая установка',
        'ул.250 лет Челябинску,12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1388, '794', '10/28/2015', '10/20/2015', '10/19/2020', 'ИП Трифонов Э.С.', 'транспарант-перетяжка',
        'ул. Худякова, 22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1389, '795', '10/28/2015', '10/20/2015', '10/19/2020', 'ИП Трифонов Э.С.', 'транспарант-перетяжка',
        'ул.Чичерина, 33');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1390, '796', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Карус"', 'щитовая установка',
        'пр.Победы (в центр),за Ленинградским мостом');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1391, '798', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Карус"', 'щитовая установка',
        'пересечение ул. Труда, напротив д. 161 и Свердловского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1392, '799', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Карус"', 'щитовая установка', 'ул. Доватора, 1г');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1393, '800', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Карус"', 'щитовая установка',
        'пересечение шоссе Меридиан и ул. Рождественского (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1394, '801', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Карус"', 'щитовая установка',
        'пересеч.пр.Победы и ул.Бажова, напротив д.23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1395, '802', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'ул. Труда,напротив д. 203, ост. "Родничок"(в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1396, '803', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка', 'ул.Кирова,1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1397, '804', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'ул.Труда,168/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1398, '805', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'ул.Цвиллинга,58');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1399, '808', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'ул.Труда,185-1,АЗС "Лукойл"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1400, '809', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'пр.Победы,158/ ул.Болейко,7б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1401, '810', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'ул.Российская, 67');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1402, '812', '10/28/2015', '10/20/2015', '10/19/2020', 'ООО"Урал Стрит"', 'щитовая установка',
        'пр.Комарова,114');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1403, '825', '11/3/2015', '11/1/2015', '10/1/2020', 'ООО"АЛВИК"', 'нестанд.рекл.констр.', 'ул.Елькина,63а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1404, '826', '11/3/2015', '10/20/2015', '10/19/2020', 'ООО ПКФ "Символ"', 'сити-формат',
        'ул.С.Кривой,49б, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1405, '827', '11/3/2015', '10/20/2015', '10/19/2020', 'ООО ПКФ "Символ"', 'сити-формат',
        'ул.С.Кривой,49б, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1406, '828', '11/3/2015', '10/20/2015', '10/19/2020', 'ООО "Армада Сервис"', 'щитовая установка',
        'ул. Кирова,3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1407, '829', '11/3/2015', '10/20/2015', '10/19/2020', 'ООО "Армада Сервис"', 'щитовая установка',
        'дорога в Аэропорт,поворот на таможню');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1408, '832', '11/5/2015', '10/20/2015', '10/19/2020', 'ООО"Планета Авто"', 'Флаги',
        'ул.Бр.Кашириных,137,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1409, '833', '11/5/2015', '10/20/2015', '10/19/2020', 'ООО"Планета Авто"', 'Флаги',
        'ул.Бр.Кашириных,137,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1410, '849', '11/13/2015', '10/1/2015', '8/10/2020', 'ООО "Аптека Норма низкие цены"', 'нестанд.рекл.констр.',
        'пр.Ленина,20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1411, '856', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'сити-борд', 'пр.Ленина,64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1412, '857', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'сити-борд', 'пр.Ленина,71');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1413, '858', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'Комсомольский пр., 66');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1414, '859', '11/13/2015', '10/20/2015', '10/19/2020', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        'пр.Победы, 325, газон у парковки ТК "Прииск"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1415, '860', '11/13/2015', '10/20/2015', '10/19/2020', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        'автодорога Меридиан, ул. Гражданская,10/1 (рынок Меридиан)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1416, '861', '11/13/2015', '10/20/2015', '10/19/2020', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        'пр.Победы,131');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1417, '862', '11/13/2015', '10/20/2015', '10/19/2020', 'ООО "Гэллэри Сервис"', 'сити-борд', 'пр.Ленина,62');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1418, '866', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул. Плеханова, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1419, '867', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'пр. Комарова, 125');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1420, '868', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул.Новороссийская, 88');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1421, '869', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул.Блюхера, 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1422, '870', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул. Доватора, 27');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1423, '871', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул. Молодогвардейцев, напротив д.40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1424, '872', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул. Российская, 267А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1425, '873', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'Комсомольский пр., 47');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1426, '874', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'ул. Бр. Кашириных, 108');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1427, '875', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'пр. Победы, 323');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1428, '876', '11/19/2015', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'щитовая установка',
        'пересечение Комсомольского пр. и ул. Чайковского, 15/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1429, '17', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Краснознаменная,1А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1430, '18', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Воровского, 73а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1431, '19', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Энгельса, 44г');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1432, '20', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. С. Кривой, 83');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1433, '21', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. 40 лет Победы/пр. Победы, 382а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1434, '22', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Воровского, 21');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1435, '23', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Доватора, 35 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1436, '24', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Братьев Кашириных, 95 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1437, '25', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Воровского, 32');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1438, '26', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. 40 лет Победы, 35');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1439, '27', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. 250 лет Челябинску, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1440, '28', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Воровского, 55');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1441, '29', '1/11/2016', '12/22/2015', '12/21/2020', 'ИП Миронова И.Л.', 'сити-формат', 'Свердловский пр.,65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1442, '30', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Университетская Набережная, 36');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1443, '31', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'Свердловский пр., напротив д. 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1444, '33', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'пересечение ул. Доватора, 23 и ул.Сулимова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1445, '35', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'дорога из аэропорта Баландино (380 м от аэропорта)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1446, '37', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Университетская Набережная, 30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1447, '38', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Курчатова, 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1448, '39', '1/12/2016', '12/22/2015', '12/21/2020', 'ИП Трифонов Э.С.', 'щитовая установка',
        'ул. Салавата Юлаева, 17в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1449, '40', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО "Омега"', 'стела', 'ул.Радонежская,12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1450, '49', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО"Техно-Моторс"', 'стела', 'ул.Бр.Кашириных,129д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1451, '50', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО"ТТМ-1"', 'стела', 'ул.Чичерина, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1452, '51', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО "Сейхо Моторс"', 'стела', 'пр. Ленина, 3е, стр. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1453, '52', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО "Сейхо Моторс"', 'стела', 'пр. Ленина, 3е, стр. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1454, '53', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО "ЧМНУ "СЭММ"', 'щитовая установка',
        'ул. Молодогвардейцев,35б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1455, '54', '1/12/2016', '12/22/2015', '12/21/2025', 'ООО"АТЛ-Компания"', 'ЩУ арочного типа',
        'Копейское шоссе, 17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1456, '55', '1/12/2016', '12/22/2015', '12/21/2025', 'ООО"АТЛ-Компания"', 'ЩУ арочного типа',
        'Троицкий тракт, 25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1457, '56', '1/12/2016', '12/22/2015', '12/21/2025', 'ООО"АТЛ-Компания"', 'ЩУ арочного типа',
        'ул. Героев Танкограда, 2в');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1458, '57', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО "Автоцентр-Керг"', 'стела',
        'ул.Новоэлеваторная,49/9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1459, '58', '1/12/2016', '12/22/2015', '12/21/2020', 'ООО "Автоцентр-Керг"', 'флаги',
        'ул.Новоэлеваторная,49/7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1460, '62', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО "Три Гор и К"', 'стела',
        'ул.Бр.Кашириных,напротив д.103');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1461, '63', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО Фирма "Урал БЭСТ"', 'стела', 'ул.Гагарина,7а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1462, '64', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО Фирма "Урал БЭСТ"', 'Флаги', 'ул.Гагарина,7а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1463, '65', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО ПРГ"Элефант"', 'панель-кронштейн',
        'ул.Татьяничевой,напротив д.22/11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1464, '66', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО ПРГ"Элефант"', 'панель-кронштейн',
        'ул.Образцова,напротив д.24');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1465, '67', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул. 40 лет Победы, 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1466, '68', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул. 50 лет ВЛКСМ, 31');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1467, '69', '1/15/2016', '12/22/2015', '12/21/2025', 'ИП Юносова Г.И.', '72 суперсайт',
        'пересечение ул. Братьев Кашириных (из центра) и ул. Молодогвардейцев, 70а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1468, '70', '1/15/2016', '12/22/2015', '12/21/2025', 'ИП Юносова Г.И.', '225 суперсайт',
        'пересечение Комсомольского пр.,2 и Свердловского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1469, '72', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул. Новороссийская, 122');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1470, '73', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул. 40 лет Победы, напротив д. 22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1471, '74', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул. 1 Пятилетки, напротив д. 37');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1472, '75', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул.Воровского, автопарковка обл.больницы, между опорами №11 и 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1473, '76', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул.С.Юлаева,27,у ТД"Одиссей"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1474, '77', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'Комсомольский пр.,101');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1475, '78', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул.1 Пятилетки,между д.41 и 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1476, '79', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул.1 Пятилетки,между д.57 и 55');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1477, '80', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'ул.50 лет ВЛКСМ,27');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1478, '81', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Юносова Г.И.', 'щитовая установка',
        'пересеч.ул.Артиллерийской и ул.Бажова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1479, '82', '1/15/2016', '12/22/2015', '12/21/2020', 'ИП Парамонов О.В.', 'щитовая установка',
        'ул. Образцова,7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1480, '87', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО "Премьер Инвест"', 'щитовая установка',
        'ул.1-я Потребительская/ Троицкий тр.,15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1481, '88', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО "Премьер Инвест"', 'щитовая установка',
        'ул.1-я Потребительская,1, напротив фабрики "Линда"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1482, '89', '1/15/2016', '12/22/2015', '12/21/2020', 'ООО "Премьер Инвест"', 'щитовая установка',
        'ул.Лесопарковая,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1483, '91', '1/26/2016', '12/22/2015', '12/21/2020', 'ООО "Содействие"', 'щитовая установка',
        'ул.Черкасская/ ул.Комаровского,2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1484, '99', '2/2/2016', '11/9/2015', '11/9/2020', 'ООО УК-2"Южуралстройсервис"', 'нестанд.рекл.констр.',
        'ул.Новороссийская,39');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1485, '100', '2/2/2016', '11/2/2015', '11/2/2020', 'ООО УК-2"Южуралстройсервис"', 'нестанд.рекл.констр.',
        'ул.Новороссийская, 43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1486, '101', '2/2/2016', '3/27/2015', '3/27/2020', 'ООО УК-4"Южуралстройсервис"', 'нестанд.рекл.констр.',
        'ул.Гагарина,47');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1487, '102', '2/4/2016', '12/22/2015', '12/21/2020', 'ИП Филиппов С.Н.', 'стела',
        'пересечение шоссе Металлургов, 70/1 и ул.Черкасская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1488, '103', '2/4/2016', '12/22/2015', '12/21/2020', 'ИП Филиппов С.Н.', 'стела',
        'пересечение ул.Богдана Хмельницкого и ул.Румянцева (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1489, '104', '2/4/2016', '12/22/2015', '12/21/2020', 'ИП Филиппов С.Н.', 'стела', 'ул.Сталеваров,17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1490, '105', '2/4/2016', '12/22/2015', '12/21/2020', 'ИП Филиппов С.Н.', 'щитовая установка',
        'автодорога Меридиан, напротив д.47 по ул.Либединского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1491, '106', '2/4/2016', '12/22/2015', '12/21/2020', 'ИП Филиппов С.Н.', 'щитовая установка',
        'пересечение ул.Автодорожная и ул.Производственная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1492, '107', '2/5/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'сити-формат', 'пр.Ленина,87');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1493, '126', '2/18/2016', '2/18/2016', '2/17/2021', 'ООО"Риэлти 74"', 'стела',
        'ул. Профессора Благих, напротив домов 61 и 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1494, '128', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'пр.Победы,330');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1495, '129', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'ул.Блюхера,95');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1496, '131', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'Свердловский тр., у АЗС "Лукойл" в районе рынка "Орбита"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1497, '132', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'ул.Блюхера,49');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1498, '133', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'ул.Бр.Кашириных,105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1499, '134', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'ул.Дзержинского,107');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1500, '135', '3/21/2016', '12/22/2015', '12/21/2025', 'ООО"Элефант-Проспект"', 'ЩУ арочного типа',
        'пересеч.ул.Бажова,11 и ул. Загорская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1501, '136', '3/21/2016', '12/1/2015', '11/30/2020', 'ООО"Леруа Мерлен Восток"', 'нестанд.рекл.констр.',
        'Копейское шоссе,64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1502, '137', '3/21/2016', '12/1/2015', '11/30/2020', 'ООО"Леруа Мерлен Восток"', 'нестанд.рекл.констр.',
        'Копейское шоссе,64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1503, '138', '3/23/2016', '2/26/2016', '2/25/2026', 'ООО"Амиго-Медиа"', 'Экран', 'ул. Кирова, 161');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1504, '139', '3/28/2016', '12/22/2015', '12/21/2020', 'ООО "РА "Элекс"', 'щитовая установка',
        'Копейское шоссе, 44');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1505, '151', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'Комсомольский пр. ,65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1506, '152', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка', 'ул. Гагарина, 42');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1507, '153', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка', 'ул.Комарова, 114');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1508, '154', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'ул. Бр. Кашириных, 79');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1509, '155', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'ул. Чичерина/ пр. Победы, 325');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1510, '156', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'Копейское шоссе, 39');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1511, '157', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'Троицкий тр. (в город), поворот в пос. Смолино');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1512, '158', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка', 'ул. Хохрякова, 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1513, '159', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'Курганское шоссе (из города), после поворота на ТЭЦ-3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1514, '160', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка', 'ул. Свободы, 74');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1515, '161', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'шоссе Металлургов/ ул. Сталеваров, 66/3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1516, '162', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'ул. Богдана Хмельницкого, напротив д. 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1517, '163', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'ул. Румянцева, 28б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1518, '164', '4/8/2016', '12/22/2015', '12/21/2020', 'ООО "Базис-Н"', 'щитовая установка',
        'ул. Гагарина, напротив д. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1519, '166', '4/18/2016', '12/22/2015', '12/21/2020', 'ООО"Элефант-Проспект"', 'нестанд.рекл.констр.',
        'ул.Сони Кривой, напротив д.26,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1520, '167', '4/18/2016', '12/22/2015', '12/21/2020', 'ООО"Элефант-Проспект"', 'нестанд.рекл.констр.',
        'ул.Сони Кривой, напротив д.26,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1521, '168', '4/18/2016', '12/22/2015', '12/21/2020', 'ООО"Элефант-Проспект"', 'нестанд.рекл.констр.',
        'ул.Сони Кривой, напротив д.26,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1522, '169', '4/18/2016', '12/22/2015', '12/21/2020', 'ООО"Элефант-Проспект"', 'нестанд.рекл.констр.',
        'ул.Сони Кривой, напротив д.26,поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1523, '170', '4/18/2016', '12/22/2015', '12/21/2020', 'ООО"Элефант-Проспект"', 'нестанд.рекл.констр.',
        'пересеч. пр.Ленина,62 и ул.Васенко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1524, '171', '4/18/2016', '12/22/2015', '12/21/2020', 'ООО"Элефант-Проспект"', 'нестанд.рекл.констр.',
        'ул.Лесопарковая,7 (поз.5)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1525, '179', '5/5/2016', '4/30/2016', '4/29/2021', 'Новоселов В.М.', 'стела', 'Копейское шоссе, 37б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1526, '196', '5/13/2016', '4/30/2016', '4/29/2021', 'ИП Зияитдинов М.Х.', 'щитовая установка', 'пр.Ленина,3/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1527, '201', '5/17/2016', '4/30/2016', '4/29/2021', 'ООО ЦЭМ "Барселона"', 'стела', 'Комсомольский пр., 78');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1528, '202', '5/17/2016', '4/30/2016', '4/29/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Воровского,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1529, '203', '5/17/2016', '4/30/2016', '4/29/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'пр.Ленина,89');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1530, '204', '5/17/2016', '4/30/2016', '4/29/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Лесопарковая,9а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1531, '205', '5/17/2016', '4/30/2016', '4/29/2021', 'ИП Сарсенов К.Н.', 'Скамья',
        'Свердловский  пр., ост. "ул. Южная" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1532, '206', '5/17/2016', '4/30/2016', '4/29/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Салютная,11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1533, '211', '5/20/2016', '4/30/2016', '4/29/2021', 'ИП Логинова О.В.', 'стела', 'ул. Дарвина,18/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1534, '218', '5/24/2016', '4/30/2016', '4/29/2021', 'ООО"Росарматура"', 'щитовая установка',
        'пересеч.ул.Автодорожная (из центра) и ул. Производственная (100 м до пересечения)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1535, '223', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка',
        'Сибирский переезд, 111а/1 в сторону Ленинского района');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1536, '224', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка', 'ул.Кожзаводская, 106а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1537, '225', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка',
        'пересечение а/д Меридиан и ул. Труда, в сторону центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1538, '226', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка',
        'а/д Меридиан, напротив дома №4 по пер. Артиллерийский');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1539, '227', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка',
        'пересечение ул.Железнодорожной и ул.Самовольной в сторону Железнодорожного вокзала');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1540, '228', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка',
        'пересечение ул.Железнодорожной и ул.Самовольной в сторону Троицкого тракта');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1541, '229', '6/2/2016', '8/1/2016', '7/31/2021', 'АО"ЛАЙСА"', 'щитовая установка',
        'а/д Меридиан, напротив здания Меридиан автодорога, 1/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1542, '231', '6/3/2016', '4/30/2016', '4/29/2021', 'ИП Парамонов О.В.', 'щитовая установка',
        'въезд в пос. Западный');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1543, '233', '6/20/2016', '11/29/2014', '11/28/2019', 'ООО ЧОП «Варяг-2»', 'щитовая установка',
        'пр.Победы, 312');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1544, '234', '6/20/2016', '11/29/2014', '11/28/2019', 'ООО ЧОП «Варяг-2»', 'щитовая установка',
        'Свердловский тр., 9а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1545, '235', '6/20/2016', '11/29/2014', '11/28/2019', 'ООО ЧОП «Варяг-2»', 'щитовая установка',
        'ул.Автодорожная (в центр), после пересечения с ул.Мастеровая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1546, '236', '6/20/2016', '11/29/2014', '11/28/2019', 'ООО ЧОП «Варяг-2»', 'щитовая установка',
        'Троицкий тр., 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1547, '237', '6/20/2016', '11/29/2014', '11/28/2019', 'ООО ЧОП «Варяг-2»', 'щитовая установка',
        'шоссе Металлургов, 31');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1548, '239', '6/22/2016', '6/30/2016', '6/29/2021', 'ООО УК-4"Южуралстройсервис"', 'нестанд.рекл.констр.',
        'ул.Гагарина, 39 (сторона А)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1549, '240', '6/22/2016', '6/30/2016', '6/29/2021', 'ООО УК-4"Южуралстройсервис"', 'нестанд.рекл.констр.',
        'ул.Гагарина, 39 (сторона Б со стороны ул. Гончаренко)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1550, '241', '6/23/2016', '3/1/2016', '2/28/2021', 'ООО"Леруа Мерлен Восток"', 'нестанд.рекл.констр.',
        'Копейское шоссе, 64, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1551, '242', '6/23/2016', '3/1/2016', '2/28/2021', 'ООО"Леруа Мерлен Восток"', 'нестанд.рекл.констр.',
        'Копейское шоссе, 64, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1552, '243', '6/23/2016', '3/1/2016', '2/28/2021', 'ООО"Леруа Мерлен Восток"', 'нестанд.рекл.констр.',
        'Копейское шоссе, 64, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1553, '255', '7/14/2016', '10/20/2015', '10/19/2020', 'ООО"Вернисаж Медиа"', 'сити-борд', 'ул.Воровского,38б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1554, '266', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 26а, ост. "ТРК Горки" (троллейбус, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1555, '267', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 98, ост. "ул. Евтеева" (троллейбус, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1556, '268', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, напротив д. 105, ост. "Каширинский рынок"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1557, '269', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 62, ост. "Педучилище" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1558, '270', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 129, ост. "Юридический институт" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1559, '271', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, 166, ост. "Молния" (троллейбус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1560, '272', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дарвина, 35, ост. "ЦРМ" (автобус, в сторону Троицкого тр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1561, '273', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 60, ост. "ул. Лермонтова" (трамвай, в сторону С/З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1562, '274', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 152, ост. "ул. Академика Макеева" (автобус, в сторону плотины)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1563, '275', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., 54б, ост. "ул. Потребительская" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1564, '276', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Героев Танкограда/ул. Салютная, 2, ост. "Отделочные материалы" (автобус, в сторону Аэропорта)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1565, '277', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького/ул. Салютная, 27, ост. "Башня" (трамвай, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1566, '278', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 17, ост. "ул. Черкасская" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1567, '279', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 168, ост. "Теплотехнический институт" (трамвай, в сторону С-З, центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1568, '280', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Ворошилова, 2к 1, ост. "Ткацкая фабрика" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1569, '281', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 34, ост. "ул. Молодогвардейцев" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1570, '282', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 8а, ост. "Театр ЧТЗ" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1571, '283', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 15/2, ост. "Авторынок" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1572, '284', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ш. Металлургов, 70/1, ост. "ДК Строителей" (троллейбус, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1573, '285', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 346, ост. "ул. Ворошилова" (трамвай, в сторону ул. Чичерина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1574, '286', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Б. Хмельницкого, 35, ост "ул. Б. Хмельницкого (троллейбус, в сторну ул. Сталеваров)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1575, '287', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Марченко, 18, сот. "Магазин № 28" (автобус, в сторону пр. Комарова)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1576, '288', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 15, ост. "Дом одежды" (троллейбус, в сторону Копейского ш.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1577, '289', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Новороссийская, 86, ост. "ДК ЧТПЗ" (троллейбус, в сторону ул. Гагарина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1578, '290', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 13а, ост. "Комсомольская пл." (троллейбус, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1579, '291', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 37, ост. "Школа" (маршрутное такси, в сторону Копейского ш.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1580, '292', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 66, ост. "ул. Сталеваров" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1581, '293', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Павелецкая, 2-я, напротив 18/1, ост. "ЧМК" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1582, '294', '8/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина напротив д. 3/1, ост. "ЧТЗ" (трамвай, конечная)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1583, '306', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 84, ост. "ул. Плеханова" (троллейбус, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1584, '307', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 19, ост. "ТРК Горки", (троллейбус, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1585, '308', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 134, ост. "Универсам Казачий" (маршрутное такси, в сторону плотины)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1586, '309', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 293, ост. "ул. Ворошилова" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1587, '310', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 9, ост. "ул. Калинина" (трамвай, в сторону Теплотехн. Института"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1588, '311', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 57к1, ост. "ул. Братьев Кашириных" (троллейбус, в сторону пр. Победы)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1589, '312', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, напротив д. 189, ост. "Зоопарк" (троллейбус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1590, '313', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дарвина, 63, ост. "Кинотеатр Маяк" (автобус, в сторону Троицкого тр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1591, '314', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 140, ост. "Юридический институт" (трамвай, в сторону Теплотехн. Института)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1592, '315', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 67, ост. "ул. Лермонтова" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1593, '316', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., 52а, ост. "Рынок Привоз" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1594, '317', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Лесопарковая, 9а, ост. "Профилакторий" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1595, '318', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 2, ост. "Мех. Завод"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1596, '319', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 59, ост. "ДК Смена" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1597, '320', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 54, ост. "пл. Революции" (троллейбус, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1598, '321', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 50 лет ВЛКСМ, 35, ост. "п. Першино" (трамвай, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1599, '322', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 4, ост. "ДЦ Импульс" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1600, '323', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ш. Металлургов, 41, ост. "ДК Строителей" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1601, '324', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 21а/1, ост. "Трубопрокатный завод" (троллейбус, в сторону Копейского ш.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1602, '325', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Б. Хмельницкого, 14а, ост. "Кинотеатр Россия" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1603, '326', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 32, ост. "Сквер Победы" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1604, '327', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 64д, ост. "Алое поле" (трамвай, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1605, '328', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 28а, ост. "пр. Победы" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1606, '329', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дзержинского, 4, ост. "ЗЭМ" (трамвай,в сторону ул. Гагарина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1607, '330', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Марченко, напротив д. 15, ост. "Магазин № 28" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1608, '331', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Новороссийская, 64, ост. "Школа" (троллейбус, в сторону ул. Гагарина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1609, '332', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 7, ост. "Сквер Победы" (автобус, в сторону ЧМК)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1610, '333', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 3, ост. "Политехникум" (троллейбус, в сторону Копейского ш.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1611, '334', '9/12/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, 43, ост. "Городской сад" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1612, '335', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 89, ост. "ПКиО им. Гагарина" (троллейбус, конечная)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1613, '336', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы/ул. Российская, 32, ост. "ул. Российская" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1614, '337', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 74, ост. "Цирк" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1615, '338', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных/ул. Чайковского, 183, ост. "ул. Чайковского" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1616, '339', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, 203, ост. "ТРК Родник" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1617, '340', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., напротив д. 35, ост. "Сад Локомотив" (автобус, в сторону п. Смолино)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1618, '341', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 41, ост. "Поликлиника" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1619, '342', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Героев Танкограда, 50, ост. "ул. Потемкина" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1620, '343', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, напротив д. 18, ост. "Монтажный колледж" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1621, '344', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 142, ост. "Юридический институт"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1622, '345', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Мамина, 15, ост. "Универсам" (автобус, в сторону ул. Бажова)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1623, '346', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Салютная, 10, ост. "Сад Победы" (троллейбус, в сторону ЖБИ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1624, '347', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, напротив д. 67, ост. "ТК Кольцо" (троллейбус, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1625, '348', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 62, ост. "Публичная библиотека" (маршрутное такси, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1626, '349', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, 77, ост. "стадион Локомотив" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1627, '350', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молдавская, 11, ост. "ул. Молдавская" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1628, '351', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., напротив д. 111, ост. "18-й микрорайон" (автобус, в сторону пр. Победы)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1629, '352', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, напротив д. 378, ост. "Поликлиника" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1630, '353', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. С. Разина, 9а, ост. "Завод им. Колющенко" (трамвай № 1, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1631, '354', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 32, ост. "ул. Молодогвардейцев" (маршрутное такси, в сторону ул. Чичерина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1632, '355', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 10, ост. "п. Першино" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1633, '356', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 50 лет ВЛКСМ, 35, ост. "п. Першино" (троллейбус, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1634, '357', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 34, ост. "Трубопрокатный завод" (трамвай, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1635, '358', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, напротив д. 3, ост. "ЧТЗ" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1636, '359', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Новороссийская, напротив д. 84, ост. "ДК ЧТПЗ" (троллейбус, в сторону ул. Машиностроителей)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1637, '360', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 1б, ост. "Комсомольская пл." (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1638, '361', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 16а, ост. "Дом одежды" (троллейбус, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1639, '362', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, напротив д. 372, ост. "ул. Молдавская" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1640, '363', '9/16/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, 55а, ост. "ул. Орджоникидзе" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1641, '382', '10/5/2016', '8/24/2016', '8/23/2021', 'ООО "Родник"', 'стела', 'ул.Труда,203');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1642, '383', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 37/1, ост. "ул. Сталеваров" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1643, '384', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 54, ост. "пл. Революции" (автобус, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1644, '385', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Худякова, 12, ост. "ТК Калибр" (автобус, в сторону плотины)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1645, '386', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 83, ост. "ул. Косарева" (автобус, в сторону Свердловского пр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1646, '387', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 60а, ост. "Цирк" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1647, '388', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, 30, ост. "Областной суд" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1648, '389', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., напротив д. 35, ост. "Сад Локомотив" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1649, '390', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 2/1, ост. "Комсомольский пр." (маршрутное такси, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1650, '391', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 15/2, ост. "Авторынок" (автобус, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1651, '392', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дарвина, 12, ост. "Кондитерская фабрика" (автобус, в сторну ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1652, '393', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комарова, 133, ост. "ДК Ровесник" (автобус, в сторону Аэропорта)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1653, '394', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 1-й Пятилетки, напротив д. 10/1, ост. "ул. 1-й Пятилетки" (троллейбус, в сторону ЖБИ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1654, '395', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Героев Танкограда, 100, ост. "ул. Котина" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1655, '396', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 14, ост. "Монтажный колледж" (трамвай, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1656, '397', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Мамина, напротив д. 13, ост. "Универсам" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1657, '398', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Октябрьская, напротив д. 11, ост. "ул. Октябрьская" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1658, '399', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Российская, 71/1, ост. "пл. Павших Революционеров"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1659, '400', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Салютная, 11, ост. "Сад Победы" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1660, '401', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 46, ост. "ДС Юность" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1661, '402', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, 42, ост. "Гостиница Центральная" (автобус, в сторону ул. Воровского)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1662, '403', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Курчатова, напротив д. 19а, ост. "ул. Курчатова" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1663, '404', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 44, ост. "ул. Рылеева" (троллейбус, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1664, '405', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 59, ост. "Публичная библиотека", (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1665, '406', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 17, ост. "ул. Черкасская" (трамвай, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1666, '407', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 29, ост. "ул. Чайковского" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1667, '408', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 76, ост. "ЮУрГУ" (маршрутное такси, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1668, '409', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, напротив д. 41а, ост. "Городской сад" (трамвай, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1669, '410', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 51, ост. "ДС Юность" (маршрутное такси, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1670, '411', '10/6/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, напротив д. 138, ост. "Универсам Казачий" (маршрутное такси, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1671, '417', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 29, ост. "Центральный рынок" (троллейбус, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1672, '418', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, напротив д. 124, ост. "25-й микрорайон" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1673, '419', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 44, ост. "ул. Калинина" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1674, '420', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 115, ост. "м-н Юрюзань" (автобус, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1675, '421', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, 21, ост. "пл. Павших Революционеров" (трамвай, в сторону пр. Победы)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1676, '422', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 47, ост. "Строительное училище" (троллейбус, в сторону КБС)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1677, '423', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., напротив д. 21, ост. "Сельхозтехника" (автобус, в сторону п. Смолино)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1678, '424', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 177, ост. "пр. Победы" (маршрутное такси, в сторону ул. Кирова)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1679, '425', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. С. Разина, 9, ост. "Ж/д вокзал" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1680, '426', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 25, ост. "Дом обуви" (троллейбус, в сторону КБС)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1681, '427', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ш. Металлургов, 28, ост. "ул. Жукова" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1682, '428', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 1-й Пятилетки, 30, ост. "Трамвайное депо № 1" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1683, '429', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Героев Танкограда/ул. Бажова, 117, ост. "ул. Бажова" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1684, '430', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бажова, 38а, ост. "М-н Радуга" (троллейбус, в сторону ул. Г. Танкограда)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1685, '431', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 6, ост. "Комсомольский пр." (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1686, '432', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 2/2, ост. "Комсомольский пр." (маршрутное такси, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1687, '433', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Островского, 2, ост "ул. Островского" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1688, '434', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 51, ост. "пл. Революции" (маршрутное такси, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1689, '435', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 72, ост. "Алое поле" (маршрутное такси, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1690, '436', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сони Кривой, 83, ост. "ПО Полет" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1691, '437', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, 35, ост. "ул. Доватора" (маршрутное такси, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1692, '438', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Худякова, 19, ост. "ТК Калибр" (маршрутное такси, в сторону вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1693, '439', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 51, ост. "ул. Рылеева" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1694, '440', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 17, ост. "ул. Черкасская" (маршрутное такси, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1695, '441', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 29, ост. "ул. Красного Урала" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1696, '442', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, напротив д. 5, ост. "Оперный театр"(трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1697, '443', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ш. Металлургов, 70/1, ост. "ДК Строителей" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1698, '444', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, напротив д. 161, ост. "ДС Юность" (трамвай, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1699, '445', '10/26/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., напротив д. 51, ост. "ДС Юность" (трамвай, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1700, '448', '11/10/2016', '11/1/2016', '1/1/2022', 'ООО"АЛВИК"', 'нестанд.рекл.констр.',
        'шоссе Металлургов,41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1701, '450', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 34, ост. "Центральный рынок" (троллейбус, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1702, '451', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 40 лет Победы, 21, ост. "Универсам" (автобус, из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1703, '452', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Чичерина/ул. Братьев Кашириных, 124, ост. "25-й микрорайон" (автобус)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1704, '453', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 95, ост. "ул. Северо-Крымская" (автобус, в сторону Свердловского пр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1705, '454', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 2а, ост. "Теплотехнический институт" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1706, '455', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы/ул. Российская, 41, ост. "ул. Российская" (трамвай, в сторону Теплотехн. Института)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1707, '456', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 36, ост. "Поликлиника" (троллейбус, в сторону ул. Молодогвардейцев)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1708, '457', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 50, ост. "Библиотека им. Мамина-Сибиряка" (троллейбус, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1709, '458', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 101, ост. "п. АМЗ" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1710, '459', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., напротив д. 21, ост. "Сельхозтехника" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1711, '460', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Профессора Благих, напротив д. 79, ост. "47-й микрорайон" ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1712, '461', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бейвеля, 54а, ост. "55-й микрорайон" (автобус, в сторону Краснопольского пр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1713, '462', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 221, ост. "ул. Косарева" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1714, '463', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. С. Разина, 9, ост. "Ж/д вокзал" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1715, '464', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, 28, ост. "ДК Колющенко" (автобус, в сторону ул. Воровского)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1716, '465', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Хохрякова, 22, ост. "ул. Чоппа" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1717, '466', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 24а, ост. "Дом обуви" (троллейбус, в сторону ул. Новороссийская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1718, '467', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ш. Металлургов, 25б, ост. "Юридический техникум" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1719, '468', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, 11, ост. "м-н "Губернский" (автобус, в сторону Ленинского района)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1720, '469', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дегтярева, 48, сот. "Кафе Сказка" (автобус, в сторону ул. Румянцева)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1721, '470', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дарвина, 91, ост. "Кондитерская фабрика" (автобус, в сторону Троицкого тр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1722, '471', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Бейвеля, 27, ост. "55-й микрорайон" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1723, '472', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Академика Сахарова, напротив д. 11, ост. "ул. Ак. Сахарова" "автобус, в сторону плотины)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1724, '473', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 22, ост. "ул. Чайковского" (троллейбус, в сторону ул. Молодогвардейцев)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1725, '474', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, напротив д. 26, ост. "ДК Колющенко" (автобус, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1726, '475', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, 93, ост. "Энергетический колледж" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1727, '476', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комарова, 110, ост. "м-н Шатура" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1728, '477', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 26/1, ост. "Авторынок" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1729, '478', '11/15/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 36, ост. "Трубопрокатный завод" (троллейбус, в сторону ул. Новороссийская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1730, '480', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, напротив д. 292, ост. "ул. Красного Урала" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1731, '481', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашироиных, 107, ост. "ул. Братьев Кашириных" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1732, '482', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 25а, ост. "Цирк" (трамвай, из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1733, '483', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького/пр. Победы, 117, ост. "ул. 5-го Декабря" (трамвай, в сторону ЧТЗ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1734, '484', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 32, ост. "Кинотеатр Победа" (троллейбус, в сторону ул. Молодогвардейцев)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1735, '485', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 64, ост. "М-н Спорттовары" (троллейбус, в торону ул. Новороссийской');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1736, '486', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, напротив д. 81, ост. "п. Мебельный" (троллейбус, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1737, '487', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., 11, ост. "Рынок Привоз" (автобус, в сторону п. Смолино)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1738, '488', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Мамина, напротив д. 23, ост. "Рынок Северо-Восточный" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1739, '489', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Артиллерийская, напротив д. 107, ост. "Киргородок" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1740, '490', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Хохрякова, 12, сот. "1-я Охотничья" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1741, '491', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Профессора Благих, 79, ост. "47-й микрорайон" (автобус, в сторону ул. Молодогвардейцев)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1742, '492', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 249, ост. "ул. Тепличная" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1743, '493', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Самохина, 162, ост. "Школа" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1744, '494', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Мамина, 27, ост. "ул. Восходящая" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1745, '495', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Артиллерийская, 107, ост. "Киргородок" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1746, '496', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 3, ост. "ЧТЗ" (автобус, в сторону п. Чурилово)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1747, '497', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Чичерина, 5, ост. "18-й микрорайон" (автобус, в сторону Краснопольского пр.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1748, '498', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. С. Разина, 4, ост. "Ж/д вокзал" (маршрутное такси, в сторону Ленинского района)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1749, '499', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Зальцмана, 10, ост. "ул. Зальцмана" (автобус, в сторону ул. 1-я Эльтонская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1750, '500', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 100, ост. "ЧелГУ"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1751, '501', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы/ ул. Российская, 30, ост. "ул. Российская" (автобус, в сторону Теплотехн. Института)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1752, '502', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Марченко, 25, ост. "Швейная фабрика" (автобус, в сторону ул. Салютная)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1753, '503', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 26/12, ост. "ул. Черкасская" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1754, '504', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Косарева, 56, ост. "ул. Косарева" (автобус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1755, '505', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Комарова, 129, ост. "М-н Шатура" (автобус, в сторону Аэропорта)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1756, '506', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 1-й Пятилетки, 59, ост. "Трамвайное депо № 1" (трамвай, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1757, '507', '11/18/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Героев Танкограда/ул. 40 лет Октября, 15, ост. "ул. 1-й Пятилетки" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1758, '509', '11/21/2016', '11/15/2016', '11/14/2021', 'ООО "Урал Стрит"', 'сити-формат (в составе ост.комп.)',
        'Привокзальная площадь, ТК "Синегорье", центр. вход, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1759, '511', '11/21/2016', '11/15/2016', '11/14/2021', 'ООО "Урал Стрит"', 'сити-формат (в составе ост.комп.)',
        'ул. Цвиллинга, авт.ост."пл.Революции" (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1760, '512', '11/21/2016', '11/15/2016', '11/14/2021', 'ООО "Урал Стрит"', 'сити-формат (в составе ост.комп.)',
        'ул. Цвиллинга, авт.ост."Оперный театр" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1761, '519', '11/22/2016', '11/15/2016', '11/14/2021', 'ИП Карелин С.Ю.', 'щитовая установка',
        'ул. Энтузиастов, 26');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1762, '528', '11/25/2016', '11/15/2016', '11/14/2021', 'ООО "Омега Трак"', 'Флаги', 'ул. Игуменка, 183');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1763, '529', '11/28/2016', '11/15/2016', '11/14/2021', 'ООО "Стоматологическая клиника Максима"', 'стела',
        'ул.Бр.Кашириных,72');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1764, '531', '11/30/2016', '11/15/2016', '11/14/2021', 'ПАО "ЧЕЛИНДБАНК"', 'панель-кронштейн на собст.опоре',
        'ул. Сталеваров, 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1765, '532', '11/30/2016', '11/15/2016', '11/14/2021', 'ПАО "ЧЕЛИНДБАНК"', 'панель-кронштейн на собст.опоре',
        'ул. Я. Гашека, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1766, '535', '30.11.2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'щитовая установка',
        'ул. Горького, 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1767, '536', '30.11.2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'стела', 'пр. Ленина, 26а/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1768, '538', '01.12.2016', '11/15/2016', '11/14/2021', 'ИП Алифанов А.Е.', 'щитовая установка',
        'пл. Мопра, 10/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1769, '539', '12/2/2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'стела', 'ул. Линейная, 98, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1770, '540', '12/2/2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'стела', 'ул. Линейная, 98, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1771, '541', '12/2/2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'Флаги', 'пр. Ленина, 26А, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1772, '542', '12/2/2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'Флаги', 'пр. Ленина, 26А, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1773, '543', '12/2/2016', '11/15/2016', '11/14/2021', 'ЗАО ТД "Бовид"', 'Флаги', 'пр. Ленина, 26А, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1774, '545', '12/5/2016', '11/15/2016', '11/14/2021', 'АО "Элис"', 'щитовая установка', 'ул. Овчинникова, 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1775, '546', '12/5/2016', '11/15/2016', '11/14/2021', 'ООО"ЮРМА-СЕРВИС"', 'стела',
        'пересечение Троицкого тр. и ул. Томинская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1776, '548', '12/6/2016', '10/12/2016', '10/11/2021', 'ООО "ОМЕГА"', 'Флаги', 'ул. Игуменка, 181');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1777, '549', '12/6/2016', '11/15/2016', '11/14/2021', 'ЗАО КХП "ЗЛАК"', 'щитовая установка',
        'пересечение Троицкого тр. и а/д Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1778, '550', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 99а, ост. "п. Бабушкина" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1779, '551', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 40 лет Победы/ул. Братьев Кашириных, 110, ост. "24-й микрорайон" (автобус, в сторону пр. Победы)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1780, '552', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 23, ост. "Цирк" (автобус, в сторону Теплотех. Института)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1781, '553', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Российская, 40, ост. "Плавательный бассейн" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1782, '554', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 30, ост. "Торговый центр" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1783, '555', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Каслинская, 18, ост. "Каслинский рынок" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1784, '556', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 82, ост. "8-й микрорайон" (маршрутное такси, в сторону ул. Чичерина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1785, '557', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Героев Танкограда, 40, ост. "пр. Победы" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1786, '558', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр., 23а, ост. "пр. Победы" (троллейбус, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1787, '559', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького/пр. Победы, 115, ост. "ул. 5-го Декабря" (трамвай, в сторону С-З');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1788, '560', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Энгельса/ул. Труда, 179, ост. "ул. Труда" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1789, '561', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 28, ост. "ул. Красного Урала" (троллейбус, в сторону ул. Молодогвардейцев)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1790, '562', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Свердловский пр./ул. Каслинская, 64, ост. "Торговый центр" (троллейбус, из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1791, '563', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Каслинская, 5а, ост. "Каслинский рынок" (трамвай, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1792, '564', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 16, ост. "Дом одежды" (трамвай, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1793, '565', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Блюхера, напротив д. 93, ост. "Энергетический колледж" (троллейбус, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1794, '566', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., напротив д. 70, ост. "п. Смолино" (автобус, в сторону п. Исаково)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1795, '567', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Пушкина, 64, ост. "Кинотеатр Пушкина" (троллейбус, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1796, '568', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Румянцева, 2а, ост. "60 лет Октября" (автобус, в сторону ш. Металлургов)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1797, '569', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Салавата Юлаева, 3, ост. "С. Юлаева" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1798, '570', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 106, ост. "Ж/д институт" (троллейбус, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1799, '571', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 26, рст. "ДК Восток" (троллейбус, в сторону ш. Металлургов)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1800, '572', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Тимирязева, 29, ост. "Кинотеатр Пушкина" (троллейбус, в сторону центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1801, '573', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, 187, ост. "Зоопарк" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1802, '574', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Центральная, напротив д. 3в, ост. "п. Шершни"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1803, '575', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 3, ост. "ДЦ Импульс" (троллейбус, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1804, '576', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 32, ост. "Торговый центр" (автобус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1805, '577', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 2-я Эльтонская, напротив д. 43, ост. "ул. 2-я Эльтонская" (автобус, в сторону ул. 1-я Эльтонская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1806, '578', '12/9/2016', '12/22/2015', '12/21/2020', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Энгельса, 83, ост. "ул. Курчатова" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1807, '579', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 149, ост. "ул. Плеханова" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1808, '580', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'Комсомольский пр., 69, ост. "ул. Солнечная" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1809, '581', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Чичерина, 38В, ост. "ул. 250 лет Челябинску" (маршрутное такси, в сторону ул. Братьев Кашириных)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1810, '582', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Чайковского, напротив д. 89, ост. "Обувная фабрика" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1811, '583', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Энгельса, 44г, ост. "ул. Курчатова", маршрутное такси, в сторону ул. Худякова)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1812, '584', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 87, ост. "ПКиО им. Гагарина" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1813, '585', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 159, ост. "ул. Евтеева" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1814, '586', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, напротив д. 40, ост. "ул. Доватора" (автобус, в сторону плотины)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1815, '587', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 287, ост. "Больница скорой помощи" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1816, '588', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Труда, напротив д. 203, ост. "ТРК Родник" (троллейбус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1817, '589', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. 250 лет Челябинску, 17, ост. "ул. 250 лет Челябинску"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1818, '590', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Кирова, 1Б, ост. "Теплотехнический институт" (трамвай, в сторону С-З, ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1819, '591', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 103, ост. "Каширинский рынок" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1820, '592', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 43, ост. "Педучилище" (троллейбус, из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1821, '593', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 124, ост. "м-н Юрюзань" (автобус, в сторону Теплотех. Института)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1822, '594', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дарвина, 2В, ост. "ЦРМ" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1823, '595', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Доватора, напротив д. 1б, ост. "м-н Губернский" (автобус, в сторону ул. Воровского)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1824, '596', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 28, ост. "ул. Правдухина" (трамвай, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1825, '597', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Центральная, 3в, ост. "п. Шершни" (маршрутное такси, в сторону плотины)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1826, '598', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 3, ост. "ДЦ Импульс" (трамвай, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1827, '599', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Чичерина, 2, ост. "ЖК Александровский" (автобус, в сторону центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1828, '600', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 147, ост. "ул. Академика Макеева" (автобус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1829, '601', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Б. Хмельницкого, 24, ост. "Общежитие" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1830, '602', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Жукова, 5а, ост. "Кинотеатр Россия" (троллейбус, в сторону ул. Сталеваров)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1831, '603', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 40, ост. "ДК Металлургов" (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1832, '604', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Павелецкая, 2-я, 18/1, ост. "ЧМК" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1833, '605', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 22, ост. "ЗЭМ" (троллейбус, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1834, '606', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 20, ост. "Комсомольская пл." (троллейбус, в сторону ПКиО)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1835, '607', '12/19/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 56, ост. "Строительное училище" (троллейбус, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1836, '624', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'сити-формат', 'ул.Кирова, 82');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1837, '627', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'пр.Победы,164');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1838, '628', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.С.Юлаева,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1839, '629', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.Мастеровая,8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1840, '631', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'пересеч.ул.Чичерина и Комсомольского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1841, '633', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.Доватора,перед виадуком в Ленинский р-н,в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1842, '634', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.Дзержинского,119');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1843, '636', '12/23/2016', '12/20/2016', '12/19/2021', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.Гагарина,29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1844, '641', '12/26/2016', '12/20/2016', '12/19/2021', 'ООО "Висма"', 'стела', 'Копейское шоссе,35Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1845, '642', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Свободы, 155/2, ост. "Академическая" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1846, '643', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 21В, ост. "Агентство воздушных сообщений" (троллейбус, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1847, '644', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, напротив д. 101, ост. "п. Бабушкина"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1848, '645', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, напротив д. 48а, ост. "пр. Победы" (троллейбус, из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1849, '646', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 289а, ост. "Университет" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1850, '647', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Горького, 38, ост. "ДК Смена" (трамвай, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1851, '648', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'Троицкий тр., 70, ост. "п. Смолино" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1852, '649', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Братьев Кашириных, 161, ост. "ул. Академика Королева" (автобус, в сторону С-З)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1853, '650', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дарвина, 10, ост. "Кинотеатр Маяк" (автобус, в сторону ул. Блюхера)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1854, '651', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Победы, 127, ост. "Юридический институт" (автобус в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1855, '652', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 37/1, ост. "ул. Сталеваров" (автобус, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1856, '653', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Румянцева, 28б, ост. "Больница ЧМК" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1857, '654', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Б. Хмельницкого, напротив д. 35, ост. "ул. Б. Хмельницкого (автобус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1858, '655', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 31, ост. "Кинотеатр Аврора" (маршрутное такси, в сторону КБС)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1859, '656', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Дзержинского, 102, ост. "Кинотеатр Аврора" (маршрутное такси, в сторону ж/д вокзала)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1860, '657', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, 85, ост. "Обл. больница" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1861, '658', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Марченко, 13, ост. "Магазин № 28" (автобус, в сторону С-В)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1862, '659', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Сталеваров, 66/3, ост. "ул. Сталеваров" (трамвай, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1863, '660', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Павелецкая, 2-я, 14, ост. "ЧМК" (трамвай, в сторону завода Теплоприбор)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1864, '661', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'пр. Ленина, 7, ост. "Театр ЧТЗ" (троллейбус, в сторону ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1865, '662', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Краснознаменная, 28, ост. "ул. Краснознаменная" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1866, '663', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 21к1, ост. "ЗЭМ" (троллейбус, в сторону Копейского ш.)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1867, '664', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Молодогвардейцев, 24, ост. "ул. Куйбышева" (маршрутное такси, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1868, '665', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Танкистов, 41, ост. "п. Первоозерный" (троллейбус, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1869, '666', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Воровского, 64, ост. "Медакадемия" (троллейбус, в сторону АМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1870, '667', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Черкасская, 15/2, ост. "Авторынок" (трамвай, в сторону ЧМЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1871, '668', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Машиностроителей, 10, ост. "ул. Энергетиков" (троллейбус, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1872, '669', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Гагарина, 40, ост. "Управление соцзащиты населения" (троллейбус, в сторону ул. Новороссийской)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1873, '670', '12/27/2016', '4/30/2016', '4/29/2021', 'ИП Сидоров А.Н.', 'Стенд',
        'ул. Цвиллинга, 61, ост. "ул. Евтеева" (трамвай, в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1874, '671', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'Комсомольский пр., 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1875, '672', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул.Труда,203 (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1876, '673', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Воровского, 85');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1877, '674', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Энгельса, 83');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1878, '675', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'Комсомольский пр., 28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1879, '676', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Труда,168');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1880, '677', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Салавата Юлаева, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1881, '678', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Труда,203 (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1882, '679', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'Комсомольский пр., 111');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1883, '680', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Труда,183');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1884, '681', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Чичерина, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1885, '682', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'Комсомольский пр., 103');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1886, '683', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Труда,187');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1887, '684', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Чичерина, 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1888, '685', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Труда,166');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1889, '686', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Бр.Кашириных, 66');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1890, '687', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Кирова, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1891, '688', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Марченко, 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1892, '689', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул.Комарова/ ул. Салютная,2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1893, '690', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул.Бр.Кашириных, ост. "ул. Северо-Крымская" (из центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1894, '691', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Марченко, 18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1895, '692', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'Свердловский пр., 23А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1896, '693', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Энгельса, 63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1897, '694', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Бр.Кашириных,152');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1898, '695', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Молодогвардейцев,62');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1899, '696', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'Свердловский пр.,28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1900, '697', '12/28/2016', '12/20/2016', '12/19/2021', 'ИП Сарсенов К.Н.', 'Скамья', 'ул.Труда,179');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1901, '1', '1/10/2017', '12/20/2016', '12/19/2021', 'АО "Элис"', 'щитовая установка', 'ул.Свободы,44');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1902, '2', '1/11/2017', '12/20/2016', '12/19/2021', 'ИП Ведерников Д.Е.', 'панель-кронштейн',
        'пересеч.ул.Мастеровая и ул.Теннисная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1903, '5', '1/11/2017', '12/20/2016', '12/19/2021', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул. Кирова, напротив д. 82,констр.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1904, '6', '1/11/2017', '12/20/2016', '12/19/2021', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова, 86, поз. 1, констр.5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1905, '9', '1/11/2017', '12/20/2016', '12/19/2021', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,напротив д. 86,констр.8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1906, '10', '1/11/2017', '12/20/2016', '12/19/2021', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,86,констр.9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1907, '13', '1/11/2017', '12/20/2016', '12/19/2021', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул. Кирова, 143,констр.12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1908, '55', '3/22/2017', '4/14/2017', '4/13/2022', 'ООО "Энерготехника"', 'нестанд.рекл.констр.',
        'ул.Российская,154');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1909, '57', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'Флаги',
        'ул.Молодогвардейцев,7, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1910, '58', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'Флаги',
        'ул.Молодогвардейцев,7, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1911, '59', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'Флаги',
        'ул.Молодогвардейцев,7, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1912, '60', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'Флаги',
        'ул.Молодогвардейцев,7, поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1913, '61', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'Флаги',
        'ул.Молодогвардейцев,7, поз. 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1914, '62', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'Флаги',
        'ул.Молодогвардейцев,7, поз. 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1915, '63', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1916, '64', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1917, '65', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1918, '66', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1919, '67', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1920, '68', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1921, '69', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1922, '70', '4/3/2017', '11/16/2016', '11/15/2021', 'ООО ТК "СТРЕЛА"', 'нестанд.рекл.констр.',
        'ул.Молодогвардейцев,7, поз. 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1923, '76', '4/21/2017', '4/18/2017', '4/17/2022', 'ООО "АЛВИК"', 'панель-кронштейн на собст.опоре',
        'пр. Победы, 287');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1924, '77', '4/21/2017', '4/18/2017', '4/17/2022', 'ООО "Омега Моторс"', 'стела', 'ул. Чичерина,52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1925, '78', '4/24/2017', '4/18/2017', '4/17/2022', 'ИП Садыков Ф.М.', 'стела', 'ул. Ш. Руставелли, 25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1926, '79', '4/25/2017', '4/18/2017', '4/17/2022', 'ООО "Автодоркомплект"', 'стела',
        'Копейское шоссе, ост. "Сад Энергетик"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1927, '80', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1928, '81', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1929, '82', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1930, '83', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1931, '84', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1932, '85', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1933, '86', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1934, '87', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1935, '88', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1936, '89', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1937, '90', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1938, '91', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1939, '92', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1940, '93', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1941, '94', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1942, '95', '4/25/2017', '4/18/2017', '4/17/2022', 'ИП Сарсенов К.Н.', 'Скамья',
        'ул. Академика Королева, 3, поз. 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1943, '96', '4/26/2017', '7/1/2016', '6/30/2021', 'ООО "Техно-Моторс"', 'Флаги', 'ул.Бр.Кашириных,129Д');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1944, '97', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Уфимский тр., 1868+300м');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1945, '98', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'ул. Курчатова, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1946, '99', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Бр. Кашириных, 114');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1947, '100', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Комсомольский пр., 93');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1948, '101', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Свердловский пр., 62А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1949, '102', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'пересечение Комсомольского пр. и ул. Чайковского, 15/2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1950, '103', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Бр. Кашириных, 132');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1951, '104', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'ул. Воровского, 64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1952, '106', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Бр. Кашириных, 100');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1953, '107', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'ул. Дзержинского,104');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1954, '108', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Бр. Кашириных, 141');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1955, '109', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Молодогвардейцев, 40');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1956, '110', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'ул. Дарвина, 113');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1957, '111', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Комсомольский пр., 94');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1958, '112', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Бр. Кашириных, 134');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1959, '113', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Комсомольский пр., 24');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1960, '114', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'ул.Гагарина, 44');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1961, '115', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Блюхера, напротив д. 69');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1962, '116', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Комсомольский пр., 50');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1963, '117', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'ул.Доватора, 22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1964, '118', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'ул. Молодогвардейцев,56');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1965, '119', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка',
        'Комсомольский пр., 105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1966, '120', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "Ректол"', 'щитовая установка', 'пр. Победы, 390');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1967, '122', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "СПП"', 'стела', 'Комсомольский пр., 34, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1968, '123', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "СПП"', 'стела', 'Комсомольский пр., 34, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1969, '124', '5/2/2017', '4/18/2017', '4/17/2022', 'ООО "СПП"', 'стела', 'Комсомольский пр., 34, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1970, '125', '5/4/2017', '4/18/2017', '4/17/2022', 'ООО "Корнет"', 'щитовая установка', 'ш. Металлургов, 21п');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1971, '126', '5/4/2017', '4/18/2017', '4/17/2022', 'ООО "Индустриальный парк "Станкомаш"', 'щитовая установка',
        'ул. Енисейская, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1972, '127', '5/5/2017', '4/18/2017', '4/17/2022', 'ООО "ЭкоСити"', 'стела',
        'Западное шоссе, 100м до поворота на пос. Западный');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1973, '128', '5/10/2017', '4/18/2017', '4/17/2027', 'МУП "Дворец спорта "Юность"', 'Экран на здании',
        'Свердловский пр., 51 (малая арена, вид на ул. Труда)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1974, '129', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'ул. Бажова, 35');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1975, '130', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'дорога из Аэропорта, 750 м от площади аэропорта (в город)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1976, '131', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'Комсомольский пр., 29');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1977, '132', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'пр. Победы, 289');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1978, '133', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'Комсомольский пр., 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1979, '134', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных (в центр) и ул. Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1980, '135', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'ул. Российская, 275');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1981, '136', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'автодорога Меридиан/  ул. Гражданская, напротив д.25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1982, '137', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'пр. Победы, 319');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1983, '138', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'ул. Курчатова,1А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1984, '139', '5/10/2017', '4/18/2017', '4/17/2022', 'ООО "Армада Сервис"', 'щитовая установка',
        'ул. Героев Танкограда, 65');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1985, '144', '5/26/2017', '4/1/2017', '3/31/2022', 'ООО "ТТМ-1"', 'нестанд.рекл.констр.', 'ул. Чичерина, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1986, '145', '5/26/2017', '4/1/2017', '3/31/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина, 52, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1987, '146', '5/26/2017', '4/1/2017', '3/31/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина, 52, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1988, '147', '5/26/2017', '4/1/2017', '3/31/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина, 52, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1989, '148', '5/26/2017', '4/1/2017', '3/31/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина, 52, поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1990, '149', '5/26/2017', '3/1/2017', '2/28/2022', 'ООО"Сити Моторс"', 'стела', 'ул. Братская, напротив д. 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1991, '152', '5/30/2017', '4/20/2017', '4/30/2021', 'Валерьянов А.С.', 'щитовая установка',
        'ул. Рабоче-Колхозная, 34,36');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1992, '162', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Ректол"', 'щитовая установка',
        'пересечение Комсомольского пр. и ул. Каслинская, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1993, '163', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Ректол"', 'щитовая установка',
        'Троицкий тр., 54/1, в центр, до ост. "ул.Потребительская"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1994, '164', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Бухус"', 'стела',
        'Копейское шоссе,ост. "Сад Энергетик"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1995, '165', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина,52, поз. 5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1996, '166', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина,52, поз. 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1997, '167', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина,52, поз. 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1998, '168', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "ТТМ-1"', 'Флаги', 'ул. Чичерина,52, поз. 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (1999, '169', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Элефант-Проспект"', 'щитовая установка',
        'ул. Зальцмана, 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2000, '170', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Фрагмент"', 'Флаги', 'Копейское шоссе,33А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2001, '171', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Фрагмент"', 'стела', 'Копейское шоссе,33А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2002, '176', '7/10/2017', '6/27/2017', '6/26/2022', 'ИП Сидоров А.Н.', 'стенд',
        'ул.60 лет Октября,2Б, ост. "ЧМК" (маршрутное такси, в сторону центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2003, '177', '7/10/2017', '6/27/2017', '6/26/2022', 'ИП Сидоров А.Н.', 'стенд',
        'ул.Блюхера/ул.Доватора, 23, ост. "ул. Доватора" (трамвай, в сторону центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2004, '178', '7/10/2017', '6/27/2017', '6/26/2022', 'ИП Сидоров А.Н.', 'стенд',
        'ул.Молодогвардейцев, 2а/1, ост. "Автоцентр" (автобус, в сторону центра)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2005, '179', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Универмаг "Детский мир"',
        'панель-кронштейн на собст.опоре', 'ул.Коммуны,58,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2006, '180', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Универмаг "Детский мир"',
        'панель-кронштейн на собст.опоре', 'ул.Коммуны,58,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2007, '181', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Универмаг "Детский мир"',
        'панель-кронштейн на собст.опоре', 'ул.Коммуны,60,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2008, '182', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Универмаг "Детский мир"',
        'панель-кронштейн на собст.опоре', 'ул.Коммуны,60,поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2009, '183', '7/10/2017', '6/27/2017', '6/26/2022', 'ООО "Универмаг "Детский мир"',
        'панель-кронштейн на собст.опоре', 'ул.Коммуны,60,поз.5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2010, '184', '7/13/2017', '6/27/2017', '6/26/2022', 'ООО"Аура"', 'панель-кронштейн', 'пр.Победы, 356');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2011, '201', '9/11/2017', '9/5/2017', '9/4/2022', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Худякова, 11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2012, '202', '9/11/2017', '9/5/2017', '9/4/2022', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Марченко, 22');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2013, '203', '9/11/2017', '9/5/2017', '9/4/2022', 'ИП Сарсенов К.Н.', 'Скамья', 'ул. Худякова, 19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2014, '204', '9/13/2017', '9/5/2017', '9/4/2022', 'ИП Смирнова Т.С.', 'стела', 'ул.Хлебозаводская,15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2015, '205', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,напротив д. 92');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2016, '206', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн', 'ул.Кирова,96');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2017, '207', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова, напротив д.100');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2018, '208', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,102');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2019, '209', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,напротив д.104,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2020, '210', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'пересеч.ул.Кирова (в центр) и ул.Коммуны');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2021, '211', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,163');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2022, '212', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,110');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2023, '213', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,167');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2024, '214', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,112');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2025, '215', '9/13/2017', '9/5/2017', '9/4/2022', 'ООО "Элефант-Проспект"', 'панель-кронштейн',
        'ул.Кирова,напротив д.114');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2026, '216', '9/18/2017', '9/5/2017', '9/4/2022', 'ООО "УралТрансСервис"', 'щитовая установка',
        'ул. Героев Танкограда, 71п');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2027, '218', '9/22/2017', '9/5/2017', '9/4/2022', 'ООО "ЦИР Южный Урал"', 'сити-формат (в составе ост.комп.)',
        'пр. Ленина, ост. "Публичная библиотека", в сторону пл. Революции');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2028, '219', '9/22/2017', '9/5/2017', '9/4/2022', 'ООО "ЦИР Южный Урал"', 'сити-формат (в составе ост.комп.)',
        'пр. Ленина, ост. "Публичная библиотека", в сторону ул. Красная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2029, '220', '9/22/2017', '9/5/2017', '9/4/2022', 'ПАО "Сбербанк"', 'Флаги', 'ул.Энтузиастов,9а,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2030, '221', '9/22/2017', '9/5/2017', '9/4/2022', 'ПАО "Сбербанк"', 'Флаги', 'ул.Энтузиастов,9а,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2031, '222', '9/22/2017', '9/5/2017', '9/4/2022', 'ПАО "Сбербанк"', 'Флаги', 'ул.Энтузиастов,9а,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2032, '223', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,86,конец дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2033, '224', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,86, начало дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2034, '225', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'пересечение пр. Ленина и Свердловского пр., до перекрестка из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2035, '226', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'пересечение ул.Кирова, 90 и ул.К.Маркса');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2036, '227', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'пересечение ул.Кирова,147 и ул.К.Маркса,109');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2037, '228', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат', 'ул.Кирова, 161б/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2038, '229', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,161а,конец дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2039, '230', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'пересечение пр. Ленина,85 и ул.Тернопольская, до перекрестка в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2040, '231', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'пересечение ул.Кирова,161/а/1 и ул.Коммуны');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2041, '232', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат', 'ул.Кирова,163');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2042, '233', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,167,начало дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2043, '234', '9/25/2017', '9/5/2017', '9/4/2022', 'ООО "РФ"Армада Аутдор"', 'сити-формат',
        'ул.Кирова,167, конец дома');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2044, '265', '11/7/2017', '9/25/2017', '9/24/2022', 'ИП Яцко М.Н.', 'нестанд.рекл.констр.',
        'ул.Кирова,161А, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2045, '266', '11/7/2017', '9/25/2017', '9/24/2022', 'ИП Яцко М.Н.', 'нестанд.рекл.констр.',
        'ул.Кирова,161А, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2046, '267', '11/9/2017', '9/5/2017', '9/4/2022', 'ООО"Башнефть-Розница"', 'стела',
        'пересечение пр.Победы и ул.Горького');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2047, '268', '11/9/2017', '9/5/2017', '9/4/2022', 'ООО"Башнефть-Розница"', 'стела',
        'пересечение пр.Победы и ул.Чайковского');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2048, '269', '11/15/2017', '9/5/2017', '9/4/2027', 'ИП Ереклинцева Т.А.', 'Экран',
        'пересечение ул. Университетская Набережная и ул. Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2049, '270', '11/15/2017', '11/14/2017', '11/13/2022', 'ООО "Урал Стрит"', 'щитовая установка',
        'пересеч.ул.Г.Танкограда и ул.1-ой Пятилетки,17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2050, '272', '11/15/2017', '11/14/2017', '11/13/2022', 'ООО "Урал Стрит"', 'щитовая установка',
        'Комсомольский пр.,104');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2051, '273', '11/15/2017', '11/14/2017', '11/13/2022', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.Черкасская,напротив д.6,на разделит.газоне');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2052, '274', '11/15/2017', '11/14/2017', '11/13/2022', 'ООО "Урал Стрит"', 'щитовая установка', 'пр.Победы,86');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2053, '275', '11/15/2017', '11/14/2017', '11/13/2022', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул.Черкасская,12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2054, '276', '11/21/2017', '11/14/2017', '11/13/2022', 'АО "Элис"', 'щитовая установка', 'ул.Блюхера,101/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2055, '277', '11/21/2017', '11/14/2017', '11/13/2022', 'ООО "Премьер Инвест"', 'щитовая установка',
        'пересчение Троицкого тр.,15 и ул. Потребитеская, за перекрестком из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2056, '278', '11/21/2017', '11/14/2017', '11/13/2022', 'ООО "Премьер Инвест"', 'щитовая установка',
        'ул.Бр. Кашириных, 109/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2057, '279', '11/21/2017', '11/14/2017', '11/13/2022', 'ООО "Премьер Инвест"', 'щитовая установка',
        'Свердловский тр.,8/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2058, '280', '11/22/2017', '11/14/2017', '11/13/2022', 'ИП Бажанов В.Н.', 'стела', ' ул.Свободы,100');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2059, '281', '11/22/2017', '11/14/2017', '11/13/2022', 'ООО Торговый дом "АТБ № 3"', 'щитовая установка',
        'пересеч.ул.Блюхера и ул.Новоэлеваторная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2060, '282', '11/23/2017', '11/14/2017', '11/13/2022', 'ИП Голиков В.В.', 'щитовая установка',
        'Троицкий тр., 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2061, '283', '11/23/2017', '11/14/2017', '11/13/2022', 'ООО "Компания "Чистая вода"', 'сити-формат',
        'пересеч.Свердловского пр.,84 и ул.К.Либкнехта');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2062, '284', '11/23/2017', '11/14/2017', '11/13/2022', 'ООО "Компания "Чистая вода"', 'панель-кронштейн',
        'ул.К.Либкнехта,9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2063, '285', '11/23/2017', '11/14/2017', '11/13/2022', 'ИП Шукуров В.М.', 'щитовая установка',
        'Троицкий тр., 46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2064, '288', '11/28/2017', '11/14/2017', '11/13/2022', 'ЗАО "ИНСИ"', 'щитовая установка',
        'Троицкий тр., напротив д.9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2065, '289', '11/28/2017', '11/14/2017', '11/13/2022', 'ООО "АВС-Моторс"', 'Флаги',
        ' Копейское шоссе,52, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2066, '290', '11/28/2017', '11/14/2017', '11/13/2022', 'ООО "АВС-Моторс"', 'Флаги',
        ' Копейское шоссе,52, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2067, '291', '11/28/2017', '11/14/2017', '11/13/2022', 'ООО "АВС-Моторс"', 'Флаги',
        ' Копейское шоссе,52, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2068, '292', '11/30/2017', '11/14/2017', '11/13/2022', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        'ул.Труда,3Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2069, '293', '11/30/2017', '11/14/2017', '11/13/2022', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        'ул.Блюхера,49');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2070, '294', '11/30/2017', '11/14/2017', '11/13/2022', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        ' ул.Блюхера,150м до остановки "АМЗ" (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2071, '295', '11/30/2017', '11/14/2017', '11/13/2022', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        ' Бродокалмакский тр.,150м от ост,"Тракторосад-1"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2072, '296', '11/30/2017', '11/14/2017', '11/13/2022', 'ООО "Гэллэри Сервис"', 'щитовая установка',
        'Свердловский тр., 300 м до поворота на пос. Шагол');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2073, '297', '12/6/2017', '9/26/2017', '9/25/2022', 'ИП Репина О.Г.', 'нестанд.рекл.констр.', 'ул.Барбюса,2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2074, '300', '12/28/2017', '12/23/2017', '12/22/2022', 'ИП Брусенский М.А.', 'стела', 'ул.40 лет Победы,33');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2075, '1', '1/9/2018', '9/25/2017', '8/30/2022', 'ИП Яцко М.Н.', 'нестанд.рекл.констр.', 'ул.Кирова,143');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2076, '2', '1/11/2018', '12/23/2017', '12/22/2022', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Бр. Кашириных (из центра) и ул. Спорта');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2077, '3', '1/11/2018', '12/23/2017', '12/22/2022', 'ООО "РФ "Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., выезд с ЧМЗ, справа в центр');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2078, '4', '1/11/2018', '12/23/2017', '12/22/2022', 'ООО "РФ "Армада Аутдор"', 'щитовая установка',
        'Свердловский тр., въезд на ЧМЗ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2079, '5', '1/11/2018', '12/23/2017', '12/22/2022', 'ООО "РФ "Армада Аутдор"', 'сити-борд', 'ул.Кирова,25');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2080, '22', '1/16/2018', '12/23/2017', '12/22/2022', 'ИП Сарсенов К.Н.', 'щитовая установка',
        'Троицкий тр., 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2081, '23', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'ул. Чичерина,30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2082, '24', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'ул. Энгельса, 49');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2083, '25', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'ул. Энтузиастов,2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2084, '26', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'ул. Энтузиастов,18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2085, '27', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'Свердловский тр.,16Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2086, '28', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'Троицкий тр.,13/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2087, '29', '1/19/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'Уфимский тр., 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2088, '30', '1/22/2018', '12/23/2017', '12/22/2022', 'ЗАО ТД "Бовид"', 'щитовая установка', 'ул.Мамина,7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2089, '31', '1/22/2018', '12/23/2017', '12/22/2022', 'ЗАО ТД "Бовид"', 'щитовая установка',
        'ул.Хохрякова,30 (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2090, '32', '1/22/2018', '12/23/2017', '12/22/2022', 'АО "Элис"', 'щитовая установка',
        'ул.Первой Пятилетки,57');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2091, '38', '1/22/2018', '12/23/2017', '12/22/2022', 'ООО "ЦЭР "Визит к стоматологу"', 'панель-кронштейн',
        'пр. Ленина,80');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2092, '39', '1/22/2018', '12/23/2017', '12/22/2022', 'ООО "ЦЭР "Визит к стоматологу"', 'панель-кронштейн',
        'ул. Лесопарковая, 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2093, '46', '1/24/2018', '12/5/2017', '12/4/2022', 'ИП Миронова И.Л.', 'сити-формат', 'Свердловский пр.,63');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2094, '48', '2/1/2018', '11/14/2017', '11/13/2022', 'ООО "ЛУКОЙЛ-Уралнефтепродукт"', 'стела',
        'ул.Воровского,28/1 ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2095, '49', '2/1/2018', '11/14/2017', '11/13/2022', 'ООО "ЛУКОЙЛ-Уралнефтепродукт"', 'Флаги',
        ' пр.Ленина, 21,БД "Спиридонов" (АЗС № 422)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2096, '50', '2/1/2018', '11/14/2017', '11/13/2022', 'ООО "ЛУКОЙЛ-Уралнефтепродукт"', 'Флаги',
        ' пр.Ленина, БД "Видгоф" (АЗС 424)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2097, '51', '2/1/2018', '11/14/2017', '11/13/2022', 'ООО "ЛУКОЙЛ-Уралнефтепродукт"', 'Флаги',
        ' пересеч.ул.Чайковского и Комсомольского пр. (АЗС 432)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2098, '52', '2/1/2018', '11/14/2017', '11/13/2022', 'ООО "ЛУКОЙЛ-Уралнефтепродукт"', 'Флаги',
        ' пересеч.ул.Дачная,37/ ул.Дубравная (АЗС № 435)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2099, '53', '2/7/2018', '12/23/2017', '12/22/2022', 'ООО "Лукойл-Уралнефтепродукт"', 'Флаги',
        ' пересеч. Свердловского пр. и ул. Воровского (АЗС №  74412)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2100, '54', '2/9/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'ул. Энгельса, 46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2101, '55', '2/9/2018', '12/23/2017', '12/22/2022', 'ООО "Вернисаж Медиа"', 'щитовая установка',
        'Копейское шоссе,39А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2102, '57', '2/13/2018', '1/18/2018', '1/17/2023', 'ООО "СПП"', 'нестанд.рекл.констр.',
        'Комсомольский пр.,34, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2103, '58', '2/13/2018', '1/18/2018', '1/17/2023', 'ООО "СПП"', 'нестанд.рекл.констр.',
        'Комсомольский пр.,34, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2104, '59', '2/13/2018', '1/18/2018', '1/17/2023', 'ООО "СПП"', 'нестанд.рекл.констр.',
        'Комсомольский пр.,34, поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2105, '60', '2/13/2018', '1/18/2018', '1/17/2023', 'ООО "СПП"', 'нестанд.рекл.констр.',
        'Комсомольский пр.,34, поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2106, '63', '2/16/2018', '3/15/2018', '12/1/2022', 'ООО "Сатурн Урал"', 'нестанд.рекл.констр.',
        'Свердловский тр.,3В');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2107, '65', '2/16/2018', '3/1/2018', '2/28/2023', 'ООО "Региональная сеть питания"', 'панель-кронштейн',
        'ул.Воровского,6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2108, '67', '2/19/2018', '12/1/2017', '12/31/2021', 'АО "Синема Парк"', 'нестанд.рекл.констр.',
        'ул.Артиллерийская,136, у входа со стороны ул. Артиллерийской');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2109, '68', '2/19/2018', '2/1/2018', '1/11/2028', 'ООО "Урал Стрит"', 'Экран', 'Комсомольский пр.,118');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2110, '69', '2/28/2018', '4/1/2018', '12/31/2027', 'ООО "Амиго Медиа"', 'Экран', 'пр.Ленина,55а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2111, '75', '3/28/2018', '3/1/2018', '12/31/2021', 'АО "СИНЕМА ПАРК"', 'нестанд.рекл.констр.',
        'ул.Артиллерийская,136, у входа со стороны а/д Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2112, '76', '4/9/2018', '5/3/2018', '3/1/2022', 'ООО "Грандико Медиа"', 'сити-формат',
        'ул.Молодогвардейцев,7 (главный вход комплекса напротив ул.Молодогвардейцев, 7А)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2113, '77', '4/9/2018', '5/3/2018', '3/1/2022', 'ООО "Грандико Медиа"', 'сити-формат',
        'ул.Молодогвардейцев,7 (вход напротив ул.Солнечная)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2114, '80', '4/19/2018', '10/25/2017', '10/25/2022', 'ООО "Автотехснаб-Гарант"', 'Флаги', 'ул.Механическая,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2115, '81', '4/19/2018', '10/14/2017', '10/14/2022', 'ООО "Автотехснаб-Гарант"', 'стела', 'ул.Механическая,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2116, '83', '5/21/2018', '1/11/2018', '1/11/2023', 'ООО "ОМЕГА"', 'стела', 'ул.Игуменка,181, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2117, '84', '5/21/2018', '1/11/2018', '1/11/2023', 'ООО "ОМЕГА"', 'стела', 'ул.Игуменка,181, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2118, '85', '5/21/2018', '5/12/2018', '5/11/2028', 'ООО "СТРИТРАДИО"', 'Экран', 'площадь Революции,1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2119, '86', '5/22/2018', '5/12/2018', '5/11/2023', 'ООО "Единый юридический центр"', 'сити-формат',
        'ул.Молодогвардейцев, 34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2120, '87', '5/22/2018', '5/12/2018', '5/11/2023', 'ИП Голиков В.В.', 'Флаги', 'ул.Бр.Кашириных,129б, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2121, '88', '5/22/2018', '5/12/2018', '5/11/2023', 'ИП Голиков В.В.', 'Флаги', 'ул.Бр.Кашириных,129б, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2122, '89', '5/22/2018', '5/12/2018', '5/11/2023', 'ИП Голиков В.В.', 'Флаги', 'ул.Бр.Кашириных,129б, поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2123, '90', '5/22/2018', '5/12/2018', '5/11/2023', 'ИП Голиков В.В.', 'Флаги', 'ул.Бр.Кашириных,129б, поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2124, '91', '5/25/2018', '5/12/2018', '5/11/2023', 'ООО "Солард"', 'панель-кронштейн',
        'ул.Воровского,38б, опора б/н');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2125, '92', '5/25/2018', '5/12/2018', '5/11/2023', 'ООО "Солард"', 'панель-кронштейн',
        'ул.Худякова,13 (на уровне середины дома), опора б/н');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2126, '93', '5/25/2018', '5/12/2018', '5/11/2023', 'ООО "Солард"', 'панель-кронштейн',
        'ул.Доватора,42, опора б/н');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2127, '94', '5/25/2018', '5/12/2018', '5/11/2023', 'ООО "Солард"', 'панель-кронштейн',
        'ул.Худякова,4, опора б/н');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2128, '95', '5/25/2018', '5/12/2018', '5/11/2023', 'ООО "Солард"', 'панель-кронштейн',
        'пересечение ул.Воровского,46 и ул.Образцова, опора б/н');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2129, '96', '5/24/2018', '5/24/2018', '5/23/2028', 'ООО"Урал Стрит"', 'Экран',
        'пересечение Свердловского пр. и ул.Труда');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2130, '97', '5/24/2018', '5/24/2018', '5/23/2028', 'ООО"Урал Стрит"', 'Экран',
        'пересечение ул.Труда (из центра) и ул. Энгельса, 21/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2131, '98', '5/24/2018', '5/24/2018', '5/23/2028', 'ООО"Урал Стрит"', 'Экран',
        'пересечение ул.Бр. Кашириных и ул. Каслинская (в центр)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2132, '101', '5/25/2018', '5/12/2018', '5/11/2023', 'ИП Барыкин И.С.', 'стела', 'ул.Бр.Кашириных,135');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2133, '102', '5/25/2018', '5/12/2018', '5/11/2023', 'ИП Садыков Ф.М.', 'панель-кронштейн',
        'ул.Машиностроителей,20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2134, '103', '5/25/2018', '5/12/2018', '5/11/2023', 'ИП Садыков Ф.М.', 'панель-кронштейн',
        'ул.Машиностроителей,34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2135, '104', '5/25/2018', '5/12/2018', '5/11/2023', 'ИП Садыков Ф.М.', 'стела', 'ул.Чичерина,43');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2136, '105', '5/25/2018', '5/12/2018', '5/11/2023', 'ПАО "Челиндбанк"', 'панель-кронштейн', 'ул.Гагарина,10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2137, '106', '5/25/2018', '5/12/2018', '5/11/2023', 'ПАО "Челиндбанк"', 'панель-кронштейн',
        'ул.Гагарина, между д. 11 и д. 13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2138, '107', '5/25/2018', '5/12/2018', '5/11/2023', 'ПАО "Челиндбанк"', 'панель-кронштейн',
        'ул. Карла Маркса, 80');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2139, '108', '5/28/2018', '5/12/2018', '5/11/2023', 'ИП Валеев Т.Р.', 'стела', 'пр.Победы, 321');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2140, '109', '5/28/2018', '5/12/2018', '5/11/2023', 'ИП Валеев Т.Р.', 'панель-кронштейн', 'пр.Победы, 384');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2141, '110', '5/28/2018', '5/12/2018', '5/11/2023', 'ООО "Атолл Авто"', 'стела',
        'ул.Новороссийская, между д.40 и д.42');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2142, '111', '5/28/2018', '5/12/2018', '5/11/2023', 'ИП Крохалев В.Ю.', 'сити-формат', 'ул.Воровского,81');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2143, '112', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'пересеч.ул.Бр.Кашириных(в центр) и ул.Краснознаменная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2144, '113', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'Троицкий тр., перед поворотом в пос. Новосинеглазово');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2145, '114', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка', 'ул.Блюхера,81');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2146, '115', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'пересеч.пр.Победы,149А и ул.Болейко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2147, '116', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'пересеч.ул.Бр.Кашириных и ул.Парашютная,41');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2148, '117', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'Троицкий тр.,35к/1, напротив заводоуправления, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2149, '118', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'ул.Блюхера,напротив Заводоуправления АМЗ');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2150, '119', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'Троицкий тр.,35к/1, напротив заводоуправления, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2151, '120', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Карус"', 'щитовая установка',
        'ул.Блюхера,напротив д.93');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2152, '121', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Фирма Торекс"', 'сити-формат (в составе ост.комп.)',
        'пр. Ленина,76,ост. "ЮУрГУ", поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2153, '122', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Фирма Торекс"', 'сити-формат (в составе ост.комп.)',
        'пр. Ленина,76,ост. "ЮУрГУ", поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2154, '123', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Фирма Торекс"', 'сити-формат (в составе ост.комп.)',
        'пр. Ленина,76,ост. "ЮУрГУ", поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2155, '124', '6/4/2018', '5/24/2018', '5/23/2023', 'ООО "Фирма Торекс"', 'сити-формат (в составе ост.комп.)',
        'пр. Ленина,76,ост. "ЮУрГУ", поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2156, '125', '6/4/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'стела', 'ул. Энгельса, 34');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2157, '126', '6/4/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'стела',
        'ул. Университетская Набережная, 64');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2158, '127', '6/4/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'стела',
        'ул.Бейвеля/ ул.Скульптора Головницкого, 16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2159, '128', '6/4/2018', '4/1/2018', '3/31/2028', 'ООО "Планета Авто"', 'щитовая установка',
        'Копейское шоссе, 82');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2160, '129', '6/4/2018', '6/1/2018', '5/30/2023', 'АО "Банк Союз"', 'панель-кронштейн', 'пр.Ленина,11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2161, '131', '6/5/2018', '5/12/2018', '5/11/2023', 'ООО "Концепт-Строй"', 'стела',
        'ул. Лесопарковая/ поворот к ул. Лесопарковая,6А/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2162, '132', '6/5/2018', '5/12/2018', '5/11/2028', 'ООО "АТЛ Компания"', 'Экран',
        'площадь Революции/ ул.Кирова,116');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2163, '133', '6/5/2018', '5/12/2018', '5/11/2023', 'ООО "Частная врачебная практика"', 'стела',
        'Комсомольский пр.,90');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2164, '134', '6/6/2018', '5/12/2018', '5/11/2023', 'ИП Козлов М.А.', 'щитовая установка', 'ул.Автодорожная,9а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2165, '135', '6/6/2018', '5/12/2018', '5/11/2023', 'ООО Компания "Полимер"', 'стела', 'ул.Енисейская,48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2166, '136', '6/7/2018', '5/12/2018', '5/11/2023', 'ООО "Фианит-Ломбард"', 'нестанд.рекл.констр.',
        'ул.Марченко,18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2167, '137', '6/9/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'панель-кронштейн',
        'Свердловский пр.,39');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2168, '138', '6/9/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'панель-кронштейн',
        'ул.Б.Хмельницкого,18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2169, '139', '6/9/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'панель-кронштейн',
        'ул.Бр.Кашириных,68А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2170, '140', '6/9/2018', '5/12/2018', '5/11/2023', 'ИП Толмачева Е.В.', 'панель-кронштейн',
        'ул.Дзержинского,105');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2171, '141', '6/9/2018', '5/12/2018', '5/11/2023', 'АО "Углеметбанк"', 'щитовая установка',
        'ул.2-ая Павелецкая,14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2172, '142', '6/18/2018', '5/24/2018', '5/23/2028', 'ООО "Амиго Медиа"', 'Экран',
        'пересеч.Комсомольского пр.,48 и ул.Молодогвардейцев');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2173, '143', '6/18/2018', '5/24/2018', '5/23/2028', 'ООО "Амиго Медиа"', 'Экран',
        'пересеч.Свердловского пр.,7 и Комсомольского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2174, '144', '6/18/2018', '5/24/2018', '5/23/2028', 'ООО "Амиго Медиа"', 'Экран',
        'пересеч.ул.Гагарина и ул. Дзержинского,91');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2175, '145', '6/18/2018', '5/24/2018', '5/23/2028', 'ООО "Амиго Медиа"', 'Экран',
        'пересечение пр.Ленина (из центра) и ул. Горького, Комсомольская площадь');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2176, '146', '6/18/2018', '5/24/2018', '5/23/2028', 'ООО "Амиго Медиа"', 'Экран',
        'пересечение ул.Черкасская и ш.Металлургов,43(через дорогу)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2177, '147', '6/18/2018', '5/24/2018', '5/23/2028', 'ООО "Амиго Медиа"', 'Экран',
        'Привокзальная пл. (ТК "Синегорье")');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2178, '148', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '96 суперсайт',
        'ул. Чичерина,22/5 и Комсомольского пр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2179, '149', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '150 супер', 'Троицкий тр., 1/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2180, '150', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '96 суперсайт', 'Свердловский тр., 7/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2181, '151', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '150 супер',
        'пересеч. Свердловского тр. и ул. Индивидуальная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2182, '152', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '150 супер',
        'Шершневское водохранилище, пост ГИБДД');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2183, '153', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '96 суперсайт',
        'ул. Худякова, напротив д. 22к1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2184, '154', '6/18/2018', '5/24/2018', '5/23/2028', 'АО "Элис"', '150 супер',
        'пересеч. Копейского шоссе и ул. Харлова, 2а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2185, '155', '21.06.2018', '5/12/2018', '5/11/2023', 'ООО "Газресурс"', 'стела', 'ул.Федорова,1А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2186, '166', '7/25/2018', '5/12/2018', '5/11/2023', 'ООО "Лукойл-Уралнефтепродукт"', 'стела', 'пр.Ленина,26/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2187, '167', '8/1/2018', '5/12/2018', '5/11/2023', 'ООО "Башнефть-Розница"', 'стела',
        'пересечение ул.Тухачевского и а/д Меридиан');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2188, '168', '8/1/2018', '5/12/2018', '5/11/2023', 'ООО "Башнефть-Розница"', 'стела',
        'ул.Новороссийская,122/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2189, '169', '8/1/2018', '5/12/2018', '5/11/2023', 'ООО "Башнефть-Розница"', 'стела',
        'пересечение ул.Блюхера и ул.Гоголя');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2190, '170', '8/1/2018', '5/12/2018', '5/11/2023', 'ООО "Башнефть-Розница"', 'стела',
        'пересечение ул.Труда и ул. Энгельса');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2191, '171', '9/3/2018', '8/23/2018', '8/22/2023', 'ООО "Мегаполис" ', 'Тумба',
        'ул.Артиллерийская, 136 (у входа со стороны а/д Меридиан)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2192, '172', '9/3/2018', '8/23/2018', '8/22/2023', 'ООО "Мегаполис" ', 'Тумба',
        'ул.Артиллерийская, 136 (у входа со стороны ул. Артиллерийская)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2193, '173', '9/3/2018', '8/23/2018', '8/22/2023', 'ООО "Мегаполис"', 'стела',
        'ул.Артиллерийская, 136 (со стороны ул. Ловина)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2194, '174', '9/3/2018', '8/23/2018', '8/22/2023', 'ИП Логинов В.В.', 'стела', 'ул.Свободы,78');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2195, '175', '9/3/2018', '8/23/2018', '8/22/2023', 'ИП Бибиков В.Н.', 'нестанд.рекл.констр.',
        'Троицкий тр.,11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2196, '176', '9/3/2018', '8/23/2018', '8/22/2023', 'ИП Бибиков В.Н.', 'нестанд.рекл.констр.',
        'Троицкий тр.,52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2197, '177', '9/3/2018', '8/23/2018', '8/22/2023', 'ООО "Челябинск-Восток-Сервис"', 'стела',
        'пр.Ленина,2а/2 (район главной проходной ЧТЗ)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2198, '178', '9/5/2018', '8/23/2018', '8/22/2023', 'ООО "АВТОИМИДЖ"', 'стела', 'ул. Труда, 166');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2199, '180', '9/5/2018', '8/23/2018', '8/22/2023', 'ООО "Авто-Вико"', 'панель-кронштейн',
        'ул.Молодогвардейцев, поворот к дому № 109 по ул.Бр. Кашириных');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2200, '181', '9/6/2018', '8/23/2018', '8/22/2023', 'ИП Михеев М.Ю.', 'Флаги', 'ул.Танкистов,177А/2,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2201, '182', '9/6/2018', '8/23/2018', '8/22/2023', 'ИП Михеев М.Ю.', 'Флаги', 'ул.Танкистов,177А/2,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2202, '183', '9/6/2018', '8/23/2018', '8/22/2023', 'ИП Михеев М.Ю.', 'Флаги', 'ул.Танкистов,177А/2,поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2203, '184', '9/6/2018', '8/23/2018', '8/22/2023', 'ИП Михеев М.Ю.', 'Флаги', 'ул.Танкистов,177А/2,поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2204, '185', '9/6/2018', '8/23/2018', '8/22/2023', 'ИП Михеев М.Ю.', 'Флаги', 'ул.Танкистов,177А/2,поз.5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2205, '186', '9/6/2018', '8/23/2018', '8/22/2023', 'АО "Литейно-механический завод "Стройэкс"', 'указатель',
        'пересечение ш.Металлургов и ул.Строительная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2206, '187', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "МОЛЛ"', 'указатель',
        'пересечение ул. Жукова и ул.Социалистическая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2207, '188', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "МОЛЛ"', 'указатель', 'ул.Агалакова,38');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2208, '189', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "МОЛЛ"', 'указатель', 'ул.Чичерина, напротив д.15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2209, '190', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "МОЛЛ"', 'указатель', 'ул.Бажова,46');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2210, '191', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "МОЛЛ"', 'указатель', 'ул.Героев Танкограда,37');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2211, '192', '9/10/2018', '8/23/2018', '8/22/2023', 'ИП Трифонов Э.С.', 'панель-кронштейн',
        'пересечение пр. Ленина и ул. Свободы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2212, '193', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "Элефант-Проспект"', 'панель-кронштейн на собст.опоре',
        'пересечение пр.Ленина, 61 и ул.Васенко');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2213, '194', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "Элефант-Проспект"', 'панель-кронштейн на собст.опоре',
        'пересечение пр.Ленина, 45 и ул.Свободы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2214, '195', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "Элефант-Проспект"', 'панель-кронштейн на собст.опоре',
        'пересечение пр.Ленина, 53 и ул.Цвиллинга');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2215, '196', '9/10/2018', '8/23/2018', '8/22/2023', 'ООО "Элефант-Проспект"', 'панель-кронштейн на собст.опоре',
        'пересечение пр.Ленина, 43 и ул.Свободы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2216, '197', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Стрела плюс"', 'стела', 'пр. Победы, 124');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2217, '198', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Стрела плюс"', 'сити-формат',
        'пр. Победы, 124, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2218, '199', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Стрела плюс"', 'сити-формат',
        'пр. Победы, 124, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2219, '200', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Стрела плюс"', 'сити-формат',
        'пр. Победы, 124, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2220, '201', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Стрела плюс"', 'сити-формат',
        'пр. Победы, 124, поз. 4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2221, '202', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Уральский технический центр"', 'стела',
        'ул.Бр.Кашириных,147, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2222, '203', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Уральский технический центр"', 'стела',
        'ул.Бр.Кашириных,147, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2223, '204', '9/11/2018', '8/23/2018', '8/22/2023', 'ООО "Концепт Строй"', 'Флаги', 'ул. Лесопарковая,6Астр.');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2224, '205', '9/12/2018', '8/23/2018', '8/22/2023', 'ИП Букреев А.С.', 'Флаги', 'ул.Татищева,264, поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2225, '206', '9/12/2018', '8/23/2018', '8/22/2023', 'ИП Букреев А.С.', 'Флаги', 'ул.Татищева,264, поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2226, '207', '9/12/2018', '8/23/2018', '8/22/2023', 'ИП Букреев А.С.', 'Флаги', 'ул.Татищева,264, поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2227, '208', '9/12/2018', '8/23/2018', '8/22/2023', 'ИП Букреев А.С.', 'Флаги', 'ул.Аношкина,12,поз. 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2228, '209', '9/12/2018', '8/23/2018', '8/22/2023', 'ИП Букреев А.С.', 'Флаги', 'ул.Аношкина,12,поз. 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2229, '210', '9/12/2018', '8/23/2018', '8/22/2023', 'ИП Букреев А.С.', 'Флаги', 'ул.Аношкина,12,поз. 3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2230, '211', '9/12/2018', '8/23/2018', '8/22/2023', 'ООО СК "Легион"', 'стела', 'ул.40 летия Победы,4Б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2231, '229', '12/14/2018', '11/23/2018', '11/22/2023', 'ООО "Лента"', 'стела', 'ул.Бр.Кашириных,75');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2232, '230', '12/14/2018', '11/23/2018', '11/22/2023', 'ООО "Лента"', 'нестанд.рекл.констр.',
        'ул.Бр.Кашириных,75,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2233, '231', '12/14/2018', '11/23/2018', '11/22/2023', 'ООО "Лента"', 'нестанд.рекл.констр.',
        'ул.Бр.Кашириных,75,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2234, '232', '12/14/2018', '11/23/2018', '11/22/2023', 'ООО "Лента"', 'Флаги', 'ул.Бр.Кашириных,75,поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2235, '233', '12/14/2018', '11/23/2018', '11/22/2023', 'ООО "Лента"', 'Флаги', 'ул.Бр.Кашириных,75,поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2236, '241', '12/26/2018', '12/22/2018', '12/21/2023', 'ИП Петровская Е.В.', 'щитовая установка',
        'ул. Татищева, 262');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2237, '243', '12/27/2018', '12/22/2018', '12/21/2023', 'ООО "Вдохновение"', 'стела', 'Комсомольский пр., 48');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2238, '244', '12/27/2018', '12/22/2018', '12/21/2028', 'ООО "Карус"', 'Экран', 'ул. Худякова, 31');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2239, '245', '12/27/2018', '12/22/2018', '12/21/2028', 'ООО "Карус"', 'Экран',
        'пересечение ул. Братьев Кашириных (из центра) и ул. Молодогвардейцев, 57/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2240, '246', '12/29/2018', '12/25/2018', '12/24/2023', 'ООО "Универмаг "Детский мир"',
        'панель-кронштейн на собст.опоре', 'ул. Коммуны, 58, поз. 6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2241, '247', '12/29/2018', '12/22/2018', '12/21/2023', 'ООО "ЧЗАН"', 'стела', 'ул. Блюхера/ ул. Кузнецова, 1А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2242, '248', '12/29/2018', '12/22/2018', '12/21/2023', 'ООО "ЧЗАН"', 'стела', 'ул. Блюхера, напротив д. 59');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2243, '1', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Элит СТ"', 'РК (ост.компл.)',
        'ул. Харлова, ООТ "КБС", из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2244, '2', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Элит СТ"', 'РК (ост.компл.)',
        'Свердловский тр., ООТ "Лакокрасочный завод", в город');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2245, '4', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Элит СТ"', 'РК (ост.компл.)',
        'Копейское ш., 58, в центр, ООТ "Профилакторий"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2246, '5', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Элит СТ"', 'РК (ост.компл.)',
        'Копейское ш., 64/1, ООТ "Профилакторий"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2247, '6', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Элит СТ"', 'РК (ост.компл.)',
        'ул. Енисейская, 1, ООТ "Профилакторий"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2248, '10', '1/9/2019', '12/25/2018', '12/24/2028', 'ООО "Карус"', 'Экран',
        'пересечение Свердловского пр., 28А и пр. Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2249, '11', '1/9/2019', '12/25/2018', '12/24/2028', 'ООО "Карус"', 'Экран',
        'пересечение Свердловского пр. и ул. Братьев Кашириных');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2250, '12', '1/9/2019', '12/25/2018', '12/24/2028', 'ООО "Карус"', 'Экран', 'ул. Доватора, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2251, '13', '1/9/2019', '12/25/2018', '12/24/2028', 'ООО "Карус"', 'Экран',
        'пересечение ул. Братьев Кашириных,126/1 и ул. Чичерина');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2252, '14', '1/9/2019', '12/25/2018', '12/24/2028', 'ООО "Карус"', 'Экран',
        'Блюхера (в центр),50 м от пересечения с ул. Мебельная (слева)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2253, '15', '1/9/2019', '12/25/2018', '12/24/2028', 'ООО "Карус"', '60 супер',
        'ул. Худякова (из центра), поворот на городской пляж');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2254, '16', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'сити-борд',
        'пр. Ленина, Комсомольская площадь, за остановкой, из центра');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2255, '17', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'пересечение пр. Победы и ул. Северо-Крымская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2256, '18', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'ул. Худякова (в центр), 700 м до ул. Лесопарковая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2257, '19', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'ул. Братьев Кашириных, 300 м от ул. Косарева в центр за АЗС');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2258, '20', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Северо-Крымская и ул. Университетская набережная');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2259, '21', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Цвиллинга, 64 и ул. Перовской');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2260, '22', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Героев Танкограда (в центр) и ул. Валдайская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2261, '23', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'ул. Худякова (в центр), 1000 м до ул. Лесопарковая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2262, '24', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'Комсомольский пр., 41 (конец дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2263, '25', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'Комсомольский пр., 47 (начало дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2264, '26', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'ул. Худякова (в центр), 1280 м до ул. Лесопарковая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2265, '27', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'Комсомольский пр., 28 (конец дома)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2266, '28', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'ул. Худякова (в центр), 1410 м до ул. Лесопарковая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2267, '29', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'пересечение Комсомольского пр (в центр) и ул. Косарева, за остановкой');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2268, '30', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка', 'ул. Энгельса, 52');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2269, '31', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'пересечение ул. Братьев Кашириных, 114 и ул. 40 лет Победы');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2270, '32', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'ул. Худякова (в центр), 1680 м до ул. Лесопарковая');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2271, '33', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка', 'Свердловский пр., 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2272, '34', '1/9/2019', '12/25/2018', '12/24/2023', 'ООО "Карус"', 'щитовая установка',
        'Комсомольский пр., 72');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2273, '35', '1/10/2019', '12/25/2018', '12/24/2028', 'ООО "Гэллэри Сервис"', 'Экран', 'Комсомольский пр., 69');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2274, '36', '1/10/2019', '12/25/2018', '12/24/2028', 'ООО "Гэллэри Сервис"', 'Экран',
        'пересечение Комсомольского пр., 86В и ул. Молдавская');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2275, '37', '1/10/2019', '12/22/2018', '12/21/2028', 'ООО "Гэллэри Сервис"', 'Экран',
        'ул. Университетская Набережная,30');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2276, '38', '1/10/2019', '12/22/2018', '12/21/2028', 'ООО "Гэллэри Сервис"', 'Экран', 'пр. Ленина, 69');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2277, '39', '1/10/2019', '12/22/2018', '12/21/2023', 'ООО "Белшина-Урал"', 'Флаги',
        'Уфимский тракт, 123/2 (11 км + 200)');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2278, '40', '1/10/2019', '12/25/2018', '12/24/2023', 'ООО "Карго-Сервис плюс"', 'щитовая установка',
        'Троицкий тр., 21/7 (поворот на станцию "Челябинск-Грузовой")');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2279, '41', '1/10/2019', '12/22/2018', '12/21/2023', 'ООО "Меркурий"', 'панель-кронштейн',
        'ул. Бейвеля/ ул. Скульптора Головницкого, 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2280, '42', '1/10/2019', '12/22/2018', '12/21/2023', 'ООО "АЗИЯ АВТО"', 'стела', 'ул. Молодогвардейцев, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2281, '43', '1/10/2019', '12/25/2018', '12/24/2023', 'ИП Рустамов Эльдар Чингис Оглы', 'щитовая установка',
        'ул. Хлебозаводская, 7а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2282, '44', '1/11/2019', '12/25/2018', '12/24/2028', 'ИП Новожилов И.Г.', 'стела', 'ул. Труда, 185А');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2283, '45', '1/11/2019', '12/25/2018', '12/24/2023', 'ООО "Леципс"', 'Флаги', 'Копейское шоссе, 1-г');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2284, '46', '1/11/2019', '12/22/2018', '12/21/2023', 'ООО "АТЛ-Компания"', 'сити-борд',
        'пр. Ленина, напротив д. 26/1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2285, '47', '1/11/2019', '12/22/2018', '12/21/2023', 'ООО "АТЛ-Компания"', 'щитовая установка',
        'Комсомольский пр., 9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2286, '48', '1/15/2019', '12/22/2018', '12/21/2023', 'ООО "ЧелябРезинаТехника"', 'стела',
        'Троицкий тр., 25 к.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2287, '49', '1/15/2019', '12/22/2018', '12/21/2028', 'Калинин А.В.', '144 супер',
        'пр. Героя России Е. Родионова, 2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2288, '50', '1/15/2019', '12/25/2018', '12/24/2023', 'ИП Гладышев К.Г.', 'стела', 'ул. Новороссийская, 44');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2289, '51', '1/16/2019', '12/22/2018', '12/21/2023', 'ООО "Остеопатия"', 'стела', 'ул. Елькина, 76а');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2290, '52', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2291, '53', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.2');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2292, '54', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.3');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2293, '55', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.4');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2294, '56', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.5');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2295, '57', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.6');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2296, '58', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2297, '59', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2298, '60', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.9');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2299, '61', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2300, '62', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.11');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2301, '63', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2302, '64', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.13');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2303, '65', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.14');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2304, '66', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2305, '67', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.16');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2306, '68', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.17');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2307, '69', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.18');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2308, '70', '2/6/2019', '4/29/2018', '4/28/2023', 'ООО ПК "Южуралмебель"', 'Флаги ', 'ул.Дарвина,18, поз.19');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2309, '73', '2/8/2019', '10/1/2018', '9/30/2023', 'АО "Бажовский"', 'Стела',
        'пересечение ул. Бажова и  пер. Лермонтова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2310, '74', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'Комсомольский пр., 10');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2311, '75', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'Комсомольский пр., 26');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2312, '76', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул. Братьев Кашириных, 107б');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2313, '77', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул. Братьев Кашириных, напротив д. 101');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2314, '80', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул. Чичерина, 23');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2315, '81', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'ул. Братьев Кашириных, 101');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2316, '82', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Урал Стрит"', 'щитовая установка',
        'автодорога Меридиан, напротив рынка "порт Артур"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2317, '85', '2/8/2019', '12/25/2018', '12/24/2023', 'ООО "Легион Моторс"', 'Стела', 'ул. Енисейская, 1');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2318, '88', '2/8/2019', '12/25/2018', '12/24/2028', 'ООО "Вернисаж Медиа"', 'Экран',
        'пересечение ул. Энгельса, 95 и ул. Курчатова');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2319, '89', '2/8/2019', '12/25/2018', '12/24/2028', 'ООО "Вернисаж Медиа"', 'Экран',
        'Комсомольского пр./ ул. Чайковского ,15');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2320, '90', '2/8/2019', '12/25/2018', '12/24/2028', 'ООО "Вернисаж Медиа"', 'Экран',
        'Копейское шоссе и ул. Енисейская, разд. газон напротив ТК "Алмаз"');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2321, '91', '2/8/2019', '12/22/2018', '12/21/2023', 'ООО "Техно"', 'Стела',
        'ул. Новоэлеваторная/ ул. Блюхера, 121Е');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2322, '92', '2/8/2019', '12/22/2018', '12/21/2023', 'ООО "Замена масла"', 'Стела',
        'ул. Болейко/ пр. Победы, 160В');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2323, '93', '2/8/2019', '12/22/2018', '12/21/2023', 'ИП Садыков Ф. М.', 'сити-формат',
        'пересечение ул. Братьев Кашириных и ул. Академика Королева,28');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2324, '94', '2/8/2019', '12/22/2018', '12/21/2023', 'ИП Садыков Ф. М.', 'Стела', 'ул. Батумская, 20');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2325, '95', '2/8/2019', '12/22/2018', '12/21/2023', 'ИП Садыков Ф. М.', 'Стела', 'ул. Героев Танкограда, 55');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2326, '96', '2/8/2019', '12/22/2018', '12/21/2023', 'ИП Садыков Ф. М.', 'Стела', 'ул. Героев Танкограда, 57');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2327, '97', '2/8/2019', '12/22/2018', '12/21/2023', 'ИП Садыков Ф. М.', 'Стела', 'ул. Молодогвардейцев, 12');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2328, '99', '2/11/2019', '12/22/2018', '12/21/2023', 'ПАО "Совкомбанк"', 'флаги', 'пр. Ленина, 73');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2329, '100', '2/12/2019', '12/25/2018', '12/24/2023', 'ООО "Стрела плюс"', 'сити-формат',
        'пр. Победы, 124, поз. 7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2330, '103', '3/19/2019', '2/15/2019', '2/1/2022', 'ООО "Грандико Медиа"', 'Экран', 'ул.Молодогвардейцев,7');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2331, '104', '4/3/2019', '3/1/2019', '2/29/2024', 'ООО Индустриальный парк "Станкомаш"', 'щитовая установка',
        'ул. Енисейская, 8');
INSERT INTO raw_permit (id, permit, issuing_at, start, finish, distributor, type, remark)
VALUES (2332, '106', '4/3/2019', '10/10/2016', '10/9/2026', 'ООО ПК "Южуралмебель"', 'Стела', 'ул.Дарвина,18');