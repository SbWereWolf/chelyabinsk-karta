<?php

use LanguageSpecific\ArrayHandler;

require_once 'D:\project\chelyabinsk-karta\vendor\autoload.php';

try {
    $connection = new PDO('mysql:host=localhost;dbname=scheme',
        'root', 'admin');
    $isSuccess = true;
} catch (Exception $e) {
    $isSuccess = false;
}
if ($connection === null) {
    echo 'не могу соединиться с базой' . PHP_EOL;
}
$export = null;
if ($connection !== null) {
    $export = $connection->prepare('
select raw_permit_id, response from with_city_response 
');
    $isSuccess = $export !== false;
}
$import = null;
if ($isSuccess) {
    $import = $connection->prepare('
insert into point_suggestion
    ( raw_permit_id, longitude, latitude, formatted, request)
value (?,?,?,?,?) 
');
    $isSuccess = $export !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET NAMES \'utf8mb4\''
        . ' COLLATE \'utf8mb4_unicode_ci\'');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('START TRANSACTION');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET AUTOCOMMIT = OFF');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $isSuccess = $export->execute();
}
$answers = [];
if ($isSuccess) {
    $answers = $export->fetchAll(PDO::FETCH_ASSOC);
    $isSuccess = !empty($answers);
}
/**
 * @param array $properties
 * @param PDOStatement $import
 * @param PDO $connection
 * @return bool
 */
function insertSuggestion(array $properties, PDOStatement $import,
                          PDO $connection)
{
    $extractor = new ArrayHandler($properties);
    $properties = $extractor->get('id')->double();
    $longitude = $extractor->get('longitude')->double();
    $latitude = $extractor->get('latitude')->double();
    $formatted = $extractor->get('formatted')->str();
    $request = $extractor->get('request')->str();
    $statistics = $import->execute([$properties, $longitude, $latitude,
        $formatted, $request]);
    $isSuccess = $statistics !== false;

    if (!$isSuccess) {
        $error = $import->errorInfo();
        echo "Exception $error" . PHP_EOL;
        /** @noinspection PhpUnusedLocalVariableInspection */
        $command = $connection->exec('ROLLBACK');
    }
    return $isSuccess;
}

/**
 * @param $item
 * @param $id
 * @return array
 */
function extractPointInfo($item, $id)
{
    $address = $item['GeoObject']['metaDataProperty']
    ['GeocoderMetaData']['Address'];
    $formatted = $address['formatted'];
    $components = $address['Components'];
    $isAllow = count($components) > 4;

    if ($isAllow) {
        $isAllow =
            $components[0]['kind'] === 'country'
            && $components[0]['name'] === 'Россия'
            && $components[1]['kind'] === 'province'
            && $components[1]['name'] ===
            'Уральский федеральный округ'
            && $components[2]['kind'] === 'province'
            && $components[2]['name'] ===
            'Челябинская область'
            && $components[3]['kind'] === 'area'
            && $components[3]['name'] ===
            'городской округ Челябинск'
            && $components[4]['kind'] === 'locality'
            && $components[4]['name'] === 'Челябинск';
    }
    if (!$isAllow) {
        echo "raw_permit_id = $id, have not enough data"
            . PHP_EOL;
    }
    $coords = [];
    If ($isAllow) {
        $pos = $item['GeoObject']['Point']['pos'];
        $coords = explode(' ', $pos);
        $isAllow = count($coords) === 2;
    }
    $longitude = 0;
    $latitude = 0;
    if ($isAllow) {
        $longitude = $coords[0];
        $latitude = $coords[1];
    }
    $pointInfo = ['formatted' => $formatted, 'longitude' => $longitude,
        'latitude' => $latitude];

    return $pointInfo;
}

if ($isSuccess) {
    foreach ($answers as $addressInfo) {

        $data = new ArrayHandler($addressInfo);
        $id = $data->get('raw_permit_id')->int();
        $response = $data->get('response')->str();

        $info = json_decode($response, true);
        $request = $info['response']['GeoObjectCollection']
        ['metaDataProperty']['GeocoderResponseMetaData']['request'];
        $formatted = '';

        $address = [];
        $isAllow = false;
        $components = [];
        $featureMember =
            $info['response']['GeoObjectCollection']['featureMember'];

        $longitude = 0;
        $latitude = 0;
        $formatted = '';

        $properties = ['id' => $id, 'longitude' => $longitude,
            'latitude' => $latitude, 'formatted' => $formatted,
            'request' => $request];
        $isEmpty = count($featureMember) === 0;
        if ($isEmpty) {
            $isSuccess = insertSuggestion($properties, $import,
                $connection);
            if (!$isSuccess) {
                break;
            }
        }
        if (!$isEmpty) {
            foreach ($featureMember as $item) {
                $pointInfo = extractPointInfo($item, $id);
                $info = new ArrayHandler($pointInfo);
                $formatted = $info->get('formatted')->str();
                $longitude = $info->get('longitude')->double();
                $latitude = $info->get('latitude')->double();

                $properties = ['id' => $id, 'longitude' => $longitude,
                    'latitude' => $latitude, 'formatted' => $formatted,
                    'request' => $request];
                $isSuccess = insertSuggestion($properties, $import,
                    $connection);
                if (!$isSuccess) {
                    break;
                }
            }
        }
    }
}

if ($isSuccess) {
    $command = $connection->exec('COMMIT');
}
if ($connection !== false) {
    $command = $connection->exec('SET AUTOCOMMIT = ON');
}
