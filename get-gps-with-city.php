<?php
const GEOCODER =
    'https://geocode-maps.yandex.ru/1.x/?lang=ru_RU&format=json'
    . '&apikey=344dde82-33ad-407f-b719-4e880eb28ff1&geocode=';

/**
 * @return false|resource
 */
function curlInit()
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($curl, CURLOPT_HTTPGET, true);
    return $curl;
}

/**
 * @param $rawAddress
 * @param $curl
 * @return bool|string
 */
function requestGeocoder($rawAddress, $curl)
{
    $address = str_replace(',', ' ', $rawAddress);
    $target = urlencode('город Челябинск ' . $address);
    curl_setopt($curl, CURLOPT_URL, GEOCODER . $target);
    $response = curl_exec($curl);

    return $response;
}

/**
 * @param $curl
 * @return string
 */
function getErrorText($curl)
{
    $error = '';
    $curlErrorNumber = curl_errno($curl);
    if ($curlErrorNumber) {
        $error = curl_error($curl);
    }

    return $error;
}

$curl = curlInit();

try {
    $connection = new PDO('mysql:host=localhost;dbname=scheme', 'root', 'admin');
    $isSuccess = true;
} catch (Exception $e) {
    $isSuccess = false;
}
if ($connection === null) {
    echo 'не могу соединиться с базой' . PHP_EOL;
}
$export = null;
if ($connection !== null) {
    $export = $connection->prepare('
select id, remark from raw_permit 
');
    $isSuccess = $export !== false;
}
$import = null;
if ($isSuccess) {
    $import = $connection->prepare('
insert into with_city_response (raw_permit_id, response,error)
values(?,?,?)  
');
    $isSuccess = $export !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET NAMES \'utf8mb4\''
        . ' COLLATE \'utf8mb4_unicode_ci\'');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('START TRANSACTION');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET AUTOCOMMIT = OFF');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $isSuccess = $export->execute();
}
$addresses = [];
if ($isSuccess) {
    $addresses = $export->fetchAll(PDO::FETCH_ASSOC);
    $isSuccess = !empty($addresses);
}
foreach ($addresses as $address) {
    $street = $address['remark'];
    $id = $address['id'];

    $response = requestGeocoder($street, $curl);
    $error = getErrorText($curl);

    $stat = $import->execute([$id, $response, $error]);
    $isSuccess = $stat !== false;

    if (!$isSuccess) {
        $error = $import->errorInfo();
        $command = $connection->exec('ROLLBACK');
    }
}
if ($isSuccess) {
    $command = $connection->exec('COMMIT');
}
if ($connection !== false) {
    $command = $connection->exec('SET AUTOCOMMIT = ON');
}

