INSERT INTO tx_type (id, title)
VALUES (0, '0');
INSERT INTO tx_type (id, title)
VALUES (1, 'white-long-rectangle');
INSERT INTO tx_type (id, title)
VALUES (2, 'six-rectangle');
INSERT INTO tx_type (id, title)
VALUES (3, 'empty');
INSERT INTO tx_type (id, title)
VALUES (4, 'twice-rectangle');
INSERT INTO tx_type (id, title)
VALUES (5, 'white-rectangle');
INSERT INTO tx_type (id, title)
VALUES (6, 'black-rectangle');
INSERT INTO tx_type (id, title)
VALUES (7, 'crocodile');
INSERT INTO tx_type (id, title)
VALUES (8, 'white-cube');
INSERT INTO tx_type (id, title)
VALUES (9, 'black-cube');
INSERT INTO tx_type (id, title)
VALUES (10, 'white-circle');
INSERT INTO tx_type (id, title)
VALUES (11, 'black-circle');
INSERT INTO tx_type (id, title)
VALUES (12, 'white-circle-with-dot');
INSERT INTO tx_type (id, title)
VALUES (13, 'white-triangle');
INSERT INTO tx_type (id, title)
VALUES (14, 'black-triangle');
INSERT INTO tx_type (id, title)
VALUES (15, 'flag');
INSERT INTO tx_type (id, title)
VALUES (16, 'star');
INSERT INTO tx_type (id, title)
VALUES (17, 'cross');
INSERT INTO tx_type (id, title)
VALUES (18, 'V');
INSERT INTO tx_type (id, title)
VALUES (19, 'arrow');