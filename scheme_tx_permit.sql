INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1, 414, 1406505600, 1398902400, 1556582400, 12, 'ул.Труда 153', 61.395933, 55.167313);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2, 415, 1406505600, 1406851200, 1564531200, 6,
        'ул.Новомеханическая напротив дома по адресу:ул.Кожзаводская 108А', 61.39622, 55.197384);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (3, 416, 1406505600, 1406851200, 1564531200, 6, 'ул.Новомеханическая напротив дома по адресу:ул.Российская 13Б',
        61.411474, 55.196038);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (4, 417, 1406505600, 1406851200, 1564531200, 6,
        'автодорога Меридиан напротив здания по адресу:ул.Артиллерийская 136', 61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (5, 418, 1406505600, 1406851200, 1564531200, 6,
        'автодорога Меридиан напротив здания по адресу:ул.Артиллерийский пер. д.3', 61.432584, 55.159702);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (6, 419, 1406505600, 1406851200, 1564531200, 6, 'автодорога Меридиан напротив здания по адресу:ул.Луценко 2/2',
        61.43722, 55.151132);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (7, 1272, 1407283200, 1406851200, 1722384000, 1, 'ул.Университетская Набережная 94', 61.30302, 55.15893);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (8, 1273, 1407369600, 1406851200, 1722384000, 1, 'автодорога Меридиан ост. "Сады"', 61.420044, 55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (9, 1284, 1407456000, 1406851200, 1564531200, 12, 'ул.Героев Танкограда 21/1', 61.441765, 55.189239);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (10, 1285, 1407715200, 1406851200, 1564531200, 8, 'ул.Энтузиастов 6', 61.375182, 55.158637);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (11, 1286, 1407888000, 1406851200, 1564531200, 15, 'ул.Дарвина 2В', 61.382854, 55.119651);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (12, 1287, 1407888000, 1406851200, 1564531200, 12, 'ул.Дарвина 2В', 61.382854, 55.119651);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (13, 1288, 1407888000, 1406851200, 1564531200, 6, 'ул.Героев Танкограда 67П', 61.439807, 55.205425);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (14, 1292, 1408406400, 1406851200, 1564531200, 12,
        'ул.Первой Пятилетки  на уровне дома № 15 по ул.40 лет Октября', 61.451081, 55.166372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (15, 1293, 1408406400, 1406851200, 1564531200, 12,
        'ул.Героев Танкограда  на уровне дома № 15 по ул. 40 лет Октября', 61.451081, 55.166372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (16, 1294, 1408406400, 1406851200, 1564531200, 9, 'ул.Блюхера 44 поз.1', 61.368741, 55.132598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (17, 1295, 1408406400, 1406851200, 1564531200, 9, 'ул.Блюхера 44 поз.2', 61.368741, 55.132598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (18, 1296, 1408406400, 1406851200, 1564531200, 9, 'ул.Блюхера 44 поз.3', 61.368741, 55.132598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (19, 1297, 1408406400, 1406851200, 1564531200, 9, 'ул.Дарвина 18 поз.1', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (20, 1298, 1408406400, 1406851200, 1564531200, 9, 'ул.Дарвина 18 поз.2', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (21, 1299, 1408406400, 1406851200, 1564531200, 9, 'ул.Дарвина 18 поз.3', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (22, 1300, 1408406400, 1406851200, 1564531200, 12, 'ул.Блюхера 67', 61.36672, 55.129026);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (23, 1320, 1409702400, 1401580800, 1559260800, 17, 'ул.Комарова 34', 61.466523, 55.188488);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (24, 1321, 1409702400, 1401580800, 1559260800, 17, 'ул.Комарова 34', 61.466523, 55.188488);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (25, 1322, 1409702400, 1401580800, 1559260800, 17, 'ул.Комарова 34', 61.466523, 55.188488);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (26, 1323, 1409702400, 1401580800, 1559260800, 17, 'ул.Комарова 34', 61.466523, 55.188488);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (27, 1324, 1410220800, 1406851200, 1564531200, 6, 'пр.Комарова  60', 61.46381, 55.184942);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (28, 1325, 1410220800, 1406851200, 1564531200, 6, 'ул.Блюхера  97  200 м до ул.Кузнецова', 61.366459, 55.115321);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (29, 1326, 1410220800, 1406851200, 1564531200, 6, 'пересечение ул.Калинина и ул.Тагильская', 61.389704,
        55.179264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (30, 1328, 1410220800, 1406851200, 1564531200, 6, 'пр.Победы  289', 61.329709, 55.188396);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (31, 1329, 1410220800, 1406851200, 1564531200, 6, 'ул.50 лет ВЛКСМ напротив д.19', 61.384219, 55.24097);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (32, 1330, 1410220800, 1406851200, 1564531200, 6,
        'пересечение автодороги Меридиан (в центр) и пер. 6-й Целинный', 61.410369, 55.111346);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (33, 1331, 1410220800, 1406851200, 1564531200, 6,
        'Троицкий тр.(в город)  750 м до виадука на п. Новосинеглазово', 61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (34, 1332, 1410220800, 1406851200, 1564531200, 12, 'пересеч.ул.Новороссийская и пер.Бугурусланский', 61.48607,
        55.10692);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (35, 1333, 1410912000, 1406851200, 1564531200, 12, 'пр.Ленина 28д', 61.426521, 55.161435);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (36, 1334, 1410912000, 1406851200, 1564531200, 15, 'пр.Ленина 28д', 61.426521, 55.161435);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (37, 1335, 1410998400, 1406851200, 1564531200, 15, 'ул.Труда 185/1', 61.373834, 55.169375);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (38, 1355, 1413763200, 1401580800, 1559260800, 17, 'ул.Пионерская 4 поз.2', 61.346939, 55.192975);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (39, 1356, 1413763200, 1401580800, 1559260800, 17, 'Свердловский пр. 40а поз.1', 61.386393, 55.168634);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (40, 1357, 1413763200, 1401580800, 1559260800, 17, 'Свердловский пр. 40а поз.2', 61.386393, 55.168634);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (41, 1358, 1413763200, 1401580800, 1559260800, 17, 'Свердловский тр. 12б', 61.373799, 55.225183);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (42, 1360, 1413763200, 1401580800, 1559260800, 17, 'Копейское шоссе 35д поз.2', 61.462085, 55.135979);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (43, 1370, 1415664000, 1414108800, 1571788800, 12, 'ул.Энтузиастов 18', 61.375469, 55.154167);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (44, 1373, 1415664000, 1414108800, 1571788800, 9, 'ул.Блюхера 10', 61.381776, 55.141634);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (45, 1374, 1415664000, 1414108800, 1571788800, 9, 'Копейское шоссе 14', 61.508088, 55.122653);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (46, 1375, 1415664000, 1414108800, 1571788800, 9, 'Копейское шоссе 9', 61.503542, 55.122267);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (47, 1376, 1415664000, 1414108800, 1571788800, 12, 'Свердловский пр. 32', 61.385863, 55.175493);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (48, 1377, 1415664000, 1414108800, 1571788800, 9, 'ул.Чайковсокго 60', 61.352032, 55.179226);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (49, 1378, 1415664000, 1414108800, 1571788800, 9, 'Свердловский пр.  на уровне дома № 21 по ул. Воровского',
        61.390786, 55.151528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (50, 1379, 1415664000, 1414108800, 1571788800, 6, 'Троицкий тр. 21 к1', 61.386123, 55.094704);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (51, 1380, 1415664000, 1414108800, 1571788800, 6, 'Свердловский тр.  1а/3', 61.386186, 55.203863);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (52, 1381, 1415664000, 1414108800, 1571788800, 6, 'ул.250 летия Челябинска 17', 61.299885, 55.178198);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (53, 1382, 1415664000, 1414108800, 1571788800, 6, 'ул.Академика Королева 14', 61.299104, 55.164582);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (54, 1384, 1415664000, 1414108800, 1571788800, 6, 'ул.Машиностроителей 6', 61.475946, 55.126313);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (55, 1385, 1415836800, 1414108800, 1571788800, 2, 'ул.2-я Павелецкая 36', 61.401458, 55.274852);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (56, 1387, 1416268800, 1414108800, 1571788800, 12, 'ул.С.Разина 1', 61.409435, 55.13621);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (57, 1388, 1416268800, 1414108800, 1571788800, 9, 'ул.Степана Разина 9', 61.413639, 55.140091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (58, 1389, 1416268800, 1414108800, 1571788800, 12, 'ул.Блюхера 61', 61.367663, 55.130498);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (59, 1390, 1416268800, 1414108800, 1571788800, 2, 'Комсомольский пр. 69', 61.315713, 55.194013);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (60, 1393, 1416268800, 1414108800, 1571788800, 12, 'ул.Бр.Кашириных 79', 61.373906, 55.17665);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (61, 1397, 1417132800, 1414108800, 1571788800, 12, 'ул. Братьев Кашириных 126 поз. 1', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (62, 1398, 1417132800, 1414108800, 1571788800, 12, 'ул. Братьев Кашириных 126 поз. 2', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (63, 1399, 1417132800, 1414108800, 1571788800, 15, 'ул. Братьев Кашириных 126 поз. 1', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (64, 1400, 1417132800, 1414108800, 1571788800, 15, 'ул. Братьев Кашириных 126 поз. 2', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (65, 1401, 1417132800, 1414108800, 1571788800, 15, 'ул. Братьев Кашириных 126 поз. 3', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (66, 1402, 1417132800, 1414108800, 1571788800, 15, 'ул. Братьев Кашириных 126 поз. 4', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (67, 1403, 1417132800, 1414108800, 1571788800, 15, 'ул. Братьев Кашириных 126 поз. 5', 61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (68, 1404, 1417132800, 1414108800, 1571788800, 12, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 1', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (69, 1405, 1417132800, 1414108800, 1571788800, 12, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 2', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (70, 1406, 1417132800, 1414108800, 1571788800, 15, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 1', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (71, 1407, 1417132800, 1414108800, 1571788800, 15, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 2', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (72, 1408, 1417132800, 1414108800, 1571788800, 15, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 3', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (73, 1409, 1417132800, 1414108800, 1571788800, 15, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 4', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (74, 1410, 1417132800, 1414108800, 1571788800, 15, 'ул. Чичерина/ул. Братьев Кашириных 126 поз. 5', 61.30311,
        55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (75, 1411, 1417132800, 1414108800, 1571788800, 12, 'Копейское шоссе 82', 61.447011, 55.148302);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (76, 1412, 1417132800, 1414108800, 1571788800, 12, 'ул.Бр.Кашириных 137 поз.1', 61.301628, 55.171087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (77, 1413, 1417132800, 1414108800, 1571788800, 12, 'ул.Бр.Кашириных 137 поз.2', 61.301628, 55.171087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (78, 1414, 1417132800, 1414108800, 1571788800, 12, 'ул.Бр.Кашириных 137 поз.3', 61.301628, 55.171087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (79, 1415, 1417132800, 1414108800, 1571788800, 12, 'ул.Бр.Кашириных 137 поз.4', 61.301628, 55.171087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (80, 1416, 1417132800, 1414108800, 1571788800, 14, 'пр. Ленина  83  автобус. ост. «ЮУрГУ» (в центр)', 61.371373,
        55.158241);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (81, 1417, 1417132800, 1414108800, 1571788800, 14,
        'пр. Ленина  71а  троллейбус. ост. «Агроинженерная академия» (в центр)', 61.382027, 55.15945);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (82, 1418, 1417132800, 1414108800, 1571788800, 14,
        'ул. Энгельса  63  троллейбус. ост. «Агроинженерная академия» (из центра)', 61.381632, 55.160782);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (83, 1419, 1417132800, 1414108800, 1571788800, 14, 'ул. Худякова  6  автобус. ост. «ул. Худякова» (из центра) ',
        61.37794, 55.147983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (84, 1420, 1417132800, 1414108800, 1571788800, 14, 'ул. Худякова  11  автобус. ост. «ул. Худякова»  (в центр)',
        61.37732, 55.147433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (85, 1421, 1417132800, 1414108800, 1571788800, 14, 'пр. Ленина  65  троллейбус. ост. «Алое Поле» (в центр)',
        61.391379, 55.159671);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (86, 1422, 1417132800, 1414108800, 1571788800, 14,
        'Свердловский пр.  напротив д. 64  трамвайн. ост. «Алое Поле» (из центра)', 61.387857, 55.160967);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (87, 1423, 1417132800, 1414108800, 1571788800, 14,
        'Свердловский пр.  напротив д.64  автобус. ост. «Алое Поле» (из центра)', 61.387857, 55.160967);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (88, 1424, 1417132800, 1414108800, 1571788800, 14, 'пр. Ленина  64д  автобусн. ост. «Алое Поле» (из центра)',
        61.383842, 55.160689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (89, 1425, 1417132800, 1414108800, 1571788800, 14,
        'пр. Ленина  53  трамвайн. ост. «площадь Революции» (в центр)', 61.40568, 55.160108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (90, 1426, 1417132800, 1414108800, 1571788800, 14,
        'площадь Революции  1  трамвайн. ост. «площадь Революции» (из центра)', 61.404215, 55.159295);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (91, 1427, 1417132800, 1414108800, 1571788800, 14,
        'пр. Ленина  51  автобусн. ост. «площадь Революции» (из центра)', 61.406641, 55.160128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (92, 1428, 1417132800, 1414108800, 1571788800, 14,
        'ул.Цвиллинга/ пр. Ленина  52  автобусн. ост. «площадь Революции» (из центра)', 61.405698, 55.160828);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (93, 1429, 1417132800, 1414108800, 1571788800, 14,
        'пр. Свердловский  84  троллейбусн. ост. «ул. Южная» (из центра)', 61.388773, 55.155129);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (94, 1430, 1417132800, 1414108800, 1571788800, 14,
        'ул. Воровского  30  троллейбусн. ост. «ул. Курчатова» (из центра)', 61.386132, 55.150674);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (95, 1431, 1417132800, 1414108800, 1571788800, 14,
        'ул. Воровского  21  троллейбусн. ост. «Горбольница» (в центр)', 61.390786, 55.151528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (96, 1432, 1417132800, 1414108800, 1571788800, 14,
        'ул. Воровского  16к7  троллейбусн. ост. «ул. Южная» ( в центр)', 61.391298, 55.155499);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (97, 1433, 1417132800, 1414108800, 1571788800, 14,
        'ул. Доватора  23  автобусн. ост. «гостиница Центральная» (в сторону ж/д вокзала)', 61.386878, 55.143029);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (98, 1434, 1417132800, 1414108800, 1571788800, 14,
        'ул. Воровского  40  троллейбусн. ост. «ул. Доватора» (из центра)', 61.38112, 55.146147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (99, 1435, 1417132800, 1414108800, 1571788800, 14, 'ул. Тарасова  56  троллейбусн. ост. «Мединститут» (в центр)',
        61.376368, 55.141655);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (100, 1436, 1417132800, 1414108800, 1571788800, 14,
        'ул. Междугородная  напротив д. 21  трамвайн. ост. «Медгородок» (из центра)', 61.37661, 55.135907);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (101, 1437, 1417132800, 1414108800, 1571788800, 14,
        'ул. Блюхера  45  ост. «Областная больница» (автобус рейсовый  в сторону Уфимского тракта)', 61.374805,
        55.135217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (102, 1438, 1417132800, 1414108800, 1571788800, 14,
        'ул. Блюхера  67  троллейбусн. ост. «Мебельная фабрика» (в центр)', 61.36672, 55.129026);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (103, 1439, 1417132800, 1414108800, 1571788800, 14,
        'ул. Блюхера  85а  троллейбусн. ост. «поселок Мебельный» (в центр)', 61.364618, 55.122771);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (104, 1440, 1417132800, 1414108800, 1571788800, 14, 'ул. Ярославская  1к2  троллейбусн. ост. «АМЗ»  (в центр)',
        61.356066, 55.10961);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (105, 1441, 1417132800, 1414108800, 1571788800, 14,
        'ул. Ярославская  напротив д.1к2   троллейбусн. ост. «АМЗ» (из центра)', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (106, 1442, 1417132800, 1414108800, 1571788800, 14,
        'ул. Степана Разина  напротив д.4  трамвайн. ост. «ж/д вокзал» (в центр)', 61.411725, 55.141428);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (107, 1443, 1417132800, 1414108800, 1571788800, 14,
        'Привокзальная площадь  1  троллейбусн.ост. «Привокзальная площадь» (в центр)', 61.416136, 55.141315);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (108, 1444, 1417132800, 1414108800, 1571788800, 14,
        'ул. Свободы  173  троллейбусн. ост. «ж/д институт» (в центр)', 61.417007, 55.145483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (109, 1445, 1417132800, 1414108800, 1571788800, 14, 'пр. Ленина  46  троллейбусн. ост. «Детский мир» (в центр)',
        61.410306, 55.160936);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (110, 1446, 1417132800, 1414108800, 1571788800, 14,
        'ул. Свободы  139  троллейбусн. ост. «Детский мир» (из центра)', 61.412121, 55.159995);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (111, 1447, 1417132800, 1414108800, 1571788800, 14,
        'ул. Свободы  68  троллейбусн.ост. «Детский мир» (из центра)', 61.411061, 55.160164);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (112, 1448, 1417132800, 1414108800, 1571788800, 14,
        'пр. Ленина  18  трамвайн. ост. «Комсомольская площадь» (в сторону северо-запада)', 61.442933, 55.16108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (113, 1449, 1417132800, 1414108800, 1571788800, 14,
        'ул. Российская  43  автобусн. ост. «ул. Российская» (из центра)', 61.414618, 55.184947);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (114, 1450, 1417132800, 1414108800, 1571788800, 14,
        'ул. Кирова  напротив д. 2а  трамвайн. ост. «Теплотехнический институт» (в центр)', 61.372388, 55.03977);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (115, 1451, 1417132800, 1414108800, 1571788800, 14,
        'пр. Победы  168  автобусн. ост. «Теплотехнический институт» (из центра)', 61.398493, 55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (116, 1452, 1417132800, 1414108800, 1571788800, 14,
        'ул. Кирова  1б  автобусн. ост. «Теплотехнический институт» (из центра)', 61.372056, 55.040446);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (117, 1453, 1417132800, 1414108800, 1571788800, 14, 'ул. Бр. Кашириных  2  автобусн. ост. «Цирк» (из центра)',
        61.39755, 55.174049);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (118, 1454, 1417132800, 1414108800, 1571788800, 14,
        'ул. Цвиллинга  напротив д. 5  автобусн. ост. «Оперный театр» (в центр)', 61.403946, 55.167226);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (119, 1455, 1417132800, 1414108800, 1571788800, 14,
        'ул. Цвиллинга  7  трамвайн. ост. «Оперный театр» (из центра)', 61.403955, 55.166958);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (120, 1456, 1417132800, 1414108800, 1571788800, 14,
        'ул. Калинина  34  троллейбусн. ост. «ул. Калинина» (из центра)', 61.433303, 55.044908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (121, 1457, 1417132800, 1414108800, 1571788800, 14,
        'Свердловский пр.  35а  троллейбусн. ост. «ул. Калинина» (в  центр)', 61.388181, 55.17865);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (122, 1458, 1417132800, 1414108800, 1571788800, 14,
        'Свердловский пр.  16  троллейбусн. ост. «ул. Островского» (в  центр)', 61.386052, 55.187738);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (123, 1459, 1417132800, 1414108800, 1571788800, 14,
        'пр. Комсомольский  10/1  троллейбусн. ост. «ул. Краснознаменная» (из центра)', 61.378317, 55.192261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (124, 1460, 1417132800, 1414108800, 1571788800, 14,
        'ул. Краснознаменная  1а/1  троллейбусн. ост. «ул. Краснознаменная» (в центр)', 61.380141, 55.191808);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (125, 1461, 1417132800, 1414108800, 1571788800, 14,
        'пр. Комсомольский  14  троллейбусн. ост. «ул. Косарева» (из центра)', 61.37069, 55.192256);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (126, 1462, 1417132800, 1414108800, 1571788800, 14,
        'пр. Комсомольский  23д  троллейбусн. ост. «ул. Косарева» (в центр)', 61.372119, 55.191485);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (127, 1463, 1417132800, 1414108800, 1571788800, 14,
        'пр. Комсомольский  61  троллейбусн. ост. «ул. Ворошилова» ( в центр)', 61.324122, 55.194023);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (128, 1464, 1417132800, 1414108800, 1571788800, 14,
        'Комсомольский пр.  85  троллейбусн. ост. «8-й микрорайон» (в центр)', 61.308527, 55.194018);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (129, 1465, 1417132800, 1414108800, 1571788800, 14,
        'Комсомольский пр.  103  автобусн. ост. «11-й микрорайон» (в центр)', 61.294917, 55.190195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (130, 1466, 1417132800, 1414108800, 1571788800, 14,
        'ул.Чичерина/ пр. Победы  392  автобусн. ост. «ул. Чичерина» (из центра)', 61.292456, 55.183554);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (131, 1467, 1417132800, 1414108800, 1571788800, 14, 'пр. Победы  327  автобусн.  ост. «ул. Чичерина» ( в центр)',
        61.293283, 55.182773);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (132, 1468, 1417132800, 1414108800, 1571788800, 14,
        'ул.Чичерина/ пр. Победы  напротив д. 325  автобусн. ост. «ул. Чичерина» (в центр)', 61.293651, 55.182444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (133, 1469, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных  95б  троллейбусн. ост. «ул. Северо-Крымская» (в центр)', 61.357629, 55.177915);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (134, 1470, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных/ ул. Партизанская  62  троллейбусн.  ост. «ул. Северо-Крымская» (из центра)', 61.357323,
        55.179087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (135, 1471, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных/ ул. Молодогвардейцев  70а  автобусн. ост. «ул. Молодогвардейцев» ( из центра)', 61.330994,
        55.179344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (136, 1472, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных/ ул. Ворошилова  57  автобусн. ост. «ул. Ворошилова» (из центра)', 61.325712, 55.179318);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (137, 1473, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных  102  автобусн. ост. «ул. Солнечная» (из центра)', 61.316998, 55.179082);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (138, 1474, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных  129а  автобусн. ост. «Челябинский государственный университет» (в центр)', 61.322747,
        55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (139, 1475, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных  108/1  автобусн. ост. «24-й микрорайон» (в центр)', 61.313054, 55.178203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (140, 1476, 1417132800, 1414108800, 1571788800, 14,
        'ул. 40-летия   36  автобусн. ост. «ул. 40-летия Победы» (в центр)Победы', 61.448287, 55.157577);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (141, 1477, 1417132800, 1414108800, 1571788800, 14,
        'ул. Чичерина  29  автобусн.ост. «ул. 250 лет Челябинску» (из центра)', 61.297729, 55.179216);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (142, 1478, 1417132800, 1414108800, 1571788800, 14,
        'ул. Гагарина  6  автобусн. ост. «Политехнический техникум» (в сторону ул. Новороссийская)', 61.397298,
        55.074216);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (143, 1479, 1417132800, 1414108800, 1571788800, 14,
        'ул. Гагарина  32  троллейбусн. ост. «кинотеатр «Аврора» (в сторону ул. Новороссийская)', 61.397415, 55.076891);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (144, 1480, 1417132800, 1414108800, 1571788800, 14,
        ' ул. Гагарина  35а  троллейбусн. ост. «Управление соцзащиты населения» (в сторону КБС)', 61.396831, 55.077437);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (145, 1481, 1417132800, 1414108800, 1571788800, 14,
        'ул. Дзержинского  130  автобусн. ост. «ул. Барбюса» (в центр)', 61.425577, 55.133961);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (146, 1482, 1417132800, 1414108800, 1571788800, 14,
        'ул. Дзержинского  109  автобусн. ост. «ул. Барбюса» (из центра) ', 61.426862, 55.132659);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (147, 1483, 1417132800, 1414108800, 1571788800, 14,
        'ул. Гагарина  39  троллейбусн. ост. «библиотека им. Мамина-Сибиряка» (в центр)', 61.396831, 55.077989);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (148, 1484, 1417132800, 1414108800, 1571788800, 14,
        'ул. Гагарина  55а  троллейбусн. ост. «магазин Спорттовары» (в центр)', 61.435872, 55.117875);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (149, 1485, 1417132800, 1414108800, 1571788800, 14,
        'ул. 40-летия Победы/ пр. Комсомольский  105  автобусн. ост. «11-й микрорайон» (в центр)', 61.29242, 55.18909);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (150, 1486, 1417132800, 1414108800, 1571788800, 14,
        'Комсомольский пр.  100  автобусн. ост. «11-й микрорайон» (из центра)', 61.292276, 55.190359);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (151, 1487, 1417132800, 1414108800, 1571788800, 14,
        'ул. Молодогвардейцев  54  троллейбусн. ост. «пр. Победы» (в центр)', 61.332009, 55.188077);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (152, 1488, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных/ ул. Парковая  5  автобусн. ост. «ул. Партизанская» (из центра)', 61.306488, 55.151821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (153, 1489, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных  89  автобусн. ост. «ул. Партизанская» (в центр)', 61.366567, 55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (154, 1490, 1417132800, 1414108800, 1571788800, 14,
        'ул. Бр. Кашириных  73  автобусн. ост. «ул. Полковая» (в центр)', 61.378551, 55.174763);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (155, 1491, 1417132800, 1414108800, 1571788800, 14,
        'ул. 40-летия Победы  35  автобусн. ост. «ул. 40-летия Победы» (из центра)', 61.305221, 55.181961);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (156, 1492, 1417132800, 1414108800, 1571788800, 14,
        'ул.Героев танкограда/ пр. Ленина  8а  троллейбусн. ост. «Театр ЧТЗ» (из центра)', 61.451943, 55.160648);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (157, 1493, 1417132800, 1414108800, 1571788800, 14,
        'Привокзальная площадь  1  ост. «ж/д вокзал» (автобус №18  в сторону центра) ', 61.416136, 55.141315);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (158, 1494, 1417132800, 1414108800, 1571788800, 14,
        'Привокзальная площадь  1  ост. «ж/д вокзал» (автобус № 64  в сторону центра)', 61.416136, 55.141315);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (159, 1495, 1417132800, 1414108800, 1571788800, 14,
        'ул. Кирова  7  автобусн. ост. «ул. Калинина» (в сторону Теплотехнического института)', 61.373125, 55.040647);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (160, 1496, 1417132800, 1414108800, 1571788800, 14,
        'ул.Энгельса/ пр. Ленина  73  автобусн. ост. «Агроинженерная академия» (из центра)', 61.380302, 55.159393);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (161, 1521, 1418169600, 1414108800, 1571788800, 9, 'ул.Красная 42-ул.К.Либкнехта  до перекр.из центра',
        61.393391, 55.156837);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (162, 1522, 1418169600, 1414108800, 1571788800, 9, 'ул.Свободы 44 - ул.Коммуны  до перекр.в центр', 61.410791,
        55.164207);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (163, 1523, 1418169600, 1414108800, 1571788800, 9,
        'ул.Коммуны  87 - ул.Елькина  до перекр.в центр  у рест. "Бад Гаштейн"', 61.397748, 55.161795);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (164, 1524, 1418169600, 1414108800, 1571788800, 9, 'ул.Цвиллинга 10- ул.К.Маркса  за перекр.в центр', 61.403048,
        55.164932);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (165, 1525, 1418169600, 1414108800, 1571788800, 9, 'пр.Ленина 76  из центра  у ЮУРГУ  поз.1', 61.37016,
        55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (166, 1526, 1418169600, 1414108800, 1571788800, 9, 'пр.Ленина 76  из центра  у ЮУРГУ  поз.2', 61.37016,
        55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (167, 1527, 1418169600, 1414108800, 1571788800, 9, 'пр.Ленина 76  из центра  у ЮУРГУ  поз.3', 61.37016,
        55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (168, 1528, 1418169600, 1414108800, 1571788800, 9, 'ул.Бр.Кашириных  в центр  до ост. "Солнечная"', 61.348969,
        55.178583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (169, 1529, 1418169600, 1414108800, 1571788800, 9, 'ул.Дзержинского 104 - ул.Гагарина  100м за перекр.в центр',
        61.433878, 55.13269);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (170, 1530, 1418169600, 1414108800, 1571788800, 9, 'ул.К.Маркса 52 - ул.Пушкина  за перекр.в центр', 61.408069,
        55.165678);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (171, 1531, 1418169600, 1414108800, 1571788800, 9, 'ул.Красная-ул.К.Маркса  до перекр.в центр  у сквера',
        61.393114, 55.165142);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (172, 1532, 1418169600, 1414108800, 1571788800, 9,
        'пр Ленина 50-ул.Советская  100м до перекр.в центр  у маг. "Ювелирный зал"', 61.4078, 55.160843);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (173, 1533, 1418169600, 1414108800, 1571788800, 9, 'пр.Ленина 50 - ул. Советская  до перекр.в центр', 61.4078,
        55.160843);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (174, 1534, 1418169600, 1414108800, 1571788800, 9, 'ул.Свободы 60 - ул.Коммуны  за перекр.в центр', 61.410818,
        55.162669);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (175, 1536, 1418256000, 1414108800, 1571788800, 6, 'Копейское шоссе  50 м до поворота на Мясокомбинат',
        61.527303, 55.118766);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (176, 1537, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Ленина 72 и ул.Энтузиастов', 61.374975,
        55.16055);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (177, 1538, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Ленина 66а и ул.Энгельса', 61.380051,
        55.160411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (178, 1539, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Ленина 62 и ул.Васенко 96', 61.394693,
        55.160483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (179, 1540, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Ленина 45 и ул.Свободы 70', 61.410234,
        55.160278);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (180, 1541, 1418256000, 1414108800, 1571788800, 11, 'пр.Ленина 28 (Агентство аэрофлота)', 61.424706, 55.161615);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (181, 1542, 1418256000, 1414108800, 1571788800, 11, 'пр.Ленина 54 (середина дома)', 61.402419, 55.160818);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (182, 1543, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Ленина 54 и ул.Цвиллинга', 61.402419,
        55.160818);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (183, 1544, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Ленина и ул.Кирова 177', 61.401287,
        55.161003);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (184, 1545, 1418256000, 1414108800, 1571788800, 11,
        'пересечение ул.Воровского (в центр) и ул.Тимирязева  за перекрестком', 61.397661, 55.157809);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (185, 1546, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр. Ленина 43 и ул. Свободы', 61.412633,
        55.160324);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (186, 1547, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр. Ленина 18  и ул.Горького ', 61.442933,
        55.16108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (187, 1548, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр. Ленина 29 и ул. 3 Интернационала',
        61.420583, 55.160545);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (188, 1549, 1418256000, 1414108800, 1571788800, 11, 'пересечение пр.Победы 168 и ул.Кирова', 61.398493,
        55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (189, 1563, 1418342400, 1417219200, 1574899200, 8, 'ул.Пушкина/ул.Тимирязева 27', 61.410558, 55.158061);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (190, 1564, 1418342400, 1414108800, 1729641600, 12, 'Свердловский тр. 8', 61.36645, 55.232654);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (191, 1565, 1418342400, 1417219200, 1574899200, 16,
        'ул. Героев Танкограда  75  ост. «ул. Первой Пятилетки» (из центра)', 61.454054, 55.170347);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (192, 1566, 1418342400, 1417219200, 1574899200, 16,
        'ул. Героев Танкограда  75  ост. «ул. Первой Пятилетки» (в центр)', 61.454054, 55.170347);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (193, 1567, 1418342400, 1417219200, 1574899200, 16, 'ул. Воровского  64', 61.37361, 55.14006);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (194, 1568, 1418342400, 1417219200, 1574899200, 16, 'ул. Кирова  7  ост. «ул. Калинина»', 61.400137, 55.180038);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (195, 1569, 1418342400, 1417219200, 1574899200, 16, 'ул. Кирова  44  ост. «ул. Калинина»', 61.398682, 55.178352);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (196, 1570, 1418342400, 1417219200, 1574899200, 16, 'ул. Братьев Кашириных  102', 61.316998, 55.179082);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (197, 1571, 1418342400, 1417219200, 1574899200, 16, 'ул. Братьев Кашириных  114', 61.308985, 55.176331);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (198, 1572, 1418342400, 1417219200, 1574899200, 16, 'ул. Братьев Кашириных  83', 61.370376, 55.1772);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (199, 1573, 1418342400, 1417219200, 1574899200, 16, 'пр. Ленина  87', 61.365938, 55.158904);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (200, 1574, 1418342400, 1417219200, 1574899200, 16, 'ул. Братьев Кашириных  68а', 61.348008, 55.179421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (201, 1575, 1418860800, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. и ул.Труда на кольце в центр  напротив д.161', 61.389624, 55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (202, 1576, 1418860800, 1417219200, 1574899200, 6, 'ул.Цвиллинга 58', 61.41027, 55.150972);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (203, 1577, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул.Бр.Кашириных и ул.1-го Мая  100 м за перекрестком из центра', 61.394886, 55.174323);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (204, 1578, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул. Труда и Свердловского пр.  на кольце (в центр)', 61.389624, 55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (205, 1579, 1418860800, 1417219200, 1574899200, 6, 'ул.Черкасская 4', 61.378578, 55.241421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (206, 1580, 1418860800, 1417219200, 1574899200, 6, 'Троицкий тр.  60/1', 61.38907, 55.089186);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (207, 1581, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул. Румянцева и ул.Богдана Хмельницкого',
        61.379367, 55.258329);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (208, 1582, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул.Красная и ул. Труда  за перекрестком из центра', 61.393084, 55.167587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (209, 1583, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул.Бр.Кашириных и ул.Тагильская  за перекрестком в центр у ТЦ', 61.389879, 55.174325);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (210, 1584, 1418860800, 1417219200, 1574899200, 6, 'пересечение Комсомольского пр.  14 и ул. Косарева', 61.37069,
        55.192256);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (211, 1585, 1418860800, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. и ул.Труда напротив ДС Юность  на кольце из центра поз.1', 61.389624, 55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (212, 1586, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул. Воровского и ул. Блюхера  со стороны Областной больницы', 61.374822, 55.136254);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (213, 1587, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул.Энгельса 23 и ул.Труда', 61.381335,
        55.166953);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (214, 1588, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул.Бр.Кашириных и Свердловского пр.  за перекр. в центр  у авт.ост."Свердловский пр."', 61.387638,
        55.174127);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (215, 1589, 1418860800, 1417219200, 1574899200, 6, 'ул. Гагарина  3', 61.445143, 55.144222);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (216, 1590, 1418860800, 1417219200, 1574899200, 6,
        'пересечение ул. Черкасская  напротив д. 6 и ул. 50 лет ВЛКСМ  на кольце', 61.378919, 55.239727);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (217, 1591, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул.Труда и ул.Красная поз.1', 61.39292,
        55.167583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (218, 1592, 1418860800, 1417219200, 1574899200, 6,
        'пересечение пр. Победы и ул. Косарева  до перекрестка в центр', 61.372157, 55.18359);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (219, 1593, 1418860800, 1417219200, 1574899200, 6, 'ул. Дзержинского  93', 61.434372, 55.131244);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (220, 1594, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул.Труда и ул.Красная поз.2', 61.39292,
        55.167583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (221, 1597, 1418860800, 1417219200, 1574899200, 6, 'пересечение пр. Победы  149 и ул. Болейко', 61.405177,
        55.183456);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (222, 1598, 1418860800, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. и ул.Труда напротив ДС Юность  на кольце из центра  поз.2', 61.389624,
        55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (223, 1599, 1418860800, 1417219200, 1574899200, 6, 'Троицкийтр.  48Б', 61.38739, 55.1132);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (224, 1600, 1418860800, 1417219200, 1574899200, 6, 'ул. Кирова  7', 61.400137, 55.180038);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (225, 1601, 1418860800, 1417219200, 1574899200, 6, 'ул. Молодогвардейцев  33Б', 61.333464, 55.187106);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (226, 1602, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул. Блюхера и ул. Крупской  34', 61.391783,
        55.147382);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (227, 1603, 1418860800, 1417219200, 1574899200, 6, 'ул. Воровского  43А', 61.386725, 55.149213);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (228, 1604, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул.Бр. Кашириных и ул. Краснознаменная  41/1',
        61.380716, 55.175493);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (229, 1605, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул. Калинина  17 и ул. Кыштымская', 61.428362,
        55.044469);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (230, 1607, 1418860800, 1417219200, 1574899200, 6, 'Свердловский тр.  10/3', 61.367888, 55.231724);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (231, 1608, 1418860800, 1417219200, 1574899200, 6, 'пересечение ул. Елькина  63 и ул. Карла Либкнехта',
        61.399167, 55.15624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (232, 1609, 1418860800, 1417219200, 1574899200, 6,
        'дорога в Аэропорт  на кольце дорожной развязки в Металлургический район', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (233, 1610, 1418860800, 1417219200, 1574899200, 6, 'ул. Новороссийская  8', 61.49278, 55.100886);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (234, 1611, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина - Энтузиастов  до перекрестка в центр', 61.37541,
        55.159689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (235, 1612, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина -ул.Свободы до перекрестка в центр', 61.411447,
        55.160654);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (236, 1613, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 30', 61.422918, 55.161281);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (237, 1614, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 43-ул.Свободы  за перекр.из центра', 61.412633,
        55.160324);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (238, 1615, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 45-ул.Пушкина  за перекр.из центра', 61.410234,
        55.160278);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (239, 1616, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 48-ул.Пушкина  до перекр.в центр', 61.409372,
        55.16091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (240, 1617, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 50-ул.Советская  до перекр. в центр', 61.4078,
        55.160843);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (241, 1618, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 51-ул.Советская', 61.406641, 55.160128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (242, 1619, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 52-ул.Цвиллинга  до перекр.в центр', 61.405698,
        55.160828);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (243, 1620, 1418860800, 1414108800, 1571788800, 8,
        'пр.Ленина 54-ул.Кирова  до перекр.из центра  у пиццерии"Помидор"', 61.402419, 55.160818);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (244, 1621, 1418860800, 1414108800, 1571788800, 8,
        'пр.Ленина 55-ул.Воровского  до перекр.в центр  у Арбитражного суда', 61.399634, 55.159928);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (245, 1622, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 61-ул.Васенко  до перекр.в центр  у "Сбербанк"',
        61.394855, 55.159794);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (246, 1623, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 63-ул.Красная  до перекр.в центр', 61.392888,
        55.159768);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (247, 1624, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 64-ул.Энгельса  до перекр.из центра', 61.383842,
        55.160689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (248, 1625, 1418860800, 1414108800, 1571788800, 8,
        'пр.Ленина 64б-ул.Володарского  32  за перекрестком из центра', 61.383842, 55.160689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (249, 1626, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина 73-ул.Энгельса  до перекрестка в центр', 61.380302,
        55.159393);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (250, 1627, 1418860800, 1414108800, 1571788800, 8,
        'пр.Ленина-ул.Красная  за перекр.из центра  у памятника "Орленок"', 61.393519, 55.160142);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (251, 1628, 1418860800, 1414108800, 1571788800, 8,
        'пр.Ленина 67-Свердловский пр.  за перекр. в центр  авт.ост. "Алое поле"', 61.389744, 55.159676);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (252, 1629, 1418860800, 1414108800, 1571788800, 8, 'пр.Ленина-ул.Горького  у Комсомольской площади  из центра',
        61.44129, 55.160835);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (253, 1630, 1418860800, 1414108800, 1571788800, 8,
        'пр.Победы 160-А - ул.Болейко  за перекр. в центр  у маг. "Гавань"', 61.403632, 55.184937);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (254, 1631, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр. 27-пр.Победы  до перекр.из центра',
        61.387588, 55.183441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (255, 1632, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр.-пр.Победы  192А  до перекр.в центр',
        61.386474, 55.184377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (256, 1633, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр. 65 - ул. С. Кривой  за перекр. в центр',
        61.389582, 55.158215);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (257, 1634, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр.  26-пр.Победы', 61.386366, 55.184747);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (258, 1635, 1418860800, 1414108800, 1571788800, 8,
        'Свердловский пр.-ул.Калинина  у КПП Автомоб. инст.  до перекр. в центр', 61.38745, 55.179306);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (259, 1636, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр.82-ул.К.Либкнехта  до перекр. из центра',
        61.388504, 55.155875);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (260, 1637, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр. 71-ул.К.Либкнехта  за перекр. в центр',
        61.389645, 55.156147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (261, 1638, 1418860800, 1414108800, 1571788800, 8, 'Свердловский пр. 76-ул.С.Кривой 41 за перекр. из центра',
        61.388567, 55.158678);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (262, 1639, 1418860800, 1414108800, 1571788800, 8, 'ул.Васенко 96-пр.Ленина  до перекр.в центр  у Дом быта',
        61.395448, 55.160987);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (263, 1640, 1418860800, 1414108800, 1571788800, 8,
        'ул.Воровского 2-пр.Ленина  за перекр.в центр  у Арбитражного суда', 61.39923, 55.159486);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (264, 1641, 1418860800, 1414108800, 1571788800, 8, 'ул.Воровского 1-пр.Ленина 150м до перекр.в центр', 61.399805,
        55.159177);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (265, 1642, 1418860800, 1414108800, 1571788800, 8, 'ул.Воровского 9-ул.Красная  за перекр. в центр', 61.396059,
        55.155684);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (266, 1643, 1418860800, 1414108800, 1571788800, 8, 'ул.Воровского 7-ул.Красная', 61.396715, 55.156127);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (267, 1644, 1418860800, 1414108800, 1571788800, 8, 'ул.Воровского 11-ул.Красная', 61.394864, 55.155288);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (268, 1645, 1418860800, 1414108800, 1571788800, 8,
        'ул.Воровского-ул.С.Кривой  за перекр.из центра  до ост."Киномакс Урал" ', 61.397307, 55.157727);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (269, 1646, 1418860800, 1414108800, 1571788800, 8, 'ул. Свободы 70 - пр.Ленина', 61.411124, 55.159861);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (270, 1647, 1418860800, 1414108800, 1571788800, 8, 'ул.Елькина -ул. Тимирязева  до перекрестка из центра ',
        61.398679, 55.157538);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (271, 1648, 1418860800, 1414108800, 1571788800, 8, 'ул.Елькина-ул.Тимирязева  за перекрестком из центра',
        61.398679, 55.157538);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (272, 1649, 1418860800, 1414108800, 1571788800, 8, 'ул.Кирова 2-пр.Победы  за перекр.в центр', 61.372388,
        55.03977);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (273, 1650, 1418860800, 1414108800, 1571788800, 8, 'ул.Кирова 9а - ул.Калинина  до перекр.в центр', 61.373125,
        55.040647);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (274, 1652, 1418860800, 1414108800, 1571788800, 8, 'ул.Коммуны  70-ул.Красная  до перекр.из центра  у ФСБ',
        61.394666, 55.163528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (275, 1653, 1418860800, 1414108800, 1571788800, 8, 'ул.Красная 40-ул.С.Кривой  за перекр.из центра', 61.3934,
        55.157577);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (276, 1654, 1418860800, 1414108800, 1571788800, 8, 'ул.Красная - ул.К.Либкнехта  за перекр. из центра', 61.39402,
        55.155772);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (277, 1655, 1418860800, 1414108800, 1571788800, 8, 'ул.Красная 71-ул.С.Кривой за перекр.в центр', 61.394136,
        55.158503);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (278, 1656, 1418860800, 1414108800, 1571788800, 8, 'ул.Красная  11-ул.К.Маркса', 61.394433, 55.165575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (279, 1657, 1418860800, 1414108800, 1571788800, 8, 'ул.Пушкина 56-пр.Ленина  до перекрестка в центр', 61.408599,
        55.161126);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (280, 1658, 1418860800, 1414108800, 1571788800, 8, 'ул.Пушкина 59 - пр. Ленина  за перекрестком из центра',
        61.409345, 55.160895);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (281, 1659, 1418860800, 1414108800, 1571788800, 8, 'ул.Пушкина 65-ул.Тимирязева  за перекр. в центр', 61.40965,
        55.159182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (282, 1660, 1418860800, 1414108800, 1571788800, 8, 'ул.Пушкина -пр.Ленина  47 за перекр.из центра', 61.408689,
        55.160134);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (283, 1661, 1418860800, 1414108800, 1571788800, 8, 'ул.Пушкина 60', 61.408851, 55.159588);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (284, 1662, 1418860800, 1414108800, 1571788800, 8, 'ул.С.Кривой  69А-ул.Энтузиастов  за перекр. в центр',
        61.376763, 55.157222);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (285, 1663, 1418860800, 1414108800, 1571788800, 8, 'ул.Свободы 149-ул. Плеханова  за пер. в центр', 61.41398,
        55.156225);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (286, 1664, 1418860800, 1414108800, 1571788800, 8, 'ул.Свободы 151-ул.Плеханова  до перекр. в центр', 61.414295,
        55.155263);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (287, 1665, 1418860800, 1414108800, 1571788800, 8, 'ул.Свободы 159-ул.Евтеева  за перекр. в центр', 61.41584,
        55.151091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (288, 1666, 1418860800, 1414108800, 1571788800, 8, 'ул.Свободы 78-ул.Тимирязева  за перекр.из центра', 61.412273,
        55.157757);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (289, 1667, 1418860800, 1414108800, 1571788800, 8, 'ул.Свободы  82-ул.Плеханова  до перекр. из центра',
        61.412938, 55.156194);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (290, 1668, 1418860800, 1414108800, 1571788800, 8, 'ул.Советская 38-пр.Ленина  до перекрестка в центр',
        61.370484, 55.039847);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (291, 1669, 1418860800, 1414108800, 1571788800, 8, 'ул.Тимирязева 27-ул.Пушкина  за перекр. из центра ',
        61.410558, 55.158061);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (292, 1670, 1418860800, 1414108800, 1571788800, 8, 'ул.Тимирязева 29-ул.Пушкина  до перекр.из центра', 61.40868,
        55.157644);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (293, 1671, 1418860800, 1414108800, 1571788800, 8, 'ул.Тимирязева 31-ул.Цвиллинга  за перекр. из центра ',
        61.407126, 55.157541);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (294, 1672, 1418860800, 1414108800, 1571788800, 8, 'ул.Тимирязева-ул. Елькина  59  до перекр. в центр',
        61.398978, 55.158009);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (295, 1673, 1418860800, 1414108800, 1571788800, 8, 'ул.Цвиллинга  28 - ул. Коммуны  за перекр. в центр',
        61.403533, 55.162715);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (296, 1674, 1418860800, 1414108800, 1571788800, 8, 'ул.Цвиллинга-ул.Комунны  за перекр. из центра', 61.404037,
        55.162888);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (297, 1675, 1418860800, 1414108800, 1571788800, 8, 'ул.Энгельса - пр.Ленина 71-А  до перекр.в центр', 61.382027,
        55.15945);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (298, 1676, 1418860800, 1414108800, 1571788800, 8, 'ул.Энтузиастов 7-ул.С.Кривой  за перекр.в центр', 61.376035,
        55.158231);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (299, 1677, 1418860800, 1414108800, 1571788800, 8, 'ул.Энтузиастов 12 - ул. С. Кривой  за перекр. из центра',
        61.375335, 55.156801);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (300, 1678, 1418860800, 1414108800, 1571788800, 8, 'ул.Цвиллинга 33 - пр. Ленина  до перекр. в центр', 61.405518,
        55.159871);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (301, 1680, 1418947200, 1417564800, 1575244800, 6, 'Свердловский тр.  8', 61.36645, 55.232654);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (302, 1681, 1418947200, 1417219200, 1574899200, 6,
        'пр. Победы/ул. Чичерина  30к1  у поворота на трамвайное кольцо', 61.290668, 55.181426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (303, 1682, 1418947200, 1417219200, 1574899200, 6, 'пересечение ул. Городская и ул. Олонецкая', 61.281994,
        55.180426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (304, 1683, 1418947200, 1417219200, 1574899200, 6, 'Комарова пр.  54', 61.464358, 55.185692);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (305, 1684, 1418947200, 1417219200, 1574899200, 6, 'Троицкий тр.  500 м до виадука к посёлку Н-Синеглазово',
        61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (306, 1685, 1418947200, 1417219200, 1574899200, 6, 'дорога в Аэропорт  у поста ГИБДД  у виадука  слева',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (307, 1688, 1418947200, 1417564800, 1575244800, 5, 'ул.Воровского  21  30 м после ост. "Горбольница" (в центр)',
        61.390786, 55.151528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (308, 1689, 1418947200, 1417219200, 1574899200, 6, 'Троицкий тр.  напротив д. 19', 61.383527, 55.093695);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (309, 1690, 1418947200, 1414108800, 1571788800, 11, 'ул.Цвиллинга 44', 61.406264, 55.157217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (310, 1691, 1418947200, 1414108800, 1571788800, 11, 'пр.Ленина 83', 61.371373, 55.158241);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (311, 1692, 1418947200, 1414108800, 1571788800, 11, 'пересеч.пр.Ленина 71 и ул.Володарского 52а', 61.383941,
        55.15947);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (312, 1693, 1418947200, 1414108800, 1571788800, 11, 'пересеч.пр.Ленина 30 и ул.3-го Интернационала', 61.422918,
        55.161281);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (313, 1694, 1418947200, 1414108800, 1571788800, 11, 'пр.Победы 170', 61.396939, 55.184562);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (314, 1695, 1418947200, 1414108800, 1571788800, 11,
        'пересеч.ул.Воровского (из центра) и Свердловского пр. (в центр)', 61.389446, 55.151841);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (315, 1696, 1418947200, 1414108800, 1571788800, 11, 'пересеч.ул.Свободы (в центр) и ул.Тимирязева (в центр)',
        61.412563, 55.158535);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (316, 1700, 1419206400, 1417219200, 1574899200, 6, 'Троицкий тр. 46', 61.387525, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (317, 1701, 1419206400, 1417219200, 1574899200, 6, 'пересечение ул.С.Юлаева и пр.Победы', 61.284198, 55.178986);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (318, 1702, 1419206400, 1417219200, 1574899200, 6, 'Свердловский пр. напротив д.62', 61.388342, 55.161821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (319, 1703, 1419379200, 1418688000, 1576368000, 6, 'ул.Ун. Набережная  напротив д. 185 по ул.Чайковского поз.2',
        61.355644, 55.177298);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (320, 1704, 1419379200, 1418688000, 1576368000, 6, 'ул.Новороссийская  напротив д. 25', 61.48686, 55.105825);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (321, 1706, 1419379200, 1418688000, 1576368000, 6, 'ул.С. Юлаева  напротив д.29', 61.294567, 55.171879);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (322, 1707, 1419379200, 1418688000, 1576368000, 6, 'ул.Ун. набережная  напротив д. 185 по ул.Чайковского  поз.1',
        61.355644, 55.177298);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (323, 1708, 1419379200, 1418688000, 1576368000, 6, 'пересечение ул.С.Юлаева и пр.Победы (из центра)', 61.284198,
        55.178986);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (324, 1709, 1419379200, 1418688000, 1576368000, 6, 'Свердловский пр.  напротив д.48', 61.388019, 55.165652);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (325, 1710, 1419379200, 1418688000, 1576368000, 6, 'ул.Курчатова 7', 61.393166, 55.14752);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (326, 1711, 1419379200, 1418688000, 1576368000, 6,
        'ул.Ун. набережная  напротив д. 185 по ул.Чайковского  поз. 3', 61.355644, 55.177298);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (327, 1712, 1419379200, 1418688000, 1576368000, 6, 'ул.Бр.Кашириных 89', 61.366567, 55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (328, 1713, 1419379200, 1418688000, 1576368000, 6, 'ул.Молодогвардейцев 19', 61.331703, 55.19927);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (329, 1714, 1419379200, 1418688000, 1576368000, 6,
        'ул.С.Юлаева  напротив д. 34  на пересечении с ул.250 лет Челябинска ', 61.272145, 55.187476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (330, 1715, 1419379200, 1418688000, 1576368000, 6, 'ул.Бр.Кашириных напротив д.101', 61.344208, 55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (331, 1716, 1419379200, 1418688000, 1576368000, 6, 'пересечение ул. Университеская Набережная и ул. Чайковского',
        61.353266, 55.174503);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (332, 1717, 1419465600, 1417219200, 1574899200, 6, 'пересечение пр. Победы  315 и ул. 40 лет Победы', 61.299876,
        55.184849);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (333, 1719, 1419552000, 1409529600, 1567209600, 15, 'ул.Черкасская 19 поз. 1', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (334, 1720, 1419552000, 1409529600, 1567209600, 15, 'ул.Черкасская 19 поз. 2', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (335, 1721, 1419552000, 1409529600, 1567209600, 15, 'ул.Черкасская 19 поз. 3', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (336, 1722, 1419552000, 1409529600, 1567209600, 15, 'ул.Черкасская 19 поз. 1', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (337, 1723, 1419552000, 1409529600, 1567209600, 15, 'ул.Черкасская 19 поз. 2', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (338, 1724, 1419552000, 1409529600, 1567209600, 15, 'ул.Черкасская 19 поз. 3', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (339, 1725, 1419552000, 1409529600, 1567209600, 12, 'ул.Черкасская 19 поз. 1', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (340, 1726, 1419552000, 1409529600, 1567209600, 12, 'ул.Черкасская 19 поз. 2', 61.375389, 55.228259);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (341, 1734, 1419552000, 1417219200, 1574899200, 6, 'ул.Карла Либкнехта  20', 61.391684, 55.155978);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (342, 1735, 1419552000, 1417219200, 1574899200, 6, 'ул.Российская  262', 61.417627, 55.151652);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (343, 1736, 1419552000, 1417219200, 1574899200, 6, 'ул.Калинина  17(середина дома)', 61.393139, 55.179108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (344, 1737, 1419552000, 1417219200, 1574899200, 6, 'Западное шоссе (ул. Центральная 3в)', 61.301493, 55.147706);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (345, 1738, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Доватора и ул. Федорова  возле АЗС Лукойл',
        61.402055, 55.137945);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (346, 1739, 1419552000, 1417219200, 1574899200, 6, 'ул.Блюхера  напротив д. 69', 61.366738, 55.128223);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (347, 1740, 1419552000, 1417219200, 1574899200, 6, 'Гидрострой  13а', 61.30276, 55.149553);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (348, 1741, 1419552000, 1417219200, 1574899200, 6, 'пересечение пр.Победы 398/1и и ул.Чичерина', 61.2888,
        55.181966);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (349, 1742, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Труда и ул.Свободы  напротив д. 16',
        61.426601, 55.168593);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (350, 1743, 1419552000, 1417219200, 1574899200, 6, 'ул.Центральная  напротив д. 3в', 61.301493, 55.147706);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (351, 1744, 1419552000, 1417219200, 1574899200, 6,
        'пересечение ул. Кожзаводской и ул.Каслинской  напротив д. 5а', 61.394541, 55.191264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (352, 1745, 1419552000, 1417219200, 1574899200, 6, 'пересечение Свердловского пр. и Комсомольского пр. 2',
        61.384614, 55.193217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (353, 1746, 1419552000, 1417219200, 1574899200, 6, 'пересечение пр. Победы 115 и ул. Горького 68/1', 61.433806,
        55.185764);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (354, 1747, 1419552000, 1417219200, 1574899200, 6, 'ул.Гагарина 23', 61.439807, 55.136267);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (355, 1748, 1419552000, 1417219200, 1574899200, 6, 'ул.Свободы 145', 61.41327, 55.157855);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (356, 1749, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Гостевой и ул. Центральная 1а', 61.373646,
        55.123173);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (357, 1750, 1419552000, 1417219200, 1574899200, 6, 'ул.Худякова 13', 61.375209, 55.147361);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (358, 1751, 1419552000, 1417219200, 1574899200, 6, 'ул.Степана Разина 9/1', 61.413639, 55.140091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (359, 1752, 1419552000, 1417219200, 1574899200, 6, 'ул.Энгельса 32/1', 61.380877, 55.158369);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (360, 1753, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Чичерина и пр.Победы 325', 61.293651,
        55.182444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (361, 1754, 1419552000, 1417219200, 1574899200, 6, 'ул.Горького 22', 61.442852, 55.170496);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (362, 1755, 1419552000, 1417219200, 1574899200, 6, 'ул.Труда 166', 61.374562, 55.170784);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (363, 1756, 1419552000, 1417219200, 1574899200, 6, 'ул.Энтузиастов 32', 61.375541, 55.148699);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (364, 1757, 1419552000, 1417219200, 1574899200, 6, 'Комсомольский пр. 64', 61.326161, 55.195159);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (365, 1759, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Энгельса напротив д.58 и ул. Худякова',
        61.3797, 55.148272);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (366, 1760, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Артиллерийской 136 и ул.Ловина', 61.430922,
        55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (367, 1761, 1419552000, 1417219200, 1574899200, 6, 'ул.Российская 159', 61.416477, 55.166799);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (368, 1762, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Коммуны 100 и ул. Энтузиастов', 61.374095,
        55.162551);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (369, 1763, 1419552000, 1417219200, 1574899200, 6, 'ул. Труда 153', 61.395933, 55.167313);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (370, 1764, 1419552000, 1417219200, 1574899200, 6, 'ул.Энгельса 51', 61.381668, 55.1627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (371, 1765, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Худякова 12а и ул.Верхнеуральской', 61.37423,
        55.148575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (372, 1766, 1419552000, 1417219200, 1574899200, 6, 'ул.Блюхера 3', 61.388567, 55.145313);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (373, 1767, 1419552000, 1417219200, 1574899200, 6, 'ул.Энгельса 69', 61.38182, 55.158714);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (374, 1768, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Краснознаменная 41/1 и ул.Братьев Кашириных',
        61.380716, 55.175493);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (375, 1769, 1419552000, 1417219200, 1574899200, 6, 'пересечение Комсомольского пр. 65 и ул.Солнечная', 61.319639,
        55.193479);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (376, 1770, 1419552000, 1417219200, 1574899200, 6, 'пересечение пр.Победы 159а и ул. Кирова', 61.400155,
        55.183739);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (377, 1771, 1419552000, 1417219200, 1574899200, 6, 'Комсомольский пр.  55', 61.32696, 55.194023);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (378, 1783, 1419552000, 1412121600, 1569801600, 6,
        'ул.Каслинская 64  Торговый центр  со стороны ул. Бр.Кашириных', 61.392457, 55.173134);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (379, 1784, 1419552000, 1412121600, 1569801600, 6,
        'ул.Каслинская 64  Торговый центр  парковка магазина "Каслинский"', 61.392457, 55.173134);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (380, 1786, 1419552000, 1414108800, 1571788800, 17,
        'пр. Победы/ Свердловский пр.  28А  трамв. ост. "пр. Победы"  поз.2', 61.385953, 55.183225);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (381, 1787, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова  10  трамв. ост. "ул. Калинина"  поз. 1',
        61.371319, 55.039729);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (382, 1789, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова  25А  трамв. ост. "Цирк"  поз.1', 61.400514,
        55.173005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (383, 1790, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова  25А  трамв. ост. "Цирк"  поз.2', 61.400514,
        55.173005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (384, 1791, 1419552000, 1414108800, 1571788800, 17,
        'пр. Победы  168  трамв. ост. "Теплотехничесий институт"  поз.1', 61.398493, 55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (385, 1793, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова  60А  трамв. ост. "Цирк"  поз. 1', 61.399149,
        55.174182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (386, 1794, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова  60А  трамв. ост. "Цирк"  поз. 2', 61.399149,
        55.174182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (387, 1795, 1419552000, 1414108800, 1571788800, 17,
        'ул. Российская  151  трамв. ост. "площадь Павших революционеров', 61.415956, 55.168331);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (388, 1796, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова 1  трамв.ост. "Теплотех"  поз. 1', 61.372056,
        55.040446);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (389, 1797, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова 1  трамв.ост. "Теплотех"  поз. 2', 61.372056,
        55.040446);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (390, 1798, 1419552000, 1414108800, 1571788800, 17, 'пр. Победы  188 трамв. ост. "пр. Победы"  поз.1', 61.388252,
        55.184346);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (391, 1800, 1419552000, 1414108800, 1571788800, 17,
        'пр. Победы  161 трамв. ост. "Теплотехнический институт"  поз.1', 61.397775, 55.183698);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (392, 1801, 1419552000, 1414108800, 1571788800, 17,
        'пр. Победы  161 трамв. ост. "Теплотехнический институт"  поз.2', 61.397775, 55.183698);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (393, 1802, 1419552000, 1414108800, 1571788800, 17, 'пр. Победы  289 трамв. ост. "ул. Молодогвардейцев"',
        61.329709, 55.188396);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (394, 1803, 1419552000, 1414108800, 1571788800, 17, 'пр. Победы  324 трамв. ост. "ул. Молодогвардейцев"  поз.1',
        61.337057, 55.189516);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (395, 1805, 1419552000, 1414108800, 1571788800, 17, 'ул. Кирова  9 трамв. ост. "ул. Калинина"  поз. 1',
        61.373125, 55.040647);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (396, 1807, 1419552000, 1414108800, 1571788800, 17,
        'пр. Победы/ ул. Чичерина  30  трамв. ост. "Конечная"  поз. 1', 61.291746, 55.180629);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (397, 1809, 1419552000, 1414108800, 1571788800, 17, 'ул. Горького 1Б  трамв. ост. "Комсомольская площадь"',
        61.441136, 55.161707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (398, 1810, 1419552000, 1414108800, 1571788800, 17, 'ул. Каслинская  19  трамв. ост."ул. Островского"  поз. 1',
        61.39349, 55.188201);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (399, 1814, 1419552000, 1414108800, 1571788800, 17,
        'Свердловский пр./ ул. Курчатова  22  трамв. ост. "ул. Курчатова"  поз.1', 61.39022, 55.150288);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (400, 1816, 1419552000, 1414108800, 1571788800, 17,
        'Свердловский пр./ ул. Курчатова  11А  трамв. ост. "ул. Курчатова"  поз.1', 61.391612, 55.148421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (401, 1818, 1419552000, 1417219200, 1574899200, 6, 'ул. Комарова 78', 61.461519, 55.181997);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (402, 1819, 1419552000, 1417219200, 1574899200, 6, 'автодорога Меридиан (ул. Приозерная  4)  поз.1', 61.409102,
        55.106788);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (403, 1820, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Энтузиастов  15Б и ул. Витебская', 61.377086,
        55.15301);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (404, 1821, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Калинина  17 и ул. Каслинская', 61.428362,
        55.044469);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (405, 1822, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул.Цвиллинга  66 и ул. Лазерная', 61.411474,
        55.147207);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (406, 1823, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Воровского и ул.Варненская  15', 61.373861,
        55.141392);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (407, 1824, 1419552000, 1417219200, 1574899200, 6, 'Троицкий тр.  50  у Металлобазы', 61.38819, 55.112726);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (408, 1825, 1419552000, 1417219200, 1574899200, 6,
        'пересечение Сверловского пр. и ул. Коммуны  со стороны Алого поля', 61.38863, 55.162505);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (409, 1826, 1419552000, 1417219200, 1574899200, 6, 'ул. Кирова  напротив д. 11', 61.400146, 55.177308);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (410, 1827, 1419552000, 1417219200, 1574899200, 6, 'ул. Воровского  49', 61.384641, 55.147773);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (411, 1828, 1419552000, 1417219200, 1574899200, 6, 'ул. Труда  50 м до ул. Красная  палисадник музея', 61.39292,
        55.167583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (412, 1829, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Коммуны и ул. Володарского', 61.386246,
        55.162435);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (413, 1830, 1419552000, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. (из центра) и ул. Труда (из центра)  в палисаднике ДС "Юность"', 61.389624,
        55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (414, 1831, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Цвиллинга  58 и ул. Евтеева', 61.41027,
        55.150972);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (415, 1832, 1419552000, 1417219200, 1574899200, 6, 'ул. Блюхера  65', 61.366881, 55.129525);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (416, 1833, 1419552000, 1417219200, 1574899200, 6, 'ул. Воровского  64', 61.37361, 55.14006);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (417, 1834, 1419552000, 1417219200, 1574899200, 6, 'Троицкий тр. 21', 61.387085, 55.09338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (418, 1836, 1419552000, 1417219200, 1574899200, 6, 'Уфимский тр.  1', 61.318741, 55.077344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (419, 1837, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Новороссийская (в центр) и ул. Ереванская',
        61.446917, 55.115797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (420, 1838, 1419552000, 1417219200, 1574899200, 6, 'Троицкий тр.  13', 61.385683, 55.108689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (421, 1839, 1419552000, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр.(в центр) и ул. Труда  с моста в центр первая', 61.389624, 55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (422, 1840, 1419552000, 1417219200, 1574899200, 6, 'ул. Труда (из центра)  135 м до ул. Северокрымская',
        61.358346, 55.170992);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (423, 1841, 1419552000, 1417219200, 1574899200, 6, 'ул. Новороссийская   м от ул. Л.Чайкиной  в центр70',
        61.453271, 55.114496);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (424, 1842, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Воровского  57 и  ул. Доватора', 61.382144,
        55.145895);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (425, 1843, 1419552000, 1417219200, 1574899200, 6,
        'пересечение ул. Блюхера и  ул. Дарвина  развязка у Мебельной фабрики', 61.365907, 55.126453);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (426, 1844, 1419552000, 1417219200, 1574899200, 6,
        'ул. Северо-Крымская  150 м до моста через реку Миасс из центра', 61.361698, 55.184711);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (427, 1845, 1419552000, 1417219200, 1574899200, 6, 'ул. Воровского  38', 61.384237, 55.148719);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (428, 1847, 1419552000, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. (из центра) и ул. Труда  напротив АЗС "ЛУКОЙЛ"', 61.389624, 55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (429, 1848, 1419552000, 1417219200, 1574899200, 6, 'Троицкий тр.  52В', 61.387704, 55.108483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (430, 1849, 1419552000, 1417219200, 1574899200, 6, 'ул. Блюхера  95  100 м до ул. Кузнецова', 61.363378,
        55.119425);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (431, 1850, 1419552000, 1417219200, 1574899200, 6, 'пересечение Свердловского пр.  16 и ул. Островского',
        61.386052, 55.187738);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (432, 1851, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Цвиллинга 62 и ул. Монакова', 61.410683,
        55.148709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (433, 1852, 1419552000, 1417219200, 1574899200, 6, 'ул. Блюхера (в центр)  после троллейбусного кольца',
        61.365902, 55.126452);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (434, 1853, 1419552000, 1417219200, 1574899200, 6, 'Троицкий тр.  напротив д. 21', 61.387085, 55.09338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (435, 1854, 1419552000, 1417219200, 1574899200, 6, 'ул. Цвиллинга 85  развязка у Привокзальной площади',
        61.414178, 55.144042);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (436, 1855, 1419552000, 1417219200, 1574899200, 6,
        'ул.Блюхера  напротив Областной больницы перед остановкой в центр', 61.365902, 55.126452);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (437, 1858, 1419552000, 1417564800, 1575244800, 12, 'пр.Ленина 28д', 61.426521, 55.161435);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (438, 1859, 1419552000, 1418083200, 1733616000, 1, 'пр. Ленина (в центр)  26д  перед ж/д мостом', 61.43483,
        55.161589);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (439, 1860, 1419552000, 1418083200, 1733616000, 1, 'пр.Ленина (в центр)  26/1  перед автомобильным мостом',
        61.430374, 55.161527);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (440, 1861, 1419552000, 1418083200, 1733616000, 1, 'пр.Ленина (из центра)  28д  перед ж/д мостом', 61.424706,
        55.161615);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (441, 1862, 1419552000, 1418083200, 1733616000, 1, 'пр.Ленина (из центра)  26/1  перед автомобильным мостом',
        61.430374, 55.161527);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (442, 1863, 1419552000, 1418688000, 1576368000, 6, 'а/д Меридиан  поворот на  реабилитационный центр', 61.420044,
        55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (443, 1864, 1419552000, 1418688000, 1576368000, 6,
        'Троицкий тр.  1 3 км от переулка Озерный(в сторону п.Новосинеглазово)', 61.385171, 55.12466);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (444, 1865, 1419552000, 1418688000, 1576368000, 6, 'ул.Черкасская поворот на ЧМЗ', 61.378353, 55.236956);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (445, 1866, 1419552000, 1418688000, 1576368000, 6, 'ул.Г.Танкограда пересечение с Копейским переулком',
        61.436591, 55.198812);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (446, 1867, 1419552000, 1418688000, 1576368000, 6, 'пр.Ленина  3 напротив проходной ЧТЗ', 61.446975, 55.151487);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (447, 1868, 1419552000, 1417219200, 1574899200, 5, 'ул.Свободы 44', 61.410791, 55.164207);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (448, 1869, 1419552000, 1417219200, 1574899200, 5, 'пересечение ул. Воровского 21 и ул.Блюхера', 61.390786,
        55.151528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (449, 1870, 1419552000, 1417219200, 1574899200, 5, 'пр. Ленина  17', 61.437004, 55.160525);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (450, 1871, 1419552000, 1417219200, 1574899200, 6, 'ул. Краснознаменная  напротив д. 29', 61.380212, 55.18451);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (451, 1872, 1419552000, 1417219200, 1574899200, 6, 'ул. Героев Танкограда  напротив д. 92', 61.447191,
        55.178701);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (452, 1873, 1419552000, 1417219200, 1574899200, 6, 'Копейское шоссе  200 м от виадука  из центра', 61.490535,
        55.125844);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (453, 1874, 1419552000, 1417219200, 1574899200, 6, 'пересечение ул. Блюхера (в центр) и ул. Корабельная',
        61.352751, 55.108667);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (454, 1875, 1419552000, 1417219200, 1574899200, 6, 'пересечение пр.Победы 133 и ул.Кудрявцева', 61.421122,
        55.185184);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (455, 1876, 1419552000, 1417219200, 1574899200, 6, 'пересечение автодороги Меридиан и Вагонного пер.  56',
        61.428138, 55.1215);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (456, 1877, 1419552000, 1417219200, 1574899200, 6, 'ул. Плеханова  28 ', 61.411249, 55.155679);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (457, 1914, 1419552000, 1417564800, 1575244800, 2, 'пр. Победы  166а', 61.401467, 55.18469);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (458, 1915, 1419552000, 1417564800, 1575244800, 6, 'пр. Комарова  42', 61.465409, 55.18706);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (459, 1921, 1419552000, 1417564800, 1575244800, 6, 'ул. Островского  19', 61.385153, 55.187774);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (460, 1922, 1419552000, 1417564800, 1575244800, 6,
        'дорога из Аэропорта (в город)  до поста ГИБДД  у виадука  справа', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (461, 1923, 1419552000, 1417564800, 1575244800, 6, 'пересечение ул. Рылеева  16 и ул. Клиническая', 61.38103,
        55.126303);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (462, 1925, 1419552000, 1417564800, 1575244800, 5, 'пр.Ленина 14', 61.446589, 55.160823);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (463, 1926, 1419552000, 1417564800, 1575244800, 5, 'ул.Воровского 4', 61.39923, 55.159486);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (464, 1927, 1419552000, 1417564800, 1733097600, 4, ' ул.Кирова  25А', 61.400514, 55.173005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (465, 1928, 1419552000, 1417564800, 1575244800, 6, 'ул. Хлебозаводская  напротив д. 20', 61.409803, 55.245353);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (466, 1929, 1419552000, 1418083200, 1575763200, 12, 'ул.Бр. Кашириных  135 к.3', 61.302634, 55.171468);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (467, 1930, 1419552000, 1418083200, 1575763200, 15, 'ул.Бр. Кашириных  135 к.1', 61.303281, 55.171822);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (468, 1931, 1419552000, 1418083200, 1575763200, 12, 'ул.Бр. Кашириных  135 к.1', 61.303281, 55.171822);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (469, 1932, 1419552000, 1418083200, 1575763200, 12, 'ул.Бр. Кашириных  135 к.3', 61.302634, 55.171468);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (470, 1933, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е стр.1  поз.1', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (471, 1934, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е стр.1  поз.2', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (472, 1935, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е стр.1  поз.3', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (473, 1936, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е стр.1  поз.4', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (474, 1937, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е стр.1  поз.5', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (475, 1938, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.1', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (476, 1939, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.2', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (477, 1940, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.3', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (478, 1941, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.4', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (479, 1942, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.5', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (480, 1943, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.6', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (481, 1944, 1419552000, 1418083200, 1575763200, 12, 'пр. Ленина  3е  стр.2  поз.7', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (482, 1971, 1419552000, 1418083200, 1575763200, 12, 'ул.Энтузиастов напротив д.40', 61.376386, 55.14609);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (483, 1972, 1419552000, 1418083200, 1575763200, 6,
        'пересечение ул. Героев Танкограда (из центра) и ул. Валдайская', 61.440319, 55.208098);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (484, 1996, 1419552000, 1418083200, 1575763200, 6,
        'пересечение ул.Бр. Кашириных (из центра) и ул. Салавата Юлаева', 61.296602, 55.169811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (485, 2000, 1419552000, 1418688000, 1576368000, 12, 'Свердловский тр. 1ж', 61.369451, 55.234974);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (486, 2001, 1419552000, 1418688000, 1576368000, 12, 'Свердловский тр. 1жк2', 61.368651, 55.234692);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (487, 2003, 1419552000, 1418688000, 1576368000, 12, 'пр. Ленина  3е  стр. 2  поз. 1', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (488, 2004, 1419552000, 1418688000, 1576368000, 12, 'пр. Ленина  3е  стр. 2  поз. 2', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (489, 2005, 1419552000, 1418688000, 1576368000, 12, 'пр.Ленина д.3е  стр.1', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (490, 2006, 1419552000, 1418083200, 1575763200, 6, 'пр. Победы  382б 130 м  до  ул. 40 лет Победы', 61.30028,
        55.187024);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (491, 2007, 1419552000, 1418083200, 1575763200, 6, 'Комарова пр.  87', 61.465319, 55.185466);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (492, 2008, 1419552000, 1418083200, 1575763200, 6, 'Калинина  ул  21', 61.391181, 55.179046);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (493, 2009, 1419552000, 1418083200, 1575763200, 6,
        'Троицкий тр.(в город)  1200 м до виадука к пос. Н-Синеглазово', 61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (494, 2010, 1419552000, 1418083200, 1575763200, 6, 'ул. Коммуны  80', 61.38713, 55.162654);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (495, 2011, 1419552000, 1418083200, 1575763200, 6, 'ул. Героев Танкограда  65', 61.451629, 55.173951);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (496, 2012, 1419552000, 1418083200, 1575763200, 6, 'ул. Бр.Кашириных 12 ', 61.401664, 55.173889);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (497, 2013, 1419552000, 1418083200, 1575763200, 6, 'пересечение пр. Победы (в центр) и ул. Российская  41',
        61.414304, 55.185841);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (498, 2014, 1419552000, 1418083200, 1575763200, 6, 'Свердловский пр.  30', 61.38713, 55.175509);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (499, 2015, 1419552000, 1418083200, 1575763200, 6, 'Комсомольский пр.  52  начало дома', 61.328353, 55.195031);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (500, 2016, 1419552000, 1418083200, 1575763200, 6, 'пр. Победы  163  у магазина "Перекресток"', 61.396975,
        55.183852);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (501, 2017, 1419552000, 1418083200, 1575763200, 6, 'ул. Худякова  1 950 м до ул. Лесопарковая  в центр ',
        61.381129, 55.146491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (502, 2018, 1419552000, 1418083200, 1575763200, 6, 'ул. ГероеТанкограда  65 - ул.Крылова', 61.451629, 55.173951);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (503, 2019, 1419552000, 1418083200, 1575763200, 6,
        'пересечение Свердловскго тр. и ул. Радонежская  у АЗС  напротив трамв. депо', 61.34525, 55.224855);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (504, 2020, 1419552000, 1418083200, 1575763200, 6, 'пересечение автодороги Меридиан и ул.Артиллерийская',
        61.433213, 55.159151);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (505, 2021, 1419552000, 1418083200, 1575763200, 6, 'Комсомольский пр.  29  у завода "Прибор"', 61.357126,
        55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (506, 2022, 1419552000, 1418083200, 1575763200, 6, 'пр. Победы  160-А', 61.403632, 55.184937);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (507, 2023, 1419552000, 1418083200, 1575763200, 6, 'ул. Худякова  1 610 м до ул.Лесопарковая  в центр справа',
        61.381129, 55.146491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (508, 2024, 1419552000, 1418083200, 1575763200, 6, 'пересечение ул. Бр. Кашириных  и ул. Чайковского', 61.353242,
        55.178593);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (509, 2025, 1419552000, 1418083200, 1575763200, 6, 'ул. Гер.Танкограда  220 м от ул. Ловина', 61.451704,
        55.161587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (510, 2026, 1419552000, 1418083200, 1575763200, 6, 'Комсомольский пр.  61  середина дома', 61.324122, 55.194023);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (511, 2027, 1419552000, 1418083200, 1575763200, 6,
        'пересечение Свердловского тр. и  ул. Радонежская  со стороны Авторынка', 61.34525, 55.224855);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (512, 2028, 1419552000, 1418083200, 1575763200, 6, 'пересечение пр. Победы и  ул. Ворошилова', 61.322165,
        55.189184);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (513, 2029, 1419552000, 1418083200, 1575763200, 6, 'ул. Худякова  1 060 м до ул. Лесопарковая  в центр справа',
        61.381129, 55.146491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (514, 2030, 1419552000, 1418083200, 1575763200, 6, 'ул. Бр.Кашириных  12  120 м до ул.Кирова', 61.401664,
        55.173889);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (515, 2031, 1419552000, 1418083200, 1575763200, 6,
        'автодорога Меридиан (в центр)  300 м до пересечения с ул. Новороссийская', 61.420044, 55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (516, 2032, 1419552000, 1418083200, 1575763200, 6,
        'пересечение ул. Героев Танкограда и ул. 1 - й Пятилетки  у Сада Победы', 61.452475, 55.166974);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (517, 2033, 1419552000, 1418083200, 1575763200, 6, 'Комсомольский пр.  52  магазин "Олимп"', 61.328353,
        55.195031);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (518, 2034, 1419552000, 1418083200, 1575763200, 6, 'ул. Худякова  780 м до ул. Лесопарковая  в центр справа ',
        61.365391, 55.147419);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (519, 2035, 1419552000, 1418083200, 1575763200, 6,
        'пересечение ул. Бр. Кашириных (в центр) и   ул. 40 лет Победы', 61.311783, 55.176563);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (520, 2036, 1419552000, 1418083200, 1575763200, 6,
        'Свердловский тр.(в центр)  ост. "Ветлечебница"  справа в центр', 61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (521, 2037, 1419552000, 1418083200, 1575763200, 6, 'автодорога Меридиан 80 м от ул. Новороссийская', 61.418903,
        55.119111);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (522, 2038, 1419552000, 1418083200, 1575763200, 6, 'пересечение пр. Победы пр.(в центр) и  ул. Горького',
        61.432144, 55.18596);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (523, 2039, 1419552000, 1418083200, 1575763200, 6, 'Комсомольский пр.  24  напротив завода "Прибор"', 61.358195,
        55.193694);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (524, 2040, 1419552000, 1418083200, 1575763200, 6, 'автодорога Меридиан  75 м до ул. Новороссийская', 61.418903,
        55.119111);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (525, 2041, 1419552000, 1418083200, 1575763200, 6, 'ул. Худякова 16А', 61.36654, 55.14805);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (526, 2042, 1419552000, 1418083200, 1575763200, 6,
        'пересечение ул.Героев Танкограда (из центра) и ул. Механическая', 61.438051, 55.193587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (527, 2043, 1419552000, 1418083200, 1575763200, 6, 'ул.Бр. Кашириных  116', 61.307144, 55.175951);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (528, 2044, 1419552000, 1418083200, 1575763200, 6,
        'выезд на автодорогу Меридиан с пр. Ленина (под мостом из центра слева)', 61.42882, 55.161157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (529, 2046, 1419811200, 1418688000, 1576368000, 6, 'Комсомольский пр.  83   конец дома', 61.312174, 55.194008);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (530, 2047, 1419811200, 1418688000, 1576368000, 6, 'ул.Бр.Кашириных 130 м до ул.Северо-Крымская  из центра',
        61.301224, 55.172907);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (531, 2048, 1419811200, 1418688000, 1576368000, 6, 'пересечение ул.Дзержинского и ул.Коммунаров 62', 61.429844,
        55.122612);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (532, 2049, 1419811200, 1418688000, 1576368000, 6, 'пр. Победы  170  у магазина "Ровесник"', 61.396939,
        55.184562);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (533, 2050, 1419811200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  118', 61.30664, 55.175288);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (534, 2051, 1419811200, 1418688000, 1576368000, 6, 'пр. Победы  158', 61.406542, 55.185091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (535, 2052, 1419811200, 1418688000, 1576368000, 6,
        'Свердловский тр.  развязка в районе Ветлечебницы  320 м от ул.Радонежской  справа в центр', 61.368543,
        55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (536, 2053, 1419811200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  120 м до ул. Чичерина  из центра справа',
        61.304811, 55.173497);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (537, 2054, 1419811200, 1418688000, 1576368000, 6, 'пересечение ул. Свободыи  ул. Коммуны', 61.411402,
        55.163093);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (538, 2055, 1419811200, 1418688000, 1576368000, 6, 'выезд на автодорогу Меридиан с пр.Ленина под мостом  справа',
        61.42882, 55.161157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (539, 2056, 1419811200, 1418688000, 1576368000, 6, 'пересечение ул. Плеханова  29 и ул. Свободы', 61.412848,
        55.155448);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (540, 2057, 1419811200, 1418688000, 1576368000, 6, 'Комсомольский пр.  50  напротив гост. "Виктория"', 61.330509,
        55.195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (541, 2058, 1419811200, 1418688000, 1576368000, 6, 'ул.Бр. Кашириных  106', 61.314384, 55.178594);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (542, 2059, 1419811200, 1418688000, 1576368000, 6, 'Победы пр.  170  у Теплотехнического института', 61.396939,
        55.184562);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (543, 2060, 1419811200, 1418688000, 1576368000, 6, 'Свердловский пр.  41 ', 61.388225, 55.176666);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (544, 2061, 1419811200, 1418688000, 1576368000, 6, 'Комсомольский пр.  83  начало дома', 61.312174, 55.194008);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (545, 2062, 1419811200, 1418688000, 1576368000, 6, 'пересечение ул.1 - й Пятилетки и ул. Артиллерийская',
        61.43501, 55.168208);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (546, 2063, 1419811200, 1418688000, 1576368000, 6, 'ул.Свободы   28', 61.409929, 55.166768);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (547, 2064, 1419811200, 1418688000, 1576368000, 6, 'пр.Победы  194', 61.383581, 55.184423);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (548, 2065, 1419811200, 1418688000, 1576368000, 6,
        'пересечение ул.Труда и Свердловского пр. на кольце напротив Дворца спорта "Юность"', 61.388193, 55.167675);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (549, 2066, 1419811200, 1418688000, 1576368000, 6, 'ул.Доватора 33', 61.383105, 55.145617);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (550, 2067, 1419811200, 1418688000, 1576368000, 6, 'пересечение пр.Победы 202 и ул.Краснознаменной', 61.380257,
        55.184433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (551, 2068, 1419811200, 1418688000, 1576368000, 6, 'пересечение ул.Дзержинского  126А и  ул. Барбюса', 61.428919,
        55.133354);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (552, 2069, 1419811200, 1418688000, 1576368000, 6, 'ул. Ст.Разина 6  у гипермаркета "М-Видео"', 61.41151,
        55.140651);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (553, 2070, 1419811200, 1418688000, 1576368000, 6, 'ул.Артиллерийская ул.  70м до пр.Ленина от ул.Ловина  слева',
        61.433914, 55.162412);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (554, 2071, 1419811200, 1418688000, 1576368000, 6, 'пересечение пр.Победы и ул. Косарева', 61.372157, 55.18359);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (555, 2072, 1419811200, 1418688000, 1576368000, 6, 'ул. Свободы  82', 61.412938, 55.156194);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (556, 2074, 1419811200, 1418688000, 1576368000, 6,
        'Свердловский тр. 14б  65 м от поворота на рынок "Северный двор"', 61.370115, 55.23021);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (557, 2075, 1419811200, 1418688000, 1576368000, 6, 'ул.Бр. Кашириных  114', 61.308985, 55.176331);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (558, 2076, 1419811200, 1418688000, 1576368000, 6,
        'ул.Свободы  из центра  100 м до ул. Труда  со стороны автостоянки', 61.411048, 55.168194);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (559, 2077, 1419811200, 1418688000, 1576368000, 6, 'пересечение ул.Артиллерийская и ул.Ловина', 61.433915,
        55.162416);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (560, 2078, 1419811200, 1418688000, 1576368000, 6, 'пересечение Комсомольского пр.  2 и Свердловского пр.',
        61.384614, 55.193217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (561, 2079, 1419811200, 1418688000, 1576368000, 6,
        'пересечение ул.Доватора 4 и ул.Степана Разина  у виадука в Ленинский район напротив завода им.Колющенко',
        61.408024, 55.138228);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (562, 2080, 1419811200, 1417564800, 1575244800, 6, 'пр. Победы  293  начало дома', 61.32414, 55.188411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (563, 2081, 1419811200, 1417564800, 1575244800, 6,
        'пересечение Свердловского тр. (в центр) и ул. Черкасская  выезд с ЧМЗ  слева', 61.378353, 55.236956);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (564, 2082, 1419811200, 1417564800, 1575244800, 6,
        'пересечение ул. Комарова  110 и ул. Шуменская  за ост. "ул. Кулибина"', 61.457234, 55.177452);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (565, 2083, 1419811200, 1417564800, 1575244800, 6, 'пересечение ул. Бр. Кашириных  129 и ул. Солнечная',
        61.318759, 55.176501);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (566, 2084, 1419811200, 1417564800, 1575244800, 6, 'ул. Комарова  110 (середина дома)', 61.457234, 55.177452);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (567, 2085, 1419811200, 1417564800, 1575244800, 6, 'пр. Победы  293 (середина дома)', 61.32414, 55.188411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (568, 2086, 1419811200, 1417564800, 1575244800, 6, 'ул. Кирова  4', 61.398628, 55.181724);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (569, 2087, 1419811200, 1417564800, 1575244800, 6, 'ул. Свободы  90', 61.414151, 55.15267);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (570, 2088, 1419811200, 1417564800, 1575244800, 6, 'ул. Черкасская/ шоссе Металлургов  41', 61.376431,
        55.245522);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (571, 2089, 1419811200, 1417564800, 1575244800, 6, 'ул. Комарова (в центр)  200 м от пересечения с ул. Бажова',
        61.463145, 55.183415);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (572, 2090, 1419811200, 1417564800, 1575244800, 6, 'ул. Свободы  161', 61.416612, 55.149465);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (573, 2091, 1419811200, 1417564800, 1575244800, 6, 'пр. Победы  154', 61.418732, 55.185847);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (574, 2092, 1419811200, 1417564800, 1575244800, 6, 'ул. Гагарина  1', 61.446248, 55.144783);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (575, 2093, 1419811200, 1417564800, 1575244800, 6, 'ул. Елькина  82', 61.397945, 55.153478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (576, 2094, 1419811200, 1417564800, 1575244800, 6,
        'пересечение ул. Героев Танкограда (в центр) и ул. Ловина  у театра ЧТЗ', 61.451704, 55.161587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (577, 2095, 1419811200, 1417564800, 1575244800, 6,
        'пересечение ул. Северо-Крымская и ул. Труда (из центра)  за ост. "Родничок"', 61.358346, 55.170828);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (578, 2096, 1419811200, 1417564800, 1575244800, 6,
        'пересечение ул. Героев Танкограда (в центр) и ул. Механическая', 61.438051, 55.193587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (579, 2097, 1419811200, 1417564800, 1575244800, 6, 'ул. Молодогвардейцев  17', 61.330751, 55.200092);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (580, 2098, 1419811200, 1417564800, 1575244800, 6, 'пересечение ул. Горького  30 и ул. Котина', 61.440166,
        55.174897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (581, 2099, 1419811200, 1417564800, 1575244800, 6, 'ул. Героев Танкограда  104', 61.448844, 55.175535);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (582, 2100, 1419811200, 1417564800, 1575244800, 6, 'Уфимский тр. (в город)  100 м до ул. Новоэлеваторная',
        61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (583, 2101, 1419811200, 1417564800, 1575244800, 6, 'пр. Победы  159', 61.400901, 55.18397);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (584, 2102, 1419811200, 1417564800, 1575244800, 6, 'ул. Елькина  63б', 61.399392, 55.154486);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (585, 2103, 1419811200, 1417564800, 1575244800, 6, 'ул. Краснознаменная  29', 61.380212, 55.18451);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (586, 2104, 1419811200, 1417564800, 1575244800, 6, 'Комсомольский пр.  10/6', 61.376547, 55.192256);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (587, 2105, 1419811200, 1417564800, 1575244800, 6, 'ул. Чичерина 32', 61.293247, 55.179565);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (588, 2106, 1419811200, 1417564800, 1575244800, 6, 'пересечение ул. Блюхера (в центр) и ул. Нефтебазовая',
        61.343243, 55.10236);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (589, 2107, 1419811200, 1417564800, 1575244800, 6, 'пересечение пл. Мопра  9 и ул. Российская', 61.417537,
        55.163055);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (590, 2108, 1419811200, 1417564800, 1575244800, 6, 'Бродокалмакский тр. (из центра)  ост. "Сад Дружба"',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (591, 2109, 1419811200, 1417564800, 1575244800, 6, 'ул. Энгельса  81', 61.382207, 55.154512);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (592, 2110, 1419811200, 1417564800, 1575244800, 6,
        'дорога в аэропорт  Бродокалмакский тр. (из центра)   за поворотом на ТЭЦ-3', 61.495394, 55.220341);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (593, 2111, 1419811200, 1417564800, 1575244800, 6, ' пр. Победы  141', 61.418831, 55.185065);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (594, 2114, 1419811200, 1417564800, 1575244800, 6, 'пр. Победы  289  150 м до ул. Молодогвардейцев', 61.329709,
        55.188396);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (595, 2115, 1419811200, 1417219200, 1574899200, 6, 'ул. Кирова  2 (конец дома)', 61.398394, 55.182768);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (596, 2116, 1419811200, 1417219200, 1574899200, 6, 'ул.Российская/ ул.Свободы  169', 61.417502, 55.146944);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (597, 2118, 1419811200, 1417564800, 1575244800, 5, 'пересечение ул.Красная  65 и пр.Ленина', 61.393993,
        55.160602);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (598, 2119, 1419811200, 1417564800, 1575244800, 5, 'пересечение пр.Ленина 26 и ул.Артиллерийская', 61.43483,
        55.161589);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (599, 2120, 1419811200, 1417564800, 1575244800, 5, 'пересечение пр. Ленина (из центра) и автодороги Меридиан',
        61.428739, 55.161044);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (600, 2121, 1419811200, 1417564800, 1575244800, 5, 'пр. Ленина 19А', 61.434255, 55.160597);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (601, 2122, 1419811200, 1417564800, 1575244800, 5, 'пересечение пр. Ленина 7 и ул. Героев Танкограда', 61.451772,
        55.159506);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (602, 2123, 1419811200, 1417564800, 1733097600, 5, 'пересечение пр.Ленина и ул.Советская 38', 61.40621,
        55.161224);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (603, 2124, 1419811200, 1417564800, 1733097600, 5, 'пересечение пр. Ленина и ул. Российская  200', 61.415947,
        55.161311);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (604, 2125, 1419811200, 1417564800, 1733097600, 5, 'ул. Воровского  53', 61.383608, 55.147037);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (605, 2126, 1419811200, 1417564800, 1733097600, 5, 'пересечение пр. Ленина  35 и ул. Российская', 61.415669,
        55.160211);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (606, 2129, 1419811200, 1417219200, 1574899200, 6, 'Свердловский тр.  12Б', 61.373799, 55.225183);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (607, 2130, 1419811200, 1417219200, 1574899200, 6,
        'пересечение пр. Комарова и ул. Салютная  за остановкой в центр', 61.452195, 55.171858);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (608, 2131, 1419811200, 1417219200, 1574899200, 6,
        'пересечение пр. Победы  146 и ул. Кудрявцева  до перекрестка в центр', 61.421005, 55.185836);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (609, 2132, 1419811200, 1417219200, 1574899200, 6,
        'пересечение ул. Воровского и ул. Тимирязева  за перекрестком в центр', 61.397661, 55.157809);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (610, 2133, 1419811200, 1417219200, 1574899200, 6, 'пересечение пр. Комарова  131 и ул. Шуменская', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (611, 2134, 1419811200, 1417219200, 1574899200, 6, 'ул. Молодогвардейцев  62', 61.331964, 55.183575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (612, 2135, 1419811200, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. и ул. Воровского  со стороны Горбольницы', 61.389446, 55.151841);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (613, 2136, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Гагарина  31 и ул. Дзержинского  96',
        61.396768, 55.077071);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (614, 2137, 1419811200, 1417219200, 1574899200, 6, 'пересечение пр. Победы 384А и ул. 40 лет Победы', 61.298017,
        55.185965);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (615, 2138, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Энгельса и ул. Труда  177/1', 61.382054,
        55.167421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (616, 2139, 1419811200, 1417219200, 1574899200, 6, 'ул. Цвиллинга  77', 61.413235, 55.147068);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (617, 2140, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Лесопарковая  9А и ул. Витебская', 61.365363,
        55.153118);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (618, 2141, 1419811200, 1417219200, 1574899200, 6, 'ул. Курчатова  напротив д. 30', 61.385072, 55.152475);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (619, 2142, 1419811200, 1417219200, 1574899200, 6, 'Свердловский пр.  напротив д. 54', 61.388109, 55.164665);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (620, 2143, 1419811200, 1417219200, 1574899200, 6, 'пересечениеи пр. Победы  142 и ул. Артиллерийская',
        61.423592, 55.185903);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (621, 2144, 1419811200, 1417219200, 1574899200, 6, 'ул. Черкасская  2г', 61.376673, 55.244839);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (622, 2145, 1419811200, 1417219200, 1574899200, 6, 'Свердловский тр.  16Б', 61.376368, 55.224223);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (623, 2146, 1419811200, 1417219200, 1574899200, 6, 'ул. Молодогвардейцев  38А', 61.332063, 55.192954);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (624, 2147, 1419811200, 1417219200, 1574899200, 6, 'ул. Худякова  17', 61.373304, 55.147171);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (625, 2148, 1419811200, 1417219200, 1574899200, 6,
        'пересечение Свердловского пр. и ул. Бр. Кашириных  за мостом из центра', 61.387627, 55.174261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (626, 2149, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Воровского  73 и ул. Тарасова', 61.376395,
        55.141691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (627, 2150, 1419811200, 1417219200, 1574899200, 6, 'Троицкий тр.  72Б', 61.394819, 55.068133);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (628, 2151, 1419811200, 1417219200, 1574899200, 6, 'пересечение пр. Победы и Свердловского пр.  25', 61.387318,
        55.184361);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (629, 2152, 1419811200, 1417219200, 1574899200, 6, 'ул. Свободы  161', 61.416612, 55.149465);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (630, 2153, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Дзержинского  91 и ул. Гагарина', 61.436663,
        55.130878);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (631, 2154, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Блюхера  44 и ул. Рылеева', 61.368741,
        55.132598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (632, 2155, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Героев Танкограда и ул. Первой Пятилетки',
        61.452475, 55.166974);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (633, 2156, 1419811200, 1417219200, 1574899200, 6, 'Троицкий тр.  напротив д. 42', 61.38748, 55.114827);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (634, 2157, 1419811200, 1417219200, 1574899200, 6, 'пересечение Свердловского тр. и ул. Автодорожная (в центр)',
        61.384884, 55.204813);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (635, 2158, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Новороссийская и автодороги Меридиан',
        61.419433, 55.119003);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (636, 2159, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Горького  12 и ул. Савина', 61.443202,
        55.165827);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (637, 2160, 1419811200, 1417219200, 1574899200, 6, 'пересечение ул. Молодогвардейцев  57/к1 и ул. Бр. Кашириных',
        61.335207, 55.180418);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (638, 2161, 1419811200, 1417219200, 1574899200, 6, 'пересечение Комсомольского пр.  10/1 и ул. Краснознаменная',
        61.378317, 55.192261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (639, 2162, 1419811200, 1417219200, 1574899200, 6, 'Свердловский тр.  автомобильный рынок', 61.368543,
        55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (640, 2163, 1419811200, 1417219200, 1574899200, 6, 'Троицкий тр.  52Б', 61.387489, 55.109281);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (641, 2164, 1419811200, 1417219200, 1574899200, 5, 'пр.Ленина  20', 61.439753, 55.161327);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (642, 2165, 1419811200, 1417219200, 1574899200, 5, 'пересечение ул.Кирова (в центр) и ул.Труда', 61.400515,
        55.167843);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (643, 2166, 1419811200, 1417219200, 1574899200, 5, 'пересечение ул. Рождественского и пр. Ленина  14', 61.446589,
        55.160823);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (644, 2167, 1419811200, 1417219200, 1574899200, 5, 'ул.Свободы 139 ', 61.412121, 55.159995);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (645, 2168, 1419811200, 1417219200, 1574899200, 5, 'ул.Российская  249', 61.416945, 55.160195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (646, 2169, 1419811200, 1417219200, 1574899200, 5, 'ул.Воровского  5', 61.397191, 55.157135);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (647, 2170, 1419811200, 1417564800, 1575244800, 6, 'ул.Блюхера  100 м до остановки "АМЗ" (в центр)', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (648, 2171, 1419811200, 1417564800, 1575244800, 6, 'пересечение пр. Победы  151 и ул. Болейко', 61.40426,
        55.18342);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (649, 2172, 1419897600, 1419033600, 1576713600, 6, 'Уфимский тр.  123/2 (1868 км + 130 м)', 61.33792, 55.057234);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (650, 2173, 1419897600, 1419033600, 1576713600, 6, 'Троицкий тр.  72а', 61.393112, 55.069458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (651, 2174, 1419897600, 1419033600, 1576713600, 6, 'ул. Чичерина  2', 61.280446, 55.190287);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (652, 2176, 1419897600, 1419033600, 1576713600, 6, 'ул. Бейвеля/ ул. Скульптора Головницкого  16', 61.283212,
        55.207783);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (653, 2177, 1419897600, 1419033600, 1576713600, 6, ' ул. Университетская Набережная / ул. Бр. Кашириных  85Б',
        61.371014, 55.175977);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (654, 2178, 1419897600, 1419033600, 1576713600, 6, 'ул. Татищева  напротив д.262', 61.281847, 55.172707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (655, 2179, 1419897600, 1419033600, 1576713600, 6, 'пересечение ул. Бейвеля  (в центр)и ул. Хариса Юсупова',
        61.28052, 55.199821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (656, 2181, 1419897600, 1418083200, 1575763200, 5, 'пр.Ленина 60', 61.3972, 55.160818);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (657, 2182, 1419897600, 1419033600, 1576713600, 6, 'ул. Центральная  1550 м до поста ГИБДД  в центр', 61.30815,
        55.150535);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (658, 2183, 1419897600, 1419033600, 1576713600, 6, 'ул. Артиллерийская  107', 61.435351, 55.167591);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (659, 2184, 1419897600, 1419033600, 1576713600, 6, 'Троицкий тр.  20/2', 61.387543, 55.121695);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (660, 2185, 1419897600, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Артиллерийская  136  поз. 2',
        61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (661, 2186, 1419897600, 1419033600, 1576713600, 6, 'Троицкий тр.  23б', 61.387156, 55.086218);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (662, 2187, 1419897600, 1418688000, 1576368000, 6, 'ул. Плеханова  43', 61.40992, 55.154887);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (663, 2188, 1419897600, 1418688000, 1576368000, 6, 'пересечение ул. Российская и ул. Нагорная', 61.415275,
        55.17347);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (664, 2189, 1419897600, 1418688000, 1576368000, 6, 'ул. Степана Разина  6', 61.41151, 55.140651);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (665, 2190, 1419897600, 1418688000, 1576368000, 6, 'пересечение ул. Калинина и Свердловского пр.', 61.387458,
        55.179208);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (666, 2191, 1419897600, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  66', 61.331946, 55.182017);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (667, 2192, 1419897600, 1418688000, 1576368000, 6, 'Свердловский тр. (в центр)  400 м от ул. Радонежской',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (668, 2193, 1419897600, 1418688000, 1576368000, 6, 'ул. 250 лет Челябинску  21', 61.294477, 55.175781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (669, 2194, 1419897600, 1418688000, 1576368000, 6, 'пр. Победы  290', 61.355671, 55.189989);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (670, 2195, 1419897600, 1418688000, 1576368000, 6, 'пересечение автодороги Меридиан (в центр) и ул. Артема  151',
        61.411734, 55.11184);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (671, 2196, 1419897600, 1418688000, 1576368000, 6, 'ул. Новороссийская  3', 61.493733, 55.097203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (672, 2197, 1419897600, 1418688000, 1576368000, 6, 'ул. 250 лет Челябинску  13', 61.304359, 55.180182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (673, 2198, 1419984000, 1418083200, 1733616000, 4,
        'пересечение ул.Героев Танкограда и пр. Победы  со стороны парка', 61.441799, 55.187755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (674, 2200, 1419984000, 1418083200, 1733616000, 4, 'пересечение пр. Победы (из центра) и ул. Молодогвардейцев',
        61.332594, 55.189074);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (675, 2201, 1419984000, 1417564800, 1575244800, 6, 'ул. Героев Танкограда  23  поз. 1', 61.444478, 55.186417);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (676, 2202, 1419984000, 1417564800, 1575244800, 6,
        'пересечение ул. Бажова (из центра) и ул. Комарова  до пересечения', 61.464613, 55.190546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (677, 2203, 1419984000, 1418083200, 1575763200, 5, 'пересечение пр.Ленина 50 и ул.Пушкина 56', 61.4078,
        55.160843);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (678, 2204, 1419984000, 1418083200, 1575763200, 5, 'пересечение пр.Ленина 29 и ул.3-го Интернационала',
        61.420583, 55.160545);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (679, 2205, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Бр.Кашириных и ул. Ворошилова 57А',
        61.323134, 55.179349);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (680, 2206, 1419984000, 1417564800, 1575244800, 6, 'ул.Салавата Юлаева  29', 61.294567, 55.171879);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (681, 2207, 1419984000, 1417564800, 1575244800, 6, 'Комсомольский пр.  66/1', 61.32458, 55.194979);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (682, 2208, 1419984000, 1417564800, 1575244800, 6, 'ул.Курчатова  напротив д. 23', 61.384542, 55.151204);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (683, 2209, 1419984000, 1417564800, 1575244800, 6, 'пр. Комарова  135', 61.457297, 55.174753);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (684, 2210, 1419984000, 1417564800, 1575244800, 6, 'шоссе Металлургов  3г', 61.401494, 55.250007);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (685, 2211, 1419984000, 1417564800, 1575244800, 6,
        'пересечение Троицкого тр. и ул. Рылеева  за перекрестком в центр', 61.377751, 55.129273);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (686, 2212, 1419984000, 1417564800, 1575244800, 6, 'ул. Дзержинского  110', 61.432432, 55.1332);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (687, 2213, 1419984000, 1417564800, 1575244800, 6,
        'пересечение ул. Героев Танкограда и ул. Потемкина  у Никольской рощи', 61.443822, 55.184619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (688, 2214, 1419984000, 1417564800, 1575244800, 6, 'ул.Салавата Юлаева  17в', 61.289456, 55.175889);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (689, 2215, 1419984000, 1417564800, 1575244800, 6,
        'пересечение ул. Комарова и ул. Завалишина  за перекрестком из центра', 61.466167, 55.187327);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (690, 2216, 1419984000, 1417564800, 1575244800, 6,
        'пересечение ул. 1-я Окружная и ул. Доватора  до перекрестка из центра', 61.400678, 55.144179);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (691, 2217, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Шоссе Металлургов  9 и ул. Дегтярева',
        61.397748, 55.249284);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (692, 2218, 1419984000, 1417564800, 1575244800, 6, 'ул. Курчатова/ ул. Воровского  30', 61.386132, 55.150674);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (693, 2219, 1419984000, 1417564800, 1575244800, 6, 'ул. Молодогвардейцев  60в', 61.332036, 55.184608);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (694, 2220, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Героев Танкограда  55 и ул. Котина',
        61.449077, 55.178193);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (695, 2221, 1419984000, 1417564800, 1575244800, 6, 'ул. Комарова  131(середина дома)', 61.4593, 55.177529);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (696, 2222, 1419984000, 1417564800, 1575244800, 6, 'ул. Блюхера  за остановкой "АМЗ" (в центр)', 61.342456,
        55.109477);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (697, 2223, 1419984000, 1417564800, 1575244800, 6, 'ул.Братьев Кашириных  102 (начало дома)', 61.316998,
        55.179082);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (698, 2224, 1419984000, 1417564800, 1575244800, 6, 'пересечние ул. Свободы  88 и ул. Орджоникидзе', 61.41363,
        55.153966);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (699, 2225, 1419984000, 1417564800, 1575244800, 6,
        'пересечение пр. Победы и ул. Героев Танкограда  за перекрестком из центра', 61.441738, 55.18785);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (700, 2226, 1419984000, 1417564800, 1575244800, 6, 'ул. Молодогвардейцев  56', 61.331991, 55.186546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (701, 2227, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Российская  251 и ул. Тимирязева', 61.417034,
        55.159115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (702, 2228, 1419984000, 1417564800, 1575244800, 6, 'ул. Блюхера  61', 61.367663, 55.130498);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (703, 2229, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Доватора  17 и ул. Шаумяна', 61.39225,
        55.141521);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (704, 2230, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Российская и пл. Мопра 9', 61.417537,
        55.163055);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (705, 2231, 1419984000, 1417564800, 1575244800, 6, 'ул. Кирова  17А', 61.401314, 55.175134);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (706, 2232, 1419984000, 1417564800, 1575244800, 6, 'пр. Победы  200/1', 61.380626, 55.184227);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (707, 2233, 1419984000, 1417564800, 1575244800, 6, 'ул. Свободы  102', 61.416046, 55.147634);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (708, 2234, 1419984000, 1417564800, 1575244800, 6, 'ул. Энгельса  32', 61.380877, 55.158369);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (709, 2235, 1419984000, 1417564800, 1575244800, 6, 'ул. Харлова 9', 61.435153, 55.148066);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (710, 2236, 1419984000, 1417564800, 1575244800, 6, 'Свердловский тр.  300 м до ост. "Ветлечебница" (в центр)',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (711, 2237, 1419984000, 1417564800, 1575244800, 6, 'ул. Комарова  112 (начало дома)', 61.455761, 55.175488);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (712, 2238, 1419984000, 1417564800, 1575244800, 6, 'пересечение ул. Бр. Кашириных  107 и ул. Молодогвардейцев',
        61.334362, 55.178002);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (713, 2239, 1419984000, 1417564800, 1575244800, 6, 'ул. Блюхера/ ул. Курчатова  20', 61.390705, 55.150659);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (714, 2240, 1419984000, 1417564800, 1733097600, 6, 'пересечение Свердловского тр. и Северного луча  у моста',
        61.416945, 55.219473);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (715, 2241, 1419984000, 1417564800, 1733097600, 6, 'ул. Воровского  71', 61.378191, 55.142468);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (716, 2242, 1419984000, 1417564800, 1733097600, 6,
        'Свердловский тр. (в центр) 150 м за пересечением ул. Индивидуальная', 61.360647, 55.241226);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (717, 2243, 1419984000, 1417564800, 1733097600, 6, 'пересечение Троицкого тр.  38 и ул. Шарова', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (718, 2245, 1419984000, 1417564800, 1733097600, 6, 'ул. Российская  40', 61.413989, 55.177632);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (719, 2246, 1419984000, 1417564800, 1733097600, 6, 'пересечение ул. Блюхера  13 и ул. Тарасова', 61.381048,
        55.140188);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (720, 2247, 1419984000, 1417564800, 1733097600, 6,
        'пересечение Свердловского тр. и ул. Автодорожная  за ост. "Цинковый завод"(в центр)', 61.36151, 55.208153);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (721, 2248, 1419984000, 1417564800, 1733097600, 6, 'ул. Российская  277', 61.418642, 55.15554);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (722, 2250, 1419984000, 1417564800, 1733097600, 6, 'Троицкий тр.  11а/4', 61.385432, 55.11339);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (723, 2251, 1419984000, 1417564800, 1733097600, 6, 'Свердловский тр.  350 м от поста ГИБДД  в центр ', 61.368543,
        55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (724, 2252, 1419984000, 1417564800, 1733097600, 6, 'пересечение ул. Героев Танкограда  40 и пр. Победы',
        61.441433, 55.186792);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (725, 2254, 1419984000, 1417564800, 1733097600, 6, 'ул. Бр. Кашириных  89 (конец дома)', 61.366567, 55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (726, 2256, 1419984000, 1417564800, 1733097600, 6, 'ул. Елькина  81А', 61.399131, 55.149192);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (727, 2257, 1419984000, 1417564800, 1733097600, 6, 'Свердловский тр.  400 м до поста ГИБДД  из центра ',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (728, 2258, 1419984000, 1417564800, 1733097600, 6, 'ул. Горького  63', 61.434875, 55.180819);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (729, 2260, 1419984000, 1417564800, 1733097600, 6, 'ул. Молодогвардейцев  47а', 61.334102, 55.18158);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (730, 2261, 1419984000, 1417564800, 1733097600, 6,
        'пересечение Свердловского тр.(из центра) и ул. Индивидуальная', 61.360647, 55.241226);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (731, 2262, 1419984000, 1417564800, 1733097600, 6, 'пересечение ул. Машиностроителей  34 и ул. Масленникова',
        61.46955, 55.11717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (732, 2263, 1419984000, 1417564800, 1733097600, 6,
        'Свердловский тр. (из центра)  100 м до пересечения с ул. Рабочекрестьянская', 61.361779, 55.237171);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (733, 2264, 1419984000, 1417564800, 1733097600, 6, 'пересечение ул. Машиностроителей  36 и ул. Масленникова',
        61.469083, 55.116377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (734, 2266, 1419984000, 1418688000, 1576368000, 6, 'пересечение ул.Российская  159 и ул. Труда', 61.416477,
        55.166799);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (735, 2267, 1419984000, 1418688000, 1576368000, 6, 'ул.Воровского  со стороны главного корпуса Мед. Академии  ',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (736, 2268, 1419984000, 1418688000, 1576368000, 6, 'ул. Чичерина  35', 61.302275, 55.176023);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (737, 2269, 1419984000, 1418688000, 1576368000, 6, 'Троицкий тр.  72б', 61.394819, 55.068133);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (738, 2270, 1419984000, 1418688000, 1576368000, 6, 'пересечение ул. 50 лет ВЛКСМ и шоссе Металлургов  19',
        61.392825, 55.248781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (739, 2271, 1419984000, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  103', 61.341845, 55.178121);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (740, 2272, 1419984000, 1418688000, 1576368000, 6, 'ул. Российская  226', 61.417618, 55.15427);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (741, 2273, 1419984000, 1418688000, 1576368000, 6, 'ул. Блюхера конечн. ост." АМЗ" (в центр)', 61.342456,
        55.109477);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (742, 2274, 1419984000, 1418688000, 1576368000, 6, 'ул. Горького  4', 61.442403, 55.161707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (743, 2275, 1419984000, 1418688000, 1576368000, 6,
        'Свердловский тр.  240 м от поворота на ЧВВАКУШ  справа в город', 61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (744, 2276, 1419984000, 1418688000, 1576368000, 6, 'ул. Энгельса  41', 61.381353, 55.166043);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (745, 2277, 1419984000, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  48', 61.332009, 55.190719);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (746, 2278, 1419984000, 1418688000, 1576368000, 6, 'пр. Ленина  2  у главной проходной ЧТЗ   поз.1', 61.4646,
        55.161563);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (747, 2279, 1419984000, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных (в центр)  300 м до ул.Молодогвардейцев  ',
        61.332633, 55.178684);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (748, 2280, 1419984000, 1418688000, 1576368000, 6,
        'шоссе Металлургов (из центра)  50 м до пересечения с ул. Сталеваров', 61.403311, 55.250885);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (749, 2281, 1419984000, 1418688000, 1576368000, 6, 'ул. Труда (из центра)  200 м до ул. Северо-Крымская',
        61.358346, 55.170992);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (750, 2282, 1419984000, 1418688000, 1576368000, 6, 'ул. Салютная  16', 61.449446, 55.171802);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (751, 2283, 1419984000, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  53', 61.333581, 55.18103);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (752, 2284, 1419984000, 1418688000, 1576368000, 6, 'пересечение ул. Евтеева и ул. Цвиллинга  59а', 61.41204,
        55.150345);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (753, 2285, 1419984000, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  68', 61.332, 55.180598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (754, 2286, 1419984000, 1418688000, 1576368000, 6, 'пр. Ленина  2  у главной проходной ЧТЗ   поз.2', 61.4646,
        55.161563);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (755, 2287, 1419984000, 1418688000, 1576368000, 6, 'Троицкий тр.  23    у "Сельхозтехники" ', 61.387471,
        55.088898);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (756, 2288, 1419984000, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  99А', 61.347882, 55.177853);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (757, 2289, 1419984000, 1418688000, 1576368000, 6, 'пересечение ул. Энтузиастов  1 и пр. Ленина  68', 61.375784,
        55.161029);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (758, 2290, 1419984000, 1418688000, 1576368000, 6, 'ул. Черкасская 2б', 61.378613, 55.242006);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (759, 2291, 1419984000, 1418688000, 1576368000, 6, 'пересечение ул. Механической и ул. Героев Танкограда ',
        61.438051, 55.193587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (760, 2292, 1419984000, 1418688000, 1576368000, 6,
        'развязка Свердловского пр. и ул. Труда  трамв. ост. "ДС Юность"', 61.389624, 55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (761, 2293, 1419984000, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  напротив д. 91А', 61.365786, 55.178141);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (762, 2294, 1419984000, 1418688000, 1576368000, 6,
        'пересечение ул. Молодогвардейцев (из центра) и Комсомольского пр.  до перекрестка', 61.332592, 55.194406);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (763, 2295, 1419984000, 1418688000, 1576368000, 6, 'пр. Комарова  125', 61.461573, 55.180547);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (764, 2296, 1419984000, 1418688000, 1576368000, 6, 'ул. Черкасская  4', 61.378578, 55.241421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (765, 2297, 1419984000, 1418688000, 1576368000, 6, 'Троицкий тр.  52', 61.387552, 55.112402);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (766, 2298, 1419984000, 1418688000, 1576368000, 6, 'пересечение ул. Блюхера (из центра) и ул. Новоэлеваторная',
        61.351679, 55.107994);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (767, 2299, 1419984000, 1418688000, 1576368000, 6, 'Свердловский тр.   250 м от поворота на ЧВВАКУШ', 61.368543,
        55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (768, 2300, 1419984000, 1418688000, 1576368000, 6, 'пересечение шоссе Металлургов и ул. Строительная', 61.40959,
        55.253976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (769, 2301, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. 23 Д и ул.Косарева  за перекр. в центр  авт.ост. "ул.Косарева"', 61.372119,
        55.191485);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (770, 2302, 1419984000, 1418688000, 1576368000, 8,
        'пересечение пр.Ленина 74 и ул.Энтузиастов  за перекр. из центра  тролл.ост. "Инст. Гражданпроект"', 61.374068,
        55.160221);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (771, 2305, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр.и ул.Ворошилова  за перекр.в центр  авт.ост."ул.Ворошилова"', 61.32138,
        55.194387);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (772, 2306, 1419984000, 1418688000, 1576368000, 8,
        'пересечение пр.Ленина 19 и ул.Артиллерийская  за перекр. из центра  авт.ост."к-т Спартак" ', 61.434255,
        55.160597);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (773, 2307, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Энгельса и ул.Труда  до перекр.из центра  тролл. ост."ул.Труда"  ', 61.380729, 55.16762);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (774, 2308, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Г.Танкограда и пр.Ленина  за перекр.из центра  авт.ост. "Театр ЧТЗ"  поз.2', 61.45144,
        55.160221);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (775, 2309, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. 22 и ул.Чайковского  за перекр.из центра  авт.ост."ул.Чайковского"', 61.359955,
        55.193201);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (776, 2310, 1419984000, 1418688000, 1576368000, 8, 'ул.Свободы 173  тролл.ост."Ж.Д. Институт"', 61.417007,
        55.145483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (777, 2311, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Труда и Свердловского пр.   в центр  трам.ост."ДС Юность"', 61.388193, 55.167675);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (778, 2312, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Цвиллинга и ул.Труда  за перекр.в центр  ост.трам "Оперный театр"', 61.40364, 55.167937);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (779, 2313, 1419984000, 1418688000, 1576368000, 8, 'Комсомольский пр.  69  в центр  ост. "Солнечная"', 61.315713,
        55.194013);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (780, 2314, 1419984000, 1418688000, 1576368000, 8, 'Комсомольский пр. 41  в центр  авт.ост. "Поликлиника"',
        61.338432, 55.194147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (781, 2316, 1419984000, 1418688000, 1576368000, 8,
        'пр.Победы 168  авт.ост. "Теплотехнический институт"  в сторону Свердловского пр.', 61.398493, 55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (782, 2317, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Худякова 6 и ул.Энгельса  за перекр.из центра  авт. ост. "ул.Худякова"', 61.37794, 55.147983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (783, 2318, 1419984000, 1418688000, 1576368000, 8,
        'пересечение пр.Ленина и Свердловского пр.  за перекр.из центра  ост."Алое поле"', 61.388823, 55.16005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (784, 2319, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. и ул.Чайковского  до перекр.в центр  авт.ост."ул.Чайковского"  поз.2',
        61.361349, 55.192472);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (785, 2320, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Г.Танкограда и пр.Ленина  за перекр.из центра  авт.ост. "Театр ЧТЗ"  поз.1', 61.45144,
        55.160221);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (786, 2321, 1419984000, 1418688000, 1576368000, 8,
        'пересечение пр.Ленина 76 и ул.Лесопарковая  200 м до перекр. из центра  авт. ост. "ЮрГУ"  поз.1', 61.37016,
        55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (787, 2322, 1419984000, 1418688000, 1576368000, 8, 'ул.Свободы 108', 61.415139, 55.145097);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (788, 2323, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Свердловского пр. 16 и ул.Островского  за перекр.в центр  ост.автобуса "ул.Островского"  в центр',
        61.386052, 55.187738);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (789, 2324, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. 14 и ул.Косарева  за перекр. из центра  авт.ост. "ул.Косарева"', 61.37069,
        55.192256);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (790, 2325, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Цвиллинга 11 и ул.Труда  до перекр.из центра  трам. ост. "Оперный театр"', 61.404305,
        55.166686);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (791, 2326, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Блюхера 44 и ул.Рылеева  за перекр.из центра  тролл.ост. "ул.Рылеева"', 61.368741, 55.132598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (792, 2327, 1419984000, 1418688000, 1576368000, 8,
        'пересечение пр.Ленина 8а и ул.Г.Танкограда  до перекр. в центр  трам.ост."Театр ЧТЗ"', 61.451943, 55.160648);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (793, 2328, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. и ул.Ворошилова  за перекр.из центра  авт.ост."ул.Ворошилова"', 61.32138,
        55.194387);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (794, 2329, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Кирова и ул.Калинина  за перекр.в центр  авт.ост "ул. Калинина" ', 61.3992, 55.1796);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (795, 2330, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Тимирязева 64 и ул.Пушкина  за перекр.в центр  тролл.ост. "Кинотеатр Пушкина"', 61.410162,
        55.158246);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (796, 2331, 1419984000, 1418688000, 1576368000, 8,
        'пересечение пр.Ленина 66А и ул.Энгельса  за перекр.из центра  авт.ост. "ул. Энгельса" ', 61.380051, 55.160411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (797, 2332, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. 35 и ул.Пионерская  за перекр.в центр  авт.ост. "ул.Пионерская"', 61.348277,
        55.194018);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (798, 2333, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. и ул.Чайковского  до перекр.в центр  авт.ост."ул.Чайковского"  поз.1',
        61.361349, 55.192472);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (799, 2334, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Свердловского пр. и ул.Калинина 34  за перекр.из центра  авт.ост "Автомобильное Училище" ',
        61.433303, 55.044908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (800, 2335, 1419984000, 1418688000, 1576368000, 8,
        'ул.Г.Танкограда-пр.Ленина  за перекр.из центра  тролл.ост. "Театр ЧТЗ"  поз.3', 61.45144, 55.160221);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (801, 2336, 1419984000, 1418688000, 1576368000, 8,
        'пересечение Комсомольского пр. 65 - ул.Солнечная  ост."ул.Солнечная"  за перекр. в центр', 61.319639,
        55.193479);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (802, 2337, 1419984000, 1418688000, 1576368000, 8,
        'пересечение ул.Цвиллинга и ул.Плеханова  до перекр.из центра  трам.ост."Городской парк" ', 61.406885, 55.155);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (803, 1, 1422230400, 1418688000, 1576368000, 6, 'ул. Труда  28', 61.423296, 55.168655);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (804, 2, 1422230400, 1418688000, 1576368000, 6, 'пр. Победы  напротив д. 305б', 61.307341, 55.187861);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (805, 3, 1422230400, 1418688000, 1576368000, 6, 'Троицкий тр.  32а', 61.387246, 55.11823);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (806, 4, 1422230400, 1418688000, 1576368000, 6, 'ул. Черкасская  17', 61.375532, 55.229085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (807, 5, 1422230400, 1418688000, 1576368000, 6, 'Комсомольский пр.  29', 61.357126, 55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (808, 6, 1422230400, 1418688000, 1576368000, 6, 'ул. Комарова  46', 61.465175, 55.186725);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (809, 7, 1422230400, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  напротив д. 1', 61.439124, 55.192888);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (810, 8, 1422230400, 1418688000, 1576368000, 6, 'ул. Свободы  92', 61.41442, 55.15196);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (811, 9, 1422230400, 1418688000, 1576368000, 6, 'Троицкий тр.  напротив д. 21', 61.387085, 55.09338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (812, 10, 1422230400, 1418688000, 1576368000, 6, 'ул. Энгельса  103', 61.381138, 55.14892);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (813, 11, 1422230400, 1418688000, 1576368000, 6, 'Троицкий тр.  46', 61.387525, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (814, 12, 1422230400, 1418688000, 1576368000, 6, 'пересечение пр. Победы  167 и ул. Каслинская', 61.394999,
        55.183734);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (815, 13, 1422230400, 1418688000, 1576368000, 6, 'ул. Комарова  103', 61.463037, 55.1825);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (816, 14, 1422230400, 1418688000, 1576368000, 6, 'ул. Богдана Хмельницкого  30', 61.376655, 55.258648);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (817, 15, 1422230400, 1418688000, 1576368000, 6, 'ул. Хлебозаводская  250 м от автодороги Меридиан', 61.418535,
        55.236586);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (818, 16, 1422230400, 1418688000, 1576368000, 6, 'ул. Дарвина  19', 61.383455, 55.118287);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (819, 17, 1422230400, 1418688000, 1576368000, 6, 'ул. Российская  61а', 61.415786, 55.172959);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (820, 18, 1422230400, 1418688000, 1576368000, 6, 'пересечение пр. Победы и ул. Бажова  23', 61.466693,
        55.192548);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (821, 19, 1422230400, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Гражданская  14а', 61.421957,
        55.137821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (822, 20, 1422230400, 1418688000, 1576368000, 6, 'Свердловский пр.  напротив д. 86', 61.388971, 55.153277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (823, 22, 1422230400, 1418688000, 1576368000, 6, 'ул. Горького  10', 61.443104, 55.16467);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (824, 23, 1422230400, 1418688000, 1576368000, 6, 'ул. Российская  между д. 196 и д. 198', 61.415804, 55.162798);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (825, 24, 1422230400, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  48', 61.332009, 55.190719);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (826, 25, 1422230400, 1418688000, 1576368000, 6, 'Комсомольский пр.  напротив д. 19', 61.377949, 55.191557);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (827, 26, 1422230400, 1418688000, 1576368000, 6, 'ул. Профинтерна/ ул. Салтыкова  52', 61.386429, 55.131084);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (828, 27, 1422230400, 1418688000, 1576368000, 6, 'ул. Краснознаменная  41', 61.380671, 55.176326);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (829, 28, 1422230400, 1418688000, 1576368000, 6, 'ул. Артиллерийская  117/1', 61.434722, 55.164366);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (830, 29, 1422230400, 1418688000, 1576368000, 6, 'ул. Ловина/ пр. Ленина  26а', 61.431345, 55.161908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (831, 30, 1422230400, 1418688000, 1576368000, 6, 'пересечение ул. Механическая и ул. Артиллерийская  2а',
        61.422649, 55.191901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (832, 31, 1422230400, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  83П', 61.437426, 55.195462);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (833, 32, 1422230400, 1418688000, 1576368000, 6, 'Копейское шоссе  1п/1', 61.541343, 55.11891);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (834, 33, 1422230400, 1418688000, 1576368000, 6, 'пр. Победы  84', 61.440786, 55.188026);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (835, 34, 1422230400, 1418688000, 1576368000, 6, 'ул. Чичерина/ ул. Бр. Кашириных  133', 61.305473, 55.171843);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (836, 35, 1422230400, 1418688000, 1576368000, 6, 'ул. Елькина  85  автостоянка', 61.399499, 55.147891);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (837, 39, 1422576000, 1418083200, 1575763200, 6, 'пересечение автодороги Меридиан и 7-го Целинного переулка',
        61.409138, 55.109219);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (838, 40, 1422576000, 1406851200, 1722384000, 1, 'Троицкий тр. 11', 61.386249, 55.109755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (839, 41, 1422576000, 1417564800, 1575244800, 12, 'Бродокалмакский тр.  6', 61.490696, 55.230374);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (840, 42, 1422921600, 1418083200, 1575763200, 6, 'пересечение ул. Новороссийская и ул. Дербенская', 61.491873,
        55.100172);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (841, 48, 1424131200, 1401580800, 1559260800, 12, 'Свердловский тр.  12б', 61.373799, 55.225183);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (842, 49, 1424131200, 1417564800, 1575244800, 6, 'ул. Кирова  2а/1', 61.398574, 55.183384);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (843, 50, 1424131200, 1417564800, 1575244800, 6, 'ул. Героев Танкограда  23  поз. 2', 61.444478, 55.186417);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (844, 51, 1424131200, 1417564800, 1575244800, 6, 'пр. Победы  155А', 61.401988, 55.184027);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (845, 52, 1424131200, 1417564800, 1575244800, 6,
        'пересечение ул. Чайковского и пр. Победы  за перекрестком в центр', 61.360553, 55.1882);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (846, 53, 1424131200, 1417564800, 1575244800, 6, 'ул. Энтузиастов 17', 61.377194, 55.151075);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (847, 54, 1424131200, 1418688000, 1576368000, 6,
        'Свердловский пр. (в центр)  40 м перед мостом через реку Миасс ', 61.387938, 55.172167);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (848, 55, 1424131200, 1418688000, 1576368000, 6, 'пересечение Троицкого тр.  9/2и ул. Дарвина', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (849, 56, 1424131200, 1418688000, 1576368000, 6, 'ул. Черкасская  5', 61.38041, 55.241668);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (850, 57, 1424131200, 1418688000, 1576368000, 6, 'ул. Линейная  51а', 61.518661, 55.158498);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (851, 58, 1424131200, 1418688000, 1576368000, 6, 'развязка Троицкого тр. и автодороги Меридиан', 61.420044,
        55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (852, 59, 1424131200, 1418688000, 1576368000, 6, 'Уфимский тр./ ул. Новоэлеваторная  51/1', 61.351116,
        55.105279);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (853, 60, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  11', 61.386249, 55.109755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (854, 61, 1424131200, 1418688000, 1576368000, 6, 'Комсомольский пр.  74', 61.314007, 55.195082);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (855, 62, 1424131200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда/ул. 40 лет Октября  23', 61.450461,
        55.162998);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (856, 63, 1424131200, 1418688000, 1576368000, 6, 'ул. Российская  179', 61.417277, 55.161718);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (857, 64, 1424131200, 1418688000, 1576368000, 6, 'ул. Комарова  68', 61.462615, 55.183456);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (858, 65, 1424131200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  42', 61.441648, 55.186438);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (859, 66, 1424131200, 1418688000, 1576368000, 6, 'ул. Танкистов  напротив д. 144', 61.469963, 55.1703);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (860, 67, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Горького  17 и ул. Первой Пятилетки',
        61.441828, 55.1672);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (861, 68, 1424131200, 1418688000, 1576368000, 6, 'ул. Краснознаменная  2', 61.378892, 55.191449);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (862, 69, 1424131200, 1418688000, 1576368000, 6, 'Комсомольский пр.  33  170 м до ул. Красного урала', 61.351305,
        55.194044);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (863, 70, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы  323', 61.29507, 55.183127);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (864, 71, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр.  напротив д.7', 61.378748, 55.223078);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (865, 72, 1424131200, 1418688000, 1576368000, 6, 'ул. Первой Пятилетки  напротив д. 43', 61.437687, 55.16757);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (866, 73, 1424131200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных/ Северокрымский 1-й пер.  16', 61.355814,
        55.179077);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (867, 74, 1424131200, 1418688000, 1576368000, 6, 'Комсомольский пр. 10/1', 61.378317, 55.192261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (868, 75, 1424131200, 1418688000, 1576368000, 6, 'Свердловский пр.  22', 61.386114, 55.186546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (869, 76, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Ш. Руставелли  напротив д. 32/1',
        61.439897, 55.142499);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (870, 77, 1424131200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  134б', 61.298367, 55.171288);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (871, 79, 1424131200, 1418688000, 1576368000, 6, 'пересечение Копейского шоссе и ул. Гагарина  1', 61.446248,
        55.144783);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (872, 78, 1424131200, 1418688000, 1576368000, 6, 'ул. Доватора  8', 61.405033, 55.138346);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (873, 80, 1424131200, 1418688000, 1576368000, 6, 'Уфимский тр./ ул. Блюхера  напротив д. 111', 61.353308,
        55.108066);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (874, 81, 1424131200, 1418688000, 1576368000, 6, 'Бродокалмакский тр.  3/1  выезд', 61.501386, 55.229476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (875, 82, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы/ ул. Чайковского  напротив д. 89', 61.439753,
        55.187784);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (876, 83, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Хлебозаводская  7/2 и ул. Автоматики',
        61.418777, 55.241447);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (877, 84, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Салавата Юлаева и ул. 250 лет Челябинску  25',
        61.292169, 55.174753);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (878, 85, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  52  поворот на Металлобазу', 61.387552,
        55.112402);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (879, 86, 1424131200, 1418688000, 1576368000, 6, 'пересечение пр. Победы  305е и ул. Молдавская', 61.308222,
        55.188216);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (880, 87, 1424131200, 1418688000, 1576368000, 6, 'ул. Энгельса  49', 61.381614, 55.163199);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (881, 88, 1424131200, 1418688000, 1576368000, 6, 'Свердловский пр.  напротив д. 51  развязка у ДС "Юность"',
        61.391244, 55.169015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (882, 89, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  ост. "Исаково"', 61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (883, 90, 1424131200, 1418688000, 1576368000, 6, 'ул. Комарова  52', 61.464753, 55.186227);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (884, 91, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан  1/3', 61.436231, 55.151024);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (885, 93, 1424131200, 1418688000, 1576368000, 6,
        'пересечение ул. Механическая  40а и ул. Артиллерийская (в центр)', 61.422227, 55.193026);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (886, 94, 1424131200, 1418688000, 1576368000, 6, 'ул. Краснознаменная  напротив д. 28', 61.379431, 55.183426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (887, 95, 1424131200, 1418688000, 1576368000, 6, 'пересечение автодороги Меридиан и ул. Ш. Руставелли  32/1',
        61.424913, 55.143096);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (888, 96, 1424131200, 1418688000, 1576368000, 6, 'ул. Северный луч/ ул. Героев Танкограда  42П', 61.439762,
        55.215755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (889, 97, 1424131200, 1418688000, 1576368000, 6,
        'автодорога Меридиан  100 м до пересечения с ул. Хлебозаводская', 61.420044, 55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (890, 99, 1424131200, 1418688000, 1576368000, 6, 'ул. Хлебозаводская  поз.2', 61.404835, 55.249438);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (891, 100, 1424131200, 1418688000, 1576368000, 6, 'Бродокалмакский тр.  1/2', 61.501386, 55.229476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (892, 101, 1424131200, 1418688000, 1576368000, 6, 'ул. Механическая/ул. Героев Танкограда  80П', 61.432773,
        55.193437);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (893, 102, 1424131200, 1418688000, 1576368000, 6, 'ул. Танкистов/ул. Кулибина 52', 61.472919, 55.173812);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (894, 103, 1424131200, 1418688000, 1576368000, 6, 'ул. Профинтерна/ ул. Табачная  2', 61.39075, 55.134955);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (895, 104, 1424131200, 1418688000, 1734220800, 6, 'Шершневское водохранилище  пост ГИБДД', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (896, 105, 1424131200, 1418688000, 1734220800, 4,
        'пересечение ул. Бр. Кашириных напротив д. 95А и ул. Северокрымская', 61.356964, 55.177853);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (897, 106, 1424131200, 1418688000, 1734220800, 6, 'ул. Худякова  напротив д. 22к1', 61.329, 55.146013);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (898, 107, 1424131200, 1418688000, 1734220800, 6, 'Копейское шоссе/ ул. Харлова  2а', 61.443607, 55.147376);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (899, 108, 1424131200, 1418688000, 1734220800, 4, 'пересечение ул. Труда  67 и  ул. Красноармейская', 61.414169,
        55.167642);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (900, 109, 1424131200, 1418688000, 1734220800, 6, 'Свердловский тр./ ул. Индивидуальная 1 ', 61.358339,
        55.240713);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (901, 110, 1424131200, 1418688000, 1734220800, 4,
        'пересечение ул. Бр. Кашириных (из центра) и ул. Северокрымская', 61.358283, 55.178598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (902, 111, 1424131200, 1418688000, 1734220800, 6, 'пересечение ул. Чичерина  22/5 и Комсомольского пр.',
        61.286572, 55.186504);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (903, 112, 1424131200, 1418688000, 1734220800, 4, 'Бродокалмакский тр.  50 м от поста ГИБДД', 61.495394,
        55.220341);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (904, 113, 1424131200, 1418688000, 1734220800, 4, 'ул. Героев Танкоград 28П  поворот на Северный луч', 61.440382,
        55.217249);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (905, 114, 1424131200, 1418688000, 1734220800, 4, 'Копейское шоссе/ ул. Харлова  1/1', 61.443661, 55.146826);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (906, 115, 1424131200, 1418688000, 1734220800, 6, 'Свердовский тр.  7/1', 61.386815, 55.198222);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (907, 116, 1424131200, 1418688000, 1576368000, 6, ' пересечение ул. Бр. Кашириных   97 и ул. Чайковского',
        61.350802, 55.178131);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (908, 117, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Салтыкова 49а и ул. Профинтерна', 61.385764,
        55.13181);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (909, 118, 1424131200, 1418688000, 1576368000, 6, 'Уфимский тр./ ул. Новоэлеваторная  51/1  поз. 1', 61.351116,
        55.105279);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (910, 119, 1424131200, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  43', 61.33394, 55.183235);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (911, 120, 1424131200, 1418688000, 1576368000, 6, 'ул. 40 лет Победы/ пр. Победы  напротив д. 315', 61.299876,
        55.184849);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (912, 121, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  напротив д. 33/1', 61.38598, 55.084338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (913, 122, 1424131200, 1418688000, 1576368000, 6, 'Комсомольский пр.  37', 61.34481, 55.194049);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (914, 123, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр. (в центр)/ ул. Индивидуальная  1а',
        61.358339, 55.240713);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (915, 124, 1424131200, 1418688000, 1576368000, 6, 'ул. Горького  30', 61.440166, 55.174897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (916, 125, 1424131200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  51П', 61.443607, 55.214296);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (917, 126, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  напротив д. 15  поз.2', 61.385593, 55.104213);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (918, 127, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Героев Танкограда и ул. Механическая  65',
        61.43889, 55.193463);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (919, 128, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр./ ул. Черкасская  15/1', 61.377527,
        55.232828);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (920, 129, 1424131200, 1418688000, 1576368000, 6, 'ул. Первой Пятилетки  9', 61.459821, 55.165266);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (921, 130, 1424131200, 1418688000, 1576368000, 6, 'Комсомолький пр.  41', 61.338432, 55.194147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (922, 132, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр.  напротив д. 7/2', 61.387327, 55.199326);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (923, 133, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы   329', 61.293283, 55.182773);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (924, 134, 1424131200, 1418688000, 1576368000, 6, 'ул. Молодогвардейцев  68', 61.332, 55.180598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (925, 135, 1424131200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  110', 61.312237, 55.177745);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (926, 136, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан  23', 61.420044, 55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (927, 137, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы  198', 61.381991, 55.184572);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (928, 138, 1424131200, 1418688000, 1576368000, 6, 'пересечение пр. Победы и ул. Российская  32', 61.41354,
        55.184793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (929, 139, 1424131200, 1418688000, 1576368000, 6, 'шоссе Металлургов  7', 61.398844, 55.249515);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (930, 140, 1424131200, 1418688000, 1576368000, 6, 'ул. Дзержинского  95', 61.433105, 55.131393);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (931, 141, 1424131200, 1418688000, 1576368000, 6, 'Курганский тр.  700 м до поста ГИБДД', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (932, 142, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Хлебозаводская  7в/1 и ул. Автоматики',
        61.420044, 55.235493);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (933, 143, 1424131200, 1418688000, 1576368000, 6,
        ' пересечение ул. Северо-Крымской (из центра) и ул. Бр. Кашириных  95', 61.361177, 55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (934, 144, 1424131200, 1418688000, 1576368000, 6, 'ул. 40 лет Победы  29', 61.301817, 55.184546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (935, 145, 1424131200, 1418688000, 1576368000, 6, 'ул. Ельина  90', 61.397972, 55.150695);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (936, 146, 1424131200, 1418688000, 1576368000, 6, 'ул. Красная/ул.Коммуны  70', 61.394666, 55.163528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (937, 147, 1424131200, 1418688000, 1576368000, 6, 'ул. Гостевая/ ул. Заводская  напротив д. 14', 61.380392,
        55.042416);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (938, 148, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Артиллерийская  124б', 61.433761,
        55.167236);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (939, 149, 1424131200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  107', 61.334362, 55.178002);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (940, 150, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  напротив д. 66 (100 м от поста ГИБДД)',
        61.388962, 55.07552);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (941, 151, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Энгельса и ул. Труда (из центра)', 61.380729,
        55.16762);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (942, 152, 1424131200, 1418688000, 1576368000, 6, 'ул. Комарова  106', 61.458932, 55.178671);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (943, 153, 1424131200, 1418688000, 1576368000, 6, 'Свердловский пр. (в центр)/ ул. 8 Марта  108', 61.386078,
        55.173581);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (944, 154, 1424131200, 1418688000, 1576368000, 6, 'ул. Энгельса  95', 61.380877, 55.152377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (945, 155, 1424131200, 1418688000, 1576368000, 6, 'ул. Ловина  200 м до ул. Артиллерийская', 61.433915,
        55.162416);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (946, 156, 1424131200, 1418688000, 1576368000, 6, 'ул. Первой Пятилетки/ ул. Марченко  25', 61.459857,
        55.166228);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (947, 157, 1424131200, 1418688000, 1576368000, 6, 'пересечение Копейского шоссе и ул. Машиностроителей',
        61.479424, 55.128145);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (948, 158, 1424131200, 1418688000, 1576368000, 6, 'ул. Дарвина  напротив д. 4а', 61.381695, 55.11995);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (949, 159, 1424131200, 1418688000, 1576368000, 6, 'ул. Салютная  напротив д. 2', 61.456965, 55.171766);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (950, 160, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Ворошилова  и пр. Победы 346', 61.323331,
        55.189491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (951, 161, 1424131200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  67П', 61.439807, 55.205425);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (952, 162, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Кирова  7 и ул. Калинина', 61.373125,
        55.040647);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (953, 163, 1424131200, 1418688000, 1576368000, 6, 'ул. Елькина 85  регистрационная палата', 61.3987, 55.157073);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (954, 164, 1424131200, 1418688000, 1576368000, 6, 'ул. Дарвина  18к1', 61.370529, 55.125716);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (955, 165, 1424131200, 1418688000, 1576368000, 6, 'ул. Труда  21', 61.416972, 55.167596);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (956, 166, 1424131200, 1418688000, 1576368000, 6, 'ул. Плеханова  43', 61.40992, 55.154887);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (957, 167, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  11а', 61.385899, 55.111789);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (958, 168, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Артиллерийская  136  по. 3',
        61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (959, 169, 1424131200, 1418688000, 1576368000, 6, 'пл. Павших Революционеров/ ул. Российская  124/1', 61.414914,
        55.169622);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (960, 170, 1424131200, 1418688000, 1576368000, 6, 'ул. Труда  183', 61.375119, 55.168578);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (961, 171, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Воровского и ул. Доватора  напротив д. 55',
        61.382997, 55.146497);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (962, 172, 1424131200, 1418688000, 1576368000, 6, 'ул. Российская  202', 61.415947, 55.161311);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (963, 173, 1424131200, 1418688000, 1576368000, 6, 'ул. Энгельса  97б', 61.381057, 55.150726);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (964, 175, 1424131200, 1418688000, 1576368000, 6, 'Западное шоссе (в центр)  1400 м до поста ГИБДД', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (965, 176, 1424131200, 1418688000, 1576368000, 6, 'ул. Свободы  185  ТК "Экспресс"', 61.415687, 55.143111);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (966, 177, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Труда и ул. 3-го Интернационала  105',
        61.421463, 55.16702);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (967, 178, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр./ ул. Морозова  5', 61.429808, 55.042633);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (968, 179, 1424131200, 1418688000, 1576368000, 6, 'ул. Кирова  19а', 61.401727, 55.174501);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (969, 180, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр.  1д', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (970, 181, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр. поворот на Новосинеглазово  выезд', 61.388387,
        55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (971, 182, 1424131200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  51п/1  справа', 61.447371, 55.214676);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (972, 183, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Артема  187/1', 61.408141,
        55.108684);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (973, 184, 1424131200, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных (в центр)/ ул. Ворошилова  поз. 2',
        61.321486, 55.178659);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (974, 185, 1424131200, 1418688000, 1576368000, 6, 'Свердловский пр.  40а/1', 61.387624, 55.168208);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (975, 186, 1422230400, 1418688000, 1576368000, 6, 'Бродокалмакский тр.   напротив д. 3/1', 61.501386, 55.229476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (976, 187, 1422230400, 1418688000, 1576368000, 6, 'пересечение ул. Героев Танкограда  80П и ул. Механическая',
        61.432773, 55.193437);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (977, 188, 1422230400, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных (центр)  400 м до ул. Краснознаменная',
        61.348969, 55.178583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (978, 189, 1422230400, 1418688000, 1576368000, 6, 'ул. Труда  28', 61.423296, 55.168655);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (979, 190, 1422230400, 1418688000, 1576368000, 6, 'Уфимский тр./ ул. Новоэлеваторная  49/7', 61.353227,
        55.105274);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (980, 191, 1422230400, 1418688000, 1576368000, 6,
        'площадь Аэропорта  с левой стороны по направлению к площади Аэропорта', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (981, 192, 1422230400, 1418688000, 1576368000, 6,
        'Свердловский тр. (из центра)  150 м до поворота на Лакокрасочный завод', 61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (982, 193, 1422230400, 1418688000, 1576368000, 6, 'ул. Комарова  90', 61.460064, 55.180316);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (983, 194, 1422230400, 1418688000, 1576368000, 6, 'Свердловский тр.  напротив д. 4б', 61.351853, 55.248381);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (984, 195, 1422230400, 1418688000, 1576368000, 6, 'дорога из Аэропорта  140 м от площади аэропорта (в город)',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (985, 196, 1422230400, 1418688000, 1576368000, 6, 'Свердловский пр.  напротив д.86', 61.388971, 55.153277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (986, 197, 1422230400, 1418688000, 1576368000, 6,
        'автодорога Меридиан (из центра)/ ул. Артиллерийская  136  поз.4', 61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (987, 198, 1422230400, 1418688000, 1576368000, 6, 'Свердловский тр.  12/1', 61.367699, 55.229522);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (988, 199, 1422230400, 1418688000, 1576368000, 6, 'ул. Свободы  напротив д. 66', 61.410953, 55.160967);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (989, 200, 1422230400, 1418688000, 1576368000, 6, 'ул. Гагарина  напротив д. 4', 61.443795, 55.144763);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (990, 201, 1422230400, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  118', 61.45179, 55.169411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (991, 202, 1422230400, 1418688000, 1576368000, 6, 'Северный луч  до поворота на ул. Хлебозаводская', 61.416945,
        55.219473);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (992, 203, 1422230400, 1418688000, 1576368000, 6, 'ул. Плеханова  16', 61.414932, 55.15607);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (993, 204, 1422230400, 1418688000, 1576368000, 6, 'ул. Гостевая/ ул. Парковая  8', 61.3032, 55.151873);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (994, 205, 1422230400, 1418688000, 1576368000, 6, 'пересечение ул. Дарвина  10/1 и ул. Новосельская', 61.378398,
        55.121597);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (995, 206, 1422230400, 1418688000, 1576368000, 6, 'ул. Молдавская  21а', 61.305419, 55.191454);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (996, 207, 1422230400, 1418688000, 1576368000, 6, 'Троицкий тр.  11  поз. 1', 61.386249, 55.109755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (997, 208, 1422230400, 1418688000, 1576368000, 6, 'ул. Гагарина  22', 61.438262, 55.136041);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (998, 209, 1422230400, 1418688000, 1576368000, 6, 'ул. Харлова  2', 61.443607, 55.147376);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (999, 210, 1422230400, 1418688000, 1576368000, 6,
        'пересечение автодороги Меридиан и ул. Гончаренко  напротив д. 99', 61.417717, 55.124676);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1000, 211, 1422230400, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  102в', 61.318795, 55.179036);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1001, 212, 1422230400, 1418688000, 1576368000, 6, 'Комсомольский пр.  110', 61.289761, 55.189414);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1002, 213, 1422230400, 1418688000, 1576368000, 6,
        'Свердловский тр. (в центр)  150 м после пересечения с ул. Радонежская', 61.375757, 55.224043);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1003, 214, 1422230400, 1418688000, 1576368000, 6, 'ул. Бр. Кашириных  52б', 61.366639, 55.179426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1004, 215, 1422230400, 1418688000, 1576368000, 6, 'Комсомолький пр.  111/1', 61.287695, 55.187373);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1005, 216, 1422230400, 1418688000, 1576368000, 6, 'пересечение ул. Бажова и ул. Комарова', 61.464613,
        55.190546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1006, 218, 1422230400, 1418688000, 1576368000, 6, 'ул. Горького  23', 61.441361, 55.170861);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1007, 219, 1422230400, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Харлова  4/1', 61.44163, 55.14928);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1008, 220, 1422230400, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  79П', 61.436717, 55.197086);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1009, 221, 1424131200, 1418688000, 1576368000, 6, 'ул. Горького  6', 61.442574, 55.162643);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1010, 222, 1424131200, 1418688000, 1576368000, 6, 'ул. Центральная (п. Западный2  стр. 1)', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1011, 223, 1424131200, 1418688000, 1576368000, 6, 'ул. Гагарина  6', 61.443014, 55.143559);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1012, 224, 1424131200, 1418688000, 1576368000, 6, 'Копейское шоссе  20', 61.504863, 55.122977);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1013, 225, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы  154', 61.418732, 55.185847);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1014, 226, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр.  6/1', 61.36363, 55.235195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1015, 227, 1424131200, 1418688000, 1576368000, 6, 'ул. Центральная  1700 м до поста ГИБДД  в центр', 61.30815,
        55.150535);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1016, 228, 1424131200, 1418688000, 1576368000, 6, ' ул. Российская  222', 61.417007, 55.155767);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1017, 229, 1424131200, 1418688000, 1576368000, 6, 'пересечение пр. Победы (из центра) и ул. Чайковского',
        61.360358, 55.188036);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1018, 230, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы  305б', 61.307341, 55.187861);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1019, 231, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  напротив д. 19', 61.383527, 55.093695);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1020, 232, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Молдавская и пр Победы  374', 61.307898,
        55.189182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1021, 233, 1424131200, 1418688000, 1576368000, 6, 'ул. Черкасская  10', 61.377958, 55.237838);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1022, 234, 1424131200, 1418688000, 1576368000, 6, 'ул. Комарова  116', 61.453713, 55.172897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1023, 235, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Доватора  1 и ул. Цехова', 61.399796,
        55.13812);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1024, 236, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы  113', 61.435872, 55.186217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1025, 237, 1424131200, 1418688000, 1576368000, 6, 'Комсомольский пр.  33  100 м до ул. Красного Урала',
        61.351305, 55.194044);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1026, 238, 1424131200, 1418688000, 1576368000, 6, 'ул. Воровского  41', 61.387255, 55.149666);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1027, 239, 1424131200, 1418688000, 1576368000, 6, 'Уфимский тр./ ул. Новоэлеваторная  49/7  поз.2', 61.353227,
        55.105274);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1028, 240, 1424131200, 1418688000, 1576368000, 6, 'Бродокалмакский тр.   напротив д. 3/1  поз. 2', 61.501386,
        55.229476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1029, 241, 1424131200, 1418688000, 1576368000, 6, 'дорога в Аэропорт  150 м до поворота на таможню', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1030, 242, 1424131200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  65П', 61.441055, 55.206796);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1031, 243, 1424131200, 1418688000, 1576368000, 6, 'ул. Артиллерийская  111', 61.435567, 55.1663);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1032, 244, 1424131200, 1418688000, 1576368000, 6, 'Свердловский тр.  напротив д. 8а', 61.366864, 55.232012);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1033, 245, 1424131200, 1418688000, 1576368000, 6, 'ул. Елькина  напротив д. 59', 61.398978, 55.158009);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1034, 246, 1424131200, 1418688000, 1576368000, 6, 'ул. Молдавская  23', 61.306623, 55.190477);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1035, 247, 1424131200, 1418688000, 1576368000, 6, 'ул. Гостевая/ ул. Центральная  3бк1', 61.304404, 55.151394);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1036, 248, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан/ ул. Сортировочная  1', 61.422182,
        55.140224);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1037, 249, 1424131200, 1418688000, 1576368000, 6, 'Троицкий тр.  напротив д. 70  выезд', 61.389312, 55.074211);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1038, 250, 1424131200, 1418688000, 1576368000, 6,
        'Свердловский тр (в центр).  200 м до поворота на ул. Радонежская', 61.375757, 55.224043);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1039, 251, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Лермонтовский пер. и ул. Котина  1',
        61.456183, 55.179);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1040, 252, 1424131200, 1418688000, 1576368000, 6, 'пр. Победы  192', 61.384929, 55.184402);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1041, 253, 1424131200, 1418688000, 1576368000, 6, 'автодорога Меридиан  напротив автодрома Обл. ГИБДД  поз. 1',
        61.420044, 55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1042, 254, 1424131200, 1418688000, 1576368000, 6, 'Комсомольский пр.  34', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1043, 255, 1424131200, 1418688000, 1576368000, 6, 'Копейское шоссе/ ул. Гагарина  2а', 61.444649, 55.146193);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1044, 256, 1424131200, 1418688000, 1576368000, 6, 'ул. Механическая 40', 61.418229, 55.194203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1045, 257, 1424131200, 1418688000, 1576368000, 6,
        'Троицкий тр.  въезд в город  700 м после  поворота на Исаково', 61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1046, 258, 1424131200, 1418688000, 1576368000, 6, 'ул. 250 лет Челябинску 14', 61.303389, 55.180665);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1047, 259, 1424131200, 1418688000, 1576368000, 6, 'ул. Новороссийская  126', 61.433123, 55.117092);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1048, 260, 1424131200, 1418688000, 1576368000, 6, 'Бродокалмакский тр. въезд в город с СНТ "Учитель"',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1049, 261, 1424131200, 1418688000, 1576368000, 6, 'ул. Салавата Юлаева  29  поз. 1', 61.294567, 55.171879);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1050, 262, 1424131200, 1418688000, 1576368000, 6, 'ул. Танкистов  138', 61.471508, 55.172373);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1051, 263, 1424131200, 1418688000, 1576368000, 6, 'Копейское шоссе  600 м от виадука  из города', 61.490535,
        55.125844);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1052, 264, 1424131200, 1418688000, 1576368000, 6, 'ул. 250 лет Челябинску  32', 61.291998, 55.175643);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1053, 265, 1424131200, 1418688000, 1576368000, 6, 'ул. Северный луч  18/1', 61.430006, 55.21917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1054, 266, 1424131200, 1418688000, 1576368000, 6, 'пересечение ул. Танкистов и ул. Кулибина', 61.472566,
        55.173645);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1055, 267, 1424131200, 1418688000, 1576368000, 6, 'ул.Хлебозаводская  7/1', 61.418777, 55.241447);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1056, 268, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  51-п/1  АЗС', 61.443607, 55.214296);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1057, 269, 1424131200, 1419033600, 1576713600, 6, ' ул. Комарова  133', 61.458384, 55.176182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1058, 270, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  10к/1', 61.368265, 55.230862);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1059, 271, 1424131200, 1419033600, 1576713600, 6, 'ул. Бр. Кашириных  73', 61.378551, 55.174763);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1060, 272, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Гражданская  23', 61.420421,
        55.132057);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1061, 273, 1424131200, 1419033600, 1576713600, 6, 'пересечение шоссе Металлургов  70 и ул. Румянцева',
        61.378748, 55.247231);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1062, 274, 1424131200, 1419033600, 1576713600, 6, 'Западное шоссе  1-й км', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1063, 275, 1424131200, 1419033600, 1576713600, 6, 'ул. Бр. Кашириных  97', 61.350802, 55.178131);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1064, 276, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Артема  105', 61.415516, 55.115136);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1065, 277, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр./ ул. Трактовая  напротив д. 9', 61.385018,
        55.122519);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1066, 278, 1424131200, 1419033600, 1576713600, 6, 'ул. Харлова/ ул. Барбюса  2/1', 61.436142, 55.148323);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1067, 279, 1424131200, 1419033600, 1576713600, 6, 'ул. Гостевая/  ул. Степная  2а', 61.301969, 55.154002);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1068, 280, 1424131200, 1419033600, 1576713600, 6, 'пр. Победы  167', 61.394999, 55.183734);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1069, 281, 1424131200, 1419033600, 1576713600, 6, 'ул. Гагарина  33а', 61.435773, 55.129648);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1070, 282, 1424131200, 1419033600, 1576713600, 6, 'ул. Дарвина  2а', 61.385207, 55.118704);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1071, 283, 1424131200, 1419033600, 1576713600, 6, 'пересечение Свердловского тр. (в центр) и ул. Радонежская',
        61.34525, 55.224855);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1072, 284, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Шота Руставели  32/1', 61.424913,
        55.143096);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1073, 285, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  напротив д. 21а', 61.387264, 55.091974);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1074, 286, 1424131200, 1419033600, 1576713600, 6, 'ул. Блюхера  напротив д. 63', 61.367205, 55.130003);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1075, 287, 1424131200, 1419033600, 1576713600, 6, 'ул. Гостевая/  ул. Садовая  1а', 61.304341, 55.152742);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1076, 288, 1424131200, 1419033600, 1576713600, 6, 'ул. Бр. Кашириных  65/1', 61.382764, 55.173787);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1077, 289, 1424131200, 1419033600, 1576713600, 6, 'ул. Чайковского  52', 61.35205, 55.181385);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1078, 290, 1424131200, 1419033600, 1576713600, 6, 'ул. Российская  275', 61.418202, 55.156477);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1079, 291, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  9к2', 61.385027, 55.121299);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1080, 292, 1424131200, 1419033600, 1576713600, 6, 'ул. Труда/ ул. Свободы  65', 61.411546, 55.16866);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1081, 293, 1424131200, 1419033600, 1576713600, 6, 'ул. Профинтерна/ ул. Рылеева  20/1', 61.385099, 55.125911);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1082, 294, 1424131200, 1419033600, 1576713600, 6, 'ул. Блюхера 101', 61.358976, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1083, 295, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  65', 61.451629, 55.173951);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1084, 296, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  23', 61.387471, 55.088898);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1085, 297, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ Троицкий тр.  1/1', 61.384965,
        55.124691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1086, 298, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  75-П', 61.437229, 55.197739);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1087, 299, 1424131200, 1419033600, 1576713600, 6, 'ул. Дарвина  12', 61.37485, 55.123703);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1088, 300, 1424131200, 1419033600, 1576713600, 6, 'Бродокалмакский тр.(в центр)  550 м от поста ГИБДД',
        61.495394, 55.220341);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1089, 301, 1424131200, 1419033600, 1576713600, 6, 'ул. Мамина  15', 61.4813, 55.194362);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1090, 302, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1868 км+30 м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1091, 303, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  17', 61.386483, 55.098501);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1092, 304, 1424131200, 1419033600, 1576713600, 6, 'Копейское шоссе  напротив д. 1/1', 61.512768, 55.120846);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1093, 305, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Игуменка  9а', 61.429305,
        55.150571);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1094, 306, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Хлебозаводская и ул. Винницкая  3',
        61.410558, 55.244337);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1095, 307, 1424131200, 1419033600, 1576713600, 6, 'ул. Танкистов  175', 61.473098, 55.172661);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1096, 308, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Тухачевского  18', 61.427733,
        55.148128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1097, 309, 1424131200, 1419033600, 1576713600, 6, 'ул. Краснознаменная  32', 61.379144, 55.18248);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1098, 310, 1424131200, 1419033600, 1576713600, 6, 'ул. Хлебозаводская  напротив д. 12', 61.406901, 55.247288);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1099, 311, 1424131200, 1419033600, 1576713600, 6, 'ул. Танкистов  напротив д. 148', 61.468508, 55.16812);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1100, 312, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Гражданская  напротив д.25',
        61.41981, 55.131084);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1101, 313, 1424131200, 1419033600, 1576713600, 6, 'пер. Лермонтова/ ул. Комарова  д. 110 к1', 61.458411,
        55.178177);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1102, 314, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр. (в центр)  200 м после ост. "Подстанция"',
        61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1103, 315, 1424131200, 1419033600, 1576713600, 6, 'Комсомольский пр.  5', 61.388333, 55.191706);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1104, 316, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  59П к1', 61.442529, 55.209555);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1105, 317, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр./ ул. Морозова 6', 61.430464, 55.042648);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1106, 318, 1424131200, 1419033600, 1576713600, 5, 'пересечение пр. Ленина  18 и ул. Горького', 61.442933,
        55.16108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1107, 319, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1869 км + 950 м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1108, 320, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  61П', 61.441783, 55.20919);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1109, 321, 1424131200, 1419033600, 1576713600, 6, 'Бродокалмакский тр.  3/1', 61.501386, 55.229476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1110, 322, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  32', 61.387246, 55.11823);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1111, 323, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  (в город)  400 м после поста ГИБДД',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1112, 324, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Хлебозаводская и ул. Пекинская  3',
        61.407818, 55.246374);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1113, 325, 1424131200, 1419033600, 1576713600, 6, 'ул. Доватора  1м', 61.399796, 55.13812);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1114, 327, 1424131200, 1419033600, 1576713600, 6, 'ул. Бр. Кашириных  95', 61.361177, 55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1115, 328, 1424131200, 1419033600, 1576713600, 6, 'ул. Труда  100', 61.397478, 55.168326);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1116, 329, 1424131200, 1419033600, 1576713600, 6, 'шоссе Металлургов  43', 61.376008, 55.245917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1117, 330, 1424131200, 1419033600, 1576713600, 6, 'ул. Линейная  51', 61.517879, 55.158251);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1118, 331, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  после поста ГИБДД  въезд в город', 61.388387,
        55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1119, 332, 1424131200, 1419033600, 1576713600, 6, 'ул. Бр. Кашириных/ Северо-Крымский 2-й пер.  10/1',
        61.354718, 55.179375);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1120, 333, 1424131200, 1419033600, 1576713600, 6, 'ул. Хлебозаводская  напротив д. 2', 61.404835, 55.249438);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1121, 334, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  напротив д. 9', 61.385018, 55.122519);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1122, 335, 1424131200, 1419033600, 1576713600, 6, 'ул. Салютная  10', 61.452949, 55.172697);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1123, 336, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  100 м за пос. Исаково', 61.432216, 55.043598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1124, 337, 1424131200, 1419033600, 1576713600, 6, 'Комсомольский пр.  105', 61.29242, 55.18909);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1125, 338, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр./ ул. Новгородская  1г', 61.360935,
        55.237438);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1126, 339, 1424131200, 1419033600, 1576713600, 6, 'Доватора  31', 61.383563, 55.145421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1127, 340, 1424131200, 1419033600, 1576713600, 6, 'пр. Победы  напротив д. 382а', 61.299086, 55.186397);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1128, 341, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр  54б', 61.387938, 55.10736);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1129, 342, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  (в город)  50 м после поста ГИБДД',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1130, 343, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  поворот на Новосинеглазово  въезд', 61.375487,
        55.040709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1131, 344, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Молодогвардейцев и ул. Куйбышева  75',
        61.333689, 55.198257);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1132, 345, 1424131200, 1419033600, 1576713600, 6, 'пересечение автодороги Меридиан и ул. Новороссийская',
        61.418903, 55.119111);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1133, 346, 1424131200, 1419033600, 1576713600, 6, 'ул. Красноармейская/ ул. Труда  56', 61.412282, 55.168465);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1134, 347, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан 170 м до виадука/ ул. Дзержинского  125',
        61.421077, 55.132767);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1135, 348, 1424131200, 1419033600, 1576713600, 6, 'Западное шоссе  поворот на п. Западный 2', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1136, 349, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Бр. Кашириных  133/2 и ул. Чичерина',
        61.304305, 55.172805);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1137, 350, 1424131200, 1419033600, 1576713600, 6, 'ул. Курчатова  28', 61.385674, 55.152058);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1138, 351, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  напротив д. 8', 61.36645, 55.232654);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1139, 352, 1424131200, 1419033600, 1576713600, 6, 'ул. Свободы  179', 61.416235, 55.144181);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1140, 353, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  56', 61.387839, 55.104883);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1141, 354, 1424131200, 1419033600, 1576713600, 5, 'пересечение пр. Ленина  10 и ул. Героев Танкограда',
        61.45029, 55.160905);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1142, 355, 1424131200, 1419033600, 1576713600, 5, 'ул. Артиллерийская  119', 61.434291, 55.162016);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1143, 356, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  76  поз. 2', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1144, 357, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  10', 61.45029, 55.160905);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1145, 358, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина/ ул. 40 лет Октября  26', 61.448853, 55.160823);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1146, 359, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  24', 61.436519, 55.161481);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1147, 360, 1424131200, 1419033600, 1576713600, 5, ' пр. Ленина  21в', 61.426224, 55.160391);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1148, 361, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр. (из центра)  250 м до поворота на ЧВВАКУШ',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1149, 362, 1424131200, 1419033600, 1576713600, 6, 'ул. Танкистов  144', 61.469963, 55.1703);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1150, 363, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Худякова 12к1 и ул. Татьяничевой', 61.368948,
        55.148472);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1151, 364, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  напротив д.35к1', 61.384974, 55.09727);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1152, 365, 1424131200, 1419033600, 1576713600, 6, 'ул. Хлебозаводская  напротив д. 4', 61.405437, 55.248499);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1153, 366, 1424131200, 1419033600, 1576713600, 6, 'развяка на пос. Новосинеглазово  поз. 1', 61.375487,
        55.040709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1154, 367, 1424131200, 1419033600, 1576713600, 6, 'пересечение автодороги Меридиан и ул. Новороссийская',
        61.418903, 55.119111);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1155, 368, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  п. Исаково/ ул. Трактовая  напротив д. 41',
        61.416298, 55.046362);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1156, 369, 1424131200, 1419033600, 1576713600, 6, 'пр. Победы  137', 61.419765, 55.185065);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1157, 370, 1424131200, 1419033600, 1576713600, 6, 'ул. Дзержинского  102', 61.435073, 55.132129);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1158, 371, 1424131200, 1419033600, 1576713600, 6, 'ул. Блюхера/ ул. Мебельная  88', 61.366325, 55.125381);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1159, 372, 1424131200, 1419033600, 1576713600, 6, 'ул. Коммуны/ ул. Энтузиастов  2', 61.374724, 55.161347);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1160, 373, 1424131200, 1419033600, 1576713600, 6, 'ул. Бажова  35/ Лермонтовский пер.  4а', 61.465984,
        55.189383);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1161, 374, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр./ ул. Цинковая  2а/1', 61.384605, 55.195323);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1162, 375, 1424131200, 1419033600, 1576713600, 6, 'ул. Первой Пятилетки  напротив д.57', 61.431596, 55.167909);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1163, 376, 1424131200, 1419033600, 1576713600, 6, 'ул. Комарова  129', 61.460324, 55.178403);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1164, 377, 1424131200, 1419033600, 1576713600, 6, 'ул. 50 лет ВЛКСМ  1', 61.391792, 55.247672);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1165, 378, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  71П ст10', 61.4434, 55.199866);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1166, 379, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  1б/1', 61.365741, 55.236458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1167, 380, 1424131200, 1419033600, 1576713600, 6, 'ул. Дзержинского  104', 61.433878, 55.13269);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1168, 381, 1424131200, 1419033600, 1576713600, 6, 'ул. Профинтерна/ ул. Марата  8', 61.387094, 55.126946);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1169, 382, 1424131200, 1419033600, 1576713600, 6, 'ул. Чичерина/ Комсомольский пр.  напротив д. 112/1',
        61.285755, 55.187774);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1170, 383, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Приозерная  4', 61.409102,
        55.106788);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1171, 384, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  800 м до поворота на п. Исаково', 61.432216,
        55.043598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1172, 385, 1424131200, 1419033600, 1576713600, 6, 'Комсомольский пр.  85', 61.308527, 55.194018);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1173, 386, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Артиллерийская  136', 61.430922,
        55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1174, 387, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  въезд  900 м до поста ГИБДД', 61.326251,
        55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1175, 388, 1424131200, 1419033600, 1576713600, 6, 'Комсомольский пр.  12', 61.372226, 55.192333);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1176, 389, 1424131200, 1419033600, 1576713600, 6,
        'Троицкий тр.  выезд из города  после поворота на Новосинеглазово', 61.375487, 55.040709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1177, 390, 1424131200, 1419033600, 1576713600, 6, 'ул. Дарвина  напротив д. 63', 61.37714, 55.121124);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1178, 391, 1424131200, 1419033600, 1576713600, 6, 'Копейское шоссе (из города)  300 м от виадука', 61.490535,
        55.125844);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1179, 392, 1424131200, 1419033600, 1576713600, 6,
        'пересечение ул. Механическая и ул.Артиллерийская (из центра)', 61.424085, 55.192041);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1180, 393, 1424131200, 1419033600, 1576713600, 6, 'ул. Горького  12', 61.443202, 55.165827);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1181, 394, 1424131200, 1419033600, 1576713600, 6, 'Свердловский пр.  20', 61.386114, 55.186546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1182, 395, 1424131200, 1419033600, 1576713600, 6, 'ул. Артиллерийская  117', 61.435621, 55.163826);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1183, 396, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  52П/3', 61.440876, 55.212925);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1184, 398, 1424131200, 1419033600, 1576713600, 6, 'Копейскре шоссе  5', 61.50701, 55.121335);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1185, 399, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  / ул. Крнтейнерная  напротив д.2', 61.38668,
        55.101045);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1186, 400, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда/ ул. Валдайская  напротив д. 1а',
        61.442654, 55.208409);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1187, 401, 1424131200, 1419033600, 1576713600, 6, 'ул. Краснознаменная  36', 61.379485, 55.181575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1188, 402, 1424131200, 1419033600, 1576713600, 6, 'ул. Чайковского  напротив д. 15', 61.363935, 55.191135);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1189, 403, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  11а', 61.385899, 55.111789);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1190, 404, 1424131200, 1419033600, 1576713600, 6, 'пр. Победы  140', 61.424733, 55.185944);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1191, 405, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан/ ул. Игуменка  155', 61.398915,
        55.090887);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1192, 406, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр. / ул. Трактовая  напротив д. 8', 61.422218,
        55.043969);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1193, 407, 1424131200, 1419033600, 1576713600, 6, 'пр. Ленина  8', 61.453758, 55.1609);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1194, 408, 1424131200, 1419033600, 1576713600, 6, 'ул. Кулибина/ ул. Мамина  27а', 61.483743, 55.173745);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1195, 410, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан  1/1', 61.427644, 55.163451);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1196, 411, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1/2', 61.318741, 55.077344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1197, 412, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  3а', 61.385198, 55.124202);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1198, 413, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  38  завод транспортных трансмиссий',
        61.382081, 55.199434);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1199, 414, 1424131200, 1419033600, 1576713600, 6, 'пересечение пр. Победы  293а и ул. Ворошилова', 61.321687,
        55.187954);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1200, 416, 1424131200, 1419033600, 1576713600, 6, 'ул. Танкистов  177', 61.472155, 55.171231);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1201, 417, 1424131200, 1419033600, 1576713600, 6, 'пересечение Комсомольского пр. и ул. Косарева  у автобазы',
        61.371654, 55.19185);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1202, 418, 1424131200, 1419033600, 1576713600, 6, 'ул. Первой Пятилетки  17', 61.453614, 55.166089);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1203, 419, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  напротив д. 5к1', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1204, 420, 1424131200, 1419033600, 1576713600, 6, 'Копейское шоссе  4', 61.511636, 55.122617);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1205, 421, 1424131200, 1419033600, 1576713600, 6, 'ул. Горького  57', 61.436366, 55.17866);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1206, 422, 1424131200, 1419033600, 1576713600, 6, 'ул. Котина  напротив д. 17', 61.448098, 55.177288);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1207, 423, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  21', 61.387085, 55.09338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1208, 424, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Бр. Кашириных и ул. Ворошилова  57а',
        61.323134, 55.179349);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1209, 425, 1424131200, 1419033600, 1576713600, 6, 'ул. Российская/ пр. Ленина  38', 61.417097, 55.162042);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1210, 426, 1424131200, 1419033600, 1576713600, 6, 'ул. Чичерина  15/1', 61.290183, 55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1211, 427, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1868 км + 510 м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1212, 428, 1424131200, 1419033600, 1576713600, 6, 'ул. Болейко  7', 61.406003, 55.185538);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1213, 429, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  напротив поста ГИБДД', 61.388387, 55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1214, 430, 1424131200, 1419033600, 1576713600, 6, 'шоссе Металлургов  15', 61.394693, 55.249032);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1215, 431, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  1ак1', 61.385001, 55.125319);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1216, 432, 1424131200, 1419033600, 1576713600, 6, 'ул. Богдана Хмельницкого  21/1', 61.390265, 55.258243);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1217, 433, 1424131200, 1419033600, 1576713600, 6, 'ул. Сталеваров  5', 61.403928, 55.263522);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1218, 434, 1424131200, 1419033600, 1576713600, 6, 'ул. Горького  напротив д. 19', 61.441882, 55.17009);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1219, 435, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр./ ул. Блюхера  101', 61.358976, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1220, 436, 1424131200, 1419033600, 1576713600, 6, 'шоссе Металлургов  25б', 61.389582, 55.247585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1221, 437, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  напротив д. 3а', 61.385198, 55.124202);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1222, 438, 1424131200, 1419033600, 1576713600, 6, 'ул. Красная  63', 61.394047, 55.161023);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1223, 439, 1424131200, 1419033600, 1576713600, 6, 'Свердловский пр.  53а/1', 61.390301, 55.16701);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1224, 440, 1424131200, 1419033600, 1576713600, 6, 'ул. Карла Маркса  81', 61.409785, 55.165313);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1225, 441, 1424131200, 1419033600, 1576713600, 6, 'уд. Доватора/ ул.Воровского  40', 61.38112, 55.146147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1226, 442, 1424131200, 1419033600, 1576713600, 6, 'ул. Российская/ ул. Труда  61', 61.41495, 55.167539);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1227, 443, 1424131200, 1419033600, 1576713600, 6, 'Свердловский пр. напротив д. 56', 61.387767, 55.164258);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1228, 444, 1424131200, 1419033600, 1576713600, 6, 'ул. Энгельса  23', 61.381335, 55.166953);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1229, 445, 1424131200, 1419033600, 1576713600, 6, 'ул. Энтузиастов/ пр. Ленина  74', 61.374068, 55.160221);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1230, 446, 1424131200, 1419033600, 1576713600, 6, 'Свердловский пр.  40а', 61.386393, 55.168634);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1231, 447, 1424131200, 1419033600, 1576713600, 6, 'ул. Гостевая/ ул. Заводская  7', 61.339923, 55.10206);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1232, 448, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  34', 61.420421, 55.161219);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1233, 449, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  9', 61.449904, 55.159717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1234, 450, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  26/1', 61.430374, 55.161527);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1235, 451, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  11', 61.447326, 55.159954);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1236, 452, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  23', 61.423709, 55.160633);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1237, 453, 1424131200, 1419033600, 1576713600, 5, 'ул. Артиллерийская  121', 61.433941, 55.160273);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1238, 454, 1424131200, 1419033600, 1576713600, 5, 'ул. Кирова  78/1', 61.399392, 55.171468);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1239, 455, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  76  поз. 1', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1240, 456, 1424131200, 1419033600, 1576713600, 5, 'пр. Ленина  30', 61.422918, 55.161281);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1241, 457, 1424131200, 1419033600, 1576713600, 5, 'ул. Рождественского  напротив д. 5  поз. 1', 61.445807,
        55.159804);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1242, 458, 1424131200, 1419033600, 1576713600, 5, 'ул. Воровского  17', 61.392142, 55.152989);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1243, 459, 1424131200, 1419033600, 1576713600, 5, 'ул. Энгельса/ пр. Ленина  71А', 61.382027, 55.15945);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1244, 460, 1424131200, 1419033600, 1576713600, 5, 'ул. Кирова (в центр)  150 м  до моста', 61.371616,
        55.040146);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1245, 461, 1424131200, 1419033600, 1576713600, 5, 'ул. Энгельса  26', 61.380509, 55.161332);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1246, 462, 1424131200, 1419033600, 1576713600, 5, 'ул. Рождественского  напротив д. 5  поз. 2', 61.445807,
        55.159804);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1247, 463, 1424131200, 1419033600, 1576713600, 5, 'ул. Российская  202', 61.415947, 55.161311);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1248, 464, 1424131200, 1419033600, 1576713600, 5, 'пересечение ул. Цвиллинга  25к2 и ул. Коммуны', 61.40594,
        55.164269);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1249, 465, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  29', 61.392987, 55.199619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1250, 466, 1424131200, 1419033600, 1576713600, 6, 'Комсомольский пр./ ул. Чайковского  20/3', 61.361788,
        55.191911);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1251, 467, 1424131200, 1419033600, 1576713600, 6, 'ул. Свободы/ ул. К. Маркса  38', 61.411788, 55.165888);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1252, 468, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр. 11', 61.386249, 55.109755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1253, 469, 1424131200, 1419033600, 1576713600, 6, 'ул. Гагарина/ ул. Гончаренко  63', 61.433339, 55.125536);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1254, 470, 1424131200, 1419033600, 1576713600, 6, 'Свердловкий тр.  цинковый завод', 61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1255, 471, 1424131200, 1419033600, 1576713600, 6, 'ул. Сталеваров  5к3', 61.403578, 55.262403);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1256, 472, 1424131200, 1419033600, 1576713600, 6, 'ул.Свободы/ Привокзальная площадь  1', 61.416136, 55.141315);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1257, 473, 1424131200, 1419033600, 1576713600, 6, 'ул. Горького  7', 61.441154, 55.16359);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1258, 474, 1424131200, 1419033600, 1576713600, 6, 'Комсомолький пр.  17', 61.380446, 55.191567);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1259, 475, 1424131200, 1419033600, 1576713600, 6, 'ул. Северный луч  напротив д. 47', 61.416001, 55.21897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1260, 476, 1424131200, 1419033600, 1576713600, 6, 'пр. Победы/ ул. Чайковского  83', 61.361168, 55.188396);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1261, 477, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Северный луч  47 и ул. Хлебозаводская',
        61.416001, 55.21897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1262, 478, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  3/3', 61.38147, 55.217516);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1263, 479, 1424131200, 1419033600, 1576713600, 6, 'ул. Героев Танкограда  61п  поз. 2', 61.441783, 55.20919);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1264, 480, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  напротив д. 14б', 61.370115, 55.23021);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1265, 481, 1424131200, 1419033600, 1576713600, 6, 'ул. Красная/ ул. Карла Маркса 133', 61.394289, 55.164942);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1266, 482, 1424131200, 1419033600, 1576713600, 6, 'пересечение Свердловского пр.  и ул. Воровского  26',
        61.388962, 55.151801);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1267, 483, 1424131200, 1419033600, 1576713600, 6, 'ул. Молодогвардейцев  56', 61.331991, 55.186546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1268, 484, 1424131200, 1419033600, 1576713600, 6, 'пересечение Свердловского пр. и пр. Победы  192а', 61.386474,
        55.184377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1269, 485, 1424131200, 1419033600, 1576713600, 6, 'ул. Черкасская 15', 61.378371, 55.230436);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1270, 486, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1868 км + 830 м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1271, 488, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1871 км + 120 м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1272, 489, 1424131200, 1419033600, 1576713600, 6, 'Комсомольский пр.  84', 61.305248, 55.194953);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1273, 490, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Богдана Хмельницкого  35 и ул. Румянцева',
        61.380347, 55.258089);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1274, 491, 1424131200, 1419033600, 1576713600, 6, 'ул. Дзержинского  119', 61.423116, 55.133334);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1275, 492, 1424131200, 1419033600, 1576713600, 6, 'Свердловский тр.  напротив д. 12в', 61.374796, 55.224341);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1276, 493, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1869 км + 850 м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1277, 494, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Красная (из центра) и ул. Труда', 61.393084,
        55.167587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1278, 495, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Красная (из центра) и ул. Коммуны',
        61.393318, 55.162616);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1279, 497, 1424131200, 1419033600, 1576713600, 6, 'ул. Салтыкова/ ул. Междугородная  21', 61.37661, 55.135907);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1280, 498, 1424131200, 1419033600, 1576713600, 6, 'Уфимский тр.  1/1 1868 км + 550 м', 61.318741, 55.077344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1281, 499, 1424131200, 1419033600, 1576713600, 6, 'автодорога Меридиан / ул. Артиллерийкая  136  поз. 1',
        61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1282, 500, 1424131200, 1419033600, 1576713600, 6, 'Свердловкий тр.  1а/2', 61.38607, 55.204156);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1283, 502, 1424131200, 1419033600, 1576713600, 6, 'ул. Краснознаменная/ ул. Островского  29', 61.380473,
        55.187584);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1284, 503, 1424131200, 1419033600, 1576713600, 6, 'ул. Молодогвардейцев  66', 61.331946, 55.182017);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1285, 504, 1424131200, 1419033600, 1576713600, 6, 'ул. 50 лет ВЛКСМ  16', 61.383249, 55.242432);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1286, 505, 1424131200, 1419033600, 1576713600, 6, 'пересечение ул. Хлебозаводская и ул. Морская', 61.417727,
        55.239627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1287, 506, 1424131200, 1419033600, 1576713600, 6, 'Троицкий тр.  100 м за после ост. "Подстанция" (в центр)',
        61.387965, 55.117957);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1288, 509, 1424131200, 1419033600, 1734566400, 1, 'автодорога Меридиан/ ул. Либединского  39', 61.421032,
        55.180465);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1289, 510, 1424131200, 1419033600, 1734566400, 7,
        'пересечение ул. Бр. Кашириных (из центра) и ул. Северо-Крымская', 61.358283, 55.178598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1290, 513, 1425427200, 1418083200, 1575763200, 5, 'пр. Ленина  17  за остановкой из центра', 61.437004,
        55.160525);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1291, 514, 1425427200, 1418083200, 1575763200, 6, 'ул.Черкасская 7', 61.38041, 55.241668);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1292, 515, 1425427200, 1418083200, 1575763200, 6, 'пр.Победы 317', 61.298223, 55.184541);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1293, 516, 1425427200, 1418083200, 1575763200, 6, 'ул.Гагарина  на газоне между д.32 и д.36', 61.397415,
        55.076891);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1294, 517, 1425427200, 1418083200, 1575763200, 6, 'ул.Свободы 88', 61.41363, 55.153966);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1295, 518, 1425427200, 1418083200, 1575763200, 6, 'пр.Ленина 6', 61.45533, 55.16054);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1296, 519, 1425427200, 1418083200, 1575763200, 6, 'ул.Молодогвардейцев напротив д.48', 61.332009, 55.190719);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1297, 520, 1425427200, 1418083200, 1575763200, 6, 'Свердловский пр.  ост. "Торговый центр" (из центра)',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1298, 521, 1425427200, 1418083200, 1575763200, 6, 'ул. Курчатова  19А', 61.389061, 55.148781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1299, 522, 1425427200, 1418083200, 1575763200, 6, 'ул.Энгельса 99', 61.380985, 55.150191);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1300, 523, 1425427200, 1418083200, 1575763200, 6, 'ул.Горького 22', 61.442852, 55.170496);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1301, 524, 1425427200, 1418083200, 1575763200, 6, ' пр.Победы 238', 61.370484, 55.184772);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1302, 525, 1425427200, 1418083200, 1575763200, 6, 'пересечение ул. Гагарина и ул. Южный Бульвар  19', 61.440975,
        55.136607);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1303, 526, 1425427200, 1418083200, 1575763200, 6, 'пересеч.Комсомольского пр.(из центра) и ул.Ворошилова у АЗС',
        61.32138, 55.194387);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1304, 527, 1425427200, 1418083200, 1575763200, 6, 'Свердловский пр 78 начало дома', 61.388773, 55.157192);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1305, 528, 1425427200, 1418083200, 1575763200, 6, 'ул.Курчатова 23 за маг."Декорум"', 61.384542, 55.151204);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1306, 529, 1425427200, 1418083200, 1575763200, 6, 'ул.Кирова 23-а', 61.400667, 55.173874);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1307, 530, 1425427200, 1418083200, 1575763200, 6, 'ул.Энгельса 46', 61.379916, 55.151276);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1308, 531, 1425427200, 1418083200, 1575763200, 6, 'ул.Комарова 110 поз.2', 61.457234, 55.177452);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1309, 532, 1425427200, 1418083200, 1575763200, 6, 'пр.Победы 287  больница Скорой помощи (в центр)', 61.339887,
        55.18781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1310, 533, 1425427200, 1418083200, 1575763200, 6, 'ул.Бр.Кашириных  87а (в центр)', 61.36866, 55.17681);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1311, 534, 1425427200, 1418083200, 1575763200, 6, 'автодорога Меридиан  со стороны дома 2/2 по ул.Луценко',
        61.43722, 55.151132);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1312, 535, 1425427200, 1418083200, 1575763200, 6, 'пр.Победы 308', 61.343795, 55.189532);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1313, 536, 1425427200, 1418083200, 1575763200, 6, 'Свердловский тр. (из центра)  перед трамвайным депо № 2',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1314, 538, 1425427200, 1418083200, 1575763200, 6, 'ул.Курчатова 24', 61.387785, 55.151559);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1315, 539, 1425427200, 1418083200, 1575763200, 6, 'пересеч.пр.Победы (из центра) и ул.Молодогвардейцев',
        61.332594, 55.189074);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1316, 540, 1425427200, 1418083200, 1575763200, 6, 'ул.Кирова  3', 61.399643, 55.181806);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1317, 541, 1425427200, 1418083200, 1575763200, 6, 'ул.Труда ост. "ТРК "Родник"(из центра)', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1318, 542, 1425427200, 1418083200, 1575763200, 6,
        'Свердловский пр.(из центр) набережная р.Миасс у Торгового центра', 61.387938, 55.172167);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1319, 543, 1425427200, 1418083200, 1575763200, 6, 'ул.Бр.Кашириных 129 (ЧелГУ)', 61.318759, 55.176501);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1320, 544, 1425427200, 1418083200, 1575763200, 6, 'ул.Гагарина 13 (газон перед Стоматологией)', 61.442196,
        55.140224);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1321, 545, 1425427200, 1418083200, 1575763200, 6, 'ул.Энгельса 43', 61.38147, 55.165271);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1322, 546, 1425427200, 1418083200, 1575763200, 6, 'пр.Победы(из центра)  перед Ленинградским мостом', 61.375227,
        55.183837);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1323, 547, 1425427200, 1418083200, 1575763200, 6, 'ул. Степана Разина  4', 61.411725, 55.141428);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1324, 548, 1425427200, 1418083200, 1575763200, 6, 'ул.Комарова 110 поз.1', 61.457234, 55.177452);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1325, 549, 1425427200, 1418083200, 1575763200, 6, 'пересечение Сверловского пр. 58 и ул. Коммуны', 61.388181,
        55.163564);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1326, 550, 1425427200, 1418083200, 1575763200, 6, 'пересечение  ул.Бр.Кашириных (из центра) и ул.Актюбинская',
        61.353894, 55.178709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1327, 551, 1425427200, 1418083200, 1575763200, 6, 'ул.Курчатова 27/ ул. Энгельса  95', 61.380877, 55.152377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1328, 552, 1425427200, 1418688000, 1576368000, 6, 'ул. Черкасская 3', 61.379853, 55.242458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1329, 553, 1425427200, 1418688000, 1576368000, 6, 'Свердловский тр. (в центр)  авторынок', 61.368543,
        55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1330, 554, 1425427200, 1418688000, 1576368000, 6, 'ул. Индивидуальная  63', 61.349733, 55.239645);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1331, 555, 1425427200, 1418688000, 1576368000, 6, 'Комсомольский пр.  78', 61.310809, 55.194959);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1332, 556, 1425427200, 1418688000, 1576368000, 6, 'Копейское шоссе  100 м от виадука  из города', 61.445161,
        55.151127);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1333, 557, 1425427200, 1418688000, 1576368000, 6, 'ул. Хлебозаводская 5а', 61.416316, 55.243397);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1334, 558, 1425427200, 1418688000, 1576368000, 6, 'ул. Профинтерна/ ул. Московская  26', 61.3858, 55.131367);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1335, 559, 1425427200, 1418688000, 1576368000, 6, 'Троицкий тр.  7/1', 61.385746, 55.106938);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1336, 560, 1425427200, 1418688000, 1576368000, 6, 'Копейское шоссе/ ул. Харлова  2', 61.443607, 55.147376);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1337, 561, 1425427200, 1418688000, 1576368000, 6, 'ул. Бажова  91', 61.45462, 55.181976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1338, 562, 1425427200, 1418688000, 1576368000, 6, 'ул.Горького  5', 61.441073, 55.162952);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1339, 563, 1425427200, 1418688000, 1576368000, 6, 'Комсомольский пр.  16', 61.368876, 55.192266);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1340, 564, 1425427200, 1418688000, 1576368000, 6, 'пр. Победы 111', 61.438765, 55.186839);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1341, 565, 1425427200, 1418688000, 1576368000, 6, 'ул. Артиллерийская  105', 61.435468, 55.168763);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1342, 566, 1425427200, 1418688000, 1576368000, 6, 'Комсомольский пр.  29  у завода "Прибор"', 61.357126,
        55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1343, 567, 1425427200, 1418688000, 1576368000, 6, 'Троицкий тр.  600 м после ост. "Подстанция"', 61.388387,
        55.086775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1344, 568, 1425427200, 1418688000, 1576368000, 6, 'ул. Модавская  19', 61.304143, 55.192713);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1345, 569, 1425427200, 1418688000, 1576368000, 6, 'ул. Чичерина  29', 61.297729, 55.179216);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1346, 570, 1425427200, 1418688000, 1576368000, 6, 'автодорога Меридиан/ул. Механическая  40', 61.418229,
        55.194203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1347, 571, 1425427200, 1418688000, 1576368000, 6, 'Копейское шоссе  49', 61.447856, 55.144022);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1348, 572, 1425427200, 1418688000, 1576368000, 6, 'ул. Черкасская/ ул. 50-летия ВЛКСМ  39', 61.379844,
        55.236088);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1349, 573, 1425427200, 1418688000, 1576368000, 6, 'Троицкий тр./ ул. Грязева  3', 61.434471, 55.041715);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1350, 574, 1425427200, 1418688000, 1576368000, 6, 'ул. Героев Танкограда  71П ст10', 61.438244, 55.211846);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1351, 575, 1425427200, 1418688000, 1576368000, 6, 'ул. Жукова 12', 61.39039, 55.257504);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1352, 576, 1425427200, 1418688000, 1576368000, 6, 'ул. Хлебозаводская  напротив д. 42', 61.415885, 55.240513);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1353, 577, 1425427200, 1418688000, 1576368000, 6, 'Комсомольский пр. / ул. Молдавская  12', 61.300074,
        55.193602);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1354, 578, 1425427200, 1418688000, 1576368000, 6, 'ул. Труда  24', 61.425398, 55.16864);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1355, 579, 1425427200, 1418688000, 1576368000, 6, 'ул. Черкасская  8', 61.378389, 55.238752);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1356, 580, 1425427200, 1418688000, 1576368000, 6, 'Свердловский тр.  1а/3  поворот на Лакокрасочный завод',
        61.386186, 55.203863);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1357, 581, 1425427200, 1418688000, 1576368000, 6, 'ул. Каслинская  19', 61.39349, 55.188201);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1358, 582, 1425427200, 1418688000, 1576368000, 6, 'Троицкий тр.  11а', 61.385899, 55.111789);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1359, 583, 1425427200, 1418688000, 1576368000, 6, 'пересечение ул. Молодогвардейцев и Комсомольского пр.  48',
        61.334515, 55.195015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1360, 584, 1425427200, 1418688000, 1576368000, 6, 'Свердловский пр./  Комсомольский пр.  2', 61.384614,
        55.193217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1361, 585, 1425427200, 1418688000, 1576368000, 6, 'ул. С. Кривой  28', 61.393238, 55.158189);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1362, 586, 1425427200, 1418688000, 1576368000, 6, 'Комсомольский пр.  103', 61.294917, 55.190195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1363, 626, 1427760000, 1398902400, 1556582400, 6, 'ул. Гагарина 7  Политех', 61.396724, 55.074494);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1364, 627, 1427760000, 1398902400, 1556582400, 6, 'пересечение ул. Гагарина 7 и ул. Тухачевского ', 61.396724,
        55.074494);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1365, 641, 1430352000, 1414800000, 1572480000, 9, 'ул. Молодогвардейцев 53', 61.333581, 55.18103);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1366, 646, 1430784000, 1426723200, 1573689600, 8, 'ул.Карла Маркса 38', 61.411788, 55.165888);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1367, 654, 1432080000, 1427760000, 1585526400, 6,
        'п.Шершни  ул.Центральная 3б (ул.Гостевая со стороны стадиона)', 61.306066, 55.149218);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1368, 658, 1432080000, 1421280000, 1578960000, 17, 'Комсомольский пр. 85', 61.308527, 55.194018);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1369, 664, 1432771200, 1432080000, 1589846400, 6, 'ул.Энтузиастов  15д', 61.376862, 55.15197);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1370, 717, 1439942400, 1437004800, 1594771200, 12, 'Копейское шоссе  52  поз. 1', 61.460172, 55.139983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1371, 718, 1439942400, 1437004800, 1594771200, 12, 'Копейское шоссе  52  поз. 1', 61.460172, 55.139983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1372, 719, 1439942400, 1437004800, 1594771200, 12, 'Копейское шоссе  52  поз. 1', 61.460172, 55.139983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1373, 727, 1440979200, 1441065600, 1598832000, 6, 'Копейское шоссе  58', 61.453084, 55.142195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1374, 767, 1445558400, 1445299200, 1603065600, 12, 'ул. Шарова  напротив д. 59', 61.394846, 55.118972);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1375, 768, 1445558400, 1445299200, 1603065600, 19,
        'Курганский тр. (перед развязкой ЧМЗ-Аэропорт-Курганский тр.)', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1376, 769, 1445558400, 1445299200, 1603065600, 19,
        'Курганский тр. (после развязки ЧМЗ-Аэропорт-Курганский тр.)', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1377, 770, 1445558400, 1445299200, 1603065600, 19,
        'Автодорога ЧМЗ (перед развязкой ЧМЗ-Аэропорт-Курганский тр.)', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1378, 781, 1445817600, 1445299200, 1603065600, 12, 'автодорога Меридиан (7-ой км)', 61.420044, 55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1379, 782, 1445817600, 1445299200, 1603065600, 12, 'ул.Коммуны 125', 61.383698, 55.162109);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1380, 786, 1445990400, 1445299200, 1603065600, 6, 'ул. Труда  197', 61.363369, 55.170321);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1381, 787, 1445990400, 1445299200, 1603065600, 6, 'ул. Бр. Кашириных 147', 61.296382, 55.168526);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1382, 788, 1445990400, 1445299200, 1603065600, 6, 'Свердловский пр.  86', 61.388971, 55.153277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1383, 789, 1445990400, 1445299200, 1603065600, 6, 'ул. Курчатова  8б', 61.393885, 55.148302);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1384, 790, 1445990400, 1445299200, 1603065600, 6, 'Свердловский пр.  напротив д. 64', 61.387857, 55.160967);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1385, 791, 1445990400, 1445299200, 1603065600, 6, 'ул. Курчатова  между домами № 16 и № 14', 61.391441,
        55.149851);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1386, 792, 1445990400, 1445299200, 1603065600, 6, 'Комсомольский пр.  30', 61.350218, 55.19519);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1387, 793, 1445990400, 1445299200, 1603065600, 6, 'ул.250 лет Челябинску 12', 61.304431, 55.181241);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1388, 794, 1445990400, 1445299200, 1603065600, 2, 'ул. Худякова  22', 61.358392, 55.148097);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1389, 795, 1445990400, 1445299200, 1603065600, 2, 'ул.Чичерина  33', 61.301394, 55.176871);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1390, 796, 1445990400, 1445299200, 1603065600, 6, 'пр.Победы (в центр) за Ленинградским мостом', 61.375227,
        55.183837);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1391, 798, 1445990400, 1445299200, 1603065600, 6, 'пересечение ул. Труда  напротив д. 161 и Свердловского пр.',
        61.38783, 55.167087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1392, 799, 1445990400, 1445299200, 1603065600, 6, 'ул. Доватора  1г', 61.403263, 55.136164);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1393, 800, 1445990400, 1445299200, 1603065600, 6,
        'пересечение шоссе Меридиан и ул. Рождественского (из центра)', 61.442978, 55.153056);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1394, 801, 1445990400, 1445299200, 1603065600, 6, 'пересеч.пр.Победы и ул.Бажова  напротив д.23', 61.466693,
        55.192548);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1395, 802, 1445990400, 1445299200, 1603065600, 6, 'ул. Труда напротив д. 203  ост. "Родничок"(в центр)',
        61.355976, 55.171051);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1396, 803, 1445990400, 1445299200, 1603065600, 6, 'ул.Кирова 1', 61.399706, 55.182937);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1397, 804, 1445990400, 1445299200, 1603065600, 6, 'ул.Труда 168/1', 61.373772, 55.172254);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1398, 805, 1445990400, 1445299200, 1603065600, 6, 'ул.Цвиллинга 58', 61.41027, 55.150972);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1399, 808, 1445990400, 1445299200, 1603065600, 6, 'ул.Труда 185-1 АЗС "Лукойл"', 61.373834, 55.169375);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1400, 809, 1445990400, 1445299200, 1603065600, 6, 'пр.Победы 158/ ул.Болейко 7б', 61.407827, 55.185605);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1401, 810, 1445990400, 1445299200, 1603065600, 6, 'ул.Российская  67', 61.4161, 55.171432);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1402, 812, 1445990400, 1445299200, 1603065600, 6, 'пр.Комарова 114', 61.454584, 55.174054);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1403, 825, 1446508800, 1446336000, 1601510400, 17, 'ул.Елькина 63а', 61.399185, 55.155119);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1404, 826, 1446508800, 1445299200, 1603065600, 8, 'ул.С.Кривой 49б  поз.1', 61.384974, 55.157613);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1405, 827, 1446508800, 1445299200, 1603065600, 8, 'ул.С.Кривой 49б  поз.2', 61.384974, 55.157613);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1406, 828, 1446508800, 1445299200, 1603065600, 6, 'ул. Кирова 3', 61.399643, 55.181806);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1407, 829, 1446508800, 1445299200, 1603065600, 6, 'дорога в Аэропорт поворот на таможню', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1408, 832, 1446681600, 1445299200, 1603065600, 15, 'ул.Бр.Кашириных 137 поз.1', 61.301628, 55.171087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1409, 833, 1446681600, 1445299200, 1603065600, 15, 'ул.Бр.Кашириных 137 поз.2', 61.301628, 55.171087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1410, 849, 1447372800, 1443657600, 1597017600, 17, 'пр.Ленина 20', 61.439753, 55.161327);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1411, 856, 1447891200, 1445299200, 1603065600, 5, 'пр.Ленина 64', 61.383842, 55.160689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1412, 857, 1447891200, 1445299200, 1603065600, 5, 'пр.Ленина 71', 61.383941, 55.15947);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1413, 858, 1447891200, 1445299200, 1603065600, 6, 'Комсомольский пр.  66', 61.32458, 55.194979);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1414, 859, 1447372800, 1445299200, 1603065600, 6, 'пр.Победы  325  газон у парковки ТК "Прииск"', 61.293651,
        55.182444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1415, 860, 1447372800, 1445299200, 1603065600, 6, 'автодорога Меридиан  ул. Гражданская 10/1 (рынок Меридиан)',
        61.423224, 55.140291);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1416, 861, 1447372800, 1445299200, 1603065600, 6, 'пр.Победы 131', 61.422388, 55.185209);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1417, 862, 1447372800, 1445299200, 1603065600, 5, 'пр.Ленина 62', 61.394693, 55.160483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1418, 866, 1447891200, 1445299200, 1603065600, 6, 'ул. Плеханова  19', 61.416046, 55.155458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1419, 867, 1447891200, 1445299200, 1603065600, 6, 'пр. Комарова  125', 61.461573, 55.180547);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1420, 868, 1447891200, 1445299200, 1603065600, 6, 'ул.Новороссийская  88', 61.455267, 55.114441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1421, 869, 1447891200, 1445299200, 1603065600, 6, 'ул.Блюхера  63', 61.367205, 55.130003);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1422, 870, 1447891200, 1445299200, 1603065600, 6, 'ул. Доватора  27', 61.384911, 55.144788);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1423, 871, 1447891200, 1445299200, 1603065600, 6, 'ул. Молодогвардейцев  напротив д.40', 61.331964, 55.192528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1424, 872, 1447891200, 1445299200, 1603065600, 6, 'ул. Российская  267А', 61.418301, 55.157593);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1425, 873, 1447891200, 1445299200, 1603065600, 6, 'Комсомольский пр.  47', 61.329035, 55.194085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1426, 874, 1447891200, 1445299200, 1603065600, 6, 'ул. Бр. Кашириных  108', 61.313054, 55.178203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1427, 875, 1447891200, 1445299200, 1603065600, 6, 'пр. Победы  323', 61.29507, 55.183127);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1428, 876, 1447891200, 1445299200, 1603065600, 6, 'пересечение Комсомольского пр. и ул. Чайковского  15/1',
        61.363935, 55.191135);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1429, 17, 1452470400, 1450742400, 1608508800, 16, 'ул.Краснознаменная 1А', 61.380141, 55.191808);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1430, 18, 1452470400, 1450742400, 1608508800, 16, 'ул. Воровского  73а', 61.376844, 55.14203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1431, 19, 1452470400, 1450742400, 1608508800, 16, 'ул. Энгельса  44г', 61.380877, 55.153457);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1432, 20, 1452470400, 1450742400, 1608508800, 16, 'ул. С. Кривой  83', 61.3684, 55.156976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1433, 21, 1452470400, 1450742400, 1608508800, 16, 'ул. 40 лет Победы/пр. Победы  382а', 61.299086, 55.186397);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1434, 22, 1452470400, 1450742400, 1608508800, 16, 'ул. Воровского  21', 61.390786, 55.151528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1435, 23, 1452470400, 1450742400, 1608508800, 16, 'ул. Доватора  35 ', 61.382458, 55.145915);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1436, 24, 1452470400, 1450742400, 1608508800, 16, 'ул. Братьев Кашириных  95 ', 61.361177, 55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1437, 25, 1452470400, 1450742400, 1608508800, 16, 'ул. Воровского  32', 61.386249, 55.150108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1438, 26, 1452470400, 1450742400, 1608508800, 16, 'ул. 40 лет Победы  35', 61.305221, 55.181961);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1439, 27, 1452470400, 1450742400, 1608508800, 16, 'ул. 250 лет Челябинску  17', 61.299885, 55.178198);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1440, 28, 1452470400, 1450742400, 1608508800, 16, 'ул. Воровского  55', 61.383168, 55.146666);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1441, 29, 1452470400, 1450742400, 1608508800, 8, 'Свердловский пр. 65', 61.389582, 55.158215);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1442, 30, 1452556800, 1450742400, 1608508800, 6, 'ул. Университетская Набережная  36', 61.342977, 55.17502);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1443, 31, 1452556800, 1450742400, 1608508800, 6, 'Свердловский пр.  напротив д. 28', 61.386456, 55.182264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1444, 33, 1452556800, 1450742400, 1608508800, 6, 'пересечение ул. Доватора  23 и ул.Сулимова', 61.386878,
        55.143029);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1445, 35, 1452556800, 1450742400, 1608508800, 6, 'дорога из аэропорта Баландино (380 м от аэропорта)', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1446, 37, 1452556800, 1450742400, 1608508800, 6, 'ул. Университетская Набережная  30', 61.349355, 55.175072);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1447, 38, 1452556800, 1450742400, 1608508800, 6, 'ул. Курчатова  6', 61.39561, 55.147809);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1448, 39, 1452556800, 1450742400, 1608508800, 6, 'ул. Салавата Юлаева  17в', 61.289456, 55.175889);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1449, 40, 1452556800, 1450742400, 1608508800, 12, 'ул.Радонежская 12', 61.356147, 55.225399);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1450, 49, 1452556800, 1450742400, 1608508800, 12, 'ул.Бр.Кашириных 129д', 61.314734, 55.177334);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1451, 50, 1452556800, 1450742400, 1608508800, 12, 'ул.Чичерина  52', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1452, 51, 1452556800, 1450742400, 1608508800, 12, 'пр. Ленина  3е  стр. 1', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1453, 52, 1452556800, 1450742400, 1608508800, 12, 'пр. Ленина  3е  стр. 2', 61.462534, 55.158585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1454, 53, 1452556800, 1450742400, 1608508800, 6, 'ул. Молодогвардейцев 35б', 61.333563, 55.186643);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1455, 54, 1452556800, 1450742400, 1766275200, 1, 'Копейское шоссе  17', 61.496535, 55.123018);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1456, 55, 1452556800, 1450742400, 1766275200, 1, 'Троицкий тракт  25', 61.387282, 55.086517);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1457, 56, 1452556800, 1450742400, 1766275200, 1, 'ул. Героев Танкограда  2в', 61.444954, 55.217768);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1458, 57, 1452556800, 1450742400, 1608508800, 12, 'ул.Новоэлеваторная 49/9', 61.353227, 55.105274);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1459, 58, 1452556800, 1450742400, 1608508800, 15, 'ул.Новоэлеваторная 49/7', 61.353227, 55.105274);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1460, 62, 1452816000, 1450742400, 1608508800, 12, 'ул.Бр.Кашириных напротив д.103', 61.341845, 55.178121);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1461, 63, 1452816000, 1450742400, 1608508800, 12, 'ул.Гагарина 7а', 61.443418, 55.142437);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1462, 64, 1452816000, 1450742400, 1608508800, 15, 'ул.Гагарина 7а', 61.443418, 55.142437);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1463, 65, 1452816000, 1450742400, 1608508800, 9, 'ул.Татьяничевой напротив д.22/11', 61.367915, 55.146749);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1464, 66, 1452816000, 1450742400, 1608508800, 9, 'ул.Образцова напротив д.24', 61.370493, 55.146023);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1465, 67, 1452816000, 1450742400, 1608508800, 6, 'ул. 40 лет Победы  15', 61.295483, 55.189264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1466, 68, 1452816000, 1450742400, 1608508800, 6, 'ул. 50 лет ВЛКСМ  31', 61.381856, 55.238691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1467, 69, 1452816000, 1450742400, 1766275200, 4,
        'пересечение ул. Братьев Кашириных (из центра) и ул. Молодогвардейцев  70а', 61.330994, 55.179344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1468, 70, 1452816000, 1450742400, 1766275200, 4, 'пересечение Комсомольского пр. 2 и Свердловского пр.',
        61.384614, 55.193217);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1469, 72, 1452816000, 1450742400, 1608508800, 6, 'ул. Новороссийская  122', 61.438477, 55.117046);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1470, 73, 1452816000, 1450742400, 1608508800, 6, 'ул. 40 лет Победы  напротив д. 22', 61.296858, 55.185949);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1471, 74, 1452816000, 1450742400, 1608508800, 6, 'ул. 1 Пятилетки  напротив д. 37', 61.441594, 55.167328);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1472, 75, 1452816000, 1450742400, 1608508800, 6,
        'ул.Воровского  автопарковка обл.больницы  между опорами №11 и 13', 61.386725, 55.149661);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1473, 76, 1452816000, 1450742400, 1608508800, 6, 'ул.С.Юлаева 27 у ТД"Одиссей"', 61.293633, 55.172825);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1474, 77, 1452816000, 1450742400, 1608508800, 6, 'Комсомольский пр. 101', 61.296921, 55.191089);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1475, 78, 1452816000, 1450742400, 1608508800, 6, 'ул.1 Пятилетки между д.41 и 43', 61.439097, 55.167236);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1476, 79, 1452816000, 1450742400, 1608508800, 6, 'ул.1 Пятилетки между д.57 и 55', 61.431596, 55.167909);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1477, 80, 1452816000, 1450742400, 1608508800, 6, 'ул.50 лет ВЛКСМ 27', 61.382386, 55.239276);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1478, 81, 1452816000, 1450742400, 1608508800, 6, 'пересеч.ул.Артиллерийской и ул.Бажова', 61.428985, 55.178059);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1479, 82, 1452816000, 1450742400, 1608508800, 6, 'ул. Образцова 7', 61.375721, 55.145545);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1480, 87, 1452816000, 1450742400, 1608508800, 6, 'ул.1-я Потребительская/ Троицкий тр. 15', 61.385593,
        55.104213);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1481, 88, 1452816000, 1450742400, 1608508800, 6, 'ул.1-я Потребительская 1  напротив фабрики "Линда"',
        61.372622, 55.102266);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1482, 89, 1452816000, 1450742400, 1608508800, 6, 'ул.Лесопарковая 6', 61.363648, 55.150345);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1483, 91, 1453766400, 1450742400, 1608508800, 6, 'ул.Черкасская/ ул.Комаровского 2', 61.378209, 55.241976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1484, 99, 1454371200, 1447027200, 1604880000, 17, 'ул.Новороссийская 39', 61.484722, 55.107061);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1485, 100, 1454371200, 1446422400, 1604275200, 17, 'ул.Новороссийская  43', 61.483609, 55.107339);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1486, 101, 1454371200, 1427414400, 1585267200, 17, 'ул.Гагарина 47', 61.435549, 55.121258);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1487, 102, 1454544000, 1450742400, 1608508800, 12, 'пересечение шоссе Металлургов  70/1 и ул.Черкасская',
        61.378748, 55.247231);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1488, 103, 1454544000, 1450742400, 1608508800, 12,
        'пересечение ул.Богдана Хмельницкого и ул.Румянцева (в центр)', 61.379359, 55.258325);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1489, 104, 1454544000, 1450742400, 1608508800, 12, 'ул.Сталеваров 17', 61.403434, 55.257237);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1490, 105, 1454544000, 1450742400, 1608508800, 6, 'автодорога Меридиан  напротив д.47 по ул.Либединского',
        61.423799, 55.176105);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1491, 106, 1454544000, 1450742400, 1608508800, 6, 'пересечение ул.Автодорожная и ул.Производственная',
        61.356024, 55.208311);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1492, 107, 1454630400, 1450742400, 1608508800, 8, 'пр.Ленина 87', 61.365938, 55.158904);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1493, 126, 1455753600, 1455753600, 1613520000, 12, 'ул. Профессора Благих  напротив домов 61 и 63', 61.302418,
        55.215426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1494, 128, 1458518400, 1450742400, 1766275200, 1, 'пр.Победы 330', 61.329197, 55.189491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1495, 129, 1458518400, 1450742400, 1766275200, 1, 'ул.Блюхера 95', 61.363378, 55.119425);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1496, 131, 1458518400, 1450742400, 1766275200, 1, 'Свердловский тр.  у АЗС "Лукойл" в районе рынка "Орбита"',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1497, 132, 1458518400, 1450742400, 1766275200, 1, 'ул.Блюхера 49', 61.371723, 55.133447);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1498, 133, 1458518400, 1450742400, 1766275200, 1, 'ул.Бр.Кашириных 105', 61.338495, 55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1499, 134, 1458518400, 1450742400, 1766275200, 1, 'ул.Дзержинского 107', 61.427653, 55.13268);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1500, 135, 1458518400, 1450742400, 1766275200, 1, 'пересеч.ул.Бажова 11 и ул. Загорская', 61.468445, 55.194758);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1501, 136, 1458518400, 1448928000, 1606694400, 17, 'Копейское шоссе 64', 61.451979, 55.145442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1502, 137, 1458518400, 1448928000, 1606694400, 17, 'Копейское шоссе 64', 61.451979, 55.145442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1503, 138, 1458691200, 1456444800, 1771977600, 7, 'ул. Кирова  161', 61.401574, 55.163775);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1504, 139, 1459123200, 1450742400, 1608508800, 6, 'Копейское шоссе  44', 61.467744, 55.134507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1505, 151, 1460073600, 1450742400, 1608508800, 6, 'Комсомольский пр.  65', 61.319639, 55.193479);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1506, 152, 1460073600, 1450742400, 1608508800, 6, 'ул. Гагарина  42', 61.433222, 55.127476);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1507, 153, 1460073600, 1450742400, 1608508800, 6, 'ул.Комарова  114', 61.454584, 55.174054);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1508, 154, 1460073600, 1450742400, 1608508800, 6, 'ул. Бр. Кашириных  79', 61.373906, 55.17665);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1509, 155, 1460073600, 1450742400, 1608508800, 6, 'ул. Чичерина/ пр. Победы  325', 61.293651, 55.182444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1510, 156, 1460073600, 1450742400, 1608508800, 6, 'Копейское шоссе  39', 61.453263, 55.139977);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1511, 157, 1460073600, 1450742400, 1608508800, 6, 'Троицкий тр. (в город)  поворот в пос. Смолино', 61.415238,
        55.056445);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1512, 158, 1460073600, 1450742400, 1608508800, 6, 'ул. Хохрякова  20', 61.487804, 55.179221);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1513, 159, 1460073600, 1450742400, 1608508800, 6, 'Курганское шоссе (из города)  после поворота на ТЭЦ-3',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1514, 160, 1460073600, 1450742400, 1608508800, 6, 'ул. Свободы  74', 61.411671, 55.158781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1515, 161, 1460073600, 1450742400, 1608508800, 6, 'шоссе Металлургов/ ул. Сталеваров  66/3', 61.40276,
        55.251778);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1516, 162, 1460073600, 1450742400, 1608508800, 6, 'ул. Богдана Хмельницкого  напротив д. 28', 61.378281,
        55.258674);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1517, 163, 1460073600, 1450742400, 1608508800, 6, 'ул. Румянцева  28б', 61.379009, 55.251511);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1518, 164, 1460073600, 1450742400, 1608508800, 6, 'ул. Гагарина  напротив д. 2', 61.444649, 55.146193);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1519, 166, 1460937600, 1450742400, 1608508800, 17, 'ул.Сони Кривой  напротив д.26 поз.1', 61.395493, 55.158261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1520, 167, 1460937600, 1450742400, 1608508800, 17, 'ул.Сони Кривой  напротив д.26 поз.2', 61.395493, 55.158261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1521, 168, 1460937600, 1450742400, 1608508800, 17, 'ул.Сони Кривой  напротив д.26 поз.3', 61.395493, 55.158261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1522, 169, 1460937600, 1450742400, 1608508800, 17, 'ул.Сони Кривой  напротив д.26 поз.4', 61.395493, 55.158261);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1523, 170, 1460937600, 1450742400, 1608508800, 17, 'пересеч. пр.Ленина 62 и ул.Васенко', 61.394693, 55.160483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1524, 171, 1460937600, 1450742400, 1608508800, 17, 'ул.Лесопарковая 7 (поз.5)', 61.365417, 55.155612);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1525, 179, 1462406400, 1461974400, 1619654400, 12, 'Копейское шоссе  37б', 61.456533, 55.138789);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1526, 196, 1463097600, 1461974400, 1619654400, 6, 'пр.Ленина 3/1', 61.465714, 55.158796);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1527, 201, 1463443200, 1461974400, 1619654400, 12, 'Комсомольский пр.  78', 61.310809, 55.194959);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1528, 202, 1463443200, 1461974400, 1619654400, 16, 'ул. Воровского 6', 61.395089, 55.157423);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1529, 203, 1463443200, 1461974400, 1619654400, 16, 'пр.Ленина 89', 61.363477, 55.158174);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1530, 204, 1463443200, 1461974400, 1619654400, 16, 'ул.Лесопарковая 9а', 61.365363, 55.153118);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1531, 205, 1463443200, 1461974400, 1619654400, 16, 'Свердловский  пр.  ост. "ул. Южная" (в центр)', 61.383851,
        55.033615);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1532, 206, 1463443200, 1461974400, 1619654400, 16, 'ул. Салютная 11', 61.451503, 55.171319);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1533, 211, 1463702400, 1461974400, 1619654400, 12, 'ул. Дарвина 18/1', 61.370529, 55.125716);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1534, 218, 1464048000, 1461974400, 1619654400, 6,
        'пересеч.ул.Автодорожная (из центра) и ул. Производственная (100 м до пересечения)', 61.356024, 55.208311);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1535, 223, 1464825600, 1470009600, 1627689600, 6, 'Сибирский переезд  111а/1 в сторону Ленинского района',
        61.431039, 55.150844);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1536, 224, 1464825600, 1470009600, 1627689600, 6, 'ул.Кожзаводская  106а', 61.39746, 55.197471);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1537, 225, 1464825600, 1470009600, 1627689600, 6, 'пересечение а/д Меридиан и ул. Труда  в сторону центра',
        61.428281, 55.16812);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1538, 226, 1464825600, 1470009600, 1627689600, 6, 'а/д Меридиан  напротив дома №4 по пер. Артиллерийский',
        61.436474, 55.159491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1539, 227, 1464825600, 1470009600, 1627689600, 6,
        'пересечение ул.Железнодорожной и ул.Самовольной в сторону Железнодорожного вокзала', 61.404649, 55.129466);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1540, 228, 1464825600, 1470009600, 1627689600, 6,
        'пересечение ул.Железнодорожной и ул.Самовольной в сторону Троицкого тракта', 61.404649, 55.129466);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1541, 229, 1464825600, 1470009600, 1627689600, 6, 'а/д Меридиан  напротив здания Меридиан автодорога  1/2',
        61.435917, 55.150756);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1542, 231, 1464912000, 1461974400, 1619654400, 6, 'въезд в пос. Западный', 61.37069, 55.208358);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1543, 233, 1466380800, 1417219200, 1574899200, 6, 'пр.Победы  312', 61.341208, 55.19001);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1544, 234, 1466380800, 1417219200, 1574899200, 6, 'Свердловский тр.  9а', 61.37794, 55.225039);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1545, 235, 1466380800, 1417219200, 1574899200, 6,
        'ул.Автодорожная (в центр)  после пересечения с ул.Мастеровая', 61.362682, 55.207986);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1546, 236, 1466380800, 1417219200, 1574899200, 6, 'Троицкий тр.  23', 61.387471, 55.088898);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1547, 237, 1466380800, 1417219200, 1574899200, 6, 'шоссе Металлургов  31', 61.384758, 55.247457);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1548, 239, 1466553600, 1467244800, 1624924800, 17, 'ул.Гагарина  39 (сторона А)', 61.434947, 55.12517);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1549, 240, 1466553600, 1467244800, 1624924800, 17, 'ул.Гагарина  39 (сторона Б со стороны ул. Гончаренко)',
        61.434947, 55.12517);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1550, 241, 1466640000, 1456790400, 1614470400, 17, 'Копейское шоссе  64  поз. 1', 61.451979, 55.145442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1551, 242, 1466640000, 1456790400, 1614470400, 17, 'Копейское шоссе  64  поз. 2', 61.451979, 55.145442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1552, 243, 1466640000, 1456790400, 1614470400, 17, 'Копейское шоссе  64  поз. 3', 61.451979, 55.145442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1553, 255, 1468454400, 1445299200, 1603065600, 5, 'ул.Воровского 38б', 61.383455, 55.14787);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1554, 266, 1471305600, 1450742400, 1608508800, 14,
        'пр. Ленина  26а  ост. "ТРК Горки" (троллейбус  в сторону ПКиО)', 61.431345, 55.161908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1555, 267, 1471305600, 1450742400, 1608508800, 14,
        'ул. Свободы  98  ост. "ул. Евтеева" (троллейбус  в сторону ж/д вокзала)', 61.415265, 55.149573);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1556, 268, 1471305600, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  напротив д. 105  ост. "Каширинский рынок"', 61.338495, 55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1557, 269, 1471305600, 1450742400, 1608508800, 14,
        'ул. Молодогвардейцев  62  ост. "Педучилище" (троллейбус  в центр)', 61.331964, 55.183575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1558, 270, 1471305600, 1450742400, 1608508800, 14,
        'пр. Победы  129  ост. "Юридический институт" (трамвай  в сторону ЧТЗ)', 61.423601, 55.185225);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1559, 271, 1471305600, 1450742400, 1608508800, 14, 'ул. Труда  166  ост. "Молния" (троллейбус  в сторону С-З)',
        61.374562, 55.170784);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1560, 272, 1471305600, 1450742400, 1608508800, 14,
        'ул. Дарвина  35  ост. "ЦРМ" (автобус  в сторону Троицкого тр.)', 61.381991, 55.118972);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1561, 273, 1471305600, 1450742400, 1608508800, 14,
        'ул. Горького  60  ост. "ул. Лермонтова" (трамвай  в сторону С/З)', 61.435396, 55.182058);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1562, 274, 1471305600, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  152  ост. "ул. Академика Макеева" (автобус  в сторону плотины)', 61.294127, 55.166547);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1563, 275, 1471305600, 1450742400, 1608508800, 14,
        'Троицкий тр.  54б  ост. "ул. Потребительская" (автобус  в сторону ул. Блюхера)', 61.387938, 55.10736);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1564, 276, 1471305600, 1450742400, 1608508800, 14,
        'ул. Героев Танкограда/ул. Салютная  2  ост. "Отделочные материалы" (автобус  в сторону Аэропорта)', 61.456965,
        55.171766);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1565, 277, 1471305600, 1450742400, 1608508800, 14,
        'ул. Горького/ул. Салютная  27  ост. "Башня" (трамвай  в сторону С-З)', 61.444065, 55.169663);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1566, 278, 1471305600, 1450742400, 1608508800, 14,
        'ул. Черкасская  17  ост. "ул. Черкасская" (трамвай  в центр)', 61.375532, 55.229085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1567, 279, 1471305600, 1450742400, 1608508800, 14,
        'пр. Победы  168  ост. "Теплотехнический институт" (трамвай  в сторону С-З  центра)', 61.398493, 55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1568, 280, 1471305600, 1450742400, 1608508800, 14,
        'ул. Ворошилова  2к 1  ост. "Ткацкая фабрика" (автобус  в центр)', 61.325191, 55.202327);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1569, 281, 1471305600, 1450742400, 1608508800, 14,
        'ул. Молодогвардейцев  34  ост. "ул. Молодогвардейцев" (маршрутное такси  в центр)', 61.331901, 55.193885);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1570, 282, 1471305600, 1450742400, 1608508800, 14, 'пр. Ленина  8а  ост. "Театр ЧТЗ" (трамвай  в центр)',
        61.451943, 55.160648);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1571, 283, 1471305600, 1450742400, 1608508800, 14, 'ул. Черкасская  15/2  ост. "Авторынок" (трамвай  в центр)',
        61.377482, 55.231411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1572, 284, 1471305600, 1450742400, 1608508800, 14,
        'ш. Металлургов  70/1  ост. "ДК Строителей" (троллейбус  в сторону ЧМЗ)', 61.378748, 55.247231);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1573, 285, 1471305600, 1450742400, 1608508800, 14,
        'пр. Победы  346  ост. "ул. Ворошилова" (трамвай  в сторону ул. Чичерина)', 61.323331, 55.189491);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1574, 286, 1471305600, 1450742400, 1608508800, 14,
        'ул. Б. Хмельницкого  35  ост "ул. Б. Хмельницкого (троллейбус  в сторну ул. Сталеваров)', 61.380347,
        55.258089);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1575, 287, 1471305600, 1450742400, 1608508800, 14,
        'ул. Марченко  18  сот. "Магазин № 28" (автобус  в сторону пр. Комарова)', 61.461241, 55.170691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1576, 288, 1471305600, 1450742400, 1608508800, 14,
        'ул. Гагарина  15  ост. "Дом одежды" (троллейбус  в сторону Копейского ш.)', 61.441657, 55.139571);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1577, 289, 1471305600, 1450742400, 1608508800, 14,
        'ул. Новороссийская  86  ост. "ДК ЧТПЗ" (троллейбус  в сторону ул. Гагарина)', 61.457117, 55.114157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1578, 290, 1471305600, 1450742400, 1608508800, 14,
        'пр. Ленина  13а  ост. "Комсомольская пл." (троллейбус  в сторону ЧТЗ)', 61.441019, 55.159388);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1579, 291, 1471305600, 1450742400, 1608508800, 14,
        'ул. Машиностроителей  37  ост. "Школа" (маршрутное такси  в сторону Копейского ш.)', 61.467798, 55.111887);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1580, 292, 1471305600, 1450742400, 1608508800, 14,
        'ул. Сталеваров  66  ост. "ул. Сталеваров" (маршрутное такси  в центр)', 61.40276, 55.251778);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1581, 293, 1471305600, 1450742400, 1608508800, 14,
        'ул. Павелецкая  2-я  напротив 18/1  ост. "ЧМК" (троллейбус  в центр)', 61.411348, 55.26423);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1582, 294, 1471305600, 1450742400, 1608508800, 14, 'пр. Ленина напротив д. 3/1  ост. "ЧТЗ" (трамвай  конечная)',
        61.465714, 55.158796);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1583, 306, 1473638400, 1450742400, 1608508800, 14,
        'ул. Свободы  84  ост. "ул. Плеханова" (троллейбус  в сторону ж/д вокзала)', 61.413253, 55.155335);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1584, 307, 1473638400, 1450742400, 1608508800, 14,
        'пр. Ленина  19  ост. "ТРК Горки"  (троллейбус  в сторону ЧТЗ)', 61.434255, 55.160597);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1585, 308, 1473638400, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  134  ост. "Универсам Казачий" (маршрутное такси  в сторону плотины)', 61.298097,
        55.17174);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1586, 309, 1473638400, 1450742400, 1608508800, 14, 'пр. Победы  293  ост. "ул. Ворошилова" (трамвай  в центр)',
        61.32414, 55.188411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1587, 310, 1473638400, 1450742400, 1608508800, 14,
        'ул. Кирова  9  ост. "ул. Калинина" (трамвай  в сторону Теплотехн. Института"', 61.373125, 55.040647);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1588, 311, 1473638400, 1450742400, 1608508800, 14,
        'ул. Молодогвардейцев  57к1  ост. "ул. Братьев Кашириных" (троллейбус  в сторону пр. Победы)', 61.333518,
        55.179483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1589, 312, 1473638400, 1450742400, 1608508800, 14,
        'ул. Труда  напротив д. 189  ост. "Зоопарк" (троллейбус  в сторону С-З)', 61.369945, 55.170074);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1590, 313, 1473638400, 1450742400, 1608508800, 14,
        'ул. Дарвина  63  ост. "Кинотеатр Маяк" (автобус  в сторону Троицкого тр.)', 61.37714, 55.121124);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1591, 314, 1473638400, 1450742400, 1608508800, 14,
        'пр. Победы  140  ост. "Юридический институт" (трамвай  в сторону Теплотехн. Института)', 61.424733, 55.185944);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1592, 315, 1473638400, 1450742400, 1608508800, 14,
        'ул. Горького  67  ост. "ул. Лермонтова" (трамвай  в сторону ЧТЗ)', 61.434147, 55.181837);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1593, 316, 1473638400, 1450742400, 1608508800, 14,
        'Троицкий тр.  52а  ост. "Рынок Привоз" (автобус  в сторону ул. Блюхера)', 61.388306, 55.11113);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1594, 317, 1473638400, 1450742400, 1608508800, 14,
        'ул. Лесопарковая  9а  ост. "Профилакторий" (маршрутное такси  в центр)', 61.365363, 55.153118);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1595, 318, 1473638400, 1450742400, 1608508800, 14, 'ул. Машиностроителей  2  ост. "Мех. Завод"', 61.477868,
        55.127399);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1596, 319, 1473638400, 1450742400, 1608508800, 14, 'ул. Горького  59  ост. "ДК Смена" (трамвай  в сторону ЧТЗ)',
        61.435845, 55.179395);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1597, 320, 1473638400, 1450742400, 1608508800, 14,
        'пр. Ленина  54  ост. "пл. Революции" (троллейбус  в сторону ПКиО)', 61.402419, 55.160818);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1598, 321, 1473638400, 1450742400, 1608508800, 14,
        'ул. 50 лет ВЛКСМ  35  ост. "п. Першино" (трамвай  в сторону ЧМЗ)', 61.380958, 55.237587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1599, 322, 1473638400, 1450742400, 1608508800, 14,
        'ул. Черкасская  4  ост. "ДЦ Импульс" (маршрутное такси  в центр)', 61.378578, 55.241421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1600, 323, 1473638400, 1450742400, 1608508800, 14,
        'ш. Металлургов  41  ост. "ДК Строителей" (маршрутное такси  в центр)', 61.376431, 55.245522);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1601, 324, 1473638400, 1450742400, 1608508800, 14,
        'ул. Машиностроителей  21а/1  ост. "Трубопрокатный завод" (троллейбус  в сторону Копейского ш.)', 61.471948,
        55.118421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1602, 325, 1473638400, 1450742400, 1608508800, 14,
        'ул. Б. Хмельницкого  14а  ост. "Кинотеатр Россия" (троллейбус  в центр)', 61.390489, 55.259418);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1603, 326, 1473638400, 1450742400, 1608508800, 14, 'ул. Сталеваров  32  ост. "Сквер Победы" (автобус  в центр)',
        61.402428, 55.260608);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1604, 327, 1473638400, 1450742400, 1608508800, 14, 'пр. Ленина  64д  ост. "Алое поле" (трамвай  в сторону АМЗ)',
        61.383842, 55.160689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1605, 328, 1473638400, 1450742400, 1608508800, 14,
        'Свердловский пр.  28а  ост. "пр. Победы" (троллейбус  в центр)', 61.385953, 55.183225);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1606, 329, 1473638400, 1450742400, 1608508800, 14,
        'ул. Дзержинского  4  ост. "ЗЭМ" (трамвай в сторону ул. Гагарина)', 61.471095, 55.121165);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1607, 330, 1473638400, 1450742400, 1608508800, 14,
        'ул. Марченко  напротив д. 15  ост. "Магазин № 28" (автобус  в центр)', 61.462193, 55.169735);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1608, 331, 1473638400, 1450742400, 1608508800, 14,
        'ул. Новороссийская  64  ост. "Школа" (троллейбус  в сторону ул. Гагарина)', 61.466361, 55.112123);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1609, 332, 1473638400, 1450742400, 1608508800, 14,
        'ул. Сталеваров  7  ост. "Сквер Победы" (автобус  в сторону ЧМК)', 61.403838, 55.26186);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1610, 333, 1473638400, 1450742400, 1608508800, 14,
        'ул. Гагарина  3  ост. "Политехникум" (троллейбус  в сторону Копейского ш.)', 61.445143, 55.144222);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1611, 334, 1473638400, 1450742400, 1608508800, 14, 'ул. Цвиллинга  43  ост. "Городской сад" (трамвай  в центр)',
        61.407755, 55.154645);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1612, 335, 1473984000, 1450742400, 1608508800, 14,
        'пр. Ленина  89  ост. "ПКиО им. Гагарина" (троллейбус  конечная)', 61.363477, 55.158174);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1613, 336, 1473984000, 1450742400, 1608508800, 14,
        'пр. Победы/ул. Российская  32  ост. "ул. Российская" (трамвай  в сторону ЧТЗ)', 61.41354, 55.184793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1614, 337, 1473984000, 1450742400, 1608508800, 14, 'ул. Кирова  74  ост. "Цирк" (автобус  в центр)', 61.398951,
        55.17226);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1615, 338, 1473984000, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных/ул. Чайковского  183  ост. "ул. Чайковского" (троллейбус  в центр)', 61.354727,
        55.177828);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1616, 339, 1473984000, 1450742400, 1608508800, 14, 'ул. Труда  203  ост. "ТРК Родник" (троллейбус  в центр)',
        61.355976, 55.171051);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1617, 340, 1473984000, 1450742400, 1608508800, 14,
        'Троицкий тр.  напротив д. 35  ост. "Сад Локомотив" (автобус  в сторону п. Смолино)', 61.386959, 55.098042);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1618, 341, 1473984000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  41  ост. "Поликлиника" (маршрутное такси  в центр)', 61.338432, 55.194147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1619, 342, 1473984000, 1450742400, 1608508800, 14,
        'ул. Героев Танкограда  50  ост. "ул. Потемкина" (троллейбус  в центр)', 61.443607, 55.183986);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1620, 343, 1473984000, 1450742400, 1608508800, 14,
        'ул. Горького  напротив д. 18  ост. "Монтажный колледж" (трамвай  в сторону ЧТЗ)', 61.445053, 55.16882);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1621, 344, 1473984000, 1450742400, 1608508800, 14, 'пр. Победы  142  ост. "Юридический институт"', 61.423592,
        55.185903);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1622, 345, 1473984000, 1450742400, 1608508800, 14,
        'ул. Мамина  15  ост. "Универсам" (автобус  в сторону ул. Бажова)', 61.4813, 55.194362);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1623, 346, 1473984000, 1450742400, 1608508800, 14,
        'ул. Салютная  10  ост. "Сад Победы" (троллейбус  в сторону ЖБИ)', 61.452949, 55.172697);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1624, 347, 1473984000, 1450742400, 1608508800, 14,
        'ул. Блюхера  напротив д. 67  ост. "ТК Кольцо" (троллейбус  в сторону АМЗ)', 61.36672, 55.129026);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1625, 348, 1473984000, 1450742400, 1608508800, 14,
        'пр. Ленина  62  ост. "Публичная библиотека" (маршрутное такси  в сторону ПКиО)', 61.394693, 55.160483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1626, 349, 1473984000, 1450742400, 1608508800, 14,
        'ул. Цвиллинга  77  ост. "стадион Локомотив" (трамвай  в центр)', 61.413235, 55.147068);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1627, 350, 1473984000, 1450742400, 1608508800, 14,
        'ул. Молдавская  11  ост. "ул. Молдавская" (троллейбус  в центр)', 61.303335, 55.193294);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1628, 351, 1473984000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  напротив д. 111  ост. "18-й микрорайон" (автобус  в сторону пр. Победы)', 61.287668,
        55.186921);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1629, 352, 1473984000, 1450742400, 1608508800, 14,
        'пр. Победы  напротив д. 378  ост. "Поликлиника" (трамвай  в центр)', 61.302302, 55.188242);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1630, 353, 1473984000, 1450742400, 1608508800, 14,
        'ул. С. Разина  9а  ост. "Завод им. Колющенко" (трамвай № 1  в центр)', 61.410926, 55.137986);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1631, 354, 1473984000, 1450742400, 1608508800, 14,
        'ул. Молодогвардейцев  32  ост. "ул. Молодогвардейцев" (маршрутное такси  в сторону ул. Чичерина)', 61.331919,
        55.195077);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1632, 355, 1473984000, 1450742400, 1608508800, 14,
        'ул. Черкасская  10  ост. "п. Першино" (троллейбус  в центр)', 61.377958, 55.237838);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1633, 356, 1473984000, 1450742400, 1608508800, 14,
        'ул. 50 лет ВЛКСМ  35  ост. "п. Першино" (троллейбус  в сторону ЧМЗ)', 61.380958, 55.237587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1634, 357, 1473984000, 1450742400, 1608508800, 14,
        'ул. Машиностроителей  34  ост. "Трубопрокатный завод" (трамвай  в сторону ул. Новороссийской)', 61.46955,
        55.11717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1635, 358, 1473984000, 1450742400, 1608508800, 14,
        'пр. Ленина  напротив д. 3  ост. "ЧТЗ" (троллейбус  в центр)', 61.446975, 55.151487);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1636, 359, 1473984000, 1450742400, 1608508800, 14,
        'ул. Новороссийская  напротив д. 84  ост. "ДК ЧТПЗ" (троллейбус  в сторону ул. Машиностроителей)', 61.457827,
        55.1139);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1637, 360, 1473984000, 1450742400, 1608508800, 14,
        'ул. Горького  1б  ост. "Комсомольская пл." (трамвай  в сторону ЧТЗ)', 61.441136, 55.161707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1638, 361, 1473984000, 1450742400, 1608508800, 14,
        'ул. Гагарина  16а  ост. "Дом одежды" (троллейбус  в сторону ул. Новороссийской)', 61.439663, 55.138989);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1639, 362, 1473984000, 1450742400, 1608508800, 14,
        'пр. Победы  напротив д. 372  ост. "ул. Молдавская" (трамвай  в центр)', 61.308787, 55.189383);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1640, 363, 1473984000, 1450742400, 1608508800, 14,
        'ул. Цвиллинга  55а  ост. "ул. Орджоникидзе" (маршрутное такси  в центр)', 61.41018, 55.152207);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1641, 382, 1475625600, 1471996800, 1629676800, 12, 'ул.Труда 203', 61.355976, 55.171051);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1642, 383, 1475712000, 1450742400, 1608508800, 14,
        'ул. Сталеваров  37/1  ост. "ул. Сталеваров" (трамвай  в центр)', 61.403766, 55.251516);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1643, 384, 1475712000, 1450742400, 1608508800, 14,
        'пр. Ленина  54  ост. "пл. Революции" (автобус  в сторону ПКиО)', 61.402419, 55.160818);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1644, 385, 1475712000, 1450742400, 1608508800, 14,
        'ул. Худякова  12  ост. "ТК Калибр" (автобус  в сторону плотины)', 61.371445, 55.148205);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1645, 386, 1475712000, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  83  ост. "ул. Косарева" (автобус  в сторону Свердловского пр.)', 61.370376, 55.1772);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1646, 387, 1475712000, 1450742400, 1608508800, 14, 'ул. Кирова  60а  ост. "Цирк" (трамвай  в центр)', 61.399149,
        55.174182);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1647, 388, 1475712000, 1450742400, 1608508800, 14, 'ул. Труда  30  ост. "Областной суд" (трамвай  в центр)',
        61.421769, 55.16867);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1648, 389, 1475712000, 1450742400, 1608508800, 14,
        'Троицкий тр.  напротив д. 35  ост. "Сад Локомотив" (автобус  в сторону ул. Блюхера)', 61.386959, 55.098042);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1649, 390, 1475712000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  2/1  ост. "Комсомольский пр." (маршрутное такси  в сторону ЧМЗ)', 61.383994, 55.193597);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1650, 391, 1475712000, 1450742400, 1608508800, 14,
        'ул. Черкасская  15/2  ост. "Авторынок" (автобус  в сторону ЧМЗ)', 61.377482, 55.231411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1651, 392, 1475712000, 1450742400, 1608508800, 14,
        'ул. Дарвина  12  ост. "Кондитерская фабрика" (автобус  в сторну ул. Блюхера)', 61.37485, 55.123703);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1652, 393, 1475712000, 1450742400, 1608508800, 14,
        'пр. Комарова  133  ост. "ДК Ровесник" (автобус  в сторону Аэропорта)', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1653, 394, 1475712000, 1450742400, 1608508800, 14,
        'ул. 1-й Пятилетки  напротив д. 10/1  ост. "ул. 1-й Пятилетки" (троллейбус  в сторону ЖБИ)', 61.451682,
        55.167457);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1654, 395, 1475712000, 1450742400, 1608508800, 14,
        'ул. Героев Танкограда  100  ост. "ул. Котина" (троллейбус  в центр)', 61.447793, 55.177355);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1655, 396, 1475712000, 1450742400, 1608508800, 14,
        'ул. Горького  14  ост. "Монтажный колледж" (трамвай  в сторону С-З)', 61.443373, 55.166866);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1656, 397, 1475712000, 1450742400, 1608508800, 14,
        'ул. Мамина  напротив д. 13  ост. "Универсам" (автобус  в центр)', 61.480464, 55.194696);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1657, 398, 1475712000, 1450742400, 1608508800, 14,
        'ул. Октябрьская  напротив д. 11  ост. "ул. Октябрьская" (автобус  в центр)', 61.372173, 55.039264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1658, 399, 1475712000, 1450742400, 1608508800, 14, 'ул. Российская  71/1  ост. "пл. Павших Революционеров"',
        61.415822, 55.169673);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1659, 400, 1475712000, 1450742400, 1608508800, 14, 'ул. Салютная  11  ост. "Сад Победы" (троллейбус  в центр)',
        61.451503, 55.171319);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1660, 401, 1475712000, 1450742400, 1608508800, 14,
        'Свердловский пр.  46  ост. "ДС Юность" (маршрутное такси  в центр)', 61.387749, 55.166326);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1661, 402, 1475712000, 1450742400, 1608508800, 14,
        'ул. Доватора  42  ост. "Гостиница Центральная" (автобус  в сторону ул. Воровского)', 61.385351, 55.145426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1662, 403, 1475712000, 1450742400, 1608508800, 14,
        'ул. Курчатова  напротив д. 19а  ост. "ул. Курчатова" (трамвай  в центр)', 61.389061, 55.148781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1663, 404, 1475712000, 1450742400, 1608508800, 14,
        'ул. Блюхера  44  ост. "ул. Рылеева" (троллейбус  в сторону АМЗ)', 61.368741, 55.132598);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1664, 405, 1475712000, 1450742400, 1608508800, 14,
        'пр. Ленина  59  ост. "Публичная библиотека"  (маршрутное такси  в центр)', 61.396517, 55.159815);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1665, 406, 1475712000, 1450742400, 1608508800, 14,
        'ул. Черкасская  17  ост. "ул. Черкасская" (трамвай  в сторону ЧМЗ)', 61.375532, 55.229085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1666, 407, 1475712000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  29  ост. "ул. Чайковского" (маршрутное такси  в центр)', 61.357126, 55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1667, 408, 1475712000, 1450742400, 1608508800, 14,
        'пр. Ленина  76  ост. "ЮУрГУ" (маршрутное такси  в сторону ПКиО)', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1668, 409, 1475712000, 1450742400, 1608508800, 14,
        'ул. Цвиллинга  напротив д. 41а  ост. "Городской сад" (трамвай  в сторону ж/д вокзала)', 61.407306, 55.155736);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1669, 410, 1475712000, 1450742400, 1608508800, 14,
        'Свердловский пр.  51  ост. "ДС Юность" (маршрутное такси  в сторону С-З)', 61.391244, 55.169015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1670, 411, 1475712000, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  напротив д. 138  ост. "Универсам Казачий" (маршрутное такси  в сторону С-З)', 61.296004,
        55.170763);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1671, 417, 1477440000, 1450742400, 1608508800, 14,
        'пр. Ленина  29  ост. "Центральный рынок" (троллейбус  в сторону ЧТЗ)', 61.420583, 55.160545);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1672, 418, 1477440000, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  напротив д. 124  ост. "25-й микрорайон" (автобус  в центр)', 61.304511, 55.174296);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1673, 419, 1477440000, 1450742400, 1608508800, 14, 'ул. Кирова  44  ост. "ул. Калинина" (автобус  в центр)',
        61.398682, 55.178352);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1674, 420, 1477440000, 1450742400, 1608508800, 14,
        'пр. Победы  115  ост. "м-н Юрюзань" (автобус  в сторону ЧТЗ)', 61.433806, 55.185764);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1675, 421, 1477440000, 1450742400, 1608508800, 14,
        'ул. Труда  21  ост. "пл. Павших Революционеров" (трамвай  в сторону пр. Победы)', 61.416972, 55.167596);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1676, 422, 1477440000, 1450742400, 1608508800, 14,
        'ул. Гагарина  47  ост. "Строительное училище" (троллейбус  в сторону КБС)', 61.435549, 55.121258);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1677, 423, 1477440000, 1450742400, 1608508800, 14,
        'Троицкий тр.  напротив д. 21  ост. "Сельхозтехника" (автобус  в сторону п. Смолино)', 61.387085, 55.09338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1678, 424, 1477440000, 1450742400, 1608508800, 14,
        'пр. Победы  177  ост. "пр. Победы" (маршрутное такси  в сторону ул. Кирова)', 61.389232, 55.183636);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1679, 425, 1477440000, 1450742400, 1608508800, 14,
        'ул. С. Разина  9  ост. "Ж/д вокзал" (маршрутное такси  в центр)', 61.413639, 55.140091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1680, 426, 1477440000, 1450742400, 1608508800, 14,
        'ул. Гагарина  25  ост. "Дом обуви" (троллейбус  в сторону КБС)', 61.439295, 55.135433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1681, 427, 1477440000, 1450742400, 1608508800, 14, 'ш. Металлургов  28  ост. "ул. Жукова" (трамвай  в центр)',
        61.391927, 55.249715);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1682, 428, 1477440000, 1450742400, 1608508800, 14,
        'ул. 1-й Пятилетки  30  ост. "Трамвайное депо № 1" (трамвай  в центр)', 61.42909, 55.168753);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1683, 429, 1477440000, 1450742400, 1608508800, 14,
        'ул. Героев Танкограда/ул. Бажова  117  ост. "ул. Бажова" (троллейбус  в центр)', 61.445619, 55.18085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1684, 430, 1477440000, 1450742400, 1608508800, 14,
        'ул. Бажова  38а  ост. "М-н Радуга" (троллейбус  в сторону ул. Г. Танкограда)', 61.45692, 55.183852);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1685, 431, 1477440000, 1450742400, 1608508800, 14,
        'Свердловский пр.  6  ост. "Комсомольский пр." (маршрутное такси  в центр)', 61.385944, 55.19132);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1686, 432, 1477440000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  2/2  ост. "Комсомольский пр." (маршрутное такси  в сторону С-З)', 61.385234, 55.192333);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1687, 433, 1477440000, 1450742400, 1608508800, 14,
        'ул. Островского  2  ост "ул. Островского" (трамвай  в центр)', 61.391432, 55.188416);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1688, 434, 1477440000, 1450742400, 1608508800, 14,
        'пр. Ленина  51  ост. "пл. Революции" (маршрутное такси  в сторону ЧТЗ)', 61.406641, 55.160128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1689, 435, 1477440000, 1450742400, 1608508800, 14,
        'Свердловский пр.  72  ост. "Алое поле" (маршрутное такси  в сторону АМЗ)', 61.388513, 55.159151);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1690, 436, 1477440000, 1450742400, 1608508800, 14,
        'ул. Сони Кривой  83  ост. "ПО Полет" (маршрутное такси  в центр)', 61.3684, 55.156976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1691, 437, 1477440000, 1450742400, 1608508800, 14,
        'ул. Доватора  35  ост. "ул. Доватора" (маршрутное такси  в сторону ж/д вокзала)', 61.382458, 55.145915);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1692, 438, 1477440000, 1450742400, 1608508800, 14,
        'ул. Худякова  19  ост. "ТК Калибр" (маршрутное такси  в сторону вокзала)', 61.372712, 55.147165);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1693, 439, 1477440000, 1450742400, 1608508800, 14,
        'ул. Блюхера  51  ост. "ул. Рылеева" (маршрутное такси  в центр)', 61.37105, 55.132937);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1694, 440, 1477440000, 1450742400, 1608508800, 14,
        'ул. Черкасская  17  ост. "ул. Черкасская" (маршрутное такси  в сторону ЧМЗ)', 61.375532, 55.229085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1695, 441, 1477440000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  29  ост. "ул. Красного Урала" (маршрутное такси  в центр)', 61.357126, 55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1696, 442, 1477440000, 1450742400, 1608508800, 14,
        'ул. Цвиллинга  напротив д. 5  ост. "Оперный театр"(трамвай  в центр)', 61.403946, 55.167226);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1697, 443, 1477440000, 1450742400, 1608508800, 14,
        'ш. Металлургов  70/1  ост. "ДК Строителей" (троллейбус  в центр)', 61.378748, 55.247231);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1698, 444, 1477440000, 1450742400, 1608508800, 14,
        'ул. Труда  напротив д. 161  ост. "ДС Юность" (трамвай  в сторону С-З)', 61.38783, 55.167087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1699, 445, 1477440000, 1450742400, 1608508800, 14,
        'Свердловский пр.  напротив д. 51  ост. "ДС Юность" (трамвай  в сторону АМЗ)', 61.391244, 55.169015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1700, 448, 1478736000, 1477958400, 1640995200, 17, 'шоссе Металлургов 41', 61.376431, 55.245522);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1701, 450, 1479168000, 1450742400, 1608508800, 14,
        'пр. Ленина  34  ост. "Центральный рынок" (троллейбус  в сторону ПКиО)', 61.420421, 55.161219);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1702, 451, 1479168000, 1450742400, 1608508800, 14,
        'ул. 40 лет Победы  21  ост. "Универсам" (автобус  из центра)', 61.298017, 55.187424);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1703, 452, 1479168000, 1450742400, 1608508800, 14,
        'ул. Чичерина/ул. Братьев Кашириных  124  ост. "25-й микрорайон" (автобус)', 61.304511, 55.174296);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1704, 453, 1479168000, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  95  ост. "ул. Северо-Крымская" (автобус  в сторону Свердловского пр.)', 61.361177,
        55.178157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1705, 454, 1479168000, 1450742400, 1608508800, 14,
        'ул. Кирова  2а  ост. "Теплотехнический институт" (автобус  в центр)', 61.372388, 55.03977);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1706, 455, 1479168000, 1450742400, 1608508800, 14,
        'пр. Победы/ул. Российская  41  ост. "ул. Российская" (трамвай  в сторону Теплотехн. Института)', 61.414304,
        55.185841);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1707, 456, 1479168000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  36  ост. "Поликлиника" (троллейбус  в сторону ул. Молодогвардейцев)', 61.340947, 55.195005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1708, 457, 1479168000, 1450742400, 1608508800, 14,
        'ул. Гагарина  50  ост. "Библиотека им. Мамина-Сибиряка" (троллейбус  в сторону ул. Новороссийской)', 61.433626,
        55.123708);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1709, 458, 1479168000, 1450742400, 1608508800, 14, 'ул. Блюхера  101  ост. "п. АМЗ" (троллейбус  в центр)',
        61.358976, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1710, 459, 1479168000, 1450742400, 1608508800, 14,
        'Троицкий тр.  напротив д. 21  ост. "Сельхозтехника" (автобус  в сторону ул. Блюхера)', 61.387085, 55.09338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1711, 460, 1479168000, 1450742400, 1608508800, 14,
        'ул. Профессора Благих  напротив д. 79  ост. "47-й микрорайон" ', 61.298681, 55.218281);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1712, 461, 1479168000, 1450742400, 1608508800, 14,
        'ул. Бейвеля  54а  ост. "55-й микрорайон" (автобус  в сторону Краснопольского пр.)', 61.286357, 55.203473);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1713, 462, 1479168000, 1450742400, 1608508800, 14, 'пр. Победы  221  ост. "ул. Косарева" (трамвай  в центр)',
        61.372505, 55.183436);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1714, 463, 1479168000, 1450742400, 1608508800, 14, 'ул. С. Разина  9  ост. "Ж/д вокзал" (троллейбус  в центр)',
        61.413639, 55.140091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1715, 464, 1479168000, 1450742400, 1608508800, 14,
        'ул. Доватора  28  ост. "ДК Колющенко" (автобус  в сторону ул. Воровского)', 61.393256, 55.141917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1716, 465, 1479168000, 1450742400, 1608508800, 14, 'ул. Хохрякова  22  ост. "ул. Чоппа" (автобус  в центр)',
        61.487238, 55.176573);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1717, 466, 1479168000, 1450742400, 1608508800, 14,
        'ул. Гагарина  24а  ост. "Дом обуви" (троллейбус  в сторону ул. Новороссийская)', 61.437453, 55.134533);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1718, 467, 1479168000, 1450742400, 1608508800, 14,
        'ш. Металлургов  25б  ост. "Юридический техникум" (маршрутное такси  в центр)', 61.389582, 55.247585);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1719, 468, 1479168000, 1450742400, 1608508800, 14,
        'ул. Доватора  11  ост. "м-н "Губернский" (автобус  в сторону Ленинского района)', 61.397038, 55.139339);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1720, 469, 1479168000, 1450742400, 1608508800, 14,
        'ул. Дегтярева  48  сот. "Кафе Сказка" (автобус  в сторону ул. Румянцева)', 61.396355, 55.264876);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1721, 470, 1479168000, 1450742400, 1608508800, 14,
        'ул. Дарвина  91  ост. "Кондитерская фабрика" (автобус  в сторону Троицкого тр.)', 61.372478, 55.12376);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1722, 471, 1479168000, 1450742400, 1608508800, 14, 'ул. Бейвеля  27  ост. "55-й микрорайон" (автобус  в центр)',
        61.283212, 55.203848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1723, 472, 1479168000, 1450742400, 1608508800, 14,
        'ул. Академика Сахарова  напротив д. 11  ост. "ул. Ак. Сахарова" "автобус  в сторону плотины)', 61.297981,
        55.159059);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1724, 473, 1479168000, 1450742400, 1608508800, 14,
        'Комсомольский пр.  22  ост. "ул. Чайковского" (троллейбус  в сторону ул. Молодогвардейцев)', 61.359955,
        55.193201);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1725, 474, 1479168000, 1450742400, 1608508800, 14,
        'ул. Доватора  напротив д. 26  ост. "ДК Колющенко" (автобус  в сторону ж/д вокзала)', 61.394424, 55.141485);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1726, 475, 1479168000, 1450742400, 1608508800, 14,
        'ул. Блюхера  93  ост. "Энергетический колледж" (троллейбус  в центр)', 61.363926, 55.120387);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1727, 476, 1479168000, 1450742400, 1608508800, 14, 'пр. Комарова  110  ост. "м-н Шатура" (автобус  в центр)',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1728, 477, 1479168000, 1450742400, 1608508800, 14, 'ул. Черкасская  26/1  ост. "Авторынок" (автобус  в центр)',
        61.374445, 55.232151);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1729, 478, 1479168000, 1450742400, 1608508800, 14,
        'ул. Машиностроителей  36  ост. "Трубопрокатный завод" (троллейбус  в сторону ул. Новороссийская)', 61.469083,
        55.116377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1730, 480, 1479427200, 1450742400, 1608508800, 14,
        'пр. Победы  напротив д. 292  ост. "ул. Красного Урала" (трамвай  в центр)', 61.353299, 55.189845);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1731, 481, 1479427200, 1450742400, 1608508800, 14,
        'ул. Братьев Кашироиных  107  ост. "ул. Братьев Кашириных" (троллейбус  в центр)', 61.334362, 55.178002);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1732, 482, 1479427200, 1450742400, 1608508800, 14, 'ул. Кирова  25а  ост. "Цирк" (трамвай  из центра)',
        61.400514, 55.173005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1733, 483, 1479427200, 1450742400, 1608508800, 14,
        'ул. Горького/пр. Победы  117  ост. "ул. 5-го Декабря" (трамвай  в сторону ЧТЗ', 61.431821, 55.185374);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1734, 484, 1479427200, 1450742400, 1608508800, 14,
        'Комсомольский пр.  32  ост. "Кинотеатр Победа" (троллейбус  в сторону ул. Молодогвардейцев)', 61.346589,
        55.195056);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1735, 485, 1479427200, 1450742400, 1608508800, 14,
        'ул. Гагарина  64  ост. "М-н Спорттовары" (троллейбус  в торону ул. Новороссийской', 61.399517, 55.079844);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1736, 486, 1479427200, 1450742400, 1608508800, 14,
        'ул. Блюхера  напротив д. 81  ост. "п. Мебельный" (троллейбус  в сторону АМЗ)', 61.364735, 55.123924);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1737, 487, 1479427200, 1450742400, 1608508800, 14,
        'Троицкий тр.  11  ост. "Рынок Привоз" (автобус  в сторону п. Смолино)', 61.386249, 55.109755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1738, 488, 1479427200, 1450742400, 1608508800, 14,
        'ул. Мамина  напротив д. 23  ост. "Рынок Северо-Восточный" (автобус  в центр)', 61.48386, 55.18927);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1739, 489, 1479427200, 1450742400, 1608508800, 14,
        'ул. Артиллерийская  напротив д. 107  ост. "Киргородок" (трамвай  в центр)', 61.435351, 55.167591);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1740, 490, 1479427200, 1450742400, 1608508800, 14, 'ул. Хохрякова  12  сот. "1-я Охотничья" (автобус  в центр)',
        61.486259, 55.181601);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1741, 491, 1479427200, 1450742400, 1608508800, 14,
        'ул. Профессора Благих  79  ост. "47-й микрорайон" (автобус  в сторону ул. Молодогвардейцев)', 61.298681,
        55.218281);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1742, 492, 1479427200, 1450742400, 1608508800, 14, 'пр. Победы  249  ост. "ул. Тепличная" (трамвай  в центр)',
        61.367142, 55.185163);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1743, 493, 1479427200, 1450742400, 1608508800, 14, 'ул. Самохина  162  ост. "Школа" (автобус  в центр)',
        61.531938, 55.164711);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1744, 494, 1479427200, 1450742400, 1608508800, 14, 'ул. Мамина  27  ост. "ул. Восходящая" (автобус  в центр)',
        61.482602, 55.173457);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1745, 495, 1479427200, 1450742400, 1608508800, 14,
        'ул. Артиллерийская  107  ост. "Киргородок" (трамвай  в сторону ЧТЗ)', 61.435351, 55.167591);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1746, 496, 1479427200, 1450742400, 1608508800, 14, 'пр. Ленина  3  ост. "ЧТЗ" (автобус  в сторону п. Чурилово)',
        61.446975, 55.151487);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1747, 497, 1479427200, 1450742400, 1608508800, 14,
        'ул. Чичерина  5  ост. "18-й микрорайон" (автобус  в сторону Краснопольского пр.)', 61.284874, 55.188658);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1748, 498, 1479427200, 1450742400, 1608508800, 14,
        'ул. С. Разина  4  ост. "Ж/д вокзал" (маршрутное такси  в сторону Ленинского района)', 61.411725, 55.141428);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1749, 499, 1479427200, 1450742400, 1608508800, 14,
        'ул. Зальцмана  10  ост. "ул. Зальцмана" (автобус  в сторону ул. 1-я Эльтонская)', 61.531624, 55.170162);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1750, 500, 1479427200, 1450742400, 1608508800, 14, 'ул. Братьев Кашириных  100  ост. "ЧелГУ"', 61.320187,
        55.179791);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1751, 501, 1479427200, 1450742400, 1608508800, 14,
        'пр. Победы/ ул. Российская  30  ост. "ул. Российская" (автобус  в сторону Теплотехн. Института)', 61.413522,
        55.185769);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1752, 502, 1479427200, 1450742400, 1608508800, 14,
        'ул. Марченко  25  ост. "Швейная фабрика" (автобус  в сторону ул. Салютная)', 61.459857, 55.166228);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1753, 503, 1479427200, 1450742400, 1608508800, 14,
        'ул. Черкасская  26/12  ост. "ул. Черкасская" (троллейбус  в центр)', 61.37485, 55.233347);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1754, 504, 1479427200, 1450742400, 1608508800, 14,
        'ул. Косарева  56  ост. "ул. Косарева" (автобус  в сторону С-З)', 61.371894, 55.178804);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1755, 505, 1479427200, 1450742400, 1608508800, 14,
        'пр. Комарова  129  ост. "М-н Шатура" (автобус  в сторону Аэропорта)', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1756, 506, 1479427200, 1450742400, 1608508800, 14,
        'ул. 1-й Пятилетки  59  ост. "Трамвайное депо № 1" (трамвай  в сторону ЧТЗ)', 61.429467, 55.167498);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1757, 507, 1479427200, 1450742400, 1608508800, 14,
        'ул. Героев Танкограда/ул. 40 лет Октября  15  ост. "ул. 1-й Пятилетки" (троллейбус  в центр)', 61.451081,
        55.166372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1758, 509, 1479686400, 1479168000, 1636848000, 8, 'Привокзальная площадь  ТК "Синегорье"  центр. вход  поз. 2',
        61.414456, 55.140507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1759, 511, 1479686400, 1479168000, 1636848000, 8, 'ул. Цвиллинга  авт.ост."пл.Революции" (из центра)',
        61.402473, 55.159866);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1760, 512, 1479686400, 1479168000, 1636848000, 8, 'ул. Цвиллинга  авт.ост."Оперный театр" (в центр)', 61.407611,
        55.154481);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1761, 519, 1479772800, 1479168000, 1636848000, 6, 'ул. Энтузиастов  26', 61.375937, 55.151358);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1762, 528, 1480032000, 1479168000, 1636848000, 15, 'ул. Игуменка  183', 61.389932, 55.081926);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1763, 529, 1480291200, 1479168000, 1636848000, 12, 'ул.Бр.Кашириных 72', 61.347146, 55.179529);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1764, 531, 1480464000, 1479168000, 1636848000, 9, 'ул. Сталеваров  15', 61.403416, 55.257596);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1765, 532, 1480464000, 1479168000, 1636848000, 9, 'ул. Я. Гашека  1', 61.405761, 55.258273);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1766, 535, 1480464000, 1479168000, 1636848000, 6, 'ул. Горького  3', 61.441001, 55.162458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1767, 536, 1480464000, 1479168000, 1636848000, 12, 'пр. Ленина  26а/2', 61.429683, 55.161769);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1768, 538, 1480550400, 1479168000, 1636848000, 6, 'пл. Мопра  10/1', 61.417007, 55.163698);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1769, 539, 1480636800, 1479168000, 1636848000, 12, 'ул. Линейная  98  поз. 1', 61.488163, 55.159676);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1770, 540, 1480636800, 1479168000, 1636848000, 12, 'ул. Линейная  98  поз. 2', 61.488163, 55.159676);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1771, 541, 1480636800, 1479168000, 1636848000, 15, 'пр. Ленина  26А  поз. 1', 61.431345, 55.161908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1772, 542, 1480636800, 1479168000, 1636848000, 15, 'пр. Ленина  26А  поз. 2', 61.431345, 55.161908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1773, 543, 1480636800, 1479168000, 1636848000, 15, 'пр. Ленина  26А  поз. 3', 61.431345, 55.161908);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1774, 545, 1480896000, 1479168000, 1636848000, 6, 'ул. Овчинникова  6', 61.409138, 55.143358);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1775, 546, 1480896000, 1479168000, 1636848000, 12, 'пересечение Троицкого тр. и ул. Томинская', 61.389869,
        55.122787);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1776, 548, 1480982400, 1476230400, 1633910400, 15, 'ул. Игуменка  181', 61.390606, 55.082503);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1777, 549, 1480982400, 1479168000, 1636848000, 6, 'пересечение Троицкого тр. и а/д Меридиан', 61.420044,
        55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1778, 550, 1481241600, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  99а  ост. "п. Бабушкина" (троллейбус  в центр)', 61.347882, 55.177853);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1779, 551, 1481241600, 1450742400, 1608508800, 14,
        'ул. 40 лет Победы/ул. Братьев Кашириных  110  ост. "24-й микрорайон" (автобус  в сторону пр. Победы)',
        61.312237, 55.177745);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1780, 552, 1481241600, 1450742400, 1608508800, 14,
        'ул. Кирова  23  ост. "Цирк" (автобус  в сторону Теплотех. Института)', 61.400461, 55.174172);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1781, 553, 1481241600, 1450742400, 1608508800, 14,
        'ул. Российская  40  ост. "Плавательный бассейн" (маршрутное такси  в центр)', 61.413989, 55.177632);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1782, 554, 1481241600, 1450742400, 1608508800, 14,
        'Свердловский пр.  30  ост. "Торговый центр" (троллейбус  в центр)', 61.38713, 55.175509);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1783, 555, 1481241600, 1450742400, 1608508800, 14,
        'ул. Каслинская  18  ост. "Каслинский рынок" (трамвай  в центр)', 61.38898, 55.192584);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1784, 556, 1481241600, 1450742400, 1608508800, 14,
        'Комсомольский пр.  82  ост. "8-й микрорайон" (маршрутное такси  в сторону ул. Чичерина)', 61.307009,
        55.194917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1785, 557, 1481241600, 1450742400, 1608508800, 14,
        'ул. Героев Танкограда  40  ост. "пр. Победы" (автобус  в центр)', 61.441433, 55.186792);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1786, 558, 1481241600, 1450742400, 1608508800, 14,
        'Свердловский пр.  23а  ост. "пр. Победы" (троллейбус  в сторону ЧМЗ)', 61.38748, 55.185502);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1787, 559, 1481241600, 1450742400, 1608508800, 14,
        'ул. Горького/пр. Победы  115  ост. "ул. 5-го Декабря" (трамвай  в сторону С-З', 61.433806, 55.185764);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1788, 560, 1481241600, 1450742400, 1608508800, 14,
        'ул. Энгельса/ул. Труда  179  ост. "ул. Труда" (троллейбус  в центр)', 61.379539, 55.16701);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1789, 561, 1481241600, 1450742400, 1608508800, 14,
        'Комсомольский пр.  28  ост. "ул. Красного Урала" (троллейбус  в сторону ул. Молодогвардейцев)', 61.352859,
        55.195061);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1790, 562, 1481241600, 1450742400, 1608508800, 14,
        'Свердловский пр./ул. Каслинская  64  ост. "Торговый центр" (троллейбус  из центра)', 61.392457, 55.173134);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1791, 563, 1481241600, 1450742400, 1608508800, 14,
        'ул. Каслинская  5а  ост. "Каслинский рынок" (трамвай  в сторону ЧМЗ)', 61.391343, 55.191875);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1792, 564, 1481241600, 1450742400, 1608508800, 14,
        'ул. Гагарина  16  ост. "Дом одежды" (трамвай  в сторону ул. Новороссийской)', 61.440543, 55.139617);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1793, 565, 1481241600, 1450742400, 1608508800, 14,
        'ул. Блюхера  напротив д. 93  ост. "Энергетический колледж" (троллейбус  в сторону АМЗ)', 61.363926, 55.120387);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1794, 566, 1481241600, 1450742400, 1608508800, 14,
        'Троицкий тр.  напротив д. 70  ост. "п. Смолино" (автобус  в сторону п. Исаково)', 61.389312, 55.074211);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1795, 567, 1481241600, 1450742400, 1608508800, 14,
        'ул. Пушкина  64  ост. "Кинотеатр Пушкина" (троллейбус  в сторону АМЗ)', 61.408752, 55.158045);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1796, 568, 1481241600, 1450742400, 1608508800, 14,
        'ул. Румянцева  2а  ост. "60 лет Октября" (автобус  в сторону ш. Металлургов)', 61.378173, 55.26068);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1797, 569, 1481241600, 1450742400, 1608508800, 14,
        'ул. Салавата Юлаева  3  ост. "С. Юлаева" (маршрутное такси  в центр)', 61.276745, 55.188668);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1798, 570, 1481241600, 1450742400, 1608508800, 14,
        'ул. Свободы  106  ост. "Ж/д институт" (троллейбус  в сторону ж/д вокзала)', 61.415669, 55.146028);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1799, 571, 1481241600, 1450742400, 1608508800, 14,
        'ул. Сталеваров  26  рст. "ДК Восток" (троллейбус  в сторону ш. Металлургов)', 61.402302, 55.263927);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1800, 572, 1481241600, 1450742400, 1608508800, 14,
        'ул. Тимирязева  29  ост. "Кинотеатр Пушкина" (троллейбус  в сторону центра)', 61.40868, 55.157644);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1801, 573, 1481241600, 1450742400, 1608508800, 14, 'ул. Труда  187  ост. "Зоопарк" (троллейбус  в центр)',
        61.372541, 55.169946);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1802, 574, 1481241600, 1450742400, 1608508800, 14, 'ул. Центральная  напротив д. 3в  ост. "п. Шершни"',
        61.301493, 55.147706);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1803, 575, 1481241600, 1450742400, 1608508800, 14,
        'ул. Черкасская  3  ост. "ДЦ Импульс" (троллейбус  в сторону ЧМЗ)', 61.379853, 55.242458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1804, 576, 1481241600, 1450742400, 1608508800, 14,
        'ул. Братьев Кашириных  32  ост. "Торговый центр" (автобус  в сторону С-З)', 61.391001, 55.175046);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1805, 577, 1481241600, 1450742400, 1608508800, 14,
        'ул. 2-я Эльтонская  напротив д. 43  ост. "ул. 2-я Эльтонская" (автобус  в сторону ул. 1-я Эльтонская)',
        61.52821, 55.168485);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1806, 578, 1481241600, 1450742400, 1608508800, 14, 'ул. Энгельса  83  ост. "ул. Курчатова" (автобус  в центр)',
        61.382261, 55.153756);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1807, 579, 1482105600, 1461974400, 1619654400, 14,
        'ул. Свободы  149  ост. "ул. Плеханова" (троллейбус  в центр)', 61.41398, 55.156225);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1808, 580, 1482105600, 1461974400, 1619654400, 14,
        'Комсомольский пр.  69  ост. "ул. Солнечная" (маршрутное такси  в центр)', 61.315713, 55.194013);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1809, 581, 1482105600, 1461974400, 1619654400, 14,
        'ул. Чичерина  38В  ост. "ул. 250 лет Челябинску" (маршрутное такси  в сторону ул. Братьев Кашириных)',
        61.298628, 55.177087);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1810, 582, 1482105600, 1461974400, 1619654400, 14,
        'ул. Чайковского  напротив д. 89  ост. "Обувная фабрика" (трамвай  в центр)', 61.360423, 55.187795);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1811, 583, 1482105600, 1461974400, 1619654400, 14,
        'ул. Энгельса  44г  ост. "ул. Курчатова"  маршрутное такси  в сторону ул. Худякова)', 61.380877, 55.153457);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1812, 584, 1482105600, 1461974400, 1619654400, 14,
        'пр. Ленина  87  ост. "ПКиО им. Гагарина" (троллейбус  в центр)', 61.365938, 55.158904);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1813, 585, 1482105600, 1461974400, 1619654400, 14, 'ул. Свободы  159  ост. "ул. Евтеева" (троллейбус  в центр)',
        61.41584, 55.151091);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1814, 586, 1482105600, 1461974400, 1619654400, 14,
        'ул. Воровского  напротив д. 40  ост. "ул. Доватора" (автобус  в сторону плотины)', 61.38112, 55.146147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1815, 587, 1482105600, 1461974400, 1619654400, 14,
        'пр. Победы  287  ост. "Больница скорой помощи" (трамвай  в центр)', 61.339887, 55.18781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1816, 588, 1482105600, 1461974400, 1619654400, 14,
        'ул. Труда  напротив д. 203  ост. "ТРК Родник" (троллейбус  в сторону С-З)', 61.355976, 55.171051);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1817, 589, 1482105600, 1461974400, 1619654400, 14, 'ул. 250 лет Челябинску  17  ост. "ул. 250 лет Челябинску"',
        61.299885, 55.178198);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1818, 590, 1482105600, 1461974400, 1619654400, 14,
        'ул. Кирова  1Б  ост. "Теплотехнический институт" (трамвай  в сторону С-З  ЧТЗ)', 61.372056, 55.040446);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1819, 591, 1482105600, 1461974400, 1619654400, 14,
        'ул. Братьев Кашириных  103  ост. "Каширинский рынок" (троллейбус  в центр)', 61.341845, 55.178121);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1820, 592, 1482105600, 1461974400, 1619654400, 14,
        'ул. Молодогвардейцев  43  ост. "Педучилище" (троллейбус  из центра)', 61.33394, 55.183235);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1821, 593, 1482105600, 1461974400, 1619654400, 14,
        'пр. Победы  124  ост. "м-н Юрюзань" (автобус  в сторону Теплотех. Института)', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1822, 594, 1482105600, 1461974400, 1619654400, 14,
        'ул. Дарвина  2В  ост. "ЦРМ" (автобус  в сторону ул. Блюхера)', 61.382854, 55.119651);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1823, 595, 1482105600, 1461974400, 1619654400, 14,
        'ул. Доватора  напротив д. 1б  ост. "м-н Губернский" (автобус  в сторону ул. Воровского)', 61.401449, 55.13762);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1824, 596, 1482105600, 1461974400, 1619654400, 14,
        'ул. Горького  28  ост. "ул. Правдухина" (трамвай  в сторону С-З)', 61.442142, 55.173303);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1825, 597, 1482105600, 1461974400, 1619654400, 14,
        'ул. Центральная  3в  ост. "п. Шершни" (маршрутное такси  в сторону плотины)', 61.301493, 55.147706);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1826, 598, 1482105600, 1461974400, 1619654400, 14,
        'ул. Черкасская  3  ост. "ДЦ Импульс" (трамвай  в сторону ЧМЗ)', 61.379853, 55.242458);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1827, 599, 1482105600, 1461974400, 1619654400, 14,
        'ул. Чичерина  2  ост. "ЖК Александровский" (автобус  в сторону центра)', 61.280446, 55.190287);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1828, 600, 1482105600, 1461974400, 1619654400, 14,
        'ул. Братьев Кашириных  147  ост. "ул. Академика Макеева" (автобус  в сторону С-З)', 61.296382, 55.168526);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1829, 601, 1482105600, 1461974400, 1619654400, 14,
        'ул. Б. Хмельницкого  24  ост. "Общежитие" (троллейбус  в центр)', 61.382315, 55.258715);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1830, 602, 1482105600, 1461974400, 1619654400, 14,
        'ул. Жукова  5а  ост. "Кинотеатр Россия" (троллейбус  в сторону ул. Сталеваров)', 61.391684, 55.258268);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1831, 603, 1482105600, 1461974400, 1619654400, 14,
        'ул. Сталеваров  40  ост. "ДК Металлургов" (автобус  в центр)', 61.402194, 55.257673);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1832, 604, 1482105600, 1461974400, 1619654400, 14, 'ул. Павелецкая  2-я  18/1  ост. "ЧМК" (трамвай  в центр)',
        61.411348, 55.26423);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1833, 605, 1482105600, 1461974400, 1619654400, 14,
        'ул. Машиностроителей  22  ост. "ЗЭМ" (троллейбус  в сторону ул. Новороссийской)', 61.471625, 55.120357);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1834, 606, 1482105600, 1461974400, 1619654400, 14,
        'пр. Ленина  20  ост. "Комсомольская пл." (троллейбус  в сторону ПКиО)', 61.439753, 55.161327);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1835, 607, 1482105600, 1461974400, 1619654400, 14,
        'ул. Гагарина  56  ост. "Строительное училище" (троллейбус  в сторону ул. Новороссийской)', 61.434282,
        55.120248);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1836, 624, 1482451200, 1482192000, 1639872000, 8, 'ул.Кирова  82', 61.399966, 55.167282);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1837, 627, 1482451200, 1482192000, 1639872000, 6, 'пр.Победы 164', 61.401898, 55.185148);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1838, 628, 1482451200, 1482192000, 1639872000, 6, 'ул.С.Юлаева 6', 61.27935, 55.190827);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1839, 629, 1482451200, 1482192000, 1639872000, 6, 'ул.Мастеровая 8', 61.363827, 55.204048);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1840, 631, 1482451200, 1482192000, 1639872000, 6, 'пересеч.ул.Чичерина и Комсомольского пр.', 61.286496,
        55.186998);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1841, 633, 1482451200, 1482192000, 1639872000, 6, 'ул.Доватора перед виадуком в Ленинский р-н в центр',
        61.394154, 55.141238);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1842, 634, 1482451200, 1482192000, 1639872000, 6, 'ул.Дзержинского 119', 61.423116, 55.133334);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1843, 636, 1482451200, 1482192000, 1639872000, 6, 'ул.Гагарина 29', 61.438046, 55.13285);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1844, 641, 1482710400, 1482192000, 1639872000, 12, 'Копейское шоссе 35Б', 61.458285, 55.137729);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1845, 642, 1482796800, 1461974400, 1619654400, 14,
        'ул. Свободы  155/2  ост. "Академическая" (троллейбус  в центр)', 61.415687, 55.153987);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1846, 643, 1482796800, 1461974400, 1619654400, 14,
        'пр. Ленина  21В  ост. "Агентство воздушных сообщений" (троллейбус  в сторону ЧТЗ)', 61.426224, 55.160391);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1847, 644, 1482796800, 1461974400, 1619654400, 14,
        'ул. Братьев Кашириных  напротив д. 101  ост. "п. Бабушкина"', 61.344208, 55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1848, 645, 1482796800, 1461974400, 1619654400, 14,
        'ул. Молодогвардейцев  напротив д. 48а  ост. "пр. Победы" (троллейбус  из центра)', 61.332036, 55.189557);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1849, 646, 1482796800, 1461974400, 1619654400, 14, 'пр. Победы  289а  ост. "Университет" (трамвай  в центр)',
        61.331712, 55.188442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1850, 647, 1482796800, 1461974400, 1619654400, 14, 'ул. Горького  38  ост. "ДК Смена" (трамвай  в сторону С-З)',
        61.438378, 55.178624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1851, 648, 1482796800, 1461974400, 1619654400, 14,
        'Троицкий тр.  70  ост. "п. Смолино" (автобус  в сторону ул. Блюхера)', 61.389312, 55.074211);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1852, 649, 1482796800, 1461974400, 1619654400, 14,
        'ул. Братьев Кашириных  161  ост. "ул. Академика Королева" (автобус  в сторону С-З)', 61.295726, 55.164305);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1853, 650, 1482796800, 1461974400, 1619654400, 14,
        'ул. Дарвина  10  ост. "Кинотеатр Маяк" (автобус  в сторону ул. Блюхера)', 61.378398, 55.121597);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1854, 651, 1482796800, 1461974400, 1619654400, 14,
        'пр. Победы  127  ост. "Юридический институт" (автобус в сторону ЧТЗ)', 61.425182, 55.185276);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1855, 652, 1482796800, 1461974400, 1619654400, 14,
        'ул. Сталеваров  37/1  ост. "ул. Сталеваров" (автобус  в сторону ЧМЗ)', 61.403766, 55.251516);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1856, 653, 1482796800, 1461974400, 1619654400, 14,
        'ул. Румянцева  28б  ост. "Больница ЧМК" (маршрутное такси  в центр)', 61.379009, 55.251511);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1857, 654, 1482796800, 1461974400, 1619654400, 14,
        'ул. Б. Хмельницкого  напротив д. 35  ост. "ул. Б. Хмельницкого (автобус  в центр)', 61.380347, 55.258089);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1858, 655, 1482796800, 1461974400, 1619654400, 14,
        'ул. Гагарина  31  ост. "Кинотеатр Аврора" (маршрутное такси  в сторону КБС)', 61.437229, 55.132119);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1859, 656, 1482796800, 1461974400, 1619654400, 14,
        'ул. Дзержинского  102  ост. "Кинотеатр Аврора" (маршрутное такси  в сторону ж/д вокзала)', 61.435073,
        55.132129);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1860, 657, 1482796800, 1461974400, 1619654400, 14,
        'ул. Воровского  85  ост. "Обл. больница" (троллейбус  в центр)', 61.376197, 55.137734);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1861, 658, 1482796800, 1461974400, 1619654400, 14,
        'ул. Марченко  13  ост. "Магазин № 28" (автобус  в сторону С-В)', 61.46292, 55.170614);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1862, 659, 1482796800, 1461974400, 1619654400, 14,
        'ул. Сталеваров  66/3  ост. "ул. Сталеваров" (трамвай  в сторону ЧМЗ)', 61.40276, 55.251778);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1863, 660, 1482796800, 1461974400, 1619654400, 14,
        'ул. Павелецкая  2-я  14  ост. "ЧМК" (трамвай  в сторону завода Теплоприбор)', 61.4131, 55.263809);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1864, 661, 1482796800, 1461974400, 1619654400, 14,
        'пр. Ленина  7  ост. "Театр ЧТЗ" (троллейбус  в сторону ЧТЗ)', 61.451772, 55.159506);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1865, 662, 1482796800, 1461974400, 1619654400, 14,
        'ул. Краснознаменная  28  ост. "ул. Краснознаменная" (трамвай  в центр)', 61.379431, 55.183426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1866, 663, 1482796800, 1461974400, 1619654400, 14,
        'ул. Машиностроителей  21к1  ост. "ЗЭМ" (троллейбус  в сторону Копейского ш.)', 61.474634, 55.122709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1867, 664, 1482796800, 1461974400, 1619654400, 14,
        'ул. Молодогвардейцев  24  ост. "ул. Куйбышева" (маршрутное такси  в центр)', 61.331811, 55.1976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1868, 665, 1482796800, 1461974400, 1619654400, 14,
        'ул. Танкистов  41  ост. "п. Первоозерный" (троллейбус  в центр)', 61.475865, 55.190611);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1869, 666, 1482796800, 1461974400, 1619654400, 14,
        'ул. Воровского  64  ост. "Медакадемия" (троллейбус  в сторону АМЗ)', 61.37361, 55.14006);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1870, 667, 1482796800, 1461974400, 1619654400, 14,
        'ул. Черкасская  15/2  ост. "Авторынок" (трамвай  в сторону ЧМЗ)', 61.377482, 55.231411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1871, 668, 1482796800, 1461974400, 1619654400, 14,
        'ул. Машиностроителей  10  ост. "ул. Энергетиков" (троллейбус  в сторону ул. Новороссийской)', 61.473871,
        55.124341);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1872, 669, 1482796800, 1461974400, 1619654400, 14,
        'ул. Гагарина  40  ост. "Управление соцзащиты населения" (троллейбус  в сторону ул. Новороссийской)', 61.397433,
        55.077772);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1873, 670, 1482796800, 1461974400, 1619654400, 14, 'ул. Цвиллинга  61  ост. "ул. Евтеева" (трамвай  в центр)',
        61.412435, 55.149403);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1874, 671, 1482883200, 1482192000, 1639872000, 16, 'Комсомольский пр.  29', 61.357126, 55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1875, 672, 1482883200, 1482192000, 1639872000, 16, 'ул.Труда 203 (из центра)', 61.355976, 55.171051);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1876, 673, 1482883200, 1482192000, 1639872000, 16, 'ул. Воровского  85', 61.376197, 55.137734);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1877, 674, 1482883200, 1482192000, 1639872000, 16, 'ул. Энгельса  83', 61.382261, 55.153756);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1878, 675, 1482883200, 1482192000, 1639872000, 16, 'Комсомольский пр.  28', 61.352859, 55.195061);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1879, 676, 1482883200, 1482192000, 1639872000, 16, 'ул.Труда 168', 61.373772, 55.172254);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1880, 677, 1482883200, 1482192000, 1639872000, 16, 'ул.Салавата Юлаева  5', 61.276098, 55.188545);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1881, 678, 1482883200, 1482192000, 1639872000, 16, 'ул. Труда 203 (в центр)', 61.355976, 55.171051);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1882, 679, 1482883200, 1482192000, 1639872000, 16, 'Комсомольский пр.  111', 61.287668, 55.186921);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1883, 680, 1482883200, 1482192000, 1639872000, 16, 'ул. Труда 183', 61.375119, 55.168578);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1884, 681, 1482883200, 1482192000, 1639872000, 16, 'ул. Чичерина  2', 61.280446, 55.190287);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1885, 682, 1482883200, 1482192000, 1639872000, 16, 'Комсомольский пр.  103', 61.294917, 55.190195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1886, 683, 1482883200, 1482192000, 1639872000, 16, 'ул. Труда 187', 61.372541, 55.169946);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1887, 684, 1482883200, 1482192000, 1639872000, 16, 'ул. Чичерина  5', 61.284874, 55.188658);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1888, 685, 1482883200, 1482192000, 1639872000, 16, 'ул.Труда 166', 61.374562, 55.170784);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1889, 686, 1482883200, 1482192000, 1639872000, 16, 'ул.Бр.Кашириных  66', 61.349769, 55.179236);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1890, 687, 1482883200, 1482192000, 1639872000, 16, 'ул. Кирова  2', 61.398394, 55.182768);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1891, 688, 1482883200, 1482192000, 1639872000, 16, 'ул.Марченко  13', 61.46292, 55.170614);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1892, 689, 1482883200, 1482192000, 1639872000, 16, 'ул.Комарова/ ул. Салютная 2', 61.456965, 55.171766);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1893, 690, 1482883200, 1482192000, 1639872000, 16, 'ул.Бр.Кашириных  ост. "ул. Северо-Крымская" (из центра)',
        61.361698, 55.184711);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1894, 691, 1482883200, 1482192000, 1639872000, 16, 'ул.Марченко  18', 61.461241, 55.170691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1895, 692, 1482883200, 1482192000, 1639872000, 16, 'Свердловский пр.  23А', 61.38748, 55.185502);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1896, 693, 1482883200, 1482192000, 1639872000, 16, 'ул. Энгельса  63', 61.381632, 55.160782);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1897, 694, 1482883200, 1482192000, 1639872000, 16, 'ул.Бр.Кашириных 152', 61.294127, 55.166547);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1898, 695, 1482883200, 1482192000, 1639872000, 16, 'ул. Молодогвардейцев 62', 61.331964, 55.183575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1899, 696, 1482883200, 1482192000, 1639872000, 16, 'Свердловский пр. 28', 61.386456, 55.182264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1900, 697, 1482883200, 1482192000, 1639872000, 16, 'ул.Труда 179', 61.379539, 55.16701);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1901, 1, 1484006400, 1482192000, 1639872000, 6, 'ул.Свободы 44', 61.410791, 55.164207);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1902, 2, 1484092800, 1482192000, 1639872000, 9, 'пересеч.ул.Мастеровая и ул.Теннисная', 61.362825, 55.204761);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1903, 5, 1484092800, 1482192000, 1639872000, 9, 'ул. Кирова  напротив д. 82 констр.4', 61.399966, 55.167282);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1904, 6, 1484092800, 1482192000, 1639872000, 9, 'ул.Кирова  86  поз. 1  констр.5', 61.40011, 55.166444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1905, 9, 1484092800, 1482192000, 1639872000, 9, 'ул.Кирова напротив д. 86 констр.8', 61.40011, 55.166444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1906, 10, 1484092800, 1482192000, 1639872000, 9, 'ул.Кирова 86 констр.9', 61.40011, 55.166444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1907, 13, 1484092800, 1482192000, 1639872000, 9, 'ул. Кирова  143 констр.12', 61.400919, 55.165693);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1908, 55, 1490140800, 1492128000, 1649808000, 17, 'ул.Российская 154', 61.41566, 55.165174);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1909, 57, 1491177600, 1479254400, 1636934400, 15, 'ул.Молодогвардейцев 7  поз. 1', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1910, 58, 1491177600, 1479254400, 1636934400, 15, 'ул.Молодогвардейцев 7  поз. 2', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1911, 59, 1491177600, 1479254400, 1636934400, 15, 'ул.Молодогвардейцев 7  поз. 3', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1912, 60, 1491177600, 1479254400, 1636934400, 15, 'ул.Молодогвардейцев 7  поз. 4', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1913, 61, 1491177600, 1479254400, 1636934400, 15, 'ул.Молодогвардейцев 7  поз. 5', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1914, 62, 1491177600, 1479254400, 1636934400, 15, 'ул.Молодогвардейцев 7  поз. 6', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1915, 63, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 1', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1916, 64, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 2', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1917, 65, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 3', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1918, 66, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 4', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1919, 67, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 5', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1920, 68, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 6', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1921, 69, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 7', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1922, 70, 1491177600, 1479254400, 1636934400, 17, 'ул.Молодогвардейцев 7  поз. 8', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1923, 76, 1492732800, 1492473600, 1650153600, 9, 'пр. Победы  287', 61.339887, 55.18781);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1924, 77, 1492732800, 1492473600, 1650153600, 12, 'ул. Чичерина 52', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1925, 78, 1492992000, 1492473600, 1650153600, 12, 'ул. Ш. Руставелли  25', 61.428641, 55.141423);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1926, 79, 1493078400, 1492473600, 1650153600, 12, 'Копейское шоссе  ост. "Сад Энергетик"', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1927, 80, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 1', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1928, 81, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 2', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1929, 82, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 3', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1930, 83, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 4', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1931, 84, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 5', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1932, 85, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 6', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1933, 86, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 7', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1934, 87, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 8', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1935, 88, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 9', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1936, 89, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 10', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1937, 90, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 11', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1938, 91, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 12', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1939, 92, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 13', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1940, 93, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 14', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1941, 94, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 15', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1942, 95, 1493078400, 1492473600, 1650153600, 16, 'ул. Академика Королева  3  поз. 16', 61.300092, 55.162721);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1943, 96, 1493164800, 1467331200, 1625011200, 15, 'ул.Бр.Кашириных 129Д', 61.314734, 55.177334);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1944, 97, 1493683200, 1492473600, 1650153600, 6, 'Уфимский тр.  1868+300м', 61.326251, 55.08157);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1945, 98, 1493683200, 1492473600, 1650153600, 6, 'ул. Курчатова  23', 61.384542, 55.151204);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1946, 99, 1493683200, 1492473600, 1650153600, 6, 'ул. Бр. Кашириных  114', 61.308985, 55.176331);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1947, 100, 1493683200, 1492473600, 1650153600, 6, 'Комсомольский пр.  93', 61.304808, 55.194049);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1948, 101, 1493683200, 1492473600, 1650153600, 6, 'Свердловский пр.  62А', 61.388172, 55.161342);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1949, 102, 1493683200, 1492473600, 1650153600, 6, 'пересечение Комсомольского пр. и ул. Чайковского  15/2',
        61.363935, 55.191135);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1950, 103, 1493683200, 1492473600, 1650153600, 6, 'ул. Бр. Кашириных  132', 61.300164, 55.172511);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1951, 104, 1493683200, 1492473600, 1650153600, 6, 'ул. Воровского  64', 61.37361, 55.14006);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1952, 106, 1493683200, 1492473600, 1650153600, 6, 'ул. Бр. Кашириных  100', 61.320187, 55.179791);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1953, 107, 1493683200, 1492473600, 1650153600, 6, 'ул. Дзержинского 104', 61.433878, 55.13269);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1954, 108, 1493683200, 1492473600, 1650153600, 6, 'ул. Бр. Кашириных  141', 61.299517, 55.170681);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1955, 109, 1493683200, 1492473600, 1650153600, 6, 'ул. Молодогвардейцев  40', 61.331964, 55.192528);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1956, 110, 1493683200, 1492473600, 1650153600, 6, 'ул. Дарвина  113', 61.367591, 55.126055);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1957, 111, 1493683200, 1492473600, 1650153600, 6, 'Комсомольский пр.  94', 61.294217, 55.191377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1958, 112, 1493683200, 1492473600, 1650153600, 6, 'ул. Бр. Кашириных  134', 61.298097, 55.17174);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1959, 113, 1493683200, 1492473600, 1650153600, 6, 'Комсомольский пр.  24', 61.358195, 55.193694);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1960, 114, 1493683200, 1492473600, 1650153600, 6, 'ул.Гагарина  44', 61.433321, 55.126925);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1961, 115, 1493683200, 1492473600, 1650153600, 6, 'ул. Блюхера  напротив д. 69', 61.366738, 55.128223);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1962, 116, 1493683200, 1492473600, 1650153600, 6, 'Комсомольский пр.  50', 61.330509, 55.195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1963, 117, 1493683200, 1492473600, 1650153600, 6, 'ул.Доватора  22', 61.39667, 55.140482);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1964, 118, 1493683200, 1492473600, 1650153600, 6, 'ул. Молодогвардейцев 56', 61.331991, 55.186546);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1965, 119, 1493683200, 1492473600, 1650153600, 6, 'Комсомольский пр.  105', 61.29242, 55.18909);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1966, 120, 1493683200, 1492473600, 1650153600, 6, 'пр. Победы  390', 61.294585, 55.184377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1967, 122, 1493683200, 1492473600, 1650153600, 12, 'Комсомольский пр.  34  поз. 1', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1968, 123, 1493683200, 1492473600, 1650153600, 12, 'Комсомольский пр.  34  поз. 2', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1969, 124, 1493683200, 1492473600, 1650153600, 12, 'Комсомольский пр.  34  поз. 3', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1970, 125, 1493856000, 1492473600, 1650153600, 6, 'ш. Металлургов  21п', 61.409929, 55.252301);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1971, 126, 1493856000, 1492473600, 1650153600, 6, 'ул. Енисейская  52', 61.493194, 55.142771);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1972, 127, 1493942400, 1492473600, 1650153600, 12, 'Западное шоссе  100м до поворота на пос. Западный',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1973, 128, 1494374400, 1492473600, 1807920000, 7, 'Свердловский пр.  51 (малая арена  вид на ул. Труда)',
        61.391244, 55.169015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1974, 129, 1494374400, 1492473600, 1650153600, 6, 'ул. Бажова  35', 61.465984, 55.189383);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1975, 130, 1494374400, 1492473600, 1650153600, 6, 'дорога из Аэропорта  750 м от площади аэропорта (в город)',
        61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1976, 131, 1494374400, 1492473600, 1650153600, 6, 'Комсомольский пр.  29', 61.357126, 55.192811);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1977, 132, 1494374400, 1492473600, 1650153600, 6, 'пр. Победы  289', 61.329709, 55.188396);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1978, 133, 1494374400, 1492473600, 1650153600, 6, 'Комсомольский пр.  1', 61.39022, 55.191665);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1979, 134, 1494374400, 1492473600, 1650153600, 6,
        'пересечение ул. Бр. Кашириных (в центр) и ул. Молодогвардейцев', 61.332633, 55.178684);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1980, 135, 1494374400, 1492473600, 1650153600, 6, 'ул. Российская  275', 61.418202, 55.156477);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1981, 136, 1494374400, 1492473600, 1650153600, 6, 'автодорога Меридиан/  ул. Гражданская  напротив д.25',
        61.41981, 55.131084);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1982, 137, 1494374400, 1492473600, 1650153600, 6, 'пр. Победы  319', 61.297289, 55.184094);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1983, 138, 1494374400, 1492473600, 1650153600, 6, 'ул. Курчатова 1А', 61.396679, 55.146358);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1984, 139, 1494374400, 1492473600, 1650153600, 6, 'ул. Героев Танкограда  65', 61.451629, 55.173951);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1985, 144, 1495756800, 1491004800, 1648684800, 17, 'ул. Чичерина  52', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1986, 145, 1495756800, 1491004800, 1648684800, 15, 'ул. Чичерина  52  поз. 1', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1987, 146, 1495756800, 1491004800, 1648684800, 15, 'ул. Чичерина  52  поз. 2', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1988, 147, 1495756800, 1491004800, 1648684800, 15, 'ул. Чичерина  52  поз. 3', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1989, 148, 1495756800, 1491004800, 1648684800, 15, 'ул. Чичерина  52  поз. 4', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1990, 149, 1495756800, 1488326400, 1646006400, 12, 'ул. Братская  напротив д. 2а', 61.369505, 55.128984);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1991, 152, 1496102400, 1492646400, 1619740800, 6, 'ул. Рабоче-Колхозная  34 36', 61.315157, 55.154167);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1992, 162, 1499644800, 1498521600, 1656201600, 6,
        'пересечение Комсомольского пр. и ул. Каслинская  за перекрестком из центра', 61.390422, 55.192173);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1993, 163, 1499644800, 1498521600, 1656201600, 6, 'Троицкий тр.  54/1  в центр  до ост. "ул.Потребительская"',
        61.388172, 55.105212);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1994, 164, 1499644800, 1498521600, 1656201600, 12, 'Копейское шоссе ост. "Сад Энергетик"', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1995, 165, 1499644800, 1498521600, 1656201600, 15, 'ул. Чичерина 52  поз. 5', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1996, 166, 1499644800, 1498521600, 1656201600, 15, 'ул. Чичерина 52  поз. 6', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1997, 167, 1499644800, 1498521600, 1656201600, 15, 'ул. Чичерина 52  поз. 7', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1998, 168, 1499644800, 1498521600, 1656201600, 15, 'ул. Чичерина 52  поз. 8', 61.306838, 55.170619);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (1999, 169, 1499644800, 1498521600, 1656201600, 6, 'ул. Зальцмана  10', 61.531624, 55.170162);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2000, 170, 1499644800, 1498521600, 1656201600, 15, 'Копейское шоссе 33А', 61.464034, 55.133961);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2001, 171, 1499644800, 1498521600, 1656201600, 12, 'Копейское шоссе 33А', 61.464034, 55.133961);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2002, 176, 1499644800, 1498521600, 1656201600, 14,
        'ул.60 лет Октября 2Б  ост. "ЧМК" (маршрутное такси  в сторону центра)', 61.408285, 55.264999);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2003, 177, 1499644800, 1498521600, 1656201600, 14,
        'ул.Блюхера/ул.Доватора  23  ост. "ул. Доватора" (трамвай  в сторону центра)', 61.386878, 55.143029);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2004, 178, 1499644800, 1498521600, 1656201600, 14,
        'ул.Молодогвардейцев  2а/1  ост. "Автоцентр" (автобус  в сторону центра)', 61.316603, 55.207572);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2005, 179, 1499644800, 1498521600, 1656201600, 9, 'ул.Коммуны 58 поз.1', 61.262443, 55.075128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2006, 180, 1499644800, 1498521600, 1656201600, 9, 'ул.Коммуны 58 поз.2', 61.262443, 55.075128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2007, 181, 1499644800, 1498521600, 1656201600, 9, 'ул.Коммуны 60 поз.3', 61.262443, 55.075128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2008, 182, 1499644800, 1498521600, 1656201600, 9, 'ул.Коммуны 60 поз.4', 61.262443, 55.075128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2009, 183, 1499644800, 1498521600, 1656201600, 9, 'ул.Коммуны 60 поз.5', 61.262443, 55.075128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2010, 184, 1499904000, 1498521600, 1656201600, 9, 'пр.Победы  356', 61.314743, 55.189511);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2011, 201, 1505088000, 1504569600, 1662249600, 16, 'ул. Худякова  11', 61.37732, 55.147433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2012, 202, 1505088000, 1504569600, 1662249600, 16, 'ул. Марченко  22', 61.459129, 55.16704);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2013, 203, 1505088000, 1504569600, 1662249600, 16, 'ул. Худякова  19', 61.372712, 55.147165);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2014, 204, 1505260800, 1504569600, 1662249600, 12, 'ул.Хлебозаводская 15', 61.410989, 55.247575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2015, 205, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова напротив д. 92', 61.400227, 55.164819);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2016, 206, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова 96', 61.400299, 55.164325);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2017, 207, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова  напротив д.100', 61.400227, 55.16396);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2018, 208, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова 102', 61.400245, 55.163739);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2019, 209, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова напротив д.104 поз.1', 61.4002, 55.163168);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2020, 210, 1505260800, 1504569600, 1662249600, 9, 'пересеч.ул.Кирова (в центр) и ул.Коммуны', 61.400774,
        55.162809);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2021, 211, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова 163', 61.401116, 55.162499);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2022, 212, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова 110', 61.400254, 55.161939);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2023, 213, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова 167', 61.40144, 55.161707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2024, 214, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова 112', 61.400478, 55.161461);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2025, 215, 1505260800, 1504569600, 1662249600, 9, 'ул.Кирова напротив д.114', 61.400559, 55.160663);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2026, 216, 1505692800, 1504569600, 1662249600, 6, 'ул. Героев Танкограда  71п', 61.4434, 55.199866);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2027, 218, 1506038400, 1504569600, 1662249600, 8,
        'пр. Ленина  ост. "Публичная библиотека"  в сторону пл. Революции', 61.425973, 55.160962);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2028, 219, 1506038400, 1504569600, 1662249600, 8,
        'пр. Ленина  ост. "Публичная библиотека"  в сторону ул. Красная', 61.425973, 55.160962);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2029, 220, 1506038400, 1504569600, 1662249600, 15, 'ул.Энтузиастов 9а поз.1', 61.376314, 55.156821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2030, 221, 1506038400, 1504569600, 1662249600, 15, 'ул.Энтузиастов 9а поз.2', 61.376314, 55.156821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2031, 222, 1506038400, 1504569600, 1662249600, 15, 'ул.Энтузиастов 9а поз.3', 61.376314, 55.156821);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2032, 223, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова 86 конец дома', 61.40011, 55.166444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2033, 224, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова 86  начало дома', 61.40011, 55.166444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2034, 225, 1506297600, 1504569600, 1662249600, 8,
        'пересечение пр. Ленина и Свердловского пр.  до перекрестка из центра', 61.388823, 55.16005);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2035, 226, 1506297600, 1504569600, 1662249600, 8, 'пересечение ул.Кирова  90 и ул.К.Маркса', 61.40011,
        55.165482);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2036, 227, 1506297600, 1504569600, 1662249600, 8, 'пересечение ул.Кирова 147 и ул.К.Маркса 109', 61.400901,
        55.165127);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2037, 228, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова  161б/1', 61.401035, 55.164176);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2038, 229, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова 161а конец дома', 61.401071, 55.163934);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2039, 230, 1506297600, 1504569600, 1662249600, 8,
        'пересечение пр. Ленина 85 и ул.Тернопольская  до перекрестка в центр', 61.36831, 55.159043);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2040, 231, 1506297600, 1504569600, 1662249600, 8, 'пересечение ул.Кирова 161/а/1 и ул.Коммуны', 61.372056,
        55.040446);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2041, 232, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова 163', 61.401116, 55.162499);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2042, 233, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова 167 начало дома', 61.40144, 55.161707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2043, 234, 1506297600, 1504569600, 1662249600, 8, 'ул.Кирова 167  конец дома', 61.40144, 55.161707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2044, 265, 1510012800, 1506297600, 1663977600, 17, 'ул.Кирова 161А  поз.1', 61.401071, 55.163934);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2045, 266, 1510012800, 1506297600, 1663977600, 17, 'ул.Кирова 161А  поз.2', 61.401071, 55.163934);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2046, 267, 1510185600, 1504569600, 1662249600, 12, 'пересечение пр.Победы и ул.Горького', 61.432517, 55.18587);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2047, 268, 1510185600, 1504569600, 1662249600, 12, 'пересечение пр.Победы и ул.Чайковского', 61.360358,
        55.188036);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2048, 269, 1510704000, 1504569600, 1820016000, 7,
        'пересечение ул. Университетская Набережная и ул. Молодогвардейцев', 61.335291, 55.174506);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2049, 270, 1510704000, 1510617600, 1668297600, 6, 'пересеч.ул.Г.Танкограда и ул.1-ой Пятилетки 17', 61.453614,
        55.166089);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2050, 272, 1510704000, 1510617600, 1668297600, 6, 'Комсомольский пр. 104', 61.291163, 55.18985);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2051, 273, 1510704000, 1510617600, 1668297600, 6, 'ул.Черкасская напротив д.6 на разделит.газоне', 61.378919,
        55.239727);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2052, 274, 1510704000, 1510617600, 1668297600, 6, 'пр.Победы 86', 61.440274, 55.187887);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2053, 275, 1510704000, 1510617600, 1668297600, 6, 'ул.Черкасская 12', 61.377194, 55.236704);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2054, 276, 1511222400, 1510617600, 1668297600, 6, 'ул.Блюхера 101/1', 61.358976, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2055, 277, 1511222400, 1510617600, 1668297600, 6,
        'пересчение Троицкого тр. 15 и ул. Потребитеская  за перекрестком из центра', 0, 0);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2056, 278, 1511222400, 1510617600, 1668297600, 6, 'ул.Бр. Кашириных  109/1', 61.331847, 55.178136);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2057, 279, 1511222400, 1510617600, 1668297600, 6, 'Свердловский тр. 8/1', 61.370511, 55.232402);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2058, 280, 1511308800, 1510617600, 1668297600, 12, ' ул.Свободы 100', 61.415687, 55.148549);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2059, 281, 1511308800, 1510617600, 1668297600, 6, 'пересеч.ул.Блюхера и ул.Новоэлеваторная', 61.351679,
        55.107994);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2060, 282, 1511395200, 1510617600, 1668297600, 6, 'Троицкий тр.  23', 61.387471, 55.088898);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2061, 283, 1511395200, 1510617600, 1668297600, 8, 'пересеч.Свердловского пр. 84 и ул.К.Либкнехта', 61.388773,
        55.155129);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2062, 284, 1511395200, 1510617600, 1668297600, 9, 'ул.К.Либкнехта 9', 61.387246, 55.155252);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2063, 285, 1511395200, 1510617600, 1668297600, 6, 'Троицкий тр.  46', 61.387525, 55.11372);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2064, 288, 1511827200, 1510617600, 1668297600, 6, 'Троицкий тр.  напротив д.9', 61.385018, 55.122519);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2065, 289, 1511827200, 1510617600, 1668297600, 15, ' Копейское шоссе 52  поз. 1', 61.460172, 55.139983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2066, 290, 1511827200, 1510617600, 1668297600, 15, ' Копейское шоссе 52  поз. 2', 61.460172, 55.139983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2067, 291, 1511827200, 1510617600, 1668297600, 15, ' Копейское шоссе 52  поз. 3', 61.460172, 55.139983);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2068, 292, 1512000000, 1510617600, 1668297600, 6, 'ул.Труда 3Б', 61.427383, 55.167344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2069, 293, 1512000000, 1510617600, 1668297600, 6, 'ул.Блюхера 49', 61.371723, 55.133447);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2070, 294, 1512000000, 1510617600, 1668297600, 6, ' ул.Блюхера 150м до остановки "АМЗ" (в центр)', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2071, 295, 1512000000, 1510617600, 1668297600, 6, ' Бродокалмакский тр. 150м от ост "Тракторосад-1"', 61.483734,
        55.204413);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2072, 296, 1512000000, 1510617600, 1668297600, 6, 'Свердловский тр.  300 м до поворота на пос. Шагол', 61.31477,
        55.236201);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2073, 297, 1512518400, 1506384000, 1664064000, 17, 'ул.Барбюса 2', 61.436896, 55.147942);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2074, 300, 1514419200, 1513987200, 1671667200, 12, 'ул.40 лет Победы 33', 61.304062, 55.182824);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2075, 1, 1515456000, 1506297600, 1661817600, 17, 'ул.Кирова 143', 61.400919, 55.165693);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2076, 2, 1515628800, 1513987200, 1671667200, 6, 'пересечение ул. Бр. Кашириных (из центра) и ул. Спорта',
        61.377658, 55.176289);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2077, 3, 1515628800, 1513987200, 1671667200, 6, 'Свердловский тр.  выезд с ЧМЗ  справа в центр', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2078, 4, 1515628800, 1513987200, 1671667200, 6, 'Свердловский тр.  въезд на ЧМЗ', 61.402554, 55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2079, 5, 1515628800, 1513987200, 1671667200, 5, 'ул.Кирова 25', 61.401512, 55.172465);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2080, 22, 1516060800, 1513987200, 1671667200, 6, 'Троицкий тр.  13', 61.385683, 55.108689);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2081, 23, 1516320000, 1513987200, 1671667200, 6, 'ул. Чичерина 30', 61.291746, 55.180629);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2082, 24, 1516320000, 1513987200, 1671667200, 6, 'ул. Энгельса  49', 61.381614, 55.163199);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2083, 25, 1516320000, 1513987200, 1671667200, 6, 'ул. Энтузиастов 2', 61.374724, 55.161347);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2084, 26, 1516320000, 1513987200, 1671667200, 6, 'ул. Энтузиастов 18', 61.375469, 55.154167);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2085, 27, 1516320000, 1513987200, 1671667200, 6, 'Свердловский тр. 16Б', 61.376368, 55.224223);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2086, 28, 1516320000, 1513987200, 1671667200, 6, 'Троицкий тр. 13/1', 61.385926, 55.108045);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2087, 29, 1516320000, 1513987200, 1671667200, 6, 'Уфимский тр.  1', 61.318741, 55.077344);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2088, 30, 1516579200, 1513987200, 1671667200, 6, 'ул.Мамина 7', 61.47768, 55.196588);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2089, 31, 1516579200, 1513987200, 1671667200, 6, 'ул.Хохрякова 30 (в центр)', 61.486321, 55.1739);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2090, 32, 1516579200, 1513987200, 1671667200, 6, 'ул.Первой Пятилетки 57', 61.431596, 55.167909);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2091, 38, 1516579200, 1513987200, 1671667200, 9, 'пр. Ленина 80', 61.366199, 55.159995);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2092, 39, 1516579200, 1513987200, 1671667200, 9, 'ул. Лесопарковая  6', 61.363648, 55.150345);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2093, 46, 1516752000, 1512432000, 1670112000, 8, 'Свердловский пр. 63', 61.389573, 55.158853);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2094, 48, 1517443200, 1510617600, 1668297600, 12, 'ул.Воровского 28/1 ', 61.389025, 55.150844);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2095, 49, 1517443200, 1510617600, 1668297600, 15, ' пр.Ленина  21 БД "Спиридонов" (АЗС № 422)', 61.427158,
        55.159938);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2096, 50, 1517443200, 1510617600, 1668297600, 15, ' пр.Ленина  БД "Видгоф" (АЗС 424)', 61.425973, 55.160962);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2097, 51, 1517443200, 1510617600, 1668297600, 15, ' пересеч.ул.Чайковского и Комсомольского пр. (АЗС 432)',
        61.358473, 55.186638);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2098, 52, 1517443200, 1510617600, 1668297600, 15, ' пересеч.ул.Дачная 37/ ул.Дубравная (АЗС № 435)', 61.362417,
        55.257612);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2099, 53, 1517961600, 1513987200, 1671667200, 15, ' пересеч. Свердловского пр. и ул. Воровского (АЗС №  74412)',
        61.389446, 55.151841);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2100, 54, 1518134400, 1513987200, 1671667200, 6, 'ул. Энгельса  46', 61.379916, 55.151276);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2101, 55, 1518134400, 1513987200, 1671667200, 6, 'Копейское шоссе 39А', 61.452635, 55.140883);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2102, 57, 1518480000, 1516233600, 1673913600, 17, 'Комсомольский пр. 34  поз.1', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2103, 58, 1518480000, 1516233600, 1673913600, 17, 'Комсомольский пр. 34  поз.2', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2104, 59, 1518480000, 1516233600, 1673913600, 17, 'Комсомольский пр. 34  поз.3', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2105, 60, 1518480000, 1516233600, 1673913600, 17, 'Комсомольский пр. 34  поз.4', 61.345061, 55.195277);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2106, 63, 1518739200, 1521072000, 1669852800, 17, 'Свердловский тр. 3В', 61.387264, 55.219411);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2107, 65, 1518739200, 1519862400, 1677542400, 9, 'ул.Воровского 6', 61.395089, 55.157423);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2108, 67, 1518998400, 1512086400, 1640908800, 17,
        'ул.Артиллерийская 136  у входа со стороны ул. Артиллерийской', 61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2109, 68, 1518998400, 1517443200, 1831161600, 7, 'Комсомольский пр. 118', 61.283994, 55.186679);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2110, 69, 1519776000, 1522540800, 1830211200, 7, 'пр.Ленина 55а', 61.398942, 55.159861);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2111, 75, 1522195200, 1519862400, 1640908800, 17, 'ул.Артиллерийская 136  у входа со стороны а/д Меридиан',
        61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2112, 76, 1523232000, 1525305600, 1646092800, 8,
        'ул.Молодогвардейцев 7 (главный вход комплекса напротив ул.Молодогвардейцев  7А)', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2113, 77, 1523232000, 1525305600, 1646092800, 8, 'ул.Молодогвардейцев 7 (вход напротив ул.Солнечная)',
        61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2114, 80, 1524096000, 1508889600, 1666656000, 15, 'ул.Механическая 14', 61.445313, 55.1965);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2115, 81, 1524096000, 1507939200, 1665705600, 12, 'ул.Механическая 14', 61.445313, 55.1965);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2116, 83, 1526860800, 1515628800, 1673395200, 12, 'ул.Игуменка 181  поз.1', 61.390606, 55.082503);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2117, 84, 1526860800, 1515628800, 1673395200, 12, 'ул.Игуменка 181  поз.2', 61.390606, 55.082503);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2118, 85, 1526860800, 1526083200, 1841616000, 7, 'площадь Революции 1', 61.404215, 55.159295);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2119, 86, 1526947200, 1526083200, 1683763200, 8, 'ул.Молодогвардейцев  34', 61.331901, 55.193885);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2120, 87, 1526947200, 1526083200, 1683763200, 15, 'ул.Бр.Кашириных 129б  поз.1', 61.31698, 55.178187);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2121, 88, 1526947200, 1526083200, 1683763200, 15, 'ул.Бр.Кашириных 129б  поз.2', 61.31698, 55.178187);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2122, 89, 1526947200, 1526083200, 1683763200, 15, 'ул.Бр.Кашириных 129б  поз.3', 61.31698, 55.178187);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2123, 90, 1526947200, 1526083200, 1683763200, 15, 'ул.Бр.Кашириных 129б  поз.4', 61.31698, 55.178187);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2124, 91, 1527206400, 1526083200, 1683763200, 9, 'ул.Воровского 38б  опора б/н', 61.383455, 55.14787);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2125, 92, 1527206400, 1526083200, 1683763200, 9, 'ул.Худякова 13 (на уровне середины дома)  опора б/н',
        61.375209, 55.147361);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2126, 93, 1527206400, 1526083200, 1683763200, 9, 'ул.Доватора 42  опора б/н', 61.385351, 55.145426);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2127, 94, 1527206400, 1526083200, 1683763200, 9, 'ул.Худякова 4  опора б/н', 61.378793, 55.147917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2128, 95, 1527206400, 1526083200, 1683763200, 9, 'пересечение ул.Воровского 46 и ул.Образцова  опора б/н',
        61.379736, 55.145514);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2129, 96, 1527120000, 1527120000, 1842652800, 7, 'пересечение Свердловского пр. и ул.Труда', 61.389624,
        55.167441);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2130, 97, 1527120000, 1527120000, 1842652800, 7, 'пересечение ул.Труда (из центра) и ул. Энгельса  21/1',
        61.381281, 55.168203);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2131, 98, 1527120000, 1527120000, 1842652800, 7, 'пересечение ул.Бр. Кашириных и ул. Каслинская (в центр)',
        61.394145, 55.183338);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2132, 101, 1527206400, 1526083200, 1683763200, 12, 'ул.Бр.Кашириных 135', 61.303281, 55.171822);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2133, 102, 1527206400, 1526083200, 1683763200, 9, 'ул.Машиностроителей 20', 61.472092, 55.121191);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2134, 103, 1527206400, 1526083200, 1683763200, 9, 'ул.Машиностроителей 34', 61.46955, 55.11717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2135, 104, 1527206400, 1526083200, 1683763200, 12, 'ул.Чичерина 43', 61.307548, 55.172177);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2136, 105, 1527206400, 1526083200, 1683763200, 9, 'ул.Гагарина 10', 61.442115, 55.142015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2137, 106, 1527206400, 1526083200, 1683763200, 9, 'ул.Гагарина  между д. 11 и д. 13', 61.396715, 55.074881);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2138, 107, 1527206400, 1526083200, 1683763200, 9, 'ул. Карла Маркса  80', 61.394487, 55.165575);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2139, 108, 1527465600, 1526083200, 1683763200, 12, 'пр.Победы  321', 61.296939, 55.183251);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2140, 109, 1527465600, 1526083200, 1683763200, 9, 'пр.Победы  384', 61.296786, 55.185934);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2141, 110, 1527465600, 1526083200, 1683763200, 12, 'ул.Новороссийская  между д.40 и д.42', 61.479288, 55.10942);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2142, 111, 1527465600, 1526083200, 1683763200, 8, 'ул.Воровского 81', 61.375667, 55.139025);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2143, 112, 1528070400, 1527120000, 1684800000, 6, 'пересеч.ул.Бр.Кашириных(в центр) и ул.Краснознаменная',
        61.380191, 55.175186);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2144, 113, 1528070400, 1527120000, 1684800000, 6, 'Троицкий тр.  перед поворотом в пос. Новосинеглазово',
        61.375487, 55.040709);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2145, 114, 1528070400, 1527120000, 1684800000, 6, 'ул.Блюхера 81', 61.364735, 55.123924);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2146, 115, 1528070400, 1527120000, 1684800000, 6, 'пересеч.пр.Победы 149А и ул.Болейко', 61.406874, 55.184161);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2147, 116, 1528070400, 1527120000, 1684800000, 6, 'пересеч.ул.Бр.Кашириных и ул.Парашютная 41', 61.375793,
        55.177437);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2148, 117, 1528070400, 1527120000, 1684800000, 6, 'Троицкий тр. 35к/1  напротив заводоуправления  поз. 1',
        61.386959, 55.098042);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2149, 118, 1528070400, 1527120000, 1684800000, 6, 'ул.Блюхера напротив Заводоуправления АМЗ', 61.342456,
        55.109477);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2150, 119, 1528070400, 1527120000, 1684800000, 6, 'Троицкий тр. 35к/1  напротив заводоуправления  поз. 2',
        61.386959, 55.098042);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2151, 120, 1528070400, 1527120000, 1684800000, 6, 'ул.Блюхера напротив д.93', 61.363926, 55.120387);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2152, 121, 1528070400, 1527120000, 1684800000, 8, 'пр. Ленина 76 ост. "ЮУрГУ"  поз.1', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2153, 122, 1528070400, 1527120000, 1684800000, 8, 'пр. Ленина 76 ост. "ЮУрГУ"  поз.2', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2154, 123, 1528070400, 1527120000, 1684800000, 8, 'пр. Ленина 76 ост. "ЮУрГУ"  поз.3', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2155, 124, 1528070400, 1527120000, 1684800000, 8, 'пр. Ленина 76 ост. "ЮУрГУ"  поз.4', 61.37016, 55.160478);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2156, 125, 1528070400, 1526083200, 1683763200, 12, 'ул. Энгельса  34', 61.380985, 55.157284);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2157, 126, 1528070400, 1526083200, 1683763200, 12, 'ул. Университетская Набережная  64', 61.310377, 55.17154);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2158, 127, 1528070400, 1526083200, 1683763200, 12, 'ул.Бейвеля/ ул.Скульптора Головницкого  16', 61.283212,
        55.207783);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2159, 128, 1528070400, 1522540800, 1838073600, 6, 'Копейское шоссе  82', 61.447011, 55.148302);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2160, 129, 1528070400, 1527811200, 1685404800, 9, 'пр.Ленина 11', 61.447326, 55.159954);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2161, 131, 1528156800, 1526083200, 1683763200, 12, 'ул. Лесопарковая/ поворот к ул. Лесопарковая 6А/1',
        61.359866, 55.151009);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2162, 132, 1528156800, 1526083200, 1841616000, 7, 'площадь Революции/ ул.Кирова 116', 61.400523, 55.159162);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2163, 133, 1528156800, 1526083200, 1683763200, 12, 'Комсомольский пр. 90', 61.298951, 55.193443);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2164, 134, 1528243200, 1526083200, 1683763200, 6, 'ул.Автодорожная 9а', 61.35788, 55.206899);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2165, 135, 1528243200, 1526083200, 1683763200, 12, 'ул.Енисейская 48', 61.49102, 55.140291);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2166, 136, 1528329600, 1526083200, 1683763200, 17, 'ул.Марченко 18', 61.461241, 55.170691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2167, 137, 1528502400, 1526083200, 1683763200, 9, 'Свердловский пр. 39', 61.387947, 55.177298);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2168, 138, 1528502400, 1526083200, 1683763200, 9, 'ул.Б.Хмельницкого 18', 61.387615, 55.258633);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2169, 139, 1528502400, 1526083200, 1683763200, 9, 'ул.Бр.Кашириных 68А', 61.348008, 55.179421);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2170, 140, 1528502400, 1526083200, 1683763200, 9, 'ул.Дзержинского 105', 61.429341, 55.132592);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2171, 141, 1528502400, 1526083200, 1683763200, 6, 'ул.2-ая Павелецкая 14', 61.4131, 55.263809);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2172, 142, 1529280000, 1527120000, 1842652800, 7, 'пересеч.Комсомольского пр. 48 и ул.Молодогвардейцев',
        61.334515, 55.195015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2173, 143, 1529280000, 1527120000, 1842652800, 7, 'пересеч.Свердловского пр. 7 и Комсомольского пр.', 61.387875,
        55.189933);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2174, 144, 1529280000, 1527120000, 1842652800, 7, 'пересеч.ул.Гагарина и ул. Дзержинского 91', 61.436663,
        55.130878);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2175, 145, 1529280000, 1527120000, 1842652800, 7,
        'пересечение пр.Ленина (из центра) и ул. Горького  Комсомольская площадь', 61.425973, 55.160962);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2176, 146, 1529280000, 1527120000, 1842652800, 7, 'пересечение ул.Черкасская и ш.Металлургов 43(через дорогу)',
        61.376008, 55.245917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2177, 147, 1529280000, 1527120000, 1842652800, 7, 'Привокзальная пл. (ТК "Синегорье")', 61.414456, 55.140507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2178, 148, 1529280000, 1527120000, 1842652800, 4, 'ул. Чичерина 22/5 и Комсомольского пр.', 61.286572,
        55.186504);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2179, 149, 1529280000, 1527120000, 1842652800, 4, 'Троицкий тр.  1/1', 61.384965, 55.124691);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2180, 150, 1529280000, 1527120000, 1842652800, 4, 'Свердловский тр.  7/1', 61.386815, 55.198222);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2181, 151, 1529280000, 1527120000, 1842652800, 4, 'пересеч. Свердловского тр. и ул. Индивидуальная', 61.351853,
        55.241935);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2182, 152, 1529280000, 1527120000, 1842652800, 4, 'Шершневское водохранилище  пост ГИБДД', 61.402554,
        55.159897);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2183, 153, 1529280000, 1527120000, 1842652800, 4, 'ул. Худякова  напротив д. 22к1', 61.329, 55.146013);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2184, 154, 1529280000, 1527120000, 1842652800, 4, 'пересеч. Копейского шоссе и ул. Харлова  2а', 61.443607,
        55.147376);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2185, 155, 1529539200, 1526083200, 1683763200, 12, 'ул.Федорова 1А', 61.40259, 55.134785);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2186, 166, 1532476800, 1526083200, 1683763200, 12, 'пр.Ленина 26/1', 61.430374, 55.161527);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2187, 167, 1533081600, 1526083200, 1683763200, 12, 'пересечение ул.Тухачевского и а/д Меридиан', 61.426763,
        55.147618);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2188, 168, 1533081600, 1526083200, 1683763200, 12, 'ул.Новороссийская 122/1', 61.441091, 55.116917);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2189, 169, 1533081600, 1526083200, 1683763200, 12, 'пересечение ул.Блюхера и ул.Гоголя', 61.372689, 55.134502);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2190, 170, 1533081600, 1526083200, 1683763200, 12, 'пересечение ул.Труда и ул. Энгельса', 61.380522, 55.16764);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2191, 171, 1535932800, 1534982400, 1692662400, 11, 'ул.Артиллерийская  136 (у входа со стороны а/д Меридиан)',
        61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2192, 172, 1535932800, 1534982400, 1692662400, 11,
        'ул.Артиллерийская  136 (у входа со стороны ул. Артиллерийская)', 61.430922, 55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2193, 173, 1535932800, 1534982400, 1692662400, 12, 'ул.Артиллерийская  136 (со стороны ул. Ловина)', 61.430922,
        55.163507);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2194, 174, 1535932800, 1534982400, 1692662400, 12, 'ул.Свободы 78', 61.412273, 55.157757);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2195, 175, 1535932800, 1534982400, 1692662400, 17, 'Троицкий тр. 11', 61.386249, 55.109755);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2196, 176, 1535932800, 1534982400, 1692662400, 17, 'Троицкий тр. 52', 61.387552, 55.112402);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2197, 177, 1535932800, 1534982400, 1692662400, 12, 'пр.Ленина 2а/2 (район главной проходной ЧТЗ)', 61.476108,
        55.159717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2198, 178, 1536105600, 1534982400, 1692662400, 12, 'ул. Труда  166', 61.374562, 55.170784);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2199, 180, 1536105600, 1534982400, 1692662400, 9,
        'ул.Молодогвардейцев  поворот к дому № 109 по ул.Бр. Кашириных', 61.332593, 55.191264);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2200, 181, 1536192000, 1534982400, 1692662400, 15, 'ул.Танкистов 177А/2 поз.1', 61.471517, 55.170624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2201, 182, 1536192000, 1534982400, 1692662400, 15, 'ул.Танкистов 177А/2 поз.2', 61.471517, 55.170624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2202, 183, 1536192000, 1534982400, 1692662400, 15, 'ул.Танкистов 177А/2 поз.3', 61.471517, 55.170624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2203, 184, 1536192000, 1534982400, 1692662400, 15, 'ул.Танкистов 177А/2 поз.4', 61.471517, 55.170624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2204, 185, 1536192000, 1534982400, 1692662400, 15, 'ул.Танкистов 177А/2 поз.5', 61.471517, 55.170624);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2205, 186, 1536192000, 1534982400, 1692662400, 19, 'пересечение ш.Металлургов и ул.Строительная', 61.40959,
        55.253976);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2206, 187, 1536537600, 1534982400, 1692662400, 19, 'пересечение ул. Жукова и ул.Социалистическая', 61.391244,
        55.252894);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2207, 188, 1536537600, 1534982400, 1692662400, 19, 'ул.Агалакова 38', 61.440777, 55.134003);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2208, 189, 1536537600, 1534982400, 1692662400, 19, 'ул.Чичерина  напротив д.15', 61.290183, 55.184901);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2209, 190, 1536537600, 1534982400, 1692662400, 19, 'ул.Бажова 46', 61.452698, 55.18323);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2210, 191, 1536537600, 1534982400, 1692662400, 19, 'ул.Героев Танкограда 37', 61.44711, 55.180444);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2211, 192, 1536537600, 1534982400, 1692662400, 9, 'пересечение пр. Ленина и ул. Свободы', 61.411447, 55.160654);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2212, 193, 1536537600, 1534982400, 1692662400, 9, 'пересечение пр.Ленина  61 и ул.Васенко', 61.394855,
        55.159794);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2213, 194, 1536537600, 1534982400, 1692662400, 9, 'пересечение пр.Ленина  45 и ул.Свободы', 61.410234,
        55.160278);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2214, 195, 1536537600, 1534982400, 1692662400, 9, 'пересечение пр.Ленина  53 и ул.Цвиллинга', 61.40568,
        55.160108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2215, 196, 1536537600, 1534982400, 1692662400, 9, 'пересечение пр.Ленина  43 и ул.Свободы', 61.412633,
        55.160324);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2216, 197, 1536624000, 1534982400, 1692662400, 12, 'пр. Победы  124', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2217, 198, 1536624000, 1534982400, 1692662400, 8, 'пр. Победы  124  поз. 1', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2218, 199, 1536624000, 1534982400, 1692662400, 8, 'пр. Победы  124  поз. 2', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2219, 200, 1536624000, 1534982400, 1692662400, 8, 'пр. Победы  124  поз. 3', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2220, 201, 1536624000, 1534982400, 1692662400, 8, 'пр. Победы  124  поз. 4', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2221, 202, 1536624000, 1534982400, 1692662400, 12, 'ул.Бр.Кашириных 147  поз.1', 61.296382, 55.168526);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2222, 203, 1536624000, 1534982400, 1692662400, 12, 'ул.Бр.Кашириных 147  поз.2', 61.296382, 55.168526);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2223, 204, 1536624000, 1534982400, 1692662400, 15, 'ул. Лесопарковая 6Астр.', 61.359578, 55.149403);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2224, 205, 1536710400, 1534982400, 1692662400, 15, 'ул.Татищева 264  поз. 1', 61.283356, 55.172717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2225, 206, 1536710400, 1534982400, 1692662400, 15, 'ул.Татищева 264  поз. 2', 61.283356, 55.172717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2226, 207, 1536710400, 1534982400, 1692662400, 15, 'ул.Татищева 264  поз. 3', 61.283356, 55.172717);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2227, 208, 1536710400, 1534982400, 1692662400, 15, 'ул.Аношкина 12 поз. 1', 61.28624, 55.193283);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2228, 209, 1536710400, 1534982400, 1692662400, 15, 'ул.Аношкина 12 поз. 2', 61.28624, 55.193283);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2229, 210, 1536710400, 1534982400, 1692662400, 15, 'ул.Аношкина 12 поз. 3', 61.28624, 55.193283);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2230, 211, 1536710400, 1534982400, 1692662400, 12, 'ул.40 летия Победы 4Б', 61.284578, 55.191279);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2231, 229, 1544745600, 1542931200, 1700611200, 12, 'ул.Бр.Кашириных 75', 61.376197, 55.175247);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2232, 230, 1544745600, 1542931200, 1700611200, 17, 'ул.Бр.Кашириных 75 поз.1', 61.376197, 55.175247);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2233, 231, 1544745600, 1542931200, 1700611200, 17, 'ул.Бр.Кашириных 75 поз.2', 61.376197, 55.175247);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2234, 232, 1544745600, 1542931200, 1700611200, 15, 'ул.Бр.Кашириных 75 поз.1', 61.376197, 55.175247);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2235, 233, 1544745600, 1542931200, 1700611200, 15, 'ул.Бр.Кашириных 75 поз.2', 61.376197, 55.175247);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2236, 241, 1545782400, 1545436800, 1703116800, 6, 'ул. Татищева  262', 61.281847, 55.172707);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2237, 243, 1545868800, 1545436800, 1703116800, 12, 'Комсомольский пр.  48', 61.334515, 55.195015);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2238, 244, 1545868800, 1545436800, 1860969600, 7, 'ул. Худякова  31', 61.367115, 55.147104);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2239, 245, 1545868800, 1545436800, 1860969600, 7,
        'пересечение ул. Братьев Кашириных (из центра) и ул. Молодогвардейцев  57/1', 61.333518, 55.179483);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2240, 246, 1546041600, 1545696000, 1703376000, 9, 'ул. Коммуны  58  поз. 6', 61.262443, 55.075128);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2241, 247, 1546041600, 1545436800, 1703116800, 12, 'ул. Блюхера/ ул. Кузнецова  1А', 61.360746, 55.117504);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2242, 248, 1546041600, 1545436800, 1703116800, 12, 'ул. Блюхера  напротив д. 59', 61.368319, 55.130899);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2243, 1, 1546992000, 1545696000, 1703376000, 17, 'ул. Харлова  ООТ "КБС"  из центра', 61.439986, 55.144799);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2244, 2, 1546992000, 1545696000, 1703376000, 17, 'Свердловский тр.  ООТ "Лакокрасочный завод"  в город',
        61.368543, 55.232797);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2245, 4, 1546992000, 1545696000, 1703376000, 17, 'Копейское ш.  58  в центр  ООТ "Профилакторий"', 61.453084,
        55.142195);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2246, 5, 1546992000, 1545696000, 1703376000, 17, 'Копейское ш.  64/1  ООТ "Профилакторий"', 61.451979,
        55.145442);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2247, 6, 1546992000, 1545696000, 1703376000, 17, 'ул. Енисейская  1  ООТ "Профилакторий"', 61.454189,
        55.142422);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2248, 10, 1546992000, 1545696000, 1861228800, 7, 'пересечение Свердловского пр.  28А и пр. Победы', 61.385953,
        55.183225);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2249, 11, 1546992000, 1545696000, 1861228800, 7, 'пересечение Свердловского пр. и ул. Братьев Кашириных',
        61.387633, 55.174126);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2250, 12, 1546992000, 1545696000, 1861228800, 7, 'ул. Доватора  8', 61.405033, 55.138346);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2251, 13, 1546992000, 1545696000, 1861228800, 7, 'пересечение ул. Братьев Кашириных 126/1 и ул. Чичерина',
        61.30311, 55.173627);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2252, 14, 1546992000, 1545696000, 1861228800, 7,
        'Блюхера (в центр) 50 м от пересечения с ул. Мебельная (слева)', 61.364705, 55.125395);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2253, 15, 1546992000, 1545696000, 1861228800, 4, 'ул. Худякова (из центра)  поворот на городской пляж',
        61.356892, 55.147171);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2254, 16, 1546992000, 1545696000, 1703376000, 5, 'пр. Ленина  Комсомольская площадь  за остановкой  из центра',
        61.441271, 55.160108);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2255, 17, 1546992000, 1545696000, 1703376000, 6, 'пересечение пр. Победы и ул. Северо-Крымская', 61.363994,
        55.186599);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2256, 18, 1546992000, 1545696000, 1703376000, 6, 'ул. Худякова (в центр)  700 м до ул. Лесопарковая', 61.365391,
        55.147419);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2257, 19, 1546992000, 1545696000, 1703376000, 6, 'ул. Братьев Кашириных  300 м от ул. Косарева в центр за АЗС',
        61.372504, 55.178107);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2258, 20, 1546992000, 1545696000, 1703376000, 6,
        'пересечение ул. Северо-Крымская и ул. Университетская набережная', 61.358318, 55.1756);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2259, 21, 1546992000, 1545696000, 1703376000, 6, 'пересечение ул. Цвиллинга  64 и ул. Перовской', 61.411456,
        55.148169);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2260, 22, 1546992000, 1545696000, 1703376000, 6, 'пересечение ул. Героев Танкограда (в центр) и ул. Валдайская',
        61.440319, 55.208098);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2261, 23, 1546992000, 1545696000, 1703376000, 6, 'ул. Худякова (в центр)  1000 м до ул. Лесопарковая',
        61.365391, 55.147419);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2262, 24, 1546992000, 1545696000, 1703376000, 6, 'Комсомольский пр.  41 (конец дома)', 61.338432, 55.194147);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2263, 25, 1546992000, 1545696000, 1703376000, 6, 'Комсомольский пр.  47 (начало дома)', 61.329035, 55.194085);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2264, 26, 1546992000, 1545696000, 1703376000, 6, 'ул. Худякова (в центр)  1280 м до ул. Лесопарковая',
        61.356892, 55.147171);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2265, 27, 1546992000, 1545696000, 1703376000, 6, 'Комсомольский пр.  28 (конец дома)', 61.352859, 55.195061);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2266, 28, 1546992000, 1545696000, 1703376000, 6, 'ул. Худякова (в центр)  1410 м до ул. Лесопарковая',
        61.356892, 55.147171);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2267, 29, 1546992000, 1545696000, 1703376000, 6,
        'пересечение Комсомольского пр (в центр) и ул. Косарева  за остановкой', 61.371654, 55.19185);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2268, 30, 1546992000, 1545696000, 1703376000, 6, 'ул. Энгельса  52', 61.380042, 55.149583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2269, 31, 1546992000, 1545696000, 1703376000, 6, 'пересечение ул. Братьев Кашириных  114 и ул. 40 лет Победы',
        61.308985, 55.176331);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2270, 32, 1546992000, 1545696000, 1703376000, 6, 'ул. Худякова (в центр)  1680 м до ул. Лесопарковая',
        61.356892, 55.147171);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2271, 33, 1546992000, 1545696000, 1703376000, 6, 'Свердловский пр.  7', 61.387875, 55.189933);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2272, 34, 1546992000, 1545696000, 1703376000, 6, 'Комсомольский пр.  72', 61.316037, 55.194979);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2273, 35, 1547078400, 1545696000, 1861228800, 7, 'Комсомольский пр.  69', 61.315713, 55.194013);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2274, 36, 1547078400, 1545696000, 1861228800, 7, 'пересечение Комсомольского пр.  86В и ул. Молдавская',
        61.30258, 55.194635);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2275, 37, 1547078400, 1545436800, 1860969600, 7, 'ул. Университетская Набережная 30', 61.349355, 55.175072);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2276, 38, 1547078400, 1545436800, 1860969600, 7, 'пр. Ленина  69', 61.387767, 55.15945);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2277, 39, 1547078400, 1545436800, 1703116800, 15, 'Уфимский тракт  123/2 (11 км + 200)', 61.33792, 55.057234);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2278, 40, 1547078400, 1545696000, 1703376000, 6, 'Троицкий тр.  21/7 (поворот на станцию "Челябинск-Грузовой")',
        61.384749, 55.091046);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2279, 41, 1547078400, 1545436800, 1703116800, 9, 'ул. Бейвеля/ ул. Скульптора Головницкого  12', 61.284973,
        55.208548);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2280, 42, 1547078400, 1545436800, 1703116800, 12, 'ул. Молодогвардейцев  2', 61.321822, 55.204233);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2281, 43, 1547078400, 1545696000, 1703376000, 6, 'ул. Хлебозаводская  7а', 61.420322, 55.242997);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2282, 44, 1547164800, 1545696000, 1861228800, 12, 'ул. Труда  185А', 61.373799, 55.169066);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2283, 45, 1547164800, 1545696000, 1703376000, 15, 'Копейское шоссе  1-г', 61.51328, 55.121088);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2284, 46, 1547164800, 1545436800, 1703116800, 5, 'пр. Ленина  напротив д. 26/1', 61.430374, 55.161527);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2285, 47, 1547164800, 1545436800, 1703116800, 6, 'Комсомольский пр.  9', 61.384542, 55.191521);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2286, 48, 1547510400, 1545436800, 1703116800, 12, 'Троицкий тр.  25 к.3', 61.387471, 55.087862);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2287, 49, 1547510400, 1545436800, 1860969600, 4, 'пр. Героя России Е. Родионова  2', 61.276834, 55.174645);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2288, 50, 1547510400, 1545696000, 1703376000, 12, 'ул. Новороссийская  44', 61.47697, 55.109889);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2289, 51, 1547596800, 1545436800, 1703116800, 12, 'ул. Елькина  76а', 61.397676, 55.155736);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2290, 52, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.1', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2291, 53, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.2', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2292, 54, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.3', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2293, 55, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.4', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2294, 56, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.5', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2295, 57, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.6', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2296, 58, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.7', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2297, 59, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.8', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2298, 60, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.9', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2299, 61, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.10', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2300, 62, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.11', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2301, 63, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.12', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2302, 64, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.13', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2303, 65, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.14', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2304, 66, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.15', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2305, 67, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.16', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2306, 68, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.17', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2307, 69, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.18', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2308, 70, 1549411200, 1524960000, 1682640000, 15, 'ул.Дарвина 18  поз.19', 61.370106, 55.126848);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2309, 73, 1549584000, 1538352000, 1696032000, 12, 'пересечение ул. Бажова и  пер. Лермонтова', 61.454532,
        55.18298);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2310, 74, 1549584000, 1545696000, 1703376000, 6, 'Комсомольский пр.  10', 61.379871, 55.192584);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2311, 75, 1549584000, 1545696000, 1703376000, 6, 'Комсомольский пр.  26', 61.357207, 55.194609);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2312, 76, 1549584000, 1545696000, 1703376000, 6, 'ул. Братьев Кашириных  107б', 61.336078, 55.178162);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2313, 77, 1549584000, 1545696000, 1703376000, 6, 'ул. Братьев Кашириных  напротив д. 101', 61.344208,
        55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2314, 80, 1549584000, 1545696000, 1703376000, 6, 'ул. Чичерина  23', 61.295286, 55.181179);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2315, 81, 1549584000, 1545696000, 1703376000, 6, 'ул. Братьев Кашириных  101', 61.344208, 55.178115);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2316, 82, 1549584000, 1545696000, 1703376000, 6, 'автодорога Меридиан  напротив рынка "порт Артур"', 61.420044,
        55.135583);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2317, 85, 1549584000, 1545696000, 1703376000, 12, 'ул. Енисейская  1', 61.454189, 55.142422);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2318, 88, 1549584000, 1545696000, 1861228800, 7, 'пересечение ул. Энгельса  95 и ул. Курчатова', 61.380877,
        55.152377);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2319, 89, 1549584000, 1545696000, 1861228800, 7, 'Комсомольского пр./ ул. Чайковского  15', 61.363935,
        55.191135);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2320, 90, 1549584000, 1545696000, 1861228800, 7,
        'Копейское шоссе и ул. Енисейская  разд. газон напротив ТК "Алмаз"', 61.451898, 55.142685);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2321, 91, 1549584000, 1545436800, 1703116800, 12, 'ул. Новоэлеваторная/ ул. Блюхера  121Е', 61.352419,
        55.104728);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2322, 92, 1549584000, 1545436800, 1703116800, 12, 'ул. Болейко/ пр. Победы  160В', 61.40488, 55.18671);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2323, 93, 1549584000, 1545436800, 1703116800, 8,
        'пересечение ул. Братьев Кашириных и ул. Академика Королева 28', 61.296175, 55.16414);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2324, 94, 1549584000, 1545436800, 1703116800, 12, 'ул. Батумская  20', 61.465912, 55.116928);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2325, 95, 1549584000, 1545436800, 1703116800, 12, 'ул. Героев Танкограда  55', 61.449077, 55.178193);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2326, 96, 1549584000, 1545436800, 1703116800, 12, 'ул. Героев Танкограда  57', 61.449706, 55.1772);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2327, 97, 1549584000, 1545436800, 1703116800, 12, 'ул. Молодогвардейцев  12', 61.329188, 55.199254);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2328, 99, 1549843200, 1545436800, 1703116800, 15, 'пр. Ленина  73', 61.380302, 55.159393);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2329, 100, 1549929600, 1545696000, 1703376000, 8, 'пр. Победы  124  поз. 7', 61.431102, 55.186433);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2330, 103, 1552953600, 1550188800, 1643673600, 7, 'ул.Молодогвардейцев 7', 61.327311, 55.204793);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2331, 104, 1554249600, 1551398400, 1709164800, 6, 'ул. Енисейская  8', 61.458878, 55.146708);
INSERT INTO tx_permit (id, permit, issuing_at, start, finish, tx_type_id, address, longitude, latitude)
VALUES (2332, 106, 1554249600, 1476057600, 1791504000, 12, 'ул.Дарвина 18', 61.370106, 55.126848);