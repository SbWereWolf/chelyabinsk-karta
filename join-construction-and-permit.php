<?php

require_once 'D:\project\chelyabinsk-karta\vendor\autoload.php';

try {
    $connection = new PDO('mysql:host=localhost;dbname=scheme', 'root', 'admin');
    $isSuccess = true;
} catch (Exception $e) {
    $isSuccess = false;
}
if ($connection === null) {
    echo 'не могу соединиться с базой' . PHP_EOL;
}
$allowance = 0.00001;
$increment = 0.00001;
while ($allowance < 0.003) {
    $allowance += $increment;
    /**/

    $export = null;
    if ($connection !== null) {
        $export = $connection->prepare("
select 
       pt.id
from 
     tx_permit pt
     left join tx_permit_scheme_construction tx
     on pt.id = tx.tx_permit_id
where 
        tx.tx_permit_id is null
        AND exists(select null
             from tx_scheme_construction sc
        left join tx_permit_scheme_construction tx
        on sc.uid = tx.tx_scheme_construction_uid
             where 
                   tx.tx_scheme_construction_uid is null
               and sc.type = pt.tx_type_id
               and sc.x between pt.longitude - $allowance 
                                and pt.longitude + $allowance
               and sc.y between pt.latitude - $allowance 
                                    and pt.latitude + $allowance
    ) 
");
        $isSuccess = $export !== false;
    }
    $calculation = null;
    if ($isSuccess) {
        $calculation = $connection->prepare("
select 
    (select sc.uid
        from tx_scheme_construction sc
        left join tx_permit_scheme_construction tx
        on sc.uid = tx.tx_scheme_construction_uid
    where 
          tx.tx_scheme_construction_uid is null
      and sc.type = pt.tx_type_id
      and sc.x
        between pt.longitude - $allowance and pt.longitude + $allowance
      and sc.y
        between pt.latitude - $allowance and pt.latitude + $allowance
      and SQRT((sc.x - pt.longitude) * (sc.x - pt.longitude) +
               (sc.y - pt.latitude) * (sc.y - pt.latitude)) =
          (
              select min(
                  SQRT((sc.x - pt.longitude) 
                                  * (sc.x - pt.longitude) +
                              (sc.y - pt.latitude) 
                                  * (sc.y - pt.latitude))
                  )
                  from tx_scheme_construction sc
                  left join tx_permit_scheme_construction tx
                  on sc.uid = tx.tx_scheme_construction_uid
              where 
                    tx.tx_scheme_construction_uid is null
                and sc.type = pt.tx_type_id
                and sc.x
                  between pt.longitude - $allowance 
                          and pt.longitude + $allowance
                and sc.y
                  between pt.latitude - $allowance 
                              and pt.latitude + $allowance
          )
    ORDER BY sc.uid LIMIT 1
    ) as uid,
    (
        select
               SQRT((sc.x - pt.longitude) * (sc.x - pt.longitude) +
                   (sc.y - pt.latitude) * (sc.y - pt.latitude))
        from tx_scheme_construction sc
                  left join tx_permit_scheme_construction tx
                  on sc.uid = tx.tx_scheme_construction_uid
        where
                  tx.tx_scheme_construction_uid is null
              and sc.type = pt.tx_type_id
              and sc.x
                between pt.longitude - $allowance and pt.longitude 
                                            + $allowance
              and sc.y
                between pt.latitude - $allowance and pt.latitude 
                                                + $allowance
              and (
                  select min(SQRT((sc.x - pt.longitude) 
                                      * (sc.x - pt.longitude) +
                                  (sc.y - pt.latitude) 
                                      * (sc.y - pt.latitude)))
                  from 
                       tx_scheme_construction sc
                        left join tx_permit_scheme_construction tx
                        on sc.uid = tx.tx_scheme_construction_uid
                  where
                      tx.tx_scheme_construction_uid is null
                      and sc.type = pt.tx_type_id
                      and sc.x
                        between pt.longitude - $allowance 
                              and pt.longitude + $allowance
                      and sc.y
                        between pt.latitude - $allowance 
                                  and pt.latitude + $allowance
              ) = SQRT((sc.x - pt.longitude) * (sc.x - pt.longitude) +
                   (sc.y - pt.latitude) * (sc.y - pt.latitude))
        ORDER BY sc.uid LIMIT 1
    ) as distance       
from 
     tx_permit pt
     left join tx_permit_scheme_construction tx
     on pt.id = tx.tx_permit_id
where 
          pt.id = ?  
      AND tx.tx_permit_id is null
      AND exists(
          select null
          from 
                tx_scheme_construction sc
                left join tx_permit_scheme_construction tx
                on sc.uid = tx.tx_scheme_construction_uid
          where 
                tx.tx_scheme_construction_uid is null
                and sc.type = pt.tx_type_id
                and sc.x between pt.longitude - $allowance
                    and pt.longitude + $allowance
                and sc.y between pt.latitude - $allowance
                    and pt.latitude + $allowance
    ) 
");
        $isSuccess = $calculation !== false;
    }
    $import = null;
    if ($isSuccess) {
        $import = $connection->prepare("
insert into tx_permit_scheme_construction
    (tx_permit_id, tx_scheme_construction_uid, allowance,distance)
    value (?, ?, $allowance,?)
");
        $isSuccess = $export !== false;
    }
    if ($isSuccess) {
        $command = $connection->exec('SET NAMES \'utf8mb4\''
            . ' COLLATE \'utf8mb4_unicode_ci\'');
        $isSuccess = $command !== false;
    }
    if ($isSuccess) {
        $command = $connection->exec('START TRANSACTION');
        $isSuccess = $command !== false;
    }
    if ($isSuccess) {
        $command = $connection->exec('SET AUTOCOMMIT = OFF');
        $isSuccess = $command !== false;
    }
    if ($isSuccess) {
        $isSuccess = $export->execute();
    }
    $permits = [];
    if ($isSuccess) {
        $permits = $export->fetchAll(PDO::FETCH_ASSOC);
        $isSuccess = !empty($permits);
    }
    $joined = 0;
    foreach ($permits as $permit) {

        $isSuccess = $calculation->execute([$permit['id']]) !== false;
        if (!$isSuccess) {
            echo var_export($permit, true) . PHP_EOL;
            $error = $calculation->errorInfo();
            echo var_export($error, true) . PHP_EOL;
        }
        $target = [];
        if ($isSuccess) {
            $target = $calculation->fetch(PDO::FETCH_ASSOC);
            $calculation->closeCursor();
        }
        $hasTarget = !empty($target);
        if ($hasTarget) {
            $isSuccess = $import->execute([$permit['id'], $target['uid'],
                    $target['distance']]) !== false;
        }
        if (!$isSuccess) {
            echo var_export([$permit['id'], $target['uid'],
                    $target['distance']], true)
                . PHP_EOL;
            $error = $import->errorInfo();
            echo $error . PHP_EOL;
        }
        if (!$isSuccess) {
            /** @noinspection PhpUnusedLocalVariableInspection */
            $command = $connection->exec('ROLLBACK');
            break;
        }
        $joined++;
    }
    if ($isSuccess) {
        $command = $connection->exec('COMMIT');
    }
    if ($connection !== false) {
        $command = $connection->exec('SET AUTOCOMMIT = ON');
    }

    $side = sqrt($allowance * 111000 * $allowance * 111000);
    echo "Точность {$side} метров,точек соединено $joined" . PHP_EOL;

    /**/
}

