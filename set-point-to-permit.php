<?php

require_once 'D:\project\chelyabinsk-karta\vendor\autoload.php';

try {
    $connection = new PDO('mysql:host=localhost;dbname=scheme', 'root', 'admin');
    $isSuccess = true;
} catch (Exception $e) {
    $isSuccess = false;
}
if ($connection === null) {
    echo 'не могу соединиться с базой' . PHP_EOL;
}
$export = null;
if ($connection !== null) {
    $export = $connection->prepare('
select rp.permit,
       rp.issuing_at,
       rp.start,
       rp.finish,
       rp.type,
       ps.request,
       ps.longitude,
       ps.latitude
from raw_permit rp
     join point_suggestion ps
     on rp.id = ps.raw_permit_id 
');
    $isSuccess = $export !== false;
}
$import = null;
if ($isSuccess) {
    $import = $connection->prepare('
insert into tx_permit (permit, issuing_at, start, finish, tx_type_id, 
                       address, longitude, latitude)
    value (?, ?, ?, ?, ?, ?, ?, ?)
');
    $isSuccess = $export !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET NAMES \'utf8mb4\''
        . ' COLLATE \'utf8mb4_unicode_ci\'');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('START TRANSACTION');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET AUTOCOMMIT = OFF');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $isSuccess = $export->execute();
}
$suggestions = [];
if ($isSuccess) {
    $suggestions = $export->fetchAll(PDO::FETCH_ASSOC);
    $isSuccess = !empty($suggestions);
}
/**
 * @param $date
 * @return int
 */
function toUnixTime($date): int
{
    $hasDot = strpos($date, '.') !== false;
    $format = 'm/d/Y H:i:s O';
    if ($hasDot) {
        $format = 'd.m.Y H:i:s O';
    }
    try {
        $unixTime = DateTimeImmutable::createFromFormat($format,
            "$date 00:00:00 +00:00")->getTimestamp();
    } catch (Exception $e) {
        $unixTime = 0;
    }

    return $unixTime;
}

foreach ($suggestions as $point) {
    $permit = $point['permit'];
    $issuingAt = $point['issuing_at'];
    $start = $point['start'];
    $finish = $point['finish'];
    $type = $point['type'];
    $request = $point['request'];
    $longitude = $point['longitude'];
    $latitude = $point['latitude'];

    $request = str_replace('город Челябинск ', '', $request);
    $issuingUtc = toUnixTime($issuingAt);
    $startUtc = toUnixTime($start);
    $finishUtc = toUnixTime($finish);

    $typeId = 0;
    switch ($type) {
        case 'ЩУ арочного типа':
            $typeId = 1;
            break;
        case 'транспарант-перетяжка':
            $typeId = 2;
            break;
        case '105 суперсайт':
        case '120, суперсайт':
        case '144 супер':
        case '150 супер':
        case '225 суперсайт':
        case '60 супер':
        case '72 суперсайт':
        case '72, суперсайт':
        case '75 супер':
        case '75, суперсайт':
        case '96 кв., суперсайт':
        case '96 суперсайт':
            $typeId = 4;
            break;
        case 'сити-борд':
            $typeId = 5;
            break;
        case 'щитовая установка':
            $typeId = 6;
            break;
        case 'Экран':
        case 'Экран на здании':
            $typeId = 7;
            break;
        case 'сити-формат (в составе ост.комп.)':
        case 'сити-формат':
            $typeId = 8;
            break;
        case 'панель-кронштейн':
        case 'панель-кронштейн на собст.опоре':
            $typeId = 9;
            break;
        case 'Тумба':
            $typeId = 11;
            break;
        case 'стела':
        case 'Стела':
            $typeId = 12;
            break;
        case 'Стенд':
        case 'стенд':
            $typeId = 14;
            break;
        case 'Флаги':
        case 'Флаги ':
        case 'флаги':
            $typeId = 15;
            break;
        case 'Рекл. на скамье':
        case 'Скамья':
            $typeId = 16;
            break;
        case 'РК (ост.компл.)':
        case 'Рекл. на ост.навесе':
        case 'нестанд.рекл.констр.':
            $typeId = 17;
            break;
        case 'указатель':
        case 'Указатель (с/о)':
            $typeId = 19;
            break;
        default:
            $typeId = 0;
    }

    $stat = $import->execute([$permit, $issuingUtc, $startUtc,
        $finishUtc, $typeId, $request, $longitude, $latitude]);
    $isSuccess = $stat !== false;

    if (!$isSuccess) {
        echo var_export([$permit, $issuingUtc, $startUtc, $finishUtc,
                $typeId, $request, $longitude, $latitude], true)
            . PHP_EOL;
        $error = $import->errorInfo();
        echo $error . PHP_EOL;
        /** @noinspection PhpUnusedLocalVariableInspection */
        $command = $connection->exec('ROLLBACK');
    }

    if (!$isSuccess) {
        break;
    }
}
if ($isSuccess) {
    $command = $connection->exec('COMMIT');
}
if ($connection !== false) {
    $command = $connection->exec('SET AUTOCOMMIT = ON');
}

