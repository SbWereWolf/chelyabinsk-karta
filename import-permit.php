<?php

$filename = $argv[1];
$file = fopen($filename, 'r');

$isSuccess = $data = fgetcsv($file);
if (!$isSuccess) {
    echo 'не могу прочитать файл импорта' . PHP_EOL;
}
$connection = null;
if ($isSuccess) {
    try {
        $connection = new PDO('mysql:host=localhost;dbname=scheme', 'root', 'admin');
    } catch (Exception $e) {
        $isSuccess = false;
    }
}
if ($connection === null) {
    echo 'не могу соединиться с базой' . PHP_EOL;
}
$query = null;
if ($connection !== null) {
    $query = $connection->prepare('
insert into raw_permit
(permit,issuing_at,start,finish,distributor,type,remark)
select ?,?,?,?,?,?,?
');
    $isSuccess = $query !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET NAMES \'utf8mb4\' COLLATE \'utf8mb4_unicode_ci\'');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('START TRANSACTION');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET AUTOCOMMIT = OFF');
    $isSuccess = $command !== false;
}
while ($isSuccess && ($data = fgetcsv($file))) {

    $response = $query->execute($data);
    $isSuccess = $response !== false;

    if (!$isSuccess) {
        $error = $query->errorInfo();
        $command = $connection->exec('ROLLBACK');
    }
}
if ($isSuccess) {
    $command = $connection->exec('COMMIT');
}
if ($connection !== false) {
    $command = $connection->exec('SET AUTOCOMMIT = ON');
}
fclose($file);
