INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3, 1, 61.395933, 55.167313, 'Россия, Челябинск, улица Труда, 153', 'город Челябинск ул.Труда 153', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (4, 2, 61.39622, 55.197384, 'Россия, Челябинск, Кожзаводская улица, 108А',
        'город Челябинск ул.Новомеханическая напротив дома по адресу:ул.Кожзаводская 108А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (5, 3, 61.411474, 55.196038, 'Россия, Челябинск, Российская улица, 13Б',
        'город Челябинск ул.Новомеханическая напротив дома по адресу:ул.Российская 13Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (6, 4, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск автодорога Меридиан напротив здания по адресу:ул.Артиллерийская 136', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (7, 5, 61.432584, 55.159702, 'Россия, Челябинск, Артиллерийский переулок, 3',
        'город Челябинск автодорога Меридиан напротив здания по адресу:ул.Артиллерийский пер. д.3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (8, 6, 61.43722, 55.151132, 'Россия, Челябинск, улица Луценко, 2/2',
        'город Челябинск автодорога Меридиан напротив здания по адресу:ул.Луценко 2/2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (9, 7, 61.30302, 55.15893, 'Россия, Челябинск, улица Университетская Набережная, 94',
        'город Челябинск ул.Университетская Набережная 94', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (10, 8, 0, 0, '', 'город Челябинск автодорога Меридиан ост. "Сады"', 'город Челябинск ост. "Сады"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (13, 9, 61.441765, 55.189239, 'Россия, Челябинск, улица Героев Танкограда, 21/1',
        'город Челябинск ул.Героев Танкограда 21/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (14, 10, 61.375182, 55.158637, 'Россия, Челябинск, улица Энтузиастов, 6', 'город Челябинск ул.Энтузиастов 6',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (16, 11, 61.382854, 55.119651, 'Россия, Челябинск, улица Дарвина, 2В', 'город Челябинск ул.Дарвина 2В', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (17, 12, 61.382854, 55.119651, 'Россия, Челябинск, улица Дарвина, 2В', 'город Челябинск ул.Дарвина 2В', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (18, 13, 61.439807, 55.205425, 'Россия, Челябинск, улица Героев Танкограда, 67П',
        'город Челябинск ул.Героев Танкограда 67П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (19, 14, 61.451081, 55.166372, 'Россия, Челябинск, улица 40-летия Октября, 15',
        'город Челябинск ул.Первой Пятилетки  на уровне дома № 15 по ул.40 лет Октября', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (21, 15, 61.451081, 55.166372, 'Россия, Челябинск, улица 40-летия Октября, 15',
        'город Челябинск ул.Героев Танкограда  на уровне дома № 15 по ул. 40 лет Октября', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (23, 16, 61.368741, 55.132598, 'Россия, Челябинск, улица Блюхера, 44', 'город Челябинск ул.Блюхера 44 поз.1',
        'город Челябинск ул.Блюхера 44', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (25, 17, 61.368741, 55.132598, 'Россия, Челябинск, улица Блюхера, 44', 'город Челябинск ул.Блюхера 44 поз.2',
        'город Челябинск ул.Блюхера 44', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (27, 18, 61.368741, 55.132598, 'Россия, Челябинск, улица Блюхера, 44', 'город Челябинск ул.Блюхера 44 поз.3',
        'город Челябинск ул.Блюхера 44', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (29, 19, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18', 'город Челябинск ул.Дарвина 18 поз.1',
        'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (30, 20, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18', 'город Челябинск ул.Дарвина 18 поз.2',
        'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (31, 21, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18', 'город Челябинск ул.Дарвина 18 поз.3',
        'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (32, 22, 61.36672, 55.129026, 'Россия, Челябинск, улица Блюхера, 67', 'город Челябинск ул.Блюхера 67', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (33, 23, 61.466523, 55.188488, 'Россия, Челябинск, улица Комарова, 34', 'город Челябинск ул.Комарова 34', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (34, 24, 61.466523, 55.188488, 'Россия, Челябинск, улица Комарова, 34', 'город Челябинск ул.Комарова 34', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (35, 25, 61.466523, 55.188488, 'Россия, Челябинск, улица Комарова, 34', 'город Челябинск ул.Комарова 34', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (36, 26, 61.466523, 55.188488, 'Россия, Челябинск, улица Комарова, 34', 'город Челябинск ул.Комарова 34', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (37, 27, 61.46381, 55.184942, 'Россия, Челябинск, улица Комарова, 60', 'город Челябинск пр.Комарова  60',
        'город Челябинск Комарова  60', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (41, 28, 61.366459, 55.115321, 'Россия, Челябинск, улица Блюхера, 97Б',
        'город Челябинск ул.Блюхера  97  200 м до ул.Кузнецова', 'город Челябинск ул.Блюхера  97', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (42, 29, 61.389704, 55.179264, 'Россия, Челябинск, улица Калинина',
        'город Челябинск пересечение ул.Калинина и ул.Тагильская', 'город Челябинск  ул.Калинина / ул.Тагильская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (43, 30, 61.329709, 55.188396, 'Россия, Челябинск, проспект Победы, 289', 'город Челябинск пр.Победы  289', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (44, 31, 61.384219, 55.24097, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 19',
        'город Челябинск ул.50 лет ВЛКСМ напротив д.19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (45, 32, 61.410369, 55.111346, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск пересечение автодороги Меридиан (в центр) и пер. 6-й Целинный',
        'город Челябинск пер. 6-й Целинный / автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (46, 33, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.(в город)  750 м до виадука на п. Новосинеглазово',
        'город Челябинск Троицкий тр. / п. Новосинеглазово', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (47, 34, 61.48607, 55.10692, 'Россия, Челябинск, Новороссийская улица',
        'город Челябинск пересеч.ул.Новороссийская и пер.Бугурусланский',
        'город Челябинск ул.Новороссийская / пер.Бугурусланский', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (48, 35, 61.426521, 55.161435, 'Россия, Челябинск, проспект Ленина, 28Д', 'город Челябинск пр.Ленина 28д', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (50, 36, 61.426521, 55.161435, 'Россия, Челябинск, проспект Ленина, 28Д', 'город Челябинск пр.Ленина 28д', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (52, 37, 61.373834, 55.169375, 'Россия, Челябинск, улица Труда, 185', 'город Челябинск ул.Труда 185/1', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (55, 38, 61.346939, 55.192975, 'Россия, Челябинск, Курчатовский район, Пионерская улица, 4',
        'город Челябинск ул.Пионерская 4 поз.2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (57, 39, 61.386393, 55.168634, 'Россия, Челябинск, Свердловский проспект, 40А',
        'город Челябинск Свердловский пр. 40а поз.1', 'город Челябинск Свердловский пр. 40а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (59, 40, 61.386393, 55.168634, 'Россия, Челябинск, Свердловский проспект, 40А',
        'город Челябинск Свердловский пр. 40а поз.2', 'город Челябинск Свердловский пр. 40а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (61, 41, 61.373799, 55.225183, 'Россия, Челябинск, Свердловский тракт, 12Б',
        'город Челябинск Свердловский тр. 12б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (62, 42, 61.462085, 55.135979, 'Россия, Челябинск, Копейское шоссе, 35',
        'город Челябинск Копейское шоссе 35д поз.2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (63, 43, 61.375469, 55.154167, 'Россия, Челябинск, улица Энтузиастов, 18', 'город Челябинск ул.Энтузиастов 18',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (65, 44, 61.381776, 55.141634, 'Россия, Челябинск, улица Блюхера, 10', 'город Челябинск ул.Блюхера 10', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (67, 45, 61.508088, 55.122653, 'Россия, Челябинск, Копейское шоссе, 12', 'город Челябинск Копейское шоссе 14',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (68, 46, 61.503542, 55.122267, 'Россия, Челябинск, Копейское шоссе, 9', 'город Челябинск Копейское шоссе 9',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (69, 47, 61.385863, 55.175493, 'Россия, Челябинск, Свердловский проспект, 32',
        'город Челябинск Свердловский пр. 32', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (70, 48, 61.352032, 55.179226, 'Россия, Челябинск, улица Чайковского, 60', 'город Челябинск ул.Чайковсокго 60',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (71, 49, 61.390786, 55.151528, 'Россия, Челябинск, улица Воровского, 21',
        'город Челябинск Свердловский пр.  на уровне дома № 21 по ул. Воровского', 'город Челябинск ул. Воровского 21',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (72, 50, 61.386123, 55.094704, 'Россия, Челябинск, Троицкий тракт, 21к1', 'город Челябинск Троицкий тр. 21 к1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (73, 51, 61.386186, 55.203863, 'Россия, Челябинск, Свердловский тракт, 1А/3',
        'город Челябинск Свердловский тр.  1а/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (74, 52, 61.299885, 55.178198, 'Россия, Челябинск, улица 250-летия Челябинска, 17',
        'город Челябинск ул.250 летия Челябинска 17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (75, 53, 61.299104, 55.164582, 'Россия, Челябинск, улица Академика Королёва, 14',
        'город Челябинск ул.Академика Королева 14', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (76, 54, 61.475946, 55.126313, 'Россия, Челябинск, улица Машиностроителей, 6',
        'город Челябинск ул.Машиностроителей 6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (77, 55, 61.401458, 55.274852, 'Россия, Челябинск, 2-я Павелецкая улица, 36',
        'город Челябинск ул.2-я Павелецкая 36', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (78, 56, 61.409435, 55.13621, 'Россия, Челябинск, улица Степана Разина, 1', 'город Челябинск ул.С.Разина 1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (79, 57, 61.413639, 55.140091, 'Россия, Челябинск, улица Степана Разина, 9',
        'город Челябинск ул.Степана Разина 9', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (80, 58, 61.367663, 55.130498, 'Россия, Челябинск, улица Блюхера, 61', 'город Челябинск ул.Блюхера 61', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (81, 59, 61.315713, 55.194013, 'Россия, Челябинск, Комсомольский проспект, 69',
        'город Челябинск Комсомольский пр. 69', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (82, 60, 61.373906, 55.17665, 'Россия, Челябинск, улица Братьев Кашириных, 79',
        'город Челябинск ул.Бр.Кашириных 79', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (83, 61, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 1', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (84, 62, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 2', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (85, 63, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 1', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (86, 64, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 2', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (87, 65, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 3', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (88, 66, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 4', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (89, 67, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Братьев Кашириных 126 поз. 5', 'город Челябинск ул. Братьев Кашириных 126 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (90, 68, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 1', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (92, 69, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 2', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (94, 70, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 1', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (96, 71, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 2', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (98, 72, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 3', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (100, 73, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 4', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (102, 74, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных 126 поз. 5', 'город Челябинск ул. Братьев Кашириных 126 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (104, 75, 61.447011, 55.148302, 'Россия, Челябинск, Копейское шоссе, 82', 'город Челябинск Копейское шоссе 82',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (105, 76, 61.301628, 55.171087, 'Россия, Челябинск, улица Братьев Кашириных, 137',
        'город Челябинск ул.Бр.Кашириных 137 поз.1', 'город Челябинск ул.Бр.Кашириных 137', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (106, 77, 61.301628, 55.171087, 'Россия, Челябинск, улица Братьев Кашириных, 137',
        'город Челябинск ул.Бр.Кашириных 137 поз.2', 'город Челябинск ул.Бр.Кашириных 137', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (107, 78, 61.301628, 55.171087, 'Россия, Челябинск, улица Братьев Кашириных, 137',
        'город Челябинск ул.Бр.Кашириных 137 поз.3', 'город Челябинск ул.Бр.Кашириных 137', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (108, 79, 61.301628, 55.171087, 'Россия, Челябинск, улица Братьев Кашириных, 137',
        'город Челябинск ул.Бр.Кашириных 137 поз.4', 'город Челябинск ул.Бр.Кашириных 137', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (110, 80, 61.371373, 55.158241, 'Россия, Челябинск, проспект Ленина, 83',
        'город Челябинск пр. Ленина  83  автобус. ост. «ЮУрГУ» (в центр)', 'город Челябинск пр. Ленина  83', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (112, 81, 61.382027, 55.15945, 'Россия, Челябинск, проспект Ленина, 71А',
        'город Челябинск пр. Ленина  71а  троллейбус. ост. «Агроинженерная академия» (в центр)',
        'город Челябинск пр. Ленина  71а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (113, 82, 61.381632, 55.160782, 'Россия, Челябинск, улица Энгельса, 63',
        'город Челябинск ул. Энгельса  63  троллейбус. ост. «Агроинженерная академия» (из центра)',
        'город Челябинск ул. Энгельса  63', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (114, 83, 61.37794, 55.147983, 'Россия, Челябинск, улица Худякова, 6',
        'город Челябинск ул. Худякова  6  автобус. ост. «ул. Худякова» (из центра) ',
        'город Челябинск ул. Худякова  6 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (115, 84, 61.37732, 55.147433, 'Россия, Челябинск, улица Худякова, 11',
        'город Челябинск ул. Худякова  11  автобус. ост. «ул. Худякова»  (в центр)', 'город Челябинск ул. Худякова  11',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (116, 85, 61.391379, 55.159671, 'Россия, Челябинск, проспект Ленина, 65',
        'город Челябинск пр. Ленина  65  троллейбус. ост. «Алое Поле» (в центр)', 'город Челябинск пр. Ленина  65', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (117, 86, 61.387857, 55.160967, 'Россия, Челябинск, Свердловский проспект, 64',
        'город Челябинск Свердловский пр.  напротив д. 64  трамвайн. ост. «Алое Поле» (из центра)',
        'город Челябинск Свердловский пр. д. 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (118, 87, 61.387857, 55.160967, 'Россия, Челябинск, Свердловский проспект, 64',
        'город Челябинск Свердловский пр.  напротив д.64  автобус. ост. «Алое Поле» (из центра)',
        'город Челябинск Свердловский пр. д. 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (119, 88, 61.383842, 55.160689, 'Россия, Челябинск, проспект Ленина, 64',
        'город Челябинск пр. Ленина  64д  автобусн. ост. «Алое Поле» (из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (121, 89, 61.40568, 55.160108, 'Россия, Челябинск, проспект Ленина, 53',
        'город Челябинск пр. Ленина  53  трамвайн. ост. «площадь Революции» (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (124, 90, 61.404215, 55.159295, 'Россия, Челябинск, площадь Революции, 1',
        'город Челябинск площадь Революции  1  трамвайн. ост. «площадь Революции» (из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (125, 91, 61.406641, 55.160128, 'Россия, Челябинск, проспект Ленина, 51',
        'город Челябинск пр. Ленина  51  автобусн. ост. «площадь Революции» (из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (128, 92, 61.405698, 55.160828, 'Россия, Челябинск, проспект Ленина, 52',
        'город Челябинск ул.Цвиллинга/ пр. Ленина  52  автобусн. ост. «площадь Революции» (из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (132, 93, 61.388773, 55.155129, 'Россия, Челябинск, Свердловский проспект, 84',
        'город Челябинск пр. Свердловский  84  троллейбусн. ост. «ул. Южная» (из центра)',
        'город Челябинск пр. Свердловский  84', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (133, 94, 61.386132, 55.150674, 'Россия, Челябинск, улица Воровского, 30',
        'город Челябинск ул. Воровского  30  троллейбусн. ост. «ул. Курчатова» (из центра)',
        'город Челябинск ул. Воровского  30', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (134, 95, 61.390786, 55.151528, 'Россия, Челябинск, улица Воровского, 21',
        'город Челябинск ул. Воровского  21  троллейбусн. ост. «Горбольница» (в центр)',
        'город Челябинск ул. Воровского  21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (135, 96, 61.391298, 55.155499, 'Россия, Челябинск, улица Воровского, 16к7',
        'город Челябинск ул. Воровского  16к7  троллейбусн. ост. «ул. Южная» ( в центр)',
        'город Челябинск ул. Воровского  16к7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (136, 97, 61.386878, 55.143029, 'Россия, Челябинск, улица Доватора, 23',
        'город Челябинск ул. Доватора  23  автобусн. ост. «гостиница Центральная» (в сторону ж/д вокзала)',
        'город Челябинск ул. Доватора  23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (137, 98, 61.38112, 55.146147, 'Россия, Челябинск, улица Воровского, 40',
        'город Челябинск ул. Воровского  40  троллейбусн. ост. «ул. Доватора» (из центра)',
        'город Челябинск ул. Воровского  40', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (138, 99, 61.376368, 55.141655, 'Россия, Челябинск, улица Тарасова, 56',
        'город Челябинск ул. Тарасова  56  троллейбусн. ост. «Мединститут» (в центр)',
        'город Челябинск ул. Тарасова  56', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (139, 100, 61.37661, 55.135907, 'Россия, Челябинск, Междугородная улица, 21',
        'город Челябинск ул. Междугородная  напротив д. 21  трамвайн. ост. «Медгородок» (из центра)',
        'город Челябинск ул. Междугородная д. 21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (140, 101, 61.374805, 55.135217, 'Россия, Челябинск, улица Блюхера, 45',
        'город Челябинск ул. Блюхера  45  ост. «Областная больница» (автобус рейсовый  в сторону Уфимского тракта)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (141, 102, 61.36672, 55.129026, 'Россия, Челябинск, улица Блюхера, 67',
        'город Челябинск ул. Блюхера  67  троллейбусн. ост. «Мебельная фабрика» (в центр)',
        'город Челябинск ул. Блюхера  67', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (142, 103, 61.364618, 55.122771, 'Россия, Челябинск, улица Блюхера, 85',
        'город Челябинск ул. Блюхера  85а  троллейбусн. ост. «поселок Мебельный» (в центр)',
        'город Челябинск ул. Блюхера  85а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (144, 104, 61.356066, 55.10961, 'Россия, Челябинск, Ярославская улица, 1к2',
        'город Челябинск ул. Ярославская  1к2  троллейбусн. ост. «АМЗ»  (в центр)',
        'город Челябинск ул. Ярославская  1к2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (146, 105, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск ул. Ярославская  напротив д.1к2   троллейбусн. ост. «АМЗ» (из центра)',
        'город Челябинск ул. Ярославская  напротив д.1к2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (147, 106, 61.411725, 55.141428, 'Россия, Челябинск, улица Степана Разина, 4',
        'город Челябинск ул. Степана Разина  напротив д.4  трамвайн. ост. «ж/д вокзал» (в центр)',
        'город Челябинск ул. Степана Разина д.4', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (148, 107, 61.416136, 55.141315, 'Россия, Челябинск, Привокзальная площадь, 1',
        'город Челябинск Привокзальная площадь  1  троллейбусн.ост. «Привокзальная площадь» (в центр)',
        'город Челябинск Привокзальная площадь  1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (149, 108, 61.417007, 55.145483, 'Россия, Челябинск, улица Свободы, 173',
        'город Челябинск ул. Свободы  173  троллейбусн. ост. «ж/д институт» (в центр)',
        'город Челябинск ул. Свободы  173', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (150, 109, 61.410306, 55.160936, 'Россия, Челябинск, проспект Ленина, 46',
        'город Челябинск пр. Ленина  46  троллейбусн. ост. «Детский мир» (в центр)', 'город Челябинск пр. Ленина  46',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (151, 110, 61.412121, 55.159995, 'Россия, Челябинск, улица Свободы, 139',
        'город Челябинск ул. Свободы  139  троллейбусн. ост. «Детский мир» (из центра)',
        'город Челябинск ул. Свободы  139', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (152, 111, 61.411061, 55.160164, 'Россия, Челябинск, улица Свободы, 68',
        'город Челябинск ул. Свободы  68  троллейбусн.ост. «Детский мир» (из центра)',
        'город Челябинск ул. Свободы  68', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (153, 112, 61.442933, 55.16108, 'Россия, Челябинск, проспект Ленина, 18',
        'город Челябинск пр. Ленина  18  трамвайн. ост. «Комсомольская площадь» (в сторону северо-запада)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (155, 113, 61.414618, 55.184947, 'Россия, Челябинск, Российская улица, 43',
        'город Челябинск ул. Российская  43  автобусн. ост. «ул. Российская» (из центра)',
        'город Челябинск ул. Российская  43', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (157, 114, 61.372388, 55.03977, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 4',
        'город Челябинск ул. Кирова  напротив д. 2а  трамвайн. ост. «Теплотехнический институт» (в центр)',
        'город Челябинск ул. Кирова д. 2а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (158, 115, 61.398493, 55.184901, 'Россия, Челябинск, проспект Победы, 168',
        'город Челябинск пр. Победы  168  автобусн. ост. «Теплотехнический институт» (из центра)',
        'город Челябинск пр. Победы  168', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (159, 116, 61.372056, 55.040446, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 1',
        'город Челябинск ул. Кирова  1б  автобусн. ост. «Теплотехнический институт» (из центра)',
        'город Челябинск ул. Кирова  1б ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (160, 117, 61.39755, 55.174049, 'Россия, Челябинск, улица Братьев Кашириных, 2',
        'город Челябинск ул. Бр. Кашириных  2  автобусн. ост. «Цирк» (из центра)',
        'город Челябинск ул. Бр. Кашириных  2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (161, 118, 61.403946, 55.167226, 'Россия, Челябинск, улица Цвиллинга, 5',
        'город Челябинск ул. Цвиллинга  напротив д. 5  автобусн. ост. «Оперный театр» (в центр)',
        'город Челябинск ул. Цвиллинга д. 5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (162, 119, 61.403955, 55.166958, 'Россия, Челябинск, улица Цвиллинга, 7',
        'город Челябинск ул. Цвиллинга  7  трамвайн. ост. «Оперный театр» (из центра)',
        'город Челябинск ул. Цвиллинга  7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (163, 120, 61.433303, 55.044908, 'Россия, Челябинск, посёлок Исаково, улица Калинина, 32',
        'город Челябинск ул. Калинина  34  троллейбусн. ост. «ул. Калинина» (из центра)',
        'город Челябинск ул. Калинина  34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (170, 121, 61.388181, 55.17865, 'Россия, Челябинск, Свердловский проспект, 35А',
        'город Челябинск Свердловский пр.  35а  троллейбусн. ост. «ул. Калинина» (в  центр)',
        'город Челябинск Свердловский пр.  35а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (171, 122, 61.386052, 55.187738, 'Россия, Челябинск, Свердловский проспект, 16',
        'город Челябинск Свердловский пр.  16  троллейбусн. ост. «ул. Островского» (в  центр)',
        'город Челябинск Свердловский пр.  16', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (172, 123, 61.378317, 55.192261, 'Россия, Челябинск, Комсомольский проспект, 10/1',
        'город Челябинск пр. Комсомольский  10/1  троллейбусн. ост. «ул. Краснознаменная» (из центра)',
        'город Челябинск пр. Комсомольский  10/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (173, 124, 61.380141, 55.191808, 'Россия, Челябинск, Краснознамённая улица, 1А',
        'город Челябинск ул. Краснознаменная  1а/1  троллейбусн. ост. «ул. Краснознаменная» (в центр)',
        'город Челябинск ул. Краснознаменная  1а/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (174, 125, 61.37069, 55.192256, 'Россия, Челябинск, Комсомольский проспект, 14',
        'город Челябинск пр. Комсомольский  14  троллейбусн. ост. «ул. Косарева» (из центра)',
        'город Челябинск пр. Комсомольский  14', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (175, 126, 61.372119, 55.191485, 'Россия, Челябинск, Комсомольский проспект, 23Д',
        'город Челябинск пр. Комсомольский  23д  троллейбусн. ост. «ул. Косарева» (в центр)',
        'город Челябинск пр. Комсомольский  23д', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (176, 127, 61.324122, 55.194023, 'Россия, Челябинск, Комсомольский проспект, 61',
        'город Челябинск пр. Комсомольский  61  троллейбусн. ост. «ул. Ворошилова» ( в центр)',
        'город Челябинск пр. Комсомольский  61', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (177, 128, 61.308527, 55.194018, 'Россия, Челябинск, Комсомольский проспект, 85',
        'город Челябинск Комсомольский пр.  85  троллейбусн. ост. «8-й микрорайон» (в центр)',
        'город Челябинск Комсомольский пр.  85', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (178, 129, 61.294917, 55.190195, 'Россия, Челябинск, Комсомольский проспект, 103',
        'город Челябинск Комсомольский пр.  103  автобусн. ост. «11-й микрорайон» (в центр)',
        'город Челябинск Комсомольский пр.  103', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (179, 130, 61.292456, 55.183554, 'Россия, Челябинск, проспект Победы, 392',
        'город Челябинск ул.Чичерина/ пр. Победы  392  автобусн. ост. «ул. Чичерина» (из центра)',
        'город Челябинск пр. Победы 392', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (180, 131, 61.293283, 55.182773, 'Россия, Челябинск, проспект Победы, 327',
        'город Челябинск пр. Победы  327  автобусн.  ост. «ул. Чичерина» ( в центр)', 'город Челябинск пр. Победы  327',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (181, 132, 61.293651, 55.182444, 'Россия, Челябинск, проспект Победы, 325',
        'город Челябинск ул.Чичерина/ пр. Победы  напротив д. 325  автобусн. ост. «ул. Чичерина» (в центр)',
        'город Челябинск пр. Победы  д. 325', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (182, 133, 61.357629, 55.177915, 'Россия, Челябинск, улица Братьев Кашириных, 95Б',
        'город Челябинск ул. Бр. Кашириных  95б  троллейбусн. ост. «ул. Северо-Крымская» (в центр)',
        'город Челябинск ул. Бр. Кашириных  95б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (183, 134, 61.357323, 55.179087, 'Россия, Челябинск, Партизанская улица, 62/137',
        'город Челябинск ул. Бр. Кашириных/ ул. Партизанская  62  троллейбусн.  ост. «ул. Северо-Крымская» (из центра)',
        'город Челябинск ул. ул. Партизанская  62', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (184, 135, 61.330994, 55.179344, 'Россия, Челябинск, улица Молодогвардейцев, 70А',
        'город Челябинск ул. Бр. Кашириных/ ул. Молодогвардейцев  70а  автобусн. ост. «ул. Молодогвардейцев» ( из центра)',
        'город Челябинск ул. ул. Молодогвардейцев  70а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (185, 136, 61.325712, 55.179318, 'Россия, Челябинск, улица Ворошилова, 57',
        'город Челябинск ул. Бр. Кашириных/ ул. Ворошилова  57  автобусн. ост. «ул. Ворошилова» (из центра)',
        'город Челябинск ул. ул. Ворошилова  57', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (186, 137, 61.316998, 55.179082, 'Россия, Челябинск, улица Братьев Кашириных, 102',
        'город Челябинск ул. Бр. Кашириных  102  автобусн. ост. «ул. Солнечная» (из центра)',
        'город Челябинск ул. Бр. Кашириных  102', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (188, 138, 61.322747, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 129А/4',
        'город Челябинск ул. Бр. Кашириных  129а  автобусн. ост. «Челябинский государственный университет» (в центр)',
        'город Челябинск ул. Бр. Кашириных  129а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (189, 139, 61.313054, 55.178203, 'Россия, Челябинск, улица Братьев Кашириных, 108',
        'город Челябинск ул. Бр. Кашириных  108/1  автобусн. ост. «24-й микрорайон» (в центр)',
        'город Челябинск ул. Бр. Кашириных  108/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (190, 140, 61.448287, 55.157577, 'Россия, Челябинск, улица 40-летия Октября, 36',
        'город Челябинск ул. 40-летия   36  автобусн. ост. «ул. 40-летия Победы» (в центр)Победы',
        'город Челябинск ул. 40-летия   36', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (192, 141, 61.297729, 55.179216, 'Россия, Челябинск, улица Чичерина, 29',
        'город Челябинск ул. Чичерина  29  автобусн.ост. «ул. 250 лет Челябинску» (из центра)',
        'город Челябинск ул. Чичерина  29 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (193, 142, 61.397298, 55.074216, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 6',
        'город Челябинск ул. Гагарина  6  автобусн. ост. «Политехнический техникум» (в сторону ул. Новороссийская)',
        'город Челябинск ул. Гагарина  6', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (194, 143, 61.397415, 55.076891, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 32',
        'город Челябинск ул. Гагарина  32  троллейбусн. ост. «кинотеатр «Аврора» (в сторону ул. Новороссийская)',
        'город Челябинск ул. Гагарина  32', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (195, 144, 61.396831, 55.077437, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 35',
        'город Челябинск  ул. Гагарина  35а  троллейбусн. ост. «Управление соцзащиты населения» (в сторону КБС)',
        'город Челябинск  ул. Гагарина  35а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (196, 145, 61.425577, 55.133961, 'Россия, Челябинск, улица Дзержинского, 130',
        'город Челябинск ул. Дзержинского  130  автобусн. ост. «ул. Барбюса» (в центр)',
        'город Челябинск ул. Дзержинского  130', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (197, 146, 61.426862, 55.132659, 'Россия, Челябинск, улица Дзержинского, 109',
        'город Челябинск ул. Дзержинского  109  автобусн. ост. «ул. Барбюса» (из центра) ',
        'город Челябинск ул. Дзержинского  109', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (198, 147, 61.396831, 55.077989, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 39',
        'город Челябинск ул. Гагарина  39  троллейбусн. ост. «библиотека им. Мамина-Сибиряка» (в центр)',
        'город Челябинск ул. Гагарина  39', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (199, 148, 61.435872, 55.117875, 'Россия, Челябинск, Ленинский район, улица Гагарина, 55А',
        'город Челябинск ул. Гагарина  55а  троллейбусн. ост. «магазин Спорттовары» (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (202, 149, 61.29242, 55.18909, 'Россия, Челябинск, Комсомольский проспект, 105',
        'город Челябинск ул. 40-летия Победы/ пр. Комсомольский  105  автобусн. ост. «11-й микрорайон» (в центр)',
        'город Челябинск пр. Комсомольский  105', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (205, 150, 61.292276, 55.190359, 'Россия, Челябинск, Комсомольский проспект, 100',
        'город Челябинск Комсомольский пр.  100  автобусн. ост. «11-й микрорайон» (из центра)',
        'город Челябинск Комсомольский пр.  100 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (206, 151, 61.332009, 55.188077, 'Россия, Челябинск, улица Молодогвардейцев, 54',
        'город Челябинск ул. Молодогвардейцев  54  троллейбусн. ост. «пр. Победы» (в центр)',
        'город Челябинск ул. Молодогвардейцев  54', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (207, 152, 61.306488, 55.151821, 'Россия, Челябинск, посёлок Шершни, Парковая улица, 3',
        'город Челябинск ул. Бр. Кашириных/ ул. Парковая  5  автобусн. ост. «ул. Партизанская» (из центра)',
        'город Челябинск ул. Парковая  5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (208, 153, 61.366567, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 89',
        'город Челябинск ул. Бр. Кашириных  89  автобусн. ост. «ул. Партизанская» (в центр)',
        'город Челябинск ул. Бр. Кашириных  89', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (209, 154, 61.378551, 55.174763, 'Россия, Челябинск, улица Братьев Кашириных, 73',
        'город Челябинск ул. Бр. Кашириных  73  автобусн. ост. «ул. Полковая» (в центр)',
        'город Челябинск ул. Бр. Кашириных  73', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (210, 155, 61.305221, 55.181961, 'Россия, Челябинск, улица 40-летия Победы, 35',
        'город Челябинск ул. 40-летия Победы  35  автобусн. ост. «ул. 40-летия Победы» (из центра)',
        'город Челябинск ул. 40-летия Победы  35', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (212, 156, 61.451943, 55.160648, 'Россия, Челябинск, проспект Ленина, 8А',
        'город Челябинск ул.Героев танкограда/ пр. Ленина  8а  троллейбусн. ост. «Театр ЧТЗ» (из центра)',
        'город Челябинск пр. Ленина  8а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (216, 157, 61.416136, 55.141315, 'Россия, Челябинск, Привокзальная площадь, 1',
        'город Челябинск Привокзальная площадь  1  ост. «ж/д вокзал» (автобус №18  в сторону центра) ',
        'город Челябинск Привокзальная площадь  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (220, 158, 61.416136, 55.141315, 'Россия, Челябинск, Привокзальная площадь, 1',
        'город Челябинск Привокзальная площадь  1  ост. «ж/д вокзал» (автобус № 64  в сторону центра)',
        'город Челябинск Привокзальная площадь  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (221, 159, 61.373125, 55.040647, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 3',
        'город Челябинск ул. Кирова  7  автобусн. ост. «ул. Калинина» (в сторону Теплотехнического института)',
        'город Челябинск ул. Кирова  7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (222, 160, 61.380302, 55.159393, 'Россия, Челябинск, проспект Ленина, 73/30',
        'город Челябинск ул.Энгельса/ пр. Ленина  73  автобусн. ост. «Агроинженерная академия» (из центра)',
        'город Челябинск пр. Ленина  73', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (227, 161, 61.393391, 55.156837, 'Россия, Челябинск, Красная улица, 42',
        'город Челябинск ул.Красная 42-ул.К.Либкнехта  до перекр.из центра', 'город Челябинск ул.Красная 42', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (233, 162, 61.410791, 55.164207, 'Россия, Челябинск, улица Свободы, 44',
        'город Челябинск ул.Свободы 44 - ул.Коммуны  до перекр.в центр', 'город Челябинск ул.Свободы 44', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (234, 163, 61.397748, 55.161795, 'Россия, Челябинск, улица Коммуны, 87',
        'город Челябинск ул.Коммуны  87 - ул.Елькина  до перекр.в центр  у рест. "Бад Гаштейн"',
        'город Челябинск ул.Коммуны  87', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (237, 164, 61.403048, 55.164932, 'Россия, Челябинск, улица Цвиллинга, 10',
        'город Челябинск ул.Цвиллинга 10- ул.К.Маркса  за перекр.в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (245, 165, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр.Ленина 76  из центра  у ЮУРГУ  поз.1', 'город Челябинск пр.Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (246, 166, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр.Ленина 76  из центра  у ЮУРГУ  поз.2', 'город Челябинск пр.Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (247, 167, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр.Ленина 76  из центра  у ЮУРГУ  поз.3', 'город Челябинск пр.Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (248, 168, 0, 0,
        'Россия, Челябинская область, Сосновский район, посёлок Новый Кременкуль, коттеджный посёлок Солнечная Долина 2',
        'город Челябинск ул.Бр.Кашириных  в центр  до ост. "Солнечная"', 'город Челябинск  ост. "Солнечная"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (250, 169, 61.433878, 55.13269, 'Россия, Челябинск, улица Дзержинского, 104',
        'город Челябинск ул.Дзержинского 104 - ул.Гагарина  100м за перекр.в центр',
        'город Челябинск ул.Дзержинского 104', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (251, 170, 61.408069, 55.165678, 'Россия, Челябинск, улица Маркса, 52',
        'город Челябинск ул.К.Маркса 52 - ул.Пушкина  за перекр.в центр', 'город Челябинск ул.К.Маркса 52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (261, 171, 61.393114, 55.165142, 'Россия, Челябинск, Красная улица',
        'город Челябинск ул.Красная-ул.К.Маркса  до перекр.в центр  у сквера',
        'город Челябинск ул.Красная / ул.К.Маркса', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (262, 172, 61.4078, 55.160843, 'Россия, Челябинск, проспект Ленина, 50',
        'город Челябинск пр Ленина 50-ул.Советская  100м до перекр.в центр  у маг. "Ювелирный зал"',
        'город Челябинск пр Ленина 50', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (263, 173, 61.4078, 55.160843, 'Россия, Челябинск, проспект Ленина, 50',
        'город Челябинск пр.Ленина 50 - ул. Советская  до перекр.в центр', 'город Челябинск пр Ленина 50', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (266, 174, 61.410818, 55.162669, 'Россия, Челябинск, улица Свободы, 60',
        'город Челябинск ул.Свободы 60 - ул.Коммуны  за перекр.в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (274, 175, 61.527303, 55.118766, 'Россия, Челябинск, Ленинский район, посёлок Мясокомбинат',
        'город Челябинск Копейское шоссе  50 м до поворота на Мясокомбинат', 'город Челябинск Мясокомбинат', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (275, 176, 61.374975, 55.16055, 'Россия, Челябинск, проспект Ленина, 72',
        'город Челябинск пересечение пр.Ленина 72 и ул.Энтузиастов', 'город Челябинск пересечение пр.Ленина 72', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (284, 177, 61.380051, 55.160411, 'Россия, Челябинск, проспект Ленина, 66А',
        'город Челябинск пересечение пр.Ленина 66а и ул.Энгельса', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (286, 178, 61.394693, 55.160483, 'Россия, Челябинск, проспект Ленина, 62',
        'город Челябинск пересечение пр.Ленина 62 и ул.Васенко 96', 'город Челябинск пересечение пр.Ленина 62', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (287, 179, 61.410234, 55.160278, 'Россия, Челябинск, проспект Ленина, 45',
        'город Челябинск пересечение пр.Ленина 45 и ул.Свободы 70', 'город Челябинск пересечение пр.Ленина 45', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (288, 180, 61.424706, 55.161615, 'Россия, Челябинск, проспект Ленина, 28',
        'город Челябинск пр.Ленина 28 (Агентство аэрофлота)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (290, 181, 61.402419, 55.160818, 'Россия, Челябинск, проспект Ленина, 54',
        'город Челябинск пр.Ленина 54 (середина дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (292, 182, 61.402419, 55.160818, 'Россия, Челябинск, проспект Ленина, 54',
        'город Челябинск пересечение пр.Ленина 54 и ул.Цвиллинга', 'город Челябинск пересечение пр.Ленина 54', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (293, 183, 61.401287, 55.161003, 'Россия, Челябинск, улица Кирова, 177',
        'город Челябинск пересечение пр.Ленина и ул.Кирова 177', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (295, 184, 61.397661, 55.157809, 'Россия, Челябинск, улица Воровского',
        'город Челябинск пересечение ул.Воровского (в центр) и ул.Тимирязева  за перекрестком',
        'город Челябинск ул.Воровского / ул.Тимирязева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (296, 185, 61.412633, 55.160324, 'Россия, Челябинск, проспект Ленина, 43',
        'город Челябинск пересечение пр. Ленина 43 и ул. Свободы', 'город Челябинск пересечение пр. Ленина 43', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (297, 186, 61.442933, 55.16108, 'Россия, Челябинск, проспект Ленина, 18',
        'город Челябинск пересечение пр. Ленина 18  и ул.Горького ', 'город Челябинск пересечение пр. Ленина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (298, 187, 61.420583, 55.160545, 'Россия, Челябинск, проспект Ленина, 29',
        'город Челябинск пересечение пр. Ленина 29 и ул. 3 Интернационала', 'город Челябинск пересечение пр. Ленина 29',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (299, 188, 61.398493, 55.184901, 'Россия, Челябинск, проспект Победы, 168',
        'город Челябинск пересечение пр.Победы 168 и ул.Кирова', 'город Челябинск пересечение пр.Победы 168', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (300, 189, 61.410558, 55.158061, 'Россия, Челябинск, улица Тимирязева, 27',
        'город Челябинск ул.Пушкина/ул.Тимирязева 27', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (304, 190, 61.36645, 55.232654, 'Россия, Челябинск, Свердловский тракт, 8', 'город Челябинск Свердловский тр. 8',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (305, 191, 61.454054, 55.170347, 'Россия, Челябинск, улица Героев Танкограда, 75',
        'город Челябинск ул. Героев Танкограда  75  ост. «ул. Первой Пятилетки» (из центра)',
        'город Челябинск ул. Героев Танкограда  75', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (306, 192, 61.454054, 55.170347, 'Россия, Челябинск, улица Героев Танкограда, 75',
        'город Челябинск ул. Героев Танкограда  75  ост. «ул. Первой Пятилетки» (в центр)',
        'город Челябинск ул. Героев Танкограда  75', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (307, 193, 61.37361, 55.14006, 'Россия, Челябинск, улица Воровского, 64', 'город Челябинск ул. Воровского  64',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (309, 194, 61.400137, 55.180038, 'Россия, Челябинск, улица Кирова, 7',
        'город Челябинск ул. Кирова  7  ост. «ул. Калинина»', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (315, 195, 61.398682, 55.178352, 'Россия, Челябинск, улица Кирова, 44',
        'город Челябинск ул. Кирова  44  ост. «ул. Калинина»', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (319, 196, 61.316998, 55.179082, 'Россия, Челябинск, улица Братьев Кашириных, 102',
        'город Челябинск ул. Братьев Кашириных  102', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (320, 197, 61.308985, 55.176331, 'Россия, Челябинск, улица Братьев Кашириных, 114',
        'город Челябинск ул. Братьев Кашириных  114', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (321, 198, 61.370376, 55.1772, 'Россия, Челябинск, улица Братьев Кашириных, 85А',
        'город Челябинск ул. Братьев Кашириных  83', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (322, 199, 61.365938, 55.158904, 'Россия, Челябинск, проспект Ленина, 87', 'город Челябинск пр. Ленина  87',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (324, 200, 61.348008, 55.179421, 'Россия, Челябинск, улица Братьев Кашириных, 68А',
        'город Челябинск ул. Братьев Кашириных  68а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (325, 201, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. и ул.Труда на кольце в центр  напротив д.161',
        'город Челябинск Свердловского пр. / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (326, 202, 61.41027, 55.150972, 'Россия, Челябинск, улица Цвиллинга, 58', 'город Челябинск ул.Цвиллинга 58',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (327, 203, 61.394886, 55.174323, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул.Бр.Кашириных и ул.1-го Мая  100 м за перекрестком из центра',
        'город Челябинск ул.Бр.Кашириных / ул.1-го Мая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (328, 204, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение ул. Труда и Свердловского пр.  на кольце (в центр)',
        'город Челябинск Свердловского пр. / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (329, 205, 61.378578, 55.241421, 'Россия, Челябинск, Черкасская улица, 4', 'город Челябинск ул.Черкасская 4',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (330, 206, 61.38907, 55.089186, 'Россия, Челябинск, Троицкий тракт, 60/1', 'город Челябинск Троицкий тр.  60/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (331, 207, 61.379367, 55.258329, 'Россия, Челябинск, улица Румянцева',
        'город Челябинск пересечение ул. Румянцева и ул.Богдана Хмельницкого',
        'город Челябинск ул. Румянцева / ул.Богдана Хмельницкого', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (332, 208, 61.393084, 55.167587, 'Россия, Челябинск, Красная улица',
        'город Челябинск пересечение ул.Красная и ул. Труда  за перекрестком из центра',
        'город Челябинск  ул.Красная / ул. Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (333, 209, 61.389879, 55.174325, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул.Бр.Кашириных и ул.Тагильская  за перекрестком в центр у ТЦ',
        'город Челябинск  ул.Бр.Кашириных / ул.Тагильская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (334, 210, 61.37069, 55.192256, 'Россия, Челябинск, Комсомольский проспект, 14',
        'город Челябинск пересечение Комсомольского пр.  14 и ул. Косарева', 'город Челябинск  Комсомольского пр.  14',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (335, 211, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. и ул.Труда напротив ДС Юность  на кольце из центра поз.1',
        'город Челябинск Свердловского пр. / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (336, 212, 61.374822, 55.136254, 'Россия, Челябинск, улица Воровского',
        'город Челябинск пересечение ул. Воровского и ул. Блюхера  со стороны Областной больницы',
        'город Челябинск ул. Воровского / ул. Блюхера ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (337, 213, 61.381335, 55.166953, 'Россия, Челябинск, улица Энгельса, 23',
        'город Челябинск пересечение ул.Энгельса 23 и ул.Труда', 'город Челябинск ул.Энгельса 23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (338, 214, 61.387638, 55.174127, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул.Бр.Кашириных и Свердловского пр.  за перекр. в центр  у авт.ост."Свердловский пр."',
        'город Челябинск  ул.Бр.Кашириных / Свердловского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (339, 215, 61.445143, 55.144222, 'Россия, Челябинск, Ленинский район, улица Гагарина, 3',
        'город Челябинск ул. Гагарина  3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (341, 216, 61.378919, 55.239727, 'Россия, Челябинск, Черкасская улица, 6',
        'город Челябинск пересечение ул. Черкасская  напротив д. 6 и ул. 50 лет ВЛКСМ  на кольце',
        'город Челябинск  ул. Черкасская д. 6', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (342, 217, 61.39292, 55.167583, 'Россия, Челябинск, улица Труда',
        'город Челябинск пересечение ул.Труда и ул.Красная поз.1', 'город Челябинск  ул.Труда / ул.Красная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (346, 218, 61.372157, 55.18359, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр. Победы и ул. Косарева  до перекрестка в центр',
        'город Челябинск пересечение пр. Победы / ул. Косарева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (353, 219, 61.434372, 55.131244, 'Россия, Челябинск, улица Дзержинского, 93',
        'город Челябинск ул. Дзержинского  93', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (354, 220, 61.39292, 55.167583, 'Россия, Челябинск, улица Труда',
        'город Челябинск пересечение ул.Труда и ул.Красная поз.2', 'город Челябинск  ул.Труда / ул.Красная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (355, 221, 61.405177, 55.183456, 'Россия, Челябинск, проспект Победы, 149',
        'город Челябинск пересечение пр. Победы  149 и ул. Болейко', 'город Челябинск пр. Победы  149', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (356, 222, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. и ул.Труда напротив ДС Юность  на кольце из центра  поз.2',
        'город Челябинск Свердловского пр. / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (357, 223, 61.38739, 55.1132, 'Россия, Челябинск, Троицкий тракт, 48Б', 'город Челябинск Троицкийтр.  48Б', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (358, 224, 61.400137, 55.180038, 'Россия, Челябинск, улица Кирова, 7', 'город Челябинск ул. Кирова  7', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (361, 225, 61.333464, 55.187106, 'Россия, Челябинск, улица Молодогвардейцев, 33Б',
        'город Челябинск ул. Молодогвардейцев  33Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (362, 226, 61.391783, 55.147382, 'Россия, Челябинск, улица Крупской, 30',
        'город Челябинск пересечение ул. Блюхера и ул. Крупской  34', 'город Челябинск ул. Крупской  34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (365, 227, 61.386725, 55.149213, 'Россия, Челябинск, улица Воровского, 43А',
        'город Челябинск ул. Воровского  43А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (366, 228, 61.380716, 55.175493, 'Россия, Челябинск, Краснознамённая улица, 41/1',
        'город Челябинск пересечение ул.Бр. Кашириных и ул. Краснознаменная  41/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (368, 229, 61.428362, 55.044469, 'Россия, Челябинск, посёлок Исаково, улица Калинина, 17',
        'город Челябинск пересечение ул. Калинина  17 и ул. Кыштымская', 'город Челябинск ул. Калинина  17', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (369, 230, 61.367888, 55.231724, 'Россия, Челябинск, Свердловский тракт, 10/3',
        'город Челябинск Свердловский тр.  10/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (370, 231, 61.399167, 55.15624, 'Россия, Челябинск, улица Елькина, 63',
        'город Челябинск пересечение ул. Елькина  63 и ул. Карла Либкнехта', 'город Челябинск ул. Елькина  63', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (371, 232, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск дорога в Аэропорт  на кольце дорожной развязки в Металлургический район',
        'город Челябинск дорога в Аэропорт', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (372, 233, 61.49278, 55.100886, 'Россия, Челябинск, Новороссийская улица, 8',
        'город Челябинск ул. Новороссийская  8', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (374, 234, 61.37541, 55.159689, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пр.Ленина - Энтузиастов  до перекрестка в центр', 'город Челябинск пр.Ленина / Энтузиастов',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (379, 235, 61.411447, 55.160654, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пр.Ленина -ул.Свободы до перекрестка в центр', 'город Челябинск пр.Ленина / ул.Свободы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (387, 236, 61.422918, 55.161281, 'Россия, Челябинск, проспект Ленина, 30', 'город Челябинск пр.Ленина 30', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (389, 237, 61.412633, 55.160324, 'Россия, Челябинск, проспект Ленина, 43',
        'город Челябинск пр.Ленина 43-ул.Свободы  за перекр.из центра', 'город Челябинск пр.Ленина 43', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (390, 238, 61.410234, 55.160278, 'Россия, Челябинск, проспект Ленина, 45',
        'город Челябинск пр.Ленина 45-ул.Пушкина  за перекр.из центра', 'город Челябинск пр.Ленина 45', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (391, 239, 61.409372, 55.16091, 'Россия, Челябинск, проспект Ленина, 48',
        'город Челябинск пр.Ленина 48-ул.Пушкина  до перекр.в центр', 'город Челябинск пр.Ленина 48', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (392, 240, 61.4078, 55.160843, 'Россия, Челябинск, проспект Ленина, 50',
        'город Челябинск пр.Ленина 50-ул.Советская  до перекр. в центр', 'город Челябинск пр.Ленина 50', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (393, 241, 61.406641, 55.160128, 'Россия, Челябинск, проспект Ленина, 51',
        'город Челябинск пр.Ленина 51-ул.Советская', 'город Челябинск пр.Ленина 51', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (394, 242, 61.405698, 55.160828, 'Россия, Челябинск, проспект Ленина, 52',
        'город Челябинск пр.Ленина 52-ул.Цвиллинга  до перекр.в центр', 'город Челябинск пр.Ленина 52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (395, 243, 61.402419, 55.160818, 'Россия, Челябинск, проспект Ленина, 54',
        'город Челябинск пр.Ленина 54-ул.Кирова  до перекр.из центра  у пиццерии"Помидор"',
        'город Челябинск пр.Ленина 54', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (396, 244, 61.399634, 55.159928, 'Россия, Челябинск, проспект Ленина, 55',
        'город Челябинск пр.Ленина 55-ул.Воровского  до перекр.в центр  у Арбитражного суда',
        'город Челябинск пр.Ленина 55', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (397, 245, 61.394855, 55.159794, 'Россия, Челябинск, проспект Ленина, 61',
        'город Челябинск пр.Ленина 61-ул.Васенко  до перекр.в центр  у "Сбербанк"', 'город Челябинск пр.Ленина 61', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (398, 246, 61.392888, 55.159768, 'Россия, Челябинск, проспект Ленина, 63',
        'город Челябинск пр.Ленина 63-ул.Красная  до перекр.в центр', 'город Челябинск пр.Ленина 63', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (399, 247, 61.383842, 55.160689, 'Россия, Челябинск, проспект Ленина, 64',
        'город Челябинск пр.Ленина 64-ул.Энгельса  до перекр.из центра', 'город Челябинск пр.Ленина 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (400, 248, 61.383842, 55.160689, 'Россия, Челябинск, проспект Ленина, 64',
        'город Челябинск пр.Ленина 64б-ул.Володарского  32  за перекрестком из центра', 'город Челябинск пр.Ленина 64б',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (401, 249, 61.380302, 55.159393, 'Россия, Челябинск, проспект Ленина, 73/30',
        'город Челябинск пр.Ленина 73-ул.Энгельса  до перекрестка в центр', 'город Челябинск пр.Ленина 73', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (402, 250, 61.393519, 55.160142, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пр.Ленина-ул.Красная  за перекр.из центра  у памятника "Орленок"',
        'город Челябинск пр.Ленина / ул.Красная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (403, 251, 61.389744, 55.159676, 'Россия, Челябинск, проспект Ленина, 67',
        'город Челябинск пр.Ленина 67-Свердловский пр.  за перекр. в центр  авт.ост. "Алое поле"',
        'город Челябинск пр.Ленина 67', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (405, 252, 61.44129, 55.160835, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пр.Ленина-ул.Горького  у Комсомольской площади  из центра',
        'город Челябинск пр.Ленина / ул.Горького', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (408, 253, 61.403632, 55.184937, 'Россия, Челябинск, проспект Победы, 160А',
        'город Челябинск пр.Победы 160-А - ул.Болейко  за перекр. в центр  у маг. "Гавань"',
        'город Челябинск пр.Победы 160А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (409, 254, 61.387588, 55.183441, 'Россия, Челябинск, Свердловский проспект, 27',
        'город Челябинск Свердловский пр. 27-пр.Победы  до перекр.из центра', 'город Челябинск Свердловский пр. 27',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (410, 255, 61.386474, 55.184377, 'Россия, Челябинск, проспект Победы, 192А',
        'город Челябинск Свердловский пр.-пр.Победы  192А  до перекр.в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (413, 256, 61.389582, 55.158215, 'Россия, Челябинск, Свердловский проспект, 65',
        'город Челябинск Свердловский пр. 65 - ул. С. Кривой  за перекр. в центр',
        'город Челябинск Свердловский пр. 65', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (414, 257, 61.386366, 55.184747, 'Россия, Челябинск, Свердловский проспект, 26',
        'город Челябинск Свердловский пр.  26-пр.Победы', 'город Челябинск Свердловский пр.  26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (415, 258, 61.38745, 55.179306, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск Свердловский пр.-ул.Калинина  у КПП Автомоб. инст.  до перекр. в центр',
        'город Челябинск Свердловский пр / ул.Калинина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (416, 259, 61.388504, 55.155875, 'Россия, Челябинск, Свердловский проспект, 82',
        'город Челябинск Свердловский пр.82-ул.К.Либкнехта  до перекр. из центра', 'город Челябинск Свердловский пр.82',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (417, 260, 61.389645, 55.156147, 'Россия, Челябинск, Свердловский проспект, 71',
        'город Челябинск Свердловский пр. 71-ул.К.Либкнехта  за перекр. в центр', 'город Челябинск Свердловский пр. 71',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (418, 261, 61.388567, 55.158678, 'Россия, Челябинск, Свердловский проспект, 74',
        'город Челябинск Свердловский пр. 76-ул.С.Кривой 41 за перекр. из центра',
        'город Челябинск Свердловский пр. 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (419, 262, 61.395448, 55.160987, 'Россия, Челябинск, улица Васенко, 96',
        'город Челябинск ул.Васенко 96-пр.Ленина  до перекр.в центр  у Дом быта', 'город Челябинск ул.Васенко 96', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (420, 263, 61.39923, 55.159486, 'Россия, Челябинск, улица Воровского, 2',
        'город Челябинск ул.Воровского 2-пр.Ленина  за перекр.в центр  у Арбитражного суда',
        'город Челябинск ул.Воровского 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (421, 264, 61.399805, 55.159177, 'Россия, Челябинск, улица Воровского, 1',
        'город Челябинск ул.Воровского 1-пр.Ленина 150м до перекр.в центр', 'город Челябинск ул.Воровского 1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (427, 265, 61.396059, 55.155684, 'Россия, Челябинск, улица Воровского, 9',
        'город Челябинск ул.Воровского 9-ул.Красная  за перекр. в центр', 'город Челябинск ул.Воровского 9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (435, 266, 61.396715, 55.156127, 'Россия, Челябинск, улица Воровского, 7',
        'город Челябинск ул.Воровского 7-ул.Красная', 'город Челябинск ул.Воровского 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (443, 267, 61.394864, 55.155288, 'Россия, Челябинск, улица Воровского, 11',
        'город Челябинск ул.Воровского 11-ул.Красная', 'город Челябинск ул.Воровского 11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (446, 268, 61.397307, 55.157727, 'Россия, Челябинск, улица Воровского',
        'город Челябинск ул.Воровского-ул.С.Кривой  за перекр.из центра  до ост."Киномакс Урал" ',
        'город Челябинск ул.Воровского / ул.С.Кривой', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (447, 269, 61.411124, 55.159861, 'Россия, Челябинск, улица Свободы, 70',
        'город Челябинск ул. Свободы 70 - пр.Ленина', 'город Челябинск ул. Свободы 70', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (448, 270, 61.398679, 55.157538, 'Россия, Челябинск, улица Елькина',
        'город Челябинск ул.Елькина -ул. Тимирязева  до перекрестка из центра ',
        'город Челябинск ул.Елькина / ул. Тимирязева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (451, 271, 61.398679, 55.157538, 'Россия, Челябинск, улица Елькина',
        'город Челябинск ул.Елькина-ул.Тимирязева  за перекрестком из центра',
        'город Челябинск ул.Елькина / ул.Тимирязева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (454, 272, 61.372388, 55.03977, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 4',
        'город Челябинск ул.Кирова 2-пр.Победы  за перекр.в центр', 'город Челябинск ул.Кирова 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (468, 273, 61.373125, 55.040647, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 3',
        'город Челябинск ул.Кирова 9а - ул.Калинина  до перекр.в центр', 'город Челябинск ул.Кирова 9а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (472, 274, 61.394666, 55.163528, 'Россия, Челябинск, улица Коммуны, 70',
        'город Челябинск ул.Коммуны  70-ул.Красная  до перекр.из центра  у ФСБ', 'город Челябинск ул.Коммуны  70', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (473, 275, 61.3934, 55.157577, 'Россия, Челябинск, Красная улица, 40',
        'город Челябинск ул.Красная 40-ул.С.Кривой  за перекр.из центра', 'город Челябинск ул.Красная 40', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (474, 276, 61.39402, 55.155772, 'Россия, Челябинск, Красная улица',
        'город Челябинск ул.Красная - ул.К.Либкнехта  за перекр. из центра',
        'город Челябинск ул.Красная / ул.К.Либкнехта ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (475, 277, 61.394136, 55.158503, 'Россия, Челябинск, Красная улица, 71',
        'город Челябинск ул.Красная 71-ул.С.Кривой за перекр.в центр', 'город Челябинск ул.Красная 71', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (476, 278, 61.394433, 55.165575, 'Россия, Челябинск, Красная улица, 11',
        'город Челябинск ул.Красная  11-ул.К.Маркса', 'город Челябинск ул.Красная  11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (479, 279, 61.408599, 55.161126, 'Россия, Челябинск, улица Пушкина, 56',
        'город Челябинск ул.Пушкина 56-пр.Ленина  до перекрестка в центр', 'город Челябинск ул.Пушкина 56', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (480, 280, 61.409345, 55.160895, 'Россия, Челябинск, улица Пушкина, 59',
        'город Челябинск ул.Пушкина 59 - пр. Ленина  за перекрестком из центра', 'город Челябинск ул.Пушкина 59', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (481, 281, 61.40965, 55.159182, 'Россия, Челябинск, улица Пушкина, 65',
        'город Челябинск ул.Пушкина 65-ул.Тимирязева  за перекр. в центр', 'город Челябинск ул.Пушкина 65', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (491, 282, 61.408689, 55.160134, 'Россия, Челябинск, проспект Ленина, 47',
        'город Челябинск ул.Пушкина -пр.Ленина  47 за перекр.из центра', 'город Челябинск пр.Ленина  47', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (492, 283, 61.408851, 55.159588, 'Россия, Челябинск, улица Пушкина, 60', 'город Челябинск ул.Пушкина 60', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (493, 284, 61.376763, 55.157222, 'Россия, Челябинск, улица Сони Кривой, 69',
        'город Челябинск ул.С.Кривой  69А-ул.Энтузиастов  за перекр. в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (496, 285, 61.41398, 55.156225, 'Россия, Челябинск, улица Свободы, 149',
        'город Челябинск ул.Свободы 149-ул. Плеханова  за пер. в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (504, 286, 61.414295, 55.155263, 'Россия, Челябинск, улица Свободы, 151',
        'город Челябинск ул.Свободы 151-ул.Плеханова  до перекр. в центр', 'город Челябинск ул.Свободы 151', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (507, 287, 61.41584, 55.151091, 'Россия, Челябинск, улица Свободы, 159',
        'город Челябинск ул.Свободы 159-ул.Евтеева  за перекр. в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (515, 288, 61.412273, 55.157757, 'Россия, Челябинск, улица Свободы, 78',
        'город Челябинск ул.Свободы 78-ул.Тимирязева  за перекр.из центра', 'город Челябинск ул.Свободы 78', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (516, 289, 61.412938, 55.156194, 'Россия, Челябинск, улица Свободы, 82',
        'город Челябинск ул.Свободы  82-ул.Плеханова  до перекр. из центра', 'город Челябинск ул.Свободы  82', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (517, 290, 61.370484, 55.039847, 'Россия, Челябинск, посёлок Новосинеглазово, Советская улица, 28',
        'город Челябинск ул.Советская 38-пр.Ленина  до перекрестка в центр', 'город Челябинск ул.Советская 38', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (518, 291, 61.410558, 55.158061, 'Россия, Челябинск, улица Тимирязева, 27',
        'город Челябинск ул.Тимирязева 27-ул.Пушкина  за перекр. из центра ', 'город Челябинск ул.Тимирязева 27', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (519, 292, 61.40868, 55.157644, 'Россия, Челябинск, улица Тимирязева, 29',
        'город Челябинск ул.Тимирязева 29-ул.Пушкина  до перекр.из центра', 'город Челябинск ул.Тимирязева 29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (520, 293, 61.407126, 55.157541, 'Россия, Челябинск, улица Тимирязева, 31',
        'город Челябинск ул.Тимирязева 31-ул.Цвиллинга  за перекр. из центра ', 'город Челябинск ул.Тимирязева 31', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (523, 294, 61.398978, 55.158009, 'Россия, Челябинск, улица Елькина, 59',
        'город Челябинск ул.Тимирязева-ул. Елькина  59  до перекр. в центр', 'город Челябинск ул. Елькина  59', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (532, 295, 61.403533, 55.162715, 'Россия, Челябинск, улица Цвиллинга, 28',
        'город Челябинск ул.Цвиллинга  28 - ул. Коммуны  за перекр. в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (540, 296, 61.404037, 55.162888, 'Россия, Челябинск, улица Цвиллинга',
        'город Челябинск ул.Цвиллинга-ул.Комунны  за перекр. из центра', 'город Челябинск ул.Цвиллинга / ул.Комунны',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (541, 297, 61.382027, 55.15945, 'Россия, Челябинск, проспект Ленина, 71А',
        'город Челябинск ул.Энгельса - пр.Ленина 71-А  до перекр.в центр', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (551, 298, 61.376035, 55.158231, 'Россия, Челябинск, улица Энтузиастов, 7',
        'город Челябинск ул.Энтузиастов 7-ул.С.Кривой  за перекр.в центр', 'город Челябинск ул.Энтузиастов 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (552, 299, 61.375335, 55.156801, 'Россия, Челябинск, улица Энтузиастов, 12',
        'город Челябинск ул.Энтузиастов 12 - ул. С. Кривой  за перекр. из центра', 'город Челябинск ул.Энтузиастов 12',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (553, 300, 61.405518, 55.159871, 'Россия, Челябинск, улица Цвиллинга, 33',
        'город Челябинск ул.Цвиллинга 33 - пр. Ленина  до перекр. в центр', 'город Челябинск ул.Цвиллинга 33', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (554, 301, 61.36645, 55.232654, 'Россия, Челябинск, Свердловский тракт, 8',
        'город Челябинск Свердловский тр.  8', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (555, 302, 61.290668, 55.181426, 'Россия, Челябинск, улица Чичерина, 30к1',
        'город Челябинск пр. Победы/ул. Чичерина  30к1  у поворота на трамвайное кольцо',
        'город Челябинск ул. Чичерина  30к1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (556, 303, 61.281994, 55.180426, 'Россия, Челябинск, Городская улица',
        'город Челябинск пересечение ул. Городская и ул. Олонецкая', 'город Челябинск ул. Городская / ул. Олонецкая',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (557, 304, 61.464358, 55.185692, 'Россия, Челябинск, улица Комарова, 54', 'город Челябинск Комарова пр.  54',
        'город Челябинск Комарова 54', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (562, 305, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.  500 м до виадука к посёлку Н-Синеглазово', 'город Челябинск Троицкий тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (563, 306, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск дорога в Аэропорт  у поста ГИБДД  у виадука  слева', 'город Челябинск дорога в Аэропорт', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (564, 307, 61.390786, 55.151528, 'Россия, Челябинск, улица Воровского, 21',
        'город Челябинск ул.Воровского  21  30 м после ост. "Горбольница" (в центр)',
        'город Челябинск ул.Воровского  21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (565, 308, 61.383527, 55.093695, 'Россия, Челябинск, Троицкий тракт, 19',
        'город Челябинск Троицкий тр.  напротив д. 19', 'город Челябинск Троицкий тр.  д. 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (566, 309, 61.406264, 55.157217, 'Россия, Челябинск, улица Цвиллинга, 44', 'город Челябинск ул.Цвиллинга 44',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (567, 310, 61.371373, 55.158241, 'Россия, Челябинск, проспект Ленина, 83', 'город Челябинск пр.Ленина 83', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (569, 311, 61.383941, 55.15947, 'Россия, Челябинск, проспект Ленина, 71',
        'город Челябинск пересеч.пр.Ленина 71 и ул.Володарского 52а', 'город Челябинск пр.Ленина 71', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (570, 312, 61.422918, 55.161281, 'Россия, Челябинск, проспект Ленина, 30',
        'город Челябинск пересеч.пр.Ленина 30 и ул.3-го Интернационала', 'город Челябинск пр.Ленина 30', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (571, 313, 61.396939, 55.184562, 'Россия, Челябинск, проспект Победы, 170', 'город Челябинск пр.Победы 170',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (572, 314, 61.389446, 55.151841, 'Россия, Челябинск, улица Воровского',
        'город Челябинск пересеч.ул.Воровского (из центра) и Свердловского пр. (в центр)',
        'город Челябинск ул.Воровского / Свердловского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (573, 315, 61.412563, 55.158535, 'Россия, Челябинск, улица Свободы',
        'город Челябинск пересеч.ул.Свободы (в центр) и ул.Тимирязева (в центр)',
        'город Челябинск ул.Свободы / ул.Тимирязева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (574, 316, 61.387525, 55.11372, 'Россия, Челябинск, Троицкий тракт, 46', 'город Челябинск Троицкий тр. 46', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (575, 317, 61.284198, 55.178986, 'Россия, Челябинск, улица Салавата Юлаева',
        'город Челябинск пересечение ул.С.Юлаева и пр.Победы', 'город Челябинск ул.С.Юлаева / пр.Победы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (576, 318, 61.388342, 55.161821, 'Россия, Челябинск, Свердловский проспект, 62',
        'город Челябинск Свердловский пр. напротив д.62', 'город Челябинск Свердловский пр. д.62', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (577, 319, 61.355644, 55.177298, 'Россия, Челябинск, улица Чайковского, 185',
        'город Челябинск ул.Ун. Набережная  напротив д. 185 по ул.Чайковского поз.2',
        'город Челябинск ул.Чайковского д.185', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (578, 320, 61.48686, 55.105825, 'Россия, Челябинск, Новороссийская улица, 25',
        'город Челябинск ул.Новороссийская  напротив д. 25', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (579, 321, 61.294567, 55.171879, 'Россия, Челябинск, улица Салавата Юлаева, 29',
        'город Челябинск ул.С. Юлаева  напротив д.29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (580, 322, 61.355644, 55.177298, 'Россия, Челябинск, улица Чайковского, 185',
        'город Челябинск ул.Ун. набережная  напротив д. 185 по ул.Чайковского  поз.1',
        'город Челябинск по ул.Чайковского д.185', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (581, 323, 61.284198, 55.178986, 'Россия, Челябинск, улица Салавата Юлаева',
        'город Челябинск пересечение ул.С.Юлаева и пр.Победы (из центра)', 'город Челябинск ул.С.Юлаева / пр.Победы',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (582, 324, 61.388019, 55.165652, 'Россия, Челябинск, Свердловский проспект, 48',
        'город Челябинск Свердловский пр.  напротив д.48', 'город Челябинск Свердловский пр.  д.48', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (583, 325, 61.393166, 55.14752, 'Россия, Челябинск, улица Курчатова, 7', 'город Челябинск ул.Курчатова 7', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (584, 326, 61.355644, 55.177298, 'Россия, Челябинск, улица Чайковского, 185',
        'город Челябинск ул.Ун. набережная  напротив д. 185 по ул.Чайковского  поз. 3',
        'город Челябинск  по ул.Чайковского д.185', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (585, 327, 61.366567, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 89',
        'город Челябинск ул.Бр.Кашириных 89', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (586, 328, 61.331703, 55.19927, 'Россия, Челябинск, улица Молодогвардейцев, 19',
        'город Челябинск ул.Молодогвардейцев 19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (587, 329, 61.272145, 55.187476, 'Россия, Челябинск, улица Салавата Юлаева, 34',
        'город Челябинск ул.С.Юлаева  напротив д. 34  на пересечении с ул.250 лет Челябинска ',
        'город Челябинск ул.С.Юлаева д.34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (588, 330, 61.344208, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 101',
        'город Челябинск ул.Бр.Кашириных напротив д.101', 'город Челябинск ул.Бр.Кашириных д.101', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (591, 331, 61.353266, 55.174503, 'Россия, Челябинск, улица Университетская Набережная',
        'город Челябинск пересечение ул. Университеская Набережная и ул. Чайковского',
        'город Челябинск ул. Университеская Набережная / ул. Чайковского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (592, 332, 61.299876, 55.184849, 'Россия, Челябинск, проспект Победы, 315',
        'город Челябинск пересечение пр. Победы  315 и ул. 40 лет Победы', 'город Челябинск пр. Победы  315', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (593, 333, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 1', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (594, 334, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 2', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (595, 335, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 3', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (596, 336, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 1', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (597, 337, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 2', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (598, 338, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 3', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (599, 339, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 1', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (600, 340, 61.375389, 55.228259, 'Россия, Челябинск, Черкасская улица, 19',
        'город Челябинск ул.Черкасская 19 поз. 2', 'город Челябинск ул.Черкасская 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (601, 341, 61.391684, 55.155978, 'Россия, Челябинск, улица Карла Либкнехта, 20',
        'город Челябинск ул.Карла Либкнехта  20', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (602, 342, 61.417627, 55.151652, 'Россия, Челябинск, Российская улица, 262',
        'город Челябинск ул.Российская  262', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (603, 343, 61.393139, 55.179108, 'Россия, Челябинск, улица Калинина, 17',
        'город Челябинск ул.Калинина  17(середина дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (606, 344, 61.301493, 55.147706, 'Россия, Челябинск, посёлок Шершни, Центральная улица, 3В',
        'город Челябинск Западное шоссе (ул. Центральная 3в)', 'город Челябинск ул. Центральная 3в', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (611, 345, 61.402055, 55.137945, 'Россия, Челябинск, улица Доватора',
        'город Челябинск пересечение ул.Доватора и ул. Федорова  возле АЗС Лукойл',
        'город Челябинск ул.Доватора / ул. Федорова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (612, 346, 61.366738, 55.128223, 'Россия, Челябинск, улица Блюхера, 69',
        'город Челябинск ул.Блюхера  напротив д. 69', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (613, 347, 61.30276, 55.149553, 'Россия, Челябинск, посёлок Шершни, улица Гидрострой, 13А',
        'город Челябинск Гидрострой  13а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (617, 348, 61.2888, 55.181966, 'Россия, Челябинск, проспект Победы, 398к1',
        'город Челябинск пересечение пр.Победы 398/1и и ул.Чичерина', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (624, 349, 61.426601, 55.168593, 'Россия, Челябинск, улица Труда, 22',
        'город Челябинск пересечение ул. Труда и ул.Свободы  напротив д. 16', 'город Челябинск ул. Труда д. 16', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (625, 350, 61.301493, 55.147706, 'Россия, Челябинск, посёлок Шершни, Центральная улица, 3В',
        'город Челябинск ул.Центральная  напротив д. 3в', 'город Челябинск ул.Центральная  д.3в', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (627, 351, 61.394541, 55.191264, 'Россия, Челябинск, Кожзаводская улица, 1',
        'город Челябинск пересечение ул. Кожзаводской и ул.Каслинской  напротив д. 5а',
        'город Челябинск ул. Кожзаводской д. 5а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (628, 352, 61.384614, 55.193217, 'Россия, Челябинск, Комсомольский проспект, 2',
        'город Челябинск пересечение Свердловского пр. и Комсомольского пр. 2', 'город Челябинск Комсомольского пр. 2',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (631, 353, 61.433806, 55.185764, 'Россия, Челябинск, проспект Победы, 115',
        'город Челябинск пересечение пр. Победы 115 и ул. Горького 68/1', 'город Челябинск пр. Победы 115', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (632, 354, 61.439807, 55.136267, 'Россия, Челябинск, Ленинский район, улица Гагарина, 23',
        'город Челябинск ул.Гагарина 23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (634, 355, 61.41327, 55.157855, 'Россия, Челябинск, улица Свободы, 145', 'город Челябинск ул.Свободы 145', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (636, 356, 61.373646, 55.123173, 'Россия, Челябинск, Центральная улица, 1',
        'город Челябинск пересечение ул.Гостевой и ул. Центральная 1а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (638, 357, 61.375209, 55.147361, 'Россия, Челябинск, улица Худякова, 13', 'город Челябинск ул.Худякова 13', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (639, 358, 61.413639, 55.140091, 'Россия, Челябинск, улица Степана Разина, 9',
        'город Челябинск ул.Степана Разина 9/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (640, 359, 61.380877, 55.158369, 'Россия, Челябинск, улица Энгельса, 32', 'город Челябинск ул.Энгельса 32/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (641, 360, 61.293651, 55.182444, 'Россия, Челябинск, проспект Победы, 325',
        'город Челябинск пересечение ул. Чичерина и пр.Победы 325', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (643, 361, 61.442852, 55.170496, 'Россия, Челябинск, улица Горького, 22', 'город Челябинск ул.Горького 22', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (644, 362, 61.374562, 55.170784, 'Россия, Челябинск, улица Труда, 166', 'город Челябинск ул.Труда 166', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (645, 363, 61.375541, 55.148699, 'Россия, Челябинск, улица Энтузиастов, 32', 'город Челябинск ул.Энтузиастов 32',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (646, 364, 61.326161, 55.195159, 'Россия, Челябинск, Комсомольский проспект, 64',
        'город Челябинск Комсомольский пр. 64', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (647, 365, 61.3797, 55.148272, 'Россия, Челябинск, улица Энгельса, 58',
        'город Челябинск пересечение ул.Энгельса напротив д.58 и ул. Худякова', 'город Челябинск  ул.Энгельса д.58',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (648, 366, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск пересечение ул. Артиллерийской 136 и ул.Ловина', 'город Челябинск ул. Артиллерийской 136', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (649, 367, 61.416477, 55.166799, 'Россия, Челябинск, Российская улица, 159', 'город Челябинск ул.Российская 159',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (650, 368, 61.374095, 55.162551, 'Россия, Челябинск, улица Коммуны, 100',
        'город Челябинск пересечение ул.Коммуны 100 и ул. Энтузиастов', 'город Челябинск пересечение ул.Коммуны 100',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (651, 369, 61.395933, 55.167313, 'Россия, Челябинск, улица Труда, 153', 'город Челябинск ул. Труда 153', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (652, 370, 61.381668, 55.1627, 'Россия, Челябинск, улица Энгельса, 51', 'город Челябинск ул.Энгельса 51', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (653, 371, 0, 0, '', 'город Челябинск пересечение ул.Худякова 12а и ул.Верхнеуральской',
        'город Челябинск пересечение ул.Худякова 12а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (654, 372, 61.388567, 55.145313, 'Россия, Челябинск, улица Блюхера, 3', 'город Челябинск ул.Блюхера 3', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (656, 373, 61.38182, 55.158714, 'Россия, Челябинск, улица Энгельса, 69', 'город Челябинск ул.Энгельса 69', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (657, 374, 61.380716, 55.175493, 'Россия, Челябинск, Краснознамённая улица, 41/1',
        'город Челябинск пересечение ул.Краснознаменная 41/1 и ул.Братьев Кашириных',
        'город Челябинск  ул.Краснознаменная 41/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (658, 375, 61.319639, 55.193479, 'Россия, Челябинск, Комсомольский проспект, 65',
        'город Челябинск пересечение Комсомольского пр. 65 и ул.Солнечная', 'город Челябинск Комсомольского пр. 65',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (662, 376, 61.400155, 55.183739, 'Россия, Челябинск, проспект Победы, 159А',
        'город Челябинск пересечение пр.Победы 159а и ул. Кирова', 'город Челябинск пр.Победы 159а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (669, 377, 61.32696, 55.194023, 'Россия, Челябинск, Комсомольский проспект, 55',
        'город Челябинск Комсомольский пр.  55', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (670, 378, 61.392457, 55.173134, 'Россия, Челябинск, Каслинская улица, 64',
        'город Челябинск ул.Каслинская 64  Торговый центр  со стороны ул. Бр.Кашириных',
        'город Челябинск ул.Каслинская 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (671, 379, 61.392457, 55.173134, 'Россия, Челябинск, Каслинская улица, 64',
        'город Челябинск ул.Каслинская 64  Торговый центр  парковка магазина "Каслинский"',
        'город Челябинск ул.Каслинская 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (672, 380, 61.385953, 55.183225, 'Россия, Челябинск, Свердловский проспект, 28А',
        'город Челябинск пр. Победы/ Свердловский пр.  28А  трамв. ост. "пр. Победы"  поз.2',
        'город Челябинск Свердловский пр.  28А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (673, 381, 61.371319, 55.039729, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 6',
        'город Челябинск ул. Кирова  10  трамв. ост. "ул. Калинина"  поз. 1', 'город Челябинск ул. Кирова  10', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (674, 382, 61.400514, 55.173005, 'Россия, Челябинск, улица Кирова, 25А',
        'город Челябинск ул. Кирова  25А  трамв. ост. "Цирк"  поз.1', 'город Челябинск ул. Кирова  25А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (675, 383, 61.400514, 55.173005, 'Россия, Челябинск, улица Кирова, 25А',
        'город Челябинск ул. Кирова  25А  трамв. ост. "Цирк"  поз.2', 'город Челябинск ул. Кирова  25А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (676, 384, 61.398493, 55.184901, 'Россия, Челябинск, проспект Победы, 168',
        'город Челябинск пр. Победы  168  трамв. ост. "Теплотехничесий институт"  поз.1',
        'город Челябинск пр. Победы  168', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (677, 385, 61.399149, 55.174182, 'Россия, Челябинск, улица Кирова, 60А',
        'город Челябинск ул. Кирова  60А  трамв. ост. "Цирк"  поз. 1', 'город Челябинск ул. Кирова  60А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (678, 386, 61.399149, 55.174182, 'Россия, Челябинск, улица Кирова, 60А',
        'город Челябинск ул. Кирова  60А  трамв. ост. "Цирк"  поз. 2', 'город Челябинск ул. Кирова  60А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (679, 387, 61.415956, 55.168331, 'Россия, Челябинск, Российская улица, 151',
        'город Челябинск ул. Российская  151  трамв. ост. "площадь Павших революционеров',
        'город Челябинск ул. Российская  151', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (680, 388, 61.372056, 55.040446, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 1',
        'город Челябинск ул. Кирова 1  трамв.ост. "Теплотех"  поз. 1', 'город Челябинск ул. Кирова 1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (681, 389, 61.372056, 55.040446, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 1',
        'город Челябинск ул. Кирова 1  трамв.ост. "Теплотех"  поз. 2', 'город Челябинск ул. Кирова 1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (682, 390, 61.388252, 55.184346, 'Россия, Челябинск, проспект Победы, 188',
        'город Челябинск пр. Победы  188 трамв. ост. "пр. Победы"  поз.1', 'город Челябинск пр. Победы  188', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (683, 391, 61.397775, 55.183698, 'Россия, Челябинск, проспект Победы, 161',
        'город Челябинск пр. Победы  161 трамв. ост. "Теплотехнический институт"  поз.1',
        'город Челябинск пр. Победы  161', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (684, 392, 61.397775, 55.183698, 'Россия, Челябинск, проспект Победы, 161',
        'город Челябинск пр. Победы  161 трамв. ост. "Теплотехнический институт"  поз.2',
        'город Челябинск пр. Победы  161', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (685, 393, 61.329709, 55.188396, 'Россия, Челябинск, проспект Победы, 289',
        'город Челябинск пр. Победы  289 трамв. ост. "ул. Молодогвардейцев"', 'город Челябинск пр. Победы  289', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (686, 394, 61.337057, 55.189516, 'Россия, Челябинск, проспект Победы, 324',
        'город Челябинск пр. Победы  324 трамв. ост. "ул. Молодогвардейцев"  поз.1', 'город Челябинск пр. Победы  324',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (687, 395, 61.373125, 55.040647, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 3',
        'город Челябинск ул. Кирова  9 трамв. ост. "ул. Калинина"  поз. 1', 'город Челябинск ул. Кирова  9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (688, 396, 61.291746, 55.180629, 'Россия, Челябинск, улица Чичерина, 30',
        'город Челябинск пр. Победы/ ул. Чичерина  30  трамв. ост. "Конечная"  поз. 1',
        'город Челябинск ул. Чичерина  30', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (689, 397, 61.441136, 55.161707, 'Россия, Челябинск, улица Горького, 1Б',
        'город Челябинск ул. Горького 1Б  трамв. ост. "Комсомольская площадь"', 'город Челябинск ул. Горького 1Б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (690, 398, 61.39349, 55.188201, 'Россия, Челябинск, Каслинская улица, 19',
        'город Челябинск ул. Каслинская  19  трамв. ост."ул. Островского"  поз. 1',
        'город Челябинск ул. Каслинская  19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (691, 399, 61.39022, 55.150288, 'Россия, Челябинск, улица Курчатова, 22',
        'город Челябинск Свердловский пр./ ул. Курчатова  22  трамв. ост. "ул. Курчатова"  поз.1',
        'город Челябинск ул. Курчатова  22', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (692, 400, 61.391612, 55.148421, 'Россия, Челябинск, улица Курчатова, 11А',
        'город Челябинск Свердловский пр./ ул. Курчатова  11А  трамв. ост. "ул. Курчатова"  поз.1',
        'город Челябинск ул. Курчатова  11А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (693, 401, 61.461519, 55.181997, 'Россия, Челябинск, улица Комарова, 78', 'город Челябинск ул. Комарова 78',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (694, 402, 61.409102, 55.106788, 'Россия, Челябинск, Приозёрная улица, 4',
        'город Челябинск автодорога Меридиан (ул. Приозерная  4)  поз.1', 'город Челябинск ул. Приозерная  4', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (695, 403, 61.377086, 55.15301, 'Россия, Челябинск, улица Энтузиастов, 15Б',
        'город Челябинск пересечение ул. Энтузиастов  15Б и ул. Витебская', 'город Челябинск ул. Энтузиастов  15Б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (696, 404, 61.428362, 55.044469, 'Россия, Челябинск, посёлок Исаково, улица Калинина, 17',
        'город Челябинск пересечение ул. Калинина  17 и ул. Каслинская', 'город Челябинск ул. Калинина  17', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (697, 405, 61.411474, 55.147207, 'Россия, Челябинск, улица Цвиллинга, 66',
        'город Челябинск пересечение ул.Цвиллинга  66 и ул. Лазерная', 'город Челябинск ул.Цвиллинга  66', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (698, 406, 61.373861, 55.141392, 'Россия, Челябинск, Варненская улица, 15',
        'город Челябинск пересечение ул. Воровского и ул.Варненская  15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (700, 407, 61.38819, 55.112726, 'Россия, Челябинск, Троицкий тракт, 50',
        'город Челябинск Троицкий тр.  50  у Металлобазы', 'город Челябинск Троицкий тр.  50', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (701, 408, 61.38863, 55.162505, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Сверловского пр. и ул. Коммуны  со стороны Алого поля',
        'город Челябинск Сверловского пр./ ул. Коммуны', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (702, 409, 61.400146, 55.177308, 'Россия, Челябинск, улица Кирова, 11',
        'город Челябинск ул. Кирова  напротив д. 11', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (705, 410, 61.384641, 55.147773, 'Россия, Челябинск, улица Воровского, 49', 'город Челябинск ул. Воровского  49',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (706, 411, 61.39292, 55.167583, 'Россия, Челябинск, улица Труда',
        'город Челябинск ул. Труда  50 м до ул. Красная  палисадник музея', 'город Челябинск ул. Труда  / ул. Красная',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (707, 412, 61.386246, 55.162435, 'Россия, Челябинск, улица Коммуны',
        'город Челябинск пересечение ул. Коммуны и ул. Володарского', 'город Челябинск  ул. Коммуны / ул. Володарского',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (708, 413, 0, 0, '',
        'город Челябинск пересечение Свердловского пр. (из центра) и ул. Труда (из центра)  в палисаднике ДС "Юность"',
        'город Челябинск ДС "Юность"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (709, 414, 61.41027, 55.150972, 'Россия, Челябинск, улица Цвиллинга, 58',
        'город Челябинск пересечение ул. Цвиллинга  58 и ул. Евтеева', 'город Челябинск пересечение ул. Цвиллинга  58 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (710, 415, 61.366881, 55.129525, 'Россия, Челябинск, улица Блюхера, 65', 'город Челябинск ул. Блюхера  65', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (711, 416, 61.37361, 55.14006, 'Россия, Челябинск, улица Воровского, 64', 'город Челябинск ул. Воровского  64',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (712, 417, 61.387085, 55.09338, 'Россия, Челябинск, Троицкий тракт, 21', 'город Челябинск Троицкий тр. 21', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (713, 418, 61.318741, 55.077344, 'Россия, Челябинск, Уфимский тракт, 1', 'город Челябинск Уфимский тр.  1', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (714, 419, 61.446917, 55.115797, 'Россия, Челябинск, Новороссийская улица',
        'город Челябинск пересечение ул. Новороссийская (в центр) и ул. Ереванская',
        'город Челябинск ул. Новороссийская / ул. Ереванская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (715, 420, 61.385683, 55.108689, 'Россия, Челябинск, Троицкий тракт, 13', 'город Челябинск Троицкий тр.  13',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (716, 421, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр.(в центр) и ул. Труда  с моста в центр первая',
        'город Челябинск Свердловского пр. / ул. Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (717, 422, 61.358346, 55.170992, 'Россия, Челябинск, улица Труда',
        'город Челябинск ул. Труда (из центра)  135 м до ул. Северокрымская',
        'город Челябинск ул. Труда / ул. Северокрымская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (718, 423, 61.453271, 55.114496, 'Россия, Челябинск, Новороссийская улица',
        'город Челябинск ул. Новороссийская   м от ул. Л.Чайкиной  в центр70',
        'город Челябинск ул. Новороссийская/ ул. Л.Чайкиной', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (719, 424, 61.382144, 55.145895, 'Россия, Челябинск, улица Воровского, 57',
        'город Челябинск пересечение ул. Воровского  57 и  ул. Доватора', 'город Челябинск ул. Воровского  57', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (720, 425, 61.365907, 55.126453, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск пересечение ул. Блюхера и  ул. Дарвина  развязка у Мебельной фабрики',
        'город Челябинск ул. Блюхера /  ул. Дарвина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (721, 426, 61.361698, 55.184711, 'Россия, Челябинск, Северо-Крымская улица',
        'город Челябинск ул. Северо-Крымская  150 м до моста через реку Миасс из центра',
        'город Челябинск ул. Северо-Крымская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (722, 427, 61.384237, 55.148719, 'Россия, Челябинск, улица Воровского, 38', 'город Челябинск ул. Воровского  38',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (723, 428, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. (из центра) и ул. Труда  напротив АЗС "ЛУКОЙЛ"',
        'город Челябинск пересечение Свердловского пр./ ул. Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (724, 429, 61.387704, 55.108483, 'Россия, Челябинск, Троицкий тракт, 52В', 'город Челябинск Троицкий тр.  52В',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (725, 430, 61.363378, 55.119425, 'Россия, Челябинск, улица Блюхера, 95',
        'город Челябинск ул. Блюхера  95  100 м до ул. Кузнецова', 'город Челябинск ул. Блюхера  95', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (726, 431, 61.386052, 55.187738, 'Россия, Челябинск, Свердловский проспект, 16',
        'город Челябинск пересечение Свердловского пр.  16 и ул. Островского', 'город Челябинск Свердловского пр.  16',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (727, 432, 61.410683, 55.148709, 'Россия, Челябинск, улица Цвиллинга, 62',
        'город Челябинск пересечение ул. Цвиллинга 62 и ул. Монакова', 'город Челябинск  ул. Цвиллинга 62', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (728, 433, 61.365902, 55.126452, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск ул. Блюхера (в центр)  после троллейбусного кольца', 'город Челябинск ул. Блюхера', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (729, 434, 61.387085, 55.09338, 'Россия, Челябинск, Троицкий тракт, 21',
        'город Челябинск Троицкий тр.  напротив д. 21', 'город Челябинск Троицкий тр.  д. 21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (730, 435, 61.414178, 55.144042, 'Россия, Челябинск, улица Цвиллинга, 85',
        'город Челябинск ул. Цвиллинга 85  развязка у Привокзальной площади', 'город Челябинск ул. Цвиллинга 85', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (731, 436, 61.365902, 55.126452, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск ул.Блюхера  напротив Областной больницы перед остановкой в центр',
        'город Челябинск ул.Блюхера', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (732, 437, 61.426521, 55.161435, 'Россия, Челябинск, проспект Ленина, 28Д', 'город Челябинск пр.Ленина 28д',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (734, 438, 61.43483, 55.161589, 'Россия, Челябинск, проспект Ленина, 26',
        'город Челябинск пр. Ленина (в центр)  26д  перед ж/д мостом', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (736, 439, 61.430374, 55.161527, 'Россия, Челябинск, проспект Ленина, 26к1',
        'город Челябинск пр.Ленина (в центр)  26/1  перед автомобильным мостом', 'город Челябинск пр.Ленина 26/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (737, 440, 61.424706, 55.161615, 'Россия, Челябинск, проспект Ленина, 28',
        'город Челябинск пр.Ленина (из центра)  28д  перед ж/д мостом', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (739, 441, 61.430374, 55.161527, 'Россия, Челябинск, проспект Ленина, 26к1',
        'город Челябинск пр.Ленина (из центра)  26/1  перед автомобильным мостом', 'город Челябинск пр.Ленина 26/1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (740, 442, 0, 0, '', 'город Челябинск а/д Меридиан  поворот на  реабилитационный центр',
        'город Челябинск реабилитационный центр', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (742, 443, 61.385171, 55.12466, 'Россия, Челябинск, Троицкий тракт, 1',
        'город Челябинск Троицкий тр.  1 3 км от переулка Озерный(в сторону п.Новосинеглазово)',
        'город Челябинск Троицкий тр.  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (743, 444, 61.378353, 55.236956, 'Россия, Челябинск, Черкасская улица',
        'город Челябинск ул.Черкасская поворот на ЧМЗ', 'город Челябинск ул.Черкасская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (744, 445, 61.436591, 55.198812, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск ул.Г.Танкограда пересечение с Копейским переулком',
        'город Челябинск ул.Г.Танкограда / Копейским переулк', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (745, 446, 61.446975, 55.151487, 'Россия, Челябинск, проспект Ленина, 3',
        'город Челябинск пр.Ленина  3 напротив проходной ЧТЗ', 'город Челябинск пр.Ленина  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (748, 447, 61.410791, 55.164207, 'Россия, Челябинск, улица Свободы, 44', 'город Челябинск ул.Свободы 44', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (749, 448, 61.390786, 55.151528, 'Россия, Челябинск, улица Воровского, 21',
        'город Челябинск пересечение ул. Воровского 21 и ул.Блюхера', 'город Челябинск пересечение ул. Воровского 21',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (750, 449, 61.437004, 55.160525, 'Россия, Челябинск, проспект Ленина, 17', 'город Челябинск пр. Ленина  17',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (752, 450, 61.380212, 55.18451, 'Россия, Челябинск, Краснознамённая улица, 29',
        'город Челябинск ул. Краснознаменная  напротив д. 29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (753, 451, 61.447191, 55.178701, 'Россия, Челябинск, улица Героев Танкограда, 92',
        'город Челябинск ул. Героев Танкограда  напротив д. 92', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (754, 452, 61.490535, 55.125844, 'Россия, Челябинск, Копейское шоссе',
        'город Челябинск Копейское шоссе  200 м от виадука  из центра', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (755, 453, 61.352751, 55.108667, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск пересечение ул. Блюхера (в центр) и ул. Корабельная',
        'город Челябинск ул. Блюхера/ ул. Корабельная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (758, 454, 61.421122, 55.185184, 'Россия, Челябинск, проспект Победы, 133',
        'город Челябинск пересечение пр.Победы 133 и ул.Кудрявцева', 'город Челябинск пр.Победы 133', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (759, 455, 0, 0, '', 'город Челябинск пересечение автодороги Меридиан и Вагонного пер.  56',
        'город Челябинск Вагонный пер.  56', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (760, 456, 61.411249, 55.155679, 'Россия, Челябинск, улица Плеханова, 28', 'город Челябинск ул. Плеханова  28 ',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (761, 457, 61.401467, 55.18469, 'Россия, Челябинск, проспект Победы, 166А', 'город Челябинск пр. Победы  166а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (762, 458, 61.465409, 55.18706, 'Россия, Челябинск, улица Комарова, 42', 'город Челябинск пр. Комарова  42',
        'город Челябинск Комарова  42', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (767, 459, 61.385153, 55.187774, 'Россия, Челябинск, улица Островского, 19',
        'город Челябинск ул. Островского  19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (768, 460, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск дорога из Аэропорта (в город)  до поста ГИБДД  у виадука  справа',
        'город Челябинск дорога из Аэропорта', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (769, 461, 61.38103, 55.126303, 'Россия, Челябинск, улица Рылеева, 16',
        'город Челябинск пересечение ул. Рылеева  16 и ул. Клиническая', 'город Челябинск ул. Рылеева  16', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (770, 462, 61.446589, 55.160823, 'Россия, Челябинск, проспект Ленина, 14', 'город Челябинск пр.Ленина 14', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (772, 463, 61.39923, 55.159486, 'Россия, Челябинск, улица Воровского, 2', 'город Челябинск ул.Воровского 4',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (773, 464, 61.400514, 55.173005, 'Россия, Челябинск, улица Кирова, 25А', 'город Челябинск  ул.Кирова  25А', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (775, 465, 61.409803, 55.245353, 'Россия, Челябинск, Хлебозаводская улица, 20',
        'город Челябинск ул. Хлебозаводская  напротив д. 20', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (776, 466, 61.302634, 55.171468, 'Россия, Челябинск, улица Братьев Кашириных, 135к3',
        'город Челябинск ул.Бр. Кашириных  135 к.3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (777, 467, 61.303281, 55.171822, 'Россия, Челябинск, улица Братьев Кашириных, 135к1',
        'город Челябинск ул.Бр. Кашириных  135 к.1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (778, 468, 61.303281, 55.171822, 'Россия, Челябинск, улица Братьев Кашириных, 135к1',
        'город Челябинск ул.Бр. Кашириных  135 к.1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (779, 469, 61.302634, 55.171468, 'Россия, Челябинск, улица Братьев Кашириных, 135к3',
        'город Челябинск ул.Бр. Кашириных  135 к.3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (780, 470, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е стр.1  поз.1', 'город Челябинск пр. Ленина  3е стр.1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (790, 471, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е стр.1  поз.2', 'город Челябинск пр. Ленина  3е стр.1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (800, 472, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е стр.1  поз.3', 'город Челябинск пр. Ленина  3е стр.1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (810, 473, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е стр.1  поз.4', 'город Челябинск пр. Ленина  3е стр.1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (820, 474, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е стр.1  поз.5', 'город Челябинск пр. Ленина  3е стр.1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (830, 475, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.1', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (840, 476, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.2', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (850, 477, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.3', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (860, 478, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.4', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (870, 479, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.5', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (880, 480, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.6', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (890, 481, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр.2  поз.7', 'город Челябинск пр. Ленина  3е  стр.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (900, 482, 61.376386, 55.14609, 'Россия, Челябинск, улица Энтузиастов, 40',
        'город Челябинск ул.Энтузиастов напротив д.40', 'город Челябинск ул.Энтузиастов д.40', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (901, 483, 61.440319, 55.208098, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда (из центра) и ул. Валдайская',
        'город Челябинск ул. Героев Танкограда / ул. Валдайская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (902, 484, 61.296602, 55.169811, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул.Бр. Кашириных (из центра) и ул. Салавата Юлаева',
        'город Челябинск ул.Бр. Кашириных / ул. Салавата Юлаева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (903, 485, 61.369451, 55.234974, 'Россия, Челябинск, Свердловский тракт, 1Ж',
        'город Челябинск Свердловский тр. 1ж', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (904, 486, 61.368651, 55.234692, 'Россия, Челябинск, Свердловский тракт, 1Жк2',
        'город Челябинск Свердловский тр. 1жк2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (905, 487, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр. 2  поз. 1', 'город Челябинск пр. Ленина  3е  стр. 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (915, 488, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр. 2  поз. 2', 'город Челябинск пр. Ленина  3е  стр. 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (925, 489, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр.Ленина д.3е  стр.1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (930, 490, 61.30028, 55.187024, 'Россия, Челябинск, проспект Победы, 382Б',
        'город Челябинск пр. Победы  382б 130 м  до  ул. 40 лет Победы', 'город Челябинск пр. Победы  382б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (937, 491, 61.465319, 55.185466, 'Россия, Челябинск, улица Комарова, 87', 'город Челябинск Комарова пр.  87',
        'город Челябинск Комарова 87', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (941, 492, 61.391181, 55.179046, 'Россия, Челябинск, улица Калинина, 21', 'город Челябинск Калинина  ул  21',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (944, 493, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.(в город)  1200 м до виадука к пос. Н-Синеглазово', 'город Челябинск Троицкий тр.',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (945, 494, 61.38713, 55.162654, 'Россия, Челябинск, улица Коммуны, 80', 'город Челябинск ул. Коммуны  80', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (946, 495, 61.451629, 55.173951, 'Россия, Челябинск, улица Героев Танкограда, 65',
        'город Челябинск ул. Героев Танкограда  65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (947, 496, 61.401664, 55.173889, 'Россия, Челябинск, улица Братьев Кашириных, 12',
        'город Челябинск ул. Бр.Кашириных 12 ', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (948, 497, 61.414304, 55.185841, 'Россия, Челябинск, Российская улица, 41',
        'город Челябинск пересечение пр. Победы (в центр) и ул. Российская  41', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (951, 498, 61.38713, 55.175509, 'Россия, Челябинск, Свердловский проспект, 30',
        'город Челябинск Свердловский пр.  30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (952, 499, 61.328353, 55.195031, 'Россия, Челябинск, Комсомольский проспект, 52',
        'город Челябинск Комсомольский пр.  52  начало дома', 'город Челябинск Комсомольский пр.  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (953, 500, 61.396975, 55.183852, 'Россия, Челябинск, проспект Победы, 163',
        'город Челябинск пр. Победы  163  у магазина "Перекресток"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (954, 501, 61.381129, 55.146491, 'Россия, Челябинск, улица Худякова, 5',
        'город Челябинск ул. Худякова  1 950 м до ул. Лесопарковая  в центр ', 'город Челябинск ул. Худякова  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (958, 502, 61.451629, 55.173951, 'Россия, Челябинск, улица Героев Танкограда, 65',
        'город Челябинск ул. ГероеТанкограда  65 - ул.Крылова', 'город Челябинск ул. ГероеТанкограда  65', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (959, 503, 61.34525, 55.224855, 'Россия, Челябинск, Радонежская улица',
        'город Челябинск пересечение Свердловскго тр. и ул. Радонежская  у АЗС  напротив трамв. депо',
        'город Челябинск Свердловскго тр. / ул. Радонежская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (960, 504, 61.433213, 55.159151, 'Россия, Челябинск, Артиллерийская улица',
        'город Челябинск пересечение автодороги Меридиан и ул.Артиллерийская',
        'город Челябинск автодорога Меридиан / ул.Артиллерийская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (961, 505, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29  у завода "Прибор"', 'город Челябинск Комсомольский пр.  29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (962, 506, 61.403632, 55.184937, 'Россия, Челябинск, проспект Победы, 160А', 'город Челябинск пр. Победы  160-А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (966, 507, 61.381129, 55.146491, 'Россия, Челябинск, улица Худякова, 5',
        'город Челябинск ул. Худякова  1 610 м до ул.Лесопарковая  в центр справа', 'город Челябинск ул. Худякова  1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (967, 508, 61.353242, 55.178593, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул. Бр. Кашириных  и ул. Чайковского',
        'город Челябинск ул. Бр. Кашириных  / ул. Чайковского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (968, 509, 61.451704, 55.161587, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск ул. Гер.Танкограда  220 м от ул. Ловина', 'город Челябинск ул. Гер.Танкограда  / ул. Ловина',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (969, 510, 61.324122, 55.194023, 'Россия, Челябинск, Комсомольский проспект, 61',
        'город Челябинск Комсомольский пр.  61  середина дома', 'город Челябинск Комсомольский пр.  61', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (970, 511, 61.34525, 55.224855, 'Россия, Челябинск, Радонежская улица',
        'город Челябинск пересечение Свердловского тр. и  ул. Радонежская  со стороны Авторынка',
        'город Челябинск Свердловского тр. /  ул. Радонежская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (971, 512, 61.322165, 55.189184, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр. Победы и  ул. Ворошилова', 'город Челябинск  пр. Победы /  ул. Ворошилова',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (972, 513, 61.381129, 55.146491, 'Россия, Челябинск, улица Худякова, 5',
        'город Челябинск ул. Худякова  1 060 м до ул. Лесопарковая  в центр справа', 'город Челябинск ул. Худякова  1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (976, 514, 61.401664, 55.173889, 'Россия, Челябинск, улица Братьев Кашириных, 12',
        'город Челябинск ул. Бр.Кашириных  12  120 м до ул.Кирова', 'город Челябинск ул. Бр.Кашириных  12 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (979, 515, 61.420044, 55.135583, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск автодорога Меридиан (в центр)  300 м до пересечения с ул. Новороссийская',
        'город Челябинск автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (980, 516, 61.452475, 55.166974, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда и ул. 1 - й Пятилетки  у Сада Победы',
        'город Челябинск ул. Героев Танкограда / ул. 1 - й Пятилетки', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (981, 517, 61.328353, 55.195031, 'Россия, Челябинск, Комсомольский проспект, 52',
        'город Челябинск Комсомольский пр.  52  магазин "Олимп"', 'город Челябинск Комсомольский пр.  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (982, 518, 61.365391, 55.147419, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова  780 м до ул. Лесопарковая  в центр справа ',
        'город Челябинск ул. Худякова  / ул. Лесопарковая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (983, 519, 61.311783, 55.176563, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул. Бр. Кашириных (в центр) и   ул. 40 лет Победы',
        'город Челябинск  ул. Бр. Кашириных /   ул. 40 лет Победы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (984, 520, 0, 0, '', 'город Челябинск Свердловский тр.(в центр)  ост. "Ветлечебница"  справа в центр',
        'город Челябинск ост. "Ветлечебница"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (985, 521, 61.418903, 55.119111, 'Россия, Челябинск, Новороссийская улица',
        'город Челябинск автодорога Меридиан 80 м от ул. Новороссийская',
        'город Челябинск автодорога Меридиан / ул. Новороссийская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (987, 522, 61.432144, 55.18596, 'Россия, Челябинск, улица Горького',
        'город Челябинск пересечение пр. Победы пр.(в центр) и  ул. Горького',
        'город Челябинск  пр. Победы пр./  ул. Горького', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (988, 523, 61.358195, 55.193694, 'Россия, Челябинск, Комсомольский проспект, 24',
        'город Челябинск Комсомольский пр.  24  напротив завода "Прибор"', 'город Челябинск Комсомольский пр.  24', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (989, 524, 61.418903, 55.119111, 'Россия, Челябинск, Новороссийская улица',
        'город Челябинск автодорога Меридиан  75 м до ул. Новороссийская',
        'город Челябинск автодорога Меридиан  / ул. Новороссийская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (990, 525, 61.36654, 55.14805, 'Россия, Челябинск, улица Худякова, 16А', 'город Челябинск ул. Худякова 16А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (991, 526, 61.438051, 55.193587, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул.Героев Танкограда (из центра) и ул. Механическая',
        'город Челябинск ул.Героев Танкограда / ул. Механическая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (992, 527, 61.307144, 55.175951, 'Россия, Челябинск, улица Братьев Кашириных, 116',
        'город Челябинск ул.Бр. Кашириных  116', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (993, 528, 61.42882, 55.161157, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск выезд на автодорогу Меридиан с пр. Ленина (под мостом из центра слева)',
        'город Челябинск автодорога Меридиан / пр. Ленина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (994, 529, 61.312174, 55.194008, 'Россия, Челябинск, Комсомольский проспект, 83',
        'город Челябинск Комсомольский пр.  83   конец дома', 'город Челябинск Комсомольский пр.  83', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (995, 530, 61.301224, 55.172907, 'Россия, Челябинск, улица Братьев Кашириных, 130',
        'город Челябинск ул.Бр.Кашириных 130 м до ул.Северо-Крымская  из центра', 'город Челябинск ул.Бр.Кашириных 130',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (996, 531, 61.429844, 55.122612, 'Россия, Челябинск, улица Коммунаров, 70',
        'город Челябинск пересечение ул.Дзержинского и ул.Коммунаров 62', 'город Челябинск ул.Коммунаров 62', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (997, 532, 61.396939, 55.184562, 'Россия, Челябинск, проспект Победы, 170',
        'город Челябинск пр. Победы  170  у магазина "Ровесник"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (998, 533, 61.30664, 55.175288, 'Россия, Челябинск, улица Братьев Кашириных, 118',
        'город Челябинск ул. Бр. Кашириных  118', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (999, 534, 61.406542, 55.185091, 'Россия, Челябинск, проспект Победы, 158', 'город Челябинск пр. Победы  158',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1000, 535, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  развязка в районе Ветлечебницы  320 м от ул.Радонежской  справа в центр',
        'город Челябинск Свердловский тр.  / ул.Радонежской', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1001, 536, 61.304811, 55.173497, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск ул. Бр. Кашириных  120 м до ул. Чичерина  из центра справа',
        'город Челябинск ул. Бр. Кашириных  / ул. Чичерина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1002, 537, 61.411402, 55.163093, 'Россия, Челябинск, улица Свободы',
        'город Челябинск пересечение ул. Свободыи  ул. Коммуны', 'город Челябинск ул. Свободы /  ул. Коммуны', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1003, 538, 0, 0, '', 'город Челябинск выезд на автодорогу Меридиан с пр.Ленина под мостом  справа',
        'город Челябинск автодорогу Меридиан / пр.Ленина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1004, 539, 61.412848, 55.155448, 'Россия, Челябинск, улица Плеханова, 29',
        'город Челябинск пересечение ул. Плеханова  29 и ул. Свободы', 'город Челябинск ул. Плеханова  29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1005, 540, 61.330509, 55.195, 'Россия, Челябинск, Комсомольский проспект, 50',
        'город Челябинск Комсомольский пр.  50  напротив гост. "Виктория"', 'город Челябинск Комсомольский пр.  50',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1006, 541, 61.314384, 55.178594, 'Россия, Челябинск, улица Братьев Кашириных, 106',
        'город Челябинск ул.Бр. Кашириных  106', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1007, 542, 61.396939, 55.184562, 'Россия, Челябинск, проспект Победы, 170',
        'город Челябинск Победы пр.  170  у Теплотехнического института', 'город Челябинск Победы пр.  170', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1008, 543, 61.388225, 55.176666, 'Россия, Челябинск, Свердловский проспект, 41',
        'город Челябинск Свердловский пр.  41 ', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1009, 544, 61.312174, 55.194008, 'Россия, Челябинск, Комсомольский проспект, 83',
        'город Челябинск Комсомольский пр.  83  начало дома', 'город Челябинск Комсомольский пр.  83 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1010, 545, 61.43501, 55.168208, 'Россия, Челябинск, Артиллерийская улица',
        'город Челябинск пересечение ул.1 - й Пятилетки и ул. Артиллерийская', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1011, 546, 61.409929, 55.166768, 'Россия, Челябинск, улица Свободы, 26', 'город Челябинск ул.Свободы   28',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1013, 547, 61.383581, 55.184423, 'Россия, Челябинск, проспект Победы, 194', 'город Челябинск пр.Победы  194',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1014, 548, 0, 0, '',
        'город Челябинск пересечение ул.Труда и Свердловского пр. на кольце напротив Дворца спорта "Юность"',
        'город Челябинск Дворец спорта "Юность"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1015, 549, 61.383105, 55.145617, 'Россия, Челябинск, улица Доватора, 33', 'город Челябинск ул.Доватора 33',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1016, 550, 61.380257, 55.184433, 'Россия, Челябинск, проспект Победы, 202',
        'город Челябинск пересечение пр.Победы 202 и ул.Краснознаменной', 'город Челябинск  пр.Победы 202', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1017, 551, 61.428919, 55.133354, 'Россия, Челябинск, улица Дзержинского, 126',
        'город Челябинск пересечение ул.Дзержинского  126А и  ул. Барбюса', 'город Челябинск ул.Дзержинского 126А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1018, 552, 61.41151, 55.140651, 'Россия, Челябинск, улица Степана Разина, 6',
        'город Челябинск ул. Ст.Разина 6  у гипермаркета "М-Видео"', 'город Челябинск ул. Ст.Разина 6', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1019, 553, 61.433914, 55.162412, 'Россия, Челябинск, улица Ловина',
        'город Челябинск ул.Артиллерийская ул.  70м до пр.Ленина от ул.Ловина  слева',
        'город Челябинск ул.Артиллерийская ул. / ул.Ловина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1020, 554, 61.372157, 55.18359, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр.Победы и ул. Косарева', 'город Челябинск пр.Победы / ул. Косарева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1021, 555, 61.412938, 55.156194, 'Россия, Челябинск, улица Свободы, 82', 'город Челябинск ул. Свободы  82',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1022, 556, 61.370115, 55.23021, 'Россия, Челябинск, Свердловский тракт, 14Б',
        'город Челябинск Свердловский тр. 14б  65 м от поворота на рынок "Северный двор"',
        'город Челябинск Свердловский тр. 14б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1023, 557, 61.308985, 55.176331, 'Россия, Челябинск, улица Братьев Кашириных, 114',
        'город Челябинск ул.Бр. Кашириных  114', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1024, 558, 61.411048, 55.168194, 'Россия, Челябинск, улица Свободы',
        'город Челябинск ул.Свободы  из центра  100 м до ул. Труда  со стороны автостоянки',
        'город Челябинск ул.Свободы  / ул. Труда ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1025, 559, 61.433915, 55.162416, 'Россия, Челябинск, Артиллерийская улица',
        'город Челябинск пересечение ул.Артиллерийская и ул.Ловина', 'город Челябинск ул.Артиллерийская / ул.Ловина',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1026, 560, 61.384614, 55.193217, 'Россия, Челябинск, Комсомольский проспект, 2',
        'город Челябинск пересечение Комсомольского пр.  2 и Свердловского пр.',
        'город Челябинск  Комсомольского пр.  2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1027, 561, 61.408024, 55.138228, 'Россия, Челябинск, улица Доватора, 4',
        'город Челябинск пересечение ул.Доватора 4 и ул.Степана Разина  у виадука в Ленинский район напротив завода им.Колющенко',
        'город Челябинск  ул.Доватора 4', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1037, 562, 61.32414, 55.188411, 'Россия, Челябинск, проспект Победы, 293',
        'город Челябинск пр. Победы  293  начало дома', 'город Челябинск пр. Победы  293', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1040, 563, 61.378353, 55.236956, 'Россия, Челябинск, Черкасская улица',
        'город Челябинск пересечение Свердловского тр. (в центр) и ул. Черкасская  выезд с ЧМЗ  слева',
        'город Челябинск Свердловского тр. / ул. Черкасская ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1041, 564, 61.457234, 55.177452, 'Россия, Челябинск, улица Комарова, 110',
        'город Челябинск пересечение ул. Комарова  110 и ул. Шуменская  за ост. "ул. Кулибина"',
        'город Челябинск  ул. Комарова  110', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1042, 565, 61.318759, 55.176501, 'Россия, Челябинск, улица Братьев Кашириных, 129',
        'город Челябинск пересечение ул. Бр. Кашириных  129 и ул. Солнечная', 'город Челябинск  ул. Бр. Кашириных  129',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1043, 566, 61.457234, 55.177452, 'Россия, Челябинск, улица Комарова, 110',
        'город Челябинск ул. Комарова  110 (середина дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1044, 567, 61.32414, 55.188411, 'Россия, Челябинск, проспект Победы, 293',
        'город Челябинск пр. Победы  293 (середина дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1045, 568, 61.398628, 55.181724, 'Россия, Челябинск, улица Кирова, 4', 'город Челябинск ул. Кирова  4', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1048, 569, 61.414151, 55.15267, 'Россия, Челябинск, улица Свободы, 90', 'город Челябинск ул. Свободы  90', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1049, 570, 61.376431, 55.245522, 'Россия, Челябинск, шоссе Металлургов, 41',
        'город Челябинск ул. Черкасская/ шоссе Металлургов  41', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1051, 571, 61.463145, 55.183415, 'Россия, Челябинск, улица Комарова',
        'город Челябинск ул. Комарова (в центр)  200 м от пересечения с ул. Бажова',
        'город Челябинск ул. Комарова / с ул. Бажова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1052, 572, 61.416612, 55.149465, 'Россия, Челябинск, улица Свободы, 161', 'город Челябинск ул. Свободы  161',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1053, 573, 61.418732, 55.185847, 'Россия, Челябинск, проспект Победы, 154', 'город Челябинск пр. Победы  154',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1054, 574, 61.446248, 55.144783, 'Россия, Челябинск, Ленинский район, улица Гагарина, 1',
        'город Челябинск ул. Гагарина  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1056, 575, 61.397945, 55.153478, 'Россия, Челябинск, улица Елькина, 82', 'город Челябинск ул. Елькина  82',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1057, 576, 61.451704, 55.161587, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда (в центр) и ул. Ловина  у театра ЧТЗ',
        'город Челябинск ул. Героев Танкограда / ул. Ловина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1058, 577, 61.358346, 55.170828, 'Россия, Челябинск, Северо-Крымская улица',
        'город Челябинск пересечение ул. Северо-Крымская и ул. Труда (из центра)  за ост. "Родничок"',
        'город Челябинск ул. Северо-Крымская/ ул. Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1059, 578, 61.438051, 55.193587, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда (в центр) и ул. Механическая',
        'город Челябинск  ул. Героев Танкограда / ул. Механическая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1060, 579, 61.330751, 55.200092, 'Россия, Челябинск, улица Молодогвардейцев, 17',
        'город Челябинск ул. Молодогвардейцев  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1061, 580, 61.440166, 55.174897, 'Россия, Челябинск, улица Горького, 30',
        'город Челябинск пересечение ул. Горького  30 и ул. Котина', 'город Челябинск ул. Горького  30', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1062, 581, 61.448844, 55.175535, 'Россия, Челябинск, улица Героев Танкограда, 104',
        'город Челябинск ул. Героев Танкограда  104', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1063, 582, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр. (в город)  100 м до ул. Новоэлеваторная',
        'город Челябинск Уфимский тр./ ул. Новоэлеваторная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1065, 583, 61.400901, 55.18397, 'Россия, Челябинск, проспект Победы, 159', 'город Челябинск пр. Победы  159',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1066, 584, 61.399392, 55.154486, 'Россия, Челябинск, улица Елькина, 63Б', 'город Челябинск ул. Елькина  63б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1067, 585, 61.380212, 55.18451, 'Россия, Челябинск, Краснознамённая улица, 29',
        'город Челябинск ул. Краснознаменная  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1068, 586, 61.376547, 55.192256, 'Россия, Челябинск, Комсомольский проспект, 10/6',
        'город Челябинск Комсомольский пр.  10/6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1070, 587, 61.293247, 55.179565, 'Россия, Челябинск, улица Чичерина, 32', 'город Челябинск ул. Чичерина 32',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1071, 588, 61.343243, 55.10236, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск пересечение ул. Блюхера (в центр) и ул. Нефтебазовая',
        'город Челябинск  ул. Блюхера / ул. Нефтебазовая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1072, 589, 61.417537, 55.163055, 'Россия, Челябинск, площадь МОПРа, 9',
        'город Челябинск пересечение пл. Мопра  9 и ул. Российская', 'город Челябинск  пл. Мопра  9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1073, 590, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Бродокалмакский тр. (из центра)  ост. "Сад Дружба"', 'город Челябинск ост. "Сад Дружба"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1074, 591, 61.382207, 55.154512, 'Россия, Челябинск, улица Энгельса, 81', 'город Челябинск ул. Энгельса  81',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1075, 592, 61.495394, 55.220341, 'Россия, Челябинск, Бродокалмакский тракт',
        'город Челябинск дорога в аэропорт  Бродокалмакский тр. (из центра)   за поворотом на ТЭЦ-3',
        'город Челябинск Бродокалмакский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1076, 593, 61.418831, 55.185065, 'Россия, Челябинск, проспект Победы, 141', 'город Челябинск  пр. Победы  141',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1077, 594, 61.329709, 55.188396, 'Россия, Челябинск, проспект Победы, 289',
        'город Челябинск пр. Победы  289  150 м до ул. Молодогвардейцев', 'город Челябинск пр. Победы  289', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1078, 595, 61.398394, 55.182768, 'Россия, Челябинск, улица Кирова, 2',
        'город Челябинск ул. Кирова  2 (конец дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1081, 596, 61.417502, 55.146944, 'Россия, Челябинск, улица Свободы, 169',
        'город Челябинск ул.Российская/ ул.Свободы  169', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1083, 597, 61.393993, 55.160602, 'Россия, Челябинск, Красная улица, 65',
        'город Челябинск пересечение ул.Красная  65 и пр.Ленина', 'город Челябинск  ул.Красная  65', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1084, 598, 61.43483, 55.161589, 'Россия, Челябинск, проспект Ленина, 26',
        'город Челябинск пересечение пр.Ленина 26 и ул.Артиллерийская', 'город Челябинск  пр.Ленина 26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1085, 599, 61.428739, 55.161044, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск пересечение пр. Ленина (из центра) и автодороги Меридиан',
        'город Челябинск  пр. Ленина/ автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1086, 600, 61.434255, 55.160597, 'Россия, Челябинск, проспект Ленина, 19', 'город Челябинск пр. Ленина 19А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1089, 601, 61.451772, 55.159506, 'Россия, Челябинск, проспект Ленина, 7',
        'город Челябинск пересечение пр. Ленина 7 и ул. Героев Танкограда', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1092, 602, 61.40621, 55.161224, 'Россия, Челябинск, Центральный район, Советская улица, 38',
        'город Челябинск пересечение пр.Ленина и ул.Советская 38', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1097, 603, 61.415947, 55.161311, 'Россия, Челябинск, Российская улица, 200',
        'город Челябинск пересечение пр. Ленина и ул. Российская  200', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1099, 604, 61.383608, 55.147037, 'Россия, Челябинск, улица Воровского, 53',
        'город Челябинск ул. Воровского  53', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1100, 605, 61.415669, 55.160211, 'Россия, Челябинск, проспект Ленина, 35',
        'город Челябинск пересечение пр. Ленина  35 и ул. Российская', 'город Челябинск  пр. Ленина  35 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1101, 606, 61.373799, 55.225183, 'Россия, Челябинск, Свердловский тракт, 12Б',
        'город Челябинск Свердловский тр.  12Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1102, 607, 61.452195, 55.171858, 'Россия, Челябинск, Салютная улица',
        'город Челябинск пересечение пр. Комарова и ул. Салютная  за остановкой в центр',
        'город Челябинск  пр. Комарова / ул. Салютная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1103, 608, 61.421005, 55.185836, 'Россия, Челябинск, проспект Победы, 146',
        'город Челябинск пересечение пр. Победы  146 и ул. Кудрявцева  до перекрестка в центр',
        'город Челябинск пр. Победы  146', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1113, 609, 61.397661, 55.157809, 'Россия, Челябинск, улица Воровского',
        'город Челябинск пересечение ул. Воровского и ул. Тимирязева  за перекрестком в центр',
        'город Челябинск  ул. Воровского / ул. Тимирязева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1114, 610, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск пересечение пр. Комарова  131 и ул. Шуменская', 'город Челябинск  пр. Комарова  131', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1115, 611, 61.331964, 55.183575, 'Россия, Челябинск, улица Молодогвардейцев, 62',
        'город Челябинск ул. Молодогвардейцев  62', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1116, 612, 61.389446, 55.151841, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. и ул. Воровского  со стороны Горбольницы',
        'город Челябинск  Свердловского пр. / ул. Воровского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1117, 613, 61.396768, 55.077071, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 31',
        'город Челябинск пересечение ул. Гагарина  31 и ул. Дзержинского  96', 'город Челябинск ул. Гагарина  31', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1118, 614, 61.298017, 55.185965, 'Россия, Челябинск, проспект Победы, 384А',
        'город Челябинск пересечение пр. Победы 384А и ул. 40 лет Победы', 'город Челябинск  пр. Победы 384А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1119, 615, 61.382054, 55.167421, 'Россия, Челябинск, улица Труда, 177/1',
        'город Челябинск пересечение ул. Энгельса и ул. Труда  177/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1121, 616, 61.413235, 55.147068, 'Россия, Челябинск, улица Цвиллинга, 77', 'город Челябинск ул. Цвиллинга  77',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1122, 617, 61.365363, 55.153118, 'Россия, Челябинск, Лесопарковая улица, 9А',
        'город Челябинск пересечение ул. Лесопарковая  9А и ул. Витебская', 'город Челябинск  ул. Лесопарковая  9А',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1123, 618, 61.385072, 55.152475, 'Россия, Челябинск, улица Курчатова, 30',
        'город Челябинск ул. Курчатова  напротив д. 30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1124, 619, 61.388109, 55.164665, 'Россия, Челябинск, Свердловский проспект, 54',
        'город Челябинск Свердловский пр.  напротив д. 54', 'город Челябинск Свердловский пр.  д. 54', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1125, 620, 61.423592, 55.185903, 'Россия, Челябинск, проспект Победы, 142',
        'город Челябинск пересечениеи пр. Победы  142 и ул. Артиллерийская', 'город Челябинск  пр. Победы  142', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1126, 621, 61.376673, 55.244839, 'Россия, Челябинск, Черкасская улица, 2Г',
        'город Челябинск ул. Черкасская  2г', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1127, 622, 61.376368, 55.224223, 'Россия, Челябинск, Свердловский тракт, 16Б',
        'город Челябинск Свердловский тр.  16Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1128, 623, 61.332063, 55.192954, 'Россия, Челябинск, улица Молодогвардейцев, 38А',
        'город Челябинск ул. Молодогвардейцев  38А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1129, 624, 61.373304, 55.147171, 'Россия, Челябинск, улица Худякова, 17', 'город Челябинск ул. Худякова  17',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1130, 625, 61.387627, 55.174261, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. и ул. Бр. Кашириных  за мостом из центра',
        'город Челябинск  Свердловского пр. / ул. Бр. Кашириных', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1131, 626, 61.376395, 55.141691, 'Россия, Челябинск, улица Воровского, 73',
        'город Челябинск пересечение ул. Воровского  73 и ул. Тарасова', 'город Челябинск ул. Воровского  73', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1132, 627, 61.394819, 55.068133, 'Россия, Челябинск, Троицкий тракт, 72Бс1',
        'город Челябинск Троицкий тр.  72Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1133, 628, 61.387318, 55.184361, 'Россия, Челябинск, Свердловский проспект, 25',
        'город Челябинск пересечение пр. Победы и Свердловского пр.  25', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1136, 629, 61.416612, 55.149465, 'Россия, Челябинск, улица Свободы, 161', 'город Челябинск ул. Свободы  161',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1137, 630, 61.436663, 55.130878, 'Россия, Челябинск, улица Дзержинского, 91',
        'город Челябинск пересечение ул. Дзержинского  91 и ул. Гагарина', 'город Челябинск  ул. Дзержинского  91', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1138, 631, 61.368741, 55.132598, 'Россия, Челябинск, улица Блюхера, 44',
        'город Челябинск пересечение ул. Блюхера  44 и ул. Рылеева', 'город Челябинск  ул. Блюхера  44', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1139, 632, 61.452475, 55.166974, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда и ул. Первой Пятилетки',
        'город Челябинск ул. Героев Танкограда / ул. Первой Пятилетки', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1140, 633, 61.38748, 55.114827, 'Россия, Челябинск, Троицкий тракт, 42',
        'город Челябинск Троицкий тр.  напротив д. 42', 'город Челябинск Троицкий тр. д. 42', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1141, 634, 61.384884, 55.204813, 'Россия, Челябинск, Автодорожная улица',
        'город Челябинск пересечение Свердловского тр. и ул. Автодорожная (в центр)',
        'город Челябинск  Свердловский тр. / ул. Автодорожная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1142, 635, 61.419433, 55.119003, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск пересечение ул. Новороссийская и автодороги Меридиан',
        'город Челябинск  ул. Новороссийская / автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1143, 636, 61.443202, 55.165827, 'Россия, Челябинск, улица Горького, 12',
        'город Челябинск пересечение ул. Горького  12 и ул. Савина', 'город Челябинск  ул. Горького  12', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1144, 637, 61.335207, 55.180418, 'Россия, Челябинск, улица Молодогвардейцев, 57А',
        'город Челябинск пересечение ул. Молодогвардейцев  57/к1 и ул. Бр. Кашириных',
        'город Челябинск  ул. Молодогвардейцев  57/к1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1145, 638, 61.378317, 55.192261, 'Россия, Челябинск, Комсомольский проспект, 10/1',
        'город Челябинск пересечение Комсомольского пр.  10/1 и ул. Краснознаменная',
        'город Челябинск  Комсомольского пр.  10/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1146, 639, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  автомобильный рынок', 'город Челябинск Свердловский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1147, 640, 61.387489, 55.109281, 'Россия, Челябинск, Троицкий тракт, 52Б', 'город Челябинск Троицкий тр.  52Б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1148, 641, 61.439753, 55.161327, 'Россия, Челябинск, проспект Ленина, 20', 'город Челябинск пр.Ленина  20',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1150, 642, 61.400515, 55.167843, 'Россия, Челябинск, улица Кирова',
        'город Челябинск пересечение ул.Кирова (в центр) и ул.Труда', 'город Челябинск  ул.Кирова / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1151, 643, 61.446589, 55.160823, 'Россия, Челябинск, проспект Ленина, 14',
        'город Челябинск пересечение ул. Рождественского и пр. Ленина  14', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1154, 644, 61.412121, 55.159995, 'Россия, Челябинск, улица Свободы, 139', 'город Челябинск ул.Свободы 139 ',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1155, 645, 61.416945, 55.160195, 'Россия, Челябинск, Российская улица, 249',
        'город Челябинск ул.Российская  249', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1156, 646, 61.397191, 55.157135, 'Россия, Челябинск, улица Воровского, 5', 'город Челябинск ул.Воровского  5',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1157, 647, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск ул.Блюхера  100 м до остановки "АМЗ" (в центр)', 'город Челябинск остановка "АМЗ"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1158, 648, 61.40426, 55.18342, 'Россия, Челябинск, проспект Победы, 151',
        'город Челябинск пересечение пр. Победы  151 и ул. Болейко', 'город Челябинск пересечение пр. Победы  151', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1159, 649, 61.33792, 55.057234, 'Россия, Челябинск, Уфимский тракт, 121/1',
        'город Челябинск Уфимский тр.  123/2 (1868 км + 130 м)', 'город Челябинск Уфимский тр.  123/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1160, 650, 61.393112, 55.069458, 'Россия, Челябинск, Троицкий тракт, 72А', 'город Челябинск Троицкий тр.  72а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1161, 651, 61.280446, 55.190287, 'Россия, Челябинск, улица Чичерина, 2', 'город Челябинск ул. Чичерина  2',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1162, 652, 61.283212, 55.207783, 'Россия, Челябинск, улица Скульптора Головницкого, 16',
        'город Челябинск ул. Бейвеля/ ул. Скульптора Головницкого  16', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1164, 653, 61.371014, 55.175977, 'Россия, Челябинск, улица Братьев Кашириных, 85Б',
        'город Челябинск  ул. Университетская Набережная / ул. Бр. Кашириных  85Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1166, 654, 61.281847, 55.172707, 'Россия, Челябинск, улица Татищева, 262',
        'город Челябинск ул. Татищева  напротив д.262', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1167, 655, 61.28052, 55.199821, 'Россия, Челябинск, улица Бейвеля',
        'город Челябинск пересечение ул. Бейвеля  (в центр)и ул. Хариса Юсупова',
        'город Челябинск  ул. Бейвеля  / ул. Хариса Юсупова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1168, 656, 61.3972, 55.160818, 'Россия, Челябинск, проспект Ленина, 60', 'город Челябинск пр.Ленина 60', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1170, 657, 61.30815, 55.150535, 'Россия, Челябинск, посёлок Шершни, Центральная улица',
        'город Челябинск ул. Центральная  1550 м до поста ГИБДД  в центр', 'город Челябинск ул. Центральная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1171, 658, 61.435351, 55.167591, 'Россия, Челябинск, Артиллерийская улица, 107',
        'город Челябинск ул. Артиллерийская  107', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1172, 659, 61.387543, 55.121695, 'Россия, Челябинск, Троицкий тракт, 20', 'город Челябинск Троицкий тр.  20/2',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1173, 660, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск автодорога Меридиан/ ул. Артиллерийская  136  поз. 2',
        'город Челябинск ул. Артиллерийская  136', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1175, 661, 61.387156, 55.086218, 'Россия, Челябинск, Троицкий тракт, 23Б', 'город Челябинск Троицкий тр.  23б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1176, 662, 61.40992, 55.154887, 'Россия, Челябинск, улица Плеханова, 43', 'город Челябинск ул. Плеханова  43',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1177, 663, 61.415275, 55.17347, 'Россия, Челябинск, Российская улица',
        'город Челябинск пересечение ул. Российская и ул. Нагорная', 'город Челябинск  ул. Российская / ул. Нагорная',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1178, 664, 61.41151, 55.140651, 'Россия, Челябинск, улица Степана Разина, 6',
        'город Челябинск ул. Степана Разина  6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1179, 665, 61.387458, 55.179208, 'Россия, Челябинск, улица Калинина',
        'город Челябинск пересечение ул. Калинина и Свердловского пр.',
        'город Челябинск  ул. Калинина / Свердловского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1180, 666, 61.331946, 55.182017, 'Россия, Челябинск, улица Молодогвардейцев, 66',
        'город Челябинск ул. Молодогвардейцев  66', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1181, 667, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр. (в центр)  400 м от ул. Радонежской',
        'город Челябинск Свердловский тр. / ул. Радонежской', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1182, 668, 61.294477, 55.175781, 'Россия, Челябинск, улица 250-летия Челябинска, 21',
        'город Челябинск ул. 250 лет Челябинску  21', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1183, 669, 61.355671, 55.189989, 'Россия, Челябинск, проспект Победы, 290', 'город Челябинск пр. Победы  290',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1184, 670, 61.411734, 55.11184, 'Россия, Челябинск, улица Артёма, 151',
        'город Челябинск пересечение автодороги Меридиан (в центр) и ул. Артема  151',
        'город Челябинск ул. Артема  151', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1185, 671, 61.493733, 55.097203, 'Россия, Челябинск, Новороссийская улица, 3',
        'город Челябинск ул. Новороссийская  3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1186, 672, 61.304359, 55.180182, 'Россия, Челябинск, улица 250-летия Челябинска, 13',
        'город Челябинск ул. 250 лет Челябинску  13', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1187, 673, 61.441799, 55.187755, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул.Героев Танкограда и пр. Победы  со стороны парка',
        'город Челябинск  ул.Героев Танкограда / пр. Победы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1188, 674, 61.332594, 55.189074, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр. Победы (из центра) и ул. Молодогвардейцев',
        'город Челябинск  пр. Победы / ул. Молодогвардейцев', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1189, 675, 61.444478, 55.186417, 'Россия, Челябинск, улица Героев Танкограда, 23',
        'город Челябинск ул. Героев Танкограда  23  поз. 1', 'город Челябинск ул. Героев Танкограда  23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1190, 676, 61.464613, 55.190546, 'Россия, Челябинск, улица Бажова',
        'город Челябинск пересечение ул. Бажова (из центра) и ул. Комарова  до пересечения',
        'город Челябинск  ул. Бажова / ул. Комарова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1191, 677, 61.4078, 55.160843, 'Россия, Челябинск, проспект Ленина, 50',
        'город Челябинск пересечение пр.Ленина 50 и ул.Пушкина 56', 'город Челябинск  пр.Ленина 50', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1192, 678, 61.420583, 55.160545, 'Россия, Челябинск, проспект Ленина, 29',
        'город Челябинск пересечение пр.Ленина 29 и ул.3-го Интернационала', 'город Челябинск  пр.Ленина 29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1193, 679, 61.323134, 55.179349, 'Россия, Челябинск, улица Ворошилова, 57А',
        'город Челябинск пересечение ул. Бр.Кашириных и ул. Ворошилова 57А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1195, 680, 61.294567, 55.171879, 'Россия, Челябинск, улица Салавата Юлаева, 29',
        'город Челябинск ул.Салавата Юлаева  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1196, 681, 61.32458, 55.194979, 'Россия, Челябинск, Комсомольский проспект, 66',
        'город Челябинск Комсомольский пр.  66/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1197, 682, 61.384542, 55.151204, 'Россия, Челябинск, улица Курчатова, 23',
        'город Челябинск ул.Курчатова  напротив д. 23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1198, 683, 61.457297, 55.174753, 'Россия, Челябинск, улица Комарова, 135', 'город Челябинск пр. Комарова  135',
        'город Челябинск  Комарова  135', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1202, 684, 61.401494, 55.250007, 'Россия, Челябинск, шоссе Металлургов, 3Г',
        'город Челябинск шоссе Металлургов  3г', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1203, 685, 61.377751, 55.129273, 'Россия, Челябинск, улица Рылеева',
        'город Челябинск пересечение Троицкого тр. и ул. Рылеева  за перекрестком в центр',
        'город Челябинск Троицкого тр. / ул. Рылеева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1204, 686, 61.432432, 55.1332, 'Россия, Челябинск, улица Дзержинского, 110',
        'город Челябинск ул. Дзержинского  110', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1205, 687, 61.443822, 55.184619, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда и ул. Потемкина  у Никольской рощи',
        'город Челябинск ул. Героев Танкограда / ул. Потемкина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1206, 688, 61.289456, 55.175889, 'Россия, Челябинск, улица Салавата Юлаева, 17В',
        'город Челябинск ул.Салавата Юлаева  17в', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1207, 689, 61.466167, 55.187327, 'Россия, Челябинск, улица Комарова',
        'город Челябинск пересечение ул. Комарова и ул. Завалишина  за перекрестком из центра',
        'город Челябинск  ул. Комарова / ул. Завалишина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1208, 690, 61.400678, 55.144179, 'Россия, Челябинск, 1-я Окружная улица',
        'город Челябинск пересечение ул. 1-я Окружная и ул. Доватора  до перекрестка из центра',
        'город Челябинск  ул. 1-я Окружная / ул. Доватора', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1209, 691, 61.397748, 55.249284, 'Россия, Челябинск, шоссе Металлургов, 9',
        'город Челябинск пересечение ул. Шоссе Металлургов  9 и ул. Дегтярева',
        'город Челябинск  ул. Шоссе Металлургов  9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1210, 692, 61.386132, 55.150674, 'Россия, Челябинск, улица Воровского, 30',
        'город Челябинск ул. Курчатова/ ул. Воровского  30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1212, 693, 61.332036, 55.184608, 'Россия, Челябинск, улица Молодогвардейцев, 60В',
        'город Челябинск ул. Молодогвардейцев  60в', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1213, 694, 61.449077, 55.178193, 'Россия, Челябинск, улица Героев Танкограда, 55',
        'город Челябинск пересечение ул. Героев Танкограда  55 и ул. Котина',
        'город Челябинск ул. Героев Танкограда  55', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1214, 695, 61.4593, 55.177529, 'Россия, Челябинск, улица Комарова, 131',
        'город Челябинск ул. Комарова  131(середина дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1215, 696, 61.342456, 55.109477, 'Россия, Челябинск, посёлок АМЗ',
        'город Челябинск ул. Блюхера  за остановкой "АМЗ" (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1216, 697, 61.316998, 55.179082, 'Россия, Челябинск, улица Братьев Кашириных, 102',
        'город Челябинск ул.Братьев Кашириных  102 (начало дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1217, 698, 61.41363, 55.153966, 'Россия, Челябинск, улица Свободы, 88',
        'город Челябинск пересечние ул. Свободы  88 и ул. Орджоникидзе', 'город Челябинск  ул. Свободы  88', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1218, 699, 61.441738, 55.18785, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр. Победы и ул. Героев Танкограда  за перекрестком из центра',
        'город Челябинск пр. Победы / ул. Героев Танкограда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1219, 700, 61.331991, 55.186546, 'Россия, Челябинск, улица Молодогвардейцев, 56',
        'город Челябинск ул. Молодогвардейцев  56', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1220, 701, 61.417034, 55.159115, 'Россия, Челябинск, Российская улица, 251',
        'город Челябинск пересечение ул. Российская  251 и ул. Тимирязева', 'город Челябинск ул. Российская  251', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1221, 702, 61.367663, 55.130498, 'Россия, Челябинск, улица Блюхера, 61', 'город Челябинск ул. Блюхера  61',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1222, 703, 61.39225, 55.141521, 'Россия, Челябинск, улица Доватора, 17',
        'город Челябинск пересечение ул. Доватора  17 и ул. Шаумяна', 'город Челябинск  ул. Доватора  17', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1223, 704, 61.417537, 55.163055, 'Россия, Челябинск, площадь МОПРа, 9',
        'город Челябинск пересечение ул. Российская и пл. Мопра 9', 'город Челябинск пл. Мопра 9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1225, 705, 61.401314, 55.175134, 'Россия, Челябинск, улица Кирова, 17А', 'город Челябинск ул. Кирова  17А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1227, 706, 61.380626, 55.184227, 'Россия, Челябинск, проспект Победы, 200с1',
        'город Челябинск пр. Победы  200/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1228, 707, 61.416046, 55.147634, 'Россия, Челябинск, улица Свободы, 102', 'город Челябинск ул. Свободы  102',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1229, 708, 61.380877, 55.158369, 'Россия, Челябинск, улица Энгельса, 32', 'город Челябинск ул. Энгельса  32',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1230, 709, 61.435153, 55.148066, 'Россия, Челябинск, улица Харлова, 9', 'город Челябинск ул. Харлова 9', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1231, 710, 0, 0, '', 'город Челябинск Свердловский тр.  300 м до ост. "Ветлечебница" (в центр)',
        'город Челябинск ост. "Ветлечебница"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1236, 711, 61.455761, 55.175488, 'Россия, Челябинск, улица Комарова, 112',
        'город Челябинск ул. Комарова  112 (начало дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1237, 712, 61.334362, 55.178002, 'Россия, Челябинск, улица Братьев Кашириных, 107',
        'город Челябинск пересечение ул. Бр. Кашириных  107 и ул. Молодогвардейцев',
        'город Челябинск ул. Бр. Кашириных  107 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1238, 713, 61.390705, 55.150659, 'Россия, Челябинск, улица Курчатова, 20',
        'город Челябинск ул. Блюхера/ ул. Курчатова  20', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1241, 714, 0, 0, '', 'город Челябинск пересечение Свердловского тр. и Северного луча  у моста',
        'город Челябинск  Свердловского тр. / Северного луча', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1242, 715, 61.378191, 55.142468, 'Россия, Челябинск, улица Воровского, 71',
        'город Челябинск ул. Воровского  71', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1243, 716, 61.360647, 55.241226, 'Россия, Челябинск, Индивидуальная улица',
        'город Челябинск Свердловский тр. (в центр) 150 м за пересечением ул. Индивидуальная',
        'город Челябинск Свердловский тр. /  ул. Индивидуальная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1244, 717, 0, 0, '', 'город Челябинск пересечение Троицкого тр.  38 и ул. Шарова',
        'город Челябинск пересечение Троицкого тр.  38', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1245, 718, 61.413989, 55.177632, 'Россия, Челябинск, Российская улица, 40',
        'город Челябинск ул. Российская  40', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1246, 719, 61.381048, 55.140188, 'Россия, Челябинск, улица Блюхера, 13',
        'город Челябинск пересечение ул. Блюхера  13 и ул. Тарасова', 'город Челябинск  ул. Блюхера  13', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1247, 720, 61.36151, 55.208153, 'Россия, Челябинск, Автодорожная улица',
        'город Челябинск пересечение Свердловского тр. и ул. Автодорожная  за ост. "Цинковый завод"(в центр)',
        'город Челябинск  Свердловского тр. / ул. Автодорожная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1248, 721, 61.418642, 55.15554, 'Россия, Челябинск, Российская улица, 277',
        'город Челябинск ул. Российская  277', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1249, 722, 61.385432, 55.11339, 'Россия, Челябинск, Троицкий тракт, 11А/4',
        'город Челябинск Троицкий тр.  11а/4', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1250, 723, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  350 м от поста ГИБДД  в центр ', 'город Челябинск Свердловский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1251, 724, 61.441433, 55.186792, 'Россия, Челябинск, улица Героев Танкограда, 40',
        'город Челябинск пересечение ул. Героев Танкограда  40 и пр. Победы',
        'город Челябинск ул. Героев Танкограда  40 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1252, 725, 61.366567, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 89',
        'город Челябинск ул. Бр. Кашириных  89 (конец дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1253, 726, 61.399131, 55.149192, 'Россия, Челябинск, улица Елькина, 81А', 'город Челябинск ул. Елькина  81А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1254, 727, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  400 м до поста ГИБДД  из центра ', 'город Челябинск Свердловский тр. ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1255, 728, 61.434875, 55.180819, 'Россия, Челябинск, улица Горького, 63', 'город Челябинск ул. Горького  63',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1256, 729, 61.334102, 55.18158, 'Россия, Челябинск, улица Молодогвардейцев, 47А',
        'город Челябинск ул. Молодогвардейцев  47а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1257, 730, 61.360647, 55.241226, 'Россия, Челябинск, Индивидуальная улица',
        'город Челябинск пересечение Свердловского тр.(из центра) и ул. Индивидуальная',
        'город Челябинск Свердловский тр. / ул. Индивидуальная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1258, 731, 61.46955, 55.11717, 'Россия, Челябинск, улица Машиностроителей, 34',
        'город Челябинск пересечение ул. Машиностроителей  34 и ул. Масленникова',
        'город Челябинск  ул. Машиностроителей  34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1259, 732, 61.361779, 55.237171, 'Россия, Челябинск, Рабоче-Крестьянская улица',
        'город Челябинск Свердловский тр. (из центра)  100 м до пересечения с ул. Рабочекрестьянская',
        'город Челябинск Свердловский тр. / пересечения с ул. Рабочекрестьянская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1260, 733, 61.469083, 55.116377, 'Россия, Челябинск, улица Машиностроителей, 36',
        'город Челябинск пересечение ул. Машиностроителей  36 и ул. Масленникова',
        'город Челябинск  ул. Машиностроителей  36', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1261, 734, 61.416477, 55.166799, 'Россия, Челябинск, Российская улица, 159',
        'город Челябинск пересечение ул.Российская  159 и ул. Труда', 'город Челябинск  ул.Российская  159', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1262, 735, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск ул.Воровского  со стороны главного корпуса Мед. Академии  ', 'город Челябинск Мед. Академия ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1263, 736, 61.302275, 55.176023, 'Россия, Челябинск, улица Чичерина, 35', 'город Челябинск ул. Чичерина  35',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1264, 737, 61.394819, 55.068133, 'Россия, Челябинск, Троицкий тракт, 72Бс1',
        'город Челябинск Троицкий тр.  72б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1265, 738, 61.392825, 55.248781, 'Россия, Челябинск, шоссе Металлургов, 19',
        'город Челябинск пересечение ул. 50 лет ВЛКСМ и шоссе Металлургов  19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1267, 739, 61.341845, 55.178121, 'Россия, Челябинск, улица Братьев Кашириных, 103',
        'город Челябинск ул. Бр. Кашириных  103', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1268, 740, 61.417618, 55.15427, 'Россия, Челябинск, Российская улица, 224',
        'город Челябинск ул. Российская  226', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1269, 741, 61.342456, 55.109477, 'Россия, Челябинск, посёлок АМЗ',
        'город Челябинск ул. Блюхера конечн. ост." АМЗ" (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1270, 742, 61.442403, 55.161707, 'Россия, Челябинск, улица Горького, 4', 'город Челябинск ул. Горького  4',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1271, 743, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  240 м от поворота на ЧВВАКУШ  справа в город',
        'город Челябинск Свердловский тр. ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1272, 744, 61.381353, 55.166043, 'Россия, Челябинск, улица Энгельса, 41', 'город Челябинск ул. Энгельса  41',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1273, 745, 61.332009, 55.190719, 'Россия, Челябинск, улица Молодогвардейцев, 48',
        'город Челябинск ул. Молодогвардейцев  48', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1274, 746, 61.4646, 55.161563, 'Россия, Челябинск, проспект Ленина, 2',
        'город Челябинск пр. Ленина  2  у главной проходной ЧТЗ   поз.1', 'город Челябинск пр. Ленина  2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1276, 747, 61.332633, 55.178684, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск ул. Бр. Кашириных (в центр)  300 м до ул.Молодогвардейцев  ',
        'город Челябинск ул. Бр. Кашириных / ул.Молодогвардейцев  ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1279, 748, 61.403311, 55.250885, 'Россия, Челябинск, шоссе Металлургов',
        'город Челябинск шоссе Металлургов (из центра)  50 м до пересечения с ул. Сталеваров',
        'город Челябинск шоссе Металлургов / ул. Сталеваров', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1280, 749, 61.358346, 55.170992, 'Россия, Челябинск, улица Труда',
        'город Челябинск ул. Труда (из центра)  200 м до ул. Северо-Крымская',
        'город Челябинск ул. Труда / ул. Северо-Крымская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1281, 750, 61.449446, 55.171802, 'Россия, Челябинск, Салютная улица, 16', 'город Челябинск ул. Салютная  16',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1282, 751, 61.333581, 55.18103, 'Россия, Челябинск, улица Молодогвардейцев, 53',
        'город Челябинск ул. Молодогвардейцев  53', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1283, 752, 61.41204, 55.150345, 'Россия, Челябинск, улица Цвиллинга, 59А',
        'город Челябинск пересечение ул. Евтеева и ул. Цвиллинга  59а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1285, 753, 61.332, 55.180598, 'Россия, Челябинск, улица Молодогвардейцев, 68',
        'город Челябинск ул. Молодогвардейцев  68', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1286, 754, 61.4646, 55.161563, 'Россия, Челябинск, проспект Ленина, 2',
        'город Челябинск пр. Ленина  2  у главной проходной ЧТЗ   поз.2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1288, 755, 61.387471, 55.088898, 'Россия, Челябинск, Троицкий тракт, 23',
        'город Челябинск Троицкий тр.  23    у "Сельхозтехники" ', 'город Челябинск Троицкий тр.  23 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1289, 756, 61.347882, 55.177853, 'Россия, Челябинск, улица Братьев Кашириных, 99А',
        'город Челябинск ул. Бр. Кашириных  99А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1290, 757, 61.375784, 55.161029, 'Россия, Челябинск, улица Энтузиастов, 1',
        'город Челябинск пересечение ул. Энтузиастов  1 и пр. Ленина  68', 'город Челябинск ул. Энтузиастов  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1291, 758, 61.378613, 55.242006, 'Россия, Челябинск, Черкасская улица, 2', 'город Челябинск ул. Черкасская 2б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1292, 759, 61.438051, 55.193587, 'Россия, Челябинск, Механическая улица',
        'город Челябинск пересечение ул. Механической и ул. Героев Танкограда ',
        'город Челябинск  ул. Механической / ул. Героев Танкограда ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1293, 760, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск развязка Свердловского пр. и ул. Труда  трамв. ост. "ДС Юность"',
        'город Челябинск  Свердловский пр./ ул. Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1294, 761, 61.365786, 55.178141, 'Россия, Челябинск, улица Братьев Кашириных, 91А',
        'город Челябинск ул. Бр. Кашириных  напротив д. 91А', 'город Челябинск ул. Бр. Кашириных  д. 91А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1297, 762, 61.332592, 55.194406, 'Россия, Челябинск, улица Молодогвардейцев',
        'город Челябинск пересечение ул. Молодогвардейцев (из центра) и Комсомольского пр.  до перекрестка',
        'город Челябинск  ул. Молодогвардейцев / Комсомольского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1298, 763, 61.461573, 55.180547, 'Россия, Челябинск, улица Комарова, 125', 'город Челябинск пр. Комарова  125',
        'город Челябинск  Комарова  125', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1302, 764, 61.378578, 55.241421, 'Россия, Челябинск, Черкасская улица, 4', 'город Челябинск ул. Черкасская  4',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1303, 765, 61.387552, 55.112402, 'Россия, Челябинск, Троицкий тракт, 52', 'город Челябинск Троицкий тр.  52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1304, 766, 61.351679, 55.107994, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск пересечение ул. Блюхера (из центра) и ул. Новоэлеваторная',
        'город Челябинск ул. Блюхера / ул. Новоэлеваторная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1305, 767, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.   250 м от поворота на ЧВВАКУШ', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1306, 768, 61.40959, 55.253976, 'Россия, Челябинск, шоссе Металлургов',
        'город Челябинск пересечение шоссе Металлургов и ул. Строительная',
        'город Челябинск  шоссе Металлургов / ул. Строительная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1307, 769, 0, 0, '',
        'город Челябинск пересечение Комсомольского пр. 23 Д и ул.Косарева  за перекр. в центр  авт.ост. "ул.Косарева"',
        'город Челябинск пересечение Комсомольского пр. 23Д', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1308, 770, 61.374068, 55.160221, 'Россия, Челябинск, проспект Ленина, 74',
        'город Челябинск пересечение пр.Ленина 74 и ул.Энтузиастов  за перекр. из центра  тролл.ост. "Инст. Гражданпроект"',
        'город Челябинск пересечение пр.Ленина 74', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1309, 771, 61.32138, 55.194387, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересечение Комсомольского пр.и ул.Ворошилова  за перекр.в центр  авт.ост."ул.Ворошилова"',
        'город Челябинск Комсомольского пр. / ул.Ворошилова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1310, 772, 61.434255, 55.160597, 'Россия, Челябинск, проспект Ленина, 19',
        'город Челябинск пересечение пр.Ленина 19 и ул.Артиллерийская  за перекр. из центра  авт.ост."к-т Спартак" ',
        'город Челябинск пересечение пр.Ленина 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1311, 773, 61.380729, 55.16762, 'Россия, Челябинск, улица Энгельса',
        'город Челябинск пересечение ул.Энгельса и ул.Труда  до перекр.из центра  тролл. ост."ул.Труда"  ',
        'город Челябинск  ул.Энгельса / ул.Труда  ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1312, 774, 61.45144, 55.160221, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пересечение ул.Г.Танкограда и пр.Ленина  за перекр.из центра  авт.ост. "Театр ЧТЗ"  поз.2',
        'город Челябинск ул.Г.Танкограда / пр.Ленина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1313, 775, 61.359955, 55.193201, 'Россия, Челябинск, Комсомольский проспект, 22',
        'город Челябинск пересечение Комсомольского пр. 22 и ул.Чайковского  за перекр.из центра  авт.ост."ул.Чайковского"',
        'город Челябинск  Комсомольского пр. 22', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1314, 776, 61.417007, 55.145483, 'Россия, Челябинск, улица Свободы, 173',
        'город Челябинск ул.Свободы 173  тролл.ост."Ж.Д. Институт"', 'город Челябинск ул.Свободы 173', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1315, 777, 61.388193, 55.167675, 'Россия, Челябинск, улица Труда',
        'город Челябинск пересечение ул.Труда и Свердловского пр.   в центр  трам.ост."ДС Юность"',
        'город Челябинск  ул.Труда / Свердловского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1316, 778, 61.40364, 55.167937, 'Россия, Челябинск, улица Цвиллинга',
        'город Челябинск пересечение ул.Цвиллинга и ул.Труда  за перекр.в центр  ост.трам "Оперный театр"',
        'город Челябинск  ул.Цвиллинга / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1317, 779, 61.315713, 55.194013, 'Россия, Челябинск, Комсомольский проспект, 69',
        'город Челябинск Комсомольский пр.  69  в центр  ост. "Солнечная"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1319, 780, 61.338432, 55.194147, 'Россия, Челябинск, Комсомольский проспект, 41',
        'город Челябинск Комсомольский пр. 41  в центр  авт.ост. "Поликлиника"', 'город Челябинск Комсомольский пр. 41',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1320, 781, 61.398493, 55.184901, 'Россия, Челябинск, проспект Победы, 168',
        'город Челябинск пр.Победы 168  авт.ост. "Теплотехнический институт"  в сторону Свердловского пр.',
        'город Челябинск пр.Победы 168', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1321, 782, 61.37794, 55.147983, 'Россия, Челябинск, улица Худякова, 6',
        'город Челябинск пересечение ул.Худякова 6 и ул.Энгельса  за перекр.из центра  авт. ост. "ул.Худякова"',
        'город Челябинск пересечение ул.Худякова 6', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1322, 783, 61.388823, 55.16005, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пересечение пр.Ленина и Свердловского пр.  за перекр.из центра  ост."Алое поле"',
        'город Челябинск  пр.Ленина / Свердловского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1323, 784, 61.361349, 55.192472, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересечение Комсомольского пр. и ул.Чайковского  до перекр.в центр  авт.ост."ул.Чайковского"  поз.2',
        'город Челябинск  Комсомольского пр. / ул.Чайковского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1324, 785, 61.45144, 55.160221, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пересечение ул.Г.Танкограда и пр.Ленина  за перекр.из центра  авт.ост. "Театр ЧТЗ"  поз.1',
        'город Челябинск ул.Г.Танкограда / пр.Ленина ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1325, 786, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пересечение пр.Ленина 76 и ул.Лесопарковая  200 м до перекр. из центра  авт. ост. "ЮрГУ"  поз.1',
        'город Челябинск пр.Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1326, 787, 61.415139, 55.145097, 'Россия, Челябинск, улица Свободы, 108', 'город Челябинск ул.Свободы 108',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1327, 788, 0, 0, '',
        'город Челябинск пересечение Свердловского пр. 16 и ул.Островского  за перекр.в центр  ост.автобуса "ул.Островского"  в центр',
        'город Челябинск пересечение Свердловского пр. 16', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1328, 789, 0, 0, '',
        'город Челябинск пересечение Комсомольского пр. 14 и ул.Косарева  за перекр. из центра  авт.ост. "ул.Косарева"',
        'город Челябинск пересечение Комсомольского пр. 14', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1329, 790, 61.404305, 55.166686, 'Россия, Челябинск, улица Цвиллинга, 11',
        'город Челябинск пересечение ул.Цвиллинга 11 и ул.Труда  до перекр.из центра  трам. ост. "Оперный театр"',
        'город Челябинск пересечение ул.Цвиллинга 11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1330, 791, 61.368741, 55.132598, 'Россия, Челябинск, улица Блюхера, 44',
        'город Челябинск пересечение ул.Блюхера 44 и ул.Рылеева  за перекр.из центра  тролл.ост. "ул.Рылеева"',
        'город Челябинск пересечение ул.Блюхера 44', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1331, 792, 61.451943, 55.160648, 'Россия, Челябинск, проспект Ленина, 8А',
        'город Челябинск пересечение пр.Ленина 8а и ул.Г.Танкограда  до перекр. в центр  трам.ост."Театр ЧТЗ"',
        'город Челябинск пересечение пр.Ленина 8а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1332, 793, 61.32138, 55.194387, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересечение Комсомольского пр. и ул.Ворошилова  за перекр.из центра  авт.ост."ул.Ворошилова"',
        'город Челябинск  Комсомольского пр. / ул.Ворошилова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1333, 794, 61.3992, 55.1796, 'Россия, Челябинск, улица Кирова',
        'город Челябинск пересечение ул.Кирова и ул.Калинина  за перекр.в центр  авт.ост "ул. Калинина" ',
        'город Челябинск  ул.Кирова / ул.Калинина ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1334, 795, 61.410162, 55.158246, 'Россия, Челябинск, улица Тимирязева',
        'город Челябинск пересечение ул.Тимирязева 64 и ул.Пушкина  за перекр.в центр  тролл.ост. "Кинотеатр Пушкина"',
        'город Челябинск  ул.Тимирязева 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1335, 796, 61.380051, 55.160411, 'Россия, Челябинск, проспект Ленина, 66А',
        'город Челябинск пересечение пр.Ленина 66А и ул.Энгельса  за перекр.из центра  авт.ост. "ул. Энгельса" ',
        'город Челябинск пр.Ленина 66А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1336, 797, 61.348277, 55.194018, 'Россия, Челябинск, Комсомольский проспект, 35',
        'город Челябинск пересечение Комсомольского пр. 35 и ул.Пионерская  за перекр.в центр  авт.ост. "ул.Пионерская"',
        'город Челябинск  Комсомольского пр. 35', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1337, 798, 61.361349, 55.192472, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересечение Комсомольского пр. и ул.Чайковского  до перекр.в центр  авт.ост."ул.Чайковского"  поз.1',
        'город Челябинск  Комсомольского пр. / ул.Чайковского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1338, 799, 61.433303, 55.044908, 'Россия, Челябинск, посёлок Исаково, улица Калинина, 32',
        'город Челябинск пересечение Свердловского пр. и ул.Калинина 34  за перекр.из центра  авт.ост "Автомобильное Училище" ',
        'город Челябинск  ул.Калинина 34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1339, 800, 61.45144, 55.160221, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск ул.Г.Танкограда-пр.Ленина  за перекр.из центра  тролл.ост. "Театр ЧТЗ"  поз.3',
        'город Челябинск ул.Г.Танкограда / пр.Ленина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1340, 801, 61.319639, 55.193479, 'Россия, Челябинск, Комсомольский проспект, 65',
        'город Челябинск пересечение Комсомольского пр. 65 - ул.Солнечная  ост."ул.Солнечная"  за перекр. в центр',
        'город Челябинск  Комсомольского пр. 65', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1341, 802, 61.406885, 55.155, 'Россия, Челябинск, улица Цвиллинга',
        'город Челябинск пересечение ул.Цвиллинга и ул.Плеханова  до перекр.из центра  трам.ост."Городской парк" ',
        'город Челябинск  ул.Цвиллинга / ул.Плеханова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1342, 803, 61.423296, 55.168655, 'Россия, Челябинск, улица Труда, 28', 'город Челябинск ул. Труда  28', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1343, 804, 61.307341, 55.187861, 'Россия, Челябинск, проспект Победы, 305Б',
        'город Челябинск пр. Победы  напротив д. 305б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1344, 805, 61.387246, 55.11823, 'Россия, Челябинск, Троицкий тракт, 32А', 'город Челябинск Троицкий тр.  32а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1345, 806, 61.375532, 55.229085, 'Россия, Челябинск, Черкасская улица, 17',
        'город Челябинск ул. Черкасская  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1346, 807, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1347, 808, 61.465175, 55.186725, 'Россия, Челябинск, улица Комарова, 46', 'город Челябинск ул. Комарова  46',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1348, 809, 61.439124, 55.192888, 'Россия, Челябинск, улица Героев Танкограда, 1',
        'город Челябинск ул. Героев Танкограда  напротив д. 1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1349, 810, 61.41442, 55.15196, 'Россия, Челябинск, улица Свободы, 92', 'город Челябинск ул. Свободы  92', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1350, 811, 61.387085, 55.09338, 'Россия, Челябинск, Троицкий тракт, 21',
        'город Челябинск Троицкий тр.  напротив д. 21', 'город Челябинск Троицкий тр.  д. 21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1351, 812, 61.381138, 55.14892, 'Россия, Челябинск, улица Энгельса, 103', 'город Челябинск ул. Энгельса  103',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1352, 813, 61.387525, 55.11372, 'Россия, Челябинск, Троицкий тракт, 46', 'город Челябинск Троицкий тр.  46',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1353, 814, 61.394999, 55.183734, 'Россия, Челябинск, проспект Победы, 167',
        'город Челябинск пересечение пр. Победы  167 и ул. Каслинская', 'город Челябинск пр. Победы  167', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1354, 815, 61.463037, 55.1825, 'Россия, Челябинск, улица Комарова, 103', 'город Челябинск ул. Комарова  103',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1355, 816, 61.376655, 55.258648, 'Россия, Челябинск, улица Богдана Хмельницкого, 30',
        'город Челябинск ул. Богдана Хмельницкого  30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1356, 817, 61.418535, 55.236586, 'Россия, Челябинск, Хлебозаводская улица',
        'город Челябинск ул. Хлебозаводская  250 м от автодороги Меридиан',
        'город Челябинск ул. Хлебозаводская  / автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1357, 818, 61.383455, 55.118287, 'Россия, Челябинск, улица Дарвина, 19', 'город Челябинск ул. Дарвина  19',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1358, 819, 61.415786, 55.172959, 'Россия, Челябинск, Российская улица, 61А',
        'город Челябинск ул. Российская  61а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1359, 820, 61.466693, 55.192548, 'Россия, Челябинск, улица Бажова, 23',
        'город Челябинск пересечение пр. Победы и ул. Бажова  23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1363, 821, 61.421957, 55.137821, 'Россия, Челябинск, Гражданская улица, 14А',
        'город Челябинск автодорога Меридиан/ ул. Гражданская  14а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1365, 822, 61.388971, 55.153277, 'Россия, Челябинск, Свердловский проспект, 86',
        'город Челябинск Свердловский пр.  напротив д. 86', 'город Челябинск Свердловский пр.  д. 86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1366, 823, 61.443104, 55.16467, 'Россия, Челябинск, улица Горького, 10', 'город Челябинск ул. Горького  10',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1367, 824, 61.415804, 55.162798, 'Россия, Челябинск, Российская улица, 196',
        'город Челябинск ул. Российская  между д. 196 и д. 198', 'город Челябинск ул. Российская   д. 196', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1370, 825, 61.332009, 55.190719, 'Россия, Челябинск, улица Молодогвардейцев, 48',
        'город Челябинск ул. Молодогвардейцев  48', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1371, 826, 61.377949, 55.191557, 'Россия, Челябинск, Комсомольский проспект, 19',
        'город Челябинск Комсомольский пр.  напротив д. 19', 'город Челябинск Комсомольский пр.   д. 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1372, 827, 61.386429, 55.131084, 'Россия, Челябинск, улица Салтыкова, 52',
        'город Челябинск ул. Профинтерна/ ул. Салтыкова  52', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1374, 828, 61.380671, 55.176326, 'Россия, Челябинск, Краснознамённая улица, 41',
        'город Челябинск ул. Краснознаменная  41', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1375, 829, 61.434722, 55.164366, 'Россия, Челябинск, Артиллерийская улица, 117/1',
        'город Челябинск ул. Артиллерийская  117/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1376, 830, 61.431345, 55.161908, 'Россия, Челябинск, проспект Ленина, 26А',
        'город Челябинск ул. Ловина/ пр. Ленина  26а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1379, 831, 61.422649, 55.191901, 'Россия, Челябинск, Артиллерийская улица, 2А',
        'город Челябинск пересечение ул. Механическая и ул. Артиллерийская  2а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1381, 832, 61.437426, 55.195462, 'Россия, Челябинск, улица Героев Танкограда, 83П',
        'город Челябинск ул. Героев Танкограда  83П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1382, 833, 61.541343, 55.11891, 'Россия, Челябинск, Копейское шоссе, 1П/1',
        'город Челябинск Копейское шоссе  1п/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1383, 834, 61.440786, 55.188026, 'Россия, Челябинск, проспект Победы, 84', 'город Челябинск пр. Победы  84',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1384, 835, 61.305473, 55.171843, 'Россия, Челябинск, улица Братьев Кашириных, 133',
        'город Челябинск ул. Чичерина/ ул. Бр. Кашириных  133', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1386, 836, 61.399499, 55.147891, 'Россия, Челябинск, улица Елькина, 85',
        'город Челябинск ул. Елькина  85  автостоянка', 'город Челябинск ул. Елькина  85', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1387, 837, 61.409138, 55.109219, 'Россия, Челябинск, 7-й Целинный переулок',
        'город Челябинск пересечение автодороги Меридиан и 7-го Целинного переулка',
        'город Челябинск  автодорога Меридиан / 7-го Целинный переулок', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1388, 838, 61.386249, 55.109755, 'Россия, Челябинск, Троицкий тракт, 11', 'город Челябинск Троицкий тр. 11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1389, 839, 61.490696, 55.230374, 'Россия, Челябинск, Бродокалмакский тракт, 6',
        'город Челябинск Бродокалмакский тр.  6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1390, 840, 61.491873, 55.100172, 'Россия, Челябинск, Новороссийская улица',
        'город Челябинск пересечение ул. Новороссийская и ул. Дербенская',
        'город Челябинск  ул. Новороссийская / ул. Дербенская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1391, 841, 61.373799, 55.225183, 'Россия, Челябинск, Свердловский тракт, 12Б',
        'город Челябинск Свердловский тр.  12б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1392, 842, 61.398574, 55.183384, 'Россия, Челябинск, улица Кирова, 2А/1', 'город Челябинск ул. Кирова  2а/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1395, 843, 61.444478, 55.186417, 'Россия, Челябинск, улица Героев Танкограда, 23',
        'город Челябинск ул. Героев Танкограда  23  поз. 2', 'город Челябинск ул. Героев Танкограда  23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1396, 844, 61.401988, 55.184027, 'Россия, Челябинск, проспект Победы, 155А', 'город Челябинск пр. Победы  155А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1397, 845, 61.360553, 55.1882, 'Россия, Челябинск, улица Чайковского',
        'город Челябинск пересечение ул. Чайковского и пр. Победы  за перекрестком в центр',
        'город Челябинск ул. Чайковского / пр. Победы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1407, 846, 61.377194, 55.151075, 'Россия, Челябинск, улица Энтузиастов, 17',
        'город Челябинск ул. Энтузиастов 17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1409, 847, 61.387938, 55.172167, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск Свердловский пр. (в центр)  40 м перед мостом через реку Миасс ',
        'город Челябинск Свердловский пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1410, 848, 0, 0, '', 'город Челябинск пересечение Троицкого тр.  9/2и ул. Дарвина',
        'город Челябинск пересечение Троицкого тр.  9/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1411, 849, 61.38041, 55.241668, 'Россия, Челябинск, Черкасская улица, 5', 'город Челябинск ул. Черкасская  5',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1412, 850, 61.518661, 55.158498, 'Россия, Челябинск, Линейная улица, 51А', 'город Челябинск ул. Линейная  51а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1413, 851, 61.420044, 55.135583, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск развязка Троицкого тр. и автодороги Меридиан',
        'город Челябинск Троицкого тр. / автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1414, 852, 61.351116, 55.105279, 'Россия, Челябинск, Новоэлеваторная улица, 51/1',
        'город Челябинск Уфимский тр./ ул. Новоэлеваторная  51/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1416, 853, 61.386249, 55.109755, 'Россия, Челябинск, Троицкий тракт, 11', 'город Челябинск Троицкий тр.  11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1417, 854, 61.314007, 55.195082, 'Россия, Челябинск, Комсомольский проспект, 74',
        'город Челябинск Комсомольский пр.  74', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1419, 855, 61.450461, 55.162998, 'Россия, Челябинск, улица 40-летия Октября, 23',
        'город Челябинск ул. Героев Танкограда/ул. 40 лет Октября  23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1420, 856, 61.417277, 55.161718, 'Россия, Челябинск, Российская улица, 179',
        'город Челябинск ул. Российская  179', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1421, 857, 61.462615, 55.183456, 'Россия, Челябинск, улица Комарова, 68', 'город Челябинск ул. Комарова  68',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1422, 858, 61.441648, 55.186438, 'Россия, Челябинск, улица Героев Танкограда, 42',
        'город Челябинск ул. Героев Танкограда  42', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1423, 859, 61.469963, 55.1703, 'Россия, Челябинск, улица Танкистов, 144',
        'город Челябинск ул. Танкистов  напротив д. 144', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1425, 860, 61.441828, 55.1672, 'Россия, Челябинск, улица Горького, 17',
        'город Челябинск пересечение ул. Горького  17 и ул. Первой Пятилетки', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1426, 861, 61.378892, 55.191449, 'Россия, Челябинск, Краснознамённая улица, 2',
        'город Челябинск ул. Краснознаменная  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1427, 862, 61.351305, 55.194044, 'Россия, Челябинск, Комсомольский проспект, 33',
        'город Челябинск Комсомольский пр.  33  170 м до ул. Красного урала', 'город Челябинск Комсомольский пр.  33',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1428, 863, 61.29507, 55.183127, 'Россия, Челябинск, проспект Победы, 323', 'город Челябинск пр. Победы  323',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1429, 864, 61.378748, 55.223078, 'Россия, Челябинск, Свердловский тракт, 7',
        'город Челябинск Свердловский тр.  напротив д.7', 'город Челябинск Свердловский тр.   д.7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1430, 865, 61.437687, 55.16757, 'Россия, Челябинск, улица Первой Пятилетки, 43',
        'город Челябинск ул. Первой Пятилетки  напротив д. 43', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1431, 866, 61.355814, 55.179077, 'Россия, Челябинск, 1-й Северо-Крымский переулок, 16/68',
        'город Челябинск ул. Бр. Кашириных/ Северокрымский 1-й пер.  16',
        'город Челябинск  Северокрымский 1-й пер.  16', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1433, 867, 61.378317, 55.192261, 'Россия, Челябинск, Комсомольский проспект, 10/1',
        'город Челябинск Комсомольский пр. 10/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1435, 868, 61.386114, 55.186546, 'Россия, Челябинск, Свердловский проспект, 22',
        'город Челябинск Свердловский пр.  22', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1436, 869, 0, 0, '', 'город Челябинск автодорога Меридиан/ ул. Ш. Руставелли  напротив д. 32/1',
        'город Челябинск ул. Ш. Руставелли  напротив д. 32/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1437, 870, 61.298367, 55.171288, 'Россия, Челябинск, улица Братьев Кашириных, 134Б',
        'город Челябинск ул. Бр. Кашириных  134б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1439, 871, 61.446248, 55.144783, 'Россия, Челябинск, Ленинский район, улица Гагарина, 1',
        'город Челябинск пересечение Копейского шоссе и ул. Гагарина  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1441, 872, 61.405033, 55.138346, 'Россия, Челябинск, улица Доватора, 8', 'город Челябинск ул. Доватора  8',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1442, 873, 61.353308, 55.108066, 'Россия, Челябинск, улица Блюхера, 111',
        'город Челябинск Уфимский тр./ ул. Блюхера  напротив д. 111', 'город Челябинск ул. Блюхера  напротив д. 111',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1443, 874, 61.501386, 55.229476, 'Россия, Челябинск, Бродокалмакский тракт, 1/2',
        'город Челябинск Бродокалмакский тр.  3/1  выезд', 'город Челябинск Бродокалмакский тр.  3/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1444, 875, 61.439753, 55.187784, 'Россия, Челябинск, проспект Победы, 88',
        'город Челябинск пр. Победы/ ул. Чайковского  напротив д. 89', 'город Челябинск пр. Победы напротив д. 89', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1445, 876, 61.418777, 55.241447, 'Россия, Челябинск, Хлебозаводская улица, 7',
        'город Челябинск пересечение ул. Хлебозаводская  7/2 и ул. Автоматики',
        'город Челябинск ул. Хлебозаводская  7/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1447, 877, 61.292169, 55.174753, 'Россия, Челябинск, улица 250-летия Челябинска, 25',
        'город Челябинск пересечение ул. Салавата Юлаева и ул. 250 лет Челябинску  25', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1448, 878, 61.387552, 55.112402, 'Россия, Челябинск, Троицкий тракт, 52',
        'город Челябинск Троицкий тр.  52  поворот на Металлобазу', 'город Челябинск Троицкий тр.  52 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1449, 879, 61.308222, 55.188216, 'Россия, Челябинск, проспект Победы, 305Е',
        'город Челябинск пересечение пр. Победы  305е и ул. Молдавская', 'город Челябинск пересечение пр. Победы  305е',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1450, 880, 61.381614, 55.163199, 'Россия, Челябинск, улица Энгельса, 49', 'город Челябинск ул. Энгельса  49',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1451, 881, 61.391244, 55.169015, 'Россия, Челябинск, Свердловский проспект, 51',
        'город Челябинск Свердловский пр.  напротив д. 51  развязка у ДС "Юность"',
        'город Челябинск Свердловский пр.  д. 51', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1452, 882, 0, 0, '', 'город Челябинск Троицкий тр.  ост. "Исаково"', 'город Челябинск ост. "Исаково"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1453, 883, 61.464753, 55.186227, 'Россия, Челябинск, улица Комарова, 52', 'город Челябинск ул. Комарова  52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1454, 884, 61.436231, 55.151024, 'Россия, Челябинск, автодорога Меридиан, 1',
        'город Челябинск автодорога Меридиан  1/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1457, 885, 61.422227, 55.193026, 'Россия, Челябинск, Механическая улица, 40А',
        'город Челябинск пересечение ул. Механическая  40а и ул. Артиллерийская (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1463, 886, 61.379431, 55.183426, 'Россия, Челябинск, Краснознамённая улица, 28',
        'город Челябинск ул. Краснознаменная  напротив д. 28', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1464, 887, 61.424913, 55.143096, 'Россия, Челябинск, улица Руставели, 32/1',
        'город Челябинск пересечение автодороги Меридиан и ул. Ш. Руставелли  32/1',
        'город Челябинск ул. Ш. Руставелли  32/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1465, 888, 61.439762, 55.215755, 'Россия, Челябинск, улица Героев Танкограда, 42П/1',
        'город Челябинск ул. Северный луч/ ул. Героев Танкограда  42П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1467, 889, 61.420044, 55.135583, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск автодорога Меридиан  100 м до пересечения с ул. Хлебозаводская',
        'город Челябинск автодорога Меридиан  / ул. Хлебозаводская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1468, 890, 61.404835, 55.249438, 'Россия, Челябинск, Хлебозаводская улица, 2',
        'город Челябинск ул. Хлебозаводская  поз.2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1469, 891, 61.501386, 55.229476, 'Россия, Челябинск, Бродокалмакский тракт, 1/2',
        'город Челябинск Бродокалмакский тр.  1/2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1470, 892, 61.432773, 55.193437, 'Россия, Челябинск, улица Героев Танкограда, 80П',
        'город Челябинск ул. Механическая/ул. Героев Танкограда  80П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1472, 893, 61.472919, 55.173812, 'Россия, Челябинск, улица Кулибина, 52/173',
        'город Челябинск ул. Танкистов/ул. Кулибина 52', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1474, 894, 61.39075, 55.134955, 'Россия, Челябинск, Табачная улица, 2',
        'город Челябинск ул. Профинтерна/ ул. Табачная  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1476, 895, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск Шершневское водохранилище  пост ГИБДД',
        'город Челябинск Шершневское водохранилище ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1477, 896, 61.356964, 55.177853, 'Россия, Челябинск, улица Братьев Кашириных, 95А',
        'город Челябинск пересечение ул. Бр. Кашириных напротив д. 95А и ул. Северокрымская',
        'город Челябинск ул. Бр. Кашириных д. 95А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1478, 897, 61.329, 55.146013, 'Россия, Челябинск, улица Худякова, 22К1',
        'город Челябинск ул. Худякова  напротив д. 22к1', 'город Челябинск ул. Худякова  д. 22к1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1482, 898, 61.443607, 55.147376, 'Россия, Челябинск, улица Харлова, 2',
        'город Челябинск Копейское шоссе/ ул. Харлова  2а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1483, 899, 61.414169, 55.167642, 'Россия, Челябинск, улица Труда, 67',
        'город Челябинск пересечение ул. Труда  67 и  ул. Красноармейская', 'город Челябинск ул. Труда  67', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1484, 900, 61.358339, 55.240713, 'Россия, Челябинск, Индивидуальная улица, 1',
        'город Челябинск Свердловский тр./ ул. Индивидуальная 1 ', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1486, 901, 61.358283, 55.178598, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул. Бр. Кашириных (из центра) и ул. Северокрымская',
        'город Челябинск ул. Бр. Кашириных / ул. Северокрымская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1487, 902, 61.286572, 55.186504, 'Россия, Челябинск, улица Чичерина, 22/5',
        'город Челябинск пересечение ул. Чичерина  22/5 и Комсомольского пр.', 'город Челябинск ул. Чичерина  22/5',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1488, 903, 61.495394, 55.220341, 'Россия, Челябинск, Бродокалмакский тракт',
        'город Челябинск Бродокалмакский тр.  50 м от поста ГИБДД', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1489, 904, 61.440382, 55.217249, 'Россия, Челябинск, улица Героев Танкограда, 28П',
        'город Челябинск ул. Героев Танкоград 28П  поворот на Северный луч', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1491, 905, 61.443661, 55.146826, 'Россия, Челябинск, улица Харлова, 1',
        'город Челябинск Копейское шоссе/ ул. Харлова  1/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1492, 906, 61.386815, 55.198222, 'Россия, Челябинск, Свердловский тракт, 7/1',
        'город Челябинск Свердовский тр.  7/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1493, 907, 0, 0, '', 'город Челябинск  пересечение ул. Бр. Кашириных   97 и ул. Чайковского',
        'город Челябинск  пересечение ул. Бр. Кашириных   97', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1494, 908, 61.385764, 55.13181, 'Россия, Челябинск, улица Салтыкова, 49А',
        'город Челябинск пересечение ул. Салтыкова 49а и ул. Профинтерна',
        'город Челябинск пересечение ул. Салтыкова 49а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1495, 909, 61.351116, 55.105279, 'Россия, Челябинск, Новоэлеваторная улица, 51/1',
        'город Челябинск Уфимский тр./ ул. Новоэлеваторная  51/1  поз. 1', 'город Челябинск ул. Новоэлеваторная  51/1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1496, 910, 61.33394, 55.183235, 'Россия, Челябинск, улица Молодогвардейцев, 43',
        'город Челябинск ул. Молодогвардейцев  43', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1497, 911, 61.299876, 55.184849, 'Россия, Челябинск, проспект Победы, 315',
        'город Челябинск ул. 40 лет Победы/ пр. Победы  напротив д. 315', 'город Челябинск пр. Победы  д. 315', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1498, 912, 61.38598, 55.084338, 'Россия, Челябинск, Троицкий тракт, 33/1',
        'город Челябинск Троицкий тр.  напротив д. 33/1', 'город Челябинск Троицкий тр.  д. 33/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1499, 913, 61.34481, 55.194049, 'Россия, Челябинск, Комсомольский проспект, 37',
        'город Челябинск Комсомольский пр.  37', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1501, 914, 61.358339, 55.240713, 'Россия, Челябинск, Индивидуальная улица, 1',
        'город Челябинск Свердловский тр. (в центр)/ ул. Индивидуальная  1а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1502, 915, 61.440166, 55.174897, 'Россия, Челябинск, улица Горького, 30', 'город Челябинск ул. Горького  30',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1503, 916, 61.443607, 55.214296, 'Россия, Челябинск, улица Героев Танкограда, 51П',
        'город Челябинск ул. Героев Танкограда  51П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1504, 917, 61.385593, 55.104213, 'Россия, Челябинск, Троицкий тракт, 15',
        'город Челябинск Троицкий тр.  напротив д. 15  поз.2', 'город Челябинск Троицкий тр.  д. 15', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1505, 918, 61.43889, 55.193463, 'Россия, Челябинск, Механическая улица, 65',
        'город Челябинск пересечение ул. Героев Танкограда и ул. Механическая  65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1507, 919, 61.377527, 55.232828, 'Россия, Челябинск, Черкасская улица, 15/1',
        'город Челябинск Свердловский тр./ ул. Черкасская  15/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1509, 920, 61.459821, 55.165266, 'Россия, Челябинск, улица Первой Пятилетки, 9',
        'город Челябинск ул. Первой Пятилетки  9', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1510, 921, 61.338432, 55.194147, 'Россия, Челябинск, Комсомольский проспект, 41',
        'город Челябинск Комсомолький пр.  41', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1511, 922, 61.387327, 55.199326, 'Россия, Челябинск, Свердловский тракт, 7/2',
        'город Челябинск Свердловский тр.  напротив д. 7/2', 'город Челябинск Свердловский тр.  д. 7/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1512, 923, 61.293283, 55.182773, 'Россия, Челябинск, проспект Победы, 327', 'город Челябинск пр. Победы   329',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1513, 924, 61.332, 55.180598, 'Россия, Челябинск, улица Молодогвардейцев, 68',
        'город Челябинск ул. Молодогвардейцев  68', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1514, 925, 61.312237, 55.177745, 'Россия, Челябинск, улица Братьев Кашириных, 110',
        'город Челябинск ул. Бр. Кашириных  110', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1515, 926, 61.420044, 55.135583, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск автодорога Меридиан  23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1516, 927, 61.381991, 55.184572, 'Россия, Челябинск, проспект Победы, 198', 'город Челябинск пр. Победы  198',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1517, 928, 61.41354, 55.184793, 'Россия, Челябинск, Российская улица, 32',
        'город Челябинск пересечение пр. Победы и ул. Российская  32', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1520, 929, 61.398844, 55.249515, 'Россия, Челябинск, шоссе Металлургов, 7',
        'город Челябинск шоссе Металлургов  7', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1521, 930, 61.433105, 55.131393, 'Россия, Челябинск, улица Дзержинского, 95',
        'город Челябинск ул. Дзержинского  95', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1522, 931, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск Курганский тр.  700 м до поста ГИБДД',
        'город Челябинск Курганский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1523, 932, 61.420044, 55.235493, 'Россия, Челябинск, Хлебозаводская улица, 7В/1',
        'город Челябинск пересечение ул. Хлебозаводская  7в/1 и ул. Автоматики',
        'город Челябинск  ул. Хлебозаводская  7в/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1524, 933, 61.361177, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 95',
        'город Челябинск  пересечение ул. Северо-Крымской (из центра) и ул. Бр. Кашириных  95',
        'город Челябинск ул. Бр. Кашириных  95', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1525, 934, 61.301817, 55.184546, 'Россия, Челябинск, улица 40-летия Победы, 29',
        'город Челябинск ул. 40 лет Победы  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1526, 935, 61.397972, 55.150695, 'Россия, Челябинск, улица Елькина, 90', 'город Челябинск ул. Ельина  90', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1527, 936, 61.394666, 55.163528, 'Россия, Челябинск, улица Коммуны, 70',
        'город Челябинск ул. Красная/ул.Коммуны  70', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1529, 937, 61.380392, 55.042416, 'Россия, Челябинск, посёлок Новосинеглазово, Заводская улица, 14',
        'город Челябинск ул. Гостевая/ ул. Заводская  напротив д. 14', 'город Челябинск ул. Заводская  д. 14', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1530, 938, 61.433761, 55.167236, 'Россия, Челябинск, Артиллерийская улица, 124Б',
        'город Челябинск автодорога Меридиан/ ул. Артиллерийская  124б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1532, 939, 61.334362, 55.178002, 'Россия, Челябинск, улица Братьев Кашириных, 107',
        'город Челябинск ул. Бр. Кашириных  107', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1533, 940, 61.388962, 55.07552, 'Россия, Челябинск, Троицкий тракт, 66',
        'город Челябинск Троицкий тр.  напротив д. 66 (100 м от поста ГИБДД)', 'город Челябинск Троицкий тр.   д. 66',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1534, 941, 61.380729, 55.16762, 'Россия, Челябинск, улица Энгельса',
        'город Челябинск пересечение ул. Энгельса и ул. Труда (из центра)', 'город Челябинск  ул. Энгельса / ул. Труда',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1535, 942, 61.458932, 55.178671, 'Россия, Челябинск, улица Комарова, 106', 'город Челябинск ул. Комарова  106',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1537, 943, 61.386078, 55.173581, 'Россия, Челябинск, Калининский район, улица 8 Марта, 108',
        'город Челябинск Свердловский пр. (в центр)/ ул. 8 Марта  108', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1538, 944, 61.380877, 55.152377, 'Россия, Челябинск, улица Энгельса, 95', 'город Челябинск ул. Энгельса  95',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1539, 945, 61.433915, 55.162416, 'Россия, Челябинск, улица Ловина',
        'город Челябинск ул. Ловина  200 м до ул. Артиллерийская', 'город Челябинск ул. Ловина  / ул. Артиллерийская',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1540, 946, 61.459857, 55.166228, 'Россия, Челябинск, улица Марченко, 25',
        'город Челябинск ул. Первой Пятилетки/ ул. Марченко  25', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1542, 947, 61.479424, 55.128145, 'Россия, Челябинск, Копейское шоссе',
        'город Челябинск пересечение Копейского шоссе и ул. Машиностроителей',
        'город Челябинск  Копейского шоссе / ул. Машиностроителей', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1543, 948, 61.381695, 55.11995, 'Россия, Челябинск, улица Дарвина, 4А',
        'город Челябинск ул. Дарвина  напротив д. 4а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1544, 949, 61.456965, 55.171766, 'Россия, Челябинск, Салютная улица, 2',
        'город Челябинск ул. Салютная  напротив д. 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1545, 950, 61.323331, 55.189491, 'Россия, Челябинск, проспект Победы, 346',
        'город Челябинск пересечение ул. Ворошилова  и пр. Победы 346', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1547, 951, 61.439807, 55.205425, 'Россия, Челябинск, улица Героев Танкограда, 67П',
        'город Челябинск ул. Героев Танкограда  67П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1548, 952, 61.373125, 55.040647, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 3',
        'город Челябинск пересечение ул. Кирова  7 и ул. Калинина', 'город Челябинск  ул. Кирова  7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1549, 953, 61.3987, 55.157073, 'Россия, Челябинск, улица Елькина',
        'город Челябинск ул. Елькина 85  регистрационная палата', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1550, 954, 61.370529, 55.125716, 'Россия, Челябинск, улица Дарвина, 18К1', 'город Челябинск ул. Дарвина  18к1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1551, 955, 61.416972, 55.167596, 'Россия, Челябинск, улица Труда, 21', 'город Челябинск ул. Труда  21', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1552, 956, 61.40992, 55.154887, 'Россия, Челябинск, улица Плеханова, 43', 'город Челябинск ул. Плеханова  43',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1553, 957, 61.385899, 55.111789, 'Россия, Челябинск, Троицкий тракт, 11А', 'город Челябинск Троицкий тр.  11а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1554, 958, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск автодорога Меридиан/ ул. Артиллерийская  136  по. 3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1556, 959, 61.414914, 55.169622, 'Россия, Челябинск, Российская улица, 124к1',
        'город Челябинск пл. Павших Революционеров/ ул. Российская  124/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1558, 960, 61.375119, 55.168578, 'Россия, Челябинск, улица Труда, 183', 'город Челябинск ул. Труда  183', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1559, 961, 61.382997, 55.146497, 'Россия, Челябинск, улица Доватора, 50',
        'город Челябинск пересечение ул. Воровского и ул. Доватора  напротив д. 55',
        'город Челябинск ул. Доватора  напротив д. 55', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1560, 962, 61.415947, 55.161311, 'Россия, Челябинск, Российская улица, 200',
        'город Челябинск ул. Российская  202', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1561, 963, 61.381057, 55.150726, 'Россия, Челябинск, улица Энгельса, 97Б', 'город Челябинск ул. Энгельса  97б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1562, 964, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Западное шоссе (в центр)  1400 м до поста ГИБДД', 'город Челябинск Западное шоссе', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1567, 965, 61.415687, 55.143111, 'Россия, Челябинск, улица Свободы, 185',
        'город Челябинск ул. Свободы  185  ТК "Экспресс"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1568, 966, 61.421463, 55.16702, 'Россия, Челябинск, улица 3 Интернационала, 105',
        'город Челябинск пересечение ул. Труда и ул. 3-го Интернационала  105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1571, 967, 61.429808, 55.042633, 'Россия, Челябинск, посёлок Исаково, улица Морозова, 5',
        'город Челябинск Троицкий тр./ ул. Морозова  5', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1573, 968, 61.401727, 55.174501, 'Россия, Челябинск, улица Кирова, 19А', 'город Челябинск ул. Кирова  19а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1582, 969, 0, 0, '', 'город Челябинск Свердловский тр.  1д', 'город Челябинск Свердловский тр.  1д', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1583, 970, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр. поворот на Новосинеглазово  выезд', 'город Челябинск Троицкий тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1584, 971, 61.447371, 55.214676, 'Россия, Челябинск, улица Героев Танкограда, 51П/1',
        'город Челябинск ул. Героев Танкограда  51п/1  справа', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1585, 972, 61.408141, 55.108684, 'Россия, Челябинск, улица Артёма, 187/1',
        'город Челябинск автодорога Меридиан/ ул. Артема  187/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1587, 973, 61.321486, 55.178659, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск ул. Бр. Кашириных (в центр)/ ул. Ворошилова  поз. 2',
        'город Челябинск ул. Бр. Кашириных / ул. Ворошилова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1588, 974, 61.387624, 55.168208, 'Россия, Челябинск, Свердловский проспект, 40Ак1',
        'город Челябинск Свердловский пр.  40а/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1589, 975, 61.501386, 55.229476, 'Россия, Челябинск, Бродокалмакский тракт, 1/2',
        'город Челябинск Бродокалмакский тр.   напротив д. 3/1', 'город Челябинск Бродокалмакский тр.   д. 3/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1590, 976, 61.432773, 55.193437, 'Россия, Челябинск, улица Героев Танкограда, 80П',
        'город Челябинск пересечение ул. Героев Танкограда  80П и ул. Механическая',
        'город Челябинск  ул. Героев Танкограда  80П', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1591, 977, 61.348969, 55.178583, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск ул. Бр. Кашириных (центр)  400 м до ул. Краснознаменная',
        'город Челябинск ул. Бр. Кашириных / до ул. Краснознаменная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1594, 978, 61.423296, 55.168655, 'Россия, Челябинск, улица Труда, 28', 'город Челябинск ул. Труда  28', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1595, 979, 61.353227, 55.105274, 'Россия, Челябинск, Новоэлеваторная улица, 49',
        'город Челябинск Уфимский тр./ ул. Новоэлеваторная  49/7', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1597, 980, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск площадь Аэропорта  с левой стороны по направлению к площади Аэропорта',
        'город Челябинск площадь Аэропорта', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1598, 981, 0, 0, 'Россия, Южно-Уральская железная дорога, станция Кир-Завод',
        'город Челябинск Свердловский тр. (из центра)  150 м до поворота на Лакокрасочный завод',
        'город Челябинск Лакокрасочный завод', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1599, 982, 61.460064, 55.180316, 'Россия, Челябинск, улица Комарова, 90', 'город Челябинск ул. Комарова  90',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1600, 983, 61.351853, 55.248381, 'Россия, Челябинск, Свердловский тракт, 4',
        'город Челябинск Свердловский тр.  напротив д. 4б', 'город Челябинск Свердловский тр.  д. 4б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1601, 984, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск дорога из Аэропорта  140 м от площади аэропорта (в город)',
        'город Челябинск площадь Аэропорта', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1602, 985, 61.388971, 55.153277, 'Россия, Челябинск, Свердловский проспект, 86',
        'город Челябинск Свердловский пр.  напротив д.86', 'город Челябинск Свердловский пр.  д.86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1603, 986, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск автодорога Меридиан (из центра)/ ул. Артиллерийская  136  поз.4',
        'город Челябинск ул. Артиллерийская  136', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1605, 987, 61.367699, 55.229522, 'Россия, Челябинск, Свердловский тракт, 12к1',
        'город Челябинск Свердловский тр.  12/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1606, 988, 61.410953, 55.160967, 'Россия, Челябинск, улица Свободы, 66',
        'город Челябинск ул. Свободы  напротив д. 66', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1607, 989, 61.443795, 55.144763, 'Россия, Челябинск, Ленинский район, улица Гагарина, 4',
        'город Челябинск ул. Гагарина  напротив д. 4', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1609, 990, 61.45179, 55.169411, 'Россия, Челябинск, улица Героев Танкограда, 118',
        'город Челябинск ул. Героев Танкограда  118', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1610, 991, 61.416945, 55.219473, 'Россия, Челябинск, улица Северный Луч',
        'город Челябинск Северный луч  до поворота на ул. Хлебозаводская', 'город Челябинск Северный луч', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1611, 992, 61.414932, 55.15607, 'Россия, Челябинск, улица Плеханова, 16', 'город Челябинск ул. Плеханова  16',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1612, 993, 61.3032, 55.151873, 'Россия, Челябинск, посёлок Шершни, Парковая улица, 8',
        'город Челябинск ул. Гостевая/ ул. Парковая  8', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1617, 994, 61.378398, 55.121597, 'Россия, Челябинск, улица Дарвина, 10',
        'город Челябинск пересечение ул. Дарвина  10/1 и ул. Новосельская', 'город Челябинск  ул. Дарвина  10/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1618, 995, 61.305419, 55.191454, 'Россия, Челябинск, Молдавская улица, 21А',
        'город Челябинск ул. Молдавская  21а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1619, 996, 61.386249, 55.109755, 'Россия, Челябинск, Троицкий тракт, 11',
        'город Челябинск Троицкий тр.  11  поз. 1', 'город Челябинск Троицкий тр.  11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1620, 997, 61.438262, 55.136041, 'Россия, Челябинск, Ленинский район, улица Гагарина, 22',
        'город Челябинск ул. Гагарина  22', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1622, 998, 61.443607, 55.147376, 'Россия, Челябинск, улица Харлова, 2', 'город Челябинск ул. Харлова  2', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1623, 999, 61.417717, 55.124676, 'Россия, Челябинск, улица Гончаренко, 99',
        'город Челябинск пересечение автодороги Меридиан и ул. Гончаренко  напротив д. 99',
        'город Челябинск ул. Гончаренко  д. 99', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1624, 1000, 61.318795, 55.179036, 'Россия, Челябинск, улица Братьев Кашириных, 102В',
        'город Челябинск ул. Бр. Кашириных  102в', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1625, 1001, 61.289761, 55.189414, 'Россия, Челябинск, Комсомольский проспект, 110',
        'город Челябинск Комсомольский пр.  110', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1626, 1002, 61.375757, 55.224043, 'Россия, Челябинск, Радонежская улица',
        'город Челябинск Свердловский тр. (в центр)  150 м после пересечения с ул. Радонежская',
        'город Челябинск Свердловский тр. / ул. Радонежская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1627, 1003, 61.366639, 55.179426, 'Россия, Челябинск, улица Братьев Кашириных, 54',
        'город Челябинск ул. Бр. Кашириных  52б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1628, 1004, 61.287695, 55.187373, 'Россия, Челябинск, Комсомольский проспект, 111/1',
        'город Челябинск Комсомолький пр.  111/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1629, 1005, 61.464613, 55.190546, 'Россия, Челябинск, улица Бажова',
        'город Челябинск пересечение ул. Бажова и ул. Комарова', 'город Челябинск  ул. Бажова / ул. Комарова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1630, 1006, 61.441361, 55.170861, 'Россия, Челябинск, улица Горького, 23', 'город Челябинск ул. Горького  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1631, 1007, 61.44163, 55.14928, 'Россия, Челябинск, улица Харлова, 4к1',
        'город Челябинск автодорога Меридиан/ ул. Харлова  4/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1633, 1008, 61.436717, 55.197086, 'Россия, Челябинск, улица Героев Танкограда, 79П/1',
        'город Челябинск ул. Героев Танкограда  79П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1634, 1009, 61.442574, 55.162643, 'Россия, Челябинск, улица Горького, 6', 'город Челябинск ул. Горького  6',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1635, 1010, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск ул. Центральная (п. Западный2  стр. 1)',
        'город Челябинск п. Западный2  стр. 1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1637, 1011, 61.443014, 55.143559, 'Россия, Челябинск, Ленинский район, улица Гагарина, 6',
        'город Челябинск ул. Гагарина  6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1639, 1012, 61.504863, 55.122977, 'Россия, Челябинск, Копейское шоссе, 20',
        'город Челябинск Копейское шоссе  20', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1640, 1013, 61.418732, 55.185847, 'Россия, Челябинск, проспект Победы, 154', 'город Челябинск пр. Победы  154',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1641, 1014, 61.36363, 55.235195, 'Россия, Челябинск, Свердловский тракт, 6к1',
        'город Челябинск Свердловский тр.  6/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1642, 1015, 61.30815, 55.150535, 'Россия, Челябинск, посёлок Шершни, Центральная улица',
        'город Челябинск ул. Центральная  1700 м до поста ГИБДД  в центр', 'город Челябинск ул. Центральная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1643, 1016, 61.417007, 55.155767, 'Россия, Челябинск, Российская улица, 222',
        'город Челябинск  ул. Российская  222', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1644, 1017, 61.360358, 55.188036, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр. Победы (из центра) и ул. Чайковского',
        'город Челябинск пр. Победы / ул. Чайковского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1645, 1018, 61.307341, 55.187861, 'Россия, Челябинск, проспект Победы, 305Б',
        'город Челябинск пр. Победы  305б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1646, 1019, 61.383527, 55.093695, 'Россия, Челябинск, Троицкий тракт, 19',
        'город Челябинск Троицкий тр.  напротив д. 19', 'город Челябинск Троицкий тр. д. 19', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1647, 1020, 61.307898, 55.189182, 'Россия, Челябинск, проспект Победы, 374',
        'город Челябинск пересечение ул. Молдавская и пр Победы  374', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1649, 1021, 61.377958, 55.237838, 'Россия, Челябинск, Черкасская улица, 10',
        'город Челябинск ул. Черкасская  10', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1650, 1022, 61.453713, 55.172897, 'Россия, Челябинск, улица Комарова, 116', 'город Челябинск ул. Комарова  116',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1651, 1023, 61.399796, 55.13812, 'Россия, Челябинск, улица Доватора, 1',
        'город Челябинск пересечение ул. Доватора  1 и ул. Цехова', 'город Челябинск  ул. Доватора  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1652, 1024, 61.435872, 55.186217, 'Россия, Челябинск, проспект Победы, 113', 'город Челябинск пр. Победы  113',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1653, 1025, 61.351305, 55.194044, 'Россия, Челябинск, Комсомольский проспект, 33',
        'город Челябинск Комсомольский пр.  33  100 м до ул. Красного Урала', 'город Челябинск Комсомольский пр.  33',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1654, 1026, 61.387255, 55.149666, 'Россия, Челябинск, улица Воровского, 41',
        'город Челябинск ул. Воровского  41', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1655, 1027, 61.353227, 55.105274, 'Россия, Челябинск, Новоэлеваторная улица, 49',
        'город Челябинск Уфимский тр./ ул. Новоэлеваторная  49/7  поз.2', 'город Челябинск ул. Новоэлеваторная  49/7',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1656, 1028, 61.501386, 55.229476, 'Россия, Челябинск, Бродокалмакский тракт, 1/2',
        'город Челябинск Бродокалмакский тр.   напротив д. 3/1  поз. 2', 'город Челябинск Бродокалмакский тр. д. 3/1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1657, 1029, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск дорога в Аэропорт  150 м до поворота на таможню', 'город Челябинск дорога в Аэропорт ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1662, 1030, 61.441055, 55.206796, 'Россия, Челябинск, улица Героев Танкограда, 65П',
        'город Челябинск ул. Героев Танкограда  65П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1663, 1031, 61.435567, 55.1663, 'Россия, Челябинск, Артиллерийская улица, 111',
        'город Челябинск ул. Артиллерийская  111', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1664, 1032, 61.366864, 55.232012, 'Россия, Челябинск, Свердловский тракт, 8А',
        'город Челябинск Свердловский тр.  напротив д. 8а', 'город Челябинск Свердловский тр.  д. 8а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1665, 1033, 61.398978, 55.158009, 'Россия, Челябинск, улица Елькина, 59',
        'город Челябинск ул. Елькина  напротив д. 59', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1666, 1034, 61.306623, 55.190477, 'Россия, Челябинск, Молдавская улица, 23',
        'город Челябинск ул. Молдавская  23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1667, 1035, 61.304404, 55.151394, 'Россия, Челябинск, посёлок Шершни, Центральная улица, 3БК1',
        'город Челябинск ул. Гостевая/ ул. Центральная  3бк1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1670, 1036, 61.422182, 55.140224, 'Россия, Челябинск, Сортировочная улица, 1',
        'город Челябинск автодорога Меридиан/ ул. Сортировочная  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1672, 1037, 61.389312, 55.074211, 'Россия, Челябинск, Троицкий тракт, 70',
        'город Челябинск Троицкий тр.  напротив д. 70  выезд', 'город Челябинск Троицкий тр.  д. 70', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1673, 1038, 61.375757, 55.224043, 'Россия, Челябинск, Радонежская улица',
        'город Челябинск Свердловский тр (в центр).  200 м до поворота на ул. Радонежская',
        'город Челябинск Свердловский тр / ул. Радонежская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1674, 1039, 61.456183, 55.179, 'Россия, Челябинск, улица Котина, 1',
        'город Челябинск пересечение ул. Лермонтовский пер. и ул. Котина  1', 'город Челябинск ул. Котина  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1675, 1040, 61.384929, 55.184402, 'Россия, Челябинск, проспект Победы, 192', 'город Челябинск пр. Победы  192',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1676, 1041, 0, 0, '', 'город Челябинск автодорога Меридиан  напротив автодрома Обл. ГИБДД  поз. 1',
        'город Челябинск автодром Обл. ГИБДД', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1677, 1042, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр.  34', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1679, 1043, 61.444649, 55.146193, 'Россия, Челябинск, Ленинский район, улица Гагарина, 2',
        'город Челябинск Копейское шоссе/ ул. Гагарина  2а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1681, 1044, 61.418229, 55.194203, 'Россия, Челябинск, Механическая улица, 40',
        'город Челябинск ул. Механическая 40', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1682, 1045, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.  въезд в город  700 м после  поворота на Исаково', 'город Челябинск Троицкий тр.',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1683, 1046, 61.303389, 55.180665, 'Россия, Челябинск, улица 250-летия Челябинска, 14',
        'город Челябинск ул. 250 лет Челябинску 14', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1684, 1047, 61.433123, 55.117092, 'Россия, Челябинск, Новороссийская улица, 126',
        'город Челябинск ул. Новороссийская  126', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1685, 1048, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Бродокалмакский тр. въезд в город с СНТ "Учитель"', 'город Челябинск СНТ "Учитель"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1686, 1049, 61.294567, 55.171879, 'Россия, Челябинск, улица Салавата Юлаева, 29',
        'город Челябинск ул. Салавата Юлаева  29  поз. 1', 'город Челябинск ул. Салавата Юлаева  29 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1687, 1050, 61.471508, 55.172373, 'Россия, Челябинск, улица Танкистов, 138',
        'город Челябинск ул. Танкистов  138', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1688, 1051, 61.490535, 55.125844, 'Россия, Челябинск, Копейское шоссе',
        'город Челябинск Копейское шоссе  600 м от виадука  из города', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1689, 1052, 61.291998, 55.175643, 'Россия, Челябинск, улица 250-летия Челябинска, 32',
        'город Челябинск ул. 250 лет Челябинску  32', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1690, 1053, 61.430006, 55.21917, 'Россия, Челябинск, улица Северный Луч, 18',
        'город Челябинск ул. Северный луч  18/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1691, 1054, 61.472566, 55.173645, 'Россия, Челябинск, улица Танкистов',
        'город Челябинск пересечение ул. Танкистов и ул. Кулибина', 'город Челябинск ул. Танкистов / ул. Кулибина', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1692, 1055, 61.418777, 55.241447, 'Россия, Челябинск, Хлебозаводская улица, 7',
        'город Челябинск ул.Хлебозаводская  7/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1693, 1056, 61.443607, 55.214296, 'Россия, Челябинск, улица Героев Танкограда, 51П',
        'город Челябинск ул. Героев Танкограда  51-п/1  АЗС', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1694, 1057, 61.458384, 55.176182, 'Россия, Челябинск, улица Комарова, 133',
        'город Челябинск  ул. Комарова  133', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1695, 1058, 61.368265, 55.230862, 'Россия, Челябинск, Свердловский тракт, 10',
        'город Челябинск Свердловский тр.  10к/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1696, 1059, 61.378551, 55.174763, 'Россия, Челябинск, улица Братьев Кашириных, 73',
        'город Челябинск ул. Бр. Кашириных  73', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1697, 1060, 61.420421, 55.132057, 'Россия, Челябинск, Гражданская улица, 23',
        'город Челябинск автодорога Меридиан/ ул. Гражданская  23', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1699, 1061, 61.378748, 55.247231, 'Россия, Челябинск, шоссе Металлургов, 70',
        'город Челябинск пересечение шоссе Металлургов  70 и ул. Румянцева', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1701, 1062, 0, 0, '', 'город Челябинск Западное шоссе  1-й км', 'город Челябинск Западное шоссе  1-й км', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1702, 1063, 61.350802, 55.178131, 'Россия, Челябинск, улица Братьев Кашириных, 97',
        'город Челябинск ул. Бр. Кашириных  97', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1703, 1064, 61.415516, 55.115136, 'Россия, Челябинск, улица Артёма, 105',
        'город Челябинск автодорога Меридиан/ ул. Артема  105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1705, 1065, 61.385018, 55.122519, 'Россия, Челябинск, Троицкий тракт, 9',
        'город Челябинск Троицкий тр./ ул. Трактовая  напротив д. 9', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1706, 1066, 61.436142, 55.148323, 'Россия, Челябинск, улица Барбюса, 2/1',
        'город Челябинск ул. Харлова/ ул. Барбюса  2/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1708, 1067, 61.301969, 55.154002, 'Россия, Челябинск, посёлок Шершни, Степная улица, 2А',
        'город Челябинск ул. Гостевая/  ул. Степная  2а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1712, 1068, 61.394999, 55.183734, 'Россия, Челябинск, проспект Победы, 167', 'город Челябинск пр. Победы  167',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1713, 1069, 61.435773, 55.129648, 'Россия, Челябинск, Ленинский район, улица Гагарина, 33А',
        'город Челябинск ул. Гагарина  33а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1715, 1070, 61.385207, 55.118704, 'Россия, Челябинск, улица Дарвина, 2А', 'город Челябинск ул. Дарвина  2а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1716, 1071, 61.34525, 55.224855, 'Россия, Челябинск, Радонежская улица',
        'город Челябинск пересечение Свердловского тр. (в центр) и ул. Радонежская',
        'город Челябинск Свердловского тр.  ул. Радонежская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1717, 1072, 61.424913, 55.143096, 'Россия, Челябинск, улица Руставели, 32/1',
        'город Челябинск автодорога Меридиан/ ул. Шота Руставели  32/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1719, 1073, 61.387264, 55.091974, 'Россия, Челябинск, Троицкий тракт, 21А',
        'город Челябинск Троицкий тр.  напротив д. 21а', 'город Челябинск Троицкий тр.  д. 21а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1720, 1074, 61.367205, 55.130003, 'Россия, Челябинск, улица Блюхера, 63',
        'город Челябинск ул. Блюхера  напротив д. 63', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1721, 1075, 61.304341, 55.152742, 'Россия, Челябинск, посёлок Шершни, Садовая улица, 1А',
        'город Челябинск ул. Гостевая/  ул. Садовая  1а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1725, 1076, 61.382764, 55.173787, 'Россия, Челябинск, улица Братьев Кашириных, 65к1',
        'город Челябинск ул. Бр. Кашириных  65/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1726, 1077, 61.35205, 55.181385, 'Россия, Челябинск, улица Чайковского, 52',
        'город Челябинск ул. Чайковского  52', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1727, 1078, 61.418202, 55.156477, 'Россия, Челябинск, Российская улица, 275',
        'город Челябинск ул. Российская  275', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1728, 1079, 61.385027, 55.121299, 'Россия, Челябинск, Троицкий тракт, 9к2', 'город Челябинск Троицкий тр.  9к2',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1729, 1080, 61.411546, 55.16866, 'Россия, Челябинск, улица Свободы, 65',
        'город Челябинск ул. Труда/ ул. Свободы  65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1731, 1081, 61.385099, 55.125911, 'Россия, Челябинск, улица Рылеева, 20/1',
        'город Челябинск ул. Профинтерна/ ул. Рылеева  20/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1733, 1082, 61.358976, 55.11372, 'Россия, Челябинск, улица Блюхера, 101', 'город Челябинск ул. Блюхера 101',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1734, 1083, 61.451629, 55.173951, 'Россия, Челябинск, улица Героев Танкограда, 65',
        'город Челябинск ул. Героев Танкограда  65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1735, 1084, 61.387471, 55.088898, 'Россия, Челябинск, Троицкий тракт, 23', 'город Челябинск Троицкий тр.  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1736, 1085, 61.384965, 55.124691, 'Россия, Челябинск, Троицкий тракт, 1/1',
        'город Челябинск автодорога Меридиан/ Троицкий тр.  1/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1738, 1086, 61.437229, 55.197739, 'Россия, Челябинск, улица Героев Танкограда, 75П',
        'город Челябинск ул. Героев Танкограда  75-П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1739, 1087, 61.37485, 55.123703, 'Россия, Челябинск, улица Дарвина, 12', 'город Челябинск ул. Дарвина  12',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1740, 1088, 61.495394, 55.220341, 'Россия, Челябинск, Бродокалмакский тракт',
        'город Челябинск Бродокалмакский тр.(в центр)  550 м от поста ГИБДД', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1741, 1089, 61.4813, 55.194362, 'Россия, Челябинск, улица Мамина, 15', 'город Челябинск ул. Мамина  15', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1742, 1090, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  1868 км+30 м', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1743, 1091, 61.386483, 55.098501, 'Россия, Челябинск, Троицкий тракт, 17', 'город Челябинск Троицкий тр.  17',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1744, 1092, 61.512768, 55.120846, 'Россия, Челябинск, Копейское шоссе, 1/1',
        'город Челябинск Копейское шоссе  напротив д. 1/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1745, 1093, 61.429305, 55.150571, 'Россия, Челябинск, улица Игуменка, 9А',
        'город Челябинск автодорога Меридиан/ ул. Игуменка  9а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1747, 1094, 61.410558, 55.244337, 'Россия, Челябинск, Винницкая улица, 3',
        'город Челябинск пересечение ул. Хлебозаводская и ул. Винницкая  3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1749, 1095, 61.473098, 55.172661, 'Россия, Челябинск, улица Танкистов, 175',
        'город Челябинск ул. Танкистов  175', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1750, 1096, 61.427733, 55.148128, 'Россия, Челябинск, улица Тухачевского, 16',
        'город Челябинск автодорога Меридиан/ ул. Тухачевского  18', 'город Челябинск ул. Тухачевского  18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1751, 1097, 61.379144, 55.18248, 'Россия, Челябинск, Краснознамённая улица, 32',
        'город Челябинск ул. Краснознаменная  32', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1752, 1098, 61.406901, 55.247288, 'Россия, Челябинск, Хлебозаводская улица, 12',
        'город Челябинск ул. Хлебозаводская  напротив д. 12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1753, 1099, 61.468508, 55.16812, 'Россия, Челябинск, улица Танкистов, 148',
        'город Челябинск ул. Танкистов  напротив д. 148', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1754, 1100, 61.41981, 55.131084, 'Россия, Челябинск, Гражданская улица, 25',
        'город Челябинск автодорога Меридиан/ ул. Гражданская  напротив д.25', 'город Челябинск ул. Гражданская   д.25',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1755, 1101, 61.458411, 55.178177, 'Россия, Челябинск, улица Комарова, 110к1',
        'город Челябинск пер. Лермонтова/ ул. Комарова  д. 110 к1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1757, 1102, 0, 0, '', 'город Челябинск Троицкий тр. (в центр)  200 м после ост. "Подстанция"',
        'город Челябинск  ост. "Подстанция"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1758, 1103, 61.388333, 55.191706, 'Россия, Челябинск, Комсомольский проспект, 5',
        'город Челябинск Комсомольский пр.  5', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1760, 1104, 61.442529, 55.209555, 'Россия, Челябинск, улица Героев Танкограда, 59П',
        'город Челябинск ул. Героев Танкограда  59П к1', 'город Челябинск ул. Героев Танкограда  59П ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1761, 1105, 61.430464, 55.042648, 'Россия, Челябинск, посёлок Исаково, улица Морозова, 6',
        'город Челябинск Троицкий тр./ ул. Морозова 6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1764, 1106, 61.442933, 55.16108, 'Россия, Челябинск, проспект Ленина, 18',
        'город Челябинск пересечение пр. Ленина  18 и ул. Горького', 'город Челябинск пересечение пр. Ленина  18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1765, 1107, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  1869 км + 950 м', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1766, 1108, 61.441783, 55.20919, 'Россия, Челябинск, улица Героев Танкограда, 61П',
        'город Челябинск ул. Героев Танкограда  61П', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1767, 1109, 61.501386, 55.229476, 'Россия, Челябинск, Бродокалмакский тракт, 1/2',
        'город Челябинск Бродокалмакский тр.  3/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1768, 1110, 61.387246, 55.11823, 'Россия, Челябинск, Троицкий тракт, 32А', 'город Челябинск Троицкий тр.  32',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1769, 1111, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  (в город)  400 м после поста ГИБДД', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1770, 1112, 61.407818, 55.246374, 'Россия, Челябинск, Пекинская улица, 3',
        'город Челябинск пересечение ул. Хлебозаводская и ул. Пекинская  3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1772, 1113, 61.399796, 55.13812, 'Россия, Челябинск, улица Доватора, 1', 'город Челябинск ул. Доватора  1м',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1773, 1114, 61.361177, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 95',
        'город Челябинск ул. Бр. Кашириных  95', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1774, 1115, 61.397478, 55.168326, 'Россия, Челябинск, улица Труда, 100', 'город Челябинск ул. Труда  100', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1775, 1116, 61.376008, 55.245917, 'Россия, Челябинск, шоссе Металлургов, 43',
        'город Челябинск шоссе Металлургов  43', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1776, 1117, 61.517879, 55.158251, 'Россия, Челябинск, Линейная улица, 51', 'город Челябинск ул. Линейная  51',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1777, 1118, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.  после поста ГИБДД  въезд в город', 'город Челябинск Троицкий тр. ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1778, 1119, 61.354718, 55.179375, 'Россия, Челябинск, 2-й Северо-Крымский переулок, 10/1',
        'город Челябинск ул. Бр. Кашириных/ Северо-Крымский 2-й пер.  10/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1780, 1120, 61.404835, 55.249438, 'Россия, Челябинск, Хлебозаводская улица, 2',
        'город Челябинск ул. Хлебозаводская  напротив д. 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1781, 1121, 61.385018, 55.122519, 'Россия, Челябинск, Троицкий тракт, 9',
        'город Челябинск Троицкий тр.  напротив д. 9', 'город Челябинск Троицкий тр. д. 9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1782, 1122, 61.452949, 55.172697, 'Россия, Челябинск, Салютная улица, 10', 'город Челябинск ул. Салютная  10',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1783, 1123, 61.432216, 55.043598, 'Россия, Челябинск, посёлок Исаково',
        'город Челябинск Троицкий тр.  100 м за пос. Исаково', 'город Челябинск пос. Исаково', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1784, 1124, 61.29242, 55.18909, 'Россия, Челябинск, Комсомольский проспект, 105',
        'город Челябинск Комсомольский пр.  105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1785, 1125, 61.360935, 55.237438, 'Россия, Челябинск, Новгородская улица, 1Г',
        'город Челябинск Свердловский тр./ ул. Новгородская  1г', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1787, 1126, 61.383563, 55.145421, 'Россия, Челябинск, улица Доватора, 31', 'город Челябинск Доватора  31', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1788, 1127, 61.299086, 55.186397, 'Россия, Челябинск, проспект Победы, 382А',
        'город Челябинск пр. Победы  напротив д. 382а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1789, 1128, 61.387938, 55.10736, 'Россия, Челябинск, Троицкий тракт, 54Б', 'город Челябинск Троицкий тр  54б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1790, 1129, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр.  (в город)  50 м после поста ГИБДД', 'город Челябинск Свердловский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1791, 1130, 61.375487, 55.040709, 'Россия, Челябинск, посёлок Новосинеглазово',
        'город Челябинск Троицкий тр.  поворот на Новосинеглазово  въезд', 'город Челябинск  Новосинеглазово', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1792, 1131, 61.333689, 55.198257, 'Россия, Челябинск, улица Куйбышева, 75',
        'город Челябинск пересечение ул. Молодогвардейцев и ул. Куйбышева  75', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1794, 1132, 0, 0, 'Россия, Челябинская область, Копейск, посёлок Потанино, Новороссийская улица',
        'город Челябинск пересечение автодороги Меридиан и ул. Новороссийская',
        'город Челябинск  автодороги Меридиан / ул. Новороссийская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1795, 1133, 61.412282, 55.168465, 'Россия, Челябинск, улица Труда, 56',
        'город Челябинск ул. Красноармейская/ ул. Труда  56', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1798, 1134, 61.421077, 55.132767, 'Россия, Челябинск, улица Дзержинского, 125',
        'город Челябинск автодорога Меридиан 170 м до виадука/ ул. Дзержинского  125', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1807, 1135, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Западное шоссе  поворот на п. Западный 2', 'город Челябинск пос. Западный 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1808, 1136, 0, 0, '', 'город Челябинск пересечение ул. Бр. Кашириных  133/2 и ул. Чичерина',
        'город Челябинск пересечение ул. Бр. Кашириных  133/2 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1809, 1137, 61.385674, 55.152058, 'Россия, Челябинск, улица Курчатова, 28', 'город Челябинск ул. Курчатова  28',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1810, 1138, 61.36645, 55.232654, 'Россия, Челябинск, Свердловский тракт, 8',
        'город Челябинск Свердловский тр.  напротив д. 8', 'город Челябинск Свердловский тр.  д. 8', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1811, 1139, 61.416235, 55.144181, 'Россия, Челябинск, улица Свободы, 179', 'город Челябинск ул. Свободы  179',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1812, 1140, 61.387839, 55.104883, 'Россия, Челябинск, Троицкий тракт, 56', 'город Челябинск Троицкий тр.  56',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1813, 1141, 61.45029, 55.160905, 'Россия, Челябинск, проспект Ленина, 10',
        'город Челябинск пересечение пр. Ленина  10 и ул. Героев Танкограда', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1816, 1142, 61.434291, 55.162016, 'Россия, Челябинск, Артиллерийская улица, 119',
        'город Челябинск ул. Артиллерийская  119', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1817, 1143, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина  76  поз. 2', 'город Челябинск пр. Ленина  76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1819, 1144, 61.45029, 55.160905, 'Россия, Челябинск, проспект Ленина, 10', 'город Челябинск пр. Ленина  10',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1822, 1145, 61.448853, 55.160823, 'Россия, Челябинск, улица 40-летия Октября, 26',
        'город Челябинск пр. Ленина/ ул. 40 лет Октября  26', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1824, 1146, 61.436519, 55.161481, 'Россия, Челябинск, проспект Ленина, 24', 'город Челябинск пр. Ленина  24',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1826, 1147, 61.426224, 55.160391, 'Россия, Челябинск, проспект Ленина, 21В', 'город Челябинск  пр. Ленина  21в',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1828, 1148, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр. (из центра)  250 м до поворота на ЧВВАКУШ',
        'город Челябинск Свердловский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1832, 1149, 61.469963, 55.1703, 'Россия, Челябинск, улица Танкистов, 144', 'город Челябинск ул. Танкистов  144',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1833, 1150, 61.368948, 55.148472, 'Россия, Челябинск, улица Худякова, 12к1',
        'город Челябинск пересечение ул. Худякова 12к1 и ул. Татьяничевой', 'город Челябинск  ул. Худякова 12к1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1834, 1151, 0, 0, '', 'город Челябинск Троицкий тр.  напротив д.35к1', 'город Челябинск Троицкий тр.   д.35к1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1835, 1152, 61.405437, 55.248499, 'Россия, Челябинск, Хлебозаводская улица, 4',
        'город Челябинск ул. Хлебозаводская  напротив д. 4', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1836, 1153, 61.375487, 55.040709, 'Россия, Челябинск, посёлок Новосинеглазово',
        'город Челябинск развяка на пос. Новосинеглазово  поз. 1', 'город Челябинск пос. Новосинеглазово', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1837, 1154, 0, 0, 'Россия, Челябинская область, Копейск, посёлок Потанино, Новороссийская улица',
        'город Челябинск пересечение автодороги Меридиан и ул. Новороссийская',
        'город Челябинск автодороги Меридиан / ул. Новороссийская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1838, 1155, 61.416298, 55.046362, 'Россия, Челябинск, посёлок Исаково, Трактовая улица, 41',
        'город Челябинск Троицкий тр.  п. Исаково/ ул. Трактовая  напротив д. 41',
        'город Челябинск п. Исаково ул. Трактоваяд.41', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1839, 1156, 61.419765, 55.185065, 'Россия, Челябинск, проспект Победы, 137', 'город Челябинск пр. Победы  137',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1840, 1157, 61.435073, 55.132129, 'Россия, Челябинск, улица Дзержинского, 102',
        'город Челябинск ул. Дзержинского  102', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1841, 1158, 61.366325, 55.125381, 'Россия, Челябинск, Мебельная улица, 88',
        'город Челябинск ул. Блюхера/ ул. Мебельная  88', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1843, 1159, 61.374724, 55.161347, 'Россия, Челябинск, улица Энтузиастов, 2',
        'город Челябинск ул. Коммуны/ ул. Энтузиастов  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1848, 1160, 61.465984, 55.189383, 'Россия, Челябинск, улица Бажова, 35',
        'город Челябинск ул. Бажова  35/ Лермонтовский пер.  4а', 'город Челябинск ул. Бажова  35', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1850, 1161, 61.384605, 55.195323, 'Россия, Челябинск, Цинковая улица, 2Ак1',
        'город Челябинск Свердловский тр./ ул. Цинковая  2а/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1852, 1162, 61.431596, 55.167909, 'Россия, Челябинск, улица Первой Пятилетки, 57',
        'город Челябинск ул. Первой Пятилетки  напротив д.57', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1853, 1163, 61.460324, 55.178403, 'Россия, Челябинск, улица Комарова, 129', 'город Челябинск ул. Комарова  129',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1854, 1164, 61.391792, 55.247672, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 1',
        'город Челябинск ул. 50 лет ВЛКСМ  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1855, 1165, 61.4434, 55.199866, 'Россия, Челябинск, улица Героев Танкограда, 71Пс1',
        'город Челябинск ул. Героев Танкограда  71П ст10', 'город Челябинск ул. Героев Танкограда  71П', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1856, 1166, 61.365741, 55.236458, 'Россия, Челябинск, Свердловский тракт, 1Бк1',
        'город Челябинск Свердловский тр.  1б/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1857, 1167, 61.433878, 55.13269, 'Россия, Челябинск, улица Дзержинского, 104',
        'город Челябинск ул. Дзержинского  104', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1858, 1168, 61.387094, 55.126946, 'Россия, Челябинск, улица Марата, 8',
        'город Челябинск ул. Профинтерна/ ул. Марата  8', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1860, 1169, 61.285755, 55.187774, 'Россия, Челябинск, Комсомольский проспект, 112/1',
        'город Челябинск ул. Чичерина/ Комсомольский пр.  напротив д. 112/1',
        'город Челябинск Комсомольский пр.  д. 112/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1861, 1170, 61.409102, 55.106788, 'Россия, Челябинск, Приозёрная улица, 4',
        'город Челябинск автодорога Меридиан/ ул. Приозерная  4', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1863, 1171, 61.432216, 55.043598, 'Россия, Челябинск, посёлок Исаково',
        'город Челябинск Троицкий тр.  800 м до поворота на п. Исаково', 'город Челябинск п. Исаково', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1864, 1172, 61.308527, 55.194018, 'Россия, Челябинск, Комсомольский проспект, 85',
        'город Челябинск Комсомольский пр.  85', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1865, 1173, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск автодорога Меридиан/ ул. Артиллерийская  136', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1868, 1174, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  въезд  900 м до поста ГИБДД', 'город Челябинск Уфимский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1869, 1175, 61.372226, 55.192333, 'Россия, Челябинск, Комсомольский проспект, 12',
        'город Челябинск Комсомольский пр.  12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1871, 1176, 61.375487, 55.040709, 'Россия, Челябинск, посёлок Новосинеглазово',
        'город Челябинск Троицкий тр.  выезд из города  после поворота на Новосинеглазово',
        'город Челябинск Новосинеглазово', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1872, 1177, 61.37714, 55.121124, 'Россия, Челябинск, улица Дарвина, 63',
        'город Челябинск ул. Дарвина  напротив д. 63', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1873, 1178, 61.490535, 55.125844, 'Россия, Челябинск, Копейское шоссе',
        'город Челябинск Копейское шоссе (из города)  300 м от виадука', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1874, 1179, 61.424085, 55.192041, 'Россия, Челябинск, Механическая улица',
        'город Челябинск пересечение ул. Механическая и ул.Артиллерийская (из центра)',
        'город Челябинск ул. Механическая / ул.Артиллерийская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1875, 1180, 61.443202, 55.165827, 'Россия, Челябинск, улица Горького, 12', 'город Челябинск ул. Горького  12',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1876, 1181, 61.386114, 55.186546, 'Россия, Челябинск, Свердловский проспект, 22',
        'город Челябинск Свердловский пр.  20', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1877, 1182, 61.435621, 55.163826, 'Россия, Челябинск, Артиллерийская улица, 117',
        'город Челябинск ул. Артиллерийская  117', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1878, 1183, 61.440876, 55.212925, 'Россия, Челябинск, улица Героев Танкограда, 52П/3',
        'город Челябинск ул. Героев Танкограда  52П/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1879, 1184, 61.50701, 55.121335, 'Россия, Челябинск, Копейское шоссе, 5', 'город Челябинск Копейскре шоссе  5',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1880, 1185, 61.38668, 55.101045, 'Россия, Челябинск, Контейнерная улица, 2',
        'город Челябинск Троицкий тр.  / ул. Крнтейнерная  напротив д.2',
        'город Челябинск ул. Контейнерная  напротив д.2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1881, 1186, 61.442654, 55.208409, 'Россия, Челябинск, Валдайская улица, 1А',
        'город Челябинск ул. Героев Танкограда/ ул. Валдайская  напротив д. 1а',
        'город Челябинск ул. Валдайская  д. 1а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1882, 1187, 61.379485, 55.181575, 'Россия, Челябинск, Краснознамённая улица, 36',
        'город Челябинск ул. Краснознаменная  36', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1883, 1188, 61.363935, 55.191135, 'Россия, Челябинск, улица Чайковского, 15',
        'город Челябинск ул. Чайковского  напротив д. 15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1884, 1189, 61.385899, 55.111789, 'Россия, Челябинск, Троицкий тракт, 11А', 'город Челябинск Троицкий тр.  11а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1885, 1190, 61.424733, 55.185944, 'Россия, Челябинск, проспект Победы, 140', 'город Челябинск пр. Победы  140',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1886, 1191, 61.398915, 55.090887, 'Россия, Челябинск, улица Игуменка, 155',
        'город Челябинск автодорога Меридиан/ ул. Игуменка  155', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1888, 1192, 61.422218, 55.043969, 'Россия, Челябинск, посёлок Исаково, Трактовая улица, 8',
        'город Челябинск Троицкий тр. / ул. Трактовая  напротив д. 8', 'город Челябинск ул. Трактовая   д. 8', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1889, 1193, 61.453758, 55.1609, 'Россия, Челябинск, проспект Ленина, 8', 'город Челябинск пр. Ленина  8', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1891, 1194, 61.483743, 55.173745, 'Россия, Челябинск, улица Мамина, 27А',
        'город Челябинск ул. Кулибина/ ул. Мамина  27а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1893, 1195, 61.427644, 55.163451, 'Россия, Челябинск, автодорога Меридиан, 1/1',
        'город Челябинск автодорога Меридиан  1/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1894, 1196, 61.318741, 55.077344, 'Россия, Челябинск, Уфимский тракт, 1', 'город Челябинск Уфимский тр.  1/2',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1895, 1197, 61.385198, 55.124202, 'Россия, Челябинск, Троицкий тракт, 3А', 'город Челябинск Троицкий тр.  3а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1896, 1198, 61.382081, 55.199434, 'Россия, Челябинск, Свердловский тракт, 38',
        'город Челябинск Свердловский тр.  38  завод транспортных трансмиссий', 'город Челябинск Свердловский тр.  38',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1898, 1199, 61.321687, 55.187954, 'Россия, Челябинск, проспект Победы, 293А',
        'город Челябинск пересечение пр. Победы  293а и ул. Ворошилова', 'город Челябинск пр. Победы  293а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1907, 1200, 61.472155, 55.171231, 'Россия, Челябинск, улица Танкистов, 177',
        'город Челябинск ул. Танкистов  177', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1908, 1201, 61.371654, 55.19185, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересечение Комсомольского пр. и ул. Косарева  у автобазы',
        'город Челябинск Комсомольского пр. / ул. Косарева ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1909, 1202, 61.453614, 55.166089, 'Россия, Челябинск, улица Первой Пятилетки, 17',
        'город Челябинск ул. Первой Пятилетки  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1910, 1203, 0, 0, '', 'город Челябинск Троицкий тр.  напротив д. 5к1', 'город Челябинск Троицкий тр. д. 5к1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1911, 1204, 61.511636, 55.122617, 'Россия, Челябинск, Копейское шоссе, 4', 'город Челябинск Копейское шоссе  4',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1912, 1205, 61.436366, 55.17866, 'Россия, Челябинск, улица Горького, 57', 'город Челябинск ул. Горького  57',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1913, 1206, 61.448098, 55.177288, 'Россия, Челябинск, улица Котина, 17',
        'город Челябинск ул. Котина  напротив д. 17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1914, 1207, 61.387085, 55.09338, 'Россия, Челябинск, Троицкий тракт, 21', 'город Челябинск Троицкий тр.  21',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1915, 1208, 61.323134, 55.179349, 'Россия, Челябинск, улица Ворошилова, 57А',
        'город Челябинск пересечение ул. Бр. Кашириных и ул. Ворошилова  57а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1917, 1209, 61.417097, 55.162042, 'Россия, Челябинск, проспект Ленина, 38',
        'город Челябинск ул. Российская/ пр. Ленина  38', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1920, 1210, 61.290183, 55.184901, 'Россия, Челябинск, улица Чичерина, 15', 'город Челябинск ул. Чичерина  15/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1921, 1211, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  1868 км + 510 м', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1922, 1212, 61.406003, 55.185538, 'Россия, Челябинск, улица Болейко, 7', 'город Челябинск ул. Болейко  7', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1923, 1213, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.  напротив поста ГИБДД', 'город Челябинск Троицкий тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1924, 1214, 61.394693, 55.249032, 'Россия, Челябинск, шоссе Металлургов, 15',
        'город Челябинск шоссе Металлургов  15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1925, 1215, 61.385001, 55.125319, 'Россия, Челябинск, Троицкий тракт, 1Ак1',
        'город Челябинск Троицкий тр.  1ак1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1926, 1216, 61.390265, 55.258243, 'Россия, Челябинск, улица Богдана Хмельницкого, 21к1',
        'город Челябинск ул. Богдана Хмельницкого  21/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1927, 1217, 61.403928, 55.263522, 'Россия, Челябинск, улица Сталеваров, 5', 'город Челябинск ул. Сталеваров  5',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1928, 1218, 61.441882, 55.17009, 'Россия, Челябинск, улица Горького, 19',
        'город Челябинск ул. Горького  напротив д. 19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1929, 1219, 61.358976, 55.11372, 'Россия, Челябинск, улица Блюхера, 101',
        'город Челябинск Уфимский тр./ ул. Блюхера  101', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1931, 1220, 61.389582, 55.247585, 'Россия, Челябинск, шоссе Металлургов, 25Б',
        'город Челябинск шоссе Металлургов  25б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1932, 1221, 61.385198, 55.124202, 'Россия, Челябинск, Троицкий тракт, 3А',
        'город Челябинск Троицкий тр.  напротив д. 3а', 'город Челябинск Троицкий тр.  д. 3а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1933, 1222, 61.394047, 55.161023, 'Россия, Челябинск, Красная улица, 63', 'город Челябинск ул. Красная  63',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1934, 1223, 61.390301, 55.16701, 'Россия, Челябинск, Свердловский проспект, 53Ак1',
        'город Челябинск Свердловский пр.  53а/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1935, 1224, 61.409785, 55.165313, 'Россия, Челябинск, улица Маркса, 81', 'город Челябинск ул. Карла Маркса  81',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1936, 1225, 61.38112, 55.146147, 'Россия, Челябинск, улица Воровского, 40',
        'город Челябинск уд. Доватора/ ул.Воровского  40', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1938, 1226, 61.41495, 55.167539, 'Россия, Челябинск, улица Труда, 61',
        'город Челябинск ул. Российская/ ул. Труда  61', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1940, 1227, 61.387767, 55.164258, 'Россия, Челябинск, Свердловский проспект, 56',
        'город Челябинск Свердловский пр. напротив д. 56', 'город Челябинск Свердловский пр. д. 56', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1941, 1228, 61.381335, 55.166953, 'Россия, Челябинск, улица Энгельса, 23', 'город Челябинск ул. Энгельса  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1942, 1229, 61.374068, 55.160221, 'Россия, Челябинск, проспект Ленина, 74',
        'город Челябинск ул. Энтузиастов/ пр. Ленина  74', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1945, 1230, 61.386393, 55.168634, 'Россия, Челябинск, Свердловский проспект, 40А',
        'город Челябинск Свердловский пр.  40а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1947, 1231, 61.339923, 55.10206, 'Россия, Челябинск, Заводская улица, 7',
        'город Челябинск ул. Гостевая/ ул. Заводская  7', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1951, 1232, 61.420421, 55.161219, 'Россия, Челябинск, проспект Ленина, 34', 'город Челябинск пр. Ленина  34',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1953, 1233, 61.449904, 55.159717, 'Россия, Челябинск, проспект Ленина, 9', 'город Челябинск пр. Ленина  9',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1955, 1234, 61.430374, 55.161527, 'Россия, Челябинск, проспект Ленина, 26к1',
        'город Челябинск пр. Ленина  26/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1957, 1235, 61.447326, 55.159954, 'Россия, Челябинск, проспект Ленина, 11', 'город Челябинск пр. Ленина  11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1959, 1236, 61.423709, 55.160633, 'Россия, Челябинск, проспект Ленина, 23', 'город Челябинск пр. Ленина  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1961, 1237, 61.433941, 55.160273, 'Россия, Челябинск, Артиллерийская улица, 121',
        'город Челябинск ул. Артиллерийская  121', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1962, 1238, 61.399392, 55.171468, 'Россия, Челябинск, улица Кирова, 78', 'город Челябинск ул. Кирова  78/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1963, 1239, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина  76  поз. 1', 'город Челябинск пр. Ленина  76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1965, 1240, 61.422918, 55.161281, 'Россия, Челябинск, проспект Ленина, 30', 'город Челябинск пр. Ленина  30',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1967, 1241, 61.445807, 55.159804, 'Россия, Челябинск, улица Рождественского, 5',
        'город Челябинск ул. Рождественского  напротив д. 5  поз. 1', 'город Челябинск ул. Рождественского  д. 5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1968, 1242, 61.392142, 55.152989, 'Россия, Челябинск, улица Воровского, 17',
        'город Челябинск ул. Воровского  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1969, 1243, 61.382027, 55.15945, 'Россия, Челябинск, проспект Ленина, 71А',
        'город Челябинск ул. Энгельса/ пр. Ленина  71А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1972, 1244, 61.371616, 55.040146, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова',
        'город Челябинск ул. Кирова (в центр)  150 м  до моста', 'город Челябинск ул. Кирова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1973, 1245, 61.380509, 55.161332, 'Россия, Челябинск, улица Энгельса, 26', 'город Челябинск ул. Энгельса  26',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1974, 1246, 61.445807, 55.159804, 'Россия, Челябинск, улица Рождественского, 5',
        'город Челябинск ул. Рождественского  напротив д. 5  поз. 2', 'город Челябинск ул. Рождественского   д. 5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1975, 1247, 61.415947, 55.161311, 'Россия, Челябинск, Российская улица, 200',
        'город Челябинск ул. Российская  202', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1976, 1248, 61.40594, 55.164269, 'Россия, Челябинск, улица Цвиллинга, 25к2',
        'город Челябинск пересечение ул. Цвиллинга  25к2 и ул. Коммуны', 'город Челябинск ул. Цвиллинга  25к2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1977, 1249, 61.392987, 55.199619, 'Россия, Челябинск, Свердловский тракт, 25А',
        'город Челябинск Свердловский тр.  29', 'город Челябинск Свердловский тр.  29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1978, 1250, 61.361788, 55.191911, 'Россия, Челябинск, улица Чайковского, 20/3',
        'город Челябинск Комсомольский пр./ ул. Чайковского  20/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1981, 1251, 61.411788, 55.165888, 'Россия, Челябинск, улица Маркса, 38',
        'город Челябинск ул. Свободы/ ул. К. Маркса  38', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1983, 1252, 61.386249, 55.109755, 'Россия, Челябинск, Троицкий тракт, 11', 'город Челябинск Троицкий тр. 11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1984, 1253, 61.433339, 55.125536, 'Россия, Челябинск, улица Гончаренко, 63',
        'город Челябинск ул. Гагарина/ ул. Гончаренко  63', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1987, 1254, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловкий тр.  цинковый завод', 'город Челябинск Свердловкий тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1988, 1255, 61.403578, 55.262403, 'Россия, Челябинск, улица Сталеваров, 5к3',
        'город Челябинск ул. Сталеваров  5к3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1989, 1256, 61.416136, 55.141315, 'Россия, Челябинск, Привокзальная площадь, 1',
        'город Челябинск ул.Свободы/ Привокзальная площадь  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1992, 1257, 61.441154, 55.16359, 'Россия, Челябинск, улица Горького, 7', 'город Челябинск ул. Горького  7',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1993, 1258, 61.380446, 55.191567, 'Россия, Челябинск, Комсомольский проспект, 17',
        'город Челябинск Комсомолький пр.  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1995, 1259, 61.416001, 55.21897, 'Россия, Челябинск, улица Северный Луч, 47',
        'город Челябинск ул. Северный луч  напротив д. 47', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1996, 1260, 61.361168, 55.188396, 'Россия, Челябинск, улица Чайковского, 83',
        'город Челябинск пр. Победы/ ул. Чайковского  83', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (1999, 1261, 61.416001, 55.21897, 'Россия, Челябинск, улица Северный Луч, 47',
        'город Челябинск пересечение ул. Северный луч  47 и ул. Хлебозаводская', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2000, 1262, 61.38147, 55.217516, 'Россия, Челябинск, Свердловский тракт, 3/3',
        'город Челябинск Свердловский тр.  3/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2001, 1263, 61.441783, 55.20919, 'Россия, Челябинск, улица Героев Танкограда, 61П',
        'город Челябинск ул. Героев Танкограда  61п  поз. 2', 'город Челябинск ул. Героев Танкограда  61п', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2002, 1264, 61.370115, 55.23021, 'Россия, Челябинск, Свердловский тракт, 14Б',
        'город Челябинск Свердловский тр.  напротив д. 14б', 'город Челябинск Свердловский тр. д. 14б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2003, 1265, 61.394289, 55.164942, 'Россия, Челябинск, улица Маркса, 131',
        'город Челябинск ул. Красная/ ул. Карла Маркса 133', 'город Челябинск ул. Карла Маркса 133', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2004, 1266, 61.388962, 55.151801, 'Россия, Челябинск, улица Воровского, 26',
        'город Челябинск пересечение Свердловского пр.  и ул. Воровского  26', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2006, 1267, 61.331991, 55.186546, 'Россия, Челябинск, улица Молодогвардейцев, 56',
        'город Челябинск ул. Молодогвардейцев  56', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2007, 1268, 61.386474, 55.184377, 'Россия, Челябинск, проспект Победы, 192А',
        'город Челябинск пересечение Свердловского пр. и пр. Победы  192а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2009, 1269, 61.378371, 55.230436, 'Россия, Челябинск, Черкасская улица, 15',
        'город Челябинск ул. Черкасская 15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2010, 1270, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  1868 км + 830 м', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2011, 1271, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  1871 км + 120 м', 'город Челябинск Уфимский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2012, 1272, 61.305248, 55.194953, 'Россия, Челябинск, Комсомольский проспект, 84',
        'город Челябинск Комсомольский пр.  84', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2013, 1273, 61.380347, 55.258089, 'Россия, Челябинск, улица Богдана Хмельницкого, 35',
        'город Челябинск пересечение ул. Богдана Хмельницкого  35 и ул. Румянцева',
        'город Челябинск ул. Богдана Хмельницкого  35', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2014, 1274, 61.423116, 55.133334, 'Россия, Челябинск, улица Дзержинского, 119',
        'город Челябинск ул. Дзержинского  119', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2015, 1275, 61.374796, 55.224341, 'Россия, Челябинск, Свердловский тракт, 12В',
        'город Челябинск Свердловский тр.  напротив д. 12в', 'город Челябинск Свердловский тр.  д. 12в', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2016, 1276, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт',
        'город Челябинск Уфимский тр.  1869 км + 850 м', 'город Челябинск Уфимский тр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2017, 1277, 61.393084, 55.167587, 'Россия, Челябинск, Красная улица',
        'город Челябинск пересечение ул. Красная (из центра) и ул. Труда', 'город Челябинск ул. Красная / ул. Труда',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2018, 1278, 61.393318, 55.162616, 'Россия, Челябинск, Красная улица',
        'город Челябинск пересечение ул. Красная (из центра) и ул. Коммуны',
        'город Челябинск ул. Красная / ул. Коммуны', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2019, 1279, 61.37661, 55.135907, 'Россия, Челябинск, Междугородная улица, 21',
        'город Челябинск ул. Салтыкова/ ул. Междугородная  21', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2021, 1280, 61.318741, 55.077344, 'Россия, Челябинск, Уфимский тракт, 1',
        'город Челябинск Уфимский тр.  1/1 1868 км + 550 м', 'город Челябинск Уфимский тр.  1/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2022, 1281, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск автодорога Меридиан / ул. Артиллерийкая  136  поз. 1',
        'город Челябинск ул. Артиллерийкая  136', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2024, 1282, 61.38607, 55.204156, 'Россия, Челябинск, Свердловский тракт, 1А/2',
        'город Челябинск Свердловкий тр.  1а/2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2025, 1283, 61.380473, 55.187584, 'Россия, Челябинск, улица Островского, 29',
        'город Челябинск ул. Краснознаменная/ ул. Островского  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2027, 1284, 61.331946, 55.182017, 'Россия, Челябинск, улица Молодогвардейцев, 66',
        'город Челябинск ул. Молодогвардейцев  66', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2028, 1285, 61.383249, 55.242432, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 16',
        'город Челябинск ул. 50 лет ВЛКСМ  16', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2029, 1286, 61.417727, 55.239627, 'Россия, Челябинск, Хлебозаводская улица',
        'город Челябинск пересечение ул. Хлебозаводская и ул. Морская',
        'город Челябинск ул. Хлебозаводская / ул. Морская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2031, 1287, 61.387965, 55.117957, 'Россия, Челябинск, остановочный пункт Подстанция',
        'город Челябинск Троицкий тр.  100 м за после ост. "Подстанция" (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2032, 1288, 61.421032, 55.180465, 'Россия, Челябинск, улица Либединского, 39',
        'город Челябинск автодорога Меридиан/ ул. Либединского  39', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2034, 1289, 61.358283, 55.178598, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул. Бр. Кашириных (из центра) и ул. Северо-Крымская',
        'город Челябинск ул. Бр. Кашириных / ул. Северо-Крымская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2035, 1290, 61.437004, 55.160525, 'Россия, Челябинск, проспект Ленина, 17',
        'город Челябинск пр. Ленина  17  за остановкой из центра', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2037, 1291, 61.38041, 55.241668, 'Россия, Челябинск, Черкасская улица, 5', 'город Челябинск ул.Черкасская 7',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2038, 1292, 61.298223, 55.184541, 'Россия, Челябинск, проспект Победы, 317', 'город Челябинск пр.Победы 317',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2039, 1293, 61.397415, 55.076891, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 32',
        'город Челябинск ул.Гагарина  на газоне между д.32 и д.36', 'город Челябинск ул.Гагарина д.32', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2040, 1294, 61.41363, 55.153966, 'Россия, Челябинск, улица Свободы, 88', 'город Челябинск ул.Свободы 88', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2041, 1295, 61.45533, 55.16054, 'Россия, Челябинск, проспект Ленина, 6', 'город Челябинск пр.Ленина 6', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2043, 1296, 61.332009, 55.190719, 'Россия, Челябинск, улица Молодогвардейцев, 48',
        'город Челябинск ул.Молодогвардейцев напротив д.48', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2044, 1297, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Свердловский пр.  ост. "Торговый центр" (из центра)', 'город Челябинскост. "Торговый центр"',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2045, 1298, 61.389061, 55.148781, 'Россия, Челябинск, улица Курчатова, 19А',
        'город Челябинск ул. Курчатова  19А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2046, 1299, 61.380985, 55.150191, 'Россия, Челябинск, улица Энгельса, 99', 'город Челябинск ул.Энгельса 99',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2047, 1300, 61.442852, 55.170496, 'Россия, Челябинск, улица Горького, 22', 'город Челябинск ул.Горького 22',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2048, 1301, 61.370484, 55.184772, 'Россия, Челябинск, проспект Победы, 238', 'город Челябинск  пр.Победы 238',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2049, 1302, 61.440975, 55.136607, 'Россия, Челябинск, улица Южный Бульвар, 19',
        'город Челябинск пересечение ул. Гагарина и ул. Южный Бульвар  19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2052, 1303, 61.32138, 55.194387, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересеч.Комсомольского пр.(из центра) и ул.Ворошилова у АЗС',
        'город Челябинск Комсомольского пр./ ул.Ворошилова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2053, 1304, 61.388773, 55.157192, 'Россия, Челябинск, Свердловский проспект, 78',
        'город Челябинск Свердловский пр 78 начало дома', 'город Челябинск Свердловский пр 78', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2054, 1305, 61.384542, 55.151204, 'Россия, Челябинск, улица Курчатова, 23',
        'город Челябинск ул.Курчатова 23 за маг."Декорум"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2055, 1306, 61.400667, 55.173874, 'Россия, Челябинск, улица Кирова, 23А', 'город Челябинск ул.Кирова 23-а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2057, 1307, 61.379916, 55.151276, 'Россия, Челябинск, улица Энгельса, 46', 'город Челябинск ул.Энгельса 46',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2058, 1308, 61.457234, 55.177452, 'Россия, Челябинск, улица Комарова, 110',
        'город Челябинск ул.Комарова 110 поз.2', 'город Челябинск ул.Комарова 110', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2059, 1309, 61.339887, 55.18781, 'Россия, Челябинск, проспект Победы, 287',
        'город Челябинск пр.Победы 287  больница Скорой помощи (в центр)', 'город Челябинск пр.Победы 287', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2060, 1310, 61.36866, 55.17681, 'Россия, Челябинск, улица Братьев Кашириных, 87А',
        'город Челябинск ул.Бр.Кашириных  87а (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2061, 1311, 61.43722, 55.151132, 'Россия, Челябинск, улица Луценко, 2/2',
        'город Челябинск автодорога Меридиан  со стороны дома 2/2 по ул.Луценко', 'город Челябинск ул.Луценко  2/2',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2062, 1312, 61.343795, 55.189532, 'Россия, Челябинск, проспект Победы, 308', 'город Челябинск пр.Победы 308',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2063, 1313, 0, 0, '', 'город Челябинск Свердловский тр. (из центра)  перед трамвайным депо № 2',
        'город Челябинск трамвайное депо № 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2064, 1314, 61.387785, 55.151559, 'Россия, Челябинск, улица Курчатова, 24', 'город Челябинск ул.Курчатова 24',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2065, 1315, 61.332594, 55.189074, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересеч.пр.Победы (из центра) и ул.Молодогвардейцев',
        'город Челябинск пр.Победы / ул.Молодогвардейцев', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2066, 1316, 61.399643, 55.181806, 'Россия, Челябинск, улица Кирова, 3', 'город Челябинск ул.Кирова  3', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2069, 1317, 0, 0, '', 'город Челябинск ул.Труда ост. "ТРК "Родник"(из центра)',
        'город Челябинск ост. "ТРК "Родник"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2072, 1318, 61.387938, 55.172167, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск Свердловский пр.(из центр) набережная р.Миасс у Торгового центра',
        'город Челябинск Свердловский пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2073, 1319, 61.318759, 55.176501, 'Россия, Челябинск, улица Братьев Кашириных, 129',
        'город Челябинск ул.Бр.Кашириных 129 (ЧелГУ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2074, 1320, 61.442196, 55.140224, 'Россия, Челябинск, Ленинский район, улица Гагарина, 13',
        'город Челябинск ул.Гагарина 13 (газон перед Стоматологией)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2076, 1321, 61.38147, 55.165271, 'Россия, Челябинск, улица Энгельса, 43', 'город Челябинск ул.Энгельса 43',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2077, 1322, 61.375227, 55.183837, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пр.Победы(из центра)  перед Ленинградским мостом', 'город Челябинск пр.Победы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2078, 1323, 61.411725, 55.141428, 'Россия, Челябинск, улица Степана Разина, 4',
        'город Челябинск ул. Степана Разина  4', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2079, 1324, 61.457234, 55.177452, 'Россия, Челябинск, улица Комарова, 110',
        'город Челябинск ул.Комарова 110 поз.1', 'город Челябинск ул.Комарова 110', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2080, 1325, 61.388181, 55.163564, 'Россия, Челябинск, Свердловский проспект, 58',
        'город Челябинск пересечение Сверловского пр. 58 и ул. Коммуны', 'город Челябинск Сверловского пр. 58', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2081, 1326, 61.353894, 55.178709, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение  ул.Бр.Кашириных (из центра) и ул.Актюбинская',
        'город Челябинск  ул.Бр.Кашириных / ул.Актюбинская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2082, 1327, 61.380877, 55.152377, 'Россия, Челябинск, улица Энгельса, 95',
        'город Челябинск ул.Курчатова 27/ ул. Энгельса  95', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2084, 1328, 61.379853, 55.242458, 'Россия, Челябинск, Черкасская улица, 3', 'город Челябинск ул. Черкасская 3',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2086, 1329, 61.368543, 55.232797, 'Россия, Челябинск, Свердловский тракт',
        'город Челябинск Свердловский тр. (в центр)  авторынок', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2087, 1330, 61.349733, 55.239645, 'Россия, Челябинск, Индивидуальная улица, 63',
        'город Челябинск ул. Индивидуальная  63', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2088, 1331, 61.310809, 55.194959, 'Россия, Челябинск, Комсомольский проспект, 78',
        'город Челябинск Комсомольский пр.  78', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2089, 1332, 61.445161, 55.151127, 'Россия, Челябинск, Копейское шоссе, 92',
        'город Челябинск Копейское шоссе  100 м от виадука  из города', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2090, 1333, 61.416316, 55.243397, 'Россия, Челябинск, Хлебозаводская улица, 5А',
        'город Челябинск ул. Хлебозаводская 5а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2091, 1334, 61.3858, 55.131367, 'Россия, Челябинск, Московская улица, 26',
        'город Челябинск ул. Профинтерна/ ул. Московская  26', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2093, 1335, 61.385746, 55.106938, 'Россия, Челябинск, Троицкий тракт, 7', 'город Челябинск Троицкий тр.  7/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2094, 1336, 61.443607, 55.147376, 'Россия, Челябинск, улица Харлова, 2',
        'город Челябинск Копейское шоссе/ ул. Харлова  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2096, 1337, 61.45462, 55.181976, 'Россия, Челябинск, улица Бажова, 91', 'город Челябинск ул. Бажова  91', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2097, 1338, 61.441073, 55.162952, 'Россия, Челябинск, улица Горького, 5', 'город Челябинск ул.Горького  5',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2098, 1339, 61.368876, 55.192266, 'Россия, Челябинск, Комсомольский проспект, 16',
        'город Челябинск Комсомольский пр.  16', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2100, 1340, 61.438765, 55.186839, 'Россия, Челябинск, проспект Победы, 111', 'город Челябинск пр. Победы 111',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2101, 1341, 61.435468, 55.168763, 'Россия, Челябинск, Артиллерийская улица, 105',
        'город Челябинск ул. Артиллерийская  105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2102, 1342, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29  у завода "Прибор"', 'город Челябинск Комсомольский пр.  29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2103, 1343, 61.388387, 55.086775, 'Россия, Челябинск, Троицкий тракт',
        'город Челябинск Троицкий тр.  600 м после ост. "Подстанция"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2104, 1344, 61.304143, 55.192713, 'Россия, Челябинск, Молдавская улица, 19',
        'город Челябинск ул. Модавская  19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2105, 1345, 61.297729, 55.179216, 'Россия, Челябинск, улица Чичерина, 29', 'город Челябинск ул. Чичерина  29',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2106, 1346, 61.418229, 55.194203, 'Россия, Челябинск, Механическая улица, 40',
        'город Челябинск автодорога Меридиан/ул. Механическая  40', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2108, 1347, 61.447856, 55.144022, 'Россия, Челябинск, Копейское шоссе, 49',
        'город Челябинск Копейское шоссе  49', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2109, 1348, 61.379844, 55.236088, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 39',
        'город Челябинск ул. Черкасская/ ул. 50-летия ВЛКСМ  39', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2111, 1349, 61.434471, 55.041715, 'Россия, Челябинск, улица Грязева, 3',
        'город Челябинск Троицкий тр./ ул. Грязева  3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2113, 1350, 61.438244, 55.211846, 'Россия, Челябинск, улица Героев Танкограда, 6',
        'город Челябинск ул. Героев Танкограда  71П ст10', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2114, 1351, 61.39039, 55.257504, 'Россия, Челябинск, улица Жукова, 12/19', 'город Челябинск ул. Жукова 12',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2115, 1352, 61.415885, 55.240513, 'Россия, Челябинск, Хлебозаводская улица, 42',
        'город Челябинск ул. Хлебозаводская  напротив д. 42', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2116, 1353, 61.300074, 55.193602, 'Россия, Челябинск, Молдавская улица, 12',
        'город Челябинск Комсомольский пр. / ул. Молдавская  12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2119, 1354, 61.425398, 55.16864, 'Россия, Челябинск, улица Труда, 24', 'город Челябинск ул. Труда  24', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2120, 1355, 61.378389, 55.238752, 'Россия, Челябинск, Черкасская улица, 8', 'город Челябинск ул. Черкасская  8',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2121, 1356, 61.386186, 55.203863, 'Россия, Челябинск, Свердловский тракт, 1А/3',
        'город Челябинск Свердловский тр.  1а/3  поворот на Лакокрасочный завод',
        'город Челябинск Свердловский тр.  1а/3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2122, 1357, 61.39349, 55.188201, 'Россия, Челябинск, Каслинская улица, 19',
        'город Челябинск ул. Каслинская  19', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2123, 1358, 61.385899, 55.111789, 'Россия, Челябинск, Троицкий тракт, 11А', 'город Челябинск Троицкий тр.  11а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2124, 1359, 61.334515, 55.195015, 'Россия, Челябинск, Комсомольский проспект, 48',
        'город Челябинск пересечение ул. Молодогвардейцев и Комсомольского пр.  48', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2126, 1360, 61.384614, 55.193217, 'Россия, Челябинск, Комсомольский проспект, 2',
        'город Челябинск Свердловский пр./  Комсомольский пр.  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2129, 1361, 61.393238, 55.158189, 'Россия, Челябинск, улица Сони Кривой, 28',
        'город Челябинск ул. С. Кривой  28', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2130, 1362, 61.294917, 55.190195, 'Россия, Челябинск, Комсомольский проспект, 103',
        'город Челябинск Комсомольский пр.  103', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2131, 1363, 61.396724, 55.074494, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 7',
        'город Челябинск ул. Гагарина 7  Политех', 'город Челябинск ул. Гагарина 7 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2134, 1364, 61.396724, 55.074494, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 7',
        'город Челябинск пересечение ул. Гагарина 7 и ул. Тухачевского ', 'город Челябинск пересечение ул. Гагарина 7',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2135, 1365, 61.333581, 55.18103, 'Россия, Челябинск, улица Молодогвардейцев, 53',
        'город Челябинск ул. Молодогвардейцев 53', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2136, 1366, 61.411788, 55.165888, 'Россия, Челябинск, улица Маркса, 38', 'город Челябинск ул.Карла Маркса 38',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2137, 1367, 61.306066, 55.149218, 'Россия, Челябинск, посёлок Шершни, Центральная улица, 3Б',
        'город Челябинск п.Шершни  ул.Центральная 3б (ул.Гостевая со стороны стадиона)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2138, 1368, 61.308527, 55.194018, 'Россия, Челябинск, Комсомольский проспект, 85',
        'город Челябинск Комсомольский пр. 85', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2139, 1369, 61.376862, 55.15197, 'Россия, Челябинск, улица Энтузиастов, 15Д',
        'город Челябинск ул.Энтузиастов  15д', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2141, 1370, 61.460172, 55.139983, 'Россия, Челябинск, Копейское шоссе, 52',
        'город Челябинск Копейское шоссе  52  поз. 1', 'город Челябинск Копейское шоссе  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2142, 1371, 61.460172, 55.139983, 'Россия, Челябинск, Копейское шоссе, 52',
        'город Челябинск Копейское шоссе  52  поз. 1', 'город Челябинск Копейское шоссе  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2143, 1372, 61.460172, 55.139983, 'Россия, Челябинск, Копейское шоссе, 52',
        'город Челябинск Копейское шоссе  52  поз. 1', 'город Челябинск Копейское шоссе  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2144, 1373, 61.453084, 55.142195, 'Россия, Челябинск, Копейское шоссе, 58',
        'город Челябинск Копейское шоссе  58', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2145, 1374, 61.394846, 55.118972, 'Россия, Челябинск, улица Шарова, 59',
        'город Челябинск ул. Шарова  напротив д. 59', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2146, 1375, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Курганский тр. (перед развязкой ЧМЗ-Аэропорт-Курганский тр.)',
        'город Челябинск Курганский тр. ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2149, 1376, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Курганский тр. (после развязки ЧМЗ-Аэропорт-Курганский тр.)',
        'город Челябинск Курганский тр. ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2152, 1377, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Автодорога ЧМЗ (перед развязкой ЧМЗ-Аэропорт-Курганский тр.)',
        'город Челябинск Курганский тр. ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2155, 1378, 61.420044, 55.135583, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск автодорога Меридиан (7-ой км)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2156, 1379, 61.383698, 55.162109, 'Россия, Челябинск, улица Коммуны, 125', 'город Челябинск ул.Коммуны 125',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2157, 1380, 61.363369, 55.170321, 'Россия, Челябинск, улица Труда, 197', 'город Челябинск ул. Труда  197', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2158, 1381, 61.296382, 55.168526, 'Россия, Челябинск, улица Братьев Кашириных, 147',
        'город Челябинск ул. Бр. Кашириных 147', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2159, 1382, 61.388971, 55.153277, 'Россия, Челябинск, Свердловский проспект, 86',
        'город Челябинск Свердловский пр.  86', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2160, 1383, 61.393885, 55.148302, 'Россия, Челябинск, улица Курчатова, 8Б', 'город Челябинск ул. Курчатова  8б',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2161, 1384, 61.387857, 55.160967, 'Россия, Челябинск, Свердловский проспект, 64',
        'город Челябинск Свердловский пр.  напротив д. 64', 'город Челябинск Свердловский пр.  д. 64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2162, 1385, 61.391441, 55.149851, 'Россия, Челябинск, улица Курчатова, 16',
        'город Челябинск ул. Курчатова  между домами № 16 и № 14', 'город Челябинск ул. Курчатова № 16', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2165, 1386, 61.350218, 55.19519, 'Россия, Челябинск, Комсомольский проспект, 30',
        'город Челябинск Комсомольский пр.  30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2166, 1387, 61.304431, 55.181241, 'Россия, Челябинск, улица 250-летия Челябинска, 12',
        'город Челябинск ул.250 лет Челябинску 12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2167, 1388, 61.358392, 55.148097, 'Россия, Челябинск, улица Худякова, 22', 'город Челябинск ул. Худякова  22',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2168, 1389, 61.301394, 55.176871, 'Россия, Челябинск, улица Чичерина, 33', 'город Челябинск ул.Чичерина  33',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2169, 1390, 61.375227, 55.183837, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пр.Победы (в центр) за Ленинградским мостом', 'город Челябинск пр.Победы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2170, 1391, 61.38783, 55.167087, 'Россия, Челябинск, улица Труда, 161',
        'город Челябинск пересечение ул. Труда  напротив д. 161 и Свердловского пр.',
        'город Челябинск ул. Труда   д. 161', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2171, 1392, 61.403263, 55.136164, 'Россия, Челябинск, улица Доватора, 1Г', 'город Челябинск ул. Доватора  1г',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2172, 1393, 61.442978, 55.153056, 'Россия, Челябинск, улица Рождественского',
        'город Челябинск пересечение шоссе Меридиан и ул. Рождественского (из центра)',
        'город Челябинск  шоссе Меридиан / ул. Рождественского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2173, 1394, 61.466693, 55.192548, 'Россия, Челябинск, улица Бажова, 23',
        'город Челябинск пересеч.пр.Победы и ул.Бажова  напротив д.23', 'город Челябинск ул.Бажова  напротив д.23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2174, 1395, 61.355976, 55.171051, 'Россия, Челябинск, улица Труда, 203',
        'город Челябинск ул. Труда напротив д. 203  ост. "Родничок"(в центр)', 'город Челябинск ул. Труда д. 203', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2184, 1396, 61.399706, 55.182937, 'Россия, Челябинск, улица Кирова, 1', 'город Челябинск ул.Кирова 1', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2187, 1397, 61.373772, 55.172254, 'Россия, Челябинск, улица Труда, 168', 'город Челябинск ул.Труда 168/1', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2188, 1398, 61.41027, 55.150972, 'Россия, Челябинск, улица Цвиллинга, 58', 'город Челябинск ул.Цвиллинга 58',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2189, 1399, 61.373834, 55.169375, 'Россия, Челябинск, улица Труда, 185',
        'город Челябинск ул.Труда 185-1 АЗС "Лукойл"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2190, 1400, 61.407827, 55.185605, 'Россия, Челябинск, улица Болейко, 7Б',
        'город Челябинск пр.Победы 158/ ул.Болейко 7б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2193, 1401, 61.4161, 55.171432, 'Россия, Челябинск, Российская улица, 67', 'город Челябинск ул.Российская  67',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2194, 1402, 61.454584, 55.174054, 'Россия, Челябинск, улица Комарова, 114', 'город Челябинск пр.Комарова 114',
        'город Челябинск  Комарова 114', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2198, 1403, 61.399185, 55.155119, 'Россия, Челябинск, улица Елькина, 63А', 'город Челябинск ул.Елькина 63а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2199, 1404, 61.384974, 55.157613, 'Россия, Челябинск, улица Сони Кривой, 49Б',
        'город Челябинск ул.С.Кривой 49б  поз.1', 'город Челябинск ул.С.Кривой 49б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2200, 1405, 61.384974, 55.157613, 'Россия, Челябинск, улица Сони Кривой, 49Б',
        'город Челябинск ул.С.Кривой 49б  поз.2', 'город Челябинск ул.С.Кривой 49б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2201, 1406, 61.399643, 55.181806, 'Россия, Челябинск, улица Кирова, 3', 'город Челябинск ул. Кирова 3', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2204, 1407, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск дорога в Аэропорт поворот на таможню',
        'город Челябинск дорога в Аэропорт', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2205, 1408, 61.301628, 55.171087, 'Россия, Челябинск, улица Братьев Кашириных, 137',
        'город Челябинск ул.Бр.Кашириных 137 поз.1', 'город Челябинск ул.Бр.Кашириных 137', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2206, 1409, 61.301628, 55.171087, 'Россия, Челябинск, улица Братьев Кашириных, 137',
        'город Челябинск ул.Бр.Кашириных 137 поз.2', 'город Челябинск ул.Бр.Кашириных 137', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2207, 1410, 61.439753, 55.161327, 'Россия, Челябинск, проспект Ленина, 20', 'город Челябинск пр.Ленина 20',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2209, 1411, 61.383842, 55.160689, 'Россия, Челябинск, проспект Ленина, 64', 'город Челябинск пр.Ленина 64',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2211, 1412, 61.383941, 55.15947, 'Россия, Челябинск, проспект Ленина, 71', 'город Челябинск пр.Ленина 71', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2213, 1413, 61.32458, 55.194979, 'Россия, Челябинск, Комсомольский проспект, 66',
        'город Челябинск Комсомольский пр.  66', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2214, 1414, 61.293651, 55.182444, 'Россия, Челябинск, проспект Победы, 325',
        'город Челябинск пр.Победы  325  газон у парковки ТК "Прииск"', 'город Челябинск пр.Победы  325 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2215, 1415, 61.423224, 55.140291, 'Россия, Челябинск, Гражданская улица, 10/1',
        'город Челябинск автодорога Меридиан  ул. Гражданская 10/1 (рынок Меридиан)',
        'город Челябинск  ул. Гражданская 10/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2217, 1416, 61.422388, 55.185209, 'Россия, Челябинск, проспект Победы, 131', 'город Челябинск пр.Победы 131',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2218, 1417, 61.394693, 55.160483, 'Россия, Челябинск, проспект Ленина, 62', 'город Челябинск пр.Ленина 62',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2220, 1418, 61.416046, 55.155458, 'Россия, Челябинск, улица Плеханова, 19', 'город Челябинск ул. Плеханова  19',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2221, 1419, 61.461573, 55.180547, 'Россия, Челябинск, улица Комарова, 125', 'город Челябинск пр. Комарова  125',
        'город Челябинск  Комарова  125', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2225, 1420, 61.455267, 55.114441, 'Россия, Челябинск, Новороссийская улица, 88',
        'город Челябинск ул.Новороссийская  88', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2226, 1421, 61.367205, 55.130003, 'Россия, Челябинск, улица Блюхера, 63', 'город Челябинск ул.Блюхера  63',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2227, 1422, 61.384911, 55.144788, 'Россия, Челябинск, улица Доватора, 27', 'город Челябинск ул. Доватора  27',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2228, 1423, 61.331964, 55.192528, 'Россия, Челябинск, улица Молодогвардейцев, 40',
        'город Челябинск ул. Молодогвардейцев  напротив д.40', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2229, 1424, 61.418301, 55.157593, 'Россия, Челябинск, Российская улица, 269',
        'город Челябинск ул. Российская  267А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2230, 1425, 61.329035, 55.194085, 'Россия, Челябинск, Комсомольский проспект, 47',
        'город Челябинск Комсомольский пр.  47', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2231, 1426, 61.313054, 55.178203, 'Россия, Челябинск, улица Братьев Кашириных, 108',
        'город Челябинск ул. Бр. Кашириных  108', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2232, 1427, 61.29507, 55.183127, 'Россия, Челябинск, проспект Победы, 323', 'город Челябинск пр. Победы  323',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2233, 1428, 61.363935, 55.191135, 'Россия, Челябинск, улица Чайковского, 15',
        'город Челябинск пересечение Комсомольского пр. и ул. Чайковского  15/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2236, 1429, 61.380141, 55.191808, 'Россия, Челябинск, Краснознамённая улица, 1А',
        'город Челябинск ул.Краснознаменная 1А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2237, 1430, 61.376844, 55.14203, 'Россия, Челябинск, улица Воровского, 73А',
        'город Челябинск ул. Воровского  73а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2238, 1431, 61.380877, 55.153457, 'Россия, Челябинск, улица Энгельса, 44Г', 'город Челябинск ул. Энгельса  44г',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2239, 1432, 61.3684, 55.156976, 'Россия, Челябинск, улица Сони Кривой, 83', 'город Челябинск ул. С. Кривой  83',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2240, 1433, 61.299086, 55.186397, 'Россия, Челябинск, проспект Победы, 382А',
        'город Челябинск ул. 40 лет Победы/пр. Победы  382а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2242, 1434, 61.390786, 55.151528, 'Россия, Челябинск, улица Воровского, 21',
        'город Челябинск ул. Воровского  21', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2243, 1435, 61.382458, 55.145915, 'Россия, Челябинск, улица Доватора, 35', 'город Челябинск ул. Доватора  35 ',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2244, 1436, 61.361177, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 95',
        'город Челябинск ул. Братьев Кашириных  95 ', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2245, 1437, 61.386249, 55.150108, 'Россия, Челябинск, улица Воровского, 32',
        'город Челябинск ул. Воровского  32', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2246, 1438, 61.305221, 55.181961, 'Россия, Челябинск, улица 40-летия Победы, 35',
        'город Челябинск ул. 40 лет Победы  35', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2247, 1439, 61.299885, 55.178198, 'Россия, Челябинск, улица 250-летия Челябинска, 17',
        'город Челябинск ул. 250 лет Челябинску  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2248, 1440, 61.383168, 55.146666, 'Россия, Челябинск, улица Воровского, 55',
        'город Челябинск ул. Воровского  55', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2249, 1441, 61.389582, 55.158215, 'Россия, Челябинск, Свердловский проспект, 65',
        'город Челябинск Свердловский пр. 65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2250, 1442, 61.342977, 55.17502, 'Россия, Челябинск, улица Университетская Набережная, 36',
        'город Челябинск ул. Университетская Набережная  36', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2251, 1443, 61.386456, 55.182264, 'Россия, Челябинск, Свердловский проспект, 28',
        'город Челябинск Свердловский пр.  напротив д. 28', 'город Челябинск Свердловский пр.  д. 28', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2252, 1444, 61.386878, 55.143029, 'Россия, Челябинск, улица Доватора, 23',
        'город Челябинск пересечение ул. Доватора  23 и ул.Сулимова', 'город Челябинск  ул. Доватора  23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2262, 1445, 0, 0,
        'Россия, Челябинская область, аэропорт Челябинск (Баландино) имени И.В. Курчатова, Челябинск (Баландино) Российский терминал',
        'город Челябинск дорога из аэропорта Баландино (380 м от аэропорта)', 'город Челябинск  аэропорт Баландино',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2263, 1446, 61.349355, 55.175072, 'Россия, Челябинск, улица Университетская Набережная, 30',
        'город Челябинск ул. Университетская Набережная  30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2264, 1447, 61.39561, 55.147809, 'Россия, Челябинск, улица Курчатова, 6', 'город Челябинск ул. Курчатова  6',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2265, 1448, 61.289456, 55.175889, 'Россия, Челябинск, улица Салавата Юлаева, 17В',
        'город Челябинск ул. Салавата Юлаева  17в', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2266, 1449, 61.356147, 55.225399, 'Россия, Челябинск, Радонежская улица, 12',
        'город Челябинск ул.Радонежская 12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2267, 1450, 61.314734, 55.177334, 'Россия, Челябинск, улица Братьев Кашириных, 129Д',
        'город Челябинск ул.Бр.Кашириных 129д', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2268, 1451, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52', 'город Челябинск ул.Чичерина  52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2269, 1452, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр. 1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2271, 1453, 61.462534, 55.158585, 'Россия, Челябинск, проспект Ленина, 3Ес1',
        'город Челябинск пр. Ленина  3е  стр. 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2273, 1454, 61.333563, 55.186643, 'Россия, Челябинск, улица Молодогвардейцев, 35Б',
        'город Челябинск ул. Молодогвардейцев 35б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2274, 1455, 61.496535, 55.123018, 'Россия, Челябинск, Копейское шоссе, 17',
        'город Челябинск Копейское шоссе  17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2275, 1456, 61.387282, 55.086517, 'Россия, Челябинск, Троицкий тракт, 25', 'город Челябинск Троицкий тракт  25',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2276, 1457, 61.444954, 55.217768, 'Россия, Челябинск, улица Героев Танкограда, 2',
        'город Челябинск ул. Героев Танкограда  2в', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2277, 1458, 61.353227, 55.105274, 'Россия, Челябинск, Новоэлеваторная улица, 49',
        'город Челябинск ул.Новоэлеваторная 49/9', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2278, 1459, 61.353227, 55.105274, 'Россия, Челябинск, Новоэлеваторная улица, 49',
        'город Челябинск ул.Новоэлеваторная 49/7', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2279, 1460, 61.341845, 55.178121, 'Россия, Челябинск, улица Братьев Кашириных, 103',
        'город Челябинск ул.Бр.Кашириных напротив д.103', 'город Челябинск ул.Бр.Кашириных д.103', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2282, 1461, 61.443418, 55.142437, 'Россия, Челябинск, Ленинский район, улица Гагарина, 7А',
        'город Челябинск ул.Гагарина 7а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2284, 1462, 61.443418, 55.142437, 'Россия, Челябинск, Ленинский район, улица Гагарина, 7А',
        'город Челябинск ул.Гагарина 7а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2286, 1463, 61.367915, 55.146749, 'Россия, Челябинск, улица Татьяничевой, 22/11',
        'город Челябинск ул.Татьяничевой напротив д.22/11', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2287, 1464, 61.370493, 55.146023, 'Россия, Челябинск, улица Образцова, 24',
        'город Челябинск ул.Образцова напротив д.24', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2288, 1465, 61.295483, 55.189264, 'Россия, Челябинск, улица 40-летия Победы, 15',
        'город Челябинск ул. 40 лет Победы  15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2289, 1466, 61.381856, 55.238691, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 31',
        'город Челябинск ул. 50 лет ВЛКСМ  31', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2290, 1467, 61.330994, 55.179344, 'Россия, Челябинск, улица Молодогвардейцев, 70А',
        'город Челябинск пересечение ул. Братьев Кашириных (из центра) и ул. Молодогвардейцев  70а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2292, 1468, 0, 0, '', 'город Челябинск пересечение Комсомольского пр. 2 и Свердловского пр.',
        'город Челябинск пересечение Комсомольского пр. 2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2293, 1469, 61.438477, 55.117046, 'Россия, Челябинск, Новороссийская улица, 122',
        'город Челябинск ул. Новороссийская  122', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2294, 1470, 61.296858, 55.185949, 'Россия, Челябинск, улица 40-летия Победы, 20',
        'город Челябинск ул. 40 лет Победы  напротив д. 22', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2295, 1471, 61.441594, 55.167328, 'Россия, Челябинск, улица Первой Пятилетки, 37',
        'город Челябинск ул. 1 Пятилетки  напротив д. 37', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2296, 1472, 61.386725, 55.149661, 'Россия, Челябинск, улица Воровского',
        'город Челябинск ул.Воровского  автопарковка обл.больницы  между опорами №11 и 13',
        'город Челябинск ул.Воровского ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2297, 1473, 61.293633, 55.172825, 'Россия, Челябинск, улица Салавата Юлаева, 27',
        'город Челябинск ул.С.Юлаева 27 у ТД"Одиссей"', 'город Челябинск ул.С.Юлаева 27', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2298, 1474, 61.296921, 55.191089, 'Россия, Челябинск, Комсомольский проспект, 101',
        'город Челябинск Комсомольский пр. 101', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2299, 1475, 61.439097, 55.167236, 'Россия, Челябинск, улица Первой Пятилетки, 41',
        'город Челябинск ул.1 Пятилетки между д.41 и 43', 'город Челябинск ул.1 Пятилетки д.41', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2302, 1476, 61.431596, 55.167909, 'Россия, Челябинск, улица Первой Пятилетки, 57',
        'город Челябинск ул.1 Пятилетки между д.57 и 55', 'город Челябинск ул.1 Пятилетки д.57', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2305, 1477, 61.382386, 55.239276, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 27',
        'город Челябинск ул.50 лет ВЛКСМ 27', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2306, 1478, 61.428985, 55.178059, 'Россия, Челябинск, Артиллерийская улица',
        'город Челябинск пересеч.ул.Артиллерийской и ул.Бажова', 'город Челябинск ул.Артиллерийской / ул.Бажова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2309, 1479, 61.375721, 55.145545, 'Россия, Челябинск, улица Образцова, 7', 'город Челябинск ул. Образцова 7',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2310, 1480, 61.385593, 55.104213, 'Россия, Челябинск, Троицкий тракт, 15',
        'город Челябинск ул.1-я Потребительская/ Троицкий тр. 15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2312, 1481, 61.372622, 55.102266, 'Россия, Челябинск, 1-я Потребительская улица, 1А',
        'город Челябинск ул.1-я Потребительская 1  напротив фабрики "Линда"',
        'город Челябинск ул.1-я Потребительская 1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2313, 1482, 61.363648, 55.150345, 'Россия, Челябинск, Лесопарковая улица, 6',
        'город Челябинск ул.Лесопарковая 6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2314, 1483, 61.378209, 55.241976, 'Россия, Челябинск, улица Комаровского, 2',
        'город Челябинск ул.Черкасская/ ул.Комаровского 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2316, 1484, 61.484722, 55.107061, 'Россия, Челябинск, Новороссийская улица, 39',
        'город Челябинск ул.Новороссийская 39', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2317, 1485, 61.483609, 55.107339, 'Россия, Челябинск, Новороссийская улица, 43',
        'город Челябинск ул.Новороссийская  43', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2318, 1486, 61.435549, 55.121258, 'Россия, Челябинск, Ленинский район, улица Гагарина, 47',
        'город Челябинск ул.Гагарина 47', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2320, 1487, 61.378748, 55.247231, 'Россия, Челябинск, шоссе Металлургов, 70',
        'город Челябинск пересечение шоссе Металлургов  70/1 и ул.Черкасская',
        'город Челябинск пересечение шоссе Металлургов  70/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2321, 1488, 61.379359, 55.258325, 'Россия, Челябинск, улица Румянцева',
        'город Челябинск пересечение ул.Богдана Хмельницкого и ул.Румянцева (в центр)',
        'город Челябинск ул.Богдана Хмельницкого / ул.Румянцева (в центр)', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2322, 1489, 61.403434, 55.257237, 'Россия, Челябинск, улица Сталеваров, 17', 'город Челябинск ул.Сталеваров 17',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2323, 1490, 61.423799, 55.176105, 'Россия, Челябинск, улица Либединского, 47',
        'город Челябинск автодорога Меридиан  напротив д.47 по ул.Либединского', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2324, 1491, 61.356024, 55.208311, 'Россия, Челябинск, Автодорожная улица',
        'город Челябинск пересечение ул.Автодорожная и ул.Производственная',
        'город Челябинск ул.Автодорожная / ул.Производственная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2325, 1492, 61.365938, 55.158904, 'Россия, Челябинск, проспект Ленина, 87', 'город Челябинск пр.Ленина 87',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2327, 1493, 61.302418, 55.215426, 'Россия, Челябинск, улица Профессора Благих, 61',
        'город Челябинск ул. Профессора Благих  напротив домов 61 и 63', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2328, 1494, 61.329197, 55.189491, 'Россия, Челябинск, проспект Победы, 330', 'город Челябинск пр.Победы 330',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2329, 1495, 61.363378, 55.119425, 'Россия, Челябинск, улица Блюхера, 95', 'город Челябинск ул.Блюхера 95', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2330, 1496, 0, 0, '', 'город Челябинск Свердловский тр.  у АЗС "Лукойл" в районе рынка "Орбита"',
        'город Челябинск рынок "Орбита"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2331, 1497, 61.371723, 55.133447, 'Россия, Челябинск, улица Блюхера, 49', 'город Челябинск ул.Блюхера 49', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2332, 1498, 61.338495, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 105',
        'город Челябинск ул.Бр.Кашириных 105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2333, 1499, 61.427653, 55.13268, 'Россия, Челябинск, улица Дзержинского, 107',
        'город Челябинск ул.Дзержинского 107', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2334, 1500, 61.468445, 55.194758, 'Россия, Челябинск, улица Бажова, 11',
        'город Челябинск пересеч.ул.Бажова 11 и ул. Загорская', 'город Челябинск ул.Бажова 11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2335, 1501, 61.451979, 55.145442, 'Россия, Челябинск, Копейское шоссе, 64',
        'город Челябинск Копейское шоссе 64', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2336, 1502, 61.451979, 55.145442, 'Россия, Челябинск, Копейское шоссе, 64',
        'город Челябинск Копейское шоссе 64', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2337, 1503, 61.401574, 55.163775, 'Россия, Челябинск, улица Кирова, 161', 'город Челябинск ул. Кирова  161',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2338, 1504, 61.467744, 55.134507, 'Россия, Челябинск, Копейское шоссе, 44',
        'город Челябинск Копейское шоссе  44', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2339, 1505, 61.319639, 55.193479, 'Россия, Челябинск, Комсомольский проспект, 65',
        'город Челябинск Комсомольский пр.  65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2340, 1506, 61.433222, 55.127476, 'Россия, Челябинск, Ленинский район, улица Гагарина, 42',
        'город Челябинск ул. Гагарина  42', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2342, 1507, 61.454584, 55.174054, 'Россия, Челябинск, улица Комарова, 114', 'город Челябинск ул.Комарова  114',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2343, 1508, 61.373906, 55.17665, 'Россия, Челябинск, улица Братьев Кашириных, 79',
        'город Челябинск ул. Бр. Кашириных  79', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2344, 1509, 61.293651, 55.182444, 'Россия, Челябинск, проспект Победы, 325',
        'город Челябинск ул. Чичерина/ пр. Победы  325', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2346, 1510, 61.453263, 55.139977, 'Россия, Челябинск, Копейское шоссе, 39',
        'город Челябинск Копейское шоссе  39', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2348, 1511, 61.415238, 55.056445, 'Россия, Челябинск, коттеджный посёлок Смолино',
        'город Челябинск Троицкий тр. (в город)  поворот в пос. Смолино', 'город Челябинск пос. Смолино', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2350, 1512, 61.487804, 55.179221, 'Россия, Челябинск, улица Хохрякова, 20', 'город Челябинск ул. Хохрякова  20',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2352, 1513, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Курганское шоссе (из города)  после поворота на ТЭЦ-3', 'город Челябинск Курганское шоссе',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2361, 1514, 61.411671, 55.158781, 'Россия, Челябинск, улица Свободы, 74', 'город Челябинск ул. Свободы  74',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2362, 1515, 61.40276, 55.251778, 'Россия, Челябинск, улица Сталеваров, 66',
        'город Челябинск шоссе Металлургов/ ул. Сталеваров  66/3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2364, 1516, 61.378281, 55.258674, 'Россия, Челябинск, улица Богдана Хмельницкого, 28',
        'город Челябинск ул. Богдана Хмельницкого  напротив д. 28', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2365, 1517, 61.379009, 55.251511, 'Россия, Челябинск, улица Румянцева, 28Б',
        'город Челябинск ул. Румянцева  28б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2366, 1518, 61.444649, 55.146193, 'Россия, Челябинск, Ленинский район, улица Гагарина, 2',
        'город Челябинск ул. Гагарина  напротив д. 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2368, 1519, 61.395493, 55.158261, 'Россия, Челябинск, улица Сони Кривой, 26',
        'город Челябинск ул.Сони Кривой  напротив д.26 поз.1', 'город Челябинск ул.Сони Кривой   д.26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2369, 1520, 61.395493, 55.158261, 'Россия, Челябинск, улица Сони Кривой, 26',
        'город Челябинск ул.Сони Кривой  напротив д.26 поз.2', 'город Челябинск ул.Сони Кривой   д.26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2370, 1521, 61.395493, 55.158261, 'Россия, Челябинск, улица Сони Кривой, 26',
        'город Челябинск ул.Сони Кривой  напротив д.26 поз.3', 'город Челябинск ул.Сони Кривой   д.26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2371, 1522, 61.395493, 55.158261, 'Россия, Челябинск, улица Сони Кривой, 26',
        'город Челябинск ул.Сони Кривой  напротив д.26 поз.4', 'город Челябинск ул.Сони Кривой   д.26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2372, 1523, 61.394693, 55.160483, 'Россия, Челябинск, проспект Ленина, 62',
        'город Челябинск пересеч. пр.Ленина 62 и ул.Васенко', 'город Челябинск пересеч. пр.Ленина 62', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2373, 1524, 61.365417, 55.155612, 'Россия, Челябинск, Лесопарковая улица, 7',
        'город Челябинск ул.Лесопарковая 7 (поз.5)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2374, 1525, 61.456533, 55.138789, 'Россия, Челябинск, Копейское шоссе, 37Б',
        'город Челябинск Копейское шоссе  37б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2375, 1526, 61.465714, 55.158796, 'Россия, Челябинск, проспект Ленина, 3к1', 'город Челябинск пр.Ленина 3/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2377, 1527, 61.310809, 55.194959, 'Россия, Челябинск, Комсомольский проспект, 78',
        'город Челябинск Комсомольский пр.  78', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2378, 1528, 61.395089, 55.157423, 'Россия, Челябинск, улица Воровского, 6', 'город Челябинск ул. Воровского 6',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2379, 1529, 61.363477, 55.158174, 'Россия, Челябинск, проспект Ленина, 89', 'город Челябинск пр.Ленина 89',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2381, 1530, 61.365363, 55.153118, 'Россия, Челябинск, Лесопарковая улица, 9А',
        'город Челябинск ул.Лесопарковая 9а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2382, 1531, 61.383851, 55.033615, 'Россия, Челябинск, посёлок Новосинеглазово, Южная улица',
        'город Челябинск Свердловский  пр.  ост. "ул. Южная" (в центр)', 'город Челябинск ост. "ул. Южная"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2383, 1532, 61.451503, 55.171319, 'Россия, Челябинск, Салютная улица, 11', 'город Челябинск ул. Салютная 11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2384, 1533, 61.370529, 55.125716, 'Россия, Челябинск, улица Дарвина, 18К1', 'город Челябинск ул. Дарвина 18/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2385, 1534, 61.356024, 55.208311, 'Россия, Челябинск, Автодорожная улица',
        'город Челябинск пересеч.ул.Автодорожная (из центра) и ул. Производственная (100 м до пересечения)',
        'город Челябинск ул.Автодорожная / ул. Производственная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2386, 1535, 61.431039, 55.150844, 'Россия, Челябинск, Сибирский переезд, 111Ас1',
        'город Челябинск Сибирский переезд  111а/1 в сторону Ленинского района',
        'город Челябинск Сибирский переезд  111а/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2387, 1536, 61.39746, 55.197471, 'Россия, Челябинск, Кожзаводская улица, 106А',
        'город Челябинск ул.Кожзаводская  106а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2388, 1537, 61.428281, 55.16812, 'Россия, Челябинск, улица Труда',
        'город Челябинск пересечение а/д Меридиан и ул. Труда  в сторону центра',
        'город Челябинск автодорога Меридиан / ул. Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2389, 1538, 61.436474, 55.159491, 'Россия, Челябинск, Артиллерийский переулок, 4',
        'город Челябинск а/д Меридиан  напротив дома №4 по пер. Артиллерийский',
        'город Челябинск пер. Артиллерийский дома №4', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2390, 1539, 61.404649, 55.129466, 'Россия, Челябинск, Железнодорожная улица',
        'город Челябинск пересечение ул.Железнодорожной и ул.Самовольной в сторону Железнодорожного вокзала',
        'город Челябинск ул.Железнодорожной / ул.Самовольной ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2391, 1540, 61.404649, 55.129466, 'Россия, Челябинск, Железнодорожная улица',
        'город Челябинск пересечение ул.Железнодорожной и ул.Самовольной в сторону Троицкого тракта',
        'город Челябинск ул.Железнодорожной / ул.Самовольной ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2392, 1541, 61.435917, 55.150756, 'Россия, Челябинск, автодорога Меридиан, 1/2',
        'город Челябинск а/д Меридиан  напротив здания Меридиан автодорога  1/2',
        'город Челябинск автодорога Меридиан   1/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2394, 1542, 61.37069, 55.208358, 'Россия, Челябинск, 2-й Западный проезд',
        'город Челябинск въезд в пос. Западный', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2397, 1543, 61.341208, 55.19001, 'Россия, Челябинск, проспект Победы, 312', 'город Челябинск пр.Победы  312',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2398, 1544, 61.37794, 55.225039, 'Россия, Челябинск, Свердловский тракт, 9А',
        'город Челябинск Свердловский тр.  9а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2399, 1545, 61.362682, 55.207986, 'Россия, Челябинск, Автодорожная улица',
        'город Челябинск ул.Автодорожная (в центр)  после пересечения с ул.Мастеровая',
        'город Челябинск ул.Автодорожная / ул.Мастеровая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2400, 1546, 61.387471, 55.088898, 'Россия, Челябинск, Троицкий тракт, 23', 'город Челябинск Троицкий тр.  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2401, 1547, 61.384758, 55.247457, 'Россия, Челябинск, шоссе Металлургов, 31',
        'город Челябинск шоссе Металлургов  31', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2402, 1548, 61.434947, 55.12517, 'Россия, Челябинск, Ленинский район, улица Гагарина, 39',
        'город Челябинск ул.Гагарина  39 (сторона А)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2404, 1549, 61.434947, 55.12517, 'Россия, Челябинск, Ленинский район, улица Гагарина, 39',
        'город Челябинск ул.Гагарина  39 (сторона Б со стороны ул. Гончаренко)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2406, 1550, 61.451979, 55.145442, 'Россия, Челябинск, Копейское шоссе, 64',
        'город Челябинск Копейское шоссе  64  поз. 1', 'город Челябинск Копейское шоссе  64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2407, 1551, 61.451979, 55.145442, 'Россия, Челябинск, Копейское шоссе, 64',
        'город Челябинск Копейское шоссе  64  поз. 2', 'город Челябинск Копейское шоссе  64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2408, 1552, 61.451979, 55.145442, 'Россия, Челябинск, Копейское шоссе, 64',
        'город Челябинск Копейское шоссе  64  поз. 3', 'город Челябинск Копейское шоссе  64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2409, 1553, 61.383455, 55.14787, 'Россия, Челябинск, улица Воровского, 38Б',
        'город Челябинск ул.Воровского 38б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2410, 1554, 61.431345, 55.161908, 'Россия, Челябинск, проспект Ленина, 26А',
        'город Челябинск пр. Ленина  26а  ост. "ТРК Горки" (троллейбус  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2412, 1555, 61.415265, 55.149573, 'Россия, Челябинск, улица Свободы, 98',
        'город Челябинск ул. Свободы  98  ост. "ул. Евтеева" (троллейбус  в сторону ж/д вокзала)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2416, 1556, 61.338495, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 105',
        'город Челябинск ул. Братьев Кашириных  напротив д. 105  ост. "Каширинский рынок"',
        'город Челябинск ул. Братьев Кашириных д. 105', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2424, 1557, 61.331964, 55.183575, 'Россия, Челябинск, улица Молодогвардейцев, 62',
        'город Челябинск ул. Молодогвардейцев  62  ост. "Педучилище" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2425, 1558, 61.423601, 55.185225, 'Россия, Челябинск, проспект Победы, 129',
        'город Челябинск пр. Победы  129  ост. "Юридический институт" (трамвай  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2426, 1559, 61.374562, 55.170784, 'Россия, Челябинск, улица Труда, 166',
        'город Челябинск ул. Труда  166  ост. "Молния" (троллейбус  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2427, 1560, 61.381991, 55.118972, 'Россия, Челябинск, улица Дарвина, 35',
        'город Челябинск ул. Дарвина  35  ост. "ЦРМ" (автобус  в сторону Троицкого тр.)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2428, 1561, 61.435396, 55.182058, 'Россия, Челябинск, улица Горького, 60',
        'город Челябинск ул. Горького  60  ост. "ул. Лермонтова" (трамвай  в сторону С/З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2430, 1562, 61.294127, 55.166547, 'Россия, Челябинск, улица Братьев Кашириных, 152',
        'город Челябинск ул. Братьев Кашириных  152  ост. "ул. Академика Макеева" (автобус  в сторону плотины)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2432, 1563, 61.387938, 55.10736, 'Россия, Челябинск, Троицкий тракт, 54Б',
        'город Челябинск Троицкий тр.  54б  ост. "ул. Потребительская" (автобус  в сторону ул. Блюхера)',
        'город Челябинск Троицкий тр.  54б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2433, 1564, 61.456965, 55.171766, 'Россия, Челябинск, Салютная улица, 2',
        'город Челябинск ул. Героев Танкограда/ул. Салютная  2  ост. "Отделочные материалы" (автобус  в сторону Аэропорта)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2436, 1565, 61.444065, 55.169663, 'Россия, Челябинск, Салютная улица, 27',
        'город Челябинск ул. Горького/ул. Салютная  27  ост. "Башня" (трамвай  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2438, 1566, 61.375532, 55.229085, 'Россия, Челябинск, Черкасская улица, 17',
        'город Челябинск ул. Черкасская  17  ост. "ул. Черкасская" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2439, 1567, 61.398493, 55.184901, 'Россия, Челябинск, проспект Победы, 168',
        'город Челябинск пр. Победы  168  ост. "Теплотехнический институт" (трамвай  в сторону С-З  центра)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2440, 1568, 61.325191, 55.202327, 'Россия, Челябинск, улица Ворошилова, 2к1',
        'город Челябинск ул. Ворошилова  2к 1  ост. "Ткацкая фабрика" (автобус  в центр)',
        'город Челябинск ул. Ворошилова  2к 1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2441, 1569, 61.331901, 55.193885, 'Россия, Челябинск, улица Молодогвардейцев, 34',
        'город Челябинск ул. Молодогвардейцев  34  ост. "ул. Молодогвардейцев" (маршрутное такси  в центр)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2442, 1570, 61.451943, 55.160648, 'Россия, Челябинск, проспект Ленина, 8А',
        'город Челябинск пр. Ленина  8а  ост. "Театр ЧТЗ" (трамвай  в центр)', 'город Челябинск пр. Ленина  8а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2443, 1571, 61.377482, 55.231411, 'Россия, Челябинск, Черкасская улица, 15/2',
        'город Челябинск ул. Черкасская  15/2  ост. "Авторынок" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2444, 1572, 61.378748, 55.247231, 'Россия, Челябинск, шоссе Металлургов, 70',
        'город Челябинск ш. Металлургов  70/1  ост. "ДК Строителей" (троллейбус  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2446, 1573, 61.323331, 55.189491, 'Россия, Челябинск, проспект Победы, 346',
        'город Челябинск пр. Победы  346  ост. "ул. Ворошилова" (трамвай  в сторону ул. Чичерина)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2448, 1574, 61.380347, 55.258089, 'Россия, Челябинск, улица Богдана Хмельницкого, 35',
        'город Челябинск ул. Б. Хмельницкого  35  ост "ул. Б. Хмельницкого (троллейбус  в сторну ул. Сталеваров)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2449, 1575, 61.461241, 55.170691, 'Россия, Челябинск, улица Марченко, 18',
        'город Челябинск ул. Марченко  18  сот. "Магазин № 28" (автобус  в сторону пр. Комарова)',
        'город Челябинск ул. Марченко  18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2450, 1576, 61.441657, 55.139571, 'Россия, Челябинск, Ленинский район, улица Гагарина, 15',
        'город Челябинск ул. Гагарина  15  ост. "Дом одежды" (троллейбус  в сторону Копейского ш.)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2453, 1577, 61.457117, 55.114157, 'Россия, Челябинск, Новороссийская улица, 86',
        'город Челябинск ул. Новороссийская  86  ост. "ДК ЧТПЗ" (троллейбус  в сторону ул. Гагарина)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2454, 1578, 61.441019, 55.159388, 'Россия, Челябинск, проспект Ленина, 13А',
        'город Челябинск пр. Ленина  13а  ост. "Комсомольская пл." (троллейбус  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2456, 1579, 61.467798, 55.111887, 'Россия, Челябинск, улица Машиностроителей, 37',
        'город Челябинск ул. Машиностроителей  37  ост. "Школа" (маршрутное такси  в сторону Копейского ш.)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2457, 1580, 61.40276, 55.251778, 'Россия, Челябинск, улица Сталеваров, 66',
        'город Челябинск ул. Сталеваров  66  ост. "ул. Сталеваров" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2458, 1581, 61.411348, 55.26423, 'Россия, Челябинск, 2-я Павелецкая улица, 18/1',
        'город Челябинск ул. Павелецкая  2-я  напротив 18/1  ост. "ЧМК" (троллейбус  в центр)',
        'город Челябинск ул. Павелецкая  2-я  18/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2459, 1582, 61.465714, 55.158796, 'Россия, Челябинск, проспект Ленина, 3к1',
        'город Челябинск пр. Ленина напротив д. 3/1  ост. "ЧТЗ" (трамвай  конечная)',
        'город Челябинск пр. Ленина д. 3/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2460, 1583, 61.413253, 55.155335, 'Россия, Челябинск, улица Свободы, 84',
        'город Челябинск ул. Свободы  84  ост. "ул. Плеханова" (троллейбус  в сторону ж/д вокзала)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2462, 1584, 61.434255, 55.160597, 'Россия, Челябинск, проспект Ленина, 19',
        'город Челябинск пр. Ленина  19  ост. "ТРК Горки"  (троллейбус  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2464, 1585, 61.298097, 55.17174, 'Россия, Челябинск, улица Братьев Кашириных, 134',
        'город Челябинск ул. Братьев Кашириных  134  ост. "Универсам Казачий" (маршрутное такси  в сторону плотины)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2465, 1586, 61.32414, 55.188411, 'Россия, Челябинск, проспект Победы, 293',
        'город Челябинск пр. Победы  293  ост. "ул. Ворошилова" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2467, 1587, 61.373125, 55.040647, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 3',
        'город Челябинск ул. Кирова  9  ост. "ул. Калинина" (трамвай  в сторону Теплотехн. Института"',
        'город Челябинск ул. Кирова  9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2470, 1588, 61.333518, 55.179483, 'Россия, Челябинск, улица Молодогвардейцев, 57к1',
        'город Челябинск ул. Молодогвардейцев  57к1  ост. "ул. Братьев Кашириных" (троллейбус  в сторону пр. Победы)',
        'город Челябинск ул. Молодогвардейцев  57к1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2473, 1589, 61.369945, 55.170074, 'Россия, Челябинск, улица Труда, 189',
        'город Челябинск ул. Труда  напротив д. 189  ост. "Зоопарк" (троллейбус  в сторону С-З)',
        'город Челябинск ул. Труда  д. 189', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2474, 1590, 61.37714, 55.121124, 'Россия, Челябинск, улица Дарвина, 63',
        'город Челябинск ул. Дарвина  63  ост. "Кинотеатр Маяк" (автобус  в сторону Троицкого тр.)',
        'город Челябинск ул. Дарвина  63', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2475, 1591, 61.424733, 55.185944, 'Россия, Челябинск, проспект Победы, 140',
        'город Челябинск пр. Победы  140  ост. "Юридический институт" (трамвай  в сторону Теплотехн. Института)',
        'город Челябинск пр. Победы  140', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2476, 1592, 61.434147, 55.181837, 'Россия, Челябинск, улица Горького, 67',
        'город Челябинск ул. Горького  67  ост. "ул. Лермонтова" (трамвай  в сторону ЧТЗ)',
        'город Челябинск ул. Горького  67', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2478, 1593, 61.388306, 55.11113, 'Россия, Челябинск, Троицкий тракт, 52А',
        'город Челябинск Троицкий тр.  52а  ост. "Рынок Привоз" (автобус  в сторону ул. Блюхера)',
        'город Челябинск Троицкий тр.  52а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2479, 1594, 61.365363, 55.153118, 'Россия, Челябинск, Лесопарковая улица, 9А',
        'город Челябинск ул. Лесопарковая  9а  ост. "Профилакторий" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2480, 1595, 61.477868, 55.127399, 'Россия, Челябинск, улица Машиностроителей, 2',
        'город Челябинск ул. Машиностроителей  2  ост. "Мех. Завод"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2481, 1596, 61.435845, 55.179395, 'Россия, Челябинск, улица Горького, 59',
        'город Челябинск ул. Горького  59  ост. "ДК Смена" (трамвай  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2482, 1597, 61.402419, 55.160818, 'Россия, Челябинск, проспект Ленина, 54',
        'город Челябинск пр. Ленина  54  ост. "пл. Революции" (троллейбус  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2485, 1598, 61.380958, 55.237587, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 35',
        'город Челябинск ул. 50 лет ВЛКСМ  35  ост. "п. Першино" (трамвай  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2486, 1599, 61.378578, 55.241421, 'Россия, Челябинск, Черкасская улица, 4',
        'город Челябинск ул. Черкасская  4  ост. "ДЦ Импульс" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2487, 1600, 61.376431, 55.245522, 'Россия, Челябинск, шоссе Металлургов, 41',
        'город Челябинск ш. Металлургов  41  ост. "ДК Строителей" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2490, 1601, 61.471948, 55.118421, 'Россия, Челябинск, улица Машиностроителей, 21А',
        'город Челябинск ул. Машиностроителей  21а/1  ост. "Трубопрокатный завод" (троллейбус  в сторону Копейского ш.)',
        'город Челябинск ул. Машиностроителей  21а/1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2491, 1602, 61.390489, 55.259418, 'Россия, Челябинск, улица Богдана Хмельницкого, 14А',
        'город Челябинск ул. Б. Хмельницкого  14а  ост. "Кинотеатр Россия" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2492, 1603, 61.402428, 55.260608, 'Россия, Челябинск, улица Сталеваров, 32',
        'город Челябинск ул. Сталеваров  32  ост. "Сквер Победы" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2494, 1604, 61.383842, 55.160689, 'Россия, Челябинск, проспект Ленина, 64',
        'город Челябинск пр. Ленина  64д  ост. "Алое поле" (трамвай  в сторону АМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2496, 1605, 61.385953, 55.183225, 'Россия, Челябинск, Свердловский проспект, 28А',
        'город Челябинск Свердловский пр.  28а  ост. "пр. Победы" (троллейбус  в центр)',
        'город Челябинск Свердловский пр.  28а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2497, 1606, 61.471095, 55.121165, 'Россия, Челябинск, улица Дзержинского, 4',
        'город Челябинск ул. Дзержинского  4  ост. "ЗЭМ" (трамвай в сторону ул. Гагарина)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2499, 1607, 61.462193, 55.169735, 'Россия, Челябинск, улица Марченко, 15',
        'город Челябинск ул. Марченко  напротив д. 15  ост. "Магазин № 28" (автобус  в центр)',
        'город Челябинск ул. Марченко  д. 15', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2502, 1608, 61.466361, 55.112123, 'Россия, Челябинск, Новороссийская улица, 64',
        'город Челябинск ул. Новороссийская  64  ост. "Школа" (троллейбус  в сторону ул. Гагарина)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2503, 1609, 61.403838, 55.26186, 'Россия, Челябинск, улица Сталеваров, 7',
        'город Челябинск ул. Сталеваров  7  ост. "Сквер Победы" (автобус  в сторону ЧМК)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2505, 1610, 61.445143, 55.144222, 'Россия, Челябинск, Ленинский район, улица Гагарина, 3',
        'город Челябинск ул. Гагарина  3  ост. "Политехникум" (троллейбус  в сторону Копейского ш.)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2507, 1611, 61.407755, 55.154645, 'Россия, Челябинск, улица Цвиллинга, 43',
        'город Челябинск ул. Цвиллинга  43  ост. "Городской сад" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2510, 1612, 61.363477, 55.158174, 'Россия, Челябинск, проспект Ленина, 89',
        'город Челябинск пр. Ленина  89  ост. "ПКиО им. Гагарина" (троллейбус  конечная)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2513, 1613, 61.41354, 55.184793, 'Россия, Челябинск, Российская улица, 32',
        'город Челябинск пр. Победы/ул. Российская  32  ост. "ул. Российская" (трамвай  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2516, 1614, 61.398951, 55.17226, 'Россия, Челябинск, улица Кирова, 74',
        'город Челябинск ул. Кирова  74  ост. "Цирк" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2517, 1615, 61.354727, 55.177828, 'Россия, Челябинск, улица Чайковского, 183',
        'город Челябинск ул. Братьев Кашириных/ул. Чайковского  183  ост. "ул. Чайковского" (троллейбус  в центр)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2519, 1616, 61.355976, 55.171051, 'Россия, Челябинск, улица Труда, 203',
        'город Челябинск ул. Труда  203  ост. "ТРК Родник" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2520, 1617, 61.386959, 55.098042, 'Россия, Челябинск, Троицкий тракт, 35',
        'город Челябинск Троицкий тр.  напротив д. 35  ост. "Сад Локомотив" (автобус  в сторону п. Смолино)',
        'город Челябинск Троицкий тр.  д. 35', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2521, 1618, 61.338432, 55.194147, 'Россия, Челябинск, Комсомольский проспект, 41',
        'город Челябинск Комсомольский пр.  41  ост. "Поликлиника" (маршрутное такси  в центр)',
        'город Челябинск Комсомольский пр.  41', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2523, 1619, 61.443607, 55.183986, 'Россия, Челябинск, улица Героев Танкограда, 50',
        'город Челябинск ул. Героев Танкограда  50  ост. "ул. Потемкина" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2524, 1620, 61.445053, 55.16882, 'Россия, Челябинск, улица Горького, 18',
        'город Челябинск ул. Горького  напротив д. 18  ост. "Монтажный колледж" (трамвай  в сторону ЧТЗ)',
        'город Челябинск ул. Горького  д. 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2525, 1621, 61.423592, 55.185903, 'Россия, Челябинск, проспект Победы, 142',
        'город Челябинск пр. Победы  142  ост. "Юридический институт"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2526, 1622, 61.4813, 55.194362, 'Россия, Челябинск, улица Мамина, 15',
        'город Челябинск ул. Мамина  15  ост. "Универсам" (автобус  в сторону ул. Бажова)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2527, 1623, 61.452949, 55.172697, 'Россия, Челябинск, Салютная улица, 10',
        'город Челябинск ул. Салютная  10  ост. "Сад Победы" (троллейбус  в сторону ЖБИ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2530, 1624, 61.36672, 55.129026, 'Россия, Челябинск, улица Блюхера, 67',
        'город Челябинск ул. Блюхера  напротив д. 67  ост. "ТК Кольцо" (троллейбус  в сторону АМЗ)',
        'город Челябинск ул. Блюхера  д. 67', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2539, 1625, 61.394693, 55.160483, 'Россия, Челябинск, проспект Ленина, 62',
        'город Челябинск пр. Ленина  62  ост. "Публичная библиотека" (маршрутное такси  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2541, 1626, 61.413235, 55.147068, 'Россия, Челябинск, улица Цвиллинга, 77',
        'город Челябинск ул. Цвиллинга  77  ост. "стадион Локомотив" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2542, 1627, 61.303335, 55.193294, 'Россия, Челябинск, Молдавская улица, 11',
        'город Челябинск ул. Молдавская  11  ост. "ул. Молдавская" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2543, 1628, 61.287668, 55.186921, 'Россия, Челябинск, Комсомольский проспект, 111',
        'город Челябинск Комсомольский пр.  напротив д. 111  ост. "18-й микрорайон" (автобус  в сторону пр. Победы)',
        'город Челябинск Комсомольский пр.  д. 111', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2544, 1629, 61.302302, 55.188242, 'Россия, Челябинск, проспект Победы, 378',
        'город Челябинск пр. Победы  напротив д. 378  ост. "Поликлиника" (трамвай  в центр)',
        'город Челябинск пр. Победы  д. 378', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2545, 1630, 61.410926, 55.137986, 'Россия, Челябинск, улица Степана Разина, 9А',
        'город Челябинск ул. С. Разина  9а  ост. "Завод им. Колющенко" (трамвай № 1  в центр)',
        'город Челябинск ул. С. Разина  9а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2546, 1631, 61.331919, 55.195077, 'Россия, Челябинск, улица Молодогвардейцев, 32',
        'город Челябинск ул. Молодогвардейцев  32  ост. "ул. Молодогвардейцев" (маршрутное такси  в сторону ул. Чичерина)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2547, 1632, 61.377958, 55.237838, 'Россия, Челябинск, Черкасская улица, 10',
        'город Челябинск ул. Черкасская  10  ост. "п. Першино" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2548, 1633, 61.380958, 55.237587, 'Россия, Челябинск, улица 50-летия ВЛКСМ, 35',
        'город Челябинск ул. 50 лет ВЛКСМ  35  ост. "п. Першино" (троллейбус  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2549, 1634, 61.46955, 55.11717, 'Россия, Челябинск, улица Машиностроителей, 34',
        'город Челябинск ул. Машиностроителей  34  ост. "Трубопрокатный завод" (трамвай  в сторону ул. Новороссийской)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2550, 1635, 61.446975, 55.151487, 'Россия, Челябинск, проспект Ленина, 3',
        'город Челябинск пр. Ленина  напротив д. 3  ост. "ЧТЗ" (троллейбус  в центр)',
        'город Челябинск пр. Ленина   д. 3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2551, 1636, 61.457827, 55.1139, 'Россия, Челябинск, Новороссийская улица, 84',
        'город Челябинск ул. Новороссийская  напротив д. 84  ост. "ДК ЧТПЗ" (троллейбус  в сторону ул. Машиностроителей)',
        'город Челябинск ул. Новороссийская   д. 84', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2552, 1637, 61.441136, 55.161707, 'Россия, Челябинск, улица Горького, 1Б',
        'город Челябинск ул. Горького  1б  ост. "Комсомольская пл." (трамвай  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2553, 1638, 61.439663, 55.138989, 'Россия, Челябинск, Ленинский район, улица Гагарина, 16А',
        'город Челябинск ул. Гагарина  16а  ост. "Дом одежды" (троллейбус  в сторону ул. Новороссийской)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2556, 1639, 61.308787, 55.189383, 'Россия, Челябинск, проспект Победы, 372',
        'город Челябинск пр. Победы  напротив д. 372  ост. "ул. Молдавская" (трамвай  в центр)',
        'город Челябинск пр. Победы  д. 372', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2557, 1640, 61.41018, 55.152207, 'Россия, Челябинск, улица Цвиллинга, 55А',
        'город Челябинск ул. Цвиллинга  55а  ост. "ул. Орджоникидзе" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2559, 1641, 61.355976, 55.171051, 'Россия, Челябинск, улица Труда, 203', 'город Челябинск ул.Труда 203', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2561, 1642, 61.403766, 55.251516, 'Россия, Челябинск, улица Сталеваров, 37',
        'город Челябинск ул. Сталеваров  37/1  ост. "ул. Сталеваров" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2570, 1643, 61.402419, 55.160818, 'Россия, Челябинск, проспект Ленина, 54',
        'город Челябинск пр. Ленина  54  ост. "пл. Революции" (автобус  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2573, 1644, 61.371445, 55.148205, 'Россия, Челябинск, улица Худякова, 12',
        'город Челябинск ул. Худякова  12  ост. "ТК Калибр" (автобус  в сторону плотины)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2574, 1645, 61.370376, 55.1772, 'Россия, Челябинск, улица Братьев Кашириных, 85А',
        'город Челябинск ул. Братьев Кашириных  83  ост. "ул. Косарева" (автобус  в сторону Свердловского пр.)',
        'город Челябинск ул. Братьев Кашириных  83 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2575, 1646, 61.399149, 55.174182, 'Россия, Челябинск, улица Кирова, 60А',
        'город Челябинск ул. Кирова  60а  ост. "Цирк" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2576, 1647, 61.421769, 55.16867, 'Россия, Челябинск, улица Труда, 30',
        'город Челябинск ул. Труда  30  ост. "Областной суд" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2577, 1648, 61.386959, 55.098042, 'Россия, Челябинск, Троицкий тракт, 35',
        'город Челябинск Троицкий тр.  напротив д. 35  ост. "Сад Локомотив" (автобус  в сторону ул. Блюхера)',
        'город Челябинск Троицкий тр.  д. 35 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2579, 1649, 61.383994, 55.193597, 'Россия, Челябинск, Комсомольский проспект, 2/1',
        'город Челябинск Комсомольский пр.  2/1  ост. "Комсомольский пр." (маршрутное такси  в сторону ЧМЗ)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2588, 1650, 61.377482, 55.231411, 'Россия, Челябинск, Черкасская улица, 15/2',
        'город Челябинск ул. Черкасская  15/2  ост. "Авторынок" (автобус  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2589, 1651, 61.37485, 55.123703, 'Россия, Челябинск, улица Дарвина, 12',
        'город Челябинск ул. Дарвина  12  ост. "Кондитерская фабрика" (автобус  в сторну ул. Блюхера)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2590, 1652, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск пр. Комарова  133  ост. "ДК Ровесник" (автобус  в сторону Аэропорта)',
        'город Челябинск пр. Комарова  133', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2591, 1653, 61.451682, 55.167457, 'Россия, Челябинск, улица Первой Пятилетки, 10/1',
        'город Челябинск ул. 1-й Пятилетки  напротив д. 10/1  ост. "ул. 1-й Пятилетки" (троллейбус  в сторону ЖБИ)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2592, 1654, 61.447793, 55.177355, 'Россия, Челябинск, улица Героев Танкограда, 100',
        'город Челябинск ул. Героев Танкограда  100  ост. "ул. Котина" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2594, 1655, 61.443373, 55.166866, 'Россия, Челябинск, улица Горького, 14',
        'город Челябинск ул. Горького  14  ост. "Монтажный колледж" (трамвай  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2595, 1656, 61.480464, 55.194696, 'Россия, Челябинск, улица Мамина, 13',
        'город Челябинск ул. Мамина  напротив д. 13  ост. "Универсам" (автобус  в центр)',
        'город Челябинск ул. Мамина  напротив д. 13', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2596, 1657, 61.372173, 55.039264, 'Россия, Челябинск, посёлок Новосинеглазово, Октябрьская улица, 15',
        'город Челябинск ул. Октябрьская  напротив д. 11  ост. "ул. Октябрьская" (автобус  в центр)',
        'город Челябинск ул. Октябрьская   д. 11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2601, 1658, 61.415822, 55.169673, 'Россия, Челябинск, Российская улица, 71/1',
        'город Челябинск ул. Российская  71/1  ост. "пл. Павших Революционеров"',
        'город Челябинск ул. Российская  71/1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2602, 1659, 61.451503, 55.171319, 'Россия, Челябинск, Салютная улица, 11',
        'город Челябинск ул. Салютная  11  ост. "Сад Победы" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2604, 1660, 61.387749, 55.166326, 'Россия, Челябинск, Свердловский проспект, 46',
        'город Челябинск Свердловский пр.  46  ост. "ДС Юность" (маршрутное такси  в центр)',
        'город Челябинск Свердловский пр.  46', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2606, 1661, 61.385351, 55.145426, 'Россия, Челябинск, улица Доватора, 42',
        'город Челябинск ул. Доватора  42  ост. "Гостиница Центральная" (автобус  в сторону ул. Воровского)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2608, 1662, 61.389061, 55.148781, 'Россия, Челябинск, улица Курчатова, 19А',
        'город Челябинск ул. Курчатова  напротив д. 19а  ост. "ул. Курчатова" (трамвай  в центр)',
        'город Челябинск ул. Курчатова   д. 19а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2611, 1663, 61.368741, 55.132598, 'Россия, Челябинск, улица Блюхера, 44',
        'город Челябинск ул. Блюхера  44  ост. "ул. Рылеева" (троллейбус  в сторону АМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2613, 1664, 61.396517, 55.159815, 'Россия, Челябинск, проспект Ленина, 59',
        'город Челябинск пр. Ленина  59  ост. "Публичная библиотека"  (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2615, 1665, 61.375532, 55.229085, 'Россия, Челябинск, Черкасская улица, 17',
        'город Челябинск ул. Черкасская  17  ост. "ул. Черкасская" (трамвай  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2616, 1666, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29  ост. "ул. Чайковского" (маршрутное такси  в центр)',
        'город Челябинск Комсомольский пр.  29', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2617, 1667, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина  76  ост. "ЮУрГУ" (маршрутное такси  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2620, 1668, 61.407306, 55.155736, 'Россия, Челябинск, улица Цвиллинга, 41А',
        'город Челябинск ул. Цвиллинга  напротив д. 41а  ост. "Городской сад" (трамвай  в сторону ж/д вокзала)',
        'город Челябинск ул. Цвиллинга  д. 41а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2621, 1669, 61.391244, 55.169015, 'Россия, Челябинск, Свердловский проспект, 51',
        'город Челябинск Свердловский пр.  51  ост. "ДС Юность" (маршрутное такси  в сторону С-З)',
        'город Челябинск Свердловский пр.  51', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2626, 1670, 61.296004, 55.170763, 'Россия, Челябинск, улица Братьев Кашириных, 138',
        'город Челябинск ул. Братьев Кашириных  напротив д. 138  ост. "Универсам Казачий" (маршрутное такси  в сторону С-З)',
        'город Челябинск ул. Братьев Кашириных  д. 138 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2632, 1671, 61.420583, 55.160545, 'Россия, Челябинск, проспект Ленина, 29',
        'город Челябинск пр. Ленина  29  ост. "Центральный рынок" (троллейбус  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2639, 1672, 61.304511, 55.174296, 'Россия, Челябинск, улица Братьев Кашириных, 124',
        'город Челябинск ул. Братьев Кашириных  напротив д. 124  ост. "25-й микрорайон" (автобус  в центр)',
        'город Челябинск ул. Братьев Кашириных   д. 124', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2640, 1673, 61.398682, 55.178352, 'Россия, Челябинск, улица Кирова, 44',
        'город Челябинск ул. Кирова  44  ост. "ул. Калинина" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2644, 1674, 61.433806, 55.185764, 'Россия, Челябинск, проспект Победы, 115',
        'город Челябинск пр. Победы  115  ост. "м-н Юрюзань" (автобус  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2645, 1675, 61.416972, 55.167596, 'Россия, Челябинск, улица Труда, 21',
        'город Челябинск ул. Труда  21  ост. "пл. Павших Революционеров" (трамвай  в сторону пр. Победы)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2647, 1676, 61.435549, 55.121258, 'Россия, Челябинск, Ленинский район, улица Гагарина, 47',
        'город Челябинск ул. Гагарина  47  ост. "Строительное училище" (троллейбус  в сторону КБС)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2649, 1677, 61.387085, 55.09338, 'Россия, Челябинск, Троицкий тракт, 21',
        'город Челябинск Троицкий тр.  напротив д. 21  ост. "Сельхозтехника" (автобус  в сторону п. Смолино)',
        'город Челябинск Троицкий тр.  д. 21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2650, 1678, 61.389232, 55.183636, 'Россия, Челябинск, проспект Победы, 177',
        'город Челябинск пр. Победы  177  ост. "пр. Победы" (маршрутное такси  в сторону ул. Кирова)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2653, 1679, 61.413639, 55.140091, 'Россия, Челябинск, улица Степана Разина, 9',
        'город Челябинск ул. С. Разина  9  ост. "Ж/д вокзал" (маршрутное такси  в центр)',
        'город Челябинск ул. С. Разина  9 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2655, 1680, 61.439295, 55.135433, 'Россия, Челябинск, Ленинский район, улица Гагарина, 25',
        'город Челябинск ул. Гагарина  25  ост. "Дом обуви" (троллейбус  в сторону КБС)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2659, 1681, 61.391927, 55.249715, 'Россия, Челябинск, шоссе Металлургов, 28',
        'город Челябинск ш. Металлургов  28  ост. "ул. Жукова" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2660, 1682, 61.42909, 55.168753, 'Россия, Челябинск, улица Первой Пятилетки, 30',
        'город Челябинск ул. 1-й Пятилетки  30  ост. "Трамвайное депо № 1" (трамвай  в центр)',
        'город Челябинск ул. 1-й Пятилетки  30', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2661, 1683, 61.445619, 55.18085, 'Россия, Челябинск, улица Бажова, 117',
        'город Челябинск ул. Героев Танкограда/ул. Бажова  117  ост. "ул. Бажова" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2663, 1684, 61.45692, 55.183852, 'Россия, Челябинск, улица Бажова, 38А',
        'город Челябинск ул. Бажова  38а  ост. "М-н Радуга" (троллейбус  в сторону ул. Г. Танкограда)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2665, 1685, 61.385944, 55.19132, 'Россия, Челябинск, Свердловский проспект, 6',
        'город Челябинск Свердловский пр.  6  ост. "Комсомольский пр." (маршрутное такси  в центр)',
        'город Челябинск Свердловский пр.  6', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2667, 1686, 61.385234, 55.192333, 'Россия, Челябинск, Комсомольский проспект, 2/2',
        'город Челябинск Комсомольский пр.  2/2  ост. "Комсомольский пр." (маршрутное такси  в сторону С-З)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2676, 1687, 61.391432, 55.188416, 'Россия, Челябинск, улица Островского, 2',
        'город Челябинск ул. Островского  2  ост "ул. Островского" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2679, 1688, 61.406641, 55.160128, 'Россия, Челябинск, проспект Ленина, 51',
        'город Челябинск пр. Ленина  51  ост. "пл. Революции" (маршрутное такси  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2682, 1689, 61.388513, 55.159151, 'Россия, Челябинск, Свердловский проспект, 72',
        'город Челябинск Свердловский пр.  72  ост. "Алое поле" (маршрутное такси  в сторону АМЗ)',
        'город Челябинск Свердловский пр.  72', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2683, 1690, 61.3684, 55.156976, 'Россия, Челябинск, улица Сони Кривой, 83',
        'город Челябинск ул. Сони Кривой  83  ост. "ПО Полет" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2684, 1691, 61.382458, 55.145915, 'Россия, Челябинск, улица Доватора, 35',
        'город Челябинск ул. Доватора  35  ост. "ул. Доватора" (маршрутное такси  в сторону ж/д вокзала)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2685, 1692, 61.372712, 55.147165, 'Россия, Челябинск, улица Худякова, 19',
        'город Челябинск ул. Худякова  19  ост. "ТК Калибр" (маршрутное такси  в сторону вокзала)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2687, 1693, 61.37105, 55.132937, 'Россия, Челябинск, улица Блюхера, 51',
        'город Челябинск ул. Блюхера  51  ост. "ул. Рылеева" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2688, 1694, 61.375532, 55.229085, 'Россия, Челябинск, Черкасская улица, 17',
        'город Челябинск ул. Черкасская  17  ост. "ул. Черкасская" (маршрутное такси  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2689, 1695, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29  ост. "ул. Красного Урала" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2691, 1696, 61.403946, 55.167226, 'Россия, Челябинск, улица Цвиллинга, 5',
        'город Челябинск ул. Цвиллинга  напротив д. 5  ост. "Оперный театр"(трамвай  в центр)',
        'город Челябинск ул. Цвиллинга  д. 5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2692, 1697, 61.378748, 55.247231, 'Россия, Челябинск, шоссе Металлургов, 70',
        'город Челябинск ш. Металлургов  70/1  ост. "ДК Строителей" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2694, 1698, 61.38783, 55.167087, 'Россия, Челябинск, улица Труда, 161',
        'город Челябинск ул. Труда  напротив д. 161  ост. "ДС Юность" (трамвай  в сторону С-З)',
        'город Челябинск ул. Труда  д. 161', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2695, 1699, 61.391244, 55.169015, 'Россия, Челябинск, Свердловский проспект, 51',
        'город Челябинск Свердловский пр.  напротив д. 51  ост. "ДС Юность" (трамвай  в сторону АМЗ)',
        'город Челябинск Свердловский пр.  д. 51', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2696, 1700, 61.376431, 55.245522, 'Россия, Челябинск, шоссе Металлургов, 41',
        'город Челябинск шоссе Металлургов 41', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2697, 1701, 61.420421, 55.161219, 'Россия, Челябинск, проспект Ленина, 34',
        'город Челябинск пр. Ленина  34  ост. "Центральный рынок" (троллейбус  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2704, 1702, 61.298017, 55.187424, 'Россия, Челябинск, улица 40-летия Победы, 21',
        'город Челябинск ул. 40 лет Победы  21  ост. "Универсам" (автобус  из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2706, 1703, 61.304511, 55.174296, 'Россия, Челябинск, улица Братьев Кашириных, 124',
        'город Челябинск ул. Чичерина/ул. Братьев Кашириных  124  ост. "25-й микрорайон" (автобус)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2708, 1704, 61.361177, 55.178157, 'Россия, Челябинск, улица Братьев Кашириных, 95',
        'город Челябинск ул. Братьев Кашириных  95  ост. "ул. Северо-Крымская" (автобус  в сторону Свердловского пр.)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2709, 1705, 61.372388, 55.03977, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 4',
        'город Челябинск ул. Кирова  2а  ост. "Теплотехнический институт" (автобус  в центр)',
        'город Челябинск ул. Кирова  2а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2710, 1706, 61.414304, 55.185841, 'Россия, Челябинск, Российская улица, 41',
        'город Челябинск пр. Победы/ул. Российская  41  ост. "ул. Российская" (трамвай  в сторону Теплотехн. Института)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2713, 1707, 61.340947, 55.195005, 'Россия, Челябинск, Комсомольский проспект, 36',
        'город Челябинск Комсомольский пр.  36  ост. "Поликлиника" (троллейбус  в сторону ул. Молодогвардейцев)',
        'город Челябинск Комсомольский пр.  36', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2714, 1708, 61.433626, 55.123708, 'Россия, Челябинск, Ленинский район, улица Гагарина, 50',
        'город Челябинск ул. Гагарина  50  ост. "Библиотека им. Мамина-Сибиряка" (троллейбус  в сторону ул. Новороссийской)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2716, 1709, 61.358976, 55.11372, 'Россия, Челябинск, улица Блюхера, 101',
        'город Челябинск ул. Блюхера  101  ост. "п. АМЗ" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2718, 1710, 61.387085, 55.09338, 'Россия, Челябинск, Троицкий тракт, 21',
        'город Челябинск Троицкий тр.  напротив д. 21  ост. "Сельхозтехника" (автобус  в сторону ул. Блюхера)',
        'город Челябинск Троицкий тр.  д. 21', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2719, 1711, 61.298681, 55.218281, 'Россия, Челябинск, улица Профессора Благих, 79',
        'город Челябинск ул. Профессора Благих  напротив д. 79  ост. "47-й микрорайон" ',
        'город Челябинск ул. Профессора Благих  д. 79', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2720, 1712, 61.286357, 55.203473, 'Россия, Челябинск, улица Бейвеля, 54А',
        'город Челябинск ул. Бейвеля  54а  ост. "55-й микрорайон" (автобус  в сторону Краснопольского пр.)',
        'город Челябинск ул. Бейвеля  54а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2721, 1713, 61.372505, 55.183436, 'Россия, Челябинск, проспект Победы, 221/55',
        'город Челябинск пр. Победы  221  ост. "ул. Косарева" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2723, 1714, 61.413639, 55.140091, 'Россия, Челябинск, улица Степана Разина, 9',
        'город Челябинск ул. С. Разина  9  ост. "Ж/д вокзал" (троллейбус  в центр)', 'город Челябинск ул. С. Разина  9',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2727, 1715, 61.393256, 55.141917, 'Россия, Челябинск, улица Доватора, 28',
        'город Челябинск ул. Доватора  28  ост. "ДК Колющенко" (автобус  в сторону ул. Воровского)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2728, 1716, 61.487238, 55.176573, 'Россия, Челябинск, улица Хохрякова, 22',
        'город Челябинск ул. Хохрякова  22  ост. "ул. Чоппа" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2730, 1717, 61.437453, 55.134533, 'Россия, Челябинск, Ленинский район, улица Гагарина, 24А',
        'город Челябинск ул. Гагарина  24а  ост. "Дом обуви" (троллейбус  в сторону ул. Новороссийская)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2733, 1718, 61.389582, 55.247585, 'Россия, Челябинск, шоссе Металлургов, 25Б',
        'город Челябинск ш. Металлургов  25б  ост. "Юридический техникум" (маршрутное такси  в центр)',
        'город Челябинск ш. Металлургов  25б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2734, 1719, 61.397038, 55.139339, 'Россия, Челябинск, улица Доватора, 11',
        'город Челябинск ул. Доватора  11  ост. "м-н "Губернский" (автобус  в сторону Ленинского района)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2735, 1720, 61.396355, 55.264876, 'Россия, Челябинск, улица Дегтярёва, 48',
        'город Челябинск ул. Дегтярева  48  сот. "Кафе Сказка" (автобус  в сторону ул. Румянцева)',
        'город Челябинск ул. Дегтярева  48', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2736, 1721, 61.372478, 55.12376, 'Россия, Челябинск, улица Дарвина, 91',
        'город Челябинск ул. Дарвина  91  ост. "Кондитерская фабрика" (автобус  в сторону Троицкого тр.)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2738, 1722, 61.283212, 55.203848, 'Россия, Челябинск, улица Бейвеля, 27',
        'город Челябинск ул. Бейвеля  27  ост. "55-й микрорайон" (автобус  в центр)', 'город Челябинск ул. Бейвеля  27',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2742, 1723, 61.297981, 55.159059, 'Россия, Челябинск, улица Академика Сахарова, 11',
        'город Челябинск ул. Академика Сахарова  напротив д. 11  ост. "ул. Ак. Сахарова" "автобус  в сторону плотины)',
        'город Челябинск ул. Академика Сахарова д. 11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2743, 1724, 61.359955, 55.193201, 'Россия, Челябинск, Комсомольский проспект, 22',
        'город Челябинск Комсомольский пр.  22  ост. "ул. Чайковского" (троллейбус  в сторону ул. Молодогвардейцев)',
        'город Челябинск Комсомольский пр.  22', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2744, 1725, 61.394424, 55.141485, 'Россия, Челябинск, улица Доватора, 26',
        'город Челябинск ул. Доватора  напротив д. 26  ост. "ДК Колющенко" (автобус  в сторону ж/д вокзала)',
        'город Челябинск ул. Доватора  д. 26', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2745, 1726, 61.363926, 55.120387, 'Россия, Челябинск, улица Блюхера, 93',
        'город Челябинск ул. Блюхера  93  ост. "Энергетический колледж" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2746, 1727, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск пр. Комарова  110  ост. "м-н Шатура" (автобус  в центр)', 'город Челябинск пр. Комарова  110',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2747, 1728, 61.374445, 55.232151, 'Россия, Челябинск, Черкасская улица, 26/1',
        'город Челябинск ул. Черкасская  26/1  ост. "Авторынок" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2748, 1729, 61.469083, 55.116377, 'Россия, Челябинск, улица Машиностроителей, 36',
        'город Челябинск ул. Машиностроителей  36  ост. "Трубопрокатный завод" (троллейбус  в сторону ул. Новороссийская)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2749, 1730, 61.353299, 55.189845, 'Россия, Челябинск, проспект Победы, 292',
        'город Челябинск пр. Победы  напротив д. 292  ост. "ул. Красного Урала" (трамвай  в центр)',
        'город Челябинск пр. Победы  д. 292', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2750, 1731, 61.334362, 55.178002, 'Россия, Челябинск, улица Братьев Кашириных, 107',
        'город Челябинск ул. Братьев Кашироиных  107  ост. "ул. Братьев Кашириных" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2751, 1732, 61.400514, 55.173005, 'Россия, Челябинск, улица Кирова, 25А',
        'город Челябинск ул. Кирова  25а  ост. "Цирк" (трамвай  из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2753, 1733, 61.431821, 55.185374, 'Россия, Челябинск, проспект Победы, 117',
        'город Челябинск ул. Горького/пр. Победы  117  ост. "ул. 5-го Декабря" (трамвай  в сторону ЧТЗ',
        'город Челябинск пр. Победы  117', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2754, 1734, 61.346589, 55.195056, 'Россия, Челябинск, Комсомольский проспект, 32',
        'город Челябинск Комсомольский пр.  32  ост. "Кинотеатр Победа" (троллейбус  в сторону ул. Молодогвардейцев)',
        'город Челябинск Комсомольский пр.  32', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2755, 1735, 61.399517, 55.079844, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 60',
        'город Челябинск ул. Гагарина  64  ост. "М-н Спорттовары" (троллейбус  в торону ул. Новороссийской',
        'город Челябинск ул. Гагарина  64', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2756, 1736, 61.364735, 55.123924, 'Россия, Челябинск, улица Блюхера, 81',
        'город Челябинск ул. Блюхера  напротив д. 81  ост. "п. Мебельный" (троллейбус  в сторону АМЗ)',
        'город Челябинск ул. Блюхера   д. 81', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2757, 1737, 61.386249, 55.109755, 'Россия, Челябинск, Троицкий тракт, 11',
        'город Челябинск Троицкий тр.  11  ост. "Рынок Привоз" (автобус  в сторону п. Смолино)',
        'город Челябинск Троицкий тр.  11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2758, 1738, 61.48386, 55.18927, 'Россия, Челябинск, улица Мамина, 23',
        'город Челябинск ул. Мамина  напротив д. 23  ост. "Рынок Северо-Восточный" (автобус  в центр)',
        'город Челябинск ул. Мамина  д. 23', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2759, 1739, 61.435351, 55.167591, 'Россия, Челябинск, Артиллерийская улица, 107',
        'город Челябинск ул. Артиллерийская  напротив д. 107  ост. "Киргородок" (трамвай  в центр)',
        'город Челябинск ул. Артиллерийская  д. 107', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2760, 1740, 61.486259, 55.181601, 'Россия, Челябинск, улица Хохрякова, 12',
        'город Челябинск ул. Хохрякова  12  сот. "1-я Охотничья" (автобус  в центр)',
        'город Челябинск ул. Хохрякова  12', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2765, 1741, 61.298681, 55.218281, 'Россия, Челябинск, улица Профессора Благих, 79',
        'город Челябинск ул. Профессора Благих  79  ост. "47-й микрорайон" (автобус  в сторону ул. Молодогвардейцев)',
        'город Челябинск ул. Профессора Благих  79', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2767, 1742, 61.367142, 55.185163, 'Россия, Челябинск, проспект Победы, 249',
        'город Челябинск пр. Победы  249  ост. "ул. Тепличная" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2769, 1743, 61.531938, 55.164711, 'Россия, Челябинск, улица Самохина, 162/1',
        'город Челябинск ул. Самохина  162  ост. "Школа" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2770, 1744, 61.482602, 55.173457, 'Россия, Челябинск, улица Мамина, 27',
        'город Челябинск ул. Мамина  27  ост. "ул. Восходящая" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2771, 1745, 61.435351, 55.167591, 'Россия, Челябинск, Артиллерийская улица, 107',
        'город Челябинск ул. Артиллерийская  107  ост. "Киргородок" (трамвай  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2772, 1746, 61.446975, 55.151487, 'Россия, Челябинск, проспект Ленина, 3',
        'город Челябинск пр. Ленина  3  ост. "ЧТЗ" (автобус  в сторону п. Чурилово)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2775, 1747, 61.284874, 55.188658, 'Россия, Челябинск, улица Чичерина, 5',
        'город Челябинск ул. Чичерина  5  ост. "18-й микрорайон" (автобус  в сторону Краснопольского пр.)',
        'город Челябинск ул. Чичерина  5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2787, 1748, 61.411725, 55.141428, 'Россия, Челябинск, улица Степана Разина, 4',
        'город Челябинск ул. С. Разина  4  ост. "Ж/д вокзал" (маршрутное такси  в сторону Ленинского района)',
        'город Челябинск ул. С. Разина  4', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2788, 1749, 61.531624, 55.170162, 'Россия, Челябинск, улица Зальцмана, 10',
        'город Челябинск ул. Зальцмана  10  ост. "ул. Зальцмана" (автобус  в сторону ул. 1-я Эльтонская)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2789, 1750, 61.320187, 55.179791, 'Россия, Челябинск, улица Братьев Кашириных, 100',
        'город Челябинск ул. Братьев Кашириных  100  ост. "ЧелГУ"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2790, 1751, 61.413522, 55.185769, 'Россия, Челябинск, Российская улица, 30',
        'город Челябинск пр. Победы/ ул. Российская  30  ост. "ул. Российская" (автобус  в сторону Теплотехн. Института)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2793, 1752, 61.459857, 55.166228, 'Россия, Челябинск, улица Марченко, 25',
        'город Челябинск ул. Марченко  25  ост. "Швейная фабрика" (автобус  в сторону ул. Салютная)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2798, 1753, 61.37485, 55.233347, 'Россия, Челябинск, Черкасская улица, 26к12',
        'город Челябинск ул. Черкасская  26/12  ост. "ул. Черкасская" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2804, 1754, 61.371894, 55.178804, 'Россия, Челябинск, улица Косарева, 56',
        'город Челябинск ул. Косарева  56  ост. "ул. Косарева" (автобус  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2805, 1755, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск пр. Комарова  129  ост. "М-н Шатура" (автобус  в сторону Аэропорта)',
        'город Челябинск пр. Комарова  129', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2806, 1756, 61.429467, 55.167498, 'Россия, Челябинск, улица Первой Пятилетки, 59',
        'город Челябинск ул. 1-й Пятилетки  59  ост. "Трамвайное депо № 1" (трамвай  в сторону ЧТЗ)',
        'город Челябинск ул. 1-й Пятилетки  59', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2808, 1757, 61.451081, 55.166372, 'Россия, Челябинск, улица 40-летия Октября, 15',
        'город Челябинск ул. Героев Танкограда/ул. 40 лет Октября  15  ост. "ул. 1-й Пятилетки" (троллейбус  в центр)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2811, 1758, 61.414456, 55.140507, 'Россия, Челябинск, Привокзальная площадь',
        'город Челябинск Привокзальная площадь  ТК "Синегорье"  центр. вход  поз. 2',
        'город Челябинск Привокзальная площадь', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2812, 1759, 61.402473, 55.159866, 'Россия, Челябинск, площадь Революции',
        'город Челябинск ул. Цвиллинга  авт.ост."пл.Революции" (из центра)', 'город Челябинск ост."пл.Революции"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2813, 1760, 0, 0, '', 'город Челябинск ул. Цвиллинга  авт.ост."Оперный театр" (в центр)',
        'город Челябинск ост."Оперный театр"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2814, 1761, 61.375937, 55.151358, 'Россия, Челябинск, улица Энтузиастов, 26',
        'город Челябинск ул. Энтузиастов  26', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2815, 1762, 61.389932, 55.081926, 'Россия, Челябинск, улица Игуменка, 183', 'город Челябинск ул. Игуменка  183',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2816, 1763, 61.347146, 55.179529, 'Россия, Челябинск, улица Братьев Кашириных, 72',
        'город Челябинск ул.Бр.Кашириных 72', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2817, 1764, 61.403416, 55.257596, 'Россия, Челябинск, улица Сталеваров, 15',
        'город Челябинск ул. Сталеваров  15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2818, 1765, 61.405761, 55.258273, 'Россия, Челябинск, улица Ярослава Гашека, 1',
        'город Челябинск ул. Я. Гашека  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2819, 1766, 61.441001, 55.162458, 'Россия, Челябинск, улица Горького, 3', 'город Челябинск ул. Горького  3',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2820, 1767, 61.429683, 55.161769, 'Россия, Челябинск, проспект Ленина, 26Ас2',
        'город Челябинск пр. Ленина  26а/2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2822, 1768, 61.417007, 55.163698, 'Россия, Челябинск, площадь МОПРа, 10/1', 'город Челябинск пл. Мопра  10/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2823, 1769, 61.488163, 55.159676, 'Россия, Челябинск, Линейная улица, 98',
        'город Челябинск ул. Линейная  98  поз. 1', 'город Челябинск ул. Линейная  98 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2824, 1770, 61.488163, 55.159676, 'Россия, Челябинск, Линейная улица, 98',
        'город Челябинск ул. Линейная  98  поз. 2', 'город Челябинск ул. Линейная  98 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2825, 1771, 61.431345, 55.161908, 'Россия, Челябинск, проспект Ленина, 26А',
        'город Челябинск пр. Ленина  26А  поз. 1', 'город Челябинск пр. Ленина  26А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2827, 1772, 61.431345, 55.161908, 'Россия, Челябинск, проспект Ленина, 26А',
        'город Челябинск пр. Ленина  26А  поз. 2', 'город Челябинск пр. Ленина  26А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2829, 1773, 61.431345, 55.161908, 'Россия, Челябинск, проспект Ленина, 26А',
        'город Челябинск пр. Ленина  26А  поз. 3', 'город Челябинск пр. Ленина  26А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2831, 1774, 61.409138, 55.143358, 'Россия, Челябинск, улица Овчинникова, 6',
        'город Челябинск ул. Овчинникова  6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2832, 1775, 61.389869, 55.122787, 'Россия, Челябинск, 1-я Томинская улица',
        'город Челябинск пересечение Троицкого тр. и ул. Томинская', 'город Челябинск  Троицкого тр. / ул. Томинская',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2833, 1776, 61.390606, 55.082503, 'Россия, Челябинск, улица Игуменка, 181', 'город Челябинск ул. Игуменка  181',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2834, 1777, 61.420044, 55.135583, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск пересечение Троицкого тр. и а/д Меридиан',
        'город Челябинск  Троицкого тр. / автодорога Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2844, 1778, 61.347882, 55.177853, 'Россия, Челябинск, улица Братьев Кашириных, 99А',
        'город Челябинск ул. Братьев Кашириных  99а  ост. "п. Бабушкина" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2846, 1779, 61.312237, 55.177745, 'Россия, Челябинск, улица Братьев Кашириных, 110',
        'город Челябинск ул. 40 лет Победы/ул. Братьев Кашириных  110  ост. "24-й микрорайон" (автобус  в сторону пр. Победы)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2849, 1780, 61.400461, 55.174172, 'Россия, Челябинск, улица Кирова, 23',
        'город Челябинск ул. Кирова  23  ост. "Цирк" (автобус  в сторону Теплотех. Института)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2851, 1781, 61.413989, 55.177632, 'Россия, Челябинск, Российская улица, 40',
        'город Челябинск ул. Российская  40  ост. "Плавательный бассейн" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2852, 1782, 61.38713, 55.175509, 'Россия, Челябинск, Свердловский проспект, 30',
        'город Челябинск Свердловский пр.  30  ост. "Торговый центр" (троллейбус  в центр)',
        'город Челябинск Свердловский пр.  30', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2858, 1783, 61.38898, 55.192584, 'Россия, Челябинск, Каслинская улица, 18',
        'город Челябинск ул. Каслинская  18  ост. "Каслинский рынок" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2859, 1784, 61.307009, 55.194917, 'Россия, Челябинск, Комсомольский проспект, 82',
        'город Челябинск Комсомольский пр.  82  ост. "8-й микрорайон" (маршрутное такси  в сторону ул. Чичерина)',
        'город Челябинск Комсомольский пр.  82', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2861, 1785, 61.441433, 55.186792, 'Россия, Челябинск, улица Героев Танкограда, 40',
        'город Челябинск ул. Героев Танкограда  40  ост. "пр. Победы" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2863, 1786, 61.38748, 55.185502, 'Россия, Челябинск, Свердловский проспект, 23А',
        'город Челябинск Свердловский пр.  23а  ост. "пр. Победы" (троллейбус  в сторону ЧМЗ)',
        'город Челябинск Свердловский пр.  23а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2864, 1787, 61.433806, 55.185764, 'Россия, Челябинск, проспект Победы, 115',
        'город Челябинск ул. Горького/пр. Победы  115  ост. "ул. 5-го Декабря" (трамвай  в сторону С-З',
        'город Челябинск пр. Победы  115', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2865, 1788, 61.379539, 55.16701, 'Россия, Челябинск, улица Труда, 179',
        'город Челябинск ул. Энгельса/ул. Труда  179  ост. "ул. Труда" (троллейбус  в центр)',
        'город Челябинск ул. Труда  179', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2867, 1789, 61.352859, 55.195061, 'Россия, Челябинск, Комсомольский проспект, 28',
        'город Челябинск Комсомольский пр.  28  ост. "ул. Красного Урала" (троллейбус  в сторону ул. Молодогвардейцев)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2869, 1790, 61.392457, 55.173134, 'Россия, Челябинск, Каслинская улица, 64',
        'город Челябинск Свердловский пр./ул. Каслинская  64  ост. "Торговый центр" (троллейбус  из центра)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2871, 1791, 61.391343, 55.191875, 'Россия, Челябинск, Каслинская улица, 5А',
        'город Челябинск ул. Каслинская  5а  ост. "Каслинский рынок" (трамвай  в сторону ЧМЗ)',
        'город Челябинск ул. Каслинская  5а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2872, 1792, 61.440543, 55.139617, 'Россия, Челябинск, Ленинский район, улица Гагарина, 16',
        'город Челябинск ул. Гагарина  16  ост. "Дом одежды" (трамвай  в сторону ул. Новороссийской)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2875, 1793, 61.363926, 55.120387, 'Россия, Челябинск, улица Блюхера, 93',
        'город Челябинск ул. Блюхера  напротив д. 93  ост. "Энергетический колледж" (троллейбус  в сторону АМЗ)',
        'город Челябинск ул. Блюхера  д. 93', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2876, 1794, 61.389312, 55.074211, 'Россия, Челябинск, Троицкий тракт, 70',
        'город Челябинск Троицкий тр.  напротив д. 70  ост. "п. Смолино" (автобус  в сторону п. Исаково)',
        'город Челябинск Троицкий тр.  д. 70', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2877, 1795, 61.408752, 55.158045, 'Россия, Челябинск, улица Пушкина, 64',
        'город Челябинск ул. Пушкина  64  ост. "Кинотеатр Пушкина" (троллейбус  в сторону АМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2878, 1796, 61.378173, 55.26068, 'Россия, Челябинск, улица Румянцева, 2А',
        'город Челябинск ул. Румянцева  2а  ост. "60 лет Октября" (автобус  в сторону ш. Металлургов)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2880, 1797, 61.276745, 55.188668, 'Россия, Челябинск, улица Салавата Юлаева, 3',
        'город Челябинск ул. Салавата Юлаева  3  ост. "С. Юлаева" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2881, 1798, 61.415669, 55.146028, 'Россия, Челябинск, улица Свободы, 106',
        'город Челябинск ул. Свободы  106  ост. "Ж/д институт" (троллейбус  в сторону ж/д вокзала)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2882, 1799, 61.402302, 55.263927, 'Россия, Челябинск, улица Сталеваров, 26',
        'город Челябинск ул. Сталеваров  26  рст. "ДК Восток" (троллейбус  в сторону ш. Металлургов)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2883, 1800, 61.40868, 55.157644, 'Россия, Челябинск, улица Тимирязева, 29',
        'город Челябинск ул. Тимирязева  29  ост. "Кинотеатр Пушкина" (троллейбус  в сторону центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2887, 1801, 61.372541, 55.169946, 'Россия, Челябинск, улица Труда, 187',
        'город Челябинск ул. Труда  187  ост. "Зоопарк" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2889, 1802, 61.301493, 55.147706, 'Россия, Челябинск, посёлок Шершни, Центральная улица, 3В',
        'город Челябинск ул. Центральная  напротив д. 3в  ост. "п. Шершни"', 'город Челябинск ул. Центральная  д. 3в',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2890, 1803, 61.379853, 55.242458, 'Россия, Челябинск, Черкасская улица, 3',
        'город Челябинск ул. Черкасская  3  ост. "ДЦ Импульс" (троллейбус  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2891, 1804, 61.391001, 55.175046, 'Россия, Челябинск, улица Братьев Кашириных, 32',
        'город Челябинск ул. Братьев Кашириных  32  ост. "Торговый центр" (автобус  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2892, 1805, 61.52821, 55.168485, 'Россия, Челябинск, 2-я Эльтонская улица, 43',
        'город Челябинск ул. 2-я Эльтонская  напротив д. 43  ост. "ул. 2-я Эльтонская" (автобус  в сторону ул. 1-я Эльтонская)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2893, 1806, 61.382261, 55.153756, 'Россия, Челябинск, улица Энгельса, 83',
        'город Челябинск ул. Энгельса  83  ост. "ул. Курчатова" (автобус  в центр)', 'город Челябинск ул. Энгельса  83',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2894, 1807, 61.41398, 55.156225, 'Россия, Челябинск, улица Свободы, 149',
        'город Челябинск ул. Свободы  149  ост. "ул. Плеханова" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2896, 1808, 61.315713, 55.194013, 'Россия, Челябинск, Комсомольский проспект, 69',
        'город Челябинск Комсомольский пр.  69  ост. "ул. Солнечная" (маршрутное такси  в центр)',
        'город Челябинск Комсомольский пр.  69', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2897, 1809, 61.298628, 55.177087, 'Россия, Челябинск, улица Чичерина, 38В',
        'город Челябинск ул. Чичерина  38В  ост. "ул. 250 лет Челябинску" (маршрутное такси  в сторону ул. Братьев Кашириных)',
        'город Челябинск ул. Чичерина  38В', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2899, 1810, 61.360423, 55.187795, 'Россия, Челябинск, улица Чайковского, 89',
        'город Челябинск ул. Чайковского  напротив д. 89  ост. "Обувная фабрика" (трамвай  в центр)',
        'город Челябинск ул. Чайковского  д. 89', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2900, 1811, 61.380877, 55.153457, 'Россия, Челябинск, улица Энгельса, 44Г',
        'город Челябинск ул. Энгельса  44г  ост. "ул. Курчатова"  маршрутное такси  в сторону ул. Худякова)',
        'город Челябинск ул. Энгельса  44г', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2902, 1812, 61.365938, 55.158904, 'Россия, Челябинск, проспект Ленина, 87',
        'город Челябинск пр. Ленина  87  ост. "ПКиО им. Гагарина" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2905, 1813, 61.41584, 55.151091, 'Россия, Челябинск, улица Свободы, 159',
        'город Челябинск ул. Свободы  159  ост. "ул. Евтеева" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2907, 1814, 61.38112, 55.146147, 'Россия, Челябинск, улица Воровского, 40',
        'город Челябинск ул. Воровского  напротив д. 40  ост. "ул. Доватора" (автобус  в сторону плотины)',
        'город Челябинск ул. Воровского  д. 40 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2908, 1815, 61.339887, 55.18781, 'Россия, Челябинск, проспект Победы, 287',
        'город Челябинск пр. Победы  287  ост. "Больница скорой помощи" (трамвай  в центр)',
        'город Челябинск пр. Победы  287', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2909, 1816, 61.355976, 55.171051, 'Россия, Челябинск, улица Труда, 203',
        'город Челябинск ул. Труда  напротив д. 203  ост. "ТРК Родник" (троллейбус  в сторону С-З)',
        'город Челябинск ул. Труда  д. 203', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2912, 1817, 61.299885, 55.178198, 'Россия, Челябинск, улица 250-летия Челябинска, 17',
        'город Челябинск ул. 250 лет Челябинску  17  ост. "ул. 250 лет Челябинску"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2913, 1818, 61.372056, 55.040446, 'Россия, Челябинск, посёлок Новосинеглазово, улица Кирова, 1',
        'город Челябинск ул. Кирова  1Б  ост. "Теплотехнический институт" (трамвай  в сторону С-З  ЧТЗ)',
        'город Челябинск ул. Кирова  1Б ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2914, 1819, 61.341845, 55.178121, 'Россия, Челябинск, улица Братьев Кашириных, 103',
        'город Челябинск ул. Братьев Кашириных  103  ост. "Каширинский рынок" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2915, 1820, 61.33394, 55.183235, 'Россия, Челябинск, улица Молодогвардейцев, 43',
        'город Челябинск ул. Молодогвардейцев  43  ост. "Педучилище" (троллейбус  из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2916, 1821, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124',
        'город Челябинск пр. Победы  124  ост. "м-н Юрюзань" (автобус  в сторону Теплотех. Института)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2917, 1822, 61.382854, 55.119651, 'Россия, Челябинск, улица Дарвина, 2В',
        'город Челябинск ул. Дарвина  2В  ост. "ЦРМ" (автобус  в сторону ул. Блюхера)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2918, 1823, 61.401449, 55.13762, 'Россия, Челябинск, улица Доватора, 1Б',
        'город Челябинск ул. Доватора  напротив д. 1б  ост. "м-н Губернский" (автобус  в сторону ул. Воровского)',
        'город Челябинск ул. Доватора  д. 1б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2919, 1824, 61.442142, 55.173303, 'Россия, Челябинск, улица Горького, 28',
        'город Челябинск ул. Горького  28  ост. "ул. Правдухина" (трамвай  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2921, 1825, 61.301493, 55.147706, 'Россия, Челябинск, посёлок Шершни, Центральная улица, 3В',
        'город Челябинск ул. Центральная  3в  ост. "п. Шершни" (маршрутное такси  в сторону плотины)',
        'город Челябинск ул. Центральная  3в', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2922, 1826, 61.379853, 55.242458, 'Россия, Челябинск, Черкасская улица, 3',
        'город Челябинск ул. Черкасская  3  ост. "ДЦ Импульс" (трамвай  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2923, 1827, 61.280446, 55.190287, 'Россия, Челябинск, улица Чичерина, 2',
        'город Челябинск ул. Чичерина  2  ост. "ЖК Александровский" (автобус  в сторону центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2925, 1828, 61.296382, 55.168526, 'Россия, Челябинск, улица Братьев Кашириных, 147',
        'город Челябинск ул. Братьев Кашириных  147  ост. "ул. Академика Макеева" (автобус  в сторону С-З)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2927, 1829, 61.382315, 55.258715, 'Россия, Челябинск, улица Богдана Хмельницкого, 24',
        'город Челябинск ул. Б. Хмельницкого  24  ост. "Общежитие" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2928, 1830, 61.391684, 55.258268, 'Россия, Челябинск, улица Жукова, 5А',
        'город Челябинск ул. Жукова  5а  ост. "Кинотеатр Россия" (троллейбус  в сторону ул. Сталеваров)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2929, 1831, 61.402194, 55.257673, 'Россия, Челябинск, улица Сталеваров, 40',
        'город Челябинск ул. Сталеваров  40  ост. "ДК Металлургов" (автобус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2933, 1832, 61.411348, 55.26423, 'Россия, Челябинск, 2-я Павелецкая улица, 18/1',
        'город Челябинск ул. Павелецкая  2-я  18/1  ост. "ЧМК" (трамвай  в центр)',
        'город Челябинск ул. Павелецкая  2-я  18/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2934, 1833, 61.471625, 55.120357, 'Россия, Челябинск, улица Машиностроителей, 22',
        'город Челябинск ул. Машиностроителей  22  ост. "ЗЭМ" (троллейбус  в сторону ул. Новороссийской)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2935, 1834, 61.439753, 55.161327, 'Россия, Челябинск, проспект Ленина, 20',
        'город Челябинск пр. Ленина  20  ост. "Комсомольская пл." (троллейбус  в сторону ПКиО)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2938, 1835, 61.434282, 55.120248, 'Россия, Челябинск, Ленинский район, улица Гагарина, 56',
        'город Челябинск ул. Гагарина  56  ост. "Строительное училище" (троллейбус  в сторону ул. Новороссийской)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2940, 1836, 61.399966, 55.167282, 'Россия, Челябинск, улица Кирова, 82', 'город Челябинск ул.Кирова  82', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2941, 1837, 61.401898, 55.185148, 'Россия, Челябинск, проспект Победы, 164', 'город Челябинск пр.Победы 164',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2942, 1838, 61.27935, 55.190827, 'Россия, Челябинск, улица Салавата Юлаева, 6', 'город Челябинск ул.С.Юлаева 6',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2943, 1839, 61.363827, 55.204048, 'Россия, Челябинск, Мастеровая улица, 8', 'город Челябинск ул.Мастеровая 8',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2944, 1840, 61.286496, 55.186998, 'Россия, Челябинск, улица Чичерина',
        'город Челябинск пересеч.ул.Чичерина и Комсомольского пр.', 'город Челябинск ул.Чичерина / Комсомольского пр.',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2945, 1841, 61.394154, 55.141238, 'Россия, Челябинск, улица Доватора',
        'город Челябинск ул.Доватора перед виадуком в Ленинский р-н в центр', 'город Челябинск ул.Доватора', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2946, 1842, 61.423116, 55.133334, 'Россия, Челябинск, улица Дзержинского, 119',
        'город Челябинск ул.Дзержинского 119', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2947, 1843, 61.438046, 55.13285, 'Россия, Челябинск, Ленинский район, улица Гагарина, 29',
        'город Челябинск ул.Гагарина 29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2949, 1844, 61.458285, 55.137729, 'Россия, Челябинск, Копейское шоссе, 35Б',
        'город Челябинск Копейское шоссе 35Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2950, 1845, 61.415687, 55.153987, 'Россия, Челябинск, улица Свободы, 155К2',
        'город Челябинск ул. Свободы  155/2  ост. "Академическая" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2951, 1846, 61.426224, 55.160391, 'Россия, Челябинск, проспект Ленина, 21В',
        'город Челябинск пр. Ленина  21В  ост. "Агентство воздушных сообщений" (троллейбус  в сторону ЧТЗ)',
        'город Челябинск пр. Ленина  21В', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2952, 1847, 61.344208, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 101',
        'город Челябинск ул. Братьев Кашириных  напротив д. 101  ост. "п. Бабушкина"',
        'город Челябинск ул. Братьев Кашириных  д. 101', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2953, 1848, 61.332036, 55.189557, 'Россия, Челябинск, улица Молодогвардейцев, 48А',
        'город Челябинск ул. Молодогвардейцев  напротив д. 48а  ост. "пр. Победы" (троллейбус  из центра)',
        'город Челябинск ул. Молодогвардейцев  д. 48а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2963, 1849, 61.331712, 55.188442, 'Россия, Челябинск, проспект Победы, 289А',
        'город Челябинск пр. Победы  289а  ост. "Университет" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2964, 1850, 61.438378, 55.178624, 'Россия, Челябинск, улица Горького, 38',
        'город Челябинск ул. Горького  38  ост. "ДК Смена" (трамвай  в сторону С-З)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2965, 1851, 61.389312, 55.074211, 'Россия, Челябинск, Троицкий тракт, 70',
        'город Челябинск Троицкий тр.  70  ост. "п. Смолино" (автобус  в сторону ул. Блюхера)',
        'город Челябинск Троицкий тр.  70', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2966, 1852, 61.295726, 55.164305, 'Россия, Челябинск, улица Братьев Кашириных, 161',
        'город Челябинск ул. Братьев Кашириных  161  ост. "ул. Академика Королева" (автобус  в сторону С-З)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2968, 1853, 61.378398, 55.121597, 'Россия, Челябинск, улица Дарвина, 10',
        'город Челябинск ул. Дарвина  10  ост. "Кинотеатр Маяк" (автобус  в сторону ул. Блюхера)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2969, 1854, 61.425182, 55.185276, 'Россия, Челябинск, проспект Победы, 127',
        'город Челябинск пр. Победы  127  ост. "Юридический институт" (автобус в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2971, 1855, 61.403766, 55.251516, 'Россия, Челябинск, улица Сталеваров, 37',
        'город Челябинск ул. Сталеваров  37/1  ост. "ул. Сталеваров" (автобус  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2980, 1856, 61.379009, 55.251511, 'Россия, Челябинск, улица Румянцева, 28Б',
        'город Челябинск ул. Румянцева  28б  ост. "Больница ЧМК" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2982, 1857, 61.380347, 55.258089, 'Россия, Челябинск, улица Богдана Хмельницкого, 35',
        'город Челябинск ул. Б. Хмельницкого  напротив д. 35  ост. "ул. Б. Хмельницкого (автобус  в центр)', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2983, 1858, 61.437229, 55.132119, 'Россия, Челябинск, Ленинский район, улица Гагарина, 31',
        'город Челябинск ул. Гагарина  31  ост. "Кинотеатр Аврора" (маршрутное такси  в сторону КБС)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2985, 1859, 61.435073, 55.132129, 'Россия, Челябинск, улица Дзержинского, 102',
        'город Челябинск ул. Дзержинского  102  ост. "Кинотеатр Аврора" (маршрутное такси  в сторону ж/д вокзала)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2986, 1860, 61.376197, 55.137734, 'Россия, Челябинск, улица Воровского, 85',
        'город Челябинск ул. Воровского  85  ост. "Обл. больница" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2987, 1861, 61.46292, 55.170614, 'Россия, Челябинск, улица Марченко, 13',
        'город Челябинск ул. Марченко  13  ост. "Магазин № 28" (автобус  в сторону С-В)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2988, 1862, 61.40276, 55.251778, 'Россия, Челябинск, улица Сталеваров, 66',
        'город Челябинск ул. Сталеваров  66/3  ост. "ул. Сталеваров" (трамвай  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2992, 1863, 61.4131, 55.263809, 'Россия, Челябинск, 2-я Павелецкая улица, 14',
        'город Челябинск ул. Павелецкая  2-я  14  ост. "ЧМК" (трамвай  в сторону завода Теплоприбор)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2994, 1864, 61.451772, 55.159506, 'Россия, Челябинск, проспект Ленина, 7',
        'город Челябинск пр. Ленина  7  ост. "Театр ЧТЗ" (троллейбус  в сторону ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2996, 1865, 61.379431, 55.183426, 'Россия, Челябинск, Краснознамённая улица, 28',
        'город Челябинск ул. Краснознаменная  28  ост. "ул. Краснознаменная" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2997, 1866, 61.474634, 55.122709, 'Россия, Челябинск, улица Машиностроителей, 21к1',
        'город Челябинск ул. Машиностроителей  21к1  ост. "ЗЭМ" (троллейбус  в сторону Копейского ш.)',
        'город Челябинск ул. Машиностроителей  21к1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (2999, 1867, 61.331811, 55.1976, 'Россия, Челябинск, улица Молодогвардейцев, 24',
        'город Челябинск ул. Молодогвардейцев  24  ост. "ул. Куйбышева" (маршрутное такси  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3000, 1868, 61.475865, 55.190611, 'Россия, Челябинск, улица Танкистов, 41',
        'город Челябинск ул. Танкистов  41  ост. "п. Первоозерный" (троллейбус  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3002, 1869, 61.37361, 55.14006, 'Россия, Челябинск, улица Воровского, 64',
        'город Челябинск ул. Воровского  64  ост. "Медакадемия" (троллейбус  в сторону АМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3003, 1870, 61.377482, 55.231411, 'Россия, Челябинск, Черкасская улица, 15/2',
        'город Челябинск ул. Черкасская  15/2  ост. "Авторынок" (трамвай  в сторону ЧМЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3005, 1871, 61.473871, 55.124341, 'Россия, Челябинск, улица Машиностроителей, 10',
        'город Челябинск ул. Машиностроителей  10  ост. "ул. Энергетиков" (троллейбус  в сторону ул. Новороссийской)',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3007, 1872, 61.397433, 55.077772, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 40',
        'город Челябинск ул. Гагарина  40  ост. "Управление соцзащиты населения" (троллейбус  в сторону ул. Новороссийской)',
        'город Челябинск ул. Гагарина  40 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3008, 1873, 61.412435, 55.149403, 'Россия, Челябинск, улица Цвиллинга, 61',
        'город Челябинск ул. Цвиллинга  61  ост. "ул. Евтеева" (трамвай  в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3010, 1874, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3011, 1875, 61.355976, 55.171051, 'Россия, Челябинск, улица Труда, 203',
        'город Челябинск ул.Труда 203 (из центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3012, 1876, 61.376197, 55.137734, 'Россия, Челябинск, улица Воровского, 85',
        'город Челябинск ул. Воровского  85', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3013, 1877, 61.382261, 55.153756, 'Россия, Челябинск, улица Энгельса, 83', 'город Челябинск ул. Энгельса  83',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3014, 1878, 61.352859, 55.195061, 'Россия, Челябинск, Комсомольский проспект, 28',
        'город Челябинск Комсомольский пр.  28', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3015, 1879, 61.373772, 55.172254, 'Россия, Челябинск, улица Труда, 168', 'город Челябинск ул.Труда 168', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3016, 1880, 61.276098, 55.188545, 'Россия, Челябинск, улица Салавата Юлаева, 5',
        'город Челябинск ул.Салавата Юлаева  5', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3017, 1881, 61.355976, 55.171051, 'Россия, Челябинск, улица Труда, 203',
        'город Челябинск ул. Труда 203 (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3018, 1882, 61.287668, 55.186921, 'Россия, Челябинск, Комсомольский проспект, 111',
        'город Челябинск Комсомольский пр.  111', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3019, 1883, 61.375119, 55.168578, 'Россия, Челябинск, улица Труда, 183', 'город Челябинск ул. Труда 183', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3020, 1884, 61.280446, 55.190287, 'Россия, Челябинск, улица Чичерина, 2', 'город Челябинск ул. Чичерина  2',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3021, 1885, 61.294917, 55.190195, 'Россия, Челябинск, Комсомольский проспект, 103',
        'город Челябинск Комсомольский пр.  103', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3022, 1886, 61.372541, 55.169946, 'Россия, Челябинск, улица Труда, 187', 'город Челябинск ул. Труда 187', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3023, 1887, 61.284874, 55.188658, 'Россия, Челябинск, улица Чичерина, 5', 'город Челябинск ул. Чичерина  5',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3024, 1888, 61.374562, 55.170784, 'Россия, Челябинск, улица Труда, 166', 'город Челябинск ул.Труда 166', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3025, 1889, 61.349769, 55.179236, 'Россия, Челябинск, улица Братьев Кашириных, 66',
        'город Челябинск ул.Бр.Кашириных  66', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3026, 1890, 61.398394, 55.182768, 'Россия, Челябинск, улица Кирова, 2', 'город Челябинск ул. Кирова  2', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3029, 1891, 61.46292, 55.170614, 'Россия, Челябинск, улица Марченко, 13', 'город Челябинск ул.Марченко  13',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3030, 1892, 61.456965, 55.171766, 'Россия, Челябинск, Салютная улица, 2',
        'город Челябинск ул.Комарова/ ул. Салютная 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3032, 1893, 61.361698, 55.184711, 'Россия, Челябинск, Северо-Крымская улица',
        'город Челябинск ул.Бр.Кашириных  ост. "ул. Северо-Крымская" (из центра)',
        'город Челябинск ост. "ул. Северо-Крымская"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3033, 1894, 61.461241, 55.170691, 'Россия, Челябинск, улица Марченко, 18', 'город Челябинск ул.Марченко  18',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3034, 1895, 61.38748, 55.185502, 'Россия, Челябинск, Свердловский проспект, 23А',
        'город Челябинск Свердловский пр.  23А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3035, 1896, 61.381632, 55.160782, 'Россия, Челябинск, улица Энгельса, 63', 'город Челябинск ул. Энгельса  63',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3036, 1897, 61.294127, 55.166547, 'Россия, Челябинск, улица Братьев Кашириных, 152',
        'город Челябинск ул.Бр.Кашириных 152', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3037, 1898, 61.331964, 55.183575, 'Россия, Челябинск, улица Молодогвардейцев, 62',
        'город Челябинск ул. Молодогвардейцев 62', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3038, 1899, 61.386456, 55.182264, 'Россия, Челябинск, Свердловский проспект, 28',
        'город Челябинск Свердловский пр. 28', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3039, 1900, 61.379539, 55.16701, 'Россия, Челябинск, улица Труда, 179', 'город Челябинск ул.Труда 179', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3040, 1901, 61.410791, 55.164207, 'Россия, Челябинск, улица Свободы, 44', 'город Челябинск ул.Свободы 44', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3041, 1902, 61.362825, 55.204761, 'Россия, Челябинск, Мастеровая улица',
        'город Челябинск пересеч.ул.Мастеровая и ул.Теннисная', 'город Челябинск ул.Мастеровая / ул.Теннисная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3042, 1903, 61.399966, 55.167282, 'Россия, Челябинск, улица Кирова, 82',
        'город Челябинск ул. Кирова  напротив д. 82 констр.4', 'город Челябинск ул. Кирова  д. 82', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3043, 1904, 61.40011, 55.166444, 'Россия, Челябинск, улица Кирова, 86',
        'город Челябинск ул.Кирова  86  поз. 1  констр.5', 'город Челябинск ул.Кирова  86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3046, 1905, 61.40011, 55.166444, 'Россия, Челябинск, улица Кирова, 86',
        'город Челябинск ул.Кирова напротив д. 86 констр.8', 'город Челябинск ул.Кирова  86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3047, 1906, 61.40011, 55.166444, 'Россия, Челябинск, улица Кирова, 86', 'город Челябинск ул.Кирова 86 констр.9',
        'город Челябинск ул.Кирова  86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3050, 1907, 61.400919, 55.165693, 'Россия, Челябинск, улица Кирова, 143',
        'город Челябинск ул. Кирова  143 констр.12', 'город Челябинск ул. Кирова  143', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3053, 1908, 61.41566, 55.165174, 'Россия, Челябинск, Российская улица, 154',
        'город Челябинск ул.Российская 154', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3054, 1909, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 1', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3055, 1910, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 2', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3056, 1911, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 3', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3057, 1912, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 4', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3058, 1913, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 5', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3059, 1914, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 6', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3060, 1915, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 1', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3061, 1916, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 2', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3062, 1917, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 3', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3063, 1918, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 4', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3064, 1919, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 5', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3065, 1920, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 6', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3066, 1921, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 7', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3067, 1922, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7  поз. 8', 'город Челябинск ул.Молодогвардейцев 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3068, 1923, 61.339887, 55.18781, 'Россия, Челябинск, проспект Победы, 287', 'город Челябинск пр. Победы  287',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3069, 1924, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52', 'город Челябинск ул. Чичерина 52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3070, 1925, 61.428641, 55.141423, 'Россия, Челябинск, улица Руставели, 25',
        'город Челябинск ул. Ш. Руставелли  25', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3071, 1926, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск Копейское шоссе  ост. "Сад Энергетик"',
        'город Челябинск ост. "Сад Энергетик"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3072, 1927, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 1', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3073, 1928, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 2', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3074, 1929, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 3', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3075, 1930, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 4', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3076, 1931, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 5', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3077, 1932, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 6', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3078, 1933, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 7', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3079, 1934, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 8', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3080, 1935, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 9', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3081, 1936, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 10', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3082, 1937, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 11', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3083, 1938, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 12', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3084, 1939, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 13', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3085, 1940, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 14', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3086, 1941, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 15', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3087, 1942, 61.300092, 55.162721, 'Россия, Челябинск, улица Академика Королёва, 3',
        'город Челябинск ул. Академика Королева  3  поз. 16', 'город Челябинск ул. Академика Королева  3', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3088, 1943, 61.314734, 55.177334, 'Россия, Челябинск, улица Братьев Кашириных, 129Д',
        'город Челябинск ул.Бр.Кашириных 129Д', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3089, 1944, 61.326251, 55.08157, 'Россия, Челябинск, Уфимский тракт', 'город Челябинск Уфимский тр.  1868+300м',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3090, 1945, 61.384542, 55.151204, 'Россия, Челябинск, улица Курчатова, 23', 'город Челябинск ул. Курчатова  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3091, 1946, 61.308985, 55.176331, 'Россия, Челябинск, улица Братьев Кашириных, 114',
        'город Челябинск ул. Бр. Кашириных  114', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3092, 1947, 61.304808, 55.194049, 'Россия, Челябинск, Комсомольский проспект, 93',
        'город Челябинск Комсомольский пр.  93', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3093, 1948, 61.388172, 55.161342, 'Россия, Челябинск, Свердловский проспект, 62А',
        'город Челябинск Свердловский пр.  62А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3094, 1949, 61.363935, 55.191135, 'Россия, Челябинск, улица Чайковского, 15',
        'город Челябинск пересечение Комсомольского пр. и ул. Чайковского  15/2',
        'город Челябинск ул. Чайковского  15/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3097, 1950, 61.300164, 55.172511, 'Россия, Челябинск, улица Братьев Кашириных, 132',
        'город Челябинск ул. Бр. Кашириных  132', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3098, 1951, 61.37361, 55.14006, 'Россия, Челябинск, улица Воровского, 64', 'город Челябинск ул. Воровского  64',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3099, 1952, 61.320187, 55.179791, 'Россия, Челябинск, улица Братьев Кашириных, 100',
        'город Челябинск ул. Бр. Кашириных  100', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3100, 1953, 61.433878, 55.13269, 'Россия, Челябинск, улица Дзержинского, 104',
        'город Челябинск ул. Дзержинского 104', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3101, 1954, 61.299517, 55.170681, 'Россия, Челябинск, улица Братьев Кашириных, 141',
        'город Челябинск ул. Бр. Кашириных  141', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3102, 1955, 61.331964, 55.192528, 'Россия, Челябинск, улица Молодогвардейцев, 40',
        'город Челябинск ул. Молодогвардейцев  40', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3103, 1956, 61.367591, 55.126055, 'Россия, Челябинск, улица Дарвина, 113', 'город Челябинск ул. Дарвина  113',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3104, 1957, 61.294217, 55.191377, 'Россия, Челябинск, Комсомольский проспект, 94',
        'город Челябинск Комсомольский пр.  94', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3105, 1958, 61.298097, 55.17174, 'Россия, Челябинск, улица Братьев Кашириных, 134',
        'город Челябинск ул. Бр. Кашириных  134', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3106, 1959, 61.358195, 55.193694, 'Россия, Челябинск, Комсомольский проспект, 24',
        'город Челябинск Комсомольский пр.  24', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3108, 1960, 61.433321, 55.126925, 'Россия, Челябинск, Ленинский район, улица Гагарина, 44',
        'город Челябинск ул.Гагарина  44', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3110, 1961, 61.366738, 55.128223, 'Россия, Челябинск, улица Блюхера, 69',
        'город Челябинск ул. Блюхера  напротив д. 69', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3111, 1962, 61.330509, 55.195, 'Россия, Челябинск, Комсомольский проспект, 50',
        'город Челябинск Комсомольский пр.  50', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3112, 1963, 61.39667, 55.140482, 'Россия, Челябинск, улица Доватора, 22', 'город Челябинск ул.Доватора  22',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3113, 1964, 61.331991, 55.186546, 'Россия, Челябинск, улица Молодогвардейцев, 56',
        'город Челябинск ул. Молодогвардейцев 56', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3114, 1965, 61.29242, 55.18909, 'Россия, Челябинск, Комсомольский проспект, 105',
        'город Челябинск Комсомольский пр.  105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3115, 1966, 61.294585, 55.184377, 'Россия, Челябинск, проспект Победы, 390', 'город Челябинск пр. Победы  390',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3116, 1967, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр.  34  поз. 1', 'город Челябинск Комсомольский пр.  34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3117, 1968, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр.  34  поз. 2', 'город Челябинск Комсомольский пр.  34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3118, 1969, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр.  34  поз. 3', 'город Челябинск Комсомольский пр.  34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3119, 1970, 61.409929, 55.252301, 'Россия, Челябинск, шоссе Металлургов, 21П',
        'город Челябинск ш. Металлургов  21п', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3120, 1971, 61.493194, 55.142771, 'Россия, Челябинск, Енисейская улица, 52/1',
        'город Челябинск ул. Енисейская  52', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3123, 1972, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Западное шоссе  100м до поворота на пос. Западный', 'город Челябинск пос. Западный', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3131, 1973, 61.391244, 55.169015, 'Россия, Челябинск, Свердловский проспект, 51',
        'город Челябинск Свердловский пр.  51 (малая арена  вид на ул. Труда)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3132, 1974, 61.465984, 55.189383, 'Россия, Челябинск, улица Бажова, 35', 'город Челябинск ул. Бажова  35', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3134, 1975, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск дорога из Аэропорта  750 м от площади аэропорта (в город)',
        'город Челябинск дорога из Аэропорта ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3135, 1976, 61.357126, 55.192811, 'Россия, Челябинск, Комсомольский проспект, 29',
        'город Челябинск Комсомольский пр.  29', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3136, 1977, 61.329709, 55.188396, 'Россия, Челябинск, проспект Победы, 289', 'город Челябинск пр. Победы  289',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3137, 1978, 61.39022, 55.191665, 'Россия, Челябинск, Комсомольский проспект, 1',
        'город Челябинск Комсомольский пр.  1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3139, 1979, 61.332633, 55.178684, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул. Бр. Кашириных (в центр) и ул. Молодогвардейцев',
        'город Челябинск ул. Бр. Кашириных / ул. Молодогвардейцев', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3140, 1980, 61.418202, 55.156477, 'Россия, Челябинск, Российская улица, 275',
        'город Челябинск ул. Российская  275', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3141, 1981, 61.41981, 55.131084, 'Россия, Челябинск, Гражданская улица, 25',
        'город Челябинск автодорога Меридиан/  ул. Гражданская  напротив д.25',
        'город Челябинск ул. Гражданская  напротив д.25', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3142, 1982, 61.297289, 55.184094, 'Россия, Челябинск, проспект Победы, 319', 'город Челябинск пр. Победы  319',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3143, 1983, 61.396679, 55.146358, 'Россия, Челябинск, улица Курчатова, 1А', 'город Челябинск ул. Курчатова 1А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3144, 1984, 61.451629, 55.173951, 'Россия, Челябинск, улица Героев Танкограда, 65',
        'город Челябинск ул. Героев Танкограда  65', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3145, 1985, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52', 'город Челябинск ул. Чичерина  52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3146, 1986, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина  52  поз. 1', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3147, 1987, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина  52  поз. 2', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3148, 1988, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина  52  поз. 3', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3149, 1989, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина  52  поз. 4', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3150, 1990, 61.369505, 55.128984, 'Россия, Челябинск, Братская улица, 2А',
        'город Челябинск ул. Братская  напротив д. 2а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3151, 1991, 61.315157, 55.154167, 'Россия, Челябинск, посёлок Шершни, Рабоче-Колхозная улица, 34',
        'город Челябинск ул. Рабоче-Колхозная  34 36', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3152, 1992, 61.390422, 55.192173, 'Россия, Челябинск, Комсомольский проспект',
        'город Челябинск пересечение Комсомольского пр. и ул. Каслинская  за перекрестком из центра',
        'город Челябинск Комсомольского пр. / ул. Каслинская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3155, 1993, 61.388172, 55.105212, 'Россия, Челябинск, Троицкий тракт, 54к1',
        'город Челябинск Троицкий тр.  54/1  в центр  до ост. "ул.Потребительская"',
        'город Челябинск Троицкий тр.  54/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3156, 1994, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск Копейское шоссе ост. "Сад Энергетик"',
        'город Челябинск ост. "Сад Энергетик"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3157, 1995, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина 52  поз. 5', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3158, 1996, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина 52  поз. 6', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3159, 1997, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина 52  поз. 7', 'город Челябинск ул. Чичерина  52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3160, 1998, 61.306838, 55.170619, 'Россия, Челябинск, улица Чичерина, 52',
        'город Челябинск ул. Чичерина 52  поз. 8', 'город Челябинск ул. Чичерина 52', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3161, 1999, 61.531624, 55.170162, 'Россия, Челябинск, улица Зальцмана, 10', 'город Челябинск ул. Зальцмана  10',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3162, 2000, 61.464034, 55.133961, 'Россия, Челябинск, Копейское шоссе, 33А',
        'город Челябинск Копейское шоссе 33А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3163, 2001, 61.464034, 55.133961, 'Россия, Челябинск, Копейское шоссе, 33А',
        'город Челябинск Копейское шоссе 33А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3164, 2002, 61.408285, 55.264999, 'Россия, Челябинск, улица 60-летия Октября, 2Б',
        'город Челябинск ул.60 лет Октября 2Б  ост. "ЧМК" (маршрутное такси  в сторону центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3166, 2003, 61.386878, 55.143029, 'Россия, Челябинск, улица Доватора, 23',
        'город Челябинск ул.Блюхера/ул.Доватора  23  ост. "ул. Доватора" (трамвай  в сторону центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3169, 2004, 61.316603, 55.207572, 'Россия, Челябинск, улица Молодогвардейцев, 2А/1',
        'город Челябинск ул.Молодогвардейцев  2а/1  ост. "Автоцентр" (автобус  в сторону центра)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3170, 2005, 61.262443, 55.075128, 'Россия, Челябинск, посёлок Сосновка, улица Коммуны, 50',
        'город Челябинск ул.Коммуны 58 поз.1', 'город Челябинск ул.Коммуны 58', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3171, 2006, 61.262443, 55.075128, 'Россия, Челябинск, посёлок Сосновка, улица Коммуны, 50',
        'город Челябинск ул.Коммуны 58 поз.2', 'город Челябинск ул.Коммуны 58', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3172, 2007, 61.262443, 55.075128, 'Россия, Челябинск, посёлок Сосновка, улица Коммуны, 50',
        'город Челябинск ул.Коммуны 60 поз.3', 'город Челябинск ул.Коммуны 60 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3173, 2008, 61.262443, 55.075128, 'Россия, Челябинск, посёлок Сосновка, улица Коммуны, 50',
        'город Челябинск ул.Коммуны 60 поз.4', 'город Челябинск ул.Коммуны 60 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3174, 2009, 61.262443, 55.075128, 'Россия, Челябинск, посёлок Сосновка, улица Коммуны, 50',
        'город Челябинск ул.Коммуны 60 поз.5', 'город Челябинск ул.Коммуны 60 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3175, 2010, 61.314743, 55.189511, 'Россия, Челябинск, проспект Победы, 356', 'город Челябинск пр.Победы  356',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3176, 2011, 61.37732, 55.147433, 'Россия, Челябинск, улица Худякова, 11', 'город Челябинск ул. Худякова  11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3177, 2012, 61.459129, 55.16704, 'Россия, Челябинск, улица Марченко, 22', 'город Челябинск ул. Марченко  22',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3178, 2013, 61.372712, 55.147165, 'Россия, Челябинск, улица Худякова, 19', 'город Челябинск ул. Худякова  19',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3179, 2014, 61.410989, 55.247575, 'Россия, Челябинск, Хлебозаводская улица, 15',
        'город Челябинск ул.Хлебозаводская 15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3180, 2015, 61.400227, 55.164819, 'Россия, Челябинск, улица Кирова, 92',
        'город Челябинск ул.Кирова напротив д. 92', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3181, 2016, 61.400299, 55.164325, 'Россия, Челябинск, улица Кирова, 96', 'город Челябинск ул.Кирова 96', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3182, 2017, 61.400227, 55.16396, 'Россия, Челябинск, улица Кирова, 100',
        'город Челябинск ул.Кирова  напротив д.100', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3183, 2018, 61.400245, 55.163739, 'Россия, Челябинск, улица Кирова, 102', 'город Челябинск ул.Кирова 102', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3184, 2019, 61.4002, 55.163168, 'Россия, Челябинск, улица Кирова, 104',
        'город Челябинск ул.Кирова напротив д.104 поз.1', 'город Челябинск ул.Кирова напротив д.104', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3185, 2020, 61.400774, 55.162809, 'Россия, Челябинск, улица Кирова',
        'город Челябинск пересеч.ул.Кирова (в центр) и ул.Коммуны', 'город Челябинск ул.Кирова / ул.Коммуны', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3186, 2021, 61.401116, 55.162499, 'Россия, Челябинск, улица Кирова, 163', 'город Челябинск ул.Кирова 163', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3187, 2022, 61.400254, 55.161939, 'Россия, Челябинск, улица Кирова, 110', 'город Челябинск ул.Кирова 110', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3188, 2023, 61.40144, 55.161707, 'Россия, Челябинск, улица Кирова, 167', 'город Челябинск ул.Кирова 167', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3189, 2024, 61.400478, 55.161461, 'Россия, Челябинск, улица Кирова, 112', 'город Челябинск ул.Кирова 112', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3190, 2025, 61.400559, 55.160663, 'Россия, Челябинск, улица Кирова, 114',
        'город Челябинск ул.Кирова напротив д.114', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3191, 2026, 61.4434, 55.199866, 'Россия, Челябинск, улица Героев Танкограда, 71Пс1',
        'город Челябинск ул. Героев Танкограда  71п', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3192, 2027, 0, 0, '', 'город Челябинск пр. Ленина  ост. "Публичная библиотека"  в сторону пл. Революции',
        'город Челябинск ост. "Публичная библиотека"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3193, 2028, 0, 0, '', 'город Челябинск пр. Ленина  ост. "Публичная библиотека"  в сторону ул. Красная',
        'город Челябинск ост. "Публичная библиотека"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3194, 2029, 61.376314, 55.156821, 'Россия, Челябинск, улица Энтузиастов, 9А',
        'город Челябинск ул.Энтузиастов 9а поз.1', 'город Челябинск ул.Энтузиастов 9а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3196, 2030, 61.376314, 55.156821, 'Россия, Челябинск, улица Энтузиастов, 9А',
        'город Челябинск ул.Энтузиастов 9а поз.2', 'город Челябинск ул.Энтузиастов 9а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3198, 2031, 61.376314, 55.156821, 'Россия, Челябинск, улица Энтузиастов, 9А',
        'город Челябинск ул.Энтузиастов 9а поз.3', 'город Челябинск ул.Энтузиастов 9а', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3200, 2032, 61.40011, 55.166444, 'Россия, Челябинск, улица Кирова, 86',
        'город Челябинск ул.Кирова 86 конец дома', 'город Челябинск ул.Кирова 86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3205, 2033, 61.40011, 55.166444, 'Россия, Челябинск, улица Кирова, 86',
        'город Челябинск ул.Кирова 86  начало дома', 'город Челябинск ул.Кирова 86', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3210, 2034, 61.388823, 55.16005, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пересечение пр. Ленина и Свердловского пр.  до перекрестка из центра',
        'город Челябинск  пр. Ленина / Свердловского пр.', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3211, 2035, 61.40011, 55.165482, 'Россия, Челябинск, улица Кирова, 90',
        'город Челябинск пересечение ул.Кирова  90 и ул.К.Маркса', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3214, 2036, 61.400901, 55.165127, 'Россия, Челябинск, улица Кирова, 147',
        'город Челябинск пересечение ул.Кирова 147 и ул.К.Маркса 109', 'город Челябинск ул.Кирова 147', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3215, 2037, 61.401035, 55.164176, 'Россия, Челябинск, улица Кирова, 161Б', 'город Челябинск ул.Кирова  161б/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3216, 2038, 61.401071, 55.163934, 'Россия, Челябинск, улица Кирова, 161А',
        'город Челябинск ул.Кирова 161а конец дома', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3217, 2039, 61.36831, 55.159043, 'Россия, Челябинск, проспект Ленина, 85',
        'город Челябинск пересечение пр. Ленина 85 и ул.Тернопольская  до перекрестка в центр',
        'город Челябинск пересечение пр. Ленина 85', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3218, 2040, 0, 0, '', 'город Челябинск пересечение ул.Кирова 161/а/1 и ул.Коммуны',
        'город Челябинск пересечение ул.Кирова 161/а/1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3228, 2041, 61.401116, 55.162499, 'Россия, Челябинск, улица Кирова, 163', 'город Челябинск ул.Кирова 163', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3229, 2042, 61.40144, 55.161707, 'Россия, Челябинск, улица Кирова, 167',
        'город Челябинск ул.Кирова 167 начало дома', 'город Челябинск ул.Кирова 167', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3234, 2043, 61.40144, 55.161707, 'Россия, Челябинск, улица Кирова, 167',
        'город Челябинск ул.Кирова 167  конец дома', 'город Челябинск ул.Кирова 167', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3239, 2044, 61.401071, 55.163934, 'Россия, Челябинск, улица Кирова, 161А',
        'город Челябинск ул.Кирова 161А  поз.1', 'город Челябинск ул.Кирова 161А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3242, 2045, 61.401071, 55.163934, 'Россия, Челябинск, улица Кирова, 161А',
        'город Челябинск ул.Кирова 161А  поз.2', 'город Челябинск ул.Кирова 161А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3245, 2046, 61.432517, 55.18587, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр.Победы и ул.Горького', 'город Челябинск пр.Победы / ул.Горького', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3246, 2047, 61.360358, 55.188036, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр.Победы и ул.Чайковского', 'город Челябинск  пр.Победы / ул.Чайковского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3247, 2048, 61.335291, 55.174506, 'Россия, Челябинск, улица Университетская Набережная',
        'город Челябинск пересечение ул. Университетская Набережная и ул. Молодогвардейцев',
        'город Челябинск ул. Университетская Набережная / ул. Молодогвардейцев', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3249, 2049, 61.453614, 55.166089, 'Россия, Челябинск, улица Первой Пятилетки, 17',
        'город Челябинск пересеч.ул.Г.Танкограда и ул.1-ой Пятилетки 17', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3250, 2050, 61.291163, 55.18985, 'Россия, Челябинск, Комсомольский проспект, 104',
        'город Челябинск Комсомольский пр. 104', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3251, 2051, 61.378919, 55.239727, 'Россия, Челябинск, Черкасская улица, 6',
        'город Челябинск ул.Черкасская напротив д.6 на разделит.газоне', 'город Челябинск ул.Черкасская  д.6', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3252, 2052, 61.440274, 55.187887, 'Россия, Челябинск, проспект Победы, 86', 'город Челябинск пр.Победы 86',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3253, 2053, 61.377194, 55.236704, 'Россия, Челябинск, Черкасская улица, 12', 'город Челябинск ул.Черкасская 12',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3254, 2054, 61.358976, 55.11372, 'Россия, Челябинск, улица Блюхера, 101', 'город Челябинск ул.Блюхера 101/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3255, 2055, 0, 0, '',
        'город Челябинск пересчение Троицкого тр. 15 и ул. Потребитеская  за перекрестком из центра',
        'город Челябинск Троицкого тр. 15', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3256, 2056, 61.331847, 55.178136, 'Россия, Челябинск, улица Братьев Кашириных, 109/1',
        'город Челябинск ул.Бр. Кашириных  109/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3257, 2057, 61.370511, 55.232402, 'Россия, Челябинск, Свердловский тракт, 8к1',
        'город Челябинск Свердловский тр. 8/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3258, 2058, 61.415687, 55.148549, 'Россия, Челябинск, улица Свободы, 100', 'город Челябинск  ул.Свободы 100',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3259, 2059, 61.351679, 55.107994, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск пересеч.ул.Блюхера и ул.Новоэлеваторная', 'город Челябинск ул.Блюхера / ул.Новоэлеваторная',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3260, 2060, 61.387471, 55.088898, 'Россия, Челябинск, Троицкий тракт, 23', 'город Челябинск Троицкий тр.  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3261, 2061, 0, 0, '', 'город Челябинск пересеч.Свердловского пр. 84 и ул.К.Либкнехта',
        'город Челябинск пересеч.Свердловского пр. 84', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3262, 2062, 61.387246, 55.155252, 'Россия, Челябинск, улица Карла Либкнехта, 9',
        'город Челябинск ул.К.Либкнехта 9', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3263, 2063, 61.387525, 55.11372, 'Россия, Челябинск, Троицкий тракт, 46', 'город Челябинск Троицкий тр.  46',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3264, 2064, 61.385018, 55.122519, 'Россия, Челябинск, Троицкий тракт, 9',
        'город Челябинск Троицкий тр.  напротив д.9', 'город Челябинск Троицкий тр.  д.9', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3265, 2065, 61.460172, 55.139983, 'Россия, Челябинск, Копейское шоссе, 52',
        'город Челябинск  Копейское шоссе 52  поз. 1', 'город Челябинск  Копейское шоссе 52 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3266, 2066, 61.460172, 55.139983, 'Россия, Челябинск, Копейское шоссе, 52',
        'город Челябинск  Копейское шоссе 52  поз. 2', 'город Челябинск  Копейское шоссе 52 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3267, 2067, 61.460172, 55.139983, 'Россия, Челябинск, Копейское шоссе, 52',
        'город Челябинск  Копейское шоссе 52  поз. 3', 'город Челябинск  Копейское шоссе 52 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3268, 2068, 61.427383, 55.167344, 'Россия, Челябинск, улица Труда, 3Б', 'город Челябинск ул.Труда 3Б', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3269, 2069, 61.371723, 55.133447, 'Россия, Челябинск, улица Блюхера, 49', 'город Челябинск ул.Блюхера 49', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3270, 2070, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск  ул.Блюхера 150м до остановки "АМЗ" (в центр)', 'город Челябинск  остановка "АМЗ"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3271, 2071, 61.483734, 55.204413, 'Россия, Челябинск, садовое товарищество Тракторосад-1',
        'город Челябинск  Бродокалмакский тр. 150м от ост "Тракторосад-1"', 'город Челябинск ост "Тракторосад-1"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3272, 2072, 61.31477, 55.236201, 'Россия, Челябинск, коттеджный поселок Шагол',
        'город Челябинск Свердловский тр.  300 м до поворота на пос. Шагол', 'город Челябинск пос. Шагол', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3273, 2073, 61.436896, 55.147942, 'Россия, Челябинск, улица Барбюса, 2', 'город Челябинск ул.Барбюса 2', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3274, 2074, 61.304062, 55.182824, 'Россия, Челябинск, улица 40-летия Победы, 33',
        'город Челябинск ул.40 лет Победы 33', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3275, 2075, 61.400919, 55.165693, 'Россия, Челябинск, улица Кирова, 143', 'город Челябинск ул.Кирова 143', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3276, 2076, 61.377658, 55.176289, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение ул. Бр. Кашириных (из центра) и ул. Спорта',
        'город Челябинск ул. Бр. Кашириных ул. Спорта', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3277, 2077, 61.402554, 55.159897, 'Россия, Челябинск',
        'город Челябинск Свердловский тр.  выезд с ЧМЗ  справа в центр', 'город Челябинск  ЧМЗ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3278, 2078, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск Свердловский тр.  въезд на ЧМЗ',
        'город Челябинск  ЧМЗ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3279, 2079, 61.401512, 55.172465, 'Россия, Челябинск, улица Кирова, 25', 'город Челябинск ул.Кирова 25', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3281, 2080, 61.385683, 55.108689, 'Россия, Челябинск, Троицкий тракт, 13', 'город Челябинск Троицкий тр.  13',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3282, 2081, 61.291746, 55.180629, 'Россия, Челябинск, улица Чичерина, 30', 'город Челябинск ул. Чичерина 30',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3283, 2082, 61.381614, 55.163199, 'Россия, Челябинск, улица Энгельса, 49', 'город Челябинск ул. Энгельса  49',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3284, 2083, 61.374724, 55.161347, 'Россия, Челябинск, улица Энтузиастов, 2',
        'город Челябинск ул. Энтузиастов 2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3286, 2084, 61.375469, 55.154167, 'Россия, Челябинск, улица Энтузиастов, 18',
        'город Челябинск ул. Энтузиастов 18', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3288, 2085, 61.376368, 55.224223, 'Россия, Челябинск, Свердловский тракт, 16Б',
        'город Челябинск Свердловский тр. 16Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3289, 2086, 61.385926, 55.108045, 'Россия, Челябинск, Троицкий тракт, 13к1',
        'город Челябинск Троицкий тр. 13/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3290, 2087, 61.318741, 55.077344, 'Россия, Челябинск, Уфимский тракт, 1', 'город Челябинск Уфимский тр.  1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3291, 2088, 61.47768, 55.196588, 'Россия, Челябинск, улица Мамина, 7', 'город Челябинск ул.Мамина 7', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3292, 2089, 61.486321, 55.1739, 'Россия, Челябинск, улица Хохрякова, 30',
        'город Челябинск ул.Хохрякова 30 (в центр)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3293, 2090, 61.431596, 55.167909, 'Россия, Челябинск, улица Первой Пятилетки, 57',
        'город Челябинск ул.Первой Пятилетки 57', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3294, 2091, 61.366199, 55.159995, 'Россия, Челябинск, проспект Ленина, 80', 'город Челябинск пр. Ленина 80',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3296, 2092, 61.363648, 55.150345, 'Россия, Челябинск, Лесопарковая улица, 6',
        'город Челябинск ул. Лесопарковая  6', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3297, 2093, 61.389573, 55.158853, 'Россия, Челябинск, Свердловский проспект, 63',
        'город Челябинск Свердловский пр. 63', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3298, 2094, 61.389025, 55.150844, 'Россия, Челябинск, улица Воровского, 28/1',
        'город Челябинск ул.Воровского 28/1 ', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3299, 2095, 61.427158, 55.159938, 'Россия, Челябинск, проспект Ленина, 21Б',
        'город Челябинск  пр.Ленина  21 БД "Спиридонов" (АЗС № 422)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3301, 2096, 61.425973, 55.160962, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск  пр.Ленина  БД "Видгоф" (АЗС 424)', 'город Челябинск  пр.Ленина  БД "Видгоф"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3304, 2097, 61.358473, 55.186638, 'Россия, Челябинск, улица Чайковского',
        'город Челябинск  пересеч.ул.Чайковского и Комсомольского пр. (АЗС 432)',
        'город Челябинск  ул.Чайковского / Комсомольского пр. (АЗС 432)', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3305, 2098, 61.362417, 55.257612, 'Россия, Челябинск, Дачная улица, 37',
        'город Челябинск  пересеч.ул.Дачная 37/ ул.Дубравная (АЗС № 435)', 'город Челябинск  ул.Дачная 37', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3306, 2099, 61.389446, 55.151841, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск  пересеч. Свердловского пр. и ул. Воровского (АЗС №  74412)',
        'город Челябинск   Свердловского пр. / ул. Воровского', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3307, 2100, 61.379916, 55.151276, 'Россия, Челябинск, улица Энгельса, 46', 'город Челябинск ул. Энгельса  46',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3308, 2101, 61.452635, 55.140883, 'Россия, Челябинск, Копейское шоссе, 39А',
        'город Челябинск Копейское шоссе 39А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3309, 2102, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр. 34  поз.1', 'город Челябинск Комсомольский пр. 34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3310, 2103, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр. 34  поз.2', 'город Челябинск Комсомольский пр. 34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3311, 2104, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр. 34  поз.3', 'город Челябинск Комсомольский пр. 34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3312, 2105, 61.345061, 55.195277, 'Россия, Челябинск, Комсомольский проспект, 34',
        'город Челябинск Комсомольский пр. 34  поз.4', 'город Челябинск Комсомольский пр. 34', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3313, 2106, 61.387264, 55.219411, 'Россия, Челябинск, Свердловский тракт, 3В',
        'город Челябинск Свердловский тр. 3В', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3314, 2107, 61.395089, 55.157423, 'Россия, Челябинск, улица Воровского, 6', 'город Челябинск ул.Воровского 6',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3315, 2108, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск ул.Артиллерийская 136  у входа со стороны ул. Артиллерийской',
        'город Челябинск ул.Артиллерийская 136', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3316, 2109, 61.283994, 55.186679, 'Россия, Челябинск, Комсомольский проспект, 118',
        'город Челябинск Комсомольский пр. 118', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3317, 2110, 61.398942, 55.159861, 'Россия, Челябинск, проспект Ленина, 55А', 'город Челябинск пр.Ленина 55а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3319, 2111, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск ул.Артиллерийская 136  у входа со стороны а/д Меридиан',
        'город Челябинск ул.Артиллерийская 136', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3320, 2112, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7 (главный вход комплекса напротив ул.Молодогвардейцев  7А)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3321, 2113, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7 (вход напротив ул.Солнечная)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3322, 2114, 61.445313, 55.1965, 'Россия, Челябинск, Механическая улица, 14',
        'город Челябинск ул.Механическая 14', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3323, 2115, 61.445313, 55.1965, 'Россия, Челябинск, Механическая улица, 14',
        'город Челябинск ул.Механическая 14', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3324, 2116, 61.390606, 55.082503, 'Россия, Челябинск, улица Игуменка, 181',
        'город Челябинск ул.Игуменка 181  поз.1', 'город Челябинск ул.Игуменка 181', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3325, 2117, 61.390606, 55.082503, 'Россия, Челябинск, улица Игуменка, 181',
        'город Челябинск ул.Игуменка 181  поз.2', 'город Челябинск ул.Игуменка 181', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3326, 2118, 61.404215, 55.159295, 'Россия, Челябинск, площадь Революции, 1',
        'город Челябинск площадь Революции 1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3327, 2119, 61.331901, 55.193885, 'Россия, Челябинск, улица Молодогвардейцев, 34',
        'город Челябинск ул.Молодогвардейцев  34', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3328, 2120, 61.31698, 55.178187, 'Россия, Челябинск, улица Братьев Кашириных, 129Б',
        'город Челябинск ул.Бр.Кашириных 129б  поз.1', 'город Челябинск ул.Бр.Кашириных 129б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3329, 2121, 61.31698, 55.178187, 'Россия, Челябинск, улица Братьев Кашириных, 129Б',
        'город Челябинск ул.Бр.Кашириных 129б  поз.2', 'город Челябинск ул.Бр.Кашириных 129б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3330, 2122, 61.31698, 55.178187, 'Россия, Челябинск, улица Братьев Кашириных, 129Б',
        'город Челябинск ул.Бр.Кашириных 129б  поз.3', 'город Челябинск ул.Бр.Кашириных 129б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3331, 2123, 61.31698, 55.178187, 'Россия, Челябинск, улица Братьев Кашириных, 129Б',
        'город Челябинск ул.Бр.Кашириных 129б  поз.4', 'город Челябинск ул.Бр.Кашириных 129б', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3332, 2124, 61.383455, 55.14787, 'Россия, Челябинск, улица Воровского, 38Б',
        'город Челябинск ул.Воровского 38б  опора б/н', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3333, 2125, 61.375209, 55.147361, 'Россия, Челябинск, улица Худякова, 13',
        'город Челябинск ул.Худякова 13 (на уровне середины дома)  опора б/н', 'город Челябинск ул.Худякова 13', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3334, 2126, 61.385351, 55.145426, 'Россия, Челябинск, улица Доватора, 42',
        'город Челябинск ул.Доватора 42  опора б/н', 'город Челябинск ул.Доватора 42', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3335, 2127, 61.378793, 55.147917, 'Россия, Челябинск, улица Худякова, 4',
        'город Челябинск ул.Худякова 4  опора б/н', 'город Челябинск ул.Худякова 4', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3336, 2128, 61.379736, 55.145514, 'Россия, Челябинск, улица Воровского, 46',
        'город Челябинск пересечение ул.Воровского 46 и ул.Образцова  опора б/н',
        'город Челябинск пересечение ул.Воровского 46', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3337, 2129, 61.389624, 55.167441, 'Россия, Челябинск, Свердловский проспект',
        'город Челябинск пересечение Свердловского пр. и ул.Труда', 'город Челябинск Свердловского пр. / ул.Труда', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3339, 2130, 61.381281, 55.168203, 'Россия, Челябинск, улица Энгельса, 21к1',
        'город Челябинск пересечение ул.Труда (из центра) и ул. Энгельса  21/1', 'город Челябинск ул. Энгельса  21/1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3348, 2131, 61.394145, 55.183338, 'Россия, Челябинск, Каслинская улица',
        'город Челябинск пересечение ул.Бр. Кашириных и ул. Каслинская (в центр)',
        'город Челябинск ул.Бр. Кашириных / ул. Каслинская (в центр)', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3349, 2132, 61.303281, 55.171822, 'Россия, Челябинск, улица Братьев Кашириных, 135к1',
        'город Челябинск ул.Бр.Кашириных 135', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3350, 2133, 61.472092, 55.121191, 'Россия, Челябинск, улица Машиностроителей, 20',
        'город Челябинск ул.Машиностроителей 20', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3351, 2134, 61.46955, 55.11717, 'Россия, Челябинск, улица Машиностроителей, 34',
        'город Челябинск ул.Машиностроителей 34', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3352, 2135, 61.307548, 55.172177, 'Россия, Челябинск, улица Чичерина, 43', 'город Челябинск ул.Чичерина 43',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3353, 2136, 61.442115, 55.142015, 'Россия, Челябинск, Ленинский район, улица Гагарина, 10',
        'город Челябинск ул.Гагарина 10', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3355, 2137, 61.396715, 55.074881, 'Россия, Челябинск, посёлок Смолино, улица Гагарина, 11',
        'город Челябинск ул.Гагарина  между д. 11 и д. 13', 'город Челябинск ул.Гагарина д. 11', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3358, 2138, 61.394487, 55.165575, 'Россия, Челябинск, улица Маркса, 80', 'город Челябинск ул. Карла Маркса  80',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3359, 2139, 61.296939, 55.183251, 'Россия, Челябинск, проспект Победы, 321', 'город Челябинск пр.Победы  321',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3360, 2140, 61.296786, 55.185934, 'Россия, Челябинск, проспект Победы, 384', 'город Челябинск пр.Победы  384',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3361, 2141, 61.479288, 55.10942, 'Россия, Челябинск, Новороссийская улица, 40',
        'город Челябинск ул.Новороссийская  между д.40 и д.42', 'город Челябинск ул.Новороссийская  д.40', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3364, 2142, 61.375667, 55.139025, 'Россия, Челябинск, улица Воровского, 81', 'город Челябинск ул.Воровского 81',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3365, 2143, 61.380191, 55.175186, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересеч.ул.Бр.Кашириных(в центр) и ул.Краснознаменная',
        'город Челябинск ул.Бр.Кашириных / ул.Краснознаменная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3366, 2144, 61.375487, 55.040709, 'Россия, Челябинск, посёлок Новосинеглазово',
        'город Челябинск Троицкий тр.  перед поворотом в пос. Новосинеглазово', 'город Челябинск пос. Новосинеглазово',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3367, 2145, 61.364735, 55.123924, 'Россия, Челябинск, улица Блюхера, 81', 'город Челябинск ул.Блюхера 81', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3369, 2146, 61.406874, 55.184161, 'Россия, Челябинск, проспект Победы, 149А',
        'город Челябинск пересеч.пр.Победы 149А и ул.Болейко', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3373, 2147, 61.375793, 55.177437, 'Россия, Челябинск, Парашютная улица, 41',
        'город Челябинск пересеч.ул.Бр.Кашириных и ул.Парашютная 41', 'город Челябинск ул.Парашютная 41', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3374, 2148, 61.386959, 55.098042, 'Россия, Челябинск, Троицкий тракт, 35',
        'город Челябинск Троицкий тр. 35к/1  напротив заводоуправления  поз. 1', 'город Челябинск Троицкий тр. 35к/1 ',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3375, 2149, 61.342456, 55.109477, 'Россия, Челябинск, посёлок АМЗ',
        'город Челябинск ул.Блюхера напротив Заводоуправления АМЗ', 'город Челябинск АМЗ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3376, 2150, 61.386959, 55.098042, 'Россия, Челябинск, Троицкий тракт, 35',
        'город Челябинск Троицкий тр. 35к/1  напротив заводоуправления  поз. 2', 'город Челябинск Троицкий тр. 35к/1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3377, 2151, 61.363926, 55.120387, 'Россия, Челябинск, улица Блюхера, 93',
        'город Челябинск ул.Блюхера напротив д.93', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3378, 2152, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина 76 ост. "ЮУрГУ"  поз.1', 'город Челябинск пр. Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3381, 2153, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина 76 ост. "ЮУрГУ"  поз.2', 'город Челябинск пр. Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3384, 2154, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина 76 ост. "ЮУрГУ"  поз.3', 'город Челябинск пр. Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3387, 2155, 61.37016, 55.160478, 'Россия, Челябинск, проспект Ленина, 76',
        'город Челябинск пр. Ленина 76 ост. "ЮУрГУ"  поз.4', 'город Челябинск пр. Ленина 76', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3390, 2156, 61.380985, 55.157284, 'Россия, Челябинск, улица Энгельса, 34', 'город Челябинск ул. Энгельса  34',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3391, 2157, 61.310377, 55.17154, 'Россия, Челябинск, улица Университетская Набережная, 64',
        'город Челябинск ул. Университетская Набережная  64', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3392, 2158, 61.283212, 55.207783, 'Россия, Челябинск, улица Скульптора Головницкого, 16',
        'город Челябинск ул.Бейвеля/ ул.Скульптора Головницкого  16', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3394, 2159, 61.447011, 55.148302, 'Россия, Челябинск, Копейское шоссе, 82',
        'город Челябинск Копейское шоссе  82', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3395, 2160, 61.447326, 55.159954, 'Россия, Челябинск, проспект Ленина, 11', 'город Челябинск пр.Ленина 11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3397, 2161, 61.359866, 55.151009, 'Россия, Челябинск, Лесопарковая улица, 6А/1',
        'город Челябинск ул. Лесопарковая/ поворот к ул. Лесопарковая 6А/1', 'город Челябинск ул. Лесопарковая 6А/1',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3398, 2162, 61.400523, 55.159162, 'Россия, Челябинск, улица Кирова, 116',
        'город Челябинск площадь Революции/ ул.Кирова 116', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3400, 2163, 61.298951, 55.193443, 'Россия, Челябинск, Комсомольский проспект, 90',
        'город Челябинск Комсомольский пр. 90', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3401, 2164, 61.35788, 55.206899, 'Россия, Челябинск, Автодорожная улица, 9А',
        'город Челябинск ул.Автодорожная 9а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3402, 2165, 61.49102, 55.140291, 'Россия, Челябинск, Енисейская улица, 48', 'город Челябинск ул.Енисейская 48',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3403, 2166, 61.461241, 55.170691, 'Россия, Челябинск, улица Марченко, 18', 'город Челябинск ул.Марченко 18',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3404, 2167, 61.387947, 55.177298, 'Россия, Челябинск, Свердловский проспект, 39',
        'город Челябинск Свердловский пр. 39', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3405, 2168, 61.387615, 55.258633, 'Россия, Челябинск, улица Богдана Хмельницкого, 18',
        'город Челябинск ул.Б.Хмельницкого 18', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3406, 2169, 61.348008, 55.179421, 'Россия, Челябинск, улица Братьев Кашириных, 68А',
        'город Челябинск ул.Бр.Кашириных 68А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3407, 2170, 61.429341, 55.132592, 'Россия, Челябинск, улица Дзержинского, 105',
        'город Челябинск ул.Дзержинского 105', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3408, 2171, 61.4131, 55.263809, 'Россия, Челябинск, 2-я Павелецкая улица, 14',
        'город Челябинск ул.2-ая Павелецкая 14', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3409, 2172, 61.334515, 55.195015, 'Россия, Челябинск, Комсомольский проспект, 48',
        'город Челябинск пересеч.Комсомольского пр. 48 и ул.Молодогвардейцев', 'город Челябинск Комсомольского пр. 48',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3410, 2173, 61.387875, 55.189933, 'Россия, Челябинск, Свердловский проспект, 7',
        'город Челябинск пересеч.Свердловского пр. 7 и Комсомольского пр.', 'город Челябинск Свердловского пр. 7', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3411, 2174, 61.436663, 55.130878, 'Россия, Челябинск, улица Дзержинского, 91',
        'город Челябинск пересеч.ул.Гагарина и ул. Дзержинского 91', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3413, 2175, 61.425973, 55.160962, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пересечение пр.Ленина (из центра) и ул. Горького  Комсомольская площадь',
        'город Челябинск пр.Ленина / ул. Горького  Комсомольская площадь', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3414, 2176, 61.376008, 55.245917, 'Россия, Челябинск, шоссе Металлургов, 43',
        'город Челябинск пересечение ул.Черкасская и ш.Металлургов 43(через дорогу)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3416, 2177, 61.414456, 55.140507, 'Россия, Челябинск, Привокзальная площадь',
        'город Челябинск Привокзальная пл. (ТК "Синегорье")', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3417, 2178, 61.286572, 55.186504, 'Россия, Челябинск, улица Чичерина, 22/5',
        'город Челябинск ул. Чичерина 22/5 и Комсомольского пр.', 'город Челябинск ул. Чичерина 22/5', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3418, 2179, 61.384965, 55.124691, 'Россия, Челябинск, Троицкий тракт, 1/1', 'город Челябинск Троицкий тр.  1/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3419, 2180, 61.386815, 55.198222, 'Россия, Челябинск, Свердловский тракт, 7/1',
        'город Челябинск Свердловский тр.  7/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3420, 2181, 61.351853, 55.241935, 'Россия, Челябинск, 2-я Индивидуальная улица',
        'город Челябинск пересеч. Свердловского тр. и ул. Индивидуальная',
        'город Челябинск Свердловского тр. / ул. Индивидуальная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3421, 2182, 61.402554, 55.159897, 'Россия, Челябинск', 'город Челябинск Шершневское водохранилище  пост ГИБДД',
        'город Челябинск Шершневское водохранилище', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3422, 2183, 61.329, 55.146013, 'Россия, Челябинск, улица Худякова, 22К1',
        'город Челябинск ул. Худякова  напротив д. 22к1', 'город Челябинск ул. Худякова  д. 22к1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3425, 2184, 61.443607, 55.147376, 'Россия, Челябинск, улица Харлова, 2',
        'город Челябинск пересеч. Копейского шоссе и ул. Харлова  2а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3431, 2185, 61.40259, 55.134785, 'Россия, Челябинск, улица Фёдорова, 1А', 'город Челябинск ул.Федорова 1А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3432, 2186, 61.430374, 55.161527, 'Россия, Челябинск, проспект Ленина, 26к1', 'город Челябинск пр.Ленина 26/1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3434, 2187, 61.426763, 55.147618, 'Россия, Челябинск, автодорога Меридиан',
        'город Челябинск пересечение ул.Тухачевского и а/д Меридиан',
        'город Челябинск ул.Тухачевского / АВТОДОРОГА Меридиан', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3435, 2188, 61.441091, 55.116917, 'Россия, Челябинск, Новороссийская улица, 122к1',
        'город Челябинск ул.Новороссийская 122/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3436, 2189, 61.372689, 55.134502, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск пересечение ул.Блюхера и ул.Гоголя', 'город Челябинск ул.Блюхера / ул.Гоголя', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3437, 2190, 61.380522, 55.16764, 'Россия, Челябинск, улица Труда',
        'город Челябинск пересечение ул.Труда и ул. Энгельса', 'город Челябинск ул.Труда / ул. Энгельса', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3438, 2191, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск ул.Артиллерийская  136 (у входа со стороны а/д Меридиан)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3439, 2192, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск ул.Артиллерийская  136 (у входа со стороны ул. Артиллерийская)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3440, 2193, 61.430922, 55.163507, 'Россия, Челябинск, Артиллерийская улица, 136',
        'город Челябинск ул.Артиллерийская  136 (со стороны ул. Ловина)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3441, 2194, 61.412273, 55.157757, 'Россия, Челябинск, улица Свободы, 78', 'город Челябинск ул.Свободы 78', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3442, 2195, 61.386249, 55.109755, 'Россия, Челябинск, Троицкий тракт, 11', 'город Челябинск Троицкий тр. 11',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3443, 2196, 61.387552, 55.112402, 'Россия, Челябинск, Троицкий тракт, 52', 'город Челябинск Троицкий тр. 52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3444, 2197, 61.476108, 55.159717, 'Россия, Челябинск, проспект Ленина, 2А/2',
        'город Челябинск пр.Ленина 2а/2 (район главной проходной ЧТЗ)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3446, 2198, 61.374562, 55.170784, 'Россия, Челябинск, улица Труда, 166', 'город Челябинск ул. Труда  166', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3447, 2199, 0, 0, '', 'город Челябинск ул.Молодогвардейцев  поворот к дому № 109 по ул.Бр. Кашириных',
        'город Челябинск  по ул.Бр. Кашириных № 109', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3448, 2200, 61.471517, 55.170624, 'Россия, Челябинск, улица Танкистов, 177А/2',
        'город Челябинск ул.Танкистов 177А/2 поз.1', 'город Челябинск ул.Танкистов 177А/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3458, 2201, 61.471517, 55.170624, 'Россия, Челябинск, улица Танкистов, 177А/2',
        'город Челябинск ул.Танкистов 177А/2 поз.2', 'город Челябинск ул.Танкистов 177А/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3468, 2202, 61.471517, 55.170624, 'Россия, Челябинск, улица Танкистов, 177А/2',
        'город Челябинск ул.Танкистов 177А/2 поз.3', 'город Челябинск ул.Танкистов 177А/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3478, 2203, 61.471517, 55.170624, 'Россия, Челябинск, улица Танкистов, 177А/2',
        'город Челябинск ул.Танкистов 177А/2 поз.4', 'город Челябинск ул.Танкистов 177А/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3488, 2204, 61.471517, 55.170624, 'Россия, Челябинск, улица Танкистов, 177А/2',
        'город Челябинск ул.Танкистов 177А/2 поз.5', 'город Челябинск ул.Танкистов 177А/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3498, 2205, 61.40959, 55.253976, 'Россия, Челябинск, шоссе Металлургов',
        'город Челябинск пересечение ш.Металлургов и ул.Строительная',
        'город Челябинск ш.Металлургов / ул.Строительная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3499, 2206, 61.391244, 55.252894, 'Россия, Челябинск, улица Жукова',
        'город Челябинск пересечение ул. Жукова и ул.Социалистическая',
        'город Челябинск ул. Жукова / ул.Социалистическая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3500, 2207, 61.440777, 55.134003, 'Россия, Челябинск, улица Агалакова, 38', 'город Челябинск ул.Агалакова 38',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3501, 2208, 61.290183, 55.184901, 'Россия, Челябинск, улица Чичерина, 15',
        'город Челябинск ул.Чичерина  напротив д.15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3502, 2209, 61.452698, 55.18323, 'Россия, Челябинск, улица Бажова, 46', 'город Челябинск ул.Бажова 46', null,
        null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3504, 2210, 61.44711, 55.180444, 'Россия, Челябинск, улица Героев Танкограда, 37',
        'город Челябинск ул.Героев Танкограда 37', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3505, 2211, 61.411447, 55.160654, 'Россия, Челябинск, проспект Ленина',
        'город Челябинск пересечение пр. Ленина и ул. Свободы', 'город Челябинск пр. Ленина / ул. Свободы', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3506, 2212, 61.394855, 55.159794, 'Россия, Челябинск, проспект Ленина, 61',
        'город Челябинск пересечение пр.Ленина  61 и ул.Васенко', 'город Челябинск пересечение пр.Ленина  61', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3507, 2213, 61.410234, 55.160278, 'Россия, Челябинск, проспект Ленина, 45',
        'город Челябинск пересечение пр.Ленина  45 и ул.Свободы', 'город Челябинск пересечение пр.Ленина  45', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3508, 2214, 61.40568, 55.160108, 'Россия, Челябинск, проспект Ленина, 53',
        'город Челябинск пересечение пр.Ленина  53 и ул.Цвиллинга', 'город Челябинск пересечение пр.Ленина  53', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3509, 2215, 61.412633, 55.160324, 'Россия, Челябинск, проспект Ленина, 43',
        'город Челябинск пересечение пр.Ленина  43 и ул.Свободы', 'город Челябинск пересечение пр.Ленина  43', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3510, 2216, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124', 'город Челябинск пр. Победы  124',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3511, 2217, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124',
        'город Челябинск пр. Победы  124  поз. 1', 'город Челябинск пр. Победы  124', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3513, 2218, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124',
        'город Челябинск пр. Победы  124  поз. 2', 'город Челябинск пр. Победы  124', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3515, 2219, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124',
        'город Челябинск пр. Победы  124  поз. 3', 'город Челябинск пр. Победы  124', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3517, 2220, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124',
        'город Челябинск пр. Победы  124  поз. 4', 'город Челябинск пр. Победы  124', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3519, 2221, 61.296382, 55.168526, 'Россия, Челябинск, улица Братьев Кашириных, 147',
        'город Челябинск ул.Бр.Кашириных 147  поз.1', 'город Челябинск ул.Бр.Кашириных 147', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3520, 2222, 61.296382, 55.168526, 'Россия, Челябинск, улица Братьев Кашириных, 147',
        'город Челябинск ул.Бр.Кашириных 147  поз.2', 'город Челябинск ул.Бр.Кашириных 147', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3521, 2223, 61.359578, 55.149403, 'Россия, Челябинск, Лесопарковая улица, 6А',
        'город Челябинск ул. Лесопарковая 6Астр.', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3522, 2224, 61.283356, 55.172717, 'Россия, Челябинск, улица Татищева, 264',
        'город Челябинск ул.Татищева 264  поз. 1', 'город Челябинск ул.Татищева 264', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3523, 2225, 61.283356, 55.172717, 'Россия, Челябинск, улица Татищева, 264',
        'город Челябинск ул.Татищева 264  поз. 2', 'город Челябинск ул.Татищева 264', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3524, 2226, 61.283356, 55.172717, 'Россия, Челябинск, улица Татищева, 264',
        'город Челябинск ул.Татищева 264  поз. 3', 'город Челябинск ул.Татищева 264', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3525, 2227, 61.28624, 55.193283, 'Россия, Челябинск, улица Аношкина, 12',
        'город Челябинск ул.Аношкина 12 поз. 1', 'город Челябинск ул.Аношкина 12', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3526, 2228, 61.28624, 55.193283, 'Россия, Челябинск, улица Аношкина, 12',
        'город Челябинск ул.Аношкина 12 поз. 2', 'город Челябинск ул.Аношкина 12', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3527, 2229, 61.28624, 55.193283, 'Россия, Челябинск, улица Аношкина, 12',
        'город Челябинск ул.Аношкина 12 поз. 3', 'город Челябинск ул.Аношкина 12', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3528, 2230, 61.284578, 55.191279, 'Россия, Челябинск, улица 40-летия Победы, 4',
        'город Челябинск ул.40 летия Победы 4Б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3529, 2231, 61.376197, 55.175247, 'Россия, Челябинск, улица Братьев Кашириных, 75',
        'город Челябинск ул.Бр.Кашириных 75', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3530, 2232, 61.376197, 55.175247, 'Россия, Челябинск, улица Братьев Кашириных, 75',
        'город Челябинск ул.Бр.Кашириных 75 поз.1', 'город Челябинск ул.Бр.Кашириных 75', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3531, 2233, 61.376197, 55.175247, 'Россия, Челябинск, улица Братьев Кашириных, 75',
        'город Челябинск ул.Бр.Кашириных 75 поз.2', 'город Челябинск ул.Бр.Кашириных 75', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3532, 2234, 61.376197, 55.175247, 'Россия, Челябинск, улица Братьев Кашириных, 75',
        'город Челябинск ул.Бр.Кашириных 75 поз.1', 'город Челябинск ул.Бр.Кашириных 75', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3533, 2235, 61.376197, 55.175247, 'Россия, Челябинск, улица Братьев Кашириных, 75',
        'город Челябинск ул.Бр.Кашириных 75 поз.2', 'город Челябинск ул.Бр.Кашириных 75', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3534, 2236, 61.281847, 55.172707, 'Россия, Челябинск, улица Татищева, 262', 'город Челябинск ул. Татищева  262',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3535, 2237, 61.334515, 55.195015, 'Россия, Челябинск, Комсомольский проспект, 48',
        'город Челябинск Комсомольский пр.  48', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3536, 2238, 61.367115, 55.147104, 'Россия, Челябинск, улица Худякова, 31', 'город Челябинск ул. Худякова  31',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3537, 2239, 61.333518, 55.179483, 'Россия, Челябинск, улица Молодогвардейцев, 57к1',
        'город Челябинск пересечение ул. Братьев Кашириных (из центра) и ул. Молодогвардейцев  57/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3539, 2240, 61.262443, 55.075128, 'Россия, Челябинск, посёлок Сосновка, улица Коммуны, 50',
        'город Челябинск ул. Коммуны  58  поз. 6', 'город Челябинск ул. Коммуны  58', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3540, 2241, 61.360746, 55.117504, 'Россия, Челябинск, улица Кузнецова, 1А',
        'город Челябинск ул. Блюхера/ ул. Кузнецова  1А', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3543, 2242, 61.368319, 55.130899, 'Россия, Челябинск, улица Блюхера, 59',
        'город Челябинск ул. Блюхера  напротив д. 59', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3545, 2243, 61.439986, 55.144799, 'Россия, Челябинск, КБС', 'город Челябинск ул. Харлова  ООТ "КБС"  из центра',
        'город Челябинск ООТ "КБС"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3554, 2244, 0, 0, 'Россия, Южно-Уральская железная дорога, станция Кир-Завод',
        'город Челябинск Свердловский тр.  ООТ "Лакокрасочный завод"  в город',
        'город Челябинск ООТ "Лакокрасочный завод"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3564, 2245, 61.453084, 55.142195, 'Россия, Челябинск, Копейское шоссе, 58',
        'город Челябинск Копейское ш.  58  в центр  ООТ "Профилакторий"', 'город Челябинск Копейское ш.  58', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3565, 2246, 61.451979, 55.145442, 'Россия, Челябинск, Копейское шоссе, 64',
        'город Челябинск Копейское ш.  64/1  ООТ "Профилакторий"', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3566, 2247, 61.454189, 55.142422, 'Россия, Челябинск, Енисейская улица, 1',
        'город Челябинск ул. Енисейская  1  ООТ "Профилакторий"', 'город Челябинск ул. Енисейская  1', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3567, 2248, 0, 0, '', 'город Челябинск пересечение Свердловского пр.  28А и пр. Победы',
        'город Челябинск пересечение Свердловского пр.  28А', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3568, 2249, 61.387633, 55.174126, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск пересечение Свердловского пр. и ул. Братьев Кашириных', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3569, 2250, 61.405033, 55.138346, 'Россия, Челябинск, улица Доватора, 8', 'город Челябинск ул. Доватора  8',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3571, 2251, 61.30311, 55.173627, 'Россия, Челябинск, улица Братьев Кашириных, 126',
        'город Челябинск пересечение ул. Братьев Кашириных 126/1 и ул. Чичерина',
        'город Челябинск пересечение ул. Братьев Кашириных 126/1 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3572, 2252, 61.364705, 55.125395, 'Россия, Челябинск, улица Блюхера',
        'город Челябинск Блюхера (в центр) 50 м от пересечения с ул. Мебельная (слева)',
        'город Челябинск Блюхера / ул. Мебельная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3574, 2253, 61.356892, 55.147171, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова (из центра)  поворот на городской пляж', 'город Челябинск ул. Худякова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3580, 2254, 61.441271, 55.160108, 'Россия, Челябинск, Комсомольская площадь',
        'город Челябинск пр. Ленина  Комсомольская площадь  за остановкой  из центра', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3584, 2255, 61.363994, 55.186599, 'Россия, Челябинск, проспект Победы',
        'город Челябинск пересечение пр. Победы и ул. Северо-Крымская',
        'город Челябинск пр. Победы / ул. Северо-Крымская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3585, 2256, 61.365391, 55.147419, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова (в центр)  700 м до ул. Лесопарковая',
        'город Челябинск ул. Худякова / ул. Лесопарковая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3586, 2257, 61.372504, 55.178107, 'Россия, Челябинск, улица Братьев Кашириных',
        'город Челябинск ул. Братьев Кашириных  300 м от ул. Косарева в центр за АЗС',
        'город Челябинск ул. Братьев Кашириных  / ул. Косарева', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3587, 2258, 61.358318, 55.1756, 'Россия, Челябинск, Северо-Крымская улица',
        'город Челябинск пересечение ул. Северо-Крымская и ул. Университетская набережная',
        'город Челябинск ул. Северо-Крымская / ул. Университетская набережная', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3588, 2259, 61.411456, 55.148169, 'Россия, Челябинск, улица Цвиллинга, 64к1',
        'город Челябинск пересечение ул. Цвиллинга  64 и ул. Перовской',
        'город Челябинск пересечение ул. Цвиллинга  64 ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3589, 2260, 61.440319, 55.208098, 'Россия, Челябинск, улица Героев Танкограда',
        'город Челябинск пересечение ул. Героев Танкограда (в центр) и ул. Валдайская',
        'город Челябинск ул. Героев Танкограда / ул. Валдайская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3590, 2261, 61.365391, 55.147419, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова (в центр)  1000 м до ул. Лесопарковая',
        'город Челябинск ул. Худякова / ул. Лесопарковая', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3591, 2262, 61.338432, 55.194147, 'Россия, Челябинск, Комсомольский проспект, 41',
        'город Челябинск Комсомольский пр.  41 (конец дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3592, 2263, 61.329035, 55.194085, 'Россия, Челябинск, Комсомольский проспект, 47',
        'город Челябинск Комсомольский пр.  47 (начало дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3593, 2264, 61.356892, 55.147171, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова (в центр)  1280 м до ул. Лесопарковая', 'город Челябинск ул. Худякова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3594, 2265, 61.352859, 55.195061, 'Россия, Челябинск, Комсомольский проспект, 28',
        'город Челябинск Комсомольский пр.  28 (конец дома)', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3595, 2266, 61.356892, 55.147171, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова (в центр)  1410 м до ул. Лесопарковая', 'город Челябинск ул. Худякова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3596, 2267, 0, 0, '', 'город Челябинск пересечение Комсомольского пр (в центр) и ул. Косарева  за остановкой',
        'город Челябинск Комсомольского пр / ул. Косарева  за остановкой', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3597, 2268, 61.380042, 55.149583, 'Россия, Челябинск, улица Энгельса, 52', 'город Челябинск ул. Энгельса  52',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3598, 2269, 61.308985, 55.176331, 'Россия, Челябинск, улица Братьев Кашириных, 114',
        'город Челябинск пересечение ул. Братьев Кашириных  114 и ул. 40 лет Победы', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3600, 2270, 61.356892, 55.147171, 'Россия, Челябинск, улица Худякова',
        'город Челябинск ул. Худякова (в центр)  1680 м до ул. Лесопарковая', 'город Челябинск ул. Худякова', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3601, 2271, 61.387875, 55.189933, 'Россия, Челябинск, Свердловский проспект, 7',
        'город Челябинск Свердловский пр.  7', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3602, 2272, 61.316037, 55.194979, 'Россия, Челябинск, Комсомольский проспект, 72',
        'город Челябинск Комсомольский пр.  72', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3603, 2273, 61.315713, 55.194013, 'Россия, Челябинск, Комсомольский проспект, 69',
        'город Челябинск Комсомольский пр.  69', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3604, 2274, 0, 0, '', 'город Челябинск пересечение Комсомольского пр.  86В и ул. Молдавская',
        'город Челябинск пересечение Комсомольского пр.  86В ', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3605, 2275, 61.349355, 55.175072, 'Россия, Челябинск, улица Университетская Набережная, 30',
        'город Челябинск ул. Университетская Набережная 30', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3606, 2276, 61.387767, 55.15945, 'Россия, Челябинск, проспект Ленина, 69', 'город Челябинск пр. Ленина  69',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3608, 2277, 61.33792, 55.057234, 'Россия, Челябинск, Уфимский тракт, 121/1',
        'город Челябинск Уфимский тракт  123/2 (11 км + 200)', 'город Челябинск Уфимский тракт  123/2', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3609, 2278, 61.384749, 55.091046, 'Россия, Челябинск, Троицкий тракт, 21к7',
        'город Челябинск Троицкий тр.  21/7 (поворот на станцию "Челябинск-Грузовой")', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3610, 2279, 61.284973, 55.208548, 'Россия, Челябинск, улица Скульптора Головницкого, 12',
        'город Челябинск ул. Бейвеля/ ул. Скульптора Головницкого  12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3612, 2280, 61.321822, 55.204233, 'Россия, Челябинск, улица Молодогвардейцев, 2',
        'город Челябинск ул. Молодогвардейцев  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3613, 2281, 61.420322, 55.242997, 'Россия, Челябинск, Хлебозаводская улица, 7А',
        'город Челябинск ул. Хлебозаводская  7а', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3614, 2282, 61.373799, 55.169066, 'Россия, Челябинск, улица Труда, 185А', 'город Челябинск ул. Труда  185А',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3615, 2283, 61.51328, 55.121088, 'Россия, Челябинск, Копейское шоссе, 1Г',
        'город Челябинск Копейское шоссе  1-г', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3616, 2284, 61.430374, 55.161527, 'Россия, Челябинск, проспект Ленина, 26к1',
        'город Челябинск пр. Ленина  напротив д. 26/1', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3618, 2285, 61.384542, 55.191521, 'Россия, Челябинск, Комсомольский проспект, 9',
        'город Челябинск Комсомольский пр.  9', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3620, 2286, 61.387471, 55.087862, 'Россия, Челябинск, Троицкий тракт, 25к3',
        'город Челябинск Троицкий тр.  25 к.3', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3621, 2287, 61.276834, 55.174645, 'Россия, Челябинск, проспект Героя России Евгения Родионова, 2',
        'город Челябинск пр. Героя России Е. Родионова  2', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3622, 2288, 61.47697, 55.109889, 'Россия, Челябинск, Новороссийская улица, 44',
        'город Челябинск ул. Новороссийская  44', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3623, 2289, 61.397676, 55.155736, 'Россия, Челябинск, улица Елькина, 76А', 'город Челябинск ул. Елькина  76а',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3624, 2290, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.1', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3625, 2291, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.2', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3626, 2292, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.3', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3627, 2293, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.4', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3628, 2294, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.5', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3629, 2295, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.6', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3630, 2296, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.7', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3631, 2297, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.8', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3632, 2298, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.9', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3633, 2299, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.10', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3634, 2300, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.11', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3635, 2301, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.12', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3636, 2302, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.13', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3637, 2303, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.14', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3638, 2304, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.15', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3639, 2305, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.16', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3640, 2306, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.17', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3641, 2307, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.18', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3642, 2308, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18',
        'город Челябинск ул.Дарвина 18  поз.19', 'город Челябинск ул.Дарвина 18', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3643, 2309, 61.454532, 55.18298, 'Россия, Челябинск, улица Бажова',
        'город Челябинск пересечение ул. Бажова и  пер. Лермонтова', 'город Челябинск ул. Бажова /  пер. Лермонтова',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3644, 2310, 61.379871, 55.192584, 'Россия, Челябинск, Комсомольский проспект, 10',
        'город Челябинск Комсомольский пр.  10', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3646, 2311, 61.357207, 55.194609, 'Россия, Челябинск, Комсомольский проспект, 26',
        'город Челябинск Комсомольский пр.  26', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3648, 2312, 61.336078, 55.178162, 'Россия, Челябинск, улица Братьев Кашириных, 107Б',
        'город Челябинск ул. Братьев Кашириных  107б', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3649, 2313, 61.344208, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 101',
        'город Челябинск ул. Братьев Кашириных  напротив д. 101', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3650, 2314, 61.295286, 55.181179, 'Россия, Челябинск, улица Чичерина, 23', 'город Челябинск ул. Чичерина  23',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3651, 2315, 61.344208, 55.178115, 'Россия, Челябинск, улица Братьев Кашириных, 101',
        'город Челябинск ул. Братьев Кашириных  101', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3652, 2316, 0, 0, '', 'город Челябинск автодорога Меридиан  напротив рынка "порт Артур"',
        'город Челябинск рынка "порт Артур"', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3662, 2317, 61.454189, 55.142422, 'Россия, Челябинск, Енисейская улица, 1', 'город Челябинск ул. Енисейская  1',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3663, 2318, 61.380877, 55.152377, 'Россия, Челябинск, улица Энгельса, 95',
        'город Челябинск пересечение ул. Энгельса  95 и ул. Курчатова', 'город Челябинск пересечение ул. Энгельса  95',
        '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3664, 2319, 61.363935, 55.191135, 'Россия, Челябинск, улица Чайковского, 15',
        'город Челябинск Комсомольского пр./ ул. Чайковского  15', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3667, 2320, 61.451898, 55.142685, 'Россия, Челябинск, Копейское шоссе',
        'город Челябинск Копейское шоссе и ул. Енисейская  разд. газон напротив ТК "Алмаз"',
        'город Челябинск Копейское шоссе / ул. Енисейская', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3668, 2321, 61.352419, 55.104728, 'Россия, Челябинск, улица Блюхера, 121Е',
        'город Челябинск ул. Новоэлеваторная/ ул. Блюхера  121Е', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3670, 2322, 61.40488, 55.18671, 'Россия, Челябинск, проспект Победы, 160В',
        'город Челябинск ул. Болейко/ пр. Победы  160В', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3672, 2323, 61.296175, 55.16414, 'Россия, Челябинск, улица Академика Королёва, 28',
        'город Челябинск пересечение ул. Братьев Кашириных и ул. Академика Королева 28', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3674, 2324, 61.465912, 55.116928, 'Россия, Челябинск, Батумская улица, 20', 'город Челябинск ул. Батумская  20',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3675, 2325, 61.449077, 55.178193, 'Россия, Челябинск, улица Героев Танкограда, 55',
        'город Челябинск ул. Героев Танкограда  55', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3676, 2326, 61.449706, 55.1772, 'Россия, Челябинск, улица Героев Танкограда, 57',
        'город Челябинск ул. Героев Танкограда  57', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3677, 2327, 61.329188, 55.199254, 'Россия, Челябинск, улица Молодогвардейцев, 12',
        'город Челябинск ул. Молодогвардейцев  12', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3678, 2328, 61.380302, 55.159393, 'Россия, Челябинск, проспект Ленина, 73/30', 'город Челябинск пр. Ленина  73',
        'город Челябинск пр. Ленина  73', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3680, 2329, 61.431102, 55.186433, 'Россия, Челябинск, проспект Победы, 124',
        'город Челябинск пр. Победы  124  поз. 7', 'город Челябинск пр. Победы  124', '');
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3682, 2330, 61.327311, 55.204793, 'Россия, Челябинск, улица Молодогвардейцев, 7',
        'город Челябинск ул.Молодогвардейцев 7', null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3683, 2331, 61.458878, 55.146708, 'Россия, Челябинск, Енисейская улица, 8', 'город Челябинск ул. Енисейская  8',
        null, null);
INSERT INTO point_suggestion (id, raw_permit_id, longitude, latitude, formatted, request, re_request, error)
VALUES (3684, 2332, 61.370106, 55.126848, 'Россия, Челябинск, улица Дарвина, 18', 'город Челябинск ул.Дарвина 18', null,
        null);