<?php

use LanguageSpecific\ArrayHandler;

require_once 'D:\project\chelyabinsk-karta\vendor\autoload.php';

const GEOCODER =
    'https://geocode-maps.yandex.ru/1.x/?lang=ru_RU&format=json'
    . '&apikey=344dde82-33ad-407f-b719-4e880eb28ff1&rspn=1'
    . '&bbox=61.042,54.968~61.759,55.375'
    . '&geocode=';

/**
 * @return false|resource
 */
function curlInit()
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($curl, CURLOPT_HTTPGET, true);
    return $curl;
}

/**
 * @param $rawAddress
 * @param $curl
 * @return bool|string
 */
function requestGeocoder($rawAddress, $curl)
{
    $address = str_replace(',', ' ', $rawAddress);
    $target = urlencode($address);
    curl_setopt($curl, CURLOPT_URL, GEOCODER . $target);
    $response = curl_exec($curl);

    return $response;
}

/**
 * @param $curl
 * @return string
 */
function getErrorText($curl)
{
    $error = '';
    $curlErrorNumber = curl_errno($curl);
    if ($curlErrorNumber) {
        $error = curl_error($curl);
    }

    return $error;
}

/**
 * @param array $properties
 * @param PDOStatement $import
 * @param PDO $connection
 * @return bool
 */
function updateSuggestion(array $properties, PDOStatement $import, PDO $connection)
{
    $longitude = $properties['longitude'];
    $latitude = $properties['latitude'];
    $formatted = $properties['formatted'];
    $error = $properties['error'];
    $id = $properties['id'];

    $stat = $import->execute([$longitude, $latitude, $formatted, $error,
        $id]);
    $isSuccess = $stat !== false;

    if (!$isSuccess) {
        echo "longitude=>$longitude, latitude=>$latitude,"
            . " formatted=>$formatted,error=>$error, id=>$id"
            . PHP_EOL;
        $error = $import->errorInfo();
        echo $error . PHP_EOL;
        /** @noinspection PhpUnusedLocalVariableInspection */
        $command = $connection->exec('ROLLBACK');
    }
    return $isSuccess;
}

function extractPointInfo($item, $id)
{
    $address = $item['GeoObject']['metaDataProperty']
    ['GeocoderMetaData']['Address'];
    $formatted = $address['formatted'];
    $components = $address['Components'];
    $isAllow = count($components) > 4;

    if ($isAllow) {
        $isAllow =
            $components[0]['kind'] === 'country'
            && $components[0]['name'] === 'Россия'
            && $components[1]['kind'] === 'province'
            && $components[1]['name'] ===
            'Уральский федеральный округ'
            && $components[2]['kind'] === 'province'
            && $components[2]['name'] ===
            'Челябинская область'
            && $components[3]['kind'] === 'area'
            && $components[3]['name'] ===
            'городской округ Челябинск'
            && $components[4]['kind'] === 'locality'
            && $components[4]['name'] === 'Челябинск';
    }
    if (!$isAllow) {
        echo "raw_permit_id = $id, have not enough data"
            . PHP_EOL;
    }
    $coords = [];
    If ($isAllow) {
        $pos = $item['GeoObject']['Point']['pos'];
        $coords = explode(' ', $pos);
        $isAllow = count($coords) === 2;
    }
    $longitude = 0;
    $latitude = 0;
    if ($isAllow) {
        $longitude = $coords[0];
        $latitude = $coords[1];
    }
    $pointInfo = ['formatted' => $formatted, 'longitude' => $longitude,
        'latitude' => $latitude];

    return $pointInfo;
}

$curl = curlInit();

try {
    $connection = new PDO('mysql:host=localhost;dbname=scheme', 'root', 'admin');
    $isSuccess = true;
} catch (Exception $e) {
    $isSuccess = false;
}
if ($connection === null) {
    echo 'не могу соединиться с базой' . PHP_EOL;
}
$export = null;
if ($connection !== null) {
    $export = $connection->prepare('
select id, re_request from point_suggestion where re_request is not null 
');
    $isSuccess = $export !== false;
}
$import = null;
if ($isSuccess) {
    $import = $connection->prepare('
update point_suggestion set longitude=?,latitude=?,formatted=?,error=?
where id=?
');
    $isSuccess = $export !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET NAMES \'utf8mb4\''
        . ' COLLATE \'utf8mb4_unicode_ci\'');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('START TRANSACTION');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $command = $connection->exec('SET AUTOCOMMIT = OFF');
    $isSuccess = $command !== false;
}
if ($isSuccess) {
    $isSuccess = $export->execute();
}
$suggestions = [];
if ($isSuccess) {
    $suggestions = $export->fetchAll(PDO::FETCH_ASSOC);
    $isSuccess = !empty($suggestions);
}
foreach ($suggestions as $call) {
    $street = $call['re_request'];
    $id = $call['id'];

    $response = requestGeocoder($street, $curl);
    $error = getErrorText($curl);

    $longitude = 0;
    $latitude = 0;
    $formatted = '';
    $properties = ['id' => $id, 'longitude' => $longitude,
        'latitude' => $latitude, 'formatted' => $formatted,
        'error' => $error];

    $isEmpty = true;
    $isAllow = empty($error);
    if ($isAllow) {
        $info = json_decode($response, true);
        $isAllow = array_key_exists('response', $info);
    }
    if (!$isAllow) {
        echo "'response' is empty, response is `$response`" . PHP_EOL;
        echo "id=>$id" . PHP_EOL;
    }
    if ($isAllow) {

        $request = $info['response']['GeoObjectCollection']
        ['metaDataProperty']['GeocoderResponseMetaData']['request'];

        $featureMember =
            $info['response']['GeoObjectCollection']['featureMember'];
        $isEmpty = count($featureMember) === 0;
    }

    if ($isEmpty) {
        $isSuccess = updateSuggestion($properties, $import,
            $connection);
        if (!$isSuccess) {
            break;
        }
    }
    if (!$isEmpty) {
        foreach ($featureMember as $item) {
            $pointInfo = extractPointInfo($item, $id);
            $info = new ArrayHandler($pointInfo);
            $formatted = $info->get('formatted')->str();
            $longitude = $info->get('longitude')->double();
            $latitude = $info->get('latitude')->double();

            $properties['formatted'] = $formatted;
            $properties['longitude'] = $longitude;
            $properties['latitude'] = $latitude;

            $isSuccess = updateSuggestion($properties, $import,
                $connection);

            if (!$isSuccess) {
                break;
            }
        }
    }
}
if ($isSuccess) {
    $command = $connection->exec('COMMIT');
}
if ($connection !== false) {
    $command = $connection->exec('SET AUTOCOMMIT = ON');
}

